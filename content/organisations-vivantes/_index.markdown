﻿---
date: 2022-03-01
title: "Les organisations vivantes"
url: /organisations-vivantes/
---

# Avertissement : texte inachevé

# LES ORGANISATIONS VIVANTES

C'est un travail en cours -- depuis 2014 -- chaque année à l'approche de Noël je me dis que je vais le finaliser. Cette année je me dis que je vais le rebâtir peu à peu avec des articles (changer des parties), et écrire les morceaux manquants toujours en utilisant des articles à écrire dans les mois à venir.

Le texte est susceptible de changer à tout moment. Il est à l'état de brouillon. Vos commentaires sont les bienvenus. 

{{< image src="/media/dance_large.jpg" title="Dance" >}}

> « There are two path you can go by, but in the long run 
> There’s still time to change the road you’re on »
> -- Robert Plant, Stairway to heaven

