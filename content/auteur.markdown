---
title: Auteur
url: /auteur/
symbol: 5
---

### 2025, "Petit manuel de pensée organisationnelle", publibook

{{< image src=/images/livres/petit-manuel-pensee-organisationnelle.jpg width="200" >}}

**Un manuel pour s'organiser ? Comment faire les groupes ? Comment prendre les décisions ? Comment communiquer ? Cela reste très accessible (pas de jargon). Et c'est la somme de 15 ans de blogging finalement.**

*S’organiser c’est décider du tissu social que l’on souhaite pour l’avenir. S’organiser, c’est traiter autant de culture que de pratique, et définir jusqu’à notre nature. Comment dessiner de nouvelles organisations, plus fonctionnelles, plus épanouissantes, plus respectueuses et plus efficaces pour ceux qui le veulent ?
L’ouvrage que vous tenez entre vos mains a deux publics.
Tout d’abord, les dirigeants, entrepreneurs, coachs, leaders, etc., car l’un des objectifs de ce livre est de délivrer des principes, des manières de faire, sur des façons de s’organiser pour évoluer avec élégance et frugalité dans le monde si complexe et compliqué d’aujourd’hui, ou mieux encore, dans le monde probablement transformé de demain.
Le deuxième public, ce sont mes enfants Sarah, Mathieu, Arthur, et j’ajoute Sibylle, la fille de ma compagne. Aujourd’hui les nouvelles des scientifiques sont de plus en plus alarmantes, ce qui était prévu pour 2100 est annoncé pour 2050. Comment mes enfants vont-ils faire face à l’avenir ? Cela m’interroge. Comment les aider ? Je me dépêche ici de consolider les connaissances que j’ai sur l’organisation dans un monde complexe, incertain, le leur, celui de demain.
Et, quel que soit le cadre de vie des générations futures, j’espère que chacun sera en mesure de s’épanouir et d’organiser une vie avec du sens, qui finalement fera passer notre époque tourmentée pour un mal nécessaire.*

Trouver le livre (EAN 9782342377484): 

{{< livres show="livres/livres.csv" >}}

{{<br>}}

* Mettre un retour sur [Babelio](https://www.babelio.com/livres/Pernot-Petit-manuel-de-pensee-organisationnelle/1800104)  


### 2019, Kanban, l’approche en flux pour l’entreprise agile, Dunod, édition 1

{{< image src=/images/livres/kanban-dunod.jpg width="200" >}}

**Édition 2 disponible le 21 mai**

*Cet ouvrage a pour ambition d’ouvrir kanban à tout lecteur souhaitant s’emparer de cette méthode d’amélioration, quel que soit son environnement.
Kanban ne s’attache pas à un domaine particulier et peut aider toute organisation à visualiser son flux de travail et ses processus. Cette approche, née dans l’automobile au Japon dans les années 1950, permet aujourd’hui aux structures, dans un contexte agile, d’aller plus loin dans l’amélioration ponctuelle ou globale de leurs méthodes de travail et de leur organisation, en assurant une meilleure fluidité.*

* *La première partie est une ouverture sur kanban destiné à n’importe quel groupe dans l’organisation.*
* *Les deuxième et troisième parties décrivent le noyau de kanban et suivent la démarche d’implémentation, qui va de la conception d’un système à son évolution*.
* *La quatrième partie est consacrée à l’appropriation de kanban dans l’entreprise avec des grilles de lecture contextuelle.*

*Les auteurs ont poussé le souci de pédagogie jusqu’à inclure une annexe pour expliquer kanban aux enfants, au cas où certains adultes seraient restés de grands enfants…*

Trouver le livre (EAN 9782100781058): 


{{< livres show="livres/kanban.csv" >}}

{{<br>}}

* Mettre un retour sur [Babelio](https://www.babelio.com/livres/Morisseau-Kanban--Lapproche-en-flux-pour-lentreprise-agil/1388948)

---

{{< br >}}

# Essais numériques

### 2024 - La disparition

* [La disparition](https://pablopernot.fr/pdf/2024-la-disparition.pdf), parler agile sans parler agile.

### 2022 - L'énigme RH

* [L'énigme RH](/pdf/enigme-rh.pdf) - Comment passer de bonnes soirées avec mes ami(e)s même en étant RH - 96 questions pour changer notre vie de RH

### 2021 - Agilité, regard critique sur les dernières années pour bien débuter les suivantes

* [Agilité, regard critique sur les dernières années pour bien débuter les suivantes](/pdf/2021-regard-agilite-20-ans.pdf)

### 2014-2017 - Texte inachevé sur les organisations vivantes

* [Les organisations vivantes - texte inachevé, 2014-2017](/organisations-vivantes/)

### 2014-2017 - Guide de survie à l'agilité et à Scrum

* [Le guide de survie à l'agilité et à Scrum](/pdf/guiderapide.pdf).

### 2013 La horde agile  

* [La horde agile, le mini livre, 2013](/pdf/lahordeagile.pdf), pdf, 42 pages, \~4mo
* [la horde agile, le mini livre (epub), 2013](/epub/la-horde-agile.epub),
    epub, 42 pages, \~3.5mo
* [Article associé](/2014/01/mini-livre-la-horde-agile/) avec les informations complémentaires

