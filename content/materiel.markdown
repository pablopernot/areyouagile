---
title: Matériel
url: /materiel/
alchemy: 2
---


[Vidéos](#vidéos) - [Podcast](#podcast) - [Slides](#slides) - [Ateliers](#ateliers) - [Vieux supports de formation](#vieux-supports-de-formation)

# Vidéos

* 2024, La disparition, Agile Niort 2024, [[youtube]](https://www.youtube.com/watch?v=BzgV7Tnk3To)
* 2022, Aborder le produit de demain, School of product 2022 [[peertube]](https://peertube.pablopernot.fr/w/heXbKpq6jqzum4rrdmaG2C) [[youtube]](https://www.youtube.com/watch?v=ILm7Bclx9hQ)
* 2021, This is not scrum or agile *ceci n'est pas du scrum / agile* [[peertube]](<https://peertube.pablopernot.fr/w/kcFjR7P1Y46zU53jWTJTdv>) [[youtube]](https://www.youtube.com/watch?v=cXK0T-8R73g)
* 2021, Ceci n'est pas du scrum, keynote agile tour Montpellier [[peertube]](https://peertube.pablopernot.fr/w/cbUL3WWeK9sWjemVW8ym8S) [[youtube]](https://www.youtube.com/watch?v=VV9pY0Yk4Rs)
* 2021, From basics to foundation / Frug'Agile Armenia  [[peertube]](https://peertube.pablopernot.fr/w/16R8QrCXHKT3pHo8zoM34g) [[youtube]](https://www.youtube.com/watch?v=FAIc7q3TqmQ)
* 2021, Roadmaps & dépendances [[peertube]](https://peertube.pablopernot.fr/w/xedFscAi2bT14iaSGtLC1A) [[youtube]](https://www.youtube.com/watch?v=qkSWp36USB4)
* 2021, Agilité, regard critique sur les dernières années. Frug'agile. [[peertube]](https://peertube.pablopernot.fr/w/qAspRdf2wS2XoFGphcsCaH) [[youtube]](https://www.youtube.com/watch?v=ykqERA7a9yk)
* 2020, Storytelling, mémoriser et transmettre [[peertube]](https://peertube.pablopernot.fr/w/9k4rpVDoBNFhA31zD3FY7z) [[youtube]](https://www.youtube.com/watch?v=uOlNwFaqBXA)
* 2020, Meetup Entreprises complexes, partie 2 [[peertube]](https://peertube.pablopernot.fr/w/3LnNvrKj2wd476kJPCqyVs) [[youtube]](https://www.youtube.com/watch?v=Iq0byH0lR-w)
* 2020, Meetup Entreprises complexes [[peertube]](https://peertube.pablopernot.fr/w/pPbVH6ZUBUfz5f17RpmB14) [[youtube]](https://www.youtube.com/watch?v=e9c4hu_lg-Y)
* 2020, Cynefin, décider dans la complexité[[peertube]](https://peertube.pablopernot.fr/w/wMAgKboPyxzfCXn4G4Lk1J) [[youtube]](https://www.youtube.com/watch?v=nhLkuhTjTAM)
* 2020, Utiliser le non-sens et l'absurde pour le changement ? [[peertube]](https://peertube.pablopernot.fr/w/ab9kfs48vZuUdFDw23CKvK)
* 2020, Conversation autour du clean coaching [[peertube]](https://peertube.pablopernot.fr/w/1F2heEULc4W6TkYnRv9Egj) [[youtube]](https://www.youtube.com/watch?v=RY_4iopXzB4)
* 2020, Mind maps : Impact map & Agile strategy map [[peertube]](https://peertube.pablopernot.fr/w/3NvkGCvjFnuCjK4KcgUW34) [[youtube]](https://www.youtube.com/watch?v=X1DlbPInh1s)
* 2020, Travail à distance, équipes distribuées [[peertube]](https://peertube.pablopernot.fr/w/gFu8kHEQbkQULDwm7fERPe) [[youtube]](https://vimeo.com/405493699)
* 2017, Keynote Agile Grenoble, être agile et sur l'engagement [[peertube]](https://peertube.pablopernot.fr/w/8YSp7gxLNgVan1kcRGeuRZ) [[youtube]](https://www.youtube.com/watch?v=pjH0x21pYA0)
* 2016, Open Adoption, NCrafts 2016 [[peertube]](https://peertube.pablopernot.fr/w/ktDh8A4ZZB3UQmdLsxERKx) [[vimeo]](https://vimeo.com/167722770)
* 2015, Lean Topologies, Flowcon [[peertube]](https://peertube.pablopernot.fr/w/fay2sqmPf9NEV4smBkPpZt) [[youtube]](https://www.youtube.com/watch?v=a9KrjWv_Rt8)
* 2015, Pecha Kucha about Slums, ALE2015 [[peertube]](https://peertube.pablopernot.fr/w/s98j8LtFi6HKDapJZKk7Gm) [[youtube]](https://www.youtube.com/watch?v=VQNQnHRo-b8)
* 2014, Open Agile Adoption - Pablo Pernot et Oana Juncu [[peertube]](https://peertube.pablopernot.fr/w/fyPy5bHQyREmKt59us9ztF) [[vimeo]](https://vimeo.com/104987420)
* 2013, Keynote Agile Tour Toulouse, La horde agile, partie 2 [[peertube]](https://peertube.pablopernot.fr/w/47wbCgZwcz1t22tcsVPEmb) [[vimeo]](https://vimeo.com/78405990)
* 2013, Keynote Agile Tour Toulouse, La horde agile, partie 1 [[peertube]](https://peertube.pablopernot.fr/w/poSyT91K5LS8KPezw1HA6L) [[vimeo]](https://vimeo.com/78404401)
* 2013, Keynote Agile Tour Montpellier, La horde agile [[peertube]](https://peertube.pablopernot.fr/w/7uCigVHqmtkhbC5jEcGyrL) [[youtube]](https://www.youtube.com/watch?v=a0cI-E603bI)
* 2012, Agile Tour Montpellier, La stratégie du product owner avec Alexis Beuve [[peertube]](https://peertube.pablopernot.fr/w/9gKTm1bms2z5wJaRibZ6CN) [[youtube]](https://www.youtube.com/watch?v=g7mYDe3ZIcM)
* 2012, Sudweb 2012, Marshmallow challenge [[peertube]](https://peertube.pablopernot.fr/w/cbo5eDAEz84ioU1pXwJu1j) [[vimeo]](https://vimeo.com/53396148)
* 2011, Sudweb 2011, Anatomie d'une mission agile [[peertube]](https://peertube.pablopernot.fr/w/rBTJLzCTNCvDkApScL8srb) [[vimeo]](https://vimeo.com/53349009)

---

# Podcast

* 2019, Révolutionner une entreprise, Da Scritch, Carré, Petit, Utile [[peertube]](https://peertube.pablopernot.fr/w/3cABdp4Z1jiSspTNZEKpH8) [[Chez CPU !]](https://cpu.dascritch.net/post/2019/03/21/Pablo-Pernot%2C-agent-provocateur-et-coach-agiliste-pour-BeNext)
* 2019, La transformation commence par celles des managers, Thomas Wickham, Café Craft [[peertube]](https://peertube.pablopernot.fr/w/3PdhWmfNZFSw4u9CQe3NXt) [[spotify]](https://open.spotify.com/episode/2jWqBst2sxUQA7T7TZKg0A?si=16cbe2d6f95c4782)
* 2019, La transformation commence par celles des managers, Questions du public] [[peertube]](https://peertube.pablopernot.fr/w/hHw61BbL4KRgMywkDPPP9s) [[spotify]](https://open.spotify.com/episode/0C55RPFcIP6DCWyCy9wYpk?si=862e07b33d5d4d0d)
* 2018, Agilité à l'échelle, Café Craft, Thomas Wickham [[peertube]](https://peertube.pablopernot.fr/w/eH4HCtv2MFeVRYjAFb4c64) [[spotify]](https://open.spotify.com/episode/0LYgFF0FROt2AsS8aR8awx?si=1deb7f4bfe9343c4)

---

# Slides

* 2024, [La disparition, Agile Niort](/pdf/2024-la-disparition.pdf)
* 2023, [Conversation sur les transformations agiles - SG Casabalanca](/pdf/2023-sgmaroc.pdf)
* 2023, [Interventions jour 2 au SICT (*doctoral school on sustainable ICT*)](/pdf/2023-SICT-D2.pdf)
* 2023, [Interventions jour 1 au SICT (*doctoral school on sustainable ICT*)](/pdf/2023-SICT-D1.pdf)
* 2021, [Ceci n'est pas du scrum / agile](/2021/12/ceci-n-est-pas-du-scrum/)
* 2021, [Roadmaps & dépendances](http://pablopernot.fr/pdf/2021-roadmaps-dependances.pdf)
* 2017, [Perspective plutôt que framework : Agile Fluency, Agile France 2017](https://www.slideshare.net/pablopernot/perspective-plutt-que-framework-agile-fluency)
* 2016, [Open Adoption, Dare to invite, Ncrafts](https://speakerdeck.com/pablopernot/open-adoption-dare-to-invite)
* 2015, [Lean Topology, Flowcon 2015](https://www.slideshare.net/pablopernot/lean-topology-lean-kanban-fr-15)
* 2015, [Open Adoption, Darefest, Antwerpen](https://www.slideshare.net/pablopernot/open-adoption-darefest-2015)
* 2014, [Transformation RH, agile & RH, avec Jas Chong](https://speakerdeck.com/pablopernot/transformation-rh)
* 2014, [Les organisations vivantes](https://speakerdeck.com/pablopernot/les-organisations-vivantes)
* 2014, [Open Agile Adoption avec Oana Juncu](https://speakerdeck.com/pablopernot/open-agile-adoption-scrumday-2014)
* 2013, [Storytelling Battle - Pablo Pernot & Oana Juncu](/pdf/storytellingbattle.pdf)
* 2012, [Agilité : Comment ne pas devenir un shadok](https://www.slideshare.net/pablopernot/agilit-comment-ne-pas-devenir-un-shadok)
* 2012, [Stratégie du product owner, avec Alexis Beuve](https://speakerdeck.com/pablopernot/strategie-du-product-owner)
* 2012, [Management visuel & cognitif](https://speakerdeck.com/pablopernot/management-visuel-and-cognitif)
* 2011, [Agile & CMMi, potion magique ou grand fossé ?](https://www.slideshare.net/pablopernot/agile-cmmi-potion-magique-ou-grand-foss)
* 2011, [Anatomie d'une mission agile](https://www.slideshare.net/pablopernot/anatomie-dune-mission-agile)

---

# Ateliers

### La scierie à pratiques, 2013-2016

Avec Stéphane Langlois.  

* [La scierie à pratique évolue](/2016/02/la-scierie-à-pratiques-évolue/), 2016
* [La scierie à pratiques le PDF](/pdf/scierie-a-pratiques.pdf), un jeu pour échanger sur nos pratiques, 2013.
* [Agile ADN, scierie à pratique première version](/2013/05/adn-agile-feedback/), 2013

### Le projet dont vous êtes le héros agile, 2012-2013

Le projet dont vous êtes le héros agile avec [Bruno](http://jehaisleprintemps.net/) et [Antoine](https://blog.crafting-labs.fr/).

* [Le projet dont vous êtes le héros agile](/heros/)

### Peetic, 2012-2015

Un scénario qui permet d'outiller mes formations, on y retrouve pas mal d'éléments d'un projet/produit agile.
[Claude](http://www.aubryconseil.com/) m'a fait le plaisir d'en intégrer des morceaux dans son livre ("Scrum") et nous l'utilisons dans le [raid
agile](http://www.raidagile.fr).

* 2012 : [Episode 1](/2012/11/peetic/)
* 2012 : [Conversations](/2012/12/peetic-conversations/)
* 2014 : Un article de [l'idée au plan de livraison](/2014/06/de-lidee-au-plan-de-livraison/)
* 2015 : [Un peu de croquettes](/2015/05/peetic-2015-un-peu-de-croquettes/)

---

# Vieux supports de formation

### Vieux supports de formation, 2012-2013-2014

* [Support de formation "agile" généraliste sur 3 jours](/pdf/pratiques_de_lagilite_2_1_15.pdf), 12.5mo, pdf, 2012.
* [Initiation agilité 1 journée](/pdf/initiation-agilite-0-6.pdf), 6.5mo, pdf, 2013.
* [Agilité pour les entrepreneurs, donnez l'initiative à vos équipes](/pdf/entrepreneurs.pdf), 7.2mo, pdf, 2014.
* [Product Owner](/pdf/product-owner-2014-1-0.pdf), 8.5mo, pdf, 2014.
* [Kanban](/pdf/kanban-0-8.pdf), 12mo, pdf, 2014.
* [Lean Software Development](/pdf/LSD-08.pdf), 8.5mo, pdf, 2014.

### Modules sur une demi-journée

* [Vision & Sens](/pdf/vision-sens-1-1.pdf), 1.9m, pdf, 2013.
* [Expression du besoin](/pdf/expression-du-besoin-1-1.pdf), 956ko, pdf, 2013.  
* [Introduction à Kanban](/pdf/kanban-1-3.pdf) , 2.2m, pdf, 2013.
* [Optimiser la valeur](/pdf/optimiser-1-1.pdf) (Value Stream Mapping), 683ko, pdf, 2013.
* [Zoom Scrum](/pdf/zoom-scrum-1-1.pdf), 800ko, pdf, 2013.
* [Responsable produit](/pdf/responsable-produit-1-1.pdf), 2.8m, pdf, 2013.
* Un [Scrumshot](/2012/11/scrumshot/) qui me permet de faire un panorama sympa sur Scrum, 2012.
