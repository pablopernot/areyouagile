---
date: 2018-06-04
slug: profession-de-foi
title: "Profession de foi"
---

Aujourd'hui pour réformer les organisations, repenser les produits, re-imaginer ses relations sociales en entreprises, définir si on souhaite utiliser une recette connue (comme un socle à la "spotify", "SAFe", "Less", etc.), des cadres ("Scrum", "Kanban"), des approches ("Lean Startup", Beyond Budgeting", "Holacratie", "Design Thinking", etc) reste une étape importante, mais à la portée de tous. C'est une petite partie de mon rôle de vous aider cependant sur ces sujets. 

Définir jusqu'où, à quel rythme, comment, dans quel sens, l'adaptation et l'émergence de votre propre écosystème doit se réaliser est déjà moins facile à appréhender. Mais si c'était facile cela ne serait pas la question. C'est une partie du rôle de personnes comme moi de vous accompagner là dessus. 

Mais j'en viens à penser avec le temps que la clef de voûte de tout ceci c'est la volonté (l'intention) et la façon d'être (l'attitude) de personnes importantes. Importantes dans le sens où elles ont des leviers, où elles se trouvent à des moments et des endroits stratégiques[^importantes]. Mon rôle consiste à accompagner ces personnes. À m'assurer de leurs intentions, ou les faire éclore, les coacher dans leur savoir être, les nourrir d'autres expériences et d'un savoir. 

Les évolutions des organisations passent par les transformations des personnes qui les composent. L'organisation est la somme des individus. Je travaille avec les individus sur leur savoir être, avec ces personnes importantes[^importantes] qui orientent, déclenchent, les flux et les énergies de l'organisation. Je travaille avec des gens qui réalisent cela et qui veulent s'emparer de ces sujets, ou des organisations qui souhaitent que des gens réalisent cela. 

[^importantes]: Il n'y a pas de jugement de valeur. D'autant que généralement tout cela est cyclique.  



