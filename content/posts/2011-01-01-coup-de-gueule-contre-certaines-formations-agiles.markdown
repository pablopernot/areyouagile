---
date: 2011-01-01T00:00:00Z
slug: coup-de-gueule-contre-certaines-formations-agiles
title: Coup de gueule contre certaines formations agiles
---

Je débute l'année par un coup de gueule contre certaines formations
agiles. Je ne les citerai pas car je me permets ce coup de gueule sans
avoir participé à l'une d'elles, sans connaitre les formateurs, ni la
société qui les parraine (prenez donc ce coup de gueule avec les
pincettes qui vont plairont). Je me permets ce coup de gueule en raison
des feedback réguliers que j'obtiens des stagiaires qui y participent
depuis la fin d'année 2010. Coup de gueule contre des formations où les
gens sortent avec plus de troubles et de questions qu'avec une vraie
réflexion ou un vrai apprentissage, et surtout le sentiment d'avoir un
peu été mené en bateau (quand on plus on sait qu'il s'agit de
formations certifiantes, chères, et que l'on connaît la valeur de la
certification .... délivrée automatiquement en fin de session...). Coup
de gueule contre les formateurs qui planent et qui répondent sans aucun
sens commun, sans aucune mise en situation, qui trônent dans
leurs éthers.

Voici certains des retours que j'ai eu :

"Pourquoi Scrum ?"

"Parce que cela va augmenter de 400% votre productivité ! "

"ah... et pourquoi ?"

"Parce que c'est Scrum !"

ok on est bien avancé...

"Je travaille avec une équipe QA (assurance qualité) de 10 personnes en
Roumanie, comment dois-je l'intégrer avec mon framework Scrum à Paris ?"

"Faites la venir en France"

C'est probablement une bonne réponse **dans l'absolu**, mais il est
inconvenant de la limiter à cela. Je sais bien qu'il faille un
électrochoc, et que devenir "agile" est un changement culturel fort. Je
comprends et je pratique souvent la "réponse par une question" en
formation, mais cela doit être un cheminement, une réflexion qui doit
s'ancrer dans les pratiques en cours :  avec une mise en situation
nécessaire, une mise en évidence des contraintes, des risques, des
objectifs, etc.

Une connaissance me rétorque : "oui mais 2 ou 3 jours pour entamer une
révolution psychologique personnelle c'est court". Oui, naturellement,
mais cette formation est souvent un premier pas essentiel. Si on a
l'impression en sortant que le formateur a bouffé trop de champignons,
c'est déjà un très mauvais départ, et cela ne va pas nous aider au
quotidien !

Sur ce, meilleurs voeux 2011
