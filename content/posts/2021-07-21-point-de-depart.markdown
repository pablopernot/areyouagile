---
date: 2021-07-21
slug: point-de-depart 
title: "Faire de ma cible mon point de départ : appréhender l'émergence"
--- 

Sauf si vous bénéficiez d'un pouvoir secret ou d'une machine à se déplacer dans le temps, il faut bien se rendre à l'évidence, tout ce que nous vivons est au présent. On parle beaucoup d'émergence, car on s'est bien rendu compte que les choses ne deviennent jamais complètement ce que nous avions prévu, avec un petit ou un grand écart. Dans cet entrelacement de causes et effets qui font la complexité, l'émergence a pris tout son sens. Et l'émergence c'est le futur. 

Pour bien travailler conjointement avec l'émergence, une attitude, une posture me paraît indispensable : tout considérer comme un point de départ et non pas comme un point d'arrivée. Car vous aurez beau tourner le sujet dans tous les sens, tout ce que vous avez en votre possession c'est le passé et le présent, mais seul le présent vous pouvez le manipuler, le diriger, le manager. 

Si on accepte, si on comprend, que tout est un point de départ, on saura recevoir, appréhender, l'émergence. 

Cela veut dire quoi ? 

Si, par exemple, je souhaite réorganiser mon entreprise de "telle façon". Si je me projette : l'entreprise devrait s'organiser de telle façon pour telles raisons. Cette "telle façon" est mon point de départ, même si c'est une cible, ou une vision. Car la complexité va faire que les choses seront un peu ou très différentes dans le futur, inévitablement. Vous savez "la carte et le territoire". 

Ainsi soit je comprends que c'est mon point de départ et je sais appréhender, recevoir, l'émergence qui va faire que les choses seront différentes. 

Soit je décide *mordicus* que les choses seront de "telle façon" et je nie la complexité et le monde qui nous entoure en fossilisant, en rendant immobiles les choses, en les asphyxiant (pas d'espace, pas d'air). Cette façon de faire échoue, en tous cas dans les environnements complexes, ou détruit énormément la valeur des choses (elles sont statufiées).

Comprendre que ma cible est mon point de départ permet d'appréhender l'émergence. L'entrelacement des choses dans un contexte complexe fait émerger, apparaître, poindre de nombreux éléments facteurs qui vont changer, bouger, améliorer, adapter votre cible, etc., pas la peine de vous faire un dessin. 

* "Je réorganise mes équipes", est un point de départ.
* "Je souhaite faire carrière dans la médecine", est un point de départ 
* "Je veux faire le tour du monde en 80 jours", est un point de départ. 
* "Nous avons le meilleur produit du monde", est un point de départ. 
* "La banque veut faire (ou fait) une transformation agile et digitale (combo)", est un point de départ.

Nous sommes au présent. Si vous comprenez que ces choses sont votre point de départ, votre attitude sur ces sujets change drastiquement, en mieux !

Si on en fait un point d'arrivée, nous ne saurons pas appréhender ce qui va véritablement se passer, on va s'asphyxier, se statufier. Si on en fait un point de départ, on est attentif *de facto* à ce qui va se passer. Il s'agit bel et bien d'un état d'esprit. 

## Le mieux est l'ennemi du bien 

Cet état d'esprit amène deux choses avec lui en sus. D'une part vous serez plus sensible à l'effort que vous souhaitez mettre à définir votre point de départ sachant qu'il est le point de départ et non d'arrivée. Est-ce utile alors de beaucoup le détailler sachant qu'il va se révéler différent ? Des fois oui, des fois non. Mais il y a une prise de conscience sur un effort de description qui se veut surtout rassurant. Mais quand on sait que cette assurance est factice, on est moins enclin à la rechercher.   

## L'éternel comment, l'éternel pourquoi

Et d'autre part, si vous saisissez mentalement que vous travaillez sur un point de départ, le comment, le moyen, s'estompe en faveur du "pourquoi". Cela ne veut pas dire que votre point de départ se doit d'être un "pourquoi". Mon point de départ peut demeurer "je réorganise mes équipes", mais cela met l'accent sur le "pourquoi".  

### Mes petits principes de management

* [L'intention première](/2021/06/intention-premiere/)
* [Vers le simple, vers le complexe](/2021/07/simple-complexe/)
* [Ma cible est mon point de départ : appréhender l'émergence](/2021/07/point-de-depart/)
* [Cultiver l'imperfection](/2021/09/cultiver-imperfection/)