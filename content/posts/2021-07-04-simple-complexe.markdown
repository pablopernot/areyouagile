---
date: 2021-07-04
slug: simple-complexe 
title: "Vers le simple, vers le complexe"
--- 

J'ai l'impression d'être un petit enfant qui manipule des concepts éculés (ou trop grands pour moi) quand je vous parle de complexe ou de simple. Cependant, dans cet objectif de recensement de mes convictions et de mes petites approches du management, je le pose sur le papier pour le fixer. 

* Dans une situation complexe, pour m'aider à la gérer, à la "manager", à la ramener à quelque chose de maîtrisable, je la simplifie.

* Dans une situation simple, pour m'aider à l'enrichir, à la sortir des causes et effets déjà observés, trouver de nouvelles alternatives, je la complexifie.

### Du complexe vers le simple

Pour le premier, du complexe vers le simple fin, c'est une chose apprise à l'usage des approches agiles. Comment je simplifie le complexe ? J'applique une ou des contraintes. J'accrois ainsi la prédictibilité, ou *a minima* je ramène les choses à un statut observable. Exemple : je ne sais absolument pas comment ce produit que nous fabriquons va finir ? Simplifier pour rendre observable, contrôlable, manipulable : délivrons ce que nous avons toutes les deux semaines. Cette contrainte me ramène dans une dimension que je peux *manager* (gérer) car elle limite (contrainte) les possibilités.

La contrainte que j'applique est un cadre. Un cadre limite. Et pas un "comment", ce que j'aime appeler un "geste". Je ne dis pas "faisons ce produit de cette façon" ce qui ne limite en rien les possibilités et garde ouvert toute la complexité. Je propose : montrez-moi ce que nous avons de fini toutes les deux semaines (peu importe "comment" et "quoi"). Ainsi je capture la complexité.   

On ne *manage* pas ce que l'on ne peut pas représenter disait, je crois, Peter Drucker. Tant que tout peut arriver, je ne peux pas représenter ce qui va arriver et quand. J'ai besoin de définir cet espace dans lequel j'aurais une image, une représentation de ce qui arrive. Je définis un cadre et je vois ce qu'il capture. Ce qu'il capture je peux le gérer, le *manager*. 

Comme tout cadre, je fais attention à laisser de l'espace : sans espace rien ne se produit. Je peux demander à ne rien produire du tout concernant ce produit. J'applique ainsi une contrainte énorme qui me donne une prédictibilité énorme : je n'aurais très probablement rien. 
Et à l'inverse sans cadre, tout peut se produire, et "tout" c'est souvent trop. 

C'est souvent un principe fort que j'utilise pour responsabiliser : "faîtes ce que vous voulez tant que". Tant que ? par exemple : "tant que les chiffres consolidés sont bons à la fin de chaque semaine", ou "tant que le budget reste identique tous les mois", "tant que personne ne se plaint de vous", "tant que nous avons quelque chose à démontrer à la fin de chaque itération", etc., etc.  

Si la contrainte est trop forte, vous n'obtiendrez peut-être rien, du vide, il faudra l'ajuster. 

### Du simple vers le complexe

À l'inverse des situations semblent imperturbablement se dérouler toujours pareil. Un incessant déjà-vu. C'est simple quand on fait cela on obtient cela. Ici je cherche à sortir de ce cadre. À penser hors de la boite dit le dicton. Pour produit quelque chose de nouveau il faut complexifier. Complexifier c'est faire que quelque chose de nouveau, d'inattendu, se produise. 

On peut complexifier en introduisant un élément de plus. En augmentant les combinaisons possibles, jusqu'à ce que quelque chose de nouveau émerge. 

"Pour résoudre cette situation, il faudrait introduire une personne de plus dans la conversation" avait subtilement suggéré [Élisabeth Georges](https://www.linkedin.com/in/elisabeth-georges-05bb317/) lors d'une intervention chez nous. Cette solution pour complexifier m'avait frappé comme un moment "haha", une épiphanie. Cela avait rendu palpable ce concept que je manipulais intuitivement inversement (simplifier) jusqu'alors. 

C'est toute l'idée de l'intelligence collective : croisons-les nous verrons bien ce qui arrive. C'est la solution soutenue par le [modèle Cynefin de Dave Snowden](/2020/04/meetup-cynefin/) : dans le domaine du complexe, déclenchez en parallèle plusieurs équipes vous accélérerez et augmenterez les chances de voir quelque chose émerger. "Essayez de maximiser les interactions pour augmenter les chances de voir émerger quelque chose".     

On ajoute quelque chose qui fasse que les combinaisons permettent de sortir du cadre, amène des choses nouvelles. Introduire une troisième personne dans un entretien annuel par exemple amène des perspectives différentes. 

### Du chaos

J'ai observé une troisième voie qui peut être utilisée pour faire émerger de nouvelles combinaisons. C'est un assemblage des deux approches évoquées plus haut, mais cela peut se révéler frustrant, douloureux, car on génère du chaos. Et le chaos demande une réaction, mais il est subit. Il s'agit d'appliquer des contraintes pour interdire à ce que les mêmes combinaisons ressortent. Inter-changer les membres d'une équipe au hasard va amener des perspectives différentes (je n'ai pas dit forcément mieux). Forcer les gens à réaliser le produit avec un outil différent (un langage différent par exemple). En un mot : tout ce que tu veux sauf ce que tu fais jusque là.  

Là encore : si la contrainte est trop forte, vous n'obtiendrez peut-être rien, du vide, il faudra l'ajuster. 

### Conclusion

Deux mouvements pour manager selon vos besoins : vers le simple, vers le complexe.

Si des gens peuvent me donner des pointeurs vers les sources théoriques de ce vécu, si je ne dis pas trop d'âneries, je suis preneur. Merci. 



### Mes petits principes de management

* [L'intention première](/2021/06/intention-premiere/)
* [Vers le simple, vers le complexe](/2021/07/simple-complexe/)
* [Ma cible est mon point de départ : appréhender l'émergence](/2021/07/point-de-depart/)
* [Cultiver l'imperfection](/2021/09/cultiver-imperfection/)