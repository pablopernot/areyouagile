---
date: 2017-09-07T00:00:00Z
slug: estimation-visuelle
title: Estimation visuelle
---

J'ai l'impression de revenir sur un sujet largement débattu par
ailleurs, mais ne trouvant pas chaussure à mon pied sur les différents
articles en ligne, je décide de rapidement revenir sur cette question de
la cotation, de l'estimation, rapide et visuelle. Au détour d'une de mes
interventions, lors de ce qu'appelle désormais le milieu agile un "PI
Planning", j'observe une nouvelle fois un écueil souvent répété : on
prend plus de temps à pinailler sur des estimations que de se poser les
vraies questions sur ce que l'on doit fabriquer, que de faire du
*storytelling* pour mieux faire comprendre et comprendre son besoin.

Naturellement l'estimation, la cotation, est initialement un alibi utile
pour une réelle conversation, mais quand le sujet devient 3 ou 2, 0, 5
ou 1, l'estimation elle-même, on a raté quelque chose et perdu du temps.
Idéalement il ne faudrait plus estimer, mais bien découper les éléments
sur lesquels on va travailler dans un futur proche, et simplement les
prioriser. De petits éléments valorisés et priorisés suffisent. Découper
c'est estimer de façon déguisée. Sans chiffre. Pour toute la validité de
cette démarche, ou plutôt de l'invalidité des estimations, je vous
renvoie à [ces slides de consolidation ici](/2017/05/agile-cfo-noestimates-beyond-budgeting/) sur les
mouvements \#noestimate et \#beyondbudgeting.

La maturité d'une organisation sur ce sujet varie souvent. Et nos
conseils, nos suggestions, ne sont valables que si ils prennent cette
évolution, cette temporalité en compte. Je ne vais pas souvent
conseiller à une organisation de passer directement du
[jour/homme](/2012/03/la-malediction-du-jour-homme/) à \#noestimate. Je
vais probablement plus souvent lui suggérer d'essayer sans
[jour/homme](/2012/03/la-malediction-du-jour-homme/), puis sans chiffre,
puis avec des phases de cotation visuelle plus rapide que les planning
poker, pour enfin peut-être évoquer le \#noestimate. Et j'espère être
pris de vitesse par l'organisation sur ces propositions, et juste
l'accompagner à la mise en œuvre. Il y a peu de conseils définitifs, il
y a peu d'agile qui s'impose, il y a un contexte, une temporalité dans
nos accompagnements, dans nos propositions.

Bref, pour cette organisation j'ai besoin de clarifier la pratique de
l'estimation visuelle (beaucoup la connaissent sous le terme de *extreme
quotation*, dont je crois que je change quelques détails, mais le
diable…). Initialement je découvre une première ébauche dans un vieil
article de Ken Power, sur un poker silencieux mural (que [je viens de retrouver ici](https://systemagility.com/2011/05/22/using-silent-grouping-to-size-user-stories/)).

L'idée essentielle qu'il faut saisir est que nous sommes mauvais en
estimation, et bons en comparaison. Et donc qu'il ne sert à rien de
perdre trop de temps sur quelque chose pour lequel nous ne sommes pas
doués. Ayez autant de conversations que nécessaire sur vos travaux grâce
aux estimations tant que les estimations ne deviennent pas le sujet de
vos conversations. Mauvais en estimation ? Faut-il encore le répéter ?
Voyez les [références de Duarte Vasco](/2017/05/agile-cfo-noestimates-beyond-budgeting/). Plus
ludiquement allez voir ce TED de Clio Cresswell sur [les mathématiques et le sexe](https://www.youtube.com/watch?v=H2vN2QXZGnc) . Ce TED est absolument hilarant et passionnant, on y apprend si votre couple va
durer plus de six ans et plein d'autres choses mais surtout au-delà de
l'estimation elle-même que notre stratégie de comptage est souvent
mauvaise. Si vous énumérez vos partenaires (David, Dragos, Pablo,
etc)*[1]* vous allez sous-estimer votre estimation. Si vous donnez une
approximation ("Alors 5 par an pour les 5 dernières années", etc.) vous
surestimez. Mauvaise estimation, mauvaise agrégation (et donc
probablement un mauvais engagement, une mauvaise projection). Autant se
tourner vers quelque chose de plus efficace et de plus fiable : la
comparaison.

## Comment mener une cotation visuelle ?

-   Vous découpez vos besoins, vos activités, sur des morceaux de papier
    (oui genre post-it).
-   Une présentation globale et/ou précise peut être réalisée (mais
    l'idée c'est de gagner du temps donc plutôt d'entrer dans les
    détails lors d'une étape ultérieure)
-   Vous demandez aux gens de ne pas parler dans un premier temps.
    L'idée est de parler, de converser autant que nécessaire ensuite sur
    les 20% qui le nécessitent et donc de gagner du temps sur les 80%
    des autres.
-   En silence chacun s'empare d'un morceau de papier / post-it
    décrivant un besoin, une activité, etc, et le place au mur en
    respectant la règle : moins cela demande d'effort plus on doit le
    placer à gauche sur le mur, plus cela demande d'effort plus on doit
    le placer à droite sur le mur. Surtout aucune échelle n'est placée
    au mur, c'est vraiment l'écart physique qui fait l'analogie avec
    l'écart dans l'effort. **J'insiste sur le fait qu'il n'y ai aucun
    chiffre, aucune échelle proposée par avance** contrairement à toutes
    les autres façons de faire que j'ai pu observé.

{{< image src="/images/2017/09/mur-cotation-1.jpg" title="Cotation visuelle 1" >}}

L'effort ? Un ensemble de complexité (jus de cerveau), de besogne
(répétition du geste, multiplicité des variations), et d'obscurité (on
n'a pas tous les éléments en main). Mais l'effort reste *in fine* du
temps (temps de réalisation), que l'on ne détaillera jamais : quand le
temps entre en jeu de façon chiffrée tous nos calculs sont encore plus
erratiques (car la référence devient notre calendrier, le calendrier du
produit, autre chose, mais plus l'activité à réaliser).

-   Dans le silence les post-its / morceaux de papier sont collés au
    mur. Ils peuvent être déplacés, par n'importe qui. Et re-déplacés.
    Et re-re-déplacés. Par n'importe qui. Par d'autres. Par soi. C'est
    ainsi que l'on repère les éléments qui demanderons une conversation
    plus aboutie.
-   Avoir une conversation plus aboutie sur les éléments qui le
    nécessitent (Pareto 80/20) : ceux sur lesquels une convergence n'a
    pas eu lieu, et / ou ceux qui sont très à droite si l'effort estimé
    est dû à une absence d'information, à trop d'incertitudes, et qu'ils
    sont prioritaires.
-   Enfin on va chercher des activités réalisées précédemment (lors de
    la précédente période, deux ou trois représentatives) et on demande
    aux gens de les placer sur le mur. Soudain votre échelle apparaît
    (qu'il s'agisse de chiffres ou autre), et alors là vous y allez "à
    la hache" pour montrer qu'une estimation ne peut pas et ne doit pas
    être précise car par définition c'est une estimation, une hypothèse.
    C'est votre attitude qui passe le message, qui démystifie certaines
    croyances. Vous agrégez les post-its / morceaux de papier autour de
    l'échelle qui soudain apparaît.

{{< image src="/images/2017/09/mur-cotation-2.jpg" title="Cotation visuelle 2" >}}

{{< image src="/images/2017/09/mur-cotation-3.jpg" title="Cotation visuelle 3" >}}

## Conclusion

Dans cet exercice chacun a pu visuellement comparer des éléments : c'est
à peu près le même effort, c'est plus, beaucoup plus, moins, beaucoup
moins, etc. Plus on a d'éléments plus la comparaison prend de
l'épaisseur. On a complètement évacué une quelconque échelle autre que
physique (du mur) et ainsi on a énormément réduit les conversations sur
les estimations pour les concentrer sur les histoires, et uniquement
celles qui n'avaient pas de consensus ou trop d'incertitudes. On
applique une échelle après que les estimations soient achevées et cette
échelle provient d'une cadence observée, pas d'une autre estimation. On
a été rapide : 80 éléments en 20 minutes ? Mais on a perdu les
conversations qui sont pourtant essentielles : on peut les retrouver à
la fin de l'exercice sur les 20% qui les nécessitent. On peut les
retrouver plus tard quand il s'agira de vraiment se projeter sur le
travail à faire, juste avant de s'y engager. On peut les retrouver au
fil de l'eau. Tout dépend, là encore, du contexte.

*[1] Ces prénoms ne sont pas réels. Les vrais prénoms ont été cachés.*
