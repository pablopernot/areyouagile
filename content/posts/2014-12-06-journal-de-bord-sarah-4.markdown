---
date: 2014-12-06T00:00:00Z
slug: sarah-journal-de-bord-jour-5
title: Sarah, journal de bord, jour 5
---

Voici la suite du journal de bord de Sarah, ma fille, qui en troisième
effectue son stage d'observation. Sinon [les deux premiers
jours](/2014/12/sarah-journal-de-bord-jours-1-et-2/), [le troisième
jour](/2014/12/sarah-journal-de-bord-jour-3/) et [le quatrième
jour](/2014/12/sarah-journal-de-bord-jour-4/).


{{< image src="/images/2014/12/final.jpg" title="A la gare" >}}

## Suite de la formation du quatrième jour

On se lève, on prépare les valises et on part en métro à Neuilly.
Arrivés là-bas mon père se met sur l'ordi et travaille. Il travaille
tous les matins avant les formations. Aujourd'hui il va travailler avec
les mêmes personnes que hier et va approfondir certaines choses.

Il leur a dit le cycle de Scrum. Il leur explique comment se passe les
sprints et la composition des équipes : les différents rôles. Je crois
que les mots qu'il dit le plus souvent sont "pourquoi" et "comment".
Dans l'agilité ce sont des questions très importantes.

Je me rend compte que c'est un travail très fatiguant. On se déplace
très souvent (train et métro) pour aller former des personnes dans
différentes entreprises. Contrairement à ce que l'on pourrait croire il
ne reste pas toujours derrière son ordinateur.

Dans ce métier il faut être à l'aise à l'oral, savoir s'exprimer devant
des gens. C'est intéressant de voir comment il est proche de ses
clients, plus les temps/journées passent plus les employés de "G" sont
détendus, ils participent plus. Mon père les tutoie pour qu'une relation
se créée, pour les mettre à l'aise. Il sont calmes, ils l'écoutent
attentivement : ils sont intéressés.

Je pense que les petits ateliers leurs plaisent beaucoup.

Après la pause repas, mon père a fait un autre atelier : faire un même
dessin avec 2 cahiers des charges différents.

Le soir on part à la gare de Lyon, prendre le train et rentrer chez nous
dans le sud.

*Note du père : je me suis régalé d'emmener ma fille "dans les
tranchées". Elle aussi manifestement, et pas seulement grâce aux supers
restos et sorties du soir. Fin de la saison 1.*

## Les liens

-   [les deux premiers
    jours](/2014/12/sarah-journal-de-bord-jours-1-et-2/)
-   [le troisième jour](/2014/12/sarah-journal-de-bord-jour-3/)
-   [le quatrième jour](/2014/12/sarah-journal-de-bord-jour-4/)
-   [le dernier jour](/2014/12/sarah-journal-de-bord-jour-5/)
