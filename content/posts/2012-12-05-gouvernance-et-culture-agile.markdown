---
date: 2012-12-05T00:00:00Z
slug: gouvernance-et-culture-agile
title: Gouvernance et culture agile
---

Souvent il est nécessaire de bien évoquer la différence entre "faire de
l'agile" : appliquer certaines pratiques, et "être agile" : avoir
intégré la culture, l'état d'esprit agile. Et souvent il est nécessaire
de rappeler que les pratiques n'ont pas de sens décorellées de la
culture qu'elles sont censées colporter.

En fait pour une organisation il est **absolument** nécessaire de
comprendre cette culture et de vouloir l'intégrer car les pratiques
proposées par Scrum, Extreme Programming, Kanban ou autre se limitent
bien souvent qu'à des aspects opérationnels assez réducteurs pour une
compagnie. Naturellement vous allez pouvoir, vouloir, reproduire des
pratiques projets ou produits comme la rétrospective à un ensemble plus
grand, plus englobant : par exemple des rétrospectives de département,
d'organisation, avec plusieurs dizaines de personnes, tous les 3 ou 6
mois ; ou vous allez pouvoir,vouloir travailler sur la vision,
l'expression du besoin sur un plan assez macro dans l'organisation, etc.
C'est possible et même recommandé.

Mais plus on se rapproche de l'organisation dans son ensemble (versus le
projet, le produit) moins les pratiques sont claires, plus l'importance
de la culture est évidente.

## Gouvernance

Or ne nous méprenons pas, pour ce qui concerne mon vécu, c'est bien
d'abord et avant toute chose face à des problématiques de gouvernance
que nous sommes confrontés : donc peu importe agile ou pas, peu importe
si c'est du *vrai* agile, ou un agile d'*opérette*, ou un agile
d'*apprentis sorciers*, ou autre chose, et XP plutôt que du Scrum, du
Kanban, ou du Lean, du CMMi ou du PMI, on s'en fout !

Encore une fois : les problèmatiques auxquelles je suis confrontées sont
des problèmatiques de gouvernance. C'est à dire que les compagnies que
je croise ne savent pas clairement vers où elles se dirigent, pourquoi
prennent-elles ces directions, qu'est ce qui est important pour elles,
quels sont les enjeux auxquels elles sont confrontées, comment les
clarifier, comment y répondre, comment prioriser, et encore une fois :
pourquoi. Cela peut se décliner : tout le monde n'a pas la même vision,
tout le monde ne place pas le même sens derrière les mêmes mots. Là si
mes clients, prospects ou contacts me lisent ils pensent : "il est
gonflé d'évoquer notre situation à mots couverts". Si cela peut vous
rassurer : j'évoque ainsi **beaucoup** de mes clients, prospects,
contacts.

Récemment encore à la fin d'un séminaire on m'interroge : "mais
finalement nous avons surtout parlé organisation, sens, vision, enjeux,
et peu d'agile" (sous entendu : sprint planning, tdd, gestion du flux,
estimations, planification, product owner, etc.) Détrompez vous ! C'est
bien là l'essence même de la culture agile : cette quête du sens
("pourquoi ?"). Tout le reste n'est que le plan d'exécution : les
pratiques. Et cette exécution ne vient nécessairement que a posteriori.

## Sens, enjeux, cohérence

Je pourrais pousser le bouchon plus loin et dire que même la question de
la culture agile ne vient qu'après : trouvez d'abord les enjeux,le sens,
le pourquoi de votre organisation. Ensuite la gouvernance c'est la
cohérence que l'on organise autour de ce sens. A vous ensuite de décider
si cette gouvernance doit être agile ou non.

Là cependant où l'agile tire son épingle du jeu c'est que cette
recherche du sens fait partie de sa culture. La culture agile qui hérite
de la pensée Lean d'après-guerre (Deming, Ohno) se fonde sur le respect
des personnes\*, l'amélioration continue, la quête de valeur et de sens
(pourquoi). L'amélioration continue sous-entendant donc une quête
**continuelle** et une remise en question **continuelle** (c'est le
point ardu de l'agile).

Quand donc les organisations réalisent qu'avant toutes choses elles
doivent s'interroger sur leur quête de sens et de valeur elles sont au
coeur de la culture agile ( et acquérir la culture agile est le meilleur
moyen d'y répondre ) .

## L'essence de la culture agile c'est le sens

Quand on parle de gouvernance, ce que j'associe donc à l'application
d'une cohérence opérationnelle relative à la vision de l'organisation,
on évoque à mes yeux l'application d'une culture agile : nous avons une
vision, soyons transparent sur son application, ayons le courage de son
application, communiquons auprès de tous à son sujet, inspectons les
résultats de cette application (feedback), adaptons nous pour suivre
cette vision, focalisons nous sur cette vision sans s'égarer, restons
simple dans cet objectif, etc. et au delà les pratiques
"opérationnelles" de l'agile qui ainsi "mettent en oeuvre".(C'est pour
cela que je suis aux antipodes d'associer obligatoirement agile et code.
Cela a pu être le cas historiquement, ceux sont avant tout des codeurs
qui rebondissent sur la pensée Lean, mais aujourd'hui nous sommes au
délà, les pratiques agiles de code ne sont qu'une émanation parmi
d'autres de pratiques relatives à une culture plus large, au delà du
"software").

D'où aussi parfois, souvent, les difficultés de "bottom-up" de l'agile :
les équipes mettent en oeuvre une culture agile mais celles-ci se
heurtent rapidement à la culture de la compagnie, elles mettent en
évidence les incohérences de la vision et de la gouvernance de
l'organisation.

Il n'y a pas de chemin clair vers la culture agile. Parfois c'est
l'application des pratiques qui font émerger le sens. Parfois c'est un
choc psychologique, culturel qui convinct, une révélation, parfois c'est
une lente maturation ; le hasard, ou l'élimination naturelle à la
Darwin/Lamarck : ne survivront que les organisations qui font sens.

*\* Je rappelle que vous pouvez avoir une lecture philanthropique de
cette sentence, comme cynique (responsabilisons les gens, respectons
les, c'est bon pour la performance, c'est bon pour le business).*
