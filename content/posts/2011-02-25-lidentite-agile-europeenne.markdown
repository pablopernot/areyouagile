---
date: 2011-02-25T00:00:00Z
slug: lidentite-agile-europeenne
title: L'identité agile européenne
---

Très récemment, Jurgen Appelo ([Noop.nl](http://www.noop.nl/)) a lancé
une réflexion sur un positionnement européen de l'agilité au travers
d'une discussion sur
[LinkedIn](http://www.linkedin.com/groups/purpose-European-network-Agile-Lean-3786271.S.43593821).

{{< image src="/images/2011/02/agile_europe-300x211.jpg" title="Europe" >}}


Ses conclusions et la synthèse des commentaires a pris la forme d'un
"post" sur son blog : [Agile Europe, entrons en
action](http://www.noop.nl/2011/02/agile-lean-europe-taking-action.html).
Je me propose de le traduire ci-dessous et je vous suggère à tous
agilistes de France et de Navarre de prendre contact avec Jurgen. En
français ou en anglais, car c'est la première chose que je me suis
permis de remonter : le problème de la langue, particulièrement dans
notre pays. Je suis sûr que nous avons plein d'agilistes de talents qui
ne comprennent rien à la langue de Shakespeare. Cela serait dommage de
perdre leur apport. A priori Jurgen Appelo a déjà bien conscience de
cette difficulté, inhérente à notre condition européenne.\*

*Une dernière réflexion avant de traduire son appel. Le débat sur
l'identité agile européenne me parait très fécond. En effet, pour ma
part le côté "Can Do" assez anglosaxon se heurte régulièrement au "Je
pense donc je suis" de notre sensibilité français (je pense avant de
faire). Il me paraîtrait très intéressant de philosopher un peu là
dessus car l'agile est une culture avant toute chose. (ps : l'improbable
drapeau à gauche provient de mon esprit retors).*

## [Jurgen Appelo, Agile Lean Europe, Entrons en action](http://www.noop.nl/2011/02/agile-lean-europe-taking-action.html).

Dans mon précédent message, j'ai analysé la [discussion au sujet d'un
réseau de penseurs agile & lean en europe (et
d'acteurs)](http://www.linkedin.com/groups/purpose-European-network-Agile-Lean-3786271.S.43593821?qid=febf2c7c-8d87-452c-9577-fd05149b9288&goback=.gmp_3786271).
Ma conclusion a été que beaucoup d'entre nous souhaitent **une meilleure
interaction et collaboration au travers de l'Europe**. Mais nous
chérissons aussi les idées de **diversité et d'émergence**.

Nous avons besoin de gens pour pousser les choses plus loin. Mais sans
créer une hiérarchie ni une volonté de contrôle.

J'ai discuté longuement avec  [Olaf Lewitz](http://hhgttg.de/blog/)
et [Ken Power](http://systemagility.com/blog/), et voici **mon plan
d'action...**

## Etape 1 : Créer la grosse liste

La première étape vers une meilleure collaboration en Europe est de
connaître et de **visualiser le réseau**. J'appelle cela la **grosse
liste**. Cela sera une liste de tous les gens en Europe qui sont *a)
représentant d'une communauté, d'un groupe b) ont des idées c) vivent en
Europe*. Cette liste contiendra des auteurs, des bloggueurs, des
orateurs, des organisateurs de conférences, et tous ceux qui sont
impliqués dans le partage d'**idées à propos de Lean et de l'Agile**.

La première étape, Olaf et moi avons comparés et fusionnés nos listes de
contacts. Je vais contacter beaucoup de gens dans beaucoup de pays ce
week-end, et **je vais vous demander de me donner plus de noms**. La
semaine prochaine je vais publier une première version de cette grosse
liste, et je vais demander à tous *de me donner leur avis dessus*. Je
vais probablement oublier des gens importants et je suis prêt à prendre
des coups pour cela (personne de mieux qu'un hollandais pour cela je
suppose). Nous aurons besoin de quelques itérations pour avoir une idée
assez précise des gens qui cultivent des idées en Europe ([idea
farmers](http://www.noop.nl/2010/11/the-idea-farmer.html)).

La grosse liste devrait aider les organisateurs de conférences, les
orateurs, et les auteurs **à entrer en contact au travers de l'Europe**.
Cela sera aussi utile pour les nouvelles initiatives qui auront besoin
de contributeurs de différents pays.

*Exemple : Quand la grosse liste sera presque complète, je demanderai
aux gens de proposer un meilleur nom pour ce futur réseau ( ALE est
toujours une option). J'aimerais aussi voir une rencontre ou une
conférence en 2012 organisée avec le maximum de gens de cette liste.*

## Etape 2: Créer la petite liste

Pour certaines activités nous aurons besoin d'une petite liste des
meilleurs penseurs d'Europe. Ils pourront s'appeler les "leaders
d'opinions" ou "gourous", mais ce n'est pas important. Ce qui est
important c'est que **le réseau qui décidera qui ils sont**. Cela sera
une méritocratie
([meritocracy](http://en.wikipedia.org/wiki/Meritocracy)), au lieu d'une
sélection de gens qui se seront eux-mêmes choisis. (J'ai une expérience
concernant la création de ces [”best-of”
lists](http://www.noop.nl/lists.html), donc pas de souci).

Cette courte liste devra être composée de quelques douzaines de
personnes (disons une c﻿inquantaine) éclairées qui sont les meilleurs en
Europe pour **produire des idées et créer des dynamiques entre les
gens**. J'aime le nom de "petite liste" car cela ne sonne pas trop
ronflant (comme la grosse liste \[note de ma part : je préfère "grosse
liste" que "grande liste"\]). Et c'est ainsi que cela devrait
fonctionner dans un monde agile. Le réseau est plus important que
l'élite.

*Exemple : la petite liste sera très utile pour les éditeurs, les
organisateurs de conférences, les instituts européens, et tous ceux qui
veulent savoir qui sont des leaders d'opinions et les principaux acteurs
au niveau européen. Les gens de la petite list peut servir de point
d'entrée pour le réseau social (agile).*

## Etape 3: Se réunir

Une communication en tête à tête est la meilleure façon de faire avancer
les choses. Donc **je vais doucement pousser les gens de ce réseau à se
rencontrer et à entrer en action**. Une chose que je souhaite réaliser
serait d'avoir 20 penseurs (\[idea farmers, note de ma part :
"intellectuel" on dirait peut-être en France\]) à la conférence XP 2011
([XP2011 conference](http://www.xp2011.org/)) à Madrid. Je vais
peut-être demander aux gens de la petite liste de nommer des
représentants de leur groupe. On pourrait appeler ce sous-ensemble la
"très petite liste" (Elle serait encore moins importante que la petite
liste).

A la conférence  XP2011 je demanderai à cette "très petite liste"
d'avoir une meilleure vision pour ce réseau, des idées supplémentaires,
et comment transformer ces idées en actions. Mais c'est trop tôt pour
entrer dans les détails. Comme Olaf me l'a montré à la conférence
 [Play4Agile](http://p4a11.pbworks.com/w/page/29184741/Home), nous
devons traiter ces détails "*au dernier moment responsable*".

## Votre aide

Naturellement, **j'ai besoin de votre aide**.

Je ne fais pas faire tout le boulot. Mon boulot c'est de booster le
réseau, de laisser les gens s'organiser, de poser quelques contraintes,
et de laisser les choses grandir, pousser (j'observe comme le
jardinier).

Je ne veux pas être Directeur, ni Président ou CxO, parce que nous
n'allons pas lancer une nouvelle alliance, un nouveau consortium ou un
nouvel institut. Nous en avons déjà plein. **Nous allons juste faire
bourgeonner notre réseau**. Je voudrais cependant être le point d'entrée
unique du réseau (Singleton) pendant un moment. Je suis disponible
pour attraper, créer, rediriger, orienter la matière du réseau.

**Voici comment vous pouvez m'aider maintenant :**

1.  Rendez nous visible (blog, tweet, discussions, etc).
2.  Inscrivez vous à [Agile Lean Europe group on
    LinkedIn](http://www.linkedin.com/groups/Agile-Lean-Europe-ALE-3786271)
    et restez informés.
3.  Donnez moi les noms des gens Agile et Lean people en Albania,
    Austria, Croatia, Cyprus, Estonia, Greece, Hungary, Latvia,
    Lithuania, Malta, Russia, Slovakia, Slovenia, ou Turkey. Parce que
    je ne connais personne dans ces pays. (Vous aurez un bonus si vous
    nommez des agilistes du Vatican)

Merci pour votre soutien !
