---
date: 2023-07-11
slug: narration-imaginaire
title: "Narration, imaginaire, futur et adaptation"
draft: false
---

Il y a l’atténuation et l'adaptation. L’atténuation c'est comment atténuer l'impact de nos usages, de nos façons de vivre sur notre existence au travers la remise en cause de l'équilibre planétaire. Mais c'est déjà dans la tête de beaucoup un scénario du passé. L'adaptation c'est comment s'adapter au nouveau monde qui arrive. Comment ne pas en prendre *trop plein la gueule* en étant prêt. Dans la tête bien sûr, mais aussi en ayant fait émerger plein de nouvelles façons de faire qui vont nous faire du bien quand l'impact nous fauchera (violemment soudainement, ou insidieusement au fil des ans, je ne sais pas). Mais on est déjà dans l'après. Et quoi de mieux pour préparer l'après que de l'imaginer. Les imaginaires et les narrations sont au cœur de beaucoup de conversations ces temps-ci. 

Lundi 4 juillet avec [Frugarilla](https://frugarilla.fr) et le pétillant [Tristan Nitot](https://www.linkedin.com/in/nitot/), entouré de nos deux "partenaires" [Climax](https://www.climaxnewsletter.fr/) et [Choses communes](https://www.chosescommunes.fr/), un débat passionnant a eu lieu sur les imaginaires utopies (surtout) / dystopies (un peu). Pourquoi il est plus difficile d'écrire une utopie qu'une dystopie par exemple ? Quelques réponses abordées : une dystopie post-apocalyptique avec quinze survivants c'est plus facile à décrire que d'imaginer la gestion d'une civilisation positive de deux millions, moins de cliffhanger dont nous sommes si friands, etc. On a parlé Solar Punk, de Malevil de Robert Merle, et de plein de choses que j'aime. Je me suis interrogé sur l'intérêt du jeu de rôle (vous savez, Donjons & Dragons, Cthulhu, Cyberpunk, etc.) dans cette réflexion, car on parle beaucoup littérature, mais on évacue trop vite cette forme d'imaginaire, car elle est peu présente ici comparé aux US par exemple. 

L'ami Tristan aura minutieusement collecté les ouvrages qui ont été cités [dans son article](https://www.standblog.org/blog/post/2023/07/11/Utopies-futurs-desirables-virage-climatique), ainsi que la liste des intervenants. 

{{< image src="/images/2023/07/apocalypse-pas-now.jpg" title="Soirée UTOPIES avec Frugarilla, Climax et Choses communes" >}}

Tout est narration, tout est récit. On le savait. Mais c'est dans la bouche de tout le monde depuis quelque temps. Sur ce sujet je me suis retrouvé dans la foulée au [SICT 23](https://www.sictdoctoralschool.com/), une école de doctorants sur les enjeux climatiques au regard du numérique. Le sujet a été le même lors de mon passage : les imaginaires (mais je n'ai assisté qu'à une journée de la conférence) (j'ai aussi vu des choses sur l'art et son rapport au numérique soutenable). Voici les diapositives que j'avais préparées pour présenter [Frugarilla](https://frugarilla.fr), et parler un peu d'imaginaire lors de la table ronde à ce sujet. **Ces diapositives n'engagent que moi**.     

## [>> SICT 2023 diapositives jour 1 : cliquez pour accéder au PDF](/pdf/2023-SICT-D1.pdf){:target="_blank"}

### diapositive 2

Le collectif, dans l'ordre, Dominique Buinier la connectrice, Alexis Nicolas et Sara Boucherot, les sachants, moi-même l'entrepreneur, Tristan Nitot l'influenceur, et chat qui râle pour équilibrer la page. 

### diapositive 3 

Frugarilla, un petit groupe de 5 dans une grande structure, elle-même dans une très grande structure. Notre positionnement en son sein sur une certaine radicalité (numériques essentiels 2030).

### diapositive 4 

La raison d'être d'OCTO

### diapositive 5 

Nos actions, principalement d'influence. 

### diapositives 6 à 9 

Une approche d'influence basée sur le rappel des risques. Kotter tout ça...
Très certainement inspiré en partie par nos conversations avec Gauthier Roussilhe. 

### diapositive 10 

Nos propres contradictions internes

## [>> SICT 2023 diapositives jour 2 : cliquez pour accéder au PDF](/pdf/2023-SICT-D2.pdf)

### diapositive 2 

Le rappel des scénarios de l'ADEME qui nous serve d'outils de conversation : https ://www.ademe.fr/en/futures-in-transition/scenarios/ (en anglais, car pas mal de doctorants anglais, mais vous trouverez facilement la française). Le positionnement de Frugarilla est clairement indiqué. 

### diapositive 3 

Les formes du **frugal** que nous défendons. 

### diapositive 4 

Des imaginaires c'est utile pour se projeter, réaliser. Mais nous ne croyons pas aux solutions d'ores et déjà toutes ficelées pour aujourd'hui. Comme cette diapositive sur la guerre en Afghanistan, le système est trop complexe. Il faut fonctionner par principes, par motifs, par émergence. 

### diapositive 5 

Aux chercheurs qui nous demandent sur quoi chercher : bâtissons un ensemble de composants qui nous permettront de faire émerger des solutions, ne cherchons pas directement la solution. 



Merci ! 
