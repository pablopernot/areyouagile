﻿---
date: 2018-07-18
slug: arretons-avec-la-tyrannie-de-la-vision
title: "Arrêtons avec la tyrannie de la vision"
---

Sous couvert de ce besoin fondamental qui est de donner du sens, besoin très légitime, j'observe une tyrannie de la vision. Dites-moi "pourquoi" sinon je ne m'engage pas. La requête est compréhensible tant qu'elle ne devient pas intolérante.   

L'intolérance serait de conditionner l'engagement à une vision précise, à une réponse détaillée.

Or la vision est une direction pas une réponse. Et la vision doit être claire, mais précise j'en suis beaucoup moins sûr. Ne pas avoir de vision est une vision, mais il faut le signaler en tant que tel, et en le signalant on indiquera aussi probablement comment on espère se sortir de l'ornière, ce qui deviendra une vision. 

Si la vision est précise, elle est devenue un grand plan en amont. En contradiction avec notre perception et compréhension du monde qui fait la part belle à la systémique, au chaos, et à l'émergence.  

Une bonne exécution est potentiellement une bonne vision. Pas la peine de grandiloquence, de sauver le monde tous les matins. Pour les acteurs dominants du marché, une amélioration incrémentale (et pas expérimentale / disruptive) peut largement suffire. Juste ici le piège est de ne pas mesurer la bonne exécution, car elle se déroule bien, mais par les résultats (valeur) qu'elle produit. 

Systémique, chaos et émergence. La vision naturellement peut aussi être émergente. Elle le sera bien souvent. Elle changera, elle s'adaptera, elle évoluera. 


Une vision devrait être 

* une direction (porter du sens)
* assez simple et claire pour se l'approprier
* désirée et donc vécue, incarnée. 
* elle peut émerger, changer, s'adapter, évoluer

Une vision n'a pas à être 

* une esclavagiste
* détaillée
* nécessairement grandiloquente (pas la peine de sauver le monde tous les matins). 
* définitive

