---
date: 2014-06-06T00:00:00Z
slug: de-lidee-au-plan-de-livraison
title: De l'idée au plan de livraison
---

Je vous propose de suivre un cheminement classique d'expression du
besoin jusqu'au plan produit. Sachant que tout cela n'est qu'un exemple
factice basé sur [Peetic](/2012/11/peetic/), et qu'en aucun cas, un plan
produit n'est autre chose qu'une estimation. C'est un rapide
récapitulatif de ce que j'utilise, et je ne creuse pas chacune des
notions, je vous invite à fouiller sur le net vous y trouverez votre
bonheur.

## L'idée, la cible, la proposition

Partons d'abord de l'idée : vous avez une idée, un besoin. Si nous
reprenons [Peetic](/2012/11/peetic/) (sur lequel nous allons - avec
[Claude Aubry](http://www.aubryconseil.com/) - relancer nos efforts,
cela sera l'objet du prochain article), disons qu'il parait évident
après sortie du site, et après avoir atteint notre premier millier
d'inscrits (comme prévu !), de faire désormais progresser les ventes en
ligne. C'est notre cible, notre idée, notre proposition.

## La fiche idée ou proposition

Faisons donc une fiche idée ou proposition. Je suggère qu'elle soit très
synthétique afin d'être très claire. Choisissez bien vos mots. La
communication est clef, comme toujours. Elle se compose des *personas*,
des critères de succès (disons que les limiter à trois et les prioriser
est un plus), et surtout des mesures à réaliser pour valider ou non le
succès. C'est essentiel de contre-balancer les critères de succès avec
la question : comment on mesure ?

On fait apparaître dans cette fiche les *personas* de façon succinct ou
non. Ils peuvent être approfondis plus tard.

Dans mon scénario on dira que les deux premiers critères de succès ont
été atteint, et que nous allons nous focaliser sur le troisième : 10 000
euros d'achat par mois (ou 1000 achats par mois). C'est bien de la
mesure que l'on part pour déclencher l'[impact
mapping](http://www.impactmapping.org), ou la carte d'impacts.

{{< image src="/images/2014/06/step1-idee.jpg" title="Idée" >}}

## La carte d'impacts

Au travers de 4 étapes elle définit des scénarios comportementaux. 4
étapes : quoi, qui, impact, mise en oeuvre, c'est à dire : a) que
voulons nous, b) qui devons nous impacter pour atteindre cet objectif,
c) comment cette personne doit elle être impactée pour atteindre cet
objectif, d) comment mettons nous en oeuvre cet impact. Le quatrième
point (qui peut se découper en plusieurs étapes) constitue les *user
stories* de notre *backlog*.

## Les personas

C'est potentiellement à ce moment que l'on aura un peu plus creusé les
personas.

{{< image src="/images/2014/06/jessica.jpg" title="Jessica" >}}

## Le chemin de valeur

Pour chaque étape et chaque branche de la carte d'impact on prendra soin
de s'interroger sur la valeur, et quelles sont les branches les plus
intéressantes. Quitte à ne pas travailler sur toutes les branches.
(Potentiellement on pourrait définir la valeur en demandant aux
participants un "buy a feature").

{{< image src="/images/2014/06/step2-impact.jpg" title="Impact" >}}

## Le backlog

En sélectionnant les branches de valeurs et en les priorisant on obtient
finalement les *user stories* qui composent le backlog.

{{< image src="/images/2014/06/step3-backlog.jpg" title="Backlog" >}}

## Le plan de livraison (Release plan)

On va **estimer** les livraisons qui font sens, les produits minimums
proposant assez de valeur pour être délivrer, au travers de ce backlog.
C'est bien un plan, il n'est pas définitif, il pourra,pourrait changer.

## Le plan produit

Si le produit est constitué par plusieurs équipes / projets on peut les
consolider au travers d'un seul plan produit. Afin de visualiser
clairement les rythmes, synchronisations, dépendances, etc. C'est bien
un plan, il n'est pas définitif, il pourra,pourrait changer.

{{< image src="/images/2014/06/allsteps.jpg" title="image" >}}

## Commentaires

Géraldine ([@ge\_legris](https://twitter.com/ge_legris)) me dit :

Merci Pablo pour cet article qui a le mérite d'organiser plusieurs
concepts pour en faire un cheminement complet. Ça me manquait ! De mon
côté je fais faire 2 fois le travail de priorisation au Po : je lui
demande de prioriser les branches de la carte d'impact, puis je lui fais
utiliser la methode moscow pour affiner encore plus la priorisation mais
cette fois ci au sein du backlog. C'est un peu fastidieux mais ça les
force à faire du tri en amont et en continu. T'en penses quoi ? Enfin,
les Pos rencontrent une vraie difficulté à définir le "comment". Je leur
dis qu'il s' agit des activités qui vont nous permettre d'atteindre les
objectifs. Mais on voit dans l'exemple de Peetic que ce n'est encore pas
vraiment ça...​
