---
date: 2014-01-02T00:00:00Z
slug: mini-livre-la-horde-agile
title: 'Mini livre : La Horde Agile'
---

Qu'est ce qui fait de l'homme, l'homme ? Observons nos ancêtres qui
parcouraient la Savane. Nous n'avions ni dents acérées, ni muscles
proéminents, encore moins une rugueuse carapace. Et pourtant nous avons
émergé et finalement dominé le monde. Pourquoi ? parce que nous avions
cette culture de l'adaptation en milieu complexe. Avec Descartes et les
Lumières nous avons un peu oublié cela. Nous avons une réponse
cartésienne à chaque question. Et nous pensons que le monde marche
ainsi. Mais ce n'est plus le cas : interdépendance, connectivité totale,
immédiateté, simultanéité, globalisation, le monde est de nouveau
imprédictible. Aujourd'hui que peut nous enseigner notre vieille horde ?


[La horde agile, le mini livre](/pdf/lahordeagile.pdf),
pdf, 42 pages, ~ 4mo

**nouveau** : [la horde agile, le mini livre (epub)](/epub/la-horde-agile.epub), epub, 42 pages, ~ 3.5mo

C'est en préparant les *keynotes* des *agile tours 2013* de Marseille,
Toulouse, Montpellier et Clermont-Ferrand, que je me suis mis à écrire
ce texte car c'est ainsi que je formalisais le mieux -- il me semble --
le fil de ma pensée. Du coup, en cette fin d'année j'ai pris le temps de
le compléter et avec l'aide de proches et d'amis, le voici.

Liens
-----

-   [La horde agile, le mini
    livre](/pdf/lahordeagile.pdf),
    pdf, 42 pages, \~4mo
-   Le [premier article sur la horde agile](/2013/02/la-horde/),
    février 2013.
-   Les [vidéos des conférences](/2013/12/la-horde-agile-les-videos/)
-   Les [slides](/2013/10/la-horde-agile-les-slides/) des conférences

