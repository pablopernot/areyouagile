---
date: 2011-05-10T00:00:00Z
slug: lagile-et-le-psychanalyste
title: L'agile et le psychanalyste
---

Une idée me trotte dans la tête depuis quelques semaines. J'assimile mon
rôle de coach agile à celui -en quelque sorte : ne me prenez pas au pied
de la lettre ! - d'un psychanalyste... Enfin de loin, disons
la silhouette du coach agile ressemble à celle du psychanalyste. Je n'ai
jamais été psychanalyste, et je ne le regrette pas du tout (argh une
confession), mais j'ai tout de même réalisé des études où la philosophie
et la psychologie ont pris une part intéressante. Bref je reviens au
fondement de ce petit article : j'opère un rapprochement entre le métier
du coach agile et le rôle de psychanalyste. Pourquoi ? Comment ?

D'abord il est nécessaire de payer son coach agile tout comme son
psychanalyste : c'est peut-être une imposture de Freud, comme le
sous-entend fortement Onfray, il n'empêche en tant que coach agile je
recommande fortement de faire intervenir un ou des externes (vous pouvez
considérez que je vend ma soupe, mais que voulez vous -à ce jour- c'est
mon avis). Ce coût procède d'un véritable engagement. quand quelqu'un
embauche un coach externe il acte de sa volonté de changement par les
moyens (coût) qu'il met en oeuvre.

A ce sujet je vous renvoie aussi à la malheureuse mais pourtant trop
souvent vérifiée maxime :  "nul n'est prophète en son pays" . Idem pour
le psychanalyste, il est impératif qu'il soit extérieur à votre monde.
Il est impensable (du moins au regard de ma médiocre connaissance  de la
psychanalyse) d'impliquer un psychanalyste proche de vous. Son analyse
est perturbée, orientée, votre restitution l'est tout autant.

Ensuite comme pour la psychanalyse, le rôle de coach agile est d'écouter
de comprendre et d'orienter votre client, mais la solution viendra quand
il saura exprimer lui même la réponse. Quand on bascule une société vers
l'agile on aborde les choses d'un façon pragmatique en offrant un
outillage, un socle auquel se rattacher (scrum, kanban, xp, lean ou
autre). Mais l'essentiel réside dans l'appropriation de la culture
agile. L'inspection, l'adaptation, la transparence pour scrum sont bien
plus importants que le mode opératoire. Mon but est atteint quand la
société possède la culture, l'esprit agile, et pas seulement quand elle
applique *stricto sensu* la méthode. Elle pourrait très bien se détacher
complètement de telle ou telle méthode (même si -insistons là dessus-ces
méthodes s'appuient sur de longues années d'expériences et donc ne sont
pas dénuées de fondement... ). Inutile pour moi de seriner mon client :
il faut faire ainsi,  il faut faire ainsi, il faut faire ainsi,
 j'aurais atteint mon but quand lui même me dira : il faut faire ainsi.
Mieux vaut donc le questionner et l'amener (si c'est vraiment la bonne
solution) à s'approprier la culture agile. Le psychanalyste ne fait pas
autrement...

Enfin un quatrième point me vient à l'esprit : l'agile fait émerger les
problèmes latents...pour les régler. N'est ce pas là tout l'objectif du
psychanalyste ?

Je saisis ces idées hier soir pour interroger une connaissance...
psychanalyste. Il m'indique qu'il est "à peu près" d'accord. Et soulève
un point intéressant : le mot "coach" est très marqué "objectif" et
réussite en quelque sorte. Mais bien souvent ses patients ne souhaitent
pas voir régler les problèmes me dit-il, ils ne souhaitent pas réussir.
Il y a un désir que les choses restent en souffrance. Il y a un plaisir
à cette souffrance en quelque sorte. Or, nous sommes bien des coachs
(même si je ne suis pas très à l'aise avec ce mot -je préfère d'ailleurs
"facilitateur", mais ce mot ne convient pas entièrement non plus-) et,
je l'espère, "mes patients" souhaitent voir régler leurs problèmes.

C'est grave docteur ?
