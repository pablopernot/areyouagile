---
date: 2015-08-14T00:00:00Z
slug: mouvement-agile
title: Mouvement agile
---

Comme souvent c'est une conversation, et un sujet qui se répète, qui me
pousse à consolider ce petit article dont le thème se veut la
présentation des mouvements, méthodes, approches agiles dont on entend
parler. C'est un article qui s'adresse plus au néophyte qu'à l'expert,
et qui n'a absolument pas velléité à être précis, mais plutôt à donner
un sens dans les grandes mailles à ces diverses appellations de
l'agilité. Car soyons clair, de tous ces mouvements, du *design
thinking* au *kanban*, c'est bien la même sensibilité, la même approche
derrière. Comme les coca-cola vous avez des saveurs mais la recette de
base est la même. Cette multiplicité des appellations est très
compréhensible. C'est un argument et un packaging marketing : on
s'adresse à telle ou telle population de différentes manières. C'est
aussi un argument pour la conduite du changement : à nouveau nom,
nouveau droit à l'essai, nouvel intérêt, nouvelle démarcation.

## Les mouvements agiles du mouvement Agile

Les dates proposées ne sont pas celles de la création du mouvement, mais
plutôt *grosso-modo* de son actualité, et encore, c'est assez flou.
L'objectif est d'avoir plus une esquisse impressionniste que figurative.
Je suis aussi très macro ou caricatural dans la description des
mouvements (les fanatiques XP vont aiguiser leurs couteaux), encore une
fois je ne cherche pas à être précis, mais à donner une vision
d'ensemble.


{{< image src="/images/2015/08/mouvementagile.jpg" title="Mouvements du mouvement agile" >}}

## Quelles sont donc ces saveurs et à qui s'adressent-elles ?

**Lean**

> Pour les méthodologistes et les industriels, oncle d'Agile quand le
> monde était encore compliqué (et pas complexe, où commençait à
> l'être). On aurait pu ajouter *Lean Software*, qui est le pendant IT
> du Lean. Attention avec l'appellation Lean : [les cowboys
> hippies](/2014/11/les-cowboys-hippies/).

**Agile**

> Historiquement pour l'IT et les DSI.
>
> **Scrum**, Faire des projets complexes en environnement complexe.
>
> **Kanban** Amélioration continue des flux de création de valeur (comme
> la maintenance, un cycle de vente, l'organisations d'évènements, des
> activités de RH, etc.).
>
> **eXtreme Programming** (XP) Des pratiques d'ingénieries et
> l'émancipation sociale du développeur.

**Devops**

> C'est *Extreme Programming* dont on pousse les pratiques jusqu'au
> déploiement continu en intégrant d'autres populations comme les
> infras.

**Software Craftmanship**

> On relook XP et on rappelle qu'il faut du temps et de la sueur, les
> compagnons du code.

**Lean Startup**

> Agile pour la gestion "produit", pour les métiers, les idées, les
> startups. Aurait du s'appeler *Agile Startup* mais : [les cowboys
> hippies](/2014/11/les-cowboys-hippies/)

**Design Thinking**

> Agile avec la mise en exergue de l'expérience utilisateur (UX): pour
> les designers, les créas, etc. Mais aussi les métiers, les gens du
> produit, très redondant ou complémentaire avec Lean Startup lui même
> très complémentaire de...

**Entreprise Libérée**

> Posture Agile pour les entrepreneurs et les managers, vision de
> l'entreprise nouvelle.

**Holocratie - Sociocratie**

> Méthodes pour équiper l'auto-organisation et le management Agile dans
> les entreprises libérées. Sociocratie était trop dur à porter comme
> nom, du coup c'est Holocratie ou Holacratie qui l'emporte.

## N'oubliez pas

La sauce de base est la même. Il s'agit de saveurs développées pour
adresser des populations souvent différentes qui sont intéressées par
des portes d'entrées différentes. Des cartes joker pour faciliter la
conduite du changement aussi. Au final, c'est la même approche, la même
philosophie, le même changement de paradigme. D'autant que dans une
compagnie il **ne** me paraîtrait **pas** absurde que **tous** ces
mouvements soient présents, au contraire. Même dans les petites
compagnies très rapidement plusieurs de ces mouvements sont nécessaires.

ps : je le redis, je sais bien que factuellement *Scrum* apparaît avant
*XP*, que le *Design Thinking* apparaît dans les 80, et la sociocratie
bien avant, etc. etc. Mais ils n'ont pas la substance dont on parle
maintenant. Ce n'est pas une encyclopédie, ce petit article, c'est un
tableau impressionniste, une silhouette.

## Feedback

### Sibylle

{{< image src="/images/2015/08/sybille.jpg" title="Sybille sleeping" >}}

Avec ce petit article j'ai fait une ravie : la mère de Sibylle, qui
m'écrit : "J'ai lu à Sibylle ton article du 14, très efficace ;-)" La
bienvenue à Sibylle dans ce monde complexe. Bravo aux parents.

### Ne dîtes pas les faits, dîtes la vérité

Certains remettent en question les dates. Ont-ils lu et compris mon
article ? Plus important, font-ils une lecture purement analytique des
choses ? Cela malheureusement ne suffit pas. Lecture blanche, avec un
seul axe, une seule perspective. Qu'est ce que l'on apprend en agile :
cela dépend du contexte. Certains devraient se le rappeler. A qui
s'adresse-t-on ? Dans quel cadre et dans quel but ? Est-ce que c'est la
première fois que le mot Scrum est prononcé qu'il compte ? La seconde ?
Ou est-ce le moment de sa vraie popularité ? Le mème a-t-il évolué ? La
méthode ? Encore une fois la méthode purement scientifique, dans le sens
abscons analytique, échoue, à mes yeux en tous cas.

Ca me fait penser à cette citation : "Ne dîtes pas les faits, dîtes la
vérité". ("Tell the truth and not the facts." [Maya
Angelou](https://en.wikipedia.org/wiki/Maya_Angelou)).

{{< image src="/images/2015/08/twitter.jpg" title="Twitter bullshit" >}}


### Sisyphe

(qui comme Sibylle plus haut, place le 'y' après le 'i').

{{< image src="/images/2015/08/twitter2.jpg" title="Twitter" >}}
