---
date: 2016-04-18T00:00:00Z
slug: 2-nouveaux-formats-de-retrospective
title: Deux nouveaux formats de rétrospective
---

Je ne sais pas trop, j'ai une sorte de frénésie d'écriture. Un nouveau
thème hier, un article hier, un autre aujourd'hui, probablement d'autres
à suivre dans la semaine. Une maladie mentale sans aucun doute. Enfin,
voici deux formats de rétrospective que nous allons essayer durant cette
semaine (que nous avons essayé, j'amende l'article).

## Le bestiaire des contes et légendes

Comme beaucoup j'ai fait des jeux de rôles (JRTM, Donjons & Dragons,
Légendes celtiques, etc.), même grandeur nature, j'ai lu J.R.R Tolkien,
George Martin ou Andrzej Sapkowski, mais aussi Astérix, ou Thorgal ou
Jon le Chninkel. Voici donc un cadre pour faire une rétrospective avec
le bestiaire de nos contes et légendes.

{{< image src="/images/2016/04/contes-et-legendes.jpg" title="rétrospective contes et légendes" >}}

**Le druide** : quels sont les ingrédients de la potion magique ?
Naturellement quels ont été les ingrédients qui ont amélioré les choses
durant le temps observé.

**L'elfe** : c'est bien beau mais c'est fragile, comment et qu'est ce
que l'on consolide, que l'on solidifie ?

**Le nain** : c'est un peu brut de décoffrage non ? Comment et qu'est ce
que l'on adoucit ? Que l'on affine ?

**Le lutin** : Il est passé subrepticement, l'avez vous vu ? Quels
détails se sont glissés ici et là que vous voudriez rendre plus visibles
? Qu'est ce que vous avez remarqué que personne n'a remarqué ?

Les **quêtes** réalisées durant le temps observé.

Les **exploits** réalisés durant le temps observé.

Les **trolls** jetés en pâtures durant le temps observé.

## Le Questions Crowdfunding

\[*modification le 22 avril suite au* [raid agile 5](http://raidagile.fr)\]

Le *Questions Crowdfunding* est le nouveau nom donné à la rétrospective
*Question Storming* suite au dernier raid où j'ai pu à nouveau la
tester.

*Questions Crowdfunding*/*question storming* est une adaptation d'un
outil du remarquable "Change your questions, change your life" de
Marilee G. Adams, que m'avait recommandé
[Valérie](https://www.linkedin.com/in/valeriewattelle) il y a de cela un
an. C'est un bouquin que j'ai adoré. Simple mais diablement efficace.
Depuis je vois la vie en *learner* et en *judger* (j'ai entrepris de
traduire le poster du *learner path / Choice map* pour
[Fabrice](http://www.fabrice-aimetti.fr/), ça viendra un de ces quat').

### Premier tour

Distribuez autant de pièce (billet, jetons, etc.) que de participants à
chacun. 7 jetons pour 7 participants. Normalement une équipe ne dépasse
pas 8 personnes, donc limitez vous à 8 sinon. Suis-je clair ? C'est une
monnaie d'achat pour les meilleures questions. Puis faîtes démarrer un
tour de table. Chacun doit poser une question à voix haute à tour de
rôle. La question NE doit PAS contenir de "tu, toi, vous", mais des "je,
nous".

Si la question semble pertinente on l'achète avec ses jetons (un seul
max par personne par achat). L'achat est validé si la somme des jetons
est supérieure ou égale au deux tiers du groupe. Disons donc que si vous
êtes 7 et que 5 personnes sur 7 achètent la question, elle est retenue,
on l'a pose dans un coin. Si la question n'est pas retenue chacun peut
reprendre son jeton, son billet. Dès que l'on atteint 3 ou 4 ou 5
questions retenues on passe au deuxième tour.

Les participants vont donc vouloir acheter : pas trop vite pour attendre
la bonne question, mais pas trop tard pour qu'il y ait encore assez
d'argent dans le jeu pour acheter une question. C'est très sympa
d'entendre les "j'achète !" dès qu'une question fait "tilt" dans la tête
(et d'essayer de convaincre les autres d'acheter aussi).

Il faut respecter le tour de table, et si un participant a un blanc, et
bien on attend. L'idée de *Questions Crowdfunding* est de rebondir
questions après questions, sans acrimonie (d'où les "je, nous"). Une
pyramide de questions. J'aime beaucoup cela.

### Deuxième tour

On affiche les questions sélectionnées. On demande à chacun de placer un
sticker, post-it, avec un simple mot, que lui a évoqué la question,
qu'il explique à haute voix aux autres (donc un post-it par question par
personne). Puis naturellement on fait ensuite un
[dotvoting](http://martinfowler.com/bliki/DotVoting.html) (chacun à là
aussi un pouvoir d'achat, disons 3 points) pour sélectionner le point
sur lequel on va essayer de s'améliorer ou un aspect positif à propager.

## Les autres articles sur les rétrospectives et les autres formats

Cet article fait écho à ces deux là :

-   Mars 2015 : [Festival de
    rétrospectives](/2015/03/festival-de-retrospectives/)
-   Mai 2013 : [L'art de la
    rétrospective](/2013/05/lart-de-la-retrospective/)

On a donc pu utiliser les rétrospectives suivantes :

-   Gyshido
-   Ishikawa
-   Etoile de mer
-   Speedboat
-   Rétro-Châtaigne
-   Rétro-glandouille

On va essayer :

-   Questions Crowdfunding
-   Bestiaire des contes et légendes
-   Rétro Satire (en provenance de
    [Claude](http://www.aubryconseil.com/) cette semaine aussi !)
-   Chapeaux de Bono

Il nous reste à mettre en œuvre :

-   Niveaux logiques de Dilts

## Marie-Anne test en Asie

Le **Gyshido** et le **Speedboat** de
[Marie-Anne](https://twitter.com/marieannelb), [notre ambassadrice en Asie](http://benextcompany.com).

{{< image src="/images/2016/04/retro-ma-1.jpg" title="rétrospective speedboat" >}}

[{{< image src="/images/2016/04/retro-ma-2.jpg" title="rétrospective gyshido" >}}

Et **depuis hier (4 mai 2016)**, la rétro "**bestiaires des contes et
légendes**".

{{< image src="/images/2016/04/retro-contes-ma.jpg" title="rétrospective Contes et légendes" >}}

Avec quelques dialogues (vive whatsapp) pour expliciter les champs :

> -   MA : Qu'entends tu par Trolls dans ce type de rétro ?
> -   Les trucs ou toi ou quelqu'un dirait : roooooh
> -   MA : Genre des trucs négatifs ? Des choses dont j'aurais pu me
>     passer ?
> -   Pas forcément. Des provocations, des trucs outranciers.
> -   MA : Et le nain? Ça en revient un peu au même qu'avec l'elfe non?
> -   Non c'est l'inverse. Le nain est rustique : il faut l'adoucir. Un
>     truc bien encore brut de décoffrage qu'il te faudrait sophistiquer
>     durant ton séjour. L'elfe un truc super mais encore fragile, en
>     équilibre, qu'il faudrait consolider. Pour reproduire, pour plus
>     de certitude.
> -   MA : Subtil. Merci pour l'explication !
> -   MA : Et le lutin c'est qqch à améliorer aussi ou pas forcément ?
> -   Non c'est un détail que tu veux rendre plus évident. Mettre en
>     évidence un petit truc caché.

Le [compte facebook du périple de Marie-Anne](https://www.facebook.com/thefrenchieinasia/?fref=ts).

## Quelques rétros du [raid agile 5](http://raidagile.fr)

Il s'agit de "rétro flash", de deux minutes, pendant un atelier de
Claudio, d'où le peu de matière.

La rétro [chapeaux de Bono](https://fr.wikipedia.org/wiki/M%C3%A9thode_des_six_chapeaux),
rétro contes et légendes,

{{< image src="/images/2016/04/retro-bono.jpg" title="rétrospective chapeaux de Bono" >}}

{{< image src="/images/2016/04/retro-contes.jpg" title="rétrospective Contes et légendes" >}}

{{< image src="/images/2016/04/retro-ishikawa.jpg" title="rétrospective Ishikawa" >}}

{{< image src="/images/2016/04/retro-gyshido.jpg" title="rétrospective Gyshido" >}}
