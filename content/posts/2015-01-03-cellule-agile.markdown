---
date: 2015-01-03T00:00:00Z
slug: presentation-cellule-agile
title: Présentation cellule agile
---

Ce **23 janvier 2015** dans les locaux (**Neuilly/Paris**) d'un
partenaire (Maltem/Openbridge) avec qui je travaille régulièrement et
depuis longtemps, je présente avec l'inénarrable [Alexandre
Martinez](http://doubleboucle.fr/) un retour d'expérience sur une très
belle réussite à mes yeux : la cellule agile que j'ai eu le plaisir de
suivre depuis les premiers jours chez ... (Ooops, je ne sais pas si j'ai
le droit de citer ce client), pensez à un groupe français de l'énergie.

## Programme (officiel) de la matinée

Venez découvrir lors d’une matinée thématique les tenants et
aboutissants de la mise en place d’une cellule agile au sein d’un grand
groupe de l’énergie en France :

-   Comment nous sommes passés de 4 personnes à 50 personnes en 3 ans
    pour le plus grand plaisir de notre client
-   Quels ont été les éléments fondateurs clefs
-   Quelles sont les étapes de cette croissance
-   Comment vit une telle cellule au jour le jour

Nos intervenants : Pablo Pernot -- SmartView -- interviendra en tant que
coach agile qui a accompagné de façon externe et impartiale la cellule
depuis la phase d’avant-vente jusqu’à présent. Alexandre Martinez
--Maltem/Openbridge-- est un des éléments clefs depuis les premiers
jours au sein de la cellule.

La présentation s’achèvera par une table ronde qui permettra d’évoquer
toutes les questions auxquelles nous n’aurions pas répondu.

### Profil des intervenants

**Pablo Pernot** - [Smartview](http://www.smartview.fr)

{{< image src="/images/2015/01/pablo.jpg" title="Pablo" >}}

Après une maîtrise sur les Monty Python, un DEA sur le non-sens et
l’absurde, l’entame d’un doctorat sur les comiques cinématographiques il
parait normal que Pablo Pernot se soit lancé dans le management et la
conduite du changement, c’est -finalement- une suite logique.
Actuellement un coach expérimenté en management organisationnel,
conduite du changement, et agilité.

Fondateur de [SmartView](http://www.smartview.fr)

**Alexandre Martinez** -
[OpenBridge](http://www.maltem.com/en/openbridge-accueil/)

{{< image src="/images/2015/01/alexandre.jpg" title="Alexandre" >}}

Développeur depuis toujours (ou presque) et après un doctorat mêlant
microélectronique, informatique et thermodynamique, Alexandre Martinez a
décidé de faire de sa passion son métier et de poursuivre son
exploration des systèmes complexes, informatiques et humains, en
découvrant l’agilité. Aujourd’hui, il officie en tant que Scrummaster,
coach agile et architecte dans une cellule agile au sein d’un grand
groupe de l’énergie en France.

## Mes premiers retours

On démarre cette semaine avec Alexandre la préparation de cette matinée.
Je ne peux vous en donner les détails, mais je peux déjà vous dire sur
quoi il me parait essentiel d'insister pour comprendre la réussite de
cette cellule. D'ailleurs qu'est ce qui me fait dire que cette cellule
réussit ? Plusieurs éléments :

-   Elle ne cesse de grossir : de 4 développeurs initiaux, nous en
    sommes à 45, et je ne serais pas surpris d'en compter 10 à 15 de
    plus cette année, ce qui va poser d'autres questions.
-   Les demandes continuent d'affluer au sein du groupe, les résultats
    passés sont donc satisfaisant à minima.
-   Quand j'interrogeais les dirigeants de OpenBridge en 2012 (la
    cellule démarre en 2012), l'un de leur souci était que *tout le
    monde voulait travailler pour la cellule* (parce que l'on
    progressait, que l'on était écouté, que l'on faisait du
    développement avec tests automatisés et plate-forme d'intégration
    continue, etc.).
-   Certains membres de la cellule qui ont quitté celle-ci veulent
    revenir, ce n'est pas forcément simple. Si on leur propose le
    diagramme de l'[Agile fluency de Martin
    Fowler](http://martinfowler.com/articles/agileFluency.html) (vu au
    [raid agile](http://raidagile.fr) par exemple) ils se situent
    spontanément au niveau *optimize value*, ce n'est pas si fréquent.
-   Les dernières pratiques sont au rendez-vous : [Open Agile
    Adoption](/2014/04/histoires-dopen-agile-adoption/), *Holocratie*,
    etc.

Quels sont les éléments qui de mon point de vue sont clefs ? (le point
de vue d'Alexandre est probablement plus important, car il y vit depuis
3 ans, je ne fais que rôder autour en quelque sorte). Je vais en citer
trois qui me tiennent à cœur (il y en a d'autres ! il faudra venir le 23
janvier) :

-   Le contrat : le contrat qui lie les deux parties prenantes (le
    groupe et le prestataire) est un engagement de moyen, pas un
    engagement de résultat. C'est essentiel pour partir sur de bonnes
    bases et ne pas faire dévier immédiatement toutes les pratiques,
    pour se concentrer sur la valeur et pas sur des chimères.
-   La co-localisation : les équipes sont co-localisées au sein du
    groupe. C'est une cellule, elle appartient au corps pour lequel elle
    travaille.
-   Des leaders courageux et humains : il fallait du courage pour signer
    l'engagement de moyen, j'ai vu du cœur et de la fierté. Je ne dis
    pas que les leaders font tout, je dis qu'ils sont clefs car leur
    pouvoir de nuisance ou de soutien est énorme.

## Invitation

Si vous êtes intéressés par cette matinée car cela est lié à votre
quotidien, n'hésitez pas à vous y
[inscrire](https://www.weezevent.com/cellule-agile-en-entreprise), c'est
gratuit. Merci aux amis et copains coachs, consultants, de **ne pas**
s'inscrire pour leur part mais de me [contacter](/pages/contact.html) en
direct pour voir si je peux les accueillir ou pas (où organiser un bon
repas pour en parler).

