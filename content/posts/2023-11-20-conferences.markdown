---
date: 2023-11-20
slug: conferences-2024 
title: "Conférences 2024 ? "
draft: false
---

2024 s'annonce une année différente. 

Je boucle un cycle. J'en prépare un nouveau. 

D'ici là je souhaite me remettre à croiser un maximum de personnes. Sous différents formats. 
L'un de ses formats peut être les conférences. Je viens de postuler chez [Agi'Lille (Nord Agile)](https://www.linkedin.com/posts/nord-agile_agilille-activity-6639867565172633600-8ZeR/) pour 2024.

Deux sujets pour le moment : 

# La disparition 

**La disparition...ou comment mener une transformation agile sans jamais en prononcer le nom.** 

"La Disparition" est un roman en lipogramme de Georges  Perec publié en 1969. Son originalité est que, sur près de trois cents pages, il ne comporte pas une seule fois la lettre e, pourtant la plus utilisée dans la langue française. (https://fr.wikipedia.org/wiki/La_Disparition_(roman)). 

Le terme agile a été mélangé à toutes les sauces. L'agile est constamment déformée, maltraitée. Ainsi, aujourd'hui quand vous dîtes "agile" dans une salle, les gens lèvent les yeux au ciel. C'est normal on leur a généralement fait avaler un brouet infâme. Pourtant il n'y a pas encore eu de changement de paradigme et l'agilité demeure une approche qui a fait ses preuves et qui continue d'être très efficace quand elle est comprise, désirée et bien appliquée. 

Aujourd'hui une des façons de faire, qui comporte pas mal d'avantages, est de ne pas prononcer le terme "agile". Ainsi comment mener une transformation agile sans jamais prononcer le mot "agile" ? C'est le sujet de cette session. 

Comprendre et penser une transformation sans utiliser le vocabulaire oblige à se poser les bonnes questions, à avoir les bons raisonnements. Cela facilite l'implantation de l'état d'esprit agile dans une entreprise.Et cela oblige à la compréhension des principes et des leviers sous-jacents à nos outils. C'est une façon parmi d'autres d'aborder cette agilité d'entreprise, il y en a d'autres, mais j'en perçois les bénéfices. 

L'autre sujet est : 

# Cartographie

**Cartographie et dynamique de nos accompagnements : qui fait quoi quand comment avec qui avec quelles demandes quelles forces quelles faiblesses dans nos accompagnements et dans le temps.**

J'imagine pendant 40/50mn tirer le fil de la pelote de laine de toutes 
ces interventions de coaching, de mentoring, d'expertise dans nos accompagnements agiles. 
Comment s'entrelacent-elles, s'opposent-elles, se complètent-elles, s'expliquent-elles, se comprennent-elles ? 

- Comment commencent-elles ? 
- Avec qui (rôle, positionnement dans l'entreprise) ? Avec quel type de demande ? 
- Quelle posture est attendue ? 
- Quelle posture serait la bonne à tel endroit ? 
- Quel tarif ? 
- Quel objectif ? 
- Quel malus ? Quel bonus ? Selon le positionnement.
- Quels sont les rôles présents (coach agile ? Coach orga ? Coach exe ? Coach indiv ? Coach système ? Manager ? Mentor ?  
PO ? PM ? SM ? Dev ? Expert ? Lead ? etc.) et comment les positionnements 
et les compréhensions de ces rôles évoluent, ou ont évolué (au fil des années) ?   

Je pense que cela serait intéressant pour pas mal de monde de bien se positionner dans nos accompagnements, de bien comprendre les attendus, de bien clarifier nos accompagnements, de bien saisir les bénéfices entre les rôles et comment ils se complètent, de bien saisir les faiblesses de certains rôles et comment ils s'opposent à d'autres, de comprendre l'évolution au fil des dernières années sur ces rôles et ces attentes. 

# L'agilité dans 20 ans ?  

*j'ajoute ce sujet suite à une conversation avec vous*. 

Est-ce que agile est encore d'actualité ? Est-ce que nous n'avons pas déjà changé de paradigme ? Est-ce que son rejet s'explique par un changement de ce type ? Que signifierait un changement de paradigme ? 

Le début de mes réflexions : 

- Il y a plusieurs formes d'agilité. En particulier deux très différentes qui coexistent aujourd'hui : celle de l'émancipation des personnes (les développeurs à l'origine de *Xtreme Programming*) qui puise ses sources dans des formes historiques comme la sociocratie. Et celle des startups californiennes : l'accélération, l'optimisation, de l'adaptation constante, de la pertinence, voire de la frugalité. Et dans ces deux grands ensembles, on peut voir des formes très différentes. Contrairement aux apparences les deux sont recherchées par la pensée dominante, le capitalisme. La première parce que l'engagement fait de chacun de nous de meilleurs artisans, ouvriers, collaborateurs. La seconde, car c'est une optimisation de la performance. 
- On doit s'interroger sur l'utilité de cette agilité à la fin du capitalisme. Est-ce qu'il y a des choses à garder ? Très certainement : l'émancipation des personnes, l'engagement, la frugalité, l'adaptation, une certaine forme d'optimisation, etc. Et qu'est-ce qui ne sera plus à l'ordre du jour : principalement une certaine idée de la valeur, une certaine forme d'optimisation, une temporalité différente, etc.
- Et l'on doit s'interroger sur les défis du monde après l'effondrement. Si on acte que l'atténuation n'étant absolument pas prise en compte par nos gouvernants, voire nos concitoyens, il me parait sensé de se préparer à l'adaptation post-effondrement. Est-ce que cette agilité sera toujours d'actualité ? Et si oui en quoi. (Je dois vous dire que j'ai commencé l'écriture d'un truc qui ressemble un peu à cela).  

Une session un peu plus hors sol... Je sais que l'on me reproche des fois d'être trop "meta". 


# 1970-2070 équipe informatique, un récit d'anticipation 

*j'ajoute ce sujet suite à une conversation avec vous*. *C'est beaucoup plus ambitieux et j'ai très envie de le faire. D'autant que cela consolide le travail que je fais actuellement avec Henri L. sur CarbonSquad, un jeu de rôle.* 

Se remémorer les grands mouvements du développement du waterfall à l'agilité, les dynamiques en jeu, les positionnements des entreprises, les différents types d'entreprise, et au milieu, le codeur ou la codeuse, de 1970 à 2024. Et puis se projeter dans les 50 prochaines années à venir. L'IA change la donne ? Quel usage ? Quels bénéfices si il y en a ?  La fin des ressources la fin de l'IT ? Oui ? Non ? Faut-il envisager l'IT et plus globalement le numérique différemment ? Le cloud ? Le renouvellement des ordinateurs des processeurs ? La loi de Moore ? Le réseau tout simplement ? 

2052 c'est l'effondrement, d'un côté des grandes corporations qui subsistent et poussent le monde d'aujourd'hui à l'extrême, de l'autre une branche lowtech s'organise. D'un côté le réseau et les satellites, de l'autre "La maille", un réseau mesh fait de bric et de broc.  

En sous-marin le *Carbon Squad*, un groupe d'intervention anti-corpo qui a fait son apparition avec un premier sabotage en 2034 à la coupe du monde de football en Arabie Saoudite.  

Que deviennent les codeurs ? 

Mi-récit réel sur les années passées, mi-récit d'anticipation sur les années à venir, on s'interroge sur le rôle de codeur et codeuse (élargi : ux, design, facilitation, etc.), sur ces équipes informatiques et les différents univers que l'on peut imaginer advenir. 
<>

# Votre avis ? 

On verra si je suis retenu. D'ici là j'aimerais savoir 

- Si cela fait sens pour vous ? 
- À quelles conférences vous aimeriez en parler avec moi ? 
- De quel sujet vous aimeriez m'entendre parler ?

Merci 



Vous pouvez m'écrire à [pablo {arobase} projetwinston {point} fr](mailto:pablo{arobase}projetwinston{point}fr)
ou sur [mastodon](https://toot.portes-imaginaire.org/@pablopernot) (ou même soyons fous sur Linkedin)
