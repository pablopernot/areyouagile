---
date: 2011-05-01T00:00:00Z
slug: anatomie-dune-mission-agile-sudweb-2011
title: Anatomie d'une mission agile / Sudweb 2011
---

Dans quelques dizaines de jours, le 27 mai plus exactement, à Nîmes,
j'aurais le plaisir de faire une session autour de l'agilité lors du
premier [Sudweb](http://sudweb.fr), 2011 donc. [Il reste 24H chrono pour
acheter des places](http://sudweb.fr/pages/Inscription) !

{{< image src="/images/2011/05/Banniere300-250-b.jpg" title="Sudweb" >}}

Je tiens d'abord à saluer l'effort réalisé par ce groupe d'aventuriers :
déclencher ce type d'évènement, avec ce standing (salle, vidéo,
traiteur, orateurs étrangers), dans notre région... Bravo, hats off !

Pour ma part je dois représenter le quota gardois. eh eh.

Au début je me suis interrogé, je suis la seule session typée
méthodologie, qui plus est : agile. Naturellement il y a plein de très
bonnes raisons pour que le web embrasse cette méthodologie, donc pas
trop d'inquiétude malgré tout. De plus je perçois un dessein dans la
programmation (si, si) : achat (de prestations web), méthodologie
(agile), outillage (cloud), politique/valeur (web & humanitaire)... Par
la suite des aspects plus techniques. Mais malgré tout il me semble que
l'on prend du recul, que l'on cherche à aborder les sujets d'une façon
plus globale, plus riche.

Tant mieux, ma session aura pour but de présenter un retour d'expérience
au sein d'un grand éditeur (que je ne nommerai pas, trop de contraintes
bureaucratiques et juridiques). 9 mois au compteur dans le déploiement
de l'agilité chez ces gens, gens ? si j'osais je dirais copains
désormais (tout un plateau me dit "bisous" en forme de boutade parfois
quand je rentre dans le sud, con). Au delà de ce retour d'expérience
-fil rouge-, il s'agira avant toute chose de *décider les décideurs à se
décider* (j'ai un copyright sur cette phrase lourdingue). Je ne
rentrerai presque pas (ou pas du tout si c'est possible) dans les
questions : qu'est ce scrum, xp, kanban ? C'est à mon avis un acquis. Et
d'ailleurs si cela ne l'est pas, cette connaissance n'est pas
obligatoire pour comprendre mon propos, et elle se retrouve facilement
sur ... le web.

Voilà en espérant vous croiser à [Sudweb](http://sudweb.fr).

Le premier slide de la session durant laquelle (en terme de
présentation) je vais essayer de réconcilier mes études
littéraires/cinématographiques avec mon métier d'aujourd'hui... (je
pense qu'il est trop tard pour qu'ils annulent ma session ah ah ! ):
([pensez à réviser les Monty Python dès ce soir sur
Arte](http://www.arte.tv/fr/mouvement-de-cinema/Monty-Python-toute-la-verite_21--/3851630,CmC=3851654.html)
!)

{{< image src="/images/2011/05/anatomie.jpg" title="Anatomie" >}}

Ps: C'est une conférence NO WIFI ! avec des humains dedans ! N'amenez
pas ou éteignez vos téléphones, ça serait trop con de louper l'occasion
de discuter avec de vraies personnes.
