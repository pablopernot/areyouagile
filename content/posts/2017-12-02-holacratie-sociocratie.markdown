---
date: 2017-12-02T00:00:00Z
slug: petite-serie-sur-holacratie-et-sociocratie
title: Petite série sur Holacratie et Sociocratie
---

C'est avec plaisir que j'avais entrepris avec [Géraldine](http://theobserverself.com) et à la demande de [Stéphane](https://www.infoq.com/fr/profile/St%C3%A9phane-Wojewoda) de [infoQ](https://www.infoq.com/fr/holacratie) une petite série sur l'**Holacratie** et la **Sociocratie**. Un minibook devrait voir le jour d'ici peu qui recueille [ces articles](https://www.infoq.com/fr/holacratie) ainsi que d'autres. L'idée était faire connaître notre vécu de l'Holacratie et de la Sociocratie, de donner un panorama de ce qui nous parait important à savoir pour se jeter dedans. 

Ces articles datent de avril à octobre 2017. J'essaye de vous les commenter un peu pour donner un sens plus globale à la lecture. 
Vous pouvez parler d'Holacratie comme de Sociocratie, il y a de petites variations, mais la plus importante c'est l'aspect marque déposé, payant de l'Holacratie *versus* l'aspect *opensource* de la Sociocratie. Pour le premier vous pourriez lire le livre de son créateur (le terme "créateur" me parait vraiment exagéré, il a assemblé et déposé, avec intelligence) : Brian Robertson, Holacracy. Pour le second je vous recommande le site [sociocracy30.org](http://sociocracy30.org).      


## Les articles

{{< image src="/images/2017/12/gothamocratie.jpg" title="Gothamocratie" >}}

### [Holacratie, pourquoi on en parle ?](https://www.infoq.com/fr/articles/holacratie-pourquoi-on-en-parle)

D'abord une entrée en matière, que fais écho avec cet article [le cadre de l'holacratie](/2016/12/cadre-de-holacratie/), donc l'objectif était de placer le cadre justement. L'idée est de présenter la pensée, ses concepts.  

### [Holacratie, comment démarrer lundi](https://www.infoq.com/fr/articles/holacratie-comment-demarrer-lundi)

Puis de proposer une façon de mettre le pied à l'étrier, de façon pragmatique. 

### [Gothamocratie, explicit lyrics](https://www.infoq.com/fr/articles/gothamocratie-explicit-lyrics)

Ensuite, faire sentir les odeurs, au travers d'une réunion imaginaire dans un tripot de Gotham City, d'une conversation de l'Holacratie. Ainsi donc le cadre, puis une façon pragmatique de démarrer, enfin faire ressentir le contenu. 

### [Chausse-trappes et chimères de l'Holacratie](https://www.infoq.com/fr/articles/chausse-trappes-et-chimeres-holacratie)   

Pour conclure, rappeler à chacun de s'approprier avec intelligence les outils qu'on lui fourni. Toujours comprendre une pensée et ne pas hésiter à bifurquer ou adapter si on trouve quelque chose de meilleur. 

## Les interviews

Un contexte personnel nous a permis d'accéder à Mark et Folkert qui ont vécu une transformation sociocratique/holacratique de grande dimension (1000 personnes) depuis cinq années. Merci à eux. 

### [Mark Bent](https://www.infoq.com/fr/articles/spirit-auto-organisation)

{{< image src="/images/2017/12/mark-bent.jpg" title="Mark" >}}

Mark était le directeur qui a initié le changement. 

### [Folkert Borstlap](https://www.infoq.com/fr/articles/spirit-un-salarie-raconte)

{{< image src="/images/2017/12/Folkert-2.jpg" title="Folkert" >}}

Folkert était un membre de l'un des syndicats et est toujours un salarié de Spirit. 

## Mes autres articles précédents sur le sujet

## [Le cadre de l'holacratie](/2016/12/cadre-de-holacratie/) 

Rappeler et synthétiser ce qui paraît essentiel dans l'Holacratie (ou la Sociocratie). 

## [Hellocratie, holocratie, holacratie, honolulucratie](/2016/04/hellocratie-holocratie-holacratie-honolulucratie/) 

Une première entrée en matière. 
