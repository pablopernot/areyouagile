---
date: 2024-07-02
slug: la-disparition 
title: "La disparition"
draft: false
---

Je me permets de réécrire rapidement cet article (et donc d'en changer la date et l'ordre).

J'ai essayé trois fois de prendre la parole sur le thème l'*agilité sans en parler*. Je pense avoir été clair, mais pas si profond à Niort, pas très clair mais plus profond à Toulouse, et finalement assez clair et assez "profond" (mes aïeux ce terme) à Lille.

Oui, oui, je pense avoir été clair à Lille. Il y a toujours les grincheux qui veulent une conférence TED, lisse, insipide, qu'on ne retient pas finalement, et ça, jamais !  

Néanmoins je pense que malgré tout cette réflexion est mieux portée à l'écrit sur ce coup, alors je m'échine -- là, maintenant -- à la mettre à l'écrit, sur papier comme on dit. Probablement disponible courant juillet.  

D'ici là vous pouvez consulter mes vignettes, qui là, je vous l'accorde, sont loin d'être mes meilleures, même si sur le grand écran à Lille, ça bastonnait bien.

{{< image src="/images/2024/06/la-disparition.jpg" alt="Nord agile" title="Nord Agile" width="600">}}
merci Fabrice Bloch pour la photo, et pour ta conférence.

Grand merci aux orgas de ces trois évènements.

Cliquez sur les images pour accéder aux vignettes pdf :

[{{< image src=/images/2024/06/la-disparition-lille.jpg title="Lille" width="600" >}}](/pdf/2024-la-disparition-v3.pdf)
[{{< image src=/images/2024/06/la-disparition-toulouse.jpg title="Toulouse" width="600" >}}](/pdf/2024-la-disparition-v2.pdf)
[{{< image src=/images/2024/06/la-disparition-niort.jpg title="Niort" width="600" >}}](/pdf/2024-la-disparition-v1.pdf)
