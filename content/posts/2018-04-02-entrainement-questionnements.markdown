---
date: 2018-04-02
slug: entrainement-questionnements
title: Entrainement aux questionnements
---

A l'occasion d'un [meetup School Of PO](http://schoolofpo.com/) je propose avec l'aide de [Nicolas](https://twitter.com/nikepot) une session d'entrainement aux questionnements. Il s'agit plus d'une session de facilitation, de coaching, d'analyse, que d'autres choses. L'objectif, probablement prétentieux, est d'amener les personnes concernées à (se) poser de meilleures questions. Une "meilleure" question est une question qui pourrait amener une meilleure compréhension de la situation, une meilleure attitude, et une meilleure attention, au travers d'une réflexion personnelle et d'un premier engagement possible au travers d'une action dont la forme peut émerger selon le contexte. 

Pour cela je propose un premier cheminement que j'ai essayé de déduire de mes comportements et de mes analyses. On l’appellera cheminement alpha, pas dans le sens premier de cordé, mais premier essai, à redéfinir, à compléter, à préciser, à corriger. Alpha comme sujet à plein d'améliorations.

	Suite au meetup School of PO je vous confirme le côté Alpha de ce premier atelier, je complète cet article dans la semaine par quelques ajouts/modifications

## Constatation : geste/cadre, moyen/finalité

Je constate que les questions portent trop souvent sur le geste : comment faut-il exactement faire ; et qu'elles ne portent pas assez sur le cadre : un espace dans lequel on peut faire de plein de façons différentes, laissant ainsi à chacun le soin de faire au mieux justement. On distingue donc d'abord **le geste : une seule façon de faire, une liberté réduite ; du cadre : un espace qui permette plein de façons de faire**. Un peu comme une question fermée qui ne laisse le choix que sur oui ou non, et une question ouverte qui laisse de l'espace pour une réponse bien plus riche. 

Je constate aussi que les questions portent bien trop souvent sur le moyen plutôt que sur la finalité. Comment traverser le pont ? Pourquoi être agile ? Mais qu'il s'agisse du pont ou de l'agile, ce n'est certainement pas la finalité. Ces questions il faut se les poser, mais chacun, à soi, c'est essentiel d'être son propre analysant. Mais la dynamique que je souhaite enclencher au travers de questions, ou **si je souhaite provoquer cette introspection, cette émergence, je me dois d'interroger le sens, pas le moyen**. 
 

On pourrait demander aux gens de noter leurs questions et de les placer dans une matrice en deux dimensions, geste/cadre, moyen/finalité. 

{{< image src="/images/2018/04/matrice-questions.jpg" title="matrice questions" >}}

L'idée de cet entrainement est de ramener les questions vers plus de cadre et plus de finalité (de sens). Dans un premier temps en trouvant la finalité si elle n'est pas apparente, dans un deuxième temps en définissant un cadre qui permette à un geste d'émerger (mais de ne pas être prédéfini) et d'être essayé. 

Le moyen et le geste sont proches, il n'est pas facile de les démêler. Le geste reste un acte, un moyen est plus vaste cela peut être un ensemble d'actes. Ce que l'on cherche à détecter c'est si un moyen est une cible. "On cherche à aller plus vite". "Plus vite" est la cible or ce n'est qu'un moyen. Ou si un moyen est déjà posé en réponse. "En allant plus vite est-ce que nous réussirions ?". "Plus vite" est là le geste : le moyen d'action, l'acte. C'est dans ce cas que j'appelle cela un geste. On veut éviter ces deux cas : le geste (comme réponse), et le moyen (comme cible).   

## Exemple

Nicolas m'interroge : 

> *Qu'est ce qui fait que la prise de décision inter équipe est agile ?*. 

Il faut contextualiser. Nicolas est *product owner* et je suis dans ce rôle le *coach agile*. Il a besoin de soutien, de support, de levier, de compréhension, dans son milieu pas si simple. 

A mes yeux sa question est assez en bas à gauche sur la matrice évoquée plus haut. On sent "qu'il faut prendre une décision". C'est le geste. C'est l'acte. Il n'y a pas trente-six façons de faire : "il faut prendre une décision". On pourrait dire qu'il y a mille façons de prendre une décision, mais dans la question énoncée, la prise de décision n'est pas mise en question, n'est pas questionnée elle-même. Elle me donne l'impression d'être un geste déjà acquis. On ne parle que d'un moyen comme cible : être agile. L'agile n'est jamais la finalité, juste un moyen. 

**J'aimerais inverser les choses et tendre vers un cadre plus large de possibles réponses qui s'orientent vers une finalité, tout en se limitant à un espace pour essayer sécurisé : c'est à dire assez réduit**.       

Pour cela je me base sur des mécanismes bien connus et assez simples à mettre en œuvre même si l'exercice peut se révéler hasardeux dans sa mise en œuvre, on ne sait jamais si on va s'en sortir.

### J'interroge Nicolas pour trouver la finalité. 

Comme un [cinq pourquoi](/2011/01/mammuth-et-les-5-pourquoi/) ou les [six poignées de main](https://fr.wikipedia.org/wiki/Six_degr%C3%A9s_de_s%C3%A9paration) je fouille le périmètre pour comprendre ce que cherche Nicolas. Je pourrais dessiner une arbre de possibilité que j'explore. **Pour que Nicolas se sente à l'aise et compris, je reprends au maximum de ses mots, je ne les substitue pas par les miens**. 

* "Pourquoi veux-tu une prise de décision inter-équipes qui soit agile ?" "Parce que aujourd'hui les prises de décisions inter-équipe ne marchent pas, ou n'ont pas lieu".  
* "Pourquoi les prises de décision inter-équipes ne marchent pas ?" "Bon disons qu'elles se déroulent trop tard, pas dans le bon timing".
* "Pourquoi est-ce que les prises de décision n'ont pas lieu ou se déroulent hors timing ?" "Parce que chacun poursuit ses objectifs sans tenir compte des dépendances car tout sera intégré à la fin, avec les problèmes que l'on connait". 
* "Pourquoi est-ce que chacun poursuit ses objectifs sans tenir compte des autres, des dépendances ?" "Parce que chacun a des objectifs personnels et pas de groupe". 
* "Pourquoi est-ce que chacun a des objectifs personnels et pas de groupe ?" "Parce que l'on a une vision projet et pas produit". 

### Quand je pense avoir trouvé une des finalités je formule une question sans jugement

Un jugement personnaliserait trop la question. Il ne s'agit pas bien ou du mal, il s'agit de créer un espace qui laisse place à une émergence. Pour cela j'utilise comme indiqué dans le livre [change your question,change your life](http://inquiryinstitute.com/resources/book/), j'utilise le "on" ou le "nous", et surtout pas le "je", ou le "il(s)", qui désigneraient déjà une partie de la réponse. Je repars sur la finalité découverte, ou choisie (si il y en a plusieurs...). 

> *"Comment peut-on avoir une vision produit ?"*

### Je rends l'émergence possible, mais comme dans un laboratoire je souhaite les observer : je fonctionne par essais. 

J'ai voulu ouvrir le cadre pour laisser un maximum d'espace aux possibilités, à l'émergence. Mais je souhaite rendre visible cette émergence pour générer un sentiment de progrès, une observation factuelle de réussite ou d'échec, un apprentissage validé ; et je souhaite faire comprendre ce cheminement étape par étape. Pour cela **si je souhaitais un cadre avec de l'espace pour les réponses, je recherche un espace assez réduit pour faire nos essais de réponses : je vais demander une limite dans la durée ou dans l'amélioration**. 

> *"Comment pourrait-on avoir une première vision produit alignée dans le mois ?"* 

C'est la question que je pose à Nicolas, le *product owner*, ou que je lui suggère de poser lui même à son groupe de travail autour du produit. Plutôt que d'interroger la prise de décision agile. 



