---
date: 2010-11-05T00:00:00Z
slug: la-communaute-agile-se-cherche-une-certification
title: La communauté agile se cherche une certification
---

La communauté agile semble se chercher actuellement, le succès
l'étouffe-t-elle ?

Entre les grands penseurs qui sont parfois trop peu confrontés au réel,
et les rapaces qui ont senti l'odeur de l'argent frais (l'agile c'est un buzz ! c'est donc aussi du
fric !), chacun essaye de se positionner et d'occuper le terrain (côté
français je vois fleurir un institut, une fédération, cela pourrait être
inquiétant. Je ne jette pas la pierre, toutes ces instances partent
probablement d'une bonne idée et ont sûrement -sans ironie- un but
avouable. Au passage agile-academie.com et .fr sont dispos, allez y !!
-avec ironie-).

On assiste donc à une guerre de positionnement dont la certification est
probablement la face la plus visible. Constat : la certification agile
n'a pas de sens réel : soit on passe 2 jours (plus de 1000 euros) avec
un coach agile et on obtient automatiquement la certification
(ScrumAlliance), soit on paye 100 euros et on a le droit de passer un
QCM pour avoir une certification (scrum.org).

Alors oui c'est mal car on se retrouve avec pas mal de gens qui n'ont
aucune expérience réelle du terrain et qui sont certifiés. Mais est-ce
que cela diffère des autres certifications ? ben non, pas vraiment à mes
yeux. Est-ce que cela a de la valeur : très peu mais pas rien (j'ai
passé du temps -2 jours- avec un coach agile, ou je me suis plongé dans
le manuel (20 pages) de scrum.org avec un QCM au  bout) ; c'est déjà une
différence n'en déplaise à certains. Le problème est plutôt que certains
s'octroient le droit d'attribuer la certification, et que d'autres n'ont
pas ce privilège.

Faut-il blamer quelqu'un ? Celui qui abuse du système en certifiant à
tour de bras ou celui qui n'emploie que des gens "certifiés" sachant que
cette certification n'a que peu de valeur ? J'opte pour l'acheteur. Sans
demande, pas d'offre... Pour autant il est compréhensible que l'on
souhaite s'assurer des compétences des personnes que l'on embauche ou
avec lesquelles on va travailler. Il me parait cependant évident que les
acheteurs doivent faire un effort concernant leur recrutement.

Est-ce possible de certifier en agile ? Pas moins que dans d'autre
domaine. Cela a-t-il un sens ? Je suis moins convaincu. Le côté humain,
conceptuel, contextuel jouent un rôle fort, comment dès lors proposer
une check list permettant de valider les connaissances agiles de
certains et industrialiser (argh) cela...

Alors faut-il certifier malgré tout l'agile et comment ? Je me baserai
sur le modèle CMMi (que bien j'ai connu). Je certifierai une
organisation, une entité, et non pas une personne, et à intervalles
réguliers. En continuant ce parallèle avec CMMi, au lieu de pratiques
j'évoquerai les principes et valeurs et je laisserai libre cours à
l'entitée de les embrasser comme elle le souhaite. Mais je ne fais que
repousser le problème (bien visible côté CMMi) : qui ??? a le privilège
de certifier...

Et on retombe ainsi sur les errements actuels de la "communauté agile" :
faut il un pragmatisme outrancier (quitte à faire tourner la machine à
billets), ou faut-il se lancer dans de longues diatribes sur le
changement culturel, le côté humain, etc... qui fleurent bon parfois le
charlatanisme ou la croyance.  Au passage les deux ne sont pas si
mauvais, négatifs. Oui c'est beau et bien de défendre un idéal, une
philosophie (même si les clients ne cherchent pas forcément cela...enfin
un salarié heureux est un bon salarié). Deuxio je ne vois aucun mal à
gagner de l'argent avec l'agile. Il faut bien gagner de l'argent, si en
plus on peut le faire en faisant ce que l'on aime...

Ca tiraille fort. Mais c'est bon signe aussi ces discussions endiablées,
ces oppositions.

Et, pour adoucir mon billet, de mon humble point de vue, il y a une voie
au milieu et la majorité des gens que j'observe semblent la choisir.

Ah je note que agile-certification est disponible en .fr (messieurs
tirez les premiers).

Un très bon article sur les problèmes soulevés par la certification
agile :

http://xprogramming.com/articles/csm-certification-thoughts/

Les faits m'accablent :

a)  je me mêle à la meute concernant la certif agile
b)  je suis certifié
c)  je gagne de l'argent avec l'agile
