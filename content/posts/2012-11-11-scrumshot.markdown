---
date: 2012-11-11T00:00:00Z
slug: scrumshot
title: Scrumshot
---

Ci-joint un petit support (réalisé avec [Inkscape](http://inkscape.org/)
et un plugin "à la" prezi : [Sozi](http://sozi.baierouge.fr/wiki/en:install)), dont je me sers de
temps en temps en formation ou ailleurs. Il s'agit donc d'une image SVG
qui permet de se parcourir étape par étape (touche espace, ou par le
menu touche F5, les flèches marchent aussi). Je vous laisse faire les
commentaires off.

Télécharger le fichier svg (clique droit, "save as") : 
[{{< image src="/images/2012/11/scrumshot.jpg" title="Scrumshot 1.0" >}}](/images/scrumshot_1_0.svg)
ouvrez le avec un bon navigateur (firefox quoi).

Comme c'est du SVG vous pouvez **zoomer** !

J'ai piqué l'idée de *Docteur House* comme "Product Owner" typique  à
[Pierre Neis](http://managingagile.blogspot.fr/) (@elpedromajor) ! Merci
Pierre.
