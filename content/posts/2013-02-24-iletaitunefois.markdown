---
date: 2013-02-24T00:00:00Z
slug: il-etait-une-fois
title: Il était une fois
---

Il était une fois un responsable de produit dont les produits ne trouvaient pas leur public. Cela rendait ce responsable produit très
malheureux. Un jour pourtant alors qu'il cherchait encore et encore comment rendre ses produit attractifs il pécha un article en ligne. Un
article par [Pixar](http://www.pixar.com/) sur l'art de raconter une histoire, et il se murmura : *si cela marche pour une histoire, cela
marche pour un produit*. Depuis ce jour là il mis un soin particulier à suivre les [22 recommandations](http://io9.com/5916970/the-22-rules-of-storytelling-according-to-pixar) de l'art de raconter des histoires de [Pixar](http://www.pixar.com/). Il
vécut heureux et ses produits se vendirent comme des petits pains.

Cette fable a été depuis longtemps oubliée, et pourtant ces 22 recommandations existent bel et bien, et le responsable produit[^ref] dont
je parlais les a consigné dans un vieux grimoire en expliquant comment il a transformé des histoires en produits. Voilà ce que le grimoire
révèle :

[^ref]: En fait il s'agit d'une "storyboardiste" nommée [Emma Coats](https://twitter.com/lawnrocket).


*  Tu admires ce personnage plus pour son obstination que pour ses succès. *Note du responsable produit : Il faut constamment remettre l'ouvrage sur le métiers.*

* Tu dois garder à l'esprit ce qui intéresse ton public, pas ce que te fais plaisir à toi. Cela peut-être très différent. *Note du responsable produit : penser à utiliser des personas, à faire des enquêtes, à intégrer mes utilisateurs finaux.* 

*  Tu peux te lancer avec un sujet sans fil précis, avec des impressions, et quand tu as compris : tu réécris tout sous forme d'histoire. *Note du responsable produit : Pour mes équipes rien de mieux qu'une histoire.* 

* Il était une fois \_\_\_. Chaque jour, \_\_\_\_. Un jour \_\_\_\_\_. En raison de \_\_\_\_\_\_. A cause de \_\_\_\_\_\_. Et finalement \_\_\_\_\_. *Note du responsable produit : penser pourquoi, qui, quoi, comment.* 

* Simplifie. Focus. Combine les personnages. Evite les détours. Tu penses perdre de la valeur, mais cela te libère. *Note du responsable produit : se concentrer sur la valeur, prioriser par valeur.* 

* En quoi ton personnage est-il doué ? à l'aise avec ? Met le dans une situation opposée et observe le : comment s'en sort-il ? *Note du responsable produit : pense différemment !* 

* Connait la fin de ton histoire avant son milieu. Sérieusement. Finir c'est dur, commence par finir ! *Note du responsable produit : commencer par ce qui est essentiel à mon produit, le reste viendra après.* 

* Fini ton histoire, même si elle n'est pas parfaite. On n'est pas dans un monde idéal. Tu feras mieux la prochaine fois. *Note du responsable produit : il faut savoir finir. Pas de sur-qualité. On s'améliore en pratiquant.* 

* Quand tu es coincé, fait une liste de ce qui ne POURRAIT PAS arriver ensuite. Souvent tu vas y trouver comment débloquer la situation. *Note du responsable produit : met toi dans des situations pour penser différemment, des jeux par exemple.* 

* Met de côté les histoires que tu aimes. Ce que tu aimes en elles, c'est un reflet de toi même. Tu dois t'en rendre compte avant d'aller plus loin. *Note du responsable produit : Ah bon sang la mémoire du muscle !* 

* Met ton histoire sur le papier pour la rendre palpable. Tant qu'elle est dans ta tête tu ne la partageras avec personne. *Note du responsable produit : partage un produit le plus tôt possible, du concret, du fini le plus tôt possible.* 

* Oubli la première chose qui te vient à l'esprit. Et la 2nde, 3ème, 4ème, 5ème - évite les choses évidentes. Surprend toi toi-même. *Note du responsable produit : Le premier "comment" n'est pas forcément le bon. Réfléchis à plusieurs, challenge, cherche d'autres alternatives.* 

* Tes personnages ont des opinions fortes. La passivité, la neutralité peuvent te paraitre intéressantes, mais c'est un poison pour le public. *Note du responsable produit : favoriser l'effet "Waouh".* 

* Pourquoi dois-tu raconter CETTE histoire ? Quel feu brûle en toi de raconter cette histoire ? C'est le coeur du sujet. *Note du responsable produit : faire un elevator pitch, proposer une vision pour chaque produit. Pourquoi bon sang !* 

* Si tu étais ton personnage, dans cette situation, comment te sentirais-tu ? L'honnêteté rend crédible les situations les plus
incrédibles. *Note du responsable produit : Sois honnête avec ton produit. Es-tu fier de lui ? Mon niveau d'exigence est-il le bon ?* 

* Quels sont les enjeux ? Ton personnage est-il bien là pour eux ? Que se passe-t-il en cas d'échec ? Pense au pire. *Note du responsable produit : Se donner des garanties au plus vite sur les choses les plus importantes.* 

* Aucun travail n'est jamais gaspillé. Si cela ne marche pas, laisse tomber et continue. Les fruits de ce travail apparaitront plus tard. *Note du responsable produit : laisser tomber ce qui ne marche pas (au moins tu as appris cela).*

* Tu dois reconnaitre le moment où tu es bon, le moment où tu délayes. Une histoire c'est essayer, pas optimiser. *Note du responsable produit : Sois honnête avec ton produit. Es-tu fier de lui ? pas de compromis !*

* Les coïncidences qui mènent nos personnages dans les ennuis sont légions. Les coïncidences qui les en sortent : c'est tricher. *Note du responsable produit : L'imprévu est la règle. La loi de Murphy bat son plein. Ne te mens pas à toi même !* 

* Exercice: Prend des morceaux de film que tu n'aimes pas. Comment les réarranges-tu pour les aimer ? *Note du responsable produit : Les grands changements peuvent se cacher dans les détails de ton produit. Pense différemment.* 

* Tu dois t'identifier avec tes situations et tes personnages, ce n'est pas juste un jeu. Qu'est ce que tu ferais REELLEMENT à leurs places ? *Note du responsable produit : Encore. Les personas. Les utilisateurs finaux. Feedback. Tout le reste n'est que fabulation.* 

* Quelle est l'essence de ton histoire ? La façon la plus courte de la résumer ? Si tu sais cela, tu peux partir de là. *Note du responsable produit : Encore : la vision, l'elevator pitch. Ma fierté ?*



{{< image src="/images/2013/03/Shrek-book-05.jpg" title="Shrek" >}}

