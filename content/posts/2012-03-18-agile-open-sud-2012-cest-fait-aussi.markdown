---
date: 2012-03-18T00:00:00Z
slug: agile-open-sud-2012-cest-fait-aussi
title: Agile Open Sud 2012, c'est fait (aussi)
---

Malgré une bonne toux et des antibios dans les bagages j'ai eu le
plaisir de participer à la première de mouture de Agile Open Sud
(\#aosud) 2012. Bonne compagnie (\~20 personnes, mais pas une fille...
sujet de session), un hotel agréable en bord de mer avec une très bonne
cuisine (notamment la mousse/fondant chocolat du soir absolument
sublime), des sujets intéressants même si nous avons manqué de temps
pour creuser certains points.

{{< image src="/images/2012/03/aosud2012-1-300x197.jpg" title="Agile Open Sud" >}}


Quelques retours à chaud sur les activités auxquelles j'ai pu participer
:

## Les estimations sont-elles nécessaires ?

Surtout par le dialogue et processus qu'elles induisent. Elles
permettent la discussion, la confrontation (sens apaisé), elles
dévoilent des surprises. Peuvent permettre (ou pousser) à tous de
s'exprimer. Elles valident une convergence. Nous oblige à découper,
designer, envisager les travaux de façon plus fine. Elles permettent de
nous projeter au travers d'une planification. Mais elles ne constituent
pas un engagement, et ne doivent pas être prise à la lettre (enfin, au
chiffre), ni de façon isolée (triangulation, comparaison).

## Scrum & XP sont sur un bateau

Une discussion animée autour de nos débats vifs et amusants concernant
Scrum & XP. Même si chacun continu de défendre naturellement ses
aspirations nous sommes tous assez d'accord. Notre combat est ailleurs,
vers la dénaturation des méthodes agiles. Voilà, pas plus. Certains
voient dans Scrum un cheval de Troie qui permet l'introduction de
l'agilité, tous sont ok pour confirmer que même une introduction, ou une
application partielle c'est mieux que rien, une porte d'entrée, un point
de départ. Pour ma part j'apprécie particulièrement de taquiner mes
camarades sur ces sujets (ils me le rendent bien) car cela nous permet
(même au travers de rires) une constante remise en question, une
constante réflexion sans douleur.  A ce sujet je vous soumet ma
proposition de changement du manifeste : "Je préfère un logiciel
opérationnel plutôt qu'un long débat technique".

{{< image src="/images/2012/03/aosud2012-2-300x124.jpg" title="Agile Open Sud" >}}

## Le scrummaster est-il inutile ?

Théoriquement l'auto-organisation devrait mener à ce constat. Mais alors
est-ce motivant de s'enticher d'un rôle dont l'objectif est de devenir
inutile ? Le scrummaster veille à l'application de Scrum, à éliminer les
obstacles, à faciliter le travail et l'organisation de l'équipe, à
protéger l'équipe. Quand tous ces objectifs sont atteints que devient-il
(ou quand l'équipe est assez mature pour faire cela toute seule) ?
Qu'est ce que l'on constate sur le terrain : cela n'est jamais
complètement le cas (ou alors avec des équipes assez réduites). Donc
c'est assez rare. Quand c'est le cas et si c'est possible le scrummaster
peut devenir petit à petit un coach agile : il prend de la hauteur et de
la distance, il peut prendre une position de facilitateur au sein de sa
structure. Il peut aussi encadrer plusieurs équipes simultanément ou
faire un travail plus transverse en se regroupant avec plusieurs
Scrummasters. Mais c'est assez rare (qu'il devienne véritablement
inutile). Quand est-ce que cela se produit ? lors des phases de
releases. Lors des phases de re-constitution des équipes.

Pour conclure : le scrummaster est (très) utile. Il peut avec le temps
le devenir beaucoup moins ce qui est la marque de la réussite de son
action. Mais nous constatons que sur le terrain il ne disparait que très
rarement. Et qu'il difficile d'expliquer aux gens (ah les gens...) qui
veulent devenir scrummaster que l'objectif est l'effacement -en
partie-de leur job.

{{< image src="/images/2012/03/skull_roses_Boite.jpg" title="Skulls" >}}

## Skull & Roses

A l'instar des "Loup Garous de Tiercelieux". Nous avons fait une partie
de Skull & Roses, un petit jeu de bluff fort sympathique. Le jeu demande
de mentir, d'être fourbe, de piéger tous les autres participants, de
mentir encore, de jubiler lors de l'échec d'un autre. Bref un grand
moment de plaisir qui permet d'en savoir long sur certains. 2
enseignements : il faut comprendre la règle pour gagner, par inversion
anti-agile : le jeu montre les travers de l'individualisme.

## Agilité, rupture ou effet de mode ?

Gros débat qu'il est difficile de résumer (à chaud dimanche matin qui
plus est). Je vous livre en vrac : Il y a trop de méconnaissances des
valeurs, de la culture de l'agilité. C'est à dire qu'il y a un vrai
schisme entre l'application des pratiques et les valeurs et principes
qui en sont le fondement. Cependant en appliquant les pratiques (sans se
soucier des valeurs) on peut arriver à mieux comprendre celles-ci
(valeurs) et finalement les retrouver. Il y a une vraie frontière entre
l'application de l'agilité sans valeurs, sans culture et avec. Nous
craignons naturellement qu'il arrive à l'agilité ce qui est arrivé à
Lean (un détournement). Nous pensons même qu'il est peut-être déjà trop
 tard, et que le mot agilité est déjà perdu. Pour le retrouver
-peut-être- cela passe par un réel évangélisme, un bon enseignement
(nous soulignons le mal fait par certains apprentissage autour de
l'agilité, et nous soulignons aussi que de nombreuses approches "agiles"
pré-existent depuis longtemps dans l'informatique  et auraient pu être
plus soutenus : par l'enseignement, par l'apprentissage, etc.)

Pour soutenir notre effort il faut aussi ne pas hésiter à montrer son
adéquation avec les attentes du "business". Pour résumer par un exemple
un chouïa réducteur : le respect des gens les rend plus performants
(donc c'est bon pour le business). Ou encore, le déploiement de
pratiques techniques soutenues par l'agilité (intégration continue, tdd,
pair programming, etc.) rend les applications de meilleure qualité (donc
c'est bon pour le business).

Pour lutter contre cette dénaturation : nous préconisons de toujours
rappeler clairement les choses : "nous devions, ceci n'est pas agile",
etc. Mais naturellement cela ne veut pas dire que nous refusons le job
ou la participation : il faut du temps pour acquérir tout cela. Donc oui
nous vous accompagnons et nous ne ferons peut-être pas tout bien tout de
suite (ni jamais, c'est une destination, nous y tendons). Mais nous
signalons constamment les deviances. Si, avec du temps, nous voyons
qu'il n'est pas possible d'appliquer l'agilité il faut songer à arrêter
ou à quitter la mission (mais c'est à la fin d'un parcours). Par contre
il faut constamment garder sa neutralité, et rappeler les écarts avec la
cible.

Voilà il s'agit de mes souvenirs livrés à chaud, par mon prisme. Ne pas
oublier la petite séance de banjo & guitare avec Olivier. Un moment très
agréable.

{{< image src="/images/2012/03/aosud2012-3-300x132.jpg" title="AOSud" >}}

Seul regret : c'est trop court. Et je n'ai pas réussi à formaliser assez
les idées qui ont émergées (je ne parle pas de compte rendu, je veux
dire : sur les lieux). Mon avis : prendre une vraie retraite de 4 jours
(dans les Cevennes ou en Tunisie).

Le retour de [Thierry](http://thierrycros.net/?post/2012/03/18/Agile-Open-Sud-2012-%3A-c-est-fait)

Le retour de [Claude](http://www.aubryconseil.com/post/Agile-Open-Sud-c-etait-bien)

Le retour de [Jean-Baptiste](http://www.arpinum.fr/2012/03/18/agile-open-sud/)

Le retour de [Fabrice](http://agilarium.blogspot.fr/2012/03/agile-open-sud-2012.html)

Le retour de [Rui](http://www.rui.fr/event/conf-agile-open-suddone/2012/03/19/)

Le retour d'[Alexis](http://ayeba.fr/2012/03/agile-open-sud-2012/)

Le retour de [Jérôme](http://blog.avoustin.com/agilite-banyuls-rugby-aosud/)
