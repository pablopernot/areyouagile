﻿---
date: 2019-03-02
slug: temporalite-du-management
title: Temporalité du management
--- 

Quand on s'interroge comme souvent actuellement sur la posture managériale à avoir de nos jours, sur le fait de donner du sens et un cadre, en lieu et place d'un management *command & control*, il est important de saisir que la temporalité de ce management change aussi. Je veux dire par là qu'un management *command & control* est un management de cause à effet, je te demande cela, tu fais cela, bien ou mal, mais la relation est direct et le temps est court, ou du moins on comprend le temps que cela va prendre. Quand on pose un cadre et que l'on donne du sens, pour faire émerger les choses, ou pour laisser les équipes s'auto-organiser, on entre dans un système, un [système adaptatif complexe](https://fr.wikipedia.org/wiki/Syst%C3%A8me_complexe_adaptatif). Il n'y a plus vraiment de causalité directe. Il s'agit de deux temporalités très différentes.

Quand on fait du management moderne, l'effort se doit d'être constant, et les résultats seront inconstants. C'est compliqué à vivre pour des gens habitués à système de cause à effet, qui ont l'habitude de demander des choses et de les obtenir (ou pas), ou pour des héros qui ont l'habitude de faire des choses et d'obtenir des résultats (ou pas).  

Pour faire un effort de management constant, il est important d'en connaître le cadre et le sens (la boucle est bouclée, car le but de ce management est de véhiculer ce cadre et ce sens). 

Faire un effort de management constant signifie que le rythme est constant. Il est inutile d'avoir des "coups de bourre". Vous pouvez avoir des "coups de bourre" sur des actions locales, temporaires : des fins de cycle, des événements, mais vous ne pourrez pas avoir de coup de bourre sur votre culture, l'image que vous donnez de vous à vos candidats, au *refactoring* de votre code, etc. 

Cet effort-là devra être régulier et continu.  

Les effets seront souvent irréguliers et souvent imprévisibles (mais ils iront dans le sens voulu et en respectant le cadre donné).  

Tout cela il faut l'avoir en tête. 

C'est difficile de faire des efforts constants dont les résultats se révèlent inconstants. 

Mais c'est aussi jubilatoire de voir ses efforts payer à un moment souvent inattendu. 

  
  

---

D'autres articles sur le management de 2018 :

* [Management d'hier et d'aujourd'hui](/2018/12/management-hier-aujourdhui/)   
* [Je dis non](/2018/11/je-dis-non/)
* [Rupture managériale](/2018/09/la-rupture-manageriale/)
* [Dirigeants est-ce que vous signalez ce que vous désirez ?](/2018/07/est-ce-que-vous-signalez-ce-que-vous-desirez/)
* [Convaincre où se laisser convaincre](/2018/03/convaincre-ou-se-laisser-convaincre/)
* [Hôte d'écosystème](/2018/01/hote-ecosysteme/)