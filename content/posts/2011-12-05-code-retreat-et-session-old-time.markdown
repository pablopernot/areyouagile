---
date: 2011-12-05T00:00:00Z
slug: code-retreat-et-session-old-time
title: Code retreat et session old time
---

Faire l'analogie entre nos pratiques agiles et la musique est chose
courante. La métaphore est évidente et peut-être utilisée dans de
nombreux cas. J'ajouterai donc ma pierre à cet édifice en mettant en
parallèle les pratiques de *coding dojo*, *coding retreat*, etc de *XP*
(que certains appeleront *craftsmanship* pour lui donner une nouvelle
vigueur) avec les règles et étiquette d'une session old time ([musique
old time](http://fr.wikipedia.org/wiki/Old-time_music)), l'un de mes
dada à moi. Le second pouvant être source d'inspiration pour les
premiers. En effet c'est en discutant avec Thierry (@thierrycros) de mes
pratiques *old time* la veille du \*global code retreat\* (du 3 décembre
2011) que certaines évidences se sont crystalisées.

{{< image src="/images/2011/12/jam.jpg" title="Jam" >}}

Dans une session old time, une "tune" (un titre) dure entre 10 et
20mn...c'est long. Cette musique est en plus très répétitive (sorte de
*trance*). Mais du coup on apprend le morceau pendant que les autres le
jouent. C'est une montée en compétence par la pratique au sein du groupe
(on avoisine entre 6 et 20 personnes dans une session, voire plus...).
Si on ne connait pas un morceau, les 5 premières minutes on va écouter.
Les 5 minutes suivantes on essaye de jouer les bases du morceau. Avec un
peu de talent on peut commencer à enrichir notre prestation les minutes
suivantes. A la fin du morceau, on le connait, et on est prêt à le
transmettre à d'autres un autre jour. On est proche de l'idée de
compagnonnage (" réseau de transmission des savoirs et des identités par
le métier ", même si ce nom peut en braquer certains) portée par
*craftsmanship* ou *XP*. En tous cas ces groupement de personnes qui
mèlent apprentissage et plaisir qu'il s'agisse d'une session\* old
time\* ou d'un *code retreat* me semblent très proche (et il doit y
avoir de nombreux autres exemples...mais bon mon dada moi c'est le old
time...).

les sessions old time se pratiquent depuis le XVIIIème siècle.

Voyons donc si nos règles (de "old timey") pour les sessions pourraient
vous inspirer pour vos *code retreat* (je ne code pas assez pour
m'inclure):

(il y a de nombreuses règles de sessions old time mais elles se
ressemblent toutes plus ou moins, je me suis donc basé sur celles -10-du
site["old time seattle"](http://www.oldtimeseattle.com/jams.html)).

1.  Tout le monde, peut importe sa compétence musicale, est invité et
    encouragé à rejoindre le cercle de la session, mais s'il vous plait
    essayez de vous entrainer et d'avoir pratiquer avant. ("Every one,
    regardless of musical ability, is invited and encouraged to join the
    jam circle, but please try to come practiced & prepared").
2.  Gardez un oeil ouvert (pour voir les nouveaux entrants ou faire de
    la place pour), laissez de la place que les nouveaux entrants se
    sentent à l'aise pour entrer dans le cercle à n'importe quel
    moment. ("Keep your eye out - make room for new players so they can
    enter the circle at any time and feel welcome to do so").
3.  Un instrument est trop présent ? Laissez votre place durant quelques
    morceaux et proposez à un quelqu'un autour du cercle de venir
    prendre votre place (il est aussi possible de faire un deuxième
    cercle, à valider avec le meneur de la session).("Too many of the
    same instruments? Take turns by leaving the circle after playing a
    few tunes and encourage a sideliner to take your place. Sometimes
    it's possible to make a second circle, check with the jam leader".)
4.  Si vous n'êtes pas familié avec un morceau. Ne jouez pas, mais
    écoutez le quelques minutes puis commencez doucement à le jouer
    jusqu'à que vous vous sentiez à l'aise. ("If you are unfamiliar with
    a tune, DON'T PLAY AND JUST LISTEN a few times through, then play
    along quietly until your sure you've got it").
5.  Faites attention à votre volume, soyez sur que la personne qui mène
    le morceau est entendue par tous. De même si vous menez un morceau
    soyez sur que chacun vous entende. ("Pay attention to your volume,
    make certain the person leading the tune can be heard by everyone.
    Likewise, if you are leading a tune, try to play loudly").
6.  Soyez sûr d'être bien accordé. Utilisez un accordeur électronique si
    besoin et vérifiez fréquemment. ("Make sure you're in tune. Use an
    electronic tuner & check yourself frequently").
7.  Quand vous menez un morceau annoncez la tonalité au préalable. Et
    annoncez la grille si le morceau est méconnu. Si le joueur à vos
    côtés ne connait pas le morceau prenez le temps de le renseigner
    tranquillement ("When leading a tune, announce the key before
    starting each tune or song. And announce the chords if the tune is
    more obscure. If a the player next to you does not know the chords
    and you do: offer to tell them quietly").
8.  Les sessions sont une grande expérience d'apprentissage mais ne sont
    pas des leçons gratuites. Si vous avez besoin de beaucoup
    d'informations n'hésitez pas à demander à la ronde si quelqu'un
    donne des cours privés ou demandez quels disques écouter pour
    progresser. ("Jams are a great learning experience, but are not
    meant to be freebie music lessons. If you're looking for a lot of
    pointers, feel free to ask around to find others that offer private
    lessons, or ask for recommended recordings to listen to at home").
9.  Concernant les danseurs : ils sont encouragés à rejoindre la
    session. Merci cependant d'aider les musiciens en attendant que le
    morceau joué est assez en place pour lancer la danse. Merci aussi de
    vous coordonner entre vous afin d'éviter trop de danseurs (ce qui
    génère une cacophonie terrible). ("Dancer etiquette: Step-dancers
    are encouraged to come to jams! Please help out the musicians by
    waiting to dance until the tune is played a few times through the
    form and is being well-grasped by the majority of the musicians.
    Also, coordinate taking turns if there are multiple dancers, as too
    many feet gets awfully cacophonic (you can take turns on the same
    tune).")"
10. Si la session se déroule au boulot, faites en un évènement ludique !
    Achetez des boissons ou des encas !, invitez vos amis à venir
    écouter, essayez d'être bon : meilleure sera la session le plus de
    personnes reviendront et nous aurons de plus en plus de support à ce
    que nous faisons. ("If the jam is at a business, keep the business
    happy! Buy a beverage/snack & tip the staff (alcoholic-free drinks
    are available at bars if you don't drink alcohol), invite your
    friends to come listen, & try to play your best as the better the
    jam sound the more people will listen and gain an interest and
    support what you're doing!")

Personnellement j'y vois beaucoup d'échos avec les *code retreat* ou
autre : apprentissage par/avec le groupe, respect des personnes et des
"préséances", ouverture aux autres et accueil, "team building", etc. on
pourrait s'amuser donc à extrapoler ces règles aux *code retreat* :

{{< image src="/images/2011/12/coderetreat.jpg" title="CodeJam" >}}

1.  Tout le monde est invité et encouragé à participer au *code
    retreat*peut importe sa compétence, mais il est opportun de
    pratiquer et de s'entrainer un peu avant.
2.  Gardez un oeil ouvert et laissez de la place pour les retardataires
    ou nouveaux arrivants.
3.  Un langage est trop présent ? On le laisse de côté durant quelques
    itérations.
4.  Je ne suis pas familié avec un langage ? J'observe d'abord et je
    commence à pratiquer doucement au bout de quelques moments
    seulement.
5.  Attention de bien laisser chacun s'exprimer et de ne pas monopoliser
    la parole.
6.  Votre code compile bien, vérifiez électroniquement et régulièrement.
7.  Si votre exercice est complexe annoncez des pistes et accompagnez
    les gens qui sont perdus.
8.  Les *code retreat* sont une grande expérience d'apprentissage mais
    pas des leçons gratuites. N'hésitez pas à questionner certains pour
    des leçons privées de code ou des listes de bouquins et références.
9.  Des profils plus fonctionnels sont encouragés à rejoindre le "code
    retreat" mais ils ne doivent pas le submerger. Le code reste
    l'élément central.
10. Si le*code retreat* se déroule au boulot, égayez le en amenant
    boissons & encas.

Voilà, à vos fiddles, java, banjos, ruby, contrebasses, python,
guitares, c\# et claviers.
