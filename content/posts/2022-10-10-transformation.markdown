---
date: 2022-10-10
slug: pensez-vous-transformation 
title: "Pensez-vous transformation ?"
draft: false
---

Pensez-vous transformation ? C'est ce que l'on m'a encore dit récemment : nous souhaitons nous transformer et avoir une approche produit. Je l'entends souvent. 
C'est normal. C'est mon métier. Je vais redire, encore un peu différemment, encore un peu comme d'habitude, ce a quoi je crois, mon expérience. C'est probablement une part de mon rôle : répéter, rappeler, répéter. Cette fois-ci j'ai rapidement fait 27 phrases / slides. Phrases sur lesquelles j'amène du contexte et du contenu. Si vous désirez en parler avec moi faîtes moi signe : je n'en ai pas marre, se répéter est normal, je n'amène que 50% des réponses, l'autre partie est dans vos mains. Voici 28 phrases / slides au sujet des transformations : 

### 1. Les organisations dont les transformations réussissent ne parlent pas de transformation, mais de ce qu'elles réussissent en s'étant transformées. 

### 2. Vous dîtes "transformation». Je comprends que vous êtes au pied d'un potentiel changement de culture, car on se transforme constamment.

### 3. Ce changement de culture, ces "transformations", viennent souvent du besoin d'évoluer bien dans un monde complexe.

### 4. Pour réussir dans un monde complexe : de l'autonomie avec des règles claires, un objectif clair, du feedback (entre autres).

### 5. Autonomie n'est pas autarcie, ni absence de responsabilisation.

### 6. L’autonomie génère de l’engagement.

### 7. Le monde complexe évolue constamment. Pour s’y adapter et se sécuriser (et permettre l’autonomie) on travaille sur des petits morceaux.

### 8. Pour que cela soit efficace : on fabrique des petites choses finies, qui portent du sens. Cela implique une organisation souvent différente qui puisse réaliser ceci.

### 9. Si on est capable de découper des petits morceaux qui portent du sens, autant les délivrer priorisés par valeur. 

### 10. Délivrer des choses morceau par morceau, on va vite, prioriser par valeur : on mesure l'impact de nos hypothèses, on continue, on arrête, on adapte. C'est ça la promesse de l'agilité. En tous cas celle de l'approche produit. 

### 11. Vous pourriez avoir un autre bénéfice : si vous donnez de l'autonomie, un cadre clair, des règles claires, du feedback, et ce travail par petits éléments : vous rendez votre organisation plus résiliente et souple. 

### 12. La difficulté pour le management : lâcher prise sur le comment, le micromanagement, et savoir créer un cadre, une direction.

### 13. La difficulté pour les métiers : savoir découper, savoir fabriquer morceau par morceau avec une vision : comme une termitière. 

### 14. La difficulté pour les équipes techniques : le flux et l'automatisation (notamment des tests).

### 15. La difficulté pour tous : s’orienter valeur, les mesures sont orientées vers l’impact, la valeur apportée. 

### 16. La mise à l'échelle demande un effort de synchronisation (quarterly planning, etc) et de vision commune (qui pense le tout, définir une "cathédrale"). 

### 17. La mise à l'échelle demande une capacité de découpage des équipes et du produit qui limite au maximum les dépendances, mais permette de délivrer des choses finies avec de la valeur.

### 18. La mise à l'échelle sans trop endommager l'autonomie : des équipes composants deviennent des équipes fonctionnalités, puis deviennent des équipes impact.

### 19. Dans la mise à l'échelle l'illusion de l'absence de contraintes physiques est dangereuse : taille maximum des équipes, des dépendances (encore), maximum de personnes dans une conversation, maximum d'équipes en parallèle autour d'un sujet proche, etc.  

### 20. La mise à l'échelle va demander une modularisation de la couche technique pour aider à minimiser les dépendances et renforcer l'autonomie. 

### 21. La mise à l'échelle va interroger sur le schéma de transformation : "par appartement", en "big bang" ?

### 22. Une transformation : vous devez suspendre vos croyances. Faire des expérimentations, vivre des choses. 

### 23. Une transformation : n'appliquez pas de recette de cuisine ou de faux-semblants. 

### 24. « People copy the most visible, obvious, and frequently least important practices » ([Pfeffer & Sutton, Three myths of management 2006](https://hbswk.hbs.edu/archive/three-myths-of-management))

### 25. [Premier regard](/2022/09/organisation-vertueuse-ou-calamiteuse/) : Est-ce une entreprise politique ou qui porte du sens ? Une transformation pour quoi ? Est-ce que l’on parle valeur ?

### 26. [Premier regard](/2022/09/organisation-vertueuse-ou-calamiteuse/) : Où se prennent les décisions ?

### 27. [Premier regard](/2022/09/organisation-vertueuse-ou-calamiteuse/) : Enfin pense-t-on le tout ?

Merci 
