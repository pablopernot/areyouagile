﻿---
date: 2018-11-28
slug: dynamic-kanban
title: "Dynamic Kanban"
--- 

Hier lors de [Flowcon](http://flowcon.fr/), la *con* férence sur le *flow*, j'ai pu, avec l'aide de [Yannick](https://twitter.com/ygrenzinger), présenter une session dont l'objectif est de faire comprendre la lecture contextuelle pour adapter les systèmes kanban à la culture dans lesquelles ils peuvent évoluer. 

Pour les gens qui ont pu participer, voici les éléments que nous avons abordés. Naturellement attention pour ceux qui voudraient participer à l'avenir mieux vaut ne pas lire les propositions suggérées en fin de session.

Cette perspective sur l'utilisation des systèmes kanban au milieu de leurs écosystèmes fait partie d'un livre sur Kanban écrit initialement seulement par Laurent Morisseau et que je rejoins dans cette troisième édition prévue pour sortir au printemps 2019.       

## Session "Dynamic Kanban" donc 

Sûrement pas le meilleur des titres, mais l'idée est de mêler une grille de lecture comme la Spirale Dynamique appliquée aux systèmes kanban. Mêler ? J'ai pu observer que souvent au début d'un parcours de coach on essaye d'appliquer une méthode. L'étape suivante est que l'on devient dangereux, car en maîtrisant cette méthode on l'applique sans discernement, telle quelle. Puis en évoluant on comprend que les "choses" ne peuvent se limiter à une méthode, mais -- et c'est un autre danger -- on essaye d'accompagner ou de "transformer" les autres comme nous nous sommes transformés. Enfin on comprend qu'accompagner les organisations c'est les "transformer" comme elles devraient se transformer.  

**Ne changeons pas les organisations comme nous avons vécu notre parcours. Aidons les organisations à changer comme elles en ont besoin.**   

Afin de répondre à ce besoin, mieux comprendre et adapter nos systèmes kanban, nous proposons d'utiliser une grille de lecture comme la Spirale Dynamique. 

### Présentation des trois contextes. 

La session démarre avec la présentation de trois contextes, et de trois "systèmes kanban". Il s'agit de contextes fictifs. Mais il est intéressant de s'interroger si ces contextes rappellent des situations, des écosystèmes vécus par soi-même.  

### La très grosse compagnie

Ici un département de la très grosse compagnie. C'est une organisation fortement basée sur des processus. Avec des rôles pour chacun clairement délimités ou les "castes sociales" n'évoluent guère car c'est ainsi que vont les choses et que les choses fonctionnent. Mais face aux chamboulements du monde qui les entoure, et la montée accrue de la concurrence, un groupe de dirigeants dans l'un des départements de la grosse compagnie, pousse pour transformer l'outil de travail. Transformer l'outil de travail pour regagner des marchés, indispensable à leurs yeux sous peine de voir s'effondrer ce géant aux pieds d'argile. 

Ils se lancent dans l'utilisation de systèmes kanban. Pourquoi ? Pour avoir des leviers qui leur permettent de transformer, d'améliorer, de changer leur outil de travail rongé par la bureaucratie et les habitudes. Il s'agit de rendre fonctionnel quelque chose de dysfonctionnel. Ils souhaitent une adhérence plus forte au marché qui est en train de changer. On essaye de rendre son outil de production plus performant. Un travail sur la fluidité de l'outil de travail : de bout en bout. 

{{< image src="/images/2018/11/verybigcompany-1.jpg" title="un département de la très grosse compagnie" >}}

### La jeune pousse pleine d'envie et d'ambition.

Ici une *startup*, pleine d'envie et d'ambition. Il s'agit d'un petit groupe de dix personnes, affûté. Leur idée de produit semble intéresser beaucoup de gens. En tous cas beaucoup de gens veulent miser sur eux. L'ennemi c'est le temps. Il presse. Entre les marques d'intérêt et la validation du marché, il y a un espace immense qu'il faut franchir. 

Le groupe fondateur est composé d'entrepreneurs en soif de réussite, mais aussi de personnes à la recherche de sens, avec des valeurs humanistes. Ils veulent créer de la valeur. Peu importe la fluidité de leur outil de travail, pas de processus, ni de véritable outil au sens propre, mais plus un commando qui doit valider son modèle, valider ses hypothèses et mesurer constamment la réception par le marché. Des *impact mapping*, des tests A/B, etc. sont les pratiques cibles. Mais cela génère des tensions. Une *startup* qui réussit, oui, mais pas à n'importe quel prix, pas uniquement pour une réussite financière.  

{{< image src="/images/2018/11/startup-1.jpg" title="la jeune pousse" >}}

### Le groupe familial avec ses secrets. 

Ici une PME. Autour de la famille, l'entreprise a bien évolué. Mais elle se heurte comme à une crise de croissance. On ne sait plus si l'organisation va bien ou mal. On a beaucoup de travail, mais tout semble se faire dans la douleur. Mais c'est ainsi, et cela semble toujours avoir été ainsi. Et l'entreprise vit bien depuis longtemps. D'ailleurs c'est un moment important qu'elle vit. Le fondateur part enfin à la retraite et l'un de ses fils prend la suite. Pour tout le monde, tout va bien ou mal, mais rien ne va changer. 

Le nouveau PDG, le fils connaît bien la tradition autour de la famille, qui se nourrit d'exploits et d'accès de rage. Il y a des sauveurs qui se tuent au travail pour la famille, pour l'entreprise. Et une équipe dirigeante, qui se connaît par cœur et qui domine ses troupes. Mais le nouveau PDG comprend que ces attitudes vont user l'entreprise et qu'elles ne mèneront à rien. 

{{< image src="/images/2018/11/famille-1.jpg" title="La famille" >}}

## Une série de phrases pour s'immerger dans les différentes façons de penser le monde qui nous entoure. 

Nous distribuons ensuite une série de phrases, et nous demandons à chaque groupe de s'en emparer et d'essayer de les attribuer à ces contextes, où seraient-elles prononcées ? Nous prévenons que toutes les phrases ne se retrouvent pas forcément dans ces trois contextes. l'idée est de bien s'immerger dans ces contextes et de commencer à s'emparer de postures, d'un lexique, d'une façon de penser le monde qui nous entoure.   

[ >> Fichier des phrases (pdf)](/pdf/dynamic-kanban-phrases.pdf)

(L'idée est de les découper pour que les gens -- table de 8 max -- puissent les manipuler aisément). 

{{< image src="/images/2018/11/dynamic-kanban-phrases.jpg" title="Phrases" >}}

## Présentation de la Spirale Dynamique

Avec ce cadre posé : 3 contextes, des phrases à s'approprier (nous ne donnons pas encore de réponse concernant les liaisons contextes / phrases), l'étape suivante est de s'emparer d'une grille de lecture, que Yannick comme moi-même aimons beaucoup, la Spirale Dynamique. 

[La présentation de Yannick chez slideshare](https://fr.slideshare.net/yannickgrenzinger/spirale-dynamique-et-organisations)

{{< image src="/images/2018/11/dynamic-kanban-spirale-dynamique.jpg" title="Spirale Dynamique - niveau rouge" >}}

Suite à la présentation de Yannick nous distribuons un petit récapitulatif qui va à nouveau enrichir la compréhension de cette grille de lecture : 

[>> Fiches couleurs Spirale Dynamique (pdf)](/pdf/dynamic-kanban-fiches-couleurs.pdf)

{{< image src="/images/2018/11/dynamic-kanban-fiches-couleurs.jpg" title="Fiches couleurs Spirale Dynamique" >}}

---

**ATTENTION À PARTIR DE MAINTENANT SI VOUS LISEZ LA SUITE L'ATELIER PERDRA DE LA SAVEUR POUR VOUS SI VOUS COMPTEZ Y PARTICIPER** 

(C'est dans les moments qui arrivent ensuite que les briques s'assemblent et qu'une petite épiphanie peut se produire). 

---

## Compréhension des contextes et nouvelle interrogation autour des phrases au regard de la Spirale Dynamique

La grille de lecture offerte par la Spirale Dynamique permet de mieux comprendre les contextes, et de s'approprier aussi différemment les phrases (à part dans notre tête, la concordance phrases/couleurs n'est pas encore formalisée, elle va venir si les forces mystiques qui nous entourent le veulent bien). 

On laisse du temps aux participants pour s'approprier à nouveau les contextes et les phrases, l'idée étant de détecter les couleurs de la Spirale Dynamique qu'il faudrait leur associer. Puis nous révélons notre analyse.  

Dans notre lecture **la très grosse compagnie se révèle un contexte BLEU/orange** : c'est à dire une forte base BLEU[^1] au sein des équipes et des poches de management ORANGE de-ci de-là. L'objectif est de rendre fonctionnelle une organisation BLEU/orange dysfonctionnelle. 

Dans notre lecture **la jeune pousse se révèle un contexte paradoxal qui associe du VERT et du ORANGE**. Il faut réussir à réconcilier ces deux visions dans ce contexte. 

Dans notre lecture **le groupe familial se révèle un contexte majoritairement ROUGE avec un fond de VIOLET, mais son nouveau dirigeant souhaite l'entraîner vers du BLEU pour le pacifier un peu**.

[^1]: pas d'accord pour respecter la terminologie de la Spirale Dynamique.   

À la fin de cette partie, nous clarifions les contextes comme évoqués ci-dessus, et nous associons les phrases aux couleurs. Chaque groupe a donc maintenant un contexte (des conditions de vie pour employer une expression de la Spirale Dynamique), un système kanban, la description d'un environnement. 

## Proposition d'amélioration des systèmes kanban selon les contextes et la compréhension de la Spirale Dynamique

Enfin, nous donnons du temps à chaque groupe pour proposer une avancée, une adaptation, une amélioration de chaque système kanban en fonction de cette lecture. Puis nous proposons nos hypothèses :  

### Proposition amélioration : La très grosse compagnie. 

#### Sur le système Kanban 

* Accepter les incohérences entre les processus officiels et ce qui apparaît (protection par les managers, incarnation de l'effort par les managers).
* Renforcer les priorités au-delà des processus "cassés ou obsolètes" : faire apparaître clairement les priorités (ligne "urgence", "macaron urgence"). 
* Bien rendre visibles et accessibles ces systèmes kanban (éviter de les enterrer dans un outil électronique uniquement). 
* Essayer de réduire la taille des éléments pour gagner en souplesse. 
* S'efforcer à cadencer les livraisons en ritualisant celles-ci (indiquer sur le système kanban livraison tous les 3 mois par exemple), ou sur les cartes kanban indiquer la livraison cible. 
* Mettre en évidence (colonnes ?) une volonté d'automatiser packaging, et tests automatisés d'intégration, ou avant même d'automatiser d'en  avoir. 
* Rendre visible là où les processus s'enlisent : mettre en évidence les "buffers". 

#### Hors du management visuel 

* Communiquer de façon traditionnelle et "corporate" sur les systèmes kanban à venir. 
* Par exemple, diffuser un guide, un référentiel, quelques retours d'expériences d'autres compagnies, des formations.
* Par exemple, Communication régulière et lors des grandes messes par les managers. 
* Soutient par les managers d'une possible J-Curve dans le renforcement de la cadence, d'une CI/CD. 

{{< image src="/images/2018/11/verybigcompany-2.jpg" title="un département de la très grosse compagnie" >}}

### Proposition amélioration : La jeune pousse pleine d'envie et d'ambition.

#### Sur le système Kanban 

* Intégrer des colonnes de fin "impact / mesure" pour mettre en évidence la véritable valeur (et pas "production", ou "livré" ou "fini"). 
* Mettre en évidence du sens dans les "swim lanes" (lignes horizontales, en rapport avec les fonctionnalités du produit) pour satisfaire cette quête chez ses personnes de raison d'être (ou sur les cartes kanban), ou aussi une mesure "sens/valeur" en intitulé de la colonne finale.   
* Mettre en place un flux tiré (plus respectueux des personnes, d'un esprit de groupe, que top down flux poussé). 
* Introduire les nouveaux éléments dans une colonne dédiée (et qui viendrait de la création d'un mode de fonctionnement type sociocratie)

#### Hors du management visuel

* Créer des instances de fonctionnement type [sociocratie/holacratie](/2017/12/petite-serie-sur-holacratie-et-sociocratie/). 

{{< image src="/images/2018/11/startup-2.jpg" title="la startup" >}}

### Proposition amélioration : Le groupe familial avec ses secrets.


#### Sur le système Kanban 

* Ne pas cacher les systèmes kanban.  
* Ne pas personnaliser les "swim lanes" ou les cartes, promouvoir un travail de groupe (mettre des noms est une mauvaise pratique pour les "swim lanes", on peut mettre les noms sur les cartes kanban plutôt). 
* Clarifier les processus (en-tête colonnes systèmes kanban), règles d'entrée et de sortie.
* Renforcer les processus (bien rendre visible les "buffers" des étapes de validation).  
* Créer une ligne urgence et la limiter ? 

#### Hors du management visuel

* Introduire plus de sérénité : des processus, des règles. 


{{< image src="/images/2018/11/famille-2.jpg" title="la famille" >}}

## Conclusion

L'idée est de montrer que les systèmes kanban vivent dans un écosystème que l'on ne peut ignorer. 

L'idée est de bien comprendre la demande de l'organisation et son contexte. Dans la Spirale Dynamique on parle d'organisations arrêtées, ouvertes ou fermées. Les organisations ouvertes sont prêtes à l'amélioration et au changement, celles arrêtées sont prêtes à changer ou à s'améliorer, mais quelque chose les arrête. Celles fermées ne sont pas prêtes au changement ou à l'amélioration. Il y aura *grosso modo* deux types de changements : l'un est disruptif, réforme, révolution, l'autre est incrémental, on s'améliore. Pour les organisations fermées, aucun n'est possible. Pour les organisations arrêtées, on pourrait s'améliorer, aborder un changement incrémental (rendre fonctionnel quelque chose de dysfonctionnel par exemple). Pour les organisations ouvertes, tous les types de changements sont envisageables.   

Enfin avoir des grilles de lecture permet d'avoir un dialogue constructif (Spirale dynamique, Agile Fluency, etc.).  

Vous retrouverez aussi tout cela et plus dans la troisième édition de Kanban (chez Dunod) au printemps 2019 :)
