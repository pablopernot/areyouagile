---
date: 2021-01-03
slug: difficile-fin-de-carriere-pour-scrum
title: "Difficile fin de carrière pour Scrum"
--- 

C'est l'histoire d'un groupe de rock. Un duo. Un vrai duo rock. Ils n'ont pas fait Woodstock, ils n'ont pas été roadies de Mötorhead, mais l'un d'eux a été pilote pendant la guerre du Vietnam. Dans les années 90 quand on pense que le rock est mort il lance "Scrum", leur groupe.  Il faut du temps pour percer. Au tout début des années 2000 pendant un festival de musique underground un mouvement plus global dont ils font partie prend forme. C'est un tremplin pour eux. Le milieu des années 2000 voit les grands groupes devenir des vrais dinosaures. À l'instar du punk des années 70, du Grunge des années 90, ils sont le fer de lance de la rupture au milieu des années 2000. De underground ils deviennent les têtes d'affiche. 

Ça doit leur tourner la tête, certainement grisant. Ce groupe a 10, 15 ans d'existence et il atteint enfin son apogée. Les dix années à venir seront leurs années d'or. En 2006, chez Google, Ken (Sutherland) décline ses gammes avec l'impudence d'un jeune Elvis[^ken] : 

	Chaque équipe a son scrummaster, aussi connu sous le nom de l'emmerdeur ou l'empêcheur de tourner en rond. Le rôle de cette personne est de s'assurer que vous ne remettez pas en cause la qualité. Ils n'ont pas d'autorité, mais si nous avons décidé que ce que nous avons fabriqué dans la période doit avoir un certain niveau de qualité pour être passé en revue, leur job est de s'assurer que ce niveau de qualité est bien là. Et si le niveau de qualité n'est pas le bon, ils peuvent vous empêcher de le passer en revue, et dire au *product owner* : "désolé on ne remplit pas les conditions attendues. Nous avons besoin de plus de temps, et prenons donc ce temps". Cette personne est probablement la personne la moins aimée, car elle se trouve au croisement entre le management du produit qui croient que l'on peut faire autant de choses que l'on souhaite et notre bonne volonté de nous mettre nous-mêmes en péril en ne respectant pas la qualité pour satisfaire cette croyance. -- Ken Schwaber 2006  

[^ken]: "Every team has a Scrum Master, also known as the prick. The role of this person is to make sure you don't cut quality. They don't have authority, but what they can do is if we define that an increment has a certain level of quality for it to be demonstrated to our product management, their job is to make sure that quality is there. And if the quality isn't, not to let you demonstrate it, but instead say to the product manager we're not done. It takes us more time to finish this, and let it bubble up that way... This person is probably the least loved person because they stand right at the nexus between product management believing any amount of stuff can be done and our willingness to cut quality to help them support that belief." -- Google conference 2006 : https://www.youtube.com/watch?v=IyNPeTn8fpo

Ça rock, c'est disruptif. Ce rôle magique -- et très difficile à jouer (rappelez-vous vous êtes le "connard", traduction plus directe encore de "the prick"[^prick]) -- est l'un de ses piliers. La fluidification et l'équilibre entre le désir du produit et sa fabrication sont assurés par sa neutralité : **they don't have autority** (ils n'ont pas d'autorité). C'est une rupture avec les anciens dinosaures. Et surtout ça marche. Mais c'est difficile à jouer.  

[^prick]: La traduction littérale est la piqûre, en argot, la bite.  

La décennie 2010 commence donc bien pour notre duo Sutherland/Schwaber. Mais d'autres s’engouffrent dans la brèche ouverte par ce mouvement. Il y a toujours des producteurs véreux qui profitent des mouvements, fabriquent des groupes factices et lancent des disques en appuyant sur les *gimmick* (ils miment, mais ils ne jouent pas vraiment, comme *guitar heros* sur les stations de jeux). 

Guitar Hero ? Air Guitar ?[^sutton] 

	"People copy the most visible, obvious, and frequently least important practices" – Three Myths of Management, Pfeffer & Sutton.

[^sutton]: Three Myths of Management: https://hbswk.hbs.edu/archive/three-myths-of-management 

Et souvent ils raflent la mise commerciale. On les oubliera vite, ils n'apporteront rien à la musique, mais pendant un temps ils prendront la tune. On s'assure qu'ils rassurent les parents, le rock devient acceptable. 

Pour le duo c'est le début de la fin ils voient partir le fruit du succès dans d'autres mains et malheureusement oublie leur musique pour l'appât du gain. C'est le début du *merchandising* à outrance : certifications. La forme commence à prendre l'ascendance sur le fond. Comme beaucoup de groupes de rock, pour essayer de retrouver leur éclat des années 2000, ils se mettent à se parodier eux-mêmes et ne se renouvellent plus. 

Et puis vient 2020 qui sonne le glas du duo. Comme malheureusement beaucoup de groupes, l’appât du gain semble être devenu plus important que sa musique. On est prêt à sacrifier n'importe quoi pour de nouveau avoir du succès, être sous les feux de la rampe. Même si cela veut dire sortir un dernier album qui fera honte à toute la discographie. Sortir un tube sirupeux qui décevra les fans de la première heure, donnera raison aux groupes restés *underground* et qui ne seront jamais *mainstream* (XP par exemple). 

C'est ce que finalement le duo décide de faire : jouer l'éphémère et ce qui plaît au public, mais qui n'a pas de fond. En 2020 le *scrummaster*, *the prick* est devenu "le responsable de l'efficacité de l'équipe". En une phrase Scrum est devenu aussi obsolète que les méthodes qu'il avait lui-même rendues obsolètes. 

En quinze années on est passé de : *vous n'êtes responsable de rien, mais vous vous assurez --quitte à vous faire haïr de tous--  du bon respect de la qualité (que l'on aura décidé ensemble) et du cadre* -- à -- *débrouillez-vous, vous êtes responsables, vous êtes le chef (c'est çà que veulent entendre les acheteurs)*.

C'est la fin d'un bon groupe, Scrum, il nous aura fait vibrer. Toujours difficile à jouer, beaucoup s'y sont cassés les dents, beaucoup l'ont plagié sans en comprendre le sens. Les premiers opus resteront dans toutes les bonnes sonothèques, les derniers seront fondus dans les décharges municipales avec les vieux *smartphones*. 

## Sur le scrummaster 

Scrummaster c'est faire vivre un équilibre. Un équilibre entre le désir du produit, et les capacités et la volonté de ceux qui le fabriquent, le produit. Pour pouvoir être écouté, ou respecté (même si on est traité de "the prick"), il faut être neutre : ni du côté du produit, ni du côté de l'équipe qui fabrique. Un seul moyen d'être neutre : n'être responsable de rien si ce n'est du cadre. 

Dans le cadre il y a cette chose un peu bizarre, ce personnage : l'équipe qui fabrique. Le scrummaster doit se débrouiller, si il n'existe pas déjà, de faire exister ce personnage. Et ce n'est pas juste un assemblage d'individualités, c'est une vraie équipe. Vous connaissez les stades de Tuckman : on crée l'équipe (*forming*), elle se cherche en se frottant (*storming*), elle trouve ses marques (*norming*), et -- par moments et de façon cyclique -- elle est très performante (*performing*). 

Être neutre cela ne veut pas dire ne pas être confrontant. Au contraire vous allez être celui qui va savoir faire passer ces stades à beaucoup d'équipes avec un mélange complexe qui allie la protection (la "psychological safety", la sécurité psychologique au sein d'une équipe et son écosystème) et la mise en évidence de leurs défaillances d'équipes (pas individuelles). Celui qui saura leur montrer la réalité en face tout en les protégeant. Sans être trop violent (une fièvre soigne, trop de fièvre tue). 

C'est en utilisant ce chaud/froid que le scrummaster aide l'équipe à passer de *forming* à *storming* à *norming*, qui aide à faire naître ce personnage qu'est l'équipe. Le scrummaster est un subtil alliage de soumission et rébellion au système. Impossible d'utiliser ce chaud/froid si on n'est pas neutre, si l'intention est ailleurs. Impossible d'être neutre, de travailler cet équilibre, cette tension, si on est responsable de quoi que ce soit.   


Pour un aperçu plus concret du rôle d'un scrummaster : [guide de survie à l'agilité](/pdf/guiderapide.pdf) 

Vous faire [mentorer](/mentorat/) ? 




