---
date: 2013-03-01T00:00:00Z
slug: pourquoi-une-user-story-technique-est-un-aveu-dechec
title: Pourquoi une *user story* technique est un aveu d'échec
---

Qu'est ce que l'agile ? Si je résume mon point de vue ces temps-ci c'est
une culture de l'évolution en milieu complexe. Dans ce cadre complexe,
l'agile propose des réponses pragmatiques : une approche souvent
itérative qui conclue ses cycles par des états "finis" (sinon l'itératif
n'a pas assez de sens). A cela il faut ajouter une notion très
importante, fondamentale, qui lui vient en grande partie de l'un des ses
papas idéologiques, *lean* : la valeur, le pourquoi nous faisons les
choses. L'approche souvent itérative est donc priorisée par cette
valeur. On fait les choses de façon itérative, elles priorisées par
valeur, avec un état fini en fin de cycle (c'est fondamental je le
rappelle). Bien voici des bases posées, loin d'êtres complètes mais qui
appuieront mon petit débat.

## Mais cette valeur c'est quoi ?

Dans le contexte souvent concurrentiel dans lequel j'évolue, celui d'un
marché en compétition, je résume celle-ci à 3 grandes familles.
Généralement on trouve cette *valeur* en s'interrogeant sur le
*pourquoi*. Donc habituellement je propose à titre d'exemple 3 grandes
familles (non exhaustives) :

-   le cash, l'argent, que ce que nous faisons rapporte : si je délivre
    telle fonctionnalité je pourrais gagner \$\$\$. C'est bien car la
    mesure est très efficace.
-   l'image, la communication : si je délivre cette fonctionnalité je me
    rendrai visible, je prendrai position sur tel ou tel marché, je
    comblerai un déficit concurrentiel, etc.
-   une performance : c'est la première fois que ce
    service/fonctionnalité est délivré sous cette forme, c'est la
    première fois que nous pouvons aussi rapidement faire..., c'est la
    première fois que nous pouvons agréger ces différents types de
    données, etc.

Vous pouvez avoir d'autres sources de valeur, ce ne sont que des
exemples très courant, les principaux à mes yeux, dans mon contexte. On
pourrait envisager des ONG dont la valeurs est de : distribuer le plus
rapidement possible leurs stocks, permettre un accès à certains
médicaments à un plus grand nombre, etc. Comme je vous le disais ma
liste n'est pas exhaustive. Mais même mes deux précédents exemples
entrent malgré tout dans ma troisième famille : performance.

## Une *User Story* doit exprimer de la valeur

Il est donc essentiel. essENTIEL. **ESSENTIEL** que les *user stories*
(le formalisme de l'expression du besoin le plus commun dans l'agilité)
expriment cette valeur. On dit généralement : "...de façon à \[mettre
ici la valeur que la user story devrait générér\]". Or, et c'est là que
je reviens au sujet de ce post, qu'est ce qu'une *user story* technique
? C'est un aveu d'échec car c'est une *user story* sans valeur. **Ouvrez
les vannes à laves incandescentes : la technique pour la technique n'a
jamais de valeur**. Donc une *user story* technique n'a pas de valeur.

Mais Pablo tu ne nous as pas vraiment dit ce qu'était une user story
technique. Ah pardon. Une *user story* technique c'est une expression de
besoin qui couvre un besoin technique : on doit déployer tel type de
composant technique, on réécrit tel fonctionnement technique, on doit
remplacer tel type de librairies. Deux **nouvelles mauvaises** raisons
pour les *user stories* techniques (la première étant qu'elles ne
possèdent pas de valeur) :

-   On a pris du retard et on *refactorise*. C'est dommage le
    refactoring cela devrait être tout le temps. ca arrive à tout le
    monde de prendre du retard, moi le premier, il n'empêche, c'est
    dommage.
-   On estime que le morceau technique a réalisé est trop gros pour être
    intégré au sein de la réalisation d'une véritable *user story*, une
    *user story* avec de la valeur, une *user story* **métiers**.

## La bonne façon agile de traiter la technique

La technique c'est le moyen, c'est le comment. Ce n'est pas le pourquoi.
Ce n'est pas la valeur.

Il faut que ces aspects techniques soient traités sous couvert de
traitement d'un besoin métiers, qui lui a de la vraie valeur. *En tant
que tel client, je souhaite pouvoir blablabla de façon à avoir telle ou
telle valeur*. La réponse, le comment, apportée à cette demande met
potentiellement en oeuvre des aspects techniques forts : utilisation de
tel ou tel composants, etc. La technique est très présente, elle est
très importante (vous en doutiez ?), mais elle est le moyen, le servant,
le Lancelot de Guenièvre, le d'Artagnan de la Reine, J'y reviendrai.

Trop souvent les équipes estiment que la part technique est trop grosse
pour être *dissimulée* sous une demande métiers. C'est généralement,
d'après mes observations, un manque d'entrainement, un manque
d'habitude, et surtout un manque d'à propos et d'acquis concernant
l'approche souvent itérative de l'agilité. Je n'obtiens jamais de
justification intéressante concernant une *user story* technique. C'est,
je crois me souvenir : toujours jusqu'à présent, un aveu d'échec (dans
le sens ou cela nous détourne de la culture agile).

Petite précision : avoir des écrans qui s'affichent vite, pouvoir être
diffusé sur des terminaux exotiques, etc ce n'est pas de la technique,
c'est bien là des points métiers : j'ai besoin de voir ma courbe se
mettre à jour en temps réel de façon à ..., je souhaite pouvoir
consulter ces informations sur... de façon à .... . Je reviens encore
aux deux exemples "ONG" employés plus haut : distribuer plus rapidement,
accéder à plus de personnes, ce n'est pas technique, c'est du métiers.

## Est-ce que c'est grave docteur ?

C'est un aveu d'échec, c'est dommage, cela nous titille, nous gratte,
mais ce n'est pas un drame. Cela arrive, aux meilleurs comme aux autres.
C'est dommage car on perd de vue une force essentielle de l'agile :
délivrer de façon souvente itérative de la valeur finie (**et priorisée
par valeur**).

L'agile c'est à mes yeux : business focus. Quand une ONG souhaite
distribuer plus de sacs de riz plus vite à une population plus variée,
c'est son Business. C'est son métier. Et mon rôle d'agiliste c'est de
débloquer, de mieux activer, ces sources de valeurs. En anglais pour
faire *in* (comme *scrum* ou *extreme programming*) : c'est **business
focus agile, value enabler**.

Pour autant peut-on éviter systématiquement des *user stories*
techniques. Il le faudrait, mais cela dépend des contextes (bingo). En
tous cas cela devrait vous titiller, vous gratter, vous géner aux
entournures, mettre des *warnings* en gros, en gras, flashy. Sinon à mes
yeux vous perdez le sens de l'agile.

## La *User Story* technique est morte, vive la technique !

Les profils techniques prennent trop souvent cela pour une attaque en
règle, un dénigrement, à tord. Dans les faits, ceux que je rencontre
apprennent vite que tout cela se tourne très vite aussi à leur avantage.
A chacun son pré carré. En devenant *Servant* du métiers, moyen/comment
du pourquoi, la technique gagne sa liberté d'exercer son art comme elle
l'entend. Ca ne veut pas dire qu'elle n'a plus le temps pour faire de la
technique, au contraire, cela veut dire qu'elle gère exactement comme
elle le souhaite (et on espère intelligemment, efficacement, avec
agilité) le temps qu'elle accorde pour répondre techniquement au mieux à
cette création de valeur qui est le but ultime de la *user story*.

Enfin il y a de la grandeur à être le *Servant*, et dans la culture
agile c'est un art.

## Feedback !

{{< image src="/images/2013/03/feedback-technique-florian.jpg" title="Florian" >}}

**Florian**, oui je considère que "refactorer" (comme tu le décris)
c'est de la *technique pour de la technique*. Mais ce n'est nullement
dévalorisant ou inutile comme tu sembles le sous-entendre dans ma
bouche. Ce n'est nullement le cas, c'est très important de "refactorer".
Mais il ne faut pas que cette valeur là, cette utilité, cette
"puissance" même dirais-je soit le sujet de nos réalisations. C'est la
façon indubitable de bien faire, mais pas la cible. Cela va amener de la
valeur comme tu le dis, mais ce n'est pas cette valeur auquelle je fais
allusion, et ce n'est pas cette valeur là qui doit mener les actions,
projets, produits agiles. Pour moi cela va amener de l'efficacité, de la
facilité, de la performance, mais pas de la valeur au sens métiers.

Oui de mon point de vue c'est attendre la *story* métiers en question
qui permet de lancer ce chantier de refactoring, c'est en tous cas la
meilleure des solutions à mes yeux.

{{< image src="/images/2013/03/tweet-stories-tech1.jpg" title="Pas d'accord" >}}

{{< image src="/images/2013/03/tweet-stories-claude.jpg" title="Stories" >}}

**Philippe & Claude** : on n'est pas d'accord. Très bien ! c'est votre
droit le plus strict. **Mais je ne vous rejoins pas sur votre analyse**.
Vous faîtes un catalogue des types de *User Stories*, une nomenclature,
de choses que l'on croise. Oui. Pas de souci. Mais de mon point de vue
il n'empêche que tout ce qui nous éloigne d'une *user story* avec de la
vraie valeur (celle du *product owner*, des parties prenantes) est un
aveu d'échec. Je ne vais pas me ré-expliquer, il suffit de relire mon
point de vue plus haut, je ne dénie absolument l'importance cruciale des
aspects techniques, mais je considère que faire autre chose que des
*user stories* avec valeur pour les *product owner* et les parties
prenantes est une déviance qui nous éloigne de nos objectifs et de notre
culture agile. Et le *spike* que tu évoques Claude est d'autant plus une
déviance qu'il ne nous fournit rien de "fini". Donc, là comme, les *user
stories* techniques, c'est un pis-aller (parfois nécessaire
malheureusement).

{{< image src="/images/2013/03/tweet-stories-tech3.jpg" title="Aveu d'apprentissage" >}}

Fabrice vient de proposer une belle formule.

