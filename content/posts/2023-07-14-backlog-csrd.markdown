---
date: 2023-07-14
slug: backlog-csrd
title: "Backlog CSRD, une estimation vertueuse"
draft: false
---

CSRD ? Complex, Simple, Reuse, Dependant ? 

Naaaannnn.  

CSRD ? C'est la **[Corporate Sustainability Reporting Directive](https://finance.ec.europa.eu/capital-markets-union-and-financial-markets/company-reporting-and-auditing/company-reporting/corporate-sustainability-reporting_en)**. 

Comment estimer mon *backlog*, la liste des choses que vous souhaitez réaliser, votre feuille de route, est une question souvent posée : sur quoi se base-t-on ? Depuis longtemps on interroge la valeur et la charge de travail. Tout cela de façon estimative : c'est-à-dire dans les grandes lignes. Pas de précision dans les estimations : c'est inutile, car par définition une estimation est imprécise. Vouloir les rendre précises c'est a) perdre du temps b) générer des erreurs.  

Quelques précédents articles sur le sujet : 

* [2018, Les estimations sont fausses, surtout si on considère qu'elles sont justes](/2018/09/les-estimations-sont-fausses-surtout-si-on-considere-qu-elles-sont-justes/)
* [2017, Estimation visuelle](/2017/09/estimation-visuelle/)
* [2016, Les estimations ne sont pas uniquement liées à la complexité, bordel](/2016/04/les-estimations-ne-sont-pas-uniquement-liees-a-la-complexite-bordel/)

## Et dans "valeur" on met quoi ? 

Vous savez *agile* c'est historiquement : maximiser la valeur, minimiser l'effort. Même si aujourd'hui maximiser la valeur est devenue une erreur à mes yeux. Et je préfère parler d'optimisation ou de résilience que de maximisation ([2022, Produit maximum tolérable](/2022/11/produit-maximum-tolérable/)). 

Généralement dans "valeur" on met un impact direct sur ce que vous recherchez : "plus de visiteurs", "plus d'achat", "plus de visibilité", "plus d'attachement", etc. Ou on met une notion d'apprentissage : "je sais désormais comment réagit cette population", "je sais comment utiliser au mieux cette brique technique", etc. Et tout cela finalement cela se réduit souvent dans la bouche de nos interlocuteurs à : combien ça rapporte ??? On ne peut pas leur en vouloir **c'est aussi ainsi que l'état français consolide tout le travail d'une organisation : par un bilan financier**. 

Et c'est cela qui vient de changer. 


## Financier...social et environnemental

Au travers de cette [directive européenne](https://finance.ec.europa.eu/capital-markets-union-and-financial-markets/company-reporting-and-auditing/company-reporting/corporate-sustainability-reporting_en) il est demandé à 50 000 entreprises européennes pour 2024/2025 (et on espère pour toutes ensuite) de ne plus consolider seulement avec un regard financier, mais aussi **un regard social et environnemental**. J'en parlais sous l'appellation **triple comptabilité** dans cet article ([2022, produit de demain](/2022/09/produit-de-demain/)). 

J'ai l'impression que pour l'instant la forme est libre, en tous cas c'est assez le début pour permettre de faire émerger un formalisme. On pourrait imaginer dans la partie sociale : des congés parentaux étendus (bénéfice), un travail sur l'équilibrage des salaires entre les sexes (bénéfice), un conseil de direction très majoritairement masculin (déficit), un très faible sentiment d'appartenance à l'entreprise (déficit), un tarif préférentiel sur l'offre pour les demandeurs d'emploi (bénéfice), etc. Sur la partie environnementale : imaginons une politique de location de vélos et d'aménagement de parking vélos autour des bureaux (bénéfice), l'annulation du grand séminaire annuel en avion à Porto pour Bédarieux en train (bénéfice), l'installation d'une intelligence artificielle comme moteur de *tchat* mais qui consomme beaucoup de données et de processeurs (déficit), etc. 

Je pense qu'il serait sain de d'ores et déjà appliquer cette directive à vos pondérations et à vos choix quand vous fabriquez quelque chose. Quel est son poids social ? Positif ou négatif ? Quel est son poids environnemental ? Positif ou négatif ? Et surtout ne pas rester à "est-ce que cela va rapporter" ? Car d'une part le non-respect de la directive se traduira par des amendes et des obligations, d'autre part le retour de bâton se fera naturellement par le coût social et environnemental que vous allez générer. 

## N'en profitez pas pour renforcer une machine à gaz

Le mal de beaucoup de systèmes d'estimation est de se vouloir précis, plein de règles. C'est inutile, pire cela génère des erreurs. Donc ajouter des éléments comme je le recommande, c'est bien, mais cela doit rester une conversation simple. 

A ceux qui se lancent ou qui se seraient lancés, je suis preneur de vos exemples de mise en évidence de ces aspects sociaux et environnementaux dans la priorisation, la pondération, la stratégie de votre feuille de route.  

