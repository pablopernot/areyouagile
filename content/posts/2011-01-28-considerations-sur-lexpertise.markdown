---
date: 2011-01-28T00:00:00Z
slug: considerations-sur-lexpertise
title: Considérations sur l'expertise
---

Ceci est un article de [Jenea
Hayes](http://www.cooper.com/journal/jenea_hayes/) sur
[Cooper](http://www.cooper.com) que j'ai beaucoup apprécié et que j'ai
souhaité faire partager aux non anglophones.Merci Thomas de m'avoir mis
sur la piste de cet
[article](http://www.cooper.com/journal/2011/01/reckoning_with_expertise.html).

Jenea a écrit :

## Considérations sur l'expertise

### Vous n'êtes pas un expert

Vous rappelez-vous la première fois que vous avez essayé de conduire une
voiture ? C'est une expérience exaltante et terrifiante que de tenter de
coordonner plusieurs parties de son corps !  Et pendant que notre
attention est focalisée sur cet effort de coordination, difficile de ne
pas s'écraser sur quelque chose et de garder le contrôle du véhicule !

Après plusieurs heures de pratique, conduire n'est cependant plus un
supplice. Les conducteurs expérimentés peuvent aisément diriger leurs
voitures tout en ayant d'autres activités : avoir une conversation,
régler la radio, manger un sandwich, etc. En effet, nous sommes nombreux
à avoir conduit sur une longue distance sans nous rappeler des détails
de la journée, en conduisant de façon inconsciente.

Ceci est l'expérience de l'expertise, c'est à dire, la mise en oeuvre
d'une grosse dose d'expérience dans un domaine précis. Ce qu'illustre
notre exemple concernant la conduite c'est que -dans leurs domaines- les
experts gèrent l'information différemment des novices. Les chercheurs en
sciences [cognitives](http://fr.wikipedia.org/wiki/Cognitif) étudient
les "experts" dans l'espoir de découvrir ce que le cerveau met en oeuvre
lorsqu'il y a expertise, et cela avec l'objectif  de capturer l'essence
même de l'expertise afin de fabriquer des systèmes "experts".   Dans une
étude célèbre, de grands joueurs d'échecs et des novices furent
confrontés à un test : on leur a rapidement montré un échiquier avec
certaines pièces dessus. Ensuite, on leur a demander de recréer celui-ci
en positionnant les pièces tels qu'ils les avaient vus. Les grands
joueurs ont été nettement meilleurs que les novices à ce jeu.
Contrairement aux novices, les experts étaient capable d'identifier de
nombreux motifs dans l'organisation des pièces et d'utiliser cette
information pour se rappeler de leurs emplacements.

Attention, ceci n'est vrai que si la disposition des pièces correspond à
quelque chose de vraisemblable (dans une partie d'échecs). Si on leur
présentait un certains nombres de pièces disséminées sur l'échiquier de
façon aléatoire les grands joueurs n'étaient pas meilleurs que les
novices.

Le facteur clef ici c'est la faculté de l'expert à regrouper les
éléments par blocs d'informations complémentaires.La plupart des gens
sont capables d'appréhender 7  informations dans leur mémoire
"immédiate" (ce principe est connu sous le nom de "[7 magique de
Miller](http://en.wikipedia.org/wiki/The_Magical_Number_Seven,_Plus_or_Minus_Two)").
C'est vrai pour les experts comme pour les novices. La différence
essentielle entre experts et novices est donc ce qui constitue pour eux
une information. Les experts sont capables d'agréger plusieurs éléments
en une seule information. Ainsi, pour un expert, 7 éléments dans son
domaine d'expertise s'agrègent en une seule information. Et donc 7
informations dans leur domaine d'expertise représente bien plus
d'éléments que les 7 informations du novice. Dans l'exemple précédent
des échecs, les grands joueurs sont capables d'agrégé plusieurs pièces
en une seule information.

Cette agrégation d'informations se déroule naturellement, sans effort
conscient de la part de l'expert. Car la plus grande part de la magie de
l'expertise réside hors de la conscience. C'est comme conduire, si vous
vous rappelez l'exemple plus haut. Le pilote réalise les mêmes actions
que le conducteur débutant, mais il les réalise sans même y penser.  Les
études du cerveau concernant l'expertise ont noté ce fait. Il semble que
les gens n'utilisent pas les mêmes parties de leurs cerveaux selon si
ils perçoivent des informations d'un de leurs domaines d'expertises ou
non.

J'insiste sur ce point car c'est fondamental : il semble que les gens
n'utilisent pas les mêmes parties de leurs cerveaux selon si ils
perçoivent des informations d'un de leurs domaines d'expertises ou non.

Et donc, qu'est ce que cela a à voir avec notre travail ? Nous
travaillons avec des experts constamment, ceux qui sont impliqués dans
la fabrication d'un produit, ceux qui sont les cibles de ce produit.
 Bien comprendre l'expertise va nous aider à mieux concevoir les
produits que nous réaliserons.

Le premier expert que vous rencontrez, c'est vous même. Sauf cas
exceptionnel, vous n'êtes pas exactement comme la cible de votre
produit, de votre projet. Soit vous êtes un responsable produit, soit un
designer, un directeur artistique, un développeur, etc. votre cerveau,
votre pensée a énormément évoluée avec le temps. Votre perception est
différente de celle de la cible de votre produit, de votre projet.
Prenez garde contre le danger de s'imaginer représentatif des attentes
de votre produit. Vous ne concevez pas un produit pour vous, mais pour
un autre.

Cela m'a soudainement frappé lors d'un projet récemment. Le projet
incluait un site web et la cible du produit semblait à s'y méprendre
être moi-même ! du moins sur le papier. Même profil économique et
social, passant autant de temps que moi en ligne, sur internet, (ce qui
n'est pas rien),etc. Je pensais que pour le coup je pouvais me prendre
comme référence pour développer des idées, parce que justement j'étais
comme la cible du produit. Ok ?

Erreur. Nos recherches ont révélé combien j'étais différent ! Avec le
temps, les comportements que nous avons observé (chez la cible) étaient
très différents des miens. Les gens normaux qui n'ont pas passé des
années à étudier l'informatique, à jouer avec ordinateurs, à manipuler
des logiciels, à concevoir des logiciels ont nécessairement et
simplement une approche différente de la mienne. Ma perception a été
altérée pour toujours par mon expérience propre, et concevoir un produit
en se basant sur cette perception modifiée est une grave erreur.

C'est pourquoi il faut s'aider d'outils comme les personnages et les
scénarios (NDT : les "personnages" sont des personnages imaginaires qui
représentent les gens qui vont effectivement utiliser le produit. On
leur invente souvent une vie, des qualités et des défauts, une photo,
etc.). Ces pratiques vous permettent de concevoir des produits faits
pour des gens différents de vous.

### Vous apprenez des experts

Dans le cas d'un produit complexe, soit vous allez devoir travailler
avec des experts pour fabriquer celui-ci, soit vous allez devoir
concevoir ce produit pour des experts, voire les deux. Quoiqu'il en soit
vous allez devoir discuter avec des experts et concevoir ce produit à
partir des informations que vous allez recueillir. Le problème est que
si vous êtes un novice dans le domaine, parler à ces experts, c'est
comme parler avec quelqu'un qui parle une langue différente et inconnue.

Rappelez vous que les experts n'ont pas conscience de leur expertise (du
moins de comment il l'utilise), et souvent ne sont pas capables de
formuler clairement quels mécanismes ils mettent en oeuvre dans leur
perception. Cela ne les empêchera pas d'essayer, naturellement, ou même
de penser que ce qu'il exprime est précis et simple. Le fait est bien
établi que nous -les humains- essayons souvent de rationaliser et
comprendre les phénomènes inconscients de la pensée. Dans une analyse de
la littérature sur l'expertise, Dreyfus & Dreyfus (2005) écrivent ainsi
:

*"Si vous demandez à un expert comment il procède, cela va le pousser,
effectivement, à se replacer au niveau du novice et à expliquer les
règles qu'il a appris à l'école. Et donc, au lieu d'énoncer des règles
dont il ne se rappelle pas, comme on l'imaginerait, l'expert est obligé
de se rappeler de règles qu'il n'utilise plus. En fait, il est
impossible de quantifier l'expérience d'un expert, la somme des
connaissances bâties sur un nombre incalculable de situations
différentes."*

C'est pourquoi demander à un expert de décrire comment il interagit avec
son domaine d'expertise donne rarement de bons résultats. Chez
**Cooper**, nous essayons d'éviter de produire ce que l'utilisateur dit
vouloir, afin de ne pas "appauvrir automatiquement" le besoin.
Comprendre l'expertise fournit une bonne raison à cela : les
utilisateurs experts ont rarement conscience de ce dont ils ont
réellement besoin.

C'est pourquoi l'observation est un facteur clef de la recherche. Les
façons de faire d'un utilisateur est un meilleur indicateur de ses
objectifs réels que le besoin qu'il exprimera de façon rationnelle.

### Concevoir pour les experts

Peut-être concevez vous des produits pour des experts d'un domaine
particulier. Un problème identique, et peut-être encore plus
intéressant, est de concevoir pour des utilisateurs experts, ou qui le
deviendront, sur **votre produit**. Donc comment concevoir pour des
experts ?

D'abord, jetons un oeil aux étapes classiques d'un apprentissage comme
le font les experts en
[heuristique](http://fr.wikipedia.org/wiki/Heuristique). Observons les
gens du stade de novice à celui d'expert. Cela nous donnera peut-être
les clefs du développement de l'expertise. Soyez ouvert à ce que vous
constatez. Un motif pourrait émerger de ce qui ne fait pas encore sens
pour vous, mais ce n'est pas surprenant : vous êtes le novice.

Si vous concevons un produit avec lequel vos utilisateurs seront
experts, gardez à l'esprit le cheminement qui mène le novice à
l'expertise. Penser votre produit comme un langage est utile (une langue
est un cas particulier, mais cela marche assez bien comme analogie pour
l'expertise de façon générale). Est-ce que votre produit communique de
façon simple et efficace au novice ?  Ou est-ce que votre produit semble
parler différentes langues en même temps ? A l'usage est-ce que
l'utilisateur se perfectionne, gagne en maturité, rassemble des concepts
simples en ensembles significatifs ? C'est à ce moment que les
interactions (NDT:avec le produit) deviennent cruciales. Si les
interactions avec le produit ne sont pas de qualité, il sera beaucoup
plus long de développer cette expertise. C'est pourquoi les produits
créés en agrégeant le travail de plusieurs sont si difficile à
apprendre, c'est comme lire un livre écrit en plusieurs langues.

### Concevoir pour les novices

Peut-être concevez vous des produits dont l'utilisation serait
ponctuelle, par des utilisateurs novices. Cependant, tout le monde est
expert dans son domaine. Y aurait-il des opportunités pour utiliser
cette expertise malgré tout ? Peut-être que votre produit peut s'appuyer
sur des méthodes ou des manières d'interagir avec lesquelles
l'utilisateur est déjà sensibilisé ("parler le même langage" que
d'autres produits). Ou peut-être pouvez vous vous appuyer sur une
expertise plus fondamentale, plus commune, plus basique. Une des raisons
du succès des écrans tactiles comme celui de l'IPhone est qu'il se base
sur notre expertise des règles de la physique élémentaire. J'ai pu
apprendre à un bébé de 18 mois d'utiliser une application photo sur mon
IPhone parce qu'il savait déjà qu'il pouvait glisser des choses sur une
surface. Il n'a pas eu à apprendre quoique ce soit de nouveau, pas de
"liste d'options", de "fichier",  de bouton "suivant" pour que
l'application fonctionne.

### Résumons tout cela

Si vous ne deviez retenir qu'une seule chose de ce texte, c'est ceci :
la perception des experts est différente de celle des novices.
Considérez tous les experts avec lesquels vous allez travailler durant
la conception du produit : responsable métiers, responsable fonctionnel,
management, responsable technique, utilisateurs,etc. Ils sont tous des
experts dans leur domaine, et des novices par ailleurs. Ils ont tous
transformés leurs cerveaux pour leurs besoins. Si vous prenez le temps
de réfléchir à cela, vos produits seront meilleurs pour les experts,
comme pour les novices.

[Jenea Hayes](http://www.cooper.com/journal/jenea_hayes/)

Dreyfus H. & Dreyfus S. (2005) "Expertise in real world contexts".
Organization studies 26(5):779-792

*Ma traduction est plus que perfectible, n'hésitez pas à me faire des
retours pour correction.*
