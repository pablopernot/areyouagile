---
date: 2011-10-28T00:00:00Z
slug: agile-cmmi-potion-magique-ou-grand-fosse
title: Agile & CMMi, potion magique ou grand fossé ?
---

Je reviens sur la session **Agile & CMMi : potion magique ou grand
fossé** que nous avons donné récemment Yassine (@yassinezakaria) et moi
même lors de l'[Agile Tour Toulouse 2011](http://agiletoulouse.org/). 
Les slides sont disponibles [ici](http://www.slideshare.net/pablopernot/agile-cmmi-potion-magique-ou-grand-foss)
(ou intégrés plus bas) et je souhaitais les commenter un peu, même si
des commentaires écrits remplacent rarement une session.

D'abord sachez que cette session nous a amené -Yassine & moi- a pas mal
de discussions assez musclées.

Idée du waterfall : 

{{< image src="/images/2011/10/waterfall-300x228.jpg" title="Waterfall" >}}

Pourquoi avoir fait une session sur Agile & CMMi ? D'abord car nous
trouvons un peu absurde les levées de boucliers et les préjugées dont
font preuve une grande partie des gens de ces deux mondes vis à vis l'un
de l'autre. Nous nous sommes amusés à mettre cela en forme au travers de
différents slides. Mais la malheureuse réalité est que quand l'esprit
des gens ces caricatures ne sont pas loin d'être vraies. Et d'ailleurs
de nombreuses mauvaises applications de l'agile ou de CMMi les rendent
souvent vraies ou les renforcent...

Ces préjugés sont d'autant plus dommageables que :

-   qu'il s'agisse d'agile ou de CMMi on fait souvent référence au
    "chaos report" du Standish Group pour expliquer que l'on souhaite
    mettre fin aux projets qui échouent, en témoinage nos différents
    slides de formation...identiques.
-   que la réponse **initiale** prend une forme comparable : un ensemble
    de pratiques sur lequel on opère un focus : ensemble intégré pour
    CMMi, pratiques poussées à l'extrême pour par exemple XP. Je précise
    bien : une réponse " **initiale** ",  ce que je n'ai pas fait dans
    la présentation ni les slides et c'est encore aujourd'hui un sujet
    de discussion (autour des petits fours) avec Thierry (@thierrycros).

Ces préjugés sont d'autant plus dommage que nos "grands sachants"
(quelques "grands noms") cherchent à opérer une convergence entre ces
deux approches (CMMi & Agile). Nous donnons en témoignage quelques
retours de Jeff Sutherland, David Anderson, Richard Basque ou Scott
Ambler. A ce sujet Tullius Detritus se fait un plaisir de vous rappeler
qu'il ne faut pas nécessairement croire les yeux fermés les "grands
sachants" et qu'un peu de recul ne fait jamais de mal. (Dans ce cadre
Jeff Sutherland parle de "potion magique", c'est ce qui a donné son nom
à cette session, ou "grand fossé" pouvons nous nous interroger).

Nous présentons ensuite les différentes façons dont chacun de notre côté
nous pouvons faire bénéficier notre approche de certaines qualités de
l'autre. De façon assez en adéquation avec les approches de Sutherland
ou Basque, Yassine essaye de faire profiter ses projets CMMi de
certaines bonnes pratiques agiles : notamment le feedback (souvent trop
tardif en CMMi), les aspects itératifs, etc. Il faut rappeler que CMMi
propose naturellement : l'implication des "stakeholders", de réaliser
les ajustements nécessaires à chaque projet (on n'embarque pas un CMMi
unique et monolithique).

*\[ajouté le 29 oct\] L'approche classique consiste à dire que CMMi est
le "quoi", et agile le "comment". En tant qu'agiliste je souhaite
changer ce paradigme. L'agile est fondamentalement au centre de la
rélfexion car il est une culture, il apporte des valeurs. Donc
schématiquement il est le "pourquoi", en amont du "quoi" et du
"comment".*

Je suis pour ma part assez d'accord avec David Anderson sur le fait que
Agile pourrait profiter des pratiques de niveau 4 & 5 de CMMi, soit pour
s'appuyer sur un cadre pour gérer l'innovation au niveau de
l'organisation, soit pour s'appuyer sur des tendances pour mieux gérer
ses projets. Attention toutefois la métrique pour la métrique est
naturellement est danger. QPM niveau 4 de CMMi ne propose justement pas
de s'appuyer sur des chiffres pour gérer les projets, mais de tendances.
Cette pratique dont on s'inpirerait nous dit juste : voilà les
statistiques que nous avons pu collectées, soyez attentifs si votre
projet semble ne pas s'y conformer alors qu'il devrait. Soyez juste
attentif, avertis (un projet averti en vaut deux) et pas nécessairement
plus. Pour que ces statistiques fonctionnent (ça aussi c'est un mot que
je n'aime pas trop, et qu'il faut manipuler avec soin), elles doivent
provenir d'informations qui puissent être comparable entre projets. Et
donc  naturellement vous pouvez d'ores et déjà écarter la vélocité. 
Quels métriques ? je peux vous renvoyer vers par exemple l' [article de
Jérémie](http://www.redsen-consulting.com/2011/02/comment-mesurer-la-qualite-logicielle/)
(@jgrodziski), on pourrait aussi se baser sur le taux de turnover
(récupéré chez les RH),etc. Attention donc vraiment de ne pas s'aliéner
avec des chiffres et de bien réfléchir aux indicateurs (difficile
d'utiliser la business value, impensable d'utiliser la vélocité, etc.)

J'évoquerai rapidement dans un futur post les "fortunes CMMi" que je
propose. Il s'agit de s'appuyer sur l'énorme rétrospective sur laquelle
CMMi se base pour faire -de façon ludique, simple et régulière- monter
en compétence les équipes agiles qui manqueraient "de poil aux pattes".

Enfin, notre conclusion souhaite faire passer deux messages :

1.  Quelque soit votre choix (Agile ou CMMi) vous devez vous donner les
    moyens de votre démarche, inutile de faire semblant, uniquement de
    façon marketing, pour la certification, etc. Il faut vraiment
    essayer de ce donner les moyens de bien embrasser la démarche.
2.  Nous ne préconisons surtout PAS un pôt pourri de ces deux méthodes.
    Vous devez avoir une épine dorsale claire et non ambigüe : agile OU
    cmmi. Ensuite pourquoi ne pas s'enrichir des apports d'autres
    méthodes, c'est notre point de vue.

Quelqu'un m'a posé une question : comment sait-on que l'on ne fait pas
d'agile "marketing". La réponse m'a semblée tellement évidente que ...
je n'ai pas eu de réponse. Cela sera aussi le sujet d'un futur post.
Comment être sûr que l'on ne fait pas d'agile "marketing". Merci à la
personne qui a posé cette question restée sans réelle réponse.

En illustration en haut à gauche et afin de relancer une nouvelle la
discussion, le mode "Waterfall" interprété par les Goths dans Astérix &
Obélix.Je ne sais pas si cela a un lien mais ce post a été écrit avec
"Overkill" de Mötorhead dans les oreilles. Je n'en sors pas grandi.

**Agile & CMMi, potion magique ou grand fossé ?**

<script async class="speakerdeck-embed" data-id="4f9cf417c10a56002200d136" data-ratio="1.44837340876945" src="//speakerdeck.com/assets/embed.js"></script>

