---
date: 2014-10-19T00:00:00Z
slug: poker-de-personnalite-personality-poker
title: Poker de personnalité, *Personality Poker*
---

Voici aussi un petit jeu que m'a fait découvrir [Olaf
Lewitz](http://trustartist.com/) cet été durant
[ALE2014](http://ale2014.alenetwork.eu/). Encore un dérivé des MBTI,
Process Com, et consorts, un jeu de cartes qui permet d'en savoir plus
sur soi, et peut-être sur l'image que l'on renvoi aux autres. Attention
toutes ces approches sur les personnalités sont très prétentieuses et
doivent être utilisées avec beaucoup de pincettes : ce n'est pas avec un
jeu de cartes que l'on en sait beaucoup sur les gens. Mais comme
toujours cela permet d'ouvrir la discussion, de créer un espace
d'amélioration, du feedback.

{{< image src="/images/2014/10/armed-dangerous.jpg" title="Personality Poker" >}}

Personnellement je l'utilise de façon très légère (c'est un jeu de
cartes après tout), et simplement pour lancer une conversation.
[Personality Poker](http://personalitypoker.com/) est un jeu de Stephen
Shapiro (de 2010), qu'il a beaucoup employé chez ... Accenture (silence,
temps mort, ... ok on continue). Le jeu s'accompagne d'un livre qui
donne des explications détaillées. Je vous recommande 1 jeu pour 3/4
personnes, 2 jeux pour 7/8, 3 jeux pour 9/11 personnes, etc.

## Quand l'utiliser ?

Lors des rétrospectives d'équipe, lors d'entretien régulier avec ses
collègues ou son supérieur hiérarchique, en entretien d'embauche (je le
rappelle : pour ces derniers, de façon décontractée si possible !),
enfin au bar avec une personne qui vous séduit.

## Comment l'utiliser seul ?

{{< image src="/images/2014/10/poker.jpg" title="Personality Poker" >}}

Seul ou avec un interlocuteur qui anime la discussion, on fait 4 tas :

-   Quelque chose dans lequel je me reconnais naturellement, sans
    effort.
-   Quelque chose dans lequel je me reconnais, mais le soir je perçois
    les efforts que j'ai du faire.
-   Quelque chose que je ne suis pas et que j'aimerais bien être.
-   Quelque chose que je ne suis pas et que je n'ai pas envie d'être.

Les 4 tas doivent arriver à être taille égale : il faut se forcer à les
équilibrer (cela oblige à se poser les bonnes questions). Ensuite on
peut s'interroger :

La personne est surprise de découvrir certaines cartes dans le premier
tas : expliquez lui, faîtes lui découvrir cet aspect de votre
personnalité. Peut-être cela va-t-il mené à reconsidérer certaines
cartes.

Essayez de placer plus votre adéquation votre activité avec ce que vous
êtes, sans effort.

La personne est là pour vous aider à vous développer, prenez une carte
du troisième tas, et discutez sur comment vous améliorer sur ce point.

Etc.

## Comment l'utiliser en groupe ?

Chacun reçoit cinq cartes, on demande à chacun de classer les cartes de
gauche à droite selon les 4 catégories évoquées plus haut. L'intérêt est
de passer du temps sur cette phase, les gens doivent s'interroger sur
eux-mêmes. Puis vous pouvez autoriser les gens à échanger en aveugle (je
te donne 2 cartes, tu m'en donnes 2) les cartes dans lesquels ils ne se
retrouvent pas (catégorie 3 et 4). Toujours l'important c'est de donner
du temps à la réflexion car finalement vous allez proposer d'échanger
les cartes dans lesquelles ils ne se retrouvent pas avec le tas que vous
avez gardé. Encore quelques temps après (pas non plus trop long !) vous
leur demandez de ne garder finalement que les cartes dans lesquelles ils
se reconnaissent, sans effort (catégorie 1). Vous pouvez proposer toutes
les cartes restantes visibles à disposition (les gens vont comprendre
que la première phase n'a servie qu'à les pousser à s'interroger plus
fortement).

Quand chacun se sent bien avec ses cinq cartes : on les dévoile devant
soi.

Attention voici une dernière action qui peut se révéler passionnante
mais dangereuse : demandez aux autres de placer des cartes devant vous
et qui vous correspondent (maximum cinq, pas de minimum). Vous vous
retrouvez donc avec vos cinq cartes (votre analyse de vous même) et
potentiellement de zéro à cinq cartes venant des autres participants.

C'est le début des explications et des dialogues. On peut reprendre les
dialogues évoqués dans la partie "seul". Sinon vous pouvez commencer à
lire dans les grandes lignes quelques analyses (du flan selon certains).

## Quelques analyses dans les grandes lignes

Les cartes de 2 à 4 représentent plutôt des défauts. Il en faut. Comment
être quelqu'un de reconnu pour ses connaissances (*knowledgeable*), sans
parfois passer pour un "monsieur je sais tout" (*know it all*, un 4 de
pique). Je dirais que c'est bien les défauts, cela explique les
qualités. Mais il n'y a pas d'obligation d'en avoir.

Les cartes de 5 à 10 sont plutôt des caractéristiques pas forcément
visibles (intra), vous l'êtes mais on peut ne pas le savoir. A l'inverse
les figures de valet à as sont des caractéristiques visibles (extra),
connus de tous. Vous pouvez être "ouvert d'esprit" (*openminded*), c'est
quelque chose d'internalisé, pas forcément visible, mais vous êtes aussi
aventureux (*adventurous*), et ça les gens le voient, ils vous voient
essayer toutes ces choses un peu risquées.

Naturellement chaque couleur possède son sens : les piques sont les
personnes analytiques, sur les faits, les données, ils creusent les
faits. Les trèfles sont associés aux gens d'action : ils planifient et
déclenchent, font marcher les idées. Les carreaux sont les innovateurs,
ceux qui pensent différemment, qui apportent une vision différente.
Enfin les cœurs sont les liants, la glue dans les équipes, les gens qui
s'occupent des gens.

Et idéalement on ne démarre une équipe, une entreprise sans un pique qui
sait de quoi il parle, un trèfle qui va actionner les idées, un carreau
qui va pouvoir introduire de l'innovation, la différence, et un cœur qui
assure la glue entre tous ces gens.

Naturellement vous pouvez avoir une grosse proportion d'une couleur,
comme proposer un panel de différentes couleurs. Il n'y a pas de mieux,
de meilleur. C'est la combinaison avec les autres qui donne tout son
sens à votre alliage.

## Mon premier "personality poker"

{{< image src="/images/2014/10/mypoker.jpg" title="Personality Poker" >}}

Sans connaître les règles, en bas ma sélection, en haut, ce que l'on m'a
attribué.

## Quelques faux-amis dans les traductions

Attention quelques faux amis :

-   Versatile : c'est polyvalent en français (et oui, c'est un valet !)
-   Gregarious : c'est fiable en français, on peut s'appuyer sur lui
    pour bâtir des équipes.
-   Bossy : rien à voir avec le boulot, c'est "petit chef" !

## Quelques traductions contre-intuitives ou compliquées

{{< image src="/images/2014/10/poker-cards.jpg" title="Personality Poker" >}}

-   Anal retentive : ne soyez pas effrayé, c'est juste "coincé du cul".
-   Boisterous : exubérant.
-   Scattered : dispersé.

Et amusez vous bien, car ce n'est qu'un jeu !
