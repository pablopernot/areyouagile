---
date: 2012-01-05T00:00:00Z
slug: agile-open-sud-coaching-agile-petit-dejeuner-after-work
title: Agile open sud, coaching agile, petit déjeuner & after work
---

Bonne année, meilleurs voeux. (bizarrement je n'en ai fait aucun, ni
aucune résolution).

Quelques évènements qui approchent et où j'espère vous croiser :

D'abord le 17 janvier 2011 si vous êtes du côté de Montpellier
Smartview organise un **petit déjeuner/séminaire** dont le sujet est
: Agilité : Identifiez les freins et adaptez votre parcours.

## Agilité : Identifiez les freins et adaptez votre parcours

Le pitch : \*Aujourd'hui l'agilité est reconnue comme un vecteur de
progrès, pour autant qu'elle soit bien comprise et bien utilisée. Ce
séminaire vous présentera les écueils les plus fréquents et la façon de
mieux appréhender ces méthodes dans votre contexte pour en tirer les
meilleurs bénéfices. (\~45mn)\*

Ce petit déjeuner aura lieu le **mardi 17 janvier de 8h30 à 10h00** dans
le centre de formation **Smartview** : 145 Avenue Clément Ader,
34170 **Castelnau Le Lez (Montpellier)**. si vous êtes intéressé : me
joindre !

Cette session sera aussi donné le **26 janvier** à **Marseille** grâce à
l'association [Esprit Agile]() et ses évènements :

## Agile After Work

Cela sera donc : le **jeudi 26 janvier 2012 à partir de 18h30** à
l'hôtel technologique, 45 rue Frédéric Joliot Curie, 13013 Marseille. 

Cette session sera aussi probablement donnée à Paris, je l'indiquerai
dans le blog si cela intéresse certaines personnes.

## Coach Retreat

Sinon question coaching une rencontre qui s'avère passionnante : un
[coach retreat organisé par Oana J](). Il aura lieu le 21 janvier à
Paris (un samedi...).  Il reste 4 places. Pour les gens intéressés il
faut se presser...

## Agile Open Sud

Et surtout le 16 et 17 mars il y aura le premier **Agile Open Sud** pour
regrouper les agilistes du sud. Vous êtes intéressé ?  Vous voulez en
savoir en plus ? Plongez vous dans le forum] : vous y trouverez les
informations, comment s'inscrire, etc.

## Conférences et sessions ?

J'ai postulé pour une nouvelle prestation à Sudweb 2012 (cette année
à Toulouse le 25/26 mai) avec Bruno, [un gars qui hait le printemps]().
J'espère y présenter la première sortie de "Un projet web dont vous êtes
le héros agile". Un jeu agile que nous souhaitons intéressant et qui
prend le format d'un "livre dont vous êtes le héros" mais sous forme de
parcours et d'ateliers divers & variés, d'obstacles terribles &
inhumains.

Il est aussi possible que j'intervienne à Mix-It pour lequel j'ai
aussi postulé pour des sessions plus classiques.

Sur ces deux conférences j'en saurais un peu plus rapidement (fail fast
? ).
