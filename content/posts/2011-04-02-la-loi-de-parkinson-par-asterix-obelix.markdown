---
date: 2011-04-02T00:00:00Z
slug: la-loi-de-parkinson-par-asterix-obelix
title: La loi de Parkinson par Astérix & Obélix
---

La [loi de Parkinson](http://fr.wikipedia.org/wiki/Loi_de_Parkinson)...
 ou les gaz parfaits... veut que "le travail s’étale de façon à occuper
le temps disponible pour son achèvement". Qui dans un projet classique
n'a pas vécu cette expérience étrange ? Donnez 3 jours à un
"réalisateur", il consomme les 3 jours pour réaliser ses tâches, donnez
lui 10 jours, c'est 10 jours qu'il utilisera... Oui on évite cela avec
Scrum au passage ou plus globalement avec l'agile, mais fallait-il le
préciser ?  Je vous laisse déguster l'explication éminement efficace de
**Uderzo & Goscinny** dans [Astérix en Corse](http://www.amazon.fr/Ast%C3%A9rix-en-Corse-Albert-Uderzo/dp/2012100201).

Merci encore à Frédéric S. pour son inspiration, ce gars fourmille
d'idées farfelues et donc drôlement intéressantes.

{{< image src="/images/2011/04/loideparkinson.jpg" title="Parkinson" >}}

