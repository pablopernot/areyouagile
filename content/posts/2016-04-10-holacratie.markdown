---
date: 2016-04-10T00:00:00Z
slug: hellocratie-holocratie-holacratie-honolulucratie
title: Hellocratie, Holocratie, Holacratie, Honolulucratie
---

Et voici l'holacratie qui se murmure un peu partout. Rien de nouveau et
pourtant des choses intéressantes. De la frustration, de l'irritation,
des moments de plaisir. Avant d'aborder dans le détails certains points
(de prochains articles ?), voici une impression d'ensemble.

## Paradoxe

L'holacratie ne semble être fait de rien de nouveau. Ce n'est qu'un
assemblage de choses connues. Et pourtant un petit air d'innovation
flotte, une sorte d'équilibre. Comme si on avait matérialisé une
avancée. Bien sûr d'un côté les vieux baroudeurs hurlent au détournement
marketing (et je dois avouer que je hurle avec les loups), de l'autre
les défenseurs de l'holacratie disent qu'il ne faut pas chercher d'où
cela vient et déposent la marque pour en faire -- j'imagine -- le
maximum de pognon. Allez on va s'en sortir.

## Organisation et flux d'énergie

{{< image src="/images/2016/04/risotto-2.jpg" title="Risotto" >}}

L'holacratie est une façon d'organiser son organisation (*sic*) ainsi
que sa mutation permanente. C'est donc une façon de gérer les flux
d'énergie quels qu'ils soient : prise de décisions, actions, dynamique
de groupe, etc. Ceci est mon analyse ce n'est pas présenté ainsi par
l'holacratie, en tous cas pas de façon franche. Pourtant à mon avis
l'idée essentielle d'une organisation est de ne pas vouloir gérer ses
flux d'énergie (et tout est énergie), mais de définir un cadre qui leur
permette de s'exprimer, de circuler, d'évoluer au mieux. Or c'est bien
ce à quoi se destine l'holacratie : définir un cadre strict, des règles
strictes qui n'empiètent pas sur l'énergie elle-même ; ce que je suis,
ce que je veux, ce que je décide, tout cela reste de mon domaine.
L'holacratie est une façon -- parmi d'autres -- de poser un cadre assez
strict pour que l'on s'appuie dessus, assez souple pour qu'il évolue et
s'adapte continuellement à l'afflux fluctuant d'énergie. C'est là que
l'holacratie semble avoir réussi un pari.

## Un assemblage de choses connues

{{< image src="/images/2016/04/risotto-3.jpg" title="Risotto" >}}

Permettez moi cependant d'être aigri cinq minutes. Si holacratie ne
s'appelle pas holocratie, je soupçonne que c'est pour une histoire de
marque déposée et de sous.

D'autre part l'holacratie est un assemblage de choses connues car les
cercles de clarification, de réaction, premier lien, second lien sont
des choses héritées de la
[sociocratie](https://fr.wikipedia.org/wiki/Sociocratie), qui avait
décidément un blaze trop difficile à porter. Malheureusement cette
inspiration amène avec elle certains mauvais aspects : dans mon
expérience le facilitateur de la sociocratie possède un pouvoir
déséquilibrant. Il mène les débats trop souvent il ne les accompagne
pas. Je retrouve ce souci avec l'holacratie.

Tous ces appels aux aspects organiques d'une organisation (*bis
repetita*) est salvateur (d'où le holon). J'en suis le premier
défenseur. Dommage de ne pas faire appel aux sources qui portent ces
idées depuis longtemps. En France, [Henri
Laborit](https://fr.wikipedia.org/wiki/Henri_Laborit) dans les soixante
dix brandit ces idées de la thermodynamique des organismes adaptée aux
organisations (Dans "la nouvelle grille" par exemple).

L'agilité fait aussi sans nul doute parti du panthéon de l'holacratie :
rétrospective, syntaxe ("plus que"/"over") et valeurs du manifeste s'y
retrouvent, pourquoi ne pas le dire. Comme un mouvement musical c'est
intéressant de savoir d'où il vient, pourquoi il est là, qu'est ce qui
le constitue, on l'appréhende et en profite beaucoup mieux.

Cette clarification exacerbée des rôles, des règles, des objectifs ne
sont d'ailleurs pas autre chose que ce que clame depuis d'autres
personnes comme Jane McGonigal ou Dan Mezick, qui eux-mêmes ont rebondi
sur d'autres idées. Je ne cherche pas pas à savoir qui a raison, je
crois qu'une invention n'est jamais une affaire individuelle : plusieurs
ont simultanément les mêmes idées, il y a ceux qui ont l'idée, et ceux
qui savent s'en servir, et ceux qui savent la mettre en oeuvre, etc.
J'aurais aimé un mouvement plus ouvert plus transparent dans ses sources
et dont la volonté affichée (ou qui semble l'être) n'est pas
nécessairement ou en tous cas pas uniquement celle d'en faire de
l'argent.

## Quelques chausses trappes pour éviter de mal commencer

{{< image src="/images/2016/04/risotto-4.jpg" title="Risotto" >}}

L'allégorie de l'avion : Robertson dans son livre ou dans ses
conférences (j'ai pu le voir à Prague fin 2015), utilise cette histoire
personnel comme déclencheur. Il aurait failli mourrir en vol privé de ne
pas avoir tenu compte d'un petit voyant dans son coucou. Son idée
sous-jacente est que tous les voyants sont importants et que le seul
moyen de les appréhender tous c'est de libérer leur autonomie. Que le
moindre petit compteur bloqué dans l'entreprise par manque d'autonomie
peut l'amener au drame. Cette allégorie ne fait pas écho en moi. Elle
amoindri une autre référence auquel je crois beaucoup, celle de la
systémie, le "voir le tout" du Lean. Il aurait pu tourner sa métaphore
ainsi et pourtant c'est l'idée inverse qui surgit. Bref, je reste sur ma
faim, son histoire fait flop (pas l'avion heureusement).

## Le faux effet BD

La [BD d'un cabinet de
conseil](http://labdsurlholacracy.com/bande-dessinee-holacracy) français
quelle bonne idée (IGIS Partner en France). Vraiment. Et pourtant elle
embrouille certains aspects. Quand on lit le livre de Robertson, et
malgré ses innombrables appels du pied à acheter ses services ou ses
produits, il est clair, beaucoup plus clair. Attention donc ne pas vous
arrêtez à la BD, c'est une belle porte d'entrée. Elle mène au vestibule
(et probablement à de nombreuses incompréhensions).

Zappos : une mauvaise référence
-------------------------------

Ah Zappos, la référence ! Pas de chance, la mutation de l'organisation
est menée par une main de fer qui broie les récalcitrants. Difficile
d'avoir une main de fer quand on parle d'évolution constante, évolution
constante rime plutôt à mes yeux avec invitation, expérimentation.
Autant évacuer Zappos des références de l'holacratie, car holacratie ou
pas peu importe, c'est la manière de faire qui laisse perplexe.

Un assemblage abouti, innovant
------------------------------

[{{< image src="/images/2016/04/risotto-5.jpg" title="Risotto" >}}{.image .left
width="400"}](/images/2016/04/risotto-5.jpg)

L'Holacratie demeure cependant un assemblage qui me parait abouti. Dont
j'espère vous parler dans de prochains articles. Pour l'instant je
mettrais en évidence quatre concepts clefs à mes yeux, que l'holacratie
a su porter :

-   Faire du changement une dynamique continue, en acter. Et parler
    plutôt d'évolution (et pas de changement, le mot effraie),
    d'évolution continue.
-   Clarifier jusqu'au rôles, cela dépassionne, clarifie. Peut-être ne
    pas se parler cependant comme des robots... L'application jusqu'au
    boutiste des rôles que je n'ai pas essayé encore me fascine comme
    m'effraie (dans un certains cadre on ne doit pas s'interpeller,
    converser, dialoguer avec nos prénoms, mais en s'interpellant par
    nos noms de rôles), quelque soit l'objectif je veux rester humain.
-   Séparer rôles et âmes : poser un cadre clair, laisser les énergies
    humaines s'y déverser sans les contrôler. C'est l'enjeu de la
    complexité : véritable autonomie dans le cadre.
-   Rappeler systématiquement la raison d'être, en appeler au sens.

Sociocratie, Holacratie, Entreprise libérée ?
---------------------------------------------

[{{< image src="/images/2016/04/risotto-6.jpg" title="Risotto" >}}{.image .left
width="400"}](/images/2016/04/risotto-6.jpg)

Je ne vais pas chercher à faire de différences significatives. La
sociocratie, dont les vrais premiers mouvements furent hollandais
pourrait se retrouver dans les expériences d'entreprises libérées que
l'on retrouve décrites dans les derniers ouvrages comme *reinventing
organisations* (passez les 150 premières pages). Holacratie, descend de
la sociocratie comme l'Homme descend du singe, et est une variation, une
déclinaison des entreprises libérées. Un assemblage particulier que
d'ailleurs je ne vous suggère pas d'appliquer en l'état. Comme toutes
ces approches pour gérer des milieux complexes, elles possèdent leurs
paradoxes : [appliquer tel quel ou laisser émerger une approche
contextualisée](/2015/09/paradoxes-des-transformations-agiles/), qu'il
est difficile de résoudre, mais en tous cas elles ne doivent sûrement
pas être considérées comme des recettes.

D'autres articles à venir donc sur le sujet : notamment notre début
d'application au sein de [beNext](http://www.benextcompany.com) et,
franchement, en accord avec valeur **FUN** disons que nous sommes plutôt
parti pour faire de l'Honolulucratie. A très bientôt mais si vous voulez
vraiment en savoir tout de suite un peu plus sur l'holacratie, lisez
donc les [articles de
Géraldine](http://www.theobserverself.com/revelations/experience-holacratie/)
à ce sujet.

N'hésitez pas à me [contacter](/pages/contact.html) (pour la recette du
risotto ?).

### Feedback

[Dragos](https://twitter.com/ddreptate)
---------------------------------------

\*Il me semble que tu assimiles entreprise libérée (comme je hais ce
nom) et holacratie. Pour moi ce sont deux choses différentes, comme
Agile est different de Scrum. EL, c'est pour moii une philosophie, un
ensemble de principes et de valeurs, une façon de concevoir le monde,
quand la holacratie est un framework qui permet d'implementer une EL,
une façon parmi d'autres. Ce n'est pas une solution en elle meme, c'est
un cadre (rigide, comme Scrum) qui donne des règles mais aussi de
l'espace pour chercher la bonne solution, celle qui est adapté au
contexte. La rigidité du cadre, comme toujours, est la pour garantir que
nous n'allons pas transgresser les principes et les valeurs auxquels
nous tenons. Comme toujours, l'echelle de shu ha ri s'applique aussi
ici. Mieux vaut commencer en respectant le cadre, pour ensuite aller au
delà. C'est le cas de ce qui c'est passé chez Medium (bon article sur le
sujet
<https://medium.com/the-story/management-and-organization-at-medium-2228cc9d93e9#.1n75tjfu2)*>

[Fabrice](https://twitter.com/_fbentz)
--------------------------------------

*Salut Pablo, j'apprécie beaucoup ton article et ta vision de la chose.
Pour être dedans depuis 2 ans, et développer un outil pour (holaSpirit).
D'ailleurs, je vois que le holon n'a pas été une mauvaise idée. Il me
tarde que ce mouvement mute pour qu'il ne soit plus aussi déposé en
terme de marque, ce qui à ce jour me déçois au plus au point. Après ce
que je trouve dommage c'est que l'holacratie se concentre trop sur ce
qui devrait être et pas sur ce qui est. On ne passe pas en revue sur ce
qui se passe bien, ce que je retrouve dommage. Bref il me tarde de me
retrouver à boire un coup avec toi pour pouvoir en discuter de vive voix
!*

[Paul](https://twitter.com/PGCrismer)
-------------------------------------

*Commentaire à propos de l'article: "Hellocratie Holocratie Holacratie
Honolulucratie" Vous parlez du pouvoir déséquilibrant du facilitateur.
Et vous avez raison : dans un groupe qui garde les stigmates des
organisations pyramidales - avec un "chef" qui "dirige" - le
facilitateur peut recevoir ce pouvoir-là. Ce pouvoir déséquilibrant
dépend, selon moi, aussi bien de la maturité du facilitateur que de
celle du groupe - et de sa capacité à "laisser faire". C'est le rôle du
tour de feedback - en sociocratie - ou des réunions de gouvernance - en
holacratie : un moment où l'on exprime comment on vit l'incarnation du
cadre et comment on peut la faire évoluer au service du collectif. Moi
aussi je ne peux m'empêcher de voir combien des pratiques de sociocratie
ou de holacratie - et leur esprit surtout - peuvent être utiles pour
supporter les groupes auto-organisés que sont les équipes Agiles. Là où
il y a le plus à prendre c'est quand on veut passer à des échelles plus
grandes et coordonner des équipes Agiles. Le principe de "double lien"
me semble tout à fait approprié pour assurer une coordination et un
feedback de qualité entre équipes. Enfin, sur les copyrights et le
business qui découle de ceux-ci... je rêve de méthodes Open Source
telles que Sociocracy 3.0 qui se veut un savant mélange de Sociocratie,
de Holacracy et d'Agilité (<http://sociocracy30.org/>). Encore merci
pour vos billets qui secouent les méninges et permettent d'avancer.*
