﻿---
date: 2019-12-13
slug: regles-ou-principes
title: Règles ou principes, il faut choisir
--- 

On me dit : j'aime bien votre principe de ... ça confirme ma façon de faire ... 

Dans les conversations il est important de bien distinguer les règles et les principes. 

Si on lit les définitions en ligne, on comprend le *quiproquo*, les deux mots semblent s'appeler mutuellement. En y regardant de plus près cependant on notera qu'une règle s'apparente à une prescription, à suivre donc, point par point, et qu'un principe s'apparente à une proposition (souvent fondamentale) qui sert de base à une mise en œuvre (qui n'est pas définie).    

Cette mise en œuvre de cette proposition fondamentale (le principe) pouvant amener des prescriptions, des règles. 

Il convient de comprendre que des principes sous-tendent les règles que l'on applique. Qu'il faudrait donc bien faire le lien, et comprendre les principes, qui normalement changent beaucoup moins souvent que les règles.  

Que l'on peut avoir plein de règles très différentes entre des contextes différents qui pourtant répondent aux mêmes principes.

Cela paraît donc dommageable de démarrer une conversation par une règle. Et pourtant c'est tellement plus facile, car cela ne demande pas d'explication. Une règle cela se prescrit. Nous ne sommes peut-être pas alignés sur la règle, mais peut-être le somme-nous sur le principe. 

(Pyramide de Dilts, niveaux logiques de Bateson, les règles et les principes sont à des niveaux différents. Mais s’il n'y a pas de cohérence, de résonnance, il y a une dissonance. Elle est toujours dommageable.)











