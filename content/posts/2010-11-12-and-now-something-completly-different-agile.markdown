---
date: 2010-11-12T00:00:00Z
slug: and-now-something-completly-different-agile
title: 'And now something completly different : Agile !'
---

On me demande de présenter l'agile en 5mn et de façon cool ("Agile is
cool" trademark Frédéric)... De mon cerveau fiévreux sont sortis 4
slides absolument inexploitables dont je vous fais part. Un lointain
écho à ma maîtrise universitaire sur les ... **Monty Python** (si si,
pour le DEA ce fut le non-sens et l'absurde...).

Les slides :

<script async class="speakerdeck-embed" data-id="4f9cf2c9b3f2f3002200d8ca" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>

Si quelqu'un réussit à les placer qu'il me fasse signe ! Wink ! Wink !
Nudge Nudge ! See what I mean ? Say no more ? ??
