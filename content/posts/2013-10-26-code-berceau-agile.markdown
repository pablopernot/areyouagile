---
date: 2013-10-26T00:00:00Z
slug: le-code-le-berceau-de-lagilite
title: Le code, le berceau de l'agilité
---

Beaucoup pensent que je n'aime pas le code ou les codeurs. Pas du tout.
Grossière erreur. (au passage j'en fus un). Laissons ces gens s'exciter,
et continuons sur le code. Oui le code ce n'est pas l'agile. On applique
la culture agile dans des environnements où le code est essentiel. On
pratique autour du code en rapport avec la culture agile. Mais si le
**code n'est pas l'agile, il en est le berceau** et ce n'est pas une
surprise.

Je m'explique. L'**agile** existe car il a bien fallu trouver quelque
chose quand on s'est rendu compte -fin du siècle dernier- qu'une
approche cartésienne, qu'une rationalisation et une compartimentation à
outrance ne menaient nulle part. Quand on a réalisé que l'on ne pouvait
plus avoir d'approche rationnelle, matricielle mais systémique. Quand on
a réalisé que nous ne vivions pas dans un monde compliqué mais complexe.
Et bien il a fallu trouver d'autres voies. C'est là qu'est apparu
**agile**. Et pour des raisons pas très éloignées, quelques dizaines
d'années avant, **Lean**.

Et ce n'est pas hasard, que ce "**là**", ce fut au beau milieu du code.

En effet l'agilité c'est une révolte pour dire que les choses sont
complexes, systémiques, et surtout pas cartésiennes. L'**agilité c'est
une science molle et non pas une science dure**. Or quel est l'endroit
où l'on s'est le plus trompé sur le sujet : le code, l'informatique. Un
assemblage de 0 et 1 ne pouvaient qu'être cartésien, des 0 et des 1 ne
pouvaient qu'amener du compliqué, rationnel. Or ce n'est pas le cas. On
s'est complètement fourvoyé, et les gens qui se débattaient au beau
milieu, qui ont mesuré l'écart ont fondé le manifeste agile. C'est là où
le scientisme rationel le plus absolu aurait du s'épanouïr qu'il a volé
en éclat. Le code c'est un assemblage complexe d'intuition, d'art, de
savoir faire, de chance et de malchance, pourtant *in fine* basé sur des
0 et des 1. L'agilité est à mes yeux née de cette contradiction.

Pour le Lean je propose le même cheminement. L'approche tayloriste
n'avait plus sa place dans le monde moderne complexe. Le mécanisisme qui
a pu un temps faire son succès a aussi été le berceau -par opposition-
de son alternative moderne, le Lean. Or comme en témoigne l'image plus
haut, tout portait à croire que l'on pouvait rationaliser l'être humain.
