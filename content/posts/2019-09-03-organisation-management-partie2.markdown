﻿---
date: 2019-09-03
slug: probleme-managerial-et-changement-de-postures
title: Problème managérial et changement de postures
--- 


Alors, dans tout cela à quoi faire attention ? Aux personnes naturellement. Aux équipes ? Sûrement pas ! Je me fous royalement des équipes. Les équipes ne m'intéressent pas concernant ce sujet. Rien. Faites sans moi s’il s'agit d'accompagner les équipes. Ce ne sont pas les équipes et les membres des équipes qui vont bloquer cette organisation fictive. Ainsi je me fous des équipes. Les équipes il y en a plein de différentes, certaines fonctionnent, d'autres moins bien, mais on peut généralement agir assez facilement si on a les leviers de l'organisation, et surtout elles veulent très souvent bien faire. Pour réaliser tout cela, je me fous des équipes, car les équipes avanceront si on les laisse avancer, et on saura agir intelligemment sur celles qui bloquent.

Je vous ai dit pourquoi je pensais qu'ils avaient plein de raisons de fuir, de s'opposer, de simuler aux "transformation", même au mouvement. Plein de bonnes raisons. Et ces gens qui s'opposent, qui freinent, on les trouve en partie dans les équipes, mais dans les équipes ils ont un pouvoir de nuisance très limité. Les managers ont un pouvoir de nuisance. Tout le monde va aider à la réussite d'avancer dans cette direction, mais seuls les managers peuvent véritablement bloquer, fuir, éluder, éviter justement.

La mise en mouvement de l'organisation dépend des actes et de la volonté des managers. De tous les managers, du *middle management*, des petits chefs, au *top management*, aux grands chefs (attention aux mots il y a des petits chefs qui sont grands, et des grands chefs qui sont petits). Plus ils sont hauts dans la chaîne alimentaire plus ils ont des leviers ou un pouvoir de nuisance, et plus leur impact et leur importance est forte. Si ce que vous appelez "transformation digitale" échoue je peux vous assurez que la responsabilité en incombe au *top management*, aux chefs. Il n'a pas assez donné de sens, n'a pas assez accompagné, n'a pas assez communiqué, n'a pas assez été présent sur le terrain, n'a pas peut-être eu le courage de virer tout le *middle management*, les petits chefs qui faisaient obstruction, je ne connais pas la raison, mais c'est lui qui a les clefs en main. Peut-être que c'est lui qui n'a pas accompagné ce *middle management*. Mais comme je vous l'ai dit, c'est difficile, car plein de freins compréhensibles sont présents. La seule chose que j'espère en vous disant cela : c'est que vous rappeliez à tout le monde, ou que vous vous rappeliez vous-même si vous êtes un manager : si la mise en mouvement de l'organisation échoue, ce n'est jamais lié aux équipes, mais toujours au management, car quasiment seul le management à le pouvoir de véritablement nuire dès que les structures atteignent une certaine taille.  

Encore une fois, partout au sein de l'entreprise on peut trouver des gens qui fuient, simulent, évitent, pour les raisons évoquées plus haut. Et aussi partout on trouve des gens qui vont aider à la réussite, qui vont avancer. Mais seuls les managers peuvent véritablement bloquer, fuir, éluder, éviter. Seuls les managers ont ce pouvoir. Et plus ils sont haut dans la chaîne alimentaire plus ils l'ont.

Si cela marche, c'est probablement lié à un ensemble d'acteurs, si cela échoue c'est les chefs. Ce n'est pas une formule, c'est mon expérience. Le manager, le chef, à tous les niveaux a un trop gros pouvoir de nuisance (ou de levier) pour ignorer ce fait. 

Alors que doit-il faire ce chef ? Il doit principalement faire deux choses. Il doit se découvrir un nouveau rôle d'encadrant. D'abord lâcher prise : sur le comment, sur les décisions, sur l'action, mais les rendre possibles en prenant soin de nourrir un cadre qui les rendra possibles. Revenons au français : encadrer, donner un cadre, et diriger : donne une direction, aller dans une direction, un sens. 

Ensuite, communiquer et incarner. Être le premier exemple, s'exposer, être en vulnérabilité. 

Être encadrant c'est savoir, en tant que manager, poser et maintenir le cadre qui va permettre à vos personnes de s'engager (dans ce mouvement).

Pourquoi ? Parce que dans le monde complexe dans lequel nous vivons c'est l'engagement des personnes qui va faire la différence. 

L'engagement des personnes va démultiplier la productivité de l'organisation, et sa capacité d'innovation, son intelligence, sa réactivité. La même personne engagée n'a rien à voir avec celle qui traine la patte pour arriver le matin au bureau et dont toute la stratégie de la journée va être de faire semblant intelligemment de faire sans vraiment savoir quoi faire (et comme souvent on la comprend). 

Alors est-ce qu'il y a des façons d'engager. Pas aussi directement. Mais il y a des façons de poser un cadre qui permettra aux personnes qui le désirent de s'engager. Et il y a un domaine où les personnes sont particulièrement engagées parce que le cadre leur permet. Elles essayent elles réessayent, elles communiquent entre elles, elles font tout ce que vous imaginez de bien pour essayer d'aboutir, elles se remettent constamment en question pour progresser. Ce domaine c'est celui des jeux vidéo, des joueurs de jeux vidéo. Alors comme beaucoup d'argent est à la clef, on a étudié ces joueurs pour comprendre leur engagement. C'est simple il repose sur quatre points. 

Premièrement, Il n'y a pas d'engagement et donc pas de performance, s’il n'y a pas un objectif clair, et encore plus si cet objectif est porteur d'un sens qui dépasse cette personne. Sauver la planète Terre de l'invasion de Zorg. On sait ce que l'on veut et pourquoi, et le sens nous dépasse (mais attention le grandiose n'est pas obligatoire, pas de tyrannie de la vision). En tous cas ce n'est pas un objectif personnel. Réussir une transformation digitale, agile, lean. On n'y comprend rien. Pourquoi ? Dans quel but ? Qui sommes-nous ? Qui a envie de se lancer ? Personne. "Réussir à ouvrir de nouveaux marchés en Afrique tout en communiquant nos valeurs", "Faciliter la vie de famille de milliers de ménages en permettant des transactions plus...". Là on va pouvoir commencer à travailler ensemble, réfléchir, essayer, rebondir.

Deuxièmement il n'y a pas d'engagement si il n'y a pas de règles claires qui permettent d'avoir un sentiment de contrôle sur son outil de travail. Dans certains jeux vidéos je peux tuer les loups-garous uniquement avec des balles en argent, et les démons avec une arme magique, si je veux et quand je veux, avec succès ou pas mais je contrôle mon action, je contrôle mon outil de travail. Par contre si comme dans cette organisation fictive, je ne sais pas exactement si j'ai le droit de toucher à cette partie du produit, je ne sais pas exactement qui peut décider des options dans tel domaine, et si à chacune de mes actions je dois demander à d'autres groupes d'intervenir, et bien, autant me dire quoi faire et me voilà désengagé. D'où ces équipes pluridisciplinaires qui sont tant recherchées actuellement, mais qui mettent tant à mal les vieilles structures silotées où l'expertise d'une compétence est plus importante que de délivrer quelque chose, où la structure hiérarchique à pris le dessus sur la création de valeur. Aux managers de lâcher prise pour laisser leurs équipes, pluridisciplinaires, avec un objectif clair, et les moyens d'agir dessus avec le minimum de dépendances, avancer. Avec autonomie. Il faut donc des règles claires qui donnent assez d'autonomie et d'espace à des groupes pluridisciplinaires. 

Troisièmement, pour garder cet engagement il nous faut nous sentir avancer, avoir un sentiment de progrès, donc avoir des informations sur ce que nous produisons régulièrement. Que les retours soient positifs ou négatifs, peu importe, il faut avant toute chose des retours. S’ils sont négatifs, je me doute qu'une équipe pluridisciplinaire autonome saura réagir. Mais si il n'y a jamais de retour d'informations sur ce que nous faisons, sur ce que nous avons produit, à quoi bon faire ? Et si les retours sont noyés dans un flot d'informations, ou incompréhensible, c'est pareil. Normalement on cherche à ce que ces retours soient liés à ce que nous fabriquons concrètement, et donc au sens, à l'objectif donné initialement. Est-ce que le test de cette nouvelle façon de faire a fonctionné sur cette nouvelle population d'utilisateur oui ? non ? Pourquoi ? C'est aussi pour cela que de fabriquer des choses immenses qui ne prennent du sens qu'en toute fin, en plus des problématiques de complexité souvent infranchissables, démobilise tout le monde. Personne ne sait si ce qu'il fabrique marche, vaut le coup, etc. On pense donc les produits différemment. Par petits ensembles priorisés par valeur que l'on délivre au fil de l'eau et dont on mesure l'impact pour valider ou invalider nos hypothèses et continuer d'avancer. Les morceaux que nous délivrons doivent donc eux-mêmes porter du sens, être autonomes, et on revient ainsi encore aux équipes pluridisciplinaires qui sont en mesure de délivrer des choses finies, apprennent au fil de l'eau, mesurent leur impact, en face des organisations par silos qui ne sont pas capables de délivrer quoi que ce soit sans l'autre silo, et ne savent pas vraiment où elles en sont. Ah si, elles en sont à 46%.  

Quatrièmement, et celui-là est difficile à appréhender, c'est l'invitation. Aucun des joueurs de jeux vidéo n'a été obligé de jouer. Idéalement aucun des membres de cette organisation fictive ne devrait être obligé de venir s’impliquer, travailler. Pas facile. On se heurte à une autre réalité, celle de la pyramide de Maslow, il faut bien d’abord avoir un toit, manger, bref... avoir une organisation et un salaire avant de penser engagement. C’est un vaste débat. Mais difficile aujourd’hui d’inviter à travailler, en tous cas pas simple selon les contextes. Je vous invite par exemple à demander aux gens de former eux-mêmes leurs équipes tant qu'elles essayent de livrer au mieux votre produit et je vous invite également à observer les résultats. Ou choisir leurs sujets. Les gens qui ne se sentent pas l'envie devraient pouvoir quitter le navire. Pas facile je vous le disais. Mais, a l'opposé, si vous faites des projets avec des personnes qui n'ont pas envie de les faire, ne soyez pas surpris par certains résultats. Dans la mesure du possible essayez de ne rien imposer, vous désengagez les acteurs qui subissent cette imposition. C'est facile à dire, je sais. Il y a des tâches ingrates que personne ne veut réaliser. Oui, mais on ne veut pas réaliser ces tâches ingrates s’il s'agit de notre unique travail, s'il est imposé, si on nous ne laisse même pas la liberté de choisir comment le réaliser, si on ne nous explique pas pourquoi il faut le réaliser, et si on ne nous laisse pas la liberté de changer le fonctionnement de notre activité, de l'améliorer, ne serait-ce que de l'appréhender différemment. Mais si on constitue une équipe qui travaille sur un domaine, dont on connaît le sens, dont on mesure les résultats régulièrement, pour lequel on nous laisse réfléchir, s'améliorer, changer le fonctionnement, et bien ces tâches ingrates deviennent une partie du quotidien qu'il est beaucoup plus simple à accepter. Et encore plus si on nous a invités à faire partie de cette équipe.      

Les gens qui refusent toutes les invitations comprennent vite que leur place n'est plus ici. Après cela devient un problème sociétal ou légal : ces personnes devraient pouvoir trouver leur bonheur ailleurs, ce qui n'est pas forcément la réalité. Vous pouvez les mettre de côté dans des rôles autres et leur permettre d'accepter une invitation plus tard (il y a une vie à côté de l'organisation qui elle aussi a naturellement un impact). 

Votre rôle de manager c'est d'instaurer ce cadre !

*Une fois cette bascule humaine et personnelle effectuée, de "manager chef" à "manager coach", impossible de revenir en arrière tant le monde apparaît d'une richesse humaine insoupçonnée au départ,* rajoute à juste titre Sophie Lenoir - Reynaud. 

À ces quatre points viennent s'ajouter des règles liées à notre espèce. On ne sait pas véritablement communiquer au delà de quinze mètres (je parle dans une dynamique de groupe locale), une équipe a du mal à fonctionner, à se bâtir au delà de sept, huit, personnes, pareil pour une réunion constructive. Qu'un département ne devrait pas excéder nos capacités cognitives sociales qui semblent avoir une barrière autour de cent cinquante/deux cents vingt relations. Mais tout cela pourrait évoluer.   

Votre rôle de manager c'est d'instaurer ce cadre ! Oui c'est plus facile de donner des tâches de 0,25j à vingt-huit personnes, avec un découpage à la Descartes. Heureusement pour le bonheur de notre espèce, cela ne marche pas. 

Ainsi donc généralement cette organisation fictive dont je parle n'a pas de problème de temps et d’argent. Elle a un problème d’impact de ses collaborateurs qui ne sont pas assez responsabilisés, qui n’ont pas assez d’autonomie, et elle a un problème pour maximiser la valeur : elle n’est pas en capacité de réaction, d’apprentissage vis-à-vis du marché. Elle ne sait pas délivrer des petits morceaux qu'elle mesure régulièrement. 

Tout le rôle des chefs, des managers, c'est de basculer du rôle de donneur d'ordre sur le comment, et le quoi, à celui d'encadrant : celui qui jardine un espace dans lequel ses collaborateurs pourront bien être engagés et avancer, et celui qui montre une direction, un sens, et qui l'incarne : nous sommes cela, nous voulons cela, donc nous nous conduisons ainsi. Pour beaucoup de managers, de chefs, c'est une révolution. Cela implique aussi beaucoup de discipline, savoir beaucoup dire non aussi que certains se rassurent.   

Intermède

J'aime les gens, et donc j'aime les managers. 
Je ne juge pas les personnes. Elles sont bien.
Je juge le bourbier dans lequel le rôle de manager est. 
Et les nuisances que ce rôle provoque. 
N'oubliez pas que c'est le système qui oppose les personnes. 

Fin de l'intermède.   

L’histoire est simple, mais elle se répète trop souvent.

Pierre est un bon organisateur, un bon leader, un bon expert, il mène le groupe avec lequel il travaille. Il devient en quelque sorte leur “manager”, leur “leader”. Puis l’implicite devient explicite : il devient officiellement “manager”. Ses capacités d’analyse, d’organisation, son savoir-faire, son implication, sa capacité de travail en font un bon manager, d’autant qu’il est très sympathique et humain. Au milieu de son équipe, il excelle. Le succès est au rendez-vous et Pierre franchit les échelons. Jusqu’à ce qu’il arrive à la tête du département, de l’organisation. Et là c’est l’échec, incompréhensible. Pourtant Pierre n’a pas changé.

Pierre vient de toucher le principal syndrome de Peters du management.

Pierre est toujours cet homme sympathique et humain, plein de qualités. Mais il est devenu son pire ennemi.

À la tête du département, il y a trop de décisions pour que ses capacités d’analyse suffisent. Pourtant Pierre a toujours réussi avec ses qualités organisationnelles à organiser cela. Il ne va pas en démordre, il va vouloir toujours décider de tout, en tous cas de trop. Trop de décisions, son échec est accentué par l’isolement lié à la position. Son savoir-faire devient un fardeau, car il se sent capable de mieux comprendre, de mieux savoir, il se pense en devoir d’absorber le sujet, de s’en mêler, et personne n’ose l’envoyer bouler. Il sait organiser. Mais encore, il veut tout organiser, n’est-ce pas tout cela qu’on lui demande depuis le début ?

C’est un moment difficile. Soit Pierre ne devrait pas accepter ce job. Soit il va devoir comprendre que cela demande une posture qui s’équilibre très différemment. Ne plus faire, ne plus contrôler, déléguer, ne plus décider (au jour le jour), mais donner la direction, inspirer, rappeler le cadre, et communiquer, communiquer, communiquer, partager, partager, communiquer, partager, inspirer, incarner, communiquer, partager, inspirer, cadrer, déléguer. Rien à voir avec ce qu’on lui demande depuis le début de son cheminement.

Les managers ne sont pas assez sensibilisés à cette rupture.

Je vois plein de gens admirables ne pas s’en sortir.

Et puis Pierre gagne plus d'argent depuis qu'il est manager, chef. Et il a investi du temps. Et comme il s'est lancé dans ce parcours carrière, il voit les prochaines étapes, même si rien ne lui plaît le parcours semble tracé. Autant continuer, c'est un travers de l'esprit dur à combattre. 

Encore une fois. 

Cette organisation fictive veut aller plus vite et pour moins cher. Elle parle toujours du moyen. Elle se trompe toujours de cible. Si il s'agit d'"une transformation digitale ou agile" on comprend qu'elle voudrait aller plus vite pour moins cher. On ne va pas plus vite et moins cher. On va mieux et on engage les personnes. Pour cela on doit savoir qui on est, ce que l'on veut, en quoi nous croyons, et conformer nos actes et notre environnement à cela. Faire d'abord comprendre à tout le management, à tous les chefs, qu'il s'agit d'un grand changement de leur part. Et ensuite, mettre en oeuvre ces équipes hétérogènes, avec de l'autonomie et un feedback régulier sur ce qui a du sens, en mesure de délivrer régulièrement des éléments de valeur, et de s'améliorer. Tout cela ne devient possible que si le management, les chefs, comprennent qu'ils ont basculé dans un autre rôle : façonner, clarifier un beau cadre, qui permet aux équipes d'évoluer. Et incarner, communiquer ce sens, cette identité. 

Et encore je le répète, plus on monte haut dans le management plus c'est important, plus vous êtes responsables. Vous allez me rire au nez et me dire que le grand manitou d'une gigantesque organisation ne peut pas communiquer ni incarner aux yeux de tous ses collaborateurs. D'une part les moyens de communication il me semble le permettre aujourd'hui. D'autre part, si il ne peut pas est-ce qu'il montre qu'il donne assez d'autonomie au "chef local" pour que celui-ci puisse prendre les bonnes décisions, créer le bon cadre pour ces équipes, ait lui-même assez d'autonomie ? "Non on ne peut pas parce que le grand manitou ne veut pas" c'est audible si le grand manitou est venu l'expliquer d'une façon ou d'une autre, qu'il a expliqué le sens de ce refus, que cette explication est en conformité avec l'identité de l'organisation. Sinon bonjour le désengagement.   

C'est fatigant d'incarner, de communiquer. Surtout si comme Pierre on a perdu le sens au fil de l'évolution de carrière. Et puis si on est monté en grade, c'est probablement que l'on a déjà donné, on voudrait reprendre un peu. 

Le manager n'est plus au centre, il est à la frontière : il est le gardien du cadre. 

Je me fous que vos équations soient justes, le management est une science molle. Être manager c'est incarner l'identité et le sens de votre 
organisation. On peut décider que l'identité de l'organisation change, mais là aussi il faudra communiquer, expliquer. Être manager c'est fatigant c'est pour cela que vous êtes plus payé.  

Tout à l'heure je parlais de virer des personnes, qui est un acte important, pas simple, et qui fait du bien à l'organisation si il est fait avec raison, et bien fait. Ce que j'ai pu observer à un ou deux endroit c'est que un quart du *middle management* s'accoutumait très bien à cette nouvelle façon de faire, que autre quart du management devenait coachs : s'accoutumait encore mieux ? Un autre quart quittait, car il ne se retrouvait soit pas dans la nouvelle façon de faire, soit dans la direction;, l'identité, qui ont été clarifiées, enfin j'ai observé le dernier quart du management qui se faisait expulser, car jugé toxique. 

---

* Première partie : [Pourquoi souvent personne ne veut mettre son organisation en mouvement ?](/2019/09/pourquoi-souvent-personne-ne-veut-mettre-son-organisation-en-mouvement/)
* Deuxième partie : [Problème managérial et changement de postures](/2019/09/probleme-managerial-et-changement-de-postures/)
* Troisième partie : [Introspection, exposition et cohérence](/2019/09/introspection-exposition-et-coherence/)


