---
date: 2018-05-15
slug: les-deux-chaos
title: "Les deux chaos"
---

Le chaos est une étape nécessaire. 


C'est dur à accepter pour certains d'entre nous. 


Mais quoi qu'il arrive lors d'un changement conséquent personnel ou organisationnel, il y a ou il y aura du chaos. 


La question étant de savoir de quel chaos il s'agit. 


A mes yeux un des chaos est celui qui a été créé par votre volonté de changer les choses, votre prise de risque, votre pas de coté ou en avant, votre impulsion, votre désir. Vous êtes sorti de votre zone de confort pour une bonne raison, avec envie. Et maintenant il faut (re)découvrir, (re)bâtir, faire émerger. 


L'autre chaos serait celui que vous subissez. Un état de fait, quelque chose qui se désagrège. C'est le chaos car ce qui fonctionnait avant ne fonctionne plus. 


Ce sont deux chaos, mais ils sont bien différents. L'un est une soupe comme celle du début des temps, il va en sortir mille choses, l'autre est un sable mouvant qui vous asphyxie. Mais les gens ne font pas nécessairement la différence et accueillent souvent  les deux de la même façon.


Dans les organisations je côtoie ces deux chaos. Le chaos des sables mouvants essaye de sauver les apparences, de réparer, de figer, le chaos d'émergence cherche à construire, à découvrir, révéler, quelque chose. Leurs forces sont opposées. Elles s'annulent ou s'affaiblissent mutuellement. On ne répare rien ou peu, on ne construit rien ou peu. L'énergie gaspillée est trop grande.  


On dépense bien trop d'énergie dans ces forces contraires. Dans les organisations ce gaspillage est fatal. On ne sait pas sur quel pied danser. Peu importe si on répare, fige, protège, ou si on crée, découvre, essaye, quand les directions sont contraires on gaspille une énergie folle pour peu de résultats. Il faut choisir son chaos. Et envoyer un signal fort. 


Et le signal indique la direction. 


Et la direction indique le signal. 


Je défends depuis longtemps ces transformations par émergence qui se nourrissent de l'invitation, de l'adaptation. Mais j'ai besoin de renforcer toujours plus le signal : nous voulons nous transformer, ce qui veut dire en fait, nous acceptons et nous voulons évoluer continuellement en accord avec le monde qui nous entoure. Ce signal il doit être donné par ceux qui possèdent les leviers, ceux qui tiennent la barre, ceux du management, je veux parler du "top management", ceux qui ont le pouvoir. Sans signal clair trop d'énergie est dépensée en dynamique contradictoire. Trop d'énergie dépensée dans des chaos contradictoires. 


Je reviens ces derniers mois sur l'idée que le meilleur des signaux c'est le "big bang". Par "big bang" j'entends que l'on indique clairement que l'on doit changer, et partout : il faut évacuer le chaos des sables mouvants, et embrasser le chaos de l'émergence.


Le "big bang" ne veut surtout pas dire : on passe d'un état À à un état B. Bam, d'un coup, lors d'une grosse explosion. Non on ne sait pas ce que sera B. Mais on veut tous quitter A, on veut tous aller vers B. Comme je le pense toujours le chemin vers B (puis C, puis D, c'est une évolution continuelle) sera émergent (d'où l'invitation, l'adaptation). Et le premier pas de A vers B c'est l'acceptation du chaos. Et la première manifestation de ce chaos positif, l'ouverture en quelque sorte, c'est la clarification, le signal que A est définitivement derrière nous. Le "big bang" c'est ce signal irrévocable. Les trois coups de canne sur le sol du théâtre. 


Quand cela se produit toutes les énergies sont dirigées dans la bonne direction. Le chaos apparaît comme cette soupe si pleine de richesse. Chaque difficulté possède une valeur. 


Ainsi parfois un mode "big bang" vient générer un chaos salvateur sur lequel une vraie émergence pourra se nourrir plutôt que de vivre dans le chaos d'une organisation inadaptée.

