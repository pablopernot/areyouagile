---
date: 2010-09-26T00:00:00Z
slug: scrum-ne-faites-pas-de-sentiments
title: 'Scrum : Ne faites pas de sentiments'
---

Sous ce titre un brin provocateur je ne recherche pas l'affrontement. Au
contraire c'est la neutralité que je souhaite mettre en exergue. Ne
faites pas de sentiment : soyez totalement neutre.

Exemple, vous déployez Scrum au sein d'une organisation ayant pignon sur
rue (disons un grand compte), avec des habitudes, des méthodes, des gens
qui ne vous ont pas attendu pour délivrer un produit. Mais vous voilà
qui débarque avec votre "agilité".

Support, documentation, QA viennent aux nouvelles.

Qu'en sera-t-il des spécifications (sur lesquelles la documentation se
fonde), qu'en sera-t-il des tests ? qu'en sera-t-il de la documentation
? etc. etc. Ma position est simple (ce qui ne veut pas dire qu'elle est
facile à tenir) : voici l'état des lieux, voici nos objectifs dictés par
la création de valeur, voici notre planning, voici le périmètre actuel,
voici la méthode, etc. Vous estimez que nous devons intégrer ce niveau
de documentation pour pouvoir vendre le produit ? Très bien, j'en fais
part à l'équipe et au product owner (ou mieux : aller leur en parler !).
Ce dernier décidera (probablement pas seul d'ailleurs, il est le
représentant investi des pouvoirs d'un groupe de travail plus vaste) de
la priorité à donner à cette création de valeur. L'équipe proposera
peut-être d'intégrer ce point dans leur notion de "fini". Il faudra
probablement se demander si la documentation que vous réclamez est
réellement porteuse de valeur. Pourquoi et comment.

Je n'ai pas la réponse, vous avez réponse, ils ont la réponse. (Le
consultant dans sa pose la plus obscène s'écrient certains).

Nous affichons clairement nos objectifs, nos priorités, le contexte, le
calendrier, la notion de "fini", etc. On peut essayer des choses si on
n'est pas sur de la bonne solution. On analysera vite si cela a
fonctionné ou non.

Transparence, Inspection, Adaptation.

Je réponds finalement souvent : "je ne sais pas, qu'en penses-tu ?". Mon
rôle est d'accompagner, protéger, garantir, expliquer, faciliter, lever
les ambiguïtés, mettre en évidence, etc (sur ma mission actuelle et qui
sert de décor à ce post j'ai un rôle ambivalent entre le coach agile et
le scrummaster, certains vont crier à l'hérésie, *perseverare
diabolicum*). Que les arbitrages et les priorités soient clairs, basés
sur les bons indicateurs, communiqués. Que le droit à l'erreur et à
l'essai soient permis.

Je n'ai pas de parti pris, pas de sentiment, je suis neutre.

Il m'arrive aussi de dire : à telle date nous aurons un feedback assez
conséquent pour estimer si cela marche ou si c'est un échec (sous
entendu le déploiement de Scrum fonctionne-t-il ou est-il un échec ?).
Nous prendrons une décision en fonction d'indicateurs clairs et
transparents.

Transparence, Inspection, Adaptation.

Cela peut irriter. Et là tout dépend de la psychologie (ouh là là comme
c'est galvaudé) du Scrummaster ou du coach agile. Est-ce pour cela que
Ken Schwaber dans une [célèbre
vidéo](http://www.youtube.com/watch?v=IyNPeTn8fpo) renomme le
scrummaster, *the prick*... (la piqûre, mais en argot : la bite, la
queue...) ? Oui très certainement, nous pouvons irriter.

Attention, un dernier point, rester neutre ne veut pas dire ne pas
encourager ou féliciter l'équipe, ou au contraire lui dire que l'on est
surpris ou déçu dans certains cas.
