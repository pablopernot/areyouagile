---
date: 2011-12-01T00:00:00Z
slug: quelques-bouquins-a-lire-pour-faire-de-lagile
title: Quelques bouquins à lire pour faire de l'agile
---

Je raffole des bouquins (pour le boulot ou autres), et je surpris du
désamour pour les livres que je constate sur le terrain. Peu de gens
lisent. C'est bien dommage. Les gens qui lisent me posent souvent des
questions très pertinentes. Et puis lire un bouquin tranquille dans son
lit, relax dans son bain, à l'aise sur la plage, calmement dans le
train, ça n'a pas de prix. (un vrai bouquin, un truc que vous tordez,
marquez, machônnez, etc.)

L'agile propose pléthore de livres, de nombreux de qualités. Je vous
donne mes suggestions et en quelques lignes pourquoi je vous recommande
ceux-ci.

Bonnes lectures.

## [Claude Aubry, Scrum](http://www.aubryconseil.com/)

{{< image src="/images/2011/12/claudeaubry.jpg" title="Claude Aubry" >}}

Je recommande toujours le bouquin de Claude d'abord car il est bien. Il
sert autant à découvrir la méthode Scrum, qu'il peut servir de livre de
référence quand on a besoin de piocher des informations ou de se
remémorer certains détails. Je le recommande aussi beaucoup car il est
en français et j'évite donc ainsi plein de quiproquos avec des gens qui
ne maitrisent pas assez bien l'anglais.Ce n'est pas grave mais du coup
ils n'osent pas me dire qu'ils ne lisent pas les références que je leur
donne en raison de l'anglais... avec le bouquin de Claude je fais coup
double : il est bon et en français.

*en français*


## [Mike Cohn, Agile estimating & planning](http://www.mountaingoatsoftware.com/)

{{< image src="/images/2011/12/mike_cohn_estimating.jpg" title="Mike Cohn" >}}

Mike Cohn est très pédagogue. Ses livres se lisent comme du petit lait.
Je ne suis pas à l'aise avec tout (notamment l'utilisation permanente de
la "business value" n'est pas simple, de la valeur oui, de la "business
value" c'est plus dur). Si vous commencez par le livre de Claude,
celui-ci constitue un complément très intéressant pour les **product
owners ** notamment.

*en anglais*


## [Mary Poppendieck, Lean Software Development](http://www.poppendieck.com/)

{{< image src="/images/2011/12/leansoftwaredeveloppement.jpg" title="Mary Poppendieck" >}}

Mon favori. Une approche globale sur **Lean ** et son application dans
le domaine du logiciel. J'aime tout.

*en anglais*

## Extreme Programming Explained, Kent Beck

{{< image src="/images/2011/12/extremeprogrammingexplained.jpg" title="XP Explained" >}}

Un truc comme du cuir dur, difficile à tordre, mais qui résiste au temps
et que l'on consulte avec plaisir. Pour se plonger dans les sources de
l'**Extreme Programming**.

*en anglais*

## Hell's Angels, Hunter Thompson

{{< image src="/images/2011/12/hells-angels1.jpg" title="Hell's Angels" >}}

Un retour d'expérience concernant un coach agile plongé au milieu d'un
gang de Hell's Angels. Indispensable. D'autant que je prédis
malheureusement la collusion massive et prochaine du coaching agile et
du [journalisme gonzo](http://fr.wikipedia.org/wiki/Journalisme_gonzo) (des signaux
forts en ce sens en ce moment).

*en anglais ou français*


## [Agile Retrospectives, Esther derby](http://www.estherderby.com/)

{{< image src="/images/2011/12/estherderby.jpg" title="Retrospective" >}}

La rétrospective est vraiment l'élément clef de l'agilité (tel que je le
conçois). Ce livre permet de renouveler le genre, nos formats, et de
pousser plus loin nos équipes.

*en anglais*


## [Management 3.0, Jurgen Appelo](http://www.noop.nl/)

{{< image src="/images/2011/12/appelo_management-3.0.jpg" title="Management" >}}

Joker ! Je ne l'ai pas lu. Je suis donc en train de vous suggérer
fortement de lire un livre que je n'ai pas encore lu ! (quel culot !) Il
est sur ma table de nuit et tous les retours que j'en ai me laissent
penser qu'il est lui aussi indispensable à tous les (futurs) managers
agiles. Je pense donc qu'il aurait été encore plus dommageable de ne pas
le lister. Promis je fais une chronique dès sa lecture (décembre/janvier
?)

*en anglais*

### Lectures, les liens depuis 2010

-   [Lectures automne/hiver 2014](/2014/03/lectures-automnehiver-2014/)
-   [Fiche de lecture 2013 : entre le cristal & la fumée (Henri
    Atlan)](/2013/12/auto-organisation-et-storytelling/)
-   [Lectures automne 2013](/2013/10/quelques-lectures-recentes-2013/)
-   [Lectures automne/hiver 2013](/2013/04/lectures-automnehiver-2013/)
-   [Lectures printemps/été 2012](/2012/10/lectures-printempsete-2012/)
-   [L'empire des coachs (Gori &
    LeCoz)](/2012/04/lecture-lempire-des-coachs-de-gori-le-coz-2006/)
-   [Quelques bouquins à lire pour faire de
    l'agile (2011)](/2011/12/quelques-bouquins-a-lire-pour-faire-de-lagile/)
-   [Fiche de lecture 2010 : Karmic
    Management](/2010/08/fiche-de-lecture-karmic-management-roachmcnallygordon/)
-   [Fiche de lecture 2010 : Scrum (de Claude
    Aubry)](/2010/08/fiche-de-lecture-scrum-guide-pratique-claude-aubry/)

