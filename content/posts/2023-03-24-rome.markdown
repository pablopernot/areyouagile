---
date: 2023-03-24
slug: rome-ne-s-est-pas-construite-en-un-jour 
title: "Rome ne s'est pas construite en un jour"
draft: false
---

1. C'est par la somme des petits actes et des postures quotidiennes que votre environnement se bâtit. 

2. En bien ou en mal. 

3. On peut décider en une seconde de changer, mais cela va prendre du temps. 

4. On ne peut décider de tout changer en une seconde, cela se nourrit de temps. 

5. Si vous le souhaitez, commencez dès à présent. 

6. Si on change tout le temps, on ne bâtit rien. 

7. Si vous ne changez jamais un jour cela va s'effondrer. 

8. Il ne s'agit souvent pas de faire des efforts, mais simplement d'être. 

9. On croit souvent qu'il est plus dur d'être que de faire des efforts pour paraître, mais c'est bien l'inverse. (C'est plus usant d'être faux si tu préfères).

10. On ne vous demande pas d'être contre ou pour, mais d'être ce que en quoi vous croyez, vous verrez bien ce que cela construit. 

11. C'est avec la persistance que vous bâtissez des choses, mais la persistance associée à un verbe transitif : vous ne cessez d'essayer **QUOI** notamment. (Sans le quoi ce n'est probablement pas un verbe transitif). Vous pouvez persister à ronger, à vérifier, etc. C'est dans la persistance qu'une grandeur peut naitre. 

12. On ne doit pas être surpris d'une situation ou se plaindre (trop, on peut se plaindre hein, on est loin de tout maitriser) car tous les actes précédents y mènent, en tous cas mènent à ce que vous êtes dans cette situation. 

13. Si vous vous plaignez d'une situation retournez au point un. 

14. Rien n'est facile, à part écrire cela. Mais essayez. 

