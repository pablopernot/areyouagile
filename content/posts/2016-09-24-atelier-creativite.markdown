---
date: 2016-09-24T00:00:00Z
slug: atelier-creativite
title: Atelier créativité
---

Je vous décris ici un petit atelier créativité réalisé récemment d'une
durée d'une heure. Si quelqu'un veut essayer le même genre de chose,
merci de me donner vos retours. Mais comme souvent, un blog c'est
surtout pour soi, pour poser ses idées, les cristalliser, laisser sécher
l'argile en quelque sorte. Donc prenez ce petit billet comme une note
pour moi-même. Pour cet atelier, j'étais ravi de me retrouver au [Bœuf
sur le toit](https://fr.wikipedia.org/wiki/Le_B%C5%93uf_sur_le_toit), en
plein Art Déco.

{{< image src="/images/2016/09/boeufsurletoit.jpg" title="Boeuf sur le toit" >}}

[Boeuf sur le toit](https://fr.wikipedia.org/wiki/Le_B%C5%93uf_sur_le_toit)

*C'est art Déco quand les lignes sont droites -- voir le lustre la haut
-- et art nouveaux quand lignes sont arrondies. Petit mémo technique.
Exemple d'arts nouveaux : la casa Batllo de Gaudi à Barcelone. Si tu
captes ça tu ne pourras plus te tromper*, me rappelle
[Lahouari](http://www.salondemontrouge.com/80-lahouari-mohammed-bakir.htm),
mon vieil ami, quand je l'interroge sur le sujet en écrivant ce billet
("art déco ? arts modernes ? arts nouveaux ?").

Ravi car effet dans mes précédents billets j'évoquais les surréalistes
pour penser la créativité, et voilà que je me retrouvais dans l'antre
des dadaïstes parisiens pour m'y atteler. Cet atelier créativité était
le point final d'une demi-journée de *team building*.

## \#1 Faire s'entrechoquer les concepts sans honte

L'objectif était de trouver en une heure le nom de la nouvelle
organisation (il s'agit d'une fusion de deux structures), ou du moins
d'apporter de sérieuses pistes. J'ai décidé comme évoqué dans un
[précédent article](/2016/08/transformation-digitale-et-creativite/) de
préparer les esprits rapidement en posant les points clefs de la
créativité, et en racontant l'histoire des ours pour **permettre à tous
d'être ridicule sans crainte, outrancier sans honte**, puis d'utiliser
mon [espace des mèmes](/2015/07/lespace-des-memes/) pour faire
s'entrechoquer les concepts.

L'idée c'est d'abord comme les dadaïstes de jouer avec les idées reçues,
les conventions, la logique, la raison, en mélangeant les concepts sans
frein. J'affiche donc 3 listes à l'écran : une contient des adjectifs
sans queue ni tête : *Vert, Bleu, Insensé, Germe, Transparent,
Moisissure, Prompt, Arrondi, Instantané, Inflexible, Coloré, Inversé,
Retourné, Invisible, Apparent, Superficiel, Virtuose, Renflé, Connexe,
Rangé, Propre, Granuleux, Adhérent, Visqueux, Rebondi, Beau, Long,
Court, Souterrain, Volant, Télépathie, Télékinésie, Flocon d'avoine,
Cuillère*.

{{< image src="/images/2016/09/groupe-creativite-1.jpg" title="Groupe créativité" >}}

L'autre contient des noms de société ou de concepts modernes ou
considérés comme (c'est un peu l'idée de la sauce magique du Lean
Canvas, "devenir le netflix des arts ménagers") : *Spotify, Netflix,
Amazon, Tesla, NASA, Playstation, Minecraft, AirBnB, Uber, Angry Birds,
Pokemon, Instagram, Youtube, Whatsapp, Tinder, Happn, Mobile, Tablette,
Objet connecté, Drone, Occulus rift, 3D, Lego, Playmobile, Snapchat,
GoogleMaps, Waze, Twitter, Voiture sans pilote, IOT, Réalité augmentée,
3D Print, Blockchain, Robot, Wearables, Machine Learning*.

Enfin une troisième liste qui contient plein de mots clefs décrivant la
nature ou le métier des deux structures actuelles. Je donne **15mn à
chaque groupe pour associer trois mots (un mot de chaque colonne) et me
raconter une histoire dont ils seraient fiers concernant leur
organisation dans 5/10 années**. Chaque groupe restitue aux autres sa
proposition.

## \#2 Se sentir investi par quelque chose de grand, qui nous dépasse

{{< image src="/images/2016/09/groupe-creativite-2.jpg" title="Groupe créativité" >}}

Rappelez vous l'[allégorie du bâtisseur de cathédrales](/2016/07/faciliter-ou-batir/), il est important de donner
du sens, et de travailler pour quelque chose qui nous dépasse, ou plutôt
qui nous fait nous dépasser. Je demande donc ensuite une **deuxième
conversation de 15mn par groupe, pour me donner trois mots, trois
concepts clefs liés à leurs histoires qui synthétisent pourquoi ils sont
transcendés, sublimés, fiers de cette projection concernant leur
organisation**. Chaque groupe restitue aux autres son histoire et
pourquoi en trois mots clefs ils seraient fiers de cet achèvement pour
leur organisation.

## \#3 Donner un nom à ce sens

L'idée est bien évidemment d'abord de faire émerger quelque chose de
nouveau (choc des concepts) au travers d'une histoire sur laquelle on
peut se projeter. Puis d'y placer du sens, ou de comprendre pourquoi on
y trouve du sens. Avec trois mots le ramener à l'essentiel. La
conversation au sein du groupe est très importante, elle permet
d'harmoniser les compréhensions, les attentes, etc. Enfin dans un
troisième temps, **chaque groupe a 15 nouvelles minutes pour inventer un
nom qui représente bien cette histoire et ces trois concepts clefs qui
font sens**. **Chaque groupe restitue son triptyque : histoire, mots
clefs, nom**. Il y avait un cahiers des charges (langues, formalisme,
longueur mini et maxi, certaines lettres, etc.) mais je ne voulais
surtout pas que les gens se lancent dans un *brainstorming* sur le nom
directement, je souhaitais donner du sens. Si une organisation possède
un sens et que ce sens est partagé, elle possède un avantage indéniable
sur celles qui n'en n'ont pas. Tout ce qui est porteur de sens est un
plus.
