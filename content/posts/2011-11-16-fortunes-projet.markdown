---
date: 2011-11-16T00:00:00Z
slug: fortunes-projet
title: Fortunes projet
---

En écho à l'article concernant [une convergence possible (ou pas) entre agile et cmmi](/2011/10/agile-cmmi-potion-magique-ou-grand-fosse/), vous trouverez ici les fortunes CMMi que j'avais évoqué. J'ai décidé de les
renommer "fortunes projet" car je les ai passablement reformulées, et pour ne pas chagriner les anti de tous poils.

L'idée derrière ces fortunes est de donner aux équipes des éléments de
réflexion pour les aider à la réussite de leurs projets (qu'ils soient
agiles, classiques, ou autre).

{{< image src="/images/2011/11/fortune-300x225.jpg" title="Fortune" >}}

Vous trouverez donc ici :

[fortune-0-3.pdf](/images/2011/11/areyouagile_fortune_0_3.pdf)

Les fortunes et une proposition de mode opératoire, mais sur ce dernier
point je fais surtout appel à votre imagination, et je serai friand de
vos retours.

ps : l'image du "make love, not bugs" vient de
[là](http://mysqldump.azundris.com/archives/67-Fortune-Cookie.html).
