---
date: 2022-09-12
slug: produit-de-demain
title: "Produit de demain"
draft: false
---


Est-ce que je sais à quoi ressemble le produit de demain ? Non, par définition c'est le produit de demain, je ne sais pas aujourd'hui à quoi il ressemble. Mais alors peut-on s'appuyer sur [les dernières années pour imaginer quelques pistes](/2022/07/produit-de-demain-preambule/) ? Peut-être, j'y reviens plus bas.  

Qu'est-ce que j'aurais envie de vous dire sur le produit de demain ? 

D'abord des bonnes nouvelles dans cette anxiété qui grandit : 

D'une part pour concevoir les produits de demain nous sommes très bien armés. Imaginer un produit, et donc un monde viable, soutenable, voire une époque réparatrice va faire appel à des compétences dans la pensée complexe, dans la *systémie*, une compréhension des mécanismes de l'émergence, un travail sur le changement de culture et d'état d'esprit. Et pour tout cela nous sommes très bien armés, nous, les "agilistes", les "coachs", les "systémiciens", etc. 

D'autre part la question de l'écoresponsabilité nous projette dans le niveau 5 de [tribal leadership](/2012/10/lectures-printempsete-2012/). Ça veut dire quoi ? Rapidement, ce bon bouquin dresse deux idées utiles : celle de la dynamique des triades, et celle des "niveaux d'ouverture à la vie" (je décide d'appeler cela ainsi). Le niveau 1 étant que la vie est une galère, le niveau 2 étant que JE suis un raté(e), le niveau 3 étant que les choses réussissent grâce à MOI (on est beaucoup là), le niveau 4 étant que NOUS sommes fortiches (versus les autres), et enfin le niveau 5 que la vie est belle et autant que tous s'y épanouissent. Pour moi comme pour beaucoup c'est souvent des allers/retours entre 3 et 4. Grâce à cette question de l'écoresponsabilité je peux enfin atteindre sans sourciller le niveau 5. Que mon concurrent direct réussisse mieux encore que moi sur le sujet, grand bien lui fasse, c'est une très bonne nouvelle. Chaque initiative, bonne ou vouée à l'échec, demeure une bonne nouvelle. Et c'est très agréable.    

Alors ce produit de demain, qui par définition est de demain ? 3 axes pour le voir se produire à mes yeux aujourd'hui.  

## Apprendre

Si l'on se repenche sur cette question de disruption ([produit de demain - préambule](/2022/07/produit-de-demain-preambule/)) on notera que la technologie comme connaissance s'est révélée clef pour retourner des marchés, créer des ruptures (vous savez Marc Andreessen "Why software is eating the world"). D'ailleurs Marty Cagan, l'actuel pape du *product management* ne s'y trompe et dédit un chapitre de son dernier "empowered" sur le rôle clef de la technologie. Faire des produits aujourd'hui sans expertises technologiques c'est une difficulté et un risque énorme. Il raconte : 

*Souvent[^1] quand j'ai un dîner avec l'un des grands chefs d'une compagnie qui ne comprend pas l'importance d'être expert sur la technologie, ils me disent qu'ils ne sont PAS une compagnie sur la technologie, qu'ils sont une compagnie d'assurance, ou sur la santé, ou sur l'agriculture. Alors je leur dis "Laissez-moi vous dire ce que je ferais  si j'étais un responsable produit chez Amazon ou Apple, et que nous décidions d'attaquer votre marché parce que nous croyions qu'il est grand et mal servi, et ce que la technologie sera capable de faire pour grandement améliorer l'expérience de vos utilisateurs". -- Marty Cagan, Empowered.*

[^1]: Often when I'm having a dinner with one of the CEOs from a company that doesn't get this, they tell me how hey're *not* technology company -- they're an insurance company, or a health care company, or an agricultural company. I'll say "Let me tell you what I would do if I as a product leader at Amazon or Apple, and we've decided to go after your market because we believe it is large and underserved, and that technology is avaiblable that enables dramatically better solutions for your customers". -- Marty Cagan, Empowered

Sans cette connaissance, pas de capacité de rupture. Pas de capacité de réponse adéquate. 

Pour ce que je vais draper du terme générique "écoresponsabilité" c'est pareil, ceux qui n'ont pas de connaissance, pas d'expertise, vont devenir complètement désuets. Il est impératif de former le maximum de personnes pour avoir les moyens de penser ces produits de demain, de s'ouvrir. Pour ceux qui ont cette connaissance, de nouveaux marchés s'ouvrent. Cela paraît évident ? Tant mieux. On parle de former les ministres du gouvernement et on voit les réticences, les freins ou le peu d'engagements. 

Avoir une expertise sur l'écoresponsabilité cela veut dire quoi ? Ça veut dire acquérir une connaissance forte sur le sujet pour avoir assez d'éléments en main pour devenir créatif, inventif. Quelques exemples. 

On peut apprendre du passé. [Alexis Nicolas](https://www.youtube.com/watch?v=-dUUNXf4jPg) me racontait, par exemple, que la chaîne à vélo avait été inventée quelque temps avant la machine à vapeur. Et finalement cette dernière avait provoqué une telle disruption que la chaine à vélo avait été très sous-exploitée. Or le principe de la chaine (à vélo) est très écoresponsable. Celui de la machine à vapeur moins... Fouillons dans notre passé avec un regard neuf. 

On peut apprendre des autres. Toutes nos interrogations de demain sont très centrées sur nos nombrils d'occidentaux. Car finalement si nous basculions tous vers des usages d'aujourd'hui venant d'autres endroits du monde la problématique énergétique disparaîtrait. Le pas semble énorme. Nous avons vu Fosbury sauter, cela parait impossible de revenir en arrière concernant nos façons de vivre (sauf sous la contrainte, mais d'ailleurs c'est comme cela que je me mesure : au niveau de contrainte que je m'impose). Mais que pourrait-on apprendre de ces autres lieux ? 

On peut apprendre la frugalité technologique : qu'est-ce qui émet du carbone ? Qu'est-ce qui réduit notre empreinte carbone ? Comment propager à d'autres des dynamiques de frugalité ? 

Bien distinguer la différence entre la réduction des émissions, et la compensation des émissions. Aujourd'hui, il ne suffit pas d'arrêter, nous allons devoir réparer. 

On peut apprendre à travailler sur des zones intermédiaires, sur le législatif : comment revendre nos crédits carbone pour faire d'autres choix (Tesla par exemple fait cela et cela est une source fondamentale de son bilan) ? Les sociétés à mission vont être soutenues par une loi PACTE qui va engager les entreprises à utiliser leurs services, en devenir une ? En quoi l'imposition de la triple comptabilité (finance, nature/environnement, sociale) va-t-elle changer la donne ? Nous avons besoin de savoir tout cela pour avancer dans cette nouvelle disruption qui nous oblige. 

On peut apprendre à avoir une bonne compréhension autour de la notion de *systémie*, qui permet de *voir le tout*, que comprendre que les optimisations locales se révèlent souvent fausses, que les interactions sont plus importantes que les acteurs du système eux-mêmes. Que tout est lié : arrêter une centrale nucléaire ici déclenche un usage du charbon là par exemple. 

Donc je commence par une évidence, mais il faut apprendre pour se donner les moyens de continuer à avoir de bonnes idées de produit. Une connaissance, une expertise sur le monde de l'écoresponsabilité est comme aujourd'hui une connaissance, une expertise technologique ;  sans cela, peu d'horizons s'ouvrent.  

## S'étalonner 

Après la connaissance, la donnée, que se passe-t-il vraiment ? Là aussi, les "connoisseurs" de bons ou beaux produits savent. Sans la donnée on est un peu aveugle, on ne sait où avancer. Pour les produits de demain c'est pareil, il me semble d'abord important de s'étalonner, pas véritablement pour savoir, mais pour se sensibiliser, pour pouvoir articuler une conversation. 

Notre vrai travail est celui de la sensibilisation. Le produit de demain comme celui d'aujourd'hui façonnera notre monde. Pour le penser différemment, il faut changer notre posture, notre regard, nos perspectives. Pour penser différemment nos produits, il faut savoir, et il faut se tourner, prendre de nouvelles perspectives. 

Je ne suis pas en accord avec les positions de certains qui veulent chercher des poux à des initiatives plus symboliques qu'efficaces. Je crois à l'intention et l'attitude, les symboles sont les premiers pas nécessaires avant qu'ils ne se muent en réalité. 


### Triple comptabilité

Ce que je propose pour commencer à s'étalonner ce sont deux outils : le premier est la triple comptabilité ("triple bottom line"). C'est quelque chose de très sérieux qui va arriver tôt ou tard officiellement sur le plan européen au niveau des entreprises. Il s'agit de consolider nos budgets pas uniquement financiers, mais aussi sociaux et écologiques (vis-à-vis de la nature). Peu importe que vous le fassiez dans les règles de l'art, l'important est de commencer à mesurer non pas uniquement financièrement, mais socialement et écologiquement.  

Un exemple d'un cabinet de conseil pioché sur internet : encore une fois, faites juste l'exercice sur vos produits de consolider leurs aspects financiers, sociétaux et écologiques. Cela mettra le doigt sur des forces ou des faiblesses qui vont commencer à orienter votre esprit vers des produits de demain. 

{{< image src="/images/2022/08/Triple+Bottom+Line.jpg" title="Exemple de triple comptabilité" class=center >}}


### Trois horizons

Une autre façon de s'étalonner serait de catégoriser ses produits selon 3 axes, 3 horizons. Cette idée provient d'un [article de Daniel Wahl](https://medium.com/activate-the-future/the-three-horizons-of-innovation-and-culture-change-d9681b0e0b0f). Il met en lumière 3 horizons : 

- le premier chemin est le produit comme d'habitude, mais il perd de son sens dans le monde dans lequel nous entrons. 
- le second est le chemin intermédiaire qui démontre des premiers pas ou des premières réussites vers ces produits de nouvelles générations, écoresponsables. 
- le troisième est le chemin des produits durables, frugaux, voire régénératifs, réparateurs de nos dégâts.   

{{< image src="/images/2022/08/3horizons.jpg" title="3 horizons" class=center >}} 

Ce qui est important dans ce schéma c'est de comprendre qu'il va falloir passer par le chemin intermédiaire dans la grande majorité des cas. Le grand saut n'est pas possible. Il y a des étapes. C'est bien ce que je reproche à ceux qui cherchent des poux aux initiatives parfois plus symboliques qu’efficaces, c'est de mésestimer le parcours mental nécessaire, le cheminement nécessaire. 

S'étalonner ? Catégorisez vos produits selon ces 3 horizons. Ils sont tous dans le premier ? Vous en avez dans la deuxième catégorie ? J'ai proposé de faire cet étalonnage dans mon environnement (cela sera fait bientôt). J'imagine que nous en aurons très très peu déjà dans la catégorie deux, celle des premiers pas, des premières avancées, qu'ils seront presque tous dans la catégorie un du "business as usual". Ce n'est pas grave, l'objectif ici est de le réaliser pour commencer à s'orienter différemment.  

Avec ces informations on entre dans le concret, on peut articuler une conversation, des choix, des actions. S'étalonner c'est voir où on en est et donc pouvoir avancer. 

## Faire émerger

Comme les questions de disruption [évoquées précédemment](/2022/07/produit-de-demain-preambule/), il serait conseillé à tout le monde de se lancer dans des laboratoires, des expérimentations, des filières alternatives. Les produits ont été dominés par des gens qui ont pensé différemment, et par des gens qui ont eu le savoir technologique entre leurs mains. La suite, si on reprend ces arguments sera menée par des gens qui enrichissent leur expertise, leur connaissance sur les sujets d'écoresponsabilité, et qui auront déclenché des approches émergentes alternatives. Une évidence ? Le faites-vous ? 

Je conseille à toutes les entreprises de France et de Navarre, d'opérer un audit de leur portefeuille de produits, de savoir éliminer les 20% les moins utiles, performants. Et de rediriger ce budget sur des initiatives dont l'objectif serait de faire émerger ces nouveaux produits. Je suis à votre disposition pour cela.  

À quoi ressembleraient ces émergences. Encore une fois par définition je ne sais pas vous dire. Je peux juste vous rappeler qu'il ne faut pas se précipiter sur les premières émergences (comme le rappel le modèle [Cynefin, de Dave Snowden](https://www.youtube.com/watch?v=nhLkuhTjTAM)). Se donner du temps. On n'est plus dans le rebond permanent du produit en constante évolution si propice à la société de consommation actuelle. 

Comme point de départ, il peut être intéressant de se pencher sur une approche basée sur un mimétisme de la nature pour faire évoluer certains de nos produits, ou en imaginer d'autres. J'avais trouvé [Michaela Emch](https://www.linkedin.com/in/michaelaemch/) particulièrement intéressante sur le sujet. Là aussi on retrouve des préceptes que nous utilisons beaucoup dans nos mondes dits "agiles". Un pointeur passionnant : [asknature.org](https://asknature.org). Vous allez dans "recherche" et par exemple vous tapez : "friendship". Et vous trouverez par exemple que les [flamants roses](https://asknature.org/strategy/flamingos-form-friendships/) forment des groupes de quatre pour accompagner le parcours des plus jeunes, et que ce groupe persiste au delà du "onboarding". N'est-ce pas là des choses passionnantes pour nos produits[^2] ?

[^2]: J'avais antérieurement dans un [travail inachevé très utilisé les termites](https://pablopernot.fr/organisations-vivantes/)... 

{{< image src="/images/2022/08/asknature.jpg" title="Ask nature" class=center >}}

Il faut plonger hors de sa zone de confort pour faire émerger des choses nouvelles. Asknature, ok. Mais on peut aussi prendre ce schéma du [Regenerative Marketing Institute](https://regenmarketing.org/) et imaginer plein de choses. 

{{< image src="/images/2022/09/systemic_reg.jpg" title="Regenerative Marketing Institute" class=center >}}

*Decorporatization* : c'est ma future entreprise, mon futur produit ! N'est-ce pas là une extension, du *de-scaling* que nous évoquons régulièrement, au travers de l'agilité, de la sociocratie, etc. Une façon passionnante de rendre les entreprises plus durables ? *Definancialization* : une façon d'aborder le *beyond budgeting*. Il faut attraper le futur par un bout et voir ce qui émerge. (Quelques pistes pour penser hors de sa zone de confort : [workshop ALE 2022](/2022/08/product-design-ale-2022/), mes trois derniers articles sont très liés entre eux). 




Apprendre, devenir expert, s'étalonner, mesurer pour articuler son action, penser différemment, faire émerger. À vous de jouer. 
Le monde dans lequel nous vivons est celui de nos produits, des usages que nous imaginons, prenons en soin.
