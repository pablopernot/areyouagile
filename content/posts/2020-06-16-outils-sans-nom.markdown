﻿---
date: 2020-06-16
slug: ne-nommez-pas-vos-outils
title: Ne nommez pas vos outils 
---

Après une énième complainte sur internet concernant un gestionnaire de tickets, une évidence m'est apparue. Évidence que je mets en oeuvre dès lors. **Il ne faut pas nommer les outils**. Cette évidence je m'y applique depuis longtemps concernant tout un tas de choses, mais son utilisation pour les outils m'avait échappée.  

### TOTEM 

Leur donner un nom c'est donner une importance à ce qui ne l'est pas. Le nom n'est pas important. Ce que l'on en fait l'est. Je dois bien en nommer certains ici pour appuyer mon propos :  jira, confluence, trello, draft.io, etc. tous ces noms rendent les choses très conceptuelles, pas du tout concrètes. Chacun en fait un usage différent, chacun en comprend l'utilité différemment. Cela devient un totem derrière chez qui tout le monde s'abrite.    

Dans le travail autour des métaphores on cherche à rendre les choses concrètes de façon à pouvoir avoir une véritable communication. On peut s'appuyer sur un acronyme utile VAKOG (qui rappelle les cinq sens : visuel, auditif, kinesthésique -- tactile -- , odorat, goût) pour ancrer notre métaphore dans quelque chose de tangible. Jira, teams, slack sont des protocoles de nommage qui peuvent nous faire oublier le sens des choses, générer des incompréhensions, des quiproquos, et surtout ne pas nous aider à résoudre les choses, à apprendre, à avancer.   

Au lieu de dire jira vous pourriez dire : la liste des tickets ou la liste des fonctionnalités du produit. Et déjà ce n'est pas du tout la même chose. Liste des tickets ou des fonctionnalités du produit ? Où le lieu de toutes les listes (qui exprime encore autre chose). Ainsi au lieu d'avoir un nom conceptuel vous dîtes à quoi ils servent vous allez bien mieux vous faire comprendre, et comprendre ce qui se déroule avec cet outil. 

Naturellement vous pouvez étendre cette pratique à tous vos outils et méthodes (c'est ça que je fais depuis longtemps) : pas de *quarterly planning* ou de *PI planning*, mais le point de synchronisation entre toutes les équipes, ou la journée de présentation des objectifs pour les trois mois à venir. Bien nommer c'est bien comprendre comme toujours. En exprimant un usage : synchronisation ? Présentation des objectifs ? Les deux ? vous priorisez, vous mettez un focus.  

C'est la fin du jargon et c'est tant mieux. Dès que l'on s'arrête de parler des outils, des méthodes du comment, on commence à parler de choses intéressantes. Dès que l'on arrête de parler des outils, des méthodes, du comment, on commence à poser les bonnes questions. 

### RESPECT

Personnellement **je suis toujours très attentif à parler de façon à ce que personne ne puisse s'échapper par le biais du jargon, ou d'un abus de langage, ou de mots chapeaux. Et cela sert aussi à respecter l'autre qui ne prend pas le jargon comme un signal lui disant qu'il est has been. Si je parle pour ne pas être compris qu'est-ce que cela dit de moi ?** 

"Quel outil utilisez-vous pour une roadmap ? Avez-vous une roadmap ?" Non. Plutôt avez-vous une vision claire de comment vous souhaitez développer votre produit, dans quel but, et comment cela s'exprime-t-il ? Un calendrier avec des fonctionnalités attendues ? Une chronologie avec des étapes clefs ? "J'hésite entre faire du scrum et de l'agile à l'échelle", non. Plutôt : j'ai besoin d'avoir une organisation qui facilite la livraison à grande échelle, ou une organisation qui permette de livrer des choses plus régulièrement".    

Les mots concepts abstraits sont des pièges. Pire certains sont des concepts marketing qui mentent souvent sur leur intention. *Confluence* c'est souvent le lieu ou chacun dépose son contenu dans son coin *sans jamais croiser celui d'un autre*, sans convergence, *SaFE* c'est la garantie que vous *ne serez pas* adapté au monde changeant et entrelacé dans lequel nous vivons. 

Essayons de ne pas nommer nos outils pour voir réapparaître les vraies conversations. 