﻿---
date: 2017-11-08T00:00:00Z
slug: pourquoi-faut-il-se-mefier-de-safe
title: Pourquoi faut-il se méfier de SAFe
---

Franchement ma vie serait beaucoup plus simple si j'étais un supporter de SAFe. J'aurais toujours des réponses prédéfinies aux questions de mes clients, dans toutes les situations, dans tous les cas de figure, je demanderais probablement plus d'argent sans avoir à expliquer pourquoi puisque j'ai toutes les réponses, j'aurais encore plus de demandes, tout le monde se sentirai en sécurité avec toutes ces réponses, je ne pousserais personne hors de sa zone de confort, je ferais preuve d'une fausse rigueur qui en jetterai, je n'aurais ni doute, ni introspection concernant mon accompagnement, je pourrais débrancher mon cerveau dès la fin de la journée, et même pendant, j'aurais du *feedback* constamment sur la bonne application du *framework* (mais pas concernant de la création de valeur ou de la réelle évolution de l'organisation). Bref le beurre et l'argent du beurre avec en sus le repos de l'esprit. Quand à savoir si cela ferait sens pour moi et pour l'organisation, cela ne serait pas mes oignons. SAFe s'occupe de tout. C'est simple, SAFe a réponse à tout. Et c'est bien ça le problème. SAFe nie la raison même d'être de l'agilité : [évoluer dans un monde complexe](/2017/07/etre-agile/). 

## C'est un mensonge qui arrange trop de monde

Je ne doute pas naturellement que l'on puisse tirer du bien de SAFe, comme de n'importe quel autre *framework*. Vu son succès j'espère que c'est ce qui *in fine* arrivera. D'ailleurs le succès de SAFe a su rappeler aux gens certains des points importants que j'évoquais [concernant les moments de synchronisation multi-équipes](/2017/11/agilite-a-grande-echelle-synchronisation/), notamment de se donner les moyens de la démarche sur ces phases. Malgré cela je doute que SAFe soit le bon point de départ, je n'aime pas le manque d'authenticité de son message. "C'est SAFe, c'est sûr" comme le sous-entend son nom. "Nous avons réponse à tout pour tous" comme le sous-entend le tragique poster qui fait sa renommée. Encore une fois c'est bien ça le problème : SAFe nie la raison même d'être de l'agilité qui est de savoir évoluer dans un monde complexe. SAFe nie le monde dans lequel il évolue.  

**Un tragique poster**. 

{{< image src="/images/2017/11/safe.jpg" title="Tragique poster" >}}

Ainsi **je ne souhaite pas baser mon accompagnement des organisations sur un mensonge**. Je ne souhaite pas imposer des pratiques sans en faire comprendre le sens. Je ne souhaite pas faire croire que ce qui s'applique dans ce contexte s'appliquera de façon identique dans tel autre. Peut-être que plus tard dans l'avenir il suffira d'appliquer des pratiques. Mais aujourd'hui nous savons que c'est d'abord un état d'esprit, une série de principes, une façon de penser qui permet d'évoluer.  

## L'inconfort est nécessaire

J'ai l'habitude de dire que Agile met le management dans l'inconfort, le management, les décideurs, les donneurs d'ordres. Avant il suffisait d'un contrat en engagement de résultats (un "forfait"), pour s'assurer de ne pas être mis en défaut. Il suffisait de manipuler une feuille *excel* pour ajouter ou soustraire des chiffres, qu'il s'agisse de vrais dollars ou de personnes. Mais dans le monde complexe, systémique, tout cela vole en éclats pour qui veut être performant. Les décideurs, les donneurs d'ordres, doivent désormais être en appui de leurs équipes dans un monde beaucoup moins prédictible. Tout ce qui semblait sous contrôle se révèle être une illusion de contrôle : les engagements de résultats deviennent des fumisteries, il n'y a pas de cause à effet dans les jeux sur les cellules d'une feuille *excel* avec la réalité, les choses sont devenues bien trop globales, et une feuille *excel* est bien trop réductrice.   

SAFe a du probablement naître quelque part, émergeant pour apparaître tel qu'il est apparu dans les premiers "posters". Mais cette forme est bien une émanation de ce contexte, et elle n'est pas répétable en l'état dans un autre contexte, d'autant qu'elle est trop précise, d'autant que :   

> People copy the most visible, obvious, and frequently least important practices – Pfeffer & Sutton, [dans les slides de Pavel ici](/2014/08/ale-2014-sous-le-soleil-de-cracovie/)

Voici ce que je picore sur les pages SAFe des dernières avancées de la version 4.5. Vous pourrez me reprocher plein de choses, notamment de me focaliser ici essentiellement sur la forme. Mais le contenant dit tellement de l'état d'esprit du contenu que cela en est glaçant. **Le cynisme de ces schémas est glaçant**. 

**Image cynique sur le lean startup par SAFe**

{{< image src="/images/2017/11/leanstartupcycle-safe.jpg" title="Cynisme sur Lean Startup" >}}

**Image cynique sur la "compliance" par SAFe**

{{< image src="/images/2017/11/compliance-safe.jpg" title="Cynisme sur le PDCA" >}}

Comme certaines publicités, on se dit "c'est trop gros, cela ne va pas passer", et bien si, ça passe.

**Pour les mêmes raisons j'ai aussi un peu de mal avec cette chose**

{{< image src="/images/2017/11/everything.jpg" title="Tout en un" >}}

Quand on fait tout, on ne fait rien. Quand il n'y a plus que des urgences, il n'y a plus d'urgence. Quand on a besoin de tout prévoir, c'est qu'on ne sait pas ce que l'on veut. *et cetera*

Le principal mensonge de SAFe est de nous faire croire que nous sommes encore dans un monde cartésien, non systémique. Son principal mensonge est le détail de sa réponse qui laisse croire que tout se découpe, et que l'on peut répondre à tout par une simple action mécanique. Pour les hiérarchies en place c'est le bonheur, elles trouvent un levier qui permet de faire persister l'Ancien Monde en se fardant des couleurs du nouveau. Pour les modèles cartésiens en place c'est aussi joyeux, ils savent se corréler facilement sans renier et donc sans comprendre le changement du monde autour d'eux. Pour les fondateurs de SAFe c'est un bonheur économique, d'autant que leur coup politique est d'avoir tapé haut, très haut. Coup double : ils renforcent les hiérarchies en place, et le prix élevé finit de les convaincre, si ce n'est les certifications.    

Intellectuellement parlant c'est choquant. Opérationnellement parlant je veux bien croire que l'état des lieux de certaines organisations soit tel que passer par là amène un progrès (comme dans la [spirale dynamique](http://www.spiraledynamique.com/Theorie/index.php), il faut bien passer par le niveau bleu avant d'espérer atteindre le niveau vert).  

J'attends plus de vrais leaders modernes, qu'ils sachent prendre le risque d'aller de l'avant, qu'ils prennent le risque de penser. 

## Le cadre pas le geste

La principale différence entre Agile et Lean est la suivante : le Lean au travers de l'adaptation constante va chercher à faire évoluer ses pratiques, mais chaque adaptation est standardisée à l'ensemble de l'usine, de la chaîne, des personnes. La standardisation l'emporte sur l'adaptation. Agile évolue dans un monde encore plus complexe, l'adaptation prime sans chercher à standardiser l'ensemble (et au passage cet agile gagne peu à peu du terrain sur le Lean, puisque le monde devient de plus en plus complexe). 

Ainsi le cadre dans lequel chacun travaille est important, mais la liberté du geste doit être laissée à chaque entité. Exemple : chaque équipe doit montrer régulièrement quelque chose de fini, doit mesurer sur le terrain son impact, par exemple. Mais la façon dont elle a décidé de fonctionner pour cela est de son ressort. La forme qu'elle décide d'utiliser pour le présenter est de son ressort. 

L'accumulation des détails et le catalogues des réponses dans SAFe instaurent un état d'esprit totalement différent. Il n'y a pas d'oxygène pour l'adaptation, et ainsi pas de place pour la performance. Juste **la médiocrité de l'application par la recherche de la conformité**.  

Quand on m'interroge sur SAFe, je réponds en utilisant un témoignage de Henrik Kniberg auquel j'ai assisté, et avec un ton goguenard : **"Je suis du même avis que Henrik Kniberg, SAFe c'est super, ils en utilisent 30% chez LEGO"**. Si déjà vous décidez d'appliquer SAFe à 30% vous déjoueriez le principal écueil de ce *framework*, son obésité, sa faim de détails. Est-ce que vous pourriez être certifié à 30% sur SAFe ? Non malheureusement.  

> Roland Gori citant et commentant Giogio Agamben : "Le citoyen libre des sociétés démocratico-technologiques est un être qui obéit sans cesse dans le geste même par lequel il donne un commandement." Ce à quoi Roland Gori complète : C'est dire à quel point nous sommes pris dans une chaîne de production des comportements dans un système qui nous assigne à des places qui sont des places fonctionnelles, des places instrumentales et qui ne requiert pas d'avoir à penser qui ne requiert pas même d'avoir un état d'âme. 

En laissant la liberté à chacun de choisir son geste, on se doit d'être porté par du sens. Je fais partie de l'école qui pense que le cadre doit être clarifié, pas le geste. C'est là que toute la vraie valeur se joue. 

## Économie de penser

Ainsi donc il faut se méfier de SAFe car il souhaite nous faire faire l'économie de penser. En ayant réponse à tout de façon mécanique. 

> Je reprends encore cette vidéo découverte de Roland Gori dont [j'avais déjà évoqué un livre il y a 5 ans](/2012/04/lecture-lempire-des-coachs-de-gori-le-coz-2006/) :"Penser, comme décider, mobilise l'angoisse de l'imprévu, mobilise l'angoisse de l'avenir, cela mobilise l'angoisse de la liberté, cela mobilise l'angoisse de la décision, et décider c'est renoncer, et on n'aime pas renoncer."

**On ne peut pas envisager une transformation sans introspection même de la part de l'organisation**. Faire de l'agile ou être agile, c'est toujours la même question. Je me méfie de SAFe car il vous fera faire, mais il ne vous proposera pas d'être. J'espère malgré tout que SAFe va réussir à mettre des gens involontairement en route vers agile en les faisant faire et que sur le chemin tout tracé par le *framework* ils sauront trouver leur propre voie. La seule qui puisse vraiment aboutir : la leur. Et ainsi être agile. 

### SHU HA RI ? 

Certains vont invoquer le stade du SHU (["Fait ce que le mentor te demande de faire sans sourciller. Applique la règle, pratique, et pratique encore."](/2015/09/paradoxes-des-transformations-agiles/)), ensuite tu sauras adapter. Mais SAFe est bien trop cannibalisant dans sa communication. SAFe ne laisse pas de place pour passer au stade du HA ([En faisant tu as vraiment compris comment cela fonctionnait et maintenant tu peux faire des adaptations contextuelles.](/2015/09/paradoxes-des-transformations-agiles/)), comme l'on montré les diagrammes plus haut des ajouts dans la version 4.5 par exemple. SAFe rempli constamment l'espace et tout l'espace, il n'y a pas de place libre pour évoluer, on doit juste respecter. **Il nous étouffe dans sa logorrhée.**          

## SAFe, une imposture

> Un imposteur est celui qui a parfaitement trouvé le moyen de s'adapter à l'attente de l'autre [...] Faire prévaloir la forme sur le fond, valoriser les moyens plutôt que les fins, se fier à l'apparence et à la réputation plutôt qu'au travail et à la probité, préférer l'audience au mérite [...]. Le propre de l'imposture, la capacité de vendre les apparences. Roland Gori. La fabrique des imposteurs. 

SAFe est une imposture si je reprends les mots de Gori car il a su parfaitement s'identifier au désir de l'autre, l'autre étant cette majorité d'organisations qui doit changer sans le vouloir, cette majorité de personnes angoissées par cette idée. Je comprends cette angoisse. Mais en étant cette imposture, SAFe ne dit pas une vérité plus importante sur le monde systémique qui nous entoure et ne nous rend pas service.     

Remarquable vidéo de Roland Gori ([dont j'avais commenté un livre en 2012](/2012/04/lecture-lempire-des-coachs-de-gori-le-coz-2006/)) qui est venu avec surprise et bonheur commenter, affiner, dépasser ma pensée : 

{{< youtube id="2FEtiA18lZU" >}}

Sur les imposteurs, sur la valeur, sur les mesures, fabrique de monstres, caractère essentiel de l'inutile, etc. 

## Une petite série sur mise à l’échelle (et auto-organisation).

* [Agile à l'échelle : c'est clair comme du cristal, 2013](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)
* [Agile à l'échelle : synchronisation, 2017](/2017/11/agilite-a-grande-echelle-synchronisation/)
* [Agile à l'échelle : pourquoi il faut se méfier de SAFe, 2017](/2017/11/pourquoi-faut-il-se-mefier-de-safe/)
* [Agile à l'échelle : libération de l'auto-organisation, 2017](/2017/12/agile-a-l-echelle-liberation-auto-organisation/) 
* [Agile à l'échelle : équipes et management, 2017](/2017/12/agile-a-l-echelle-equipes-et-management/)
* [Agile à l'échelle : grandir ou massifier](/2018/02/grandir-ou-massifier/)
* [Agile à l'échelle : Podcast "Café Craft" - "L'agilité à l'échelle"](https://open.spotify.com/episode/0LYgFF0FROt2AsS8aR8awx?si=1deb7f4bfe9343c4)

Et sinon l'événement que nous organisons autour de l'agile à l'échelle avec [Dragos Dreptate](http://andwhatif.fr): 

* [VALUE DRIVEN SCALING](http://valuedrivenscaling.com)



