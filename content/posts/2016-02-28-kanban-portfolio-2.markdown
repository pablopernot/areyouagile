---
date: 2016-02-28T00:00:00Z
slug: portfolio-projets-kanban-partie-2
title: Portfolio Projets kanban, partie 2
---

Petite suite au sujet du [portfolio projets kanban partie 1](/2016/01/portfolio-projets-kanban-partie-1/) de janvier 2016. Petite,
car l'essentiel de cette introduction à kanban était présent
précédemment.

## Qu'est ce que l'on mesure ?

Finalement qu'est ce que l'on mesure ? Bien tout simplement, par
exemple, le nombre d'éléments dans votre kanban. Donc le nombre
d'éléments sur lesquels un effort de création de valeur est accompli. Il
faut bien comprendre que le kanban montre un flux qui vit, un travail en
cours. Mise à part la première et la dernière colonnes qui sont des
déversoirs (importants), toutes les autres colonnes devraient montrer
une activité, une action, une dynamique. Si vous avez plusieurs milliers
d'anomalies et un kanban lié à la gestion de ces anomalies, vous n'allez
pas les placer sur le mur, c'est impossible et inutile. Vous allez faire
entrer dans le flux du kanban celles sur lesquelles vous voulez
travailler (c'est déjà une décision forte).

Vous allez donc mesurer le nombre d'éléments qu'il y a dans votre
kanban, et vous allez mesurer le temps moyen qu'il faut à un élément
pour traverser les étapes. Vous avez ainsi deux informations clefs :
combien d'éléments et en combien de temps. Une cadence apparaît, elle
peut et devrait vous aider à gouverner, planifier, arbitrer. On appelle
généralement le "combien d'éléments" : le *wip* (*work in progress*), et
le "combien de temps" : le *lead time* (ou *cycle time*, il y a des
[variations dans les
définitions](http://www.simplilearn.com/time-confusion-cycle-time-takt-time-lead-time-part-1-article)),
le *lead time* c'est le temps de l'entrée de la fiche dans le kanban
jusqu'à son arrivée au fini (ou pour le *cycle time*, le temps du début
du travail jusqu'à la fin de celui-ci, ou selon d'[autres
définitions](http://xprocess.blogspot.fr/2013/07/whats-difference-between-cycle-time-and.html),
le temps entre certaines colonnes du kanban).

Oui il y a probablement des éléments différents dans le kanban, qui ne
se comparent pas. Faîtes au mieux. Des fois il est utile de les
distinguer, des fois non, mais vous saurez y répondre.

Dans les exemples ci-dessous je prends le parti de dire que le *wip*
exclu la première colonne qui est un déversoir, et pareil pour la
colonne "fini". C'est un arbitrage. Faîtes comme vous voulez. Par contre
j'intègre un *lead time* jusqu'au "fini". *Cliquez sur les images pour
agrandir*.

{{< image src="/images/2016/02/014-kanban-wip.jpg" title="kanban Portfolio 014" >}}

{{< image src="/images/2016/02/015-kanban-leadtime.jpg" title="kanban Portfolio 015" >}}

Après vous pouvez creuser la [Loi de Little](https://fr.wikipedia.org/wiki/Th%C3%A9orie_des_files_d'attente)
mais cela devient des mathématiques, je ne suis plus compétent.
J'utilise plus kanban comme un outil d'amélioration continue que comme
un outil mathématiques de traitement de la volatilité du flux.

## Les déversoirs

Sous leurs allures d'entrée en matière et de fin de parcours les
déversoirs sont importants. Notamment le premier. Souvent la première
colonne est un bric à brac de demandes, d'idées, d'origines multiples.
Il vaut mieux ne pas le restreindre. Laissez les gens s'épancher, être
libre, et y ajouter des éléments. Un kanban, par son aspect concret,
physique, va inéluctablement rendre évident que parmi toutes ces choses,
il va falloir choisir. (Et souvent la colonne suivante s'appelle
"sélection", un premier acte fort de gouvernance).

Le déversoir de fin montre souvent le travail accompli, il ne faut pas
le négliger.

## Apprendre plus vite, coût du délai

On mesure donc le temps pour traverser et le nombre d'éléments traités
simultanément. On aimerait souvent qu'ils traversent plus vite. On est
gêné par un projet inerte. On est irrité, on aurait pu le découper en
lot, en "release", en fonctionnalités. Après avoir choisi quoi mettre
dans le flux de création de valeur, après avoir mesurer les temps de
traversées, on se dit qu'il faudrait découper en plus petits éléments
pour fluidifier encore notre travail, atteindre plus rapidement la rive
du fini. C'était l'idée des limites ([portfolio projets kanban partie
1](/2016/01/portfolio-projets-kanban-partie-1/)), restreindre notre
activité pour l'obliger à avancer mieux : *commencer à finir, arrêter de
commencer*. Les limites vont nous pousser à finir, pour finir plus vite
on va découper notre création de valeur en plus petits éléments.

Mais pourquoi finir plus vite ?

Pour réussir plus vite, pour échouer plus vite, mais surtout pour
apprendre plus vite (sur son marché, sur son offre, sur son idée, sur
son projet, etc.). On retrouve ici une des constantes de notre [monde
moderne complexe](/2015/01/modernite-agilite-engage/).

Une autre mesure souvent lié au domaine kanban est **le coût du délai**
(de mémoire cet [article de Derek
Huether](http://www.leadingagile.com/2015/06/an-introduction-to-cost-of-delay/)
était bon). Le coût du délai c'est l'argent que vous perdez à ne pas
sortir dès à présent une fonctionnalité, lorsque vous décidez de la
garder sous le coude tant que les autres ne sont pas prêtes. Vous
limitez donc les bénéfices financiers de cette livraison en la
contraignant à la dernière livraison. En fait vous perdez de l'argent
(l'argent est souvent un argument que l'on entend plus facilement que
celui des bénéfices d'apprentissage).

Bien souvent les kanban vont rendre cela évident et vous pousser à
changer dans cette direction. Ce n'est cependant pas une règle. Souvent
on se retrouve néanmoins avec des éléments plus petits qui font sens de
façon autonome pour mieux naviguer dans les contraintes, finir plus
vite, avoir peu de coûts liés au délai, et qui délivrent un
apprentissage rapide et essentiel.

Un kanban nous pousse souvent à ne plus penser en mode projet ou TMA
([tierce maintenance
applicative](https://fr.wikipedia.org/wiki/Tierce_maintenance_applicative)),
mais en mode produit (les lignes horizontales) et en fonctionnalités ou
anomalies déposées dans cette approche globale valeur / produit. De fait
souvent on passe de projet, à "release", puis à fonctionnalités, dans le
découpage de nos éléments. Mon portfolio projets kanban va donc
probablement (mais toujours rien n'est sûr, respectez votre réalité) me
pousser à penser une perspective produit, avec des éléments assez petits
pour gagner en apprentissage, minimiser mon coût du délai, maximiser ma
valeur et ma connaissance.

## Flux tiré

Dans [portfolio projets kanban partie 1](/2016/01/portfolio-projets-kanban-partie-1/) je vous avais parlé de
l'histoire des tickets du parc de Tokyo pour présenter les limites qui
définissent en grande partie la pensée kanban. Voici une autre petite
histoire pour le deuxième pan de cette pensée (à mes yeux) : le montage
de portière de voiture chez Toyota. Je suis quelqu'un qui monte des
portières de voiture, j'ai un tas de dix portières devant moi. J'en
place une, puis deux, trois, quatre, cinq, sur la sixième se trouve une
petite étiquette (un kanban, une étiquette historiquement) elle
m'indique que je devrais demander dix nouvelles portières. Très bien je
le fais. Puis je place la sixième, la septième, etc. jusqu'à la dixième.
Les choses sont bien faites, j'ai demandé dix portières au bon moment,
dix nouvelles portières arrivent alors.

Variante du scénario : alors que je place ma troisième portière,
Jean-Luc m'interrompt et me demande de l'aide, que j'ai plaisir à lui
fournir pendant quelques heures. Quand je reviens deux options, soit
j'utilise un flux tiré ("pull") avec une étiquette sur la sixième
portière qui me suggérera de demander dix portières de plus, et donc mon
tas de portières n'a pas changé ; soit je n'utilise pas de kanban et
j'ai un classique flux poussé ("push"), c'est à dire que l'on m'amène
régulièrement des portières et pas quand je le demande ("pull"), et dans
ce cas il se peut que plusieurs tas de dix portières m'attendent. C'est
dommage : c'est du stock inutile, coûteux (utilisation de la matière
première), et risqué (si on découvre une anomalie elle s'étend à bien
plus d'éléments, si on décide de changer d'avis, tous ces éléments sont
obsolètes).

Le flux tiré c'est donc de laisser à la colonne suivante l'opportunité
de venir tirer ("pull") les éléments de la colonnes précédentes quand
elle en a besoin, permettant ainsi un traitement juste à temps bien plus
efficace. Dans un kanban cela se représente avec deux sous colonnes :
"en cours" et "fini". Les propriétaires de la colonne suivante peuvent
venir tirer les éléments de la sous-colonne "fini" de la colonne
précédente.

Voici un petit scénario.

### \#1 vision brouillée

Un kanban (sans limite, ou juste sur la ligne urgence), mais avec de
gros amas irritants dans la colonne réalisation. Impossible de savoir si
c'est la colonne "étude/préparation" qui asphyxie la colonne
"réalisation" en poussant ("push"), ou si c'est la colonne "réalisation"
qui n'avance pas (pour de bonnes ou mauvaises raisons : comme commencer
sans jamais finir). *Il faut cliquer sur les images pour les agrandir*.

{{< image src="/images/2016/02/011-kanban-pull-1.jpg" title="kanban Portfolio 011" >}}

### \#2 vision clarifiée

Passer en système tiré ("pull") va clarifier cette visibilité. En fait
il y a plein de choses en attente, prêtes à être réalisées dans la gamme
produit 1, mais probablement la majeur partie de l'effort est réalisé
dans la gamme produit 4. N'empêche on y voit plus clair.

{{< image src="/images/2016/02/012-kanban-pull-2.jpg" title="kanban Portfolio 012" >}}

### \#3 retour des limites

Naturellement on en revient aux limites. Elles vont permettre
d'appliquer une stratégie : inutile de préparer autant de matière pour
la gamme produit 1 (par exemple), en limitant l'effort sur la gamme
produit 4 on va dégager de l'effort pour la gamme produit 1 (par
exemple).

{{< image src="/images/2016/02/013-kanban-pull-3.jpg" title="kanban Portfolio 013" >}}

### \#4 La meilleure adaptation

Le quatrième bénéfice, peut-être le plus important, je ne peux pas le
représenter visuellement : c'est l'adaptation de l'équipe au besoin.
Avec le flux l'équipe, et chaque membre qui la compose, tire ("pull") au
meilleur moment (quand ils peuvent), et avec la meilleure adaptation au
travail à réaliser : ils tirent ("pull") ce qu'ils peuvent et ce qu'ils
veulent (si certains veulent essayer de s'aventurer sur des compétences
pas forcément maitrisées ils le peuvent).

Naturellement cela peut-être un travail en binome ou en groupe.
Naturellement si personne ne veut rien prendre c'est aussi une
indication forte que l'on a besoin de quelque chose : renforcer les
compétences ? sécuriser le travail ? Que l'on est allez trop vite dans
un système à flux tiré ?

Voilà j'espère que cette deuxième partie complète bien la première
([portfolio projets kanban partie
1](/2016/01/portfolio-projets-kanban-partie-1/)) et saura vous donner
envie d'essayer kanban.