---
date: 2014-02-10T00:00:00Z
slug: penser-son-organisation-sur-le-terrain
title: Penser son organisation - sur le terrain
---

Après avoir lancé l'idée d'utiliser la théorie des catastrophes, idée
difficile à saisir et à appliquer -- je vais essayer bientôt de la
rendre plus palpable en l'associant à la métaphore du
[cristal](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)
-- je vais revenir à des choses beaucoup plus concrètes comme
l'organisation physique, sur le terrain.

Dernièrement j'ai pu réaliser deux [Open Agile
Adoption](/2013/10/deux-amis/) (je vous en parle dans la semaine au
travers de ce blog), et l'une des choses qui me frappent c'est l'aspect
concret des propositions. On imagine que les personnes vont demander des
processus de prises de décision, des façons poussées de gérer la
communication, etc. Mais non, pas du tout, ils ramènent leurs
propositions à des choses très concrètes, très terre à terre, mais
probablement terriblement efficaces : avoir des ordinateurs portables
plutôt que des postes fixes, changer l'emplacement du bureau pour avoir
un accès plus facile à tout le monde, regrouper les membres d'un projet,
plus de puissance de calcul du processeur, plus d'espace disque, etc.

## Un conteneur, de l'auto-organisation

Ben oui, je le rappelle, car j'adore cette phrase de **Harrison Owen**,
*"organiser un système auto-organisé ce n'est pas seulement un oxymore,
c'est surtout stupide !"* ; et l'être humain, comme tout système vivant
est auto-organisé. Il ne faut donc surtout pas s'attacher à essayer de
l'organiser, mais lui donner les moyens de s'auto-organiser plus
aisément, plus facilement, plus rapidement. On retrouve là une idée
chère à la complexité : le meilleur moyen de générer des pratiques
émergentes c'est de placer un conteneur, un cadre, des contraintes, et
de laisser l'élément (complexe) s'épanouir en son sein.

Donnez donc les moyens à votre organisation, aux personnes qui la
composent, de s'épanouir, de s'auto-organiser, en leur offrant un cadre
propice. Et n'allez pas chercher midi à quatorze heures.

## Espace de travail, *openspace*, *obeya* ?

Pour démarrer on peut évoquer les *openspace* -- là je parle des grands
espaces ouverts avec tous les bureaux --. La tendance est plutôt de les
proscrire. Il y a trop d'agitation et pas assez de focus par thème. En
plus, très bizarrement, dans ces grands espaces les équipes "produit" ou
"projet" ne sont pas si souvent que cela regroupées. A cela il faut
préférer ce que **Toyota** appelait les *Obeya*.

{{< image src="/images/2014/02/obeya_bigprojectroom.gif" title="Obeya" >}}

C'est à dire une pièce "projet". Elle rassemble tous les éléments et les
acteurs du projet. Il y a un focus fort, le conteneur est physiquement
représenté. Et, ajoute **John Medina**, dans *Brain Rules*, la mémoire
fonctionne mieux en associant lieu & sujet, et ainsi la résolution des
problèmes. Vous noterez qu'une *Obeya* propose une approche très
visuelle, avec un mur dédié par thème, je reviendrai sur ce point plus
bas, mais il est aussi essentiel.

Les *openspaces* ont cependant pour bénéfice de faire circuler
facilement l'information à tout le monde d'un coup. Pour ne pas perdre
cette transmission, si vous optez pour l'option "pièce projet" -- ce que
je recommande -- accompagnez la de beaux espaces *détente & café* (avec
canapé et tapis persans, ça c'est mon combat perso, les canapés et les
tapis persans). Les équipes ne devraient pas non plus être immuables, on
imagine qu'elles changent de temps en temps (un membre tous les 3 ou 6
mois ?).

## Co-localisation ?

Oui cela implique probablement une co-localisation. Faut-il le répéter
combien de fois ? On est bien meilleur en étant co-localisé. Et
co-localisé cela veut dire, si on emploie la belle image de **Alistair
Cockburn**, **pas plus de la distance d'un bus** (voir son graphisme
ci-dessous, cliquez pour agrandir, qui provient de cette [excellente
vidéo](http://www.youtube.com/watch?v=Se0P2z2UiVg)).

{{< image src="/images/2014/02/distance.jpg" title="Alistair Cockburn - Distance" >}}

Je ne sais malheureusement plus où j'avais lu cela, et donc je ne peux
vous donner la référence, mais il semble que certaines études font même
apparaître que se situer à un autre étage est une distance encore plus
dur à franchir que se situer dans deux villes différentes.

La principale question des organisations est une affaire de
**communication**. On passe un temps fou à mal communiquer, car pour des
questions de coûts ou de facilité on a décidé de ne pas être
co-localisé. C'est quelque chose qui coûte très cher. Autant en terme
d'implication des personnes, de qualité, de temps, etc.

## *Off-shore* ?

Mais alors et le *off-shore* ? Le *off-shore* tel qu'on l'entend
habituellement (ou même le *near shore*) cumule deux problèmes : la non
co-localisation évoquée plus haut, et le fait que l'on destine celui-ci
à une tâche bien précise dans un projet ; bien souvent par exemple, la
recette, l'assurance qualité, ou les développements. Et là c'est le
drame. Non content de ne pas pouvoir communiquer (par la distance, mais
aussi souvent la langue et la culture), on rend aveugle le centre
externe en ne lui donnant aucune vision, aucune capacité
d'auto-organisation justement. On perd tout [engagement et
motivation](/2013/11/engagement-et-motivation/) comme j'ai pu l'évoquer
dans ce précédent article, et ainsi toute productivité. Comme je le
disais, dans le livre ["Mythical Man
Month"](http://en.wikipedia.org/wiki/The_Mythical_Man-Month) de **Fred
Brooks**, on évoque un ratio de "productivité" de l’ingénieur avec un
facteur entre 1 et 10. Ce facteur est pour moi grandement basé sur la
capacité d'engagement et d'implication de l'ingénieur. Avec le
*off-shore* classique il s'effondre. Les indiens, les polonais, les
tunisiens, ne sont naturellement ni plus ni moins intelligents que les
français (faut-il le rappeler...!!!), c'est le cadre qui encourage
l'intelligence, l'innovation, l'engagement, l'implication, la
productivité. Ajoutez à cela le coût de la distance... Reprenez vos
calculs avec vos achats et vos viviers et votre *off-shore*, prenez le
coût du *off-shore* et multipliez par dix ou quinze.

Le *off-shore* peut très bien s'envisager si il se bâtit sur une équipe
(de 3 à 7 personnes) ou un département (50 personnes) hétérogène, avec
des compétences multiples (du métiers, au test, au déploiement, etc.) et
co-localisé. Je ne suis pas en train de vous dire que l'on ne peut pas
avoir une organisation sur plusieurs continents, mais que **chaque
localisation doit avoir son propre sens et s'auto-organiser**.

Pour les chiffres 7 et 50 je vous encourage à lire [la horde
agile](/2014/01/mini-livre-la-horde-agile/).

## Télétravail

Mais alors, et le télé-travail ? Le télé-travail est une question qui se
traite au sein de l'équipe, avec le support du management si il existe.
C'est donc une question d'auto-organisation. Selon les moments l'équipe
devrait savoir placer le curseur au bon endroit, et ce curseur peut
bouger. *A priori* le télé-travail va à l'encontre de la co-localisation
et de l'*obeya*. Je ne l'envisage donc pas à plein temps, mais si
quelqu'un veut se protéger -au calme- chez soi, si il s'épanouit ainsi,
cela ne sera que mieux pour l'équipe. Cette accord est possible au sein
d'une équipe. J'estime qu'il faut ménager des moments où tout le monde
est présent ensemble. J'ai lu le chiffre de 60% de télétravail maximum,
il me semble cohérent, mais comme il s'agit d'auto-organisation au sein
de l'équipe je suis près à observer toutes les solutions, toutes les
options.

En cas de télé-travail, pour réaménager la communication il est
fortement recommandé de placer tout le monde **au même niveau**, c'est à
dire que si une personne devait bosser en téléconférence via un outil
quelconque, il est préconisé de placer tout le monde au travers de cet
outil afin de garantir le même niveau de communication à tous (même si
les quatre autres sont autour d'un même bureau...). Cela renforce la
confiance et aligne tout le monde. (Ne soyez pas fous non plus, je ne
parle que de réunions journalières, ou hebdomadaires, pas durant toute
la journée).

D'ailleurs on observe que les équipes distribuées : un membre dans
chaque capitale d'Europe, fonctionnent mieux que les équipes dispersées
: la majorité de l'équipe à Paris, et deux membres à Berlin. Notamment
pour raison d'accès égal à l'information et donc de dynamique d'équipe.

## Outils de travail

Dans tous cela, au lieu de généralement se focaliser sur le processus de
consentement, ou la façon d'estimer au plus juste, il serait mieux de
s'attacher à libérer la capacité des personnes. Oui avoir des machines
plus puissantes coûte généralement peu au regard de ce que cela peut
ramener comme valeur. Imaginez vous ne puissiez pas avoir votre
plate-forme d'intégration continue (je parle aux informaticiens), juste
à cause d'un serveur essouflé... Pensez au coût d'un bon serveur comparé
au gain d'avoir une plate-forme d'intégration continue.

C'est pareil pour les murs et les tableaux physiques, le management
visuel (le vrai), mettez 2000 à 5000 euros et sauvez un projet de dix ou
cent fois plus.

Idem pour les outils de communication à distance, car même quand
l'organisation opte pour du *off-shore* classique, histoire de garantir
des coûts plus élevés, elle s'attache à s'équiper d'un système de
vidéo-conférence défaillant, ou d'un réseau dramatiquement sous
dimensionné.

**Un meilleur réseau, de meilleurs outils, de bons murs, de belles
pièces projet/produit ; des équipes co-localisées, auto-organisées et
porteuses de sens, n'allez pas chercher midi à quatorze heures.**

### Série sur les organisations

0 - prémisses : [Agile à grande échelle : c’est clair comme du
cristal](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)

1 - penser son organisation :
[introduction](/2014/01/penser-son-organisation-introduction/)

2 - penser son organisation : [théorie des
catastrophes](/2014/01/penser-son-organisation-theorie-des-catastrophes/)

3 - penser son organisation : [sur le
terrain](/2014/02/penser-son-organisation-sur-le-terrain/)
