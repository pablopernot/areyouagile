---
date: 2014-10-28T00:00:00Z
slug: charlotte-claude-les-invites-de-novembre
title: Charlotte & Claude, les invités de novembre
---

Le lundi 24 novembre 2014, lors d'un repas conférence, et le mardi 25
novembre, toute la journée, pour une conférence/atelier, nous
accueillons Charlotte Goudreault & [Claude
Emond](http://claudeemond.com) pour parler conduite du changement,
engagement, savoir-être et bien-être à Montpellier (le lieu reste à
définir, tout comme le restaurant).

{{< image src="/images/2014/10/charlotteclaude.jpg" title="Charlotte & Claude" >}}

C'est un évènement <http://convergenc.es> (le site n'existe plus). Convergences, c'est un
étendard que nous portons avec Oana Juncu pour monter les conférences
qui nous font plaisir. Nous avions eu le plaisir de croiser Dan Mezick
l'année dernière, et cette année nous aurons la joie d'avoir donc
Charlotte & Claude pour novembre, et nous espérons Olaf Lewitz pour
février.

Les interventions de Charlotte & Claude sont exclusivement en français
avec le bel accent québecois et à Montpellier. Voici, ci-dessous, un
petit entretien qui vous en dira plus sur le contenu (même si
l'enregistrement avec skype n'est pas de toute première qualité).

{{< vimeo id="110146372" >}}

[L'équipe projet se déploie](http://convergenc.es/claudeemond.html#main), diner-conférence,
le lundi 24 novembre à Montpellier. [Comment transformer votre
organisation à la vitesse grand « V » grâce au
Changeboxing](http://convergenc.es/claudeemond.html#main), le mardi 25
novembre, à Montpellier. Les
[inscriptions](https://www.weezevent.com/claude-emond-charlotte-goudreault-montpellier-2014)
sont ouvertes, et si vous voulez en savoir beaucoup plus : [Claude Emond
sur convergenc.es](http://convergenc.es/claudeemond.html#main).

