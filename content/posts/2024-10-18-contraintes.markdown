---
date: 2024-11-04
slug: de-la-bonne-contrainte
title: "De la bonne contrainte"
draft: false
---

C'est naturellement en rédigeant l'édition II de Kanban avec [Laurent Morisseau](https://www.morisseauconsulting.com/) que m'ait spontanément venu ce sujet sur la contrainte, sur la bonne contrainte.

Cette pandémie de covid a été dramatique, mais en toutes choses malheur est bon elle nous a permis de mesurer l'intérêt de la contrainte. Contraintes par l'injonction de demeurer chez soi, les organisations ont développé leurs capacités de travail à distance. La contrainte a drastiquement réduit la variabilité de la réponse à la question : peut-on, sait-on, travailler à distance ? La contrainte a augmenté la prédictibilité : on allait vite savoir. On n'allait pas rester dans le flou.  

J'associe cela avec la révision de ce livre sur Kanban, car la contrainte dans Kanban est souvent utilisée. De plusieurs manières, mais la plus connue est l'application de limites. On limite le travail en cours pour s'assurer justement une prédictibilité. Il faut pousser le bouchon pour bien comprendre ce qui se passe. Vous êtes une équipe de six personnes, je vous contrains à ne travailler que sur un seul élément à me délivrer. Cela améliore grandement ma prédictibilité. La contrainte améliore la prédictibilité. L'élément va être délivré dans un temps assez court même si je perds de la performance globale. Si je laisse l'équipe travailler sur dix éléments, j'améliore peut-être ma performance globale avec une meilleure utilisation de l'effort et des compétences des six personnes, mais je perds l'assurance d'avoir au moins un seul élément fini, et fini le plus rapidement possible. Vous voyez l'idée. Poussons le raisonnement à l'absurde : si je vous demande de ne *rien* réaliser pour ce projet j'ai la certitude que le projet est fini immédiatement, une super prédictibilité, de l'absurde je vous disais. À l'inverse, si je vous dis : faites ce que vous voulez pour ce projet et présentez-le-moi quand vous voulez, c'est-à-dire sans aucune contrainte, et bien je n'ai aucune prédictibilité.

Limite, cadre, contrainte, tout cela est voisin. Ils ou elles sont utiles tant qu'elles respectent certaines conditions.

Le cadre qui décrit ce que l'on attend d'un groupe, d'une équipe, en sociocratie ou holacratie, sa raison d'être, ses redevabilités, etc, donne de l'autonomie au groupe, car il a un repère, un sens, des règles, et il est autonome, en autogestion, quant à la façon dont il y répond. (Combien de fois ai-je observé que l'on mélange sociocratie ou holacratie avec absence de cadre ou de contraintes...).

Les limites dans Kanban par exemple : si on décide de limiter les éléments sur lesquels on travaille, améliore la prédictibilité de nos résultats (comme évoqué plus haut).

Les contraintes obligent. À penser différemment, à imaginer des choses inattendues, à croiser des concepts, à travailler ensemble, etc.

La contrainte de montrer des choses finies toutes les deux semaines (ou une ou trois, on s'en fout), oblige à savoir, à voir, à partager, et réduit l'ignorance.

La frontière entre cadre, limite, contrainte est subtile, et n'a pas forcément d'importance. Deux choses ont de l'importance.

D'une part cela améliore ma prédictibilité. Le groupe sociocratique définit par un cadre évoluera dans ce cadre (et ne fera pas quelque chose d'imprédictible, ou du moins sera orienté), les limites ou les contraintes réduisent la variabilité (ça ne part dans tous les sens, je suis au courant toutes les deux semaines, tout le monde s'est focalisé sur cet élément, etc.).

D'autre part cela donne de l'autonomie, c'est une façon de gouverner qui émancipe (selon certaines conditions dont on parlera plus bas). Car je ne dis pas comment faire, mais ce que l'on attend d'un groupe soumis à un cadre, une limite, une contrainte. Tout ce qui n'est pas la limite, la contrainte, le cadre, est possible. Normalement c'est un espace assez vaste.

"Faites ce que vous voulez tant que <ma contrainte> afin d'atteindre cet objectif" c'est la façon dont je l'exprime. Ce qui n'a rien à voir avec : "Faites ceci pour atteindre cet objectif". Quand on parle d'engagement, ou même de principes pour évoluer dans des mondes complexes, la première expression répond à ces enjeux. La seconde expression est du management qui fonctionnera dans des cas plutôt rares (des crises violentes et courtes par exemple) et sera généralement contre-productif.

Ainsi la contrainte, le cadre, la limite possède cette deuxième qualité de libérer un espace, d'engager en donnant une autonomie et donc une certaine responsabilisation, à la condition qu'il ou elle libère un espace. On libère cet espace en ne disant pas comment faire, en ne décrivant pas le geste attendu. On libère cet espace en ne le surchargeant pas de contraintes, de limites, de cadres. Si elles sont trop nombreuses (contraintes, limites), elles étouffent et finalement sont devenues des règles qui imposent des gestes "Faites ceci", car il n'y a pas d'espace pour autre chose.

Je pense que l'on a là des clefs d'une bonne dynamique de groupe, d'une bonne gestion projet.

*Sinon je vous rappelle que j'écris aussi désormais une newsletter et que l'[abonnement se fait via le lien dans le menu](http://eepurl.com/iZPT62). Pour la première je ne suis pas sûr qu'elle ait été très bonne, car je me suis fait conspuer par ma compagne (qui a eu la gentillesse de s'abonner) parce que ... enfin ... bon j'en parlerai dans la prochaine newsletter (ce qui se passe dans la newsletter reste dans la newsletter).*
