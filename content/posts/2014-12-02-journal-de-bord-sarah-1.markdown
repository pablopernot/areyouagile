---
date: 2014-12-02T00:00:00Z
slug: sarah-journal-de-bord-jours-1-et-2
title: Sarah, journal de bord, jours 1 et 2
---

Voici le journal de bord de Sarah, ma fille, qui en troisième effectue
son stage d'observation. Elle va donc m'observer toute la semaine en
entreprise dans mes tribulations principalement parisiennes pour
découvrir ce qu'est la vie professionnelle, en tous cas, la mienne. Je
lui laisse la parole, sans déformer son propos, si si.

{{< image src="/images/2014/12/journal.jpg" title="Journal" >}}

## Jour \#1, Stage d'observation de 3ème, Sarah P.

Nous nous sommes levés à 6h00 pour prendre le train à 7h50. Mon père a
l'habitude de travailler dans le train, il le prend chaque semaine. Il a
un appartement dans le Marais (Paris), proche de la place des Vosges et
de l'église Saint Paul. Mon père, avec deux amis, a créé une entreprise
nommée *smartview*. Ils sont trois patrons : consultants dans
l'organisation et microsoft. Si une entreprise veut organiser un projet
mais ne sait pas comment procéder, l'entreprise de mon père les aidera,
leur expliquera certaines choses.

Dans ce métier il faut se faire connaître : sur les réseaux sociaux
(twitter), et aussi grâce à leurs blogs/sites, ou par des conférences.
Ce premier jour, on a fait beaucoup de train : Montpellier-Paris,
Paris-Strasbourg. Mon père a passé quelques coups de fil, et a navigué
sur twitter : cela fait parti de son travail. Il n'a pas d'emploi du
temps fixe.

## Jour \#2, Stage d'observation de 3ème, Sarah P.

Strasbourg, conférence au château de l'Ile. (Atlassian Tour,
Netapsys/Smartview)

{{< image src="/images/2014/12/los3amigos.jpg" title="Los 3 amigos" >}}

Avec Stéphane et Aurélie de Netapsys, Alexis un client de Stéphane et
mon père.

Mon père est coach en agilité. Qu'est ce que c'est l'agilité ? C'est un
ensemble d'améliorations ou mises à jour qui prennent en compte les
concurrents, les futurs utilisateurs, avec du travail en équipe.
Netapsys est une société en ingénierie informatique (développement,
outillage, etc.). Smartview est une entreprise de conseil.

L'agilité ? On ne sait plus ce que c'est que l'agilité, tout le monde se
dit "agile". C'est surtout répondre à la performance. Agile c'est le mot
marketing, cela veut surtout dire "complexe". Qu'est ce que l'on doit
faire pour que cela marche ? Avoir une équipe motivée, des objectifs
clairs, intéresser les gens, les responsabiliser sur les projets ; faire
des choses finies par petits morceaux, faire les choses les plus
importantes. On peut avoir le droit de se tromper. Mon père était très
détendu pour en parler. Je pense que ce travail l'intéresse beaucoup et
qu'il a du plaisir à le faire ce qui est important pour moi pour choisir
un travail.

{{< image src="/images/2014/12/avecalexis.jpg" title="Avec Alexis" >}}

Outils et méthodes (Stéphane et Alexis). Pour mettre bout à bout un
projet il faut savoir où on va et faire un carnet de bord. Grâce à
certains outils comme Jira cela peut faciliter la tâche aux entreprises.
Alexis a parlé de son expérience avec cet outil et de ses résultats.
Après une pause, ils ont fait un rond avec les chaises, pour faire une
"table ronde" et répondre à quelques questions. Je cite Alexis : "nous
ne sommes pas des formateurs mais des déformateurs". J'ai l'impression
que les gens n'aiment pas le changement (même moi d'ailleurs) et donc il
doit falloir certaines personnes pour les sortir de leur zone de
confort.

Ce que j'ai bien aimé : La table ronde, ça permet de répondre aux
questions de chacun, on a pu voir qui était vraiment intéressé.

Ce que j'ai moins aimé : le côté "pub" de la présentation, essayer de
vendre un produit, ce n'est pas très intéressant pour moi.

Conclusion : j'ai bien aimé la conférence même si il y a certains termes
compliqués que je n'ai pas compris.

{{< image src="/images/2014/12/strasbourg.jpg" title="Strasbourg" >}}

## Les liens

-   [les deux premiers jours](/2014/12/sarah-journal-de-bord-jours-1-et-2/)
-   [le troisième jour](/2014/12/sarah-journal-de-bord-jour-3/)
-   [le quatrième jour](/2014/12/sarah-journal-de-bord-jour-4/)
-   [le dernier jour](/2014/12/sarah-journal-de-bord-jour-5/)
