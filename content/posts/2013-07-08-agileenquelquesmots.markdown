---
date: 2013-07-08T00:00:00Z
slug: agile-en-quelques-mots
title: Agile en quelques mots
---

Les quelques mots que j'utilise pour présenter "agile" au début des
formations ou lors de conférences. C'est quoi l'agilité, l'agile ?
Comment *le* résumer.

## Responsabiliser

L'agilité c'est acter de la complexité de notre monde. Complexe ?
beaucoup d'interconnections, beaucoup de dépendances, des
renouvellements technologiques très rapides, tout est entrelacé
(étymologie de complexe) le monde est devenu imprédictible, incertains.
L'agilité est une réponse cette complexité et quelle est cette réponse ?
Il faut responsabiliser les gens. En responsabilisant les gens nous
sommes en mesure d'appréhender le mieux possible cette complexité. Un
cercle vertueux peut aussi se créer : en étant responsabilisés les gens
sont motivés, heureux, et ils sont d'autant meilleurs à ce qu'ils font.
Ne soyez pas rétif, vous ne prenez *aucun* risque à responsabiliser les
gens, vous n'aurez que mieux.

## Effort/Valeur/Amélioration

Faites uniquement ce qui est utile, ce qui a de la valeur. Et faites un
pas en arrière à intervalles réguliers pour vous améliorer. Toujours
avoir en tête le compromis effort/valeur, toujours chercher à
s'améliorer.

Cette deuxième définition est très *lean*. Donc dans faire *ce qui est
utile, ce qui a de la valeur*, n'oubliez pas le "voir le tout" (*see the
whole*) : prendre le café avec ses camarades est utile (partage
d'information, détente, etc.)

Voilà avec ces deux cartes en main vous êtes agiles. :)
