---
date: 2010-02-14T00:00:00Z
slug: scrum-agit-cmmi-observe-cmmiscrum-billet-4
title: Scrum agit, CMMi observe. scrum et cmmi billet 4
---

Donc cette fameuse question : Scrum et CMMi sont-ils antinomiques ? ou
sont-ils complémentaires ?

Si l'on en croit le fameux article de Jeff Sutherland [Scrum and CMMI
Level 5: The Magic Potion for Code
Warriors](http://jeffsutherland.com/scrum/Sutherland-ScrumCMMI6pages.pdf),
que vous retrouverez traduit
[ici](http://www.fabrice-aimetti.fr/dotclear/public/mes-documents/Scrum_et_CMMI_Niveau_5_La_Potion_Magique_pour_les_Guerriers_du_Code.pdf)
grâce à Fabrice Aimetti (merci à lui), la réponse est oui, mille fois
oui. Mais bon, il s'agit d'une entreprise CMMi niveau 5 (ça ne court pas
les rues dans le coin, je ne parle pas de telle ou telle SSII qui s'est
fait tamponner niveau 5 sur son groupuscule indien de 10 développeurs et
qui étend magiquement le macaron à l'ensemble de son groupe...). Si tant
est que ayez la chance de travailler dans une structure CMMi niveau 5,
je suppose qu'elles sont rares celles qui implémentent Scrum ou Lean
comme dans l'exemple de Jeff Sutherland.

Mais il n'empêche je suis assez d'accord pour dire, selon mon expérience
personnelle que les deux se complètent bien, car je vois bien dans ma
pratique personnelle que selon les besoins je me tourne soit vers
l'agilité (Scrum/XP) soit vers CMMi. Alors dans quels cas ? Puisque
c'est cela la question.

Pour résumer : je me tourne vers l'agilité dès qu'il s'agit de produire,
de créer, donc de réaliser des projets. Je me tourne vers CMMi dès que
je dois auditer. Normal, rien de surprenant. C'est une joyeuse
lapalissade. CMMi est un modèle, Scrum est une méthode. Les mots sont
importants, une méthode, c'est un ensemble de procédés raisonnés pour
faire une chose ;  un modèle c'est une représentation, en informatique
(pour CMMi) des structures, des traitements, de pratiques... Une
représentation c'est aussi une image, un tableau, une photo prise à
l'instant T. La temporalité (oh là là on va m'accuser de branlette
intellectuelle, mais que voulez vous j'ai fait fac de lettres ) est
différente : action (mouvement, durée) du côté de Scrum, représentation
(état, analyse, définition, instant présent) du côté de CMMi.

Naturellement dans l'entreprise les deux sont requis : l'action
(création de valeur : les projets etc.), la représentation (indicateurs,
analyse, consolidation, etc.). La difficulté est que les deux se situent
sur des plans différents et ne sont pas forcément réalisés par les mêmes
personnes, ni comme on l'a vu dans le même rythme. Le scrummaster (allez
disons le chef de projet), les développeurs, etc. sont dans l'action ;
le management (pour utiliser un mot chapeau facile) consolide, analyse à
un niveau macro (le Scrummaster consolide et analyse au niveau de son
projet !). Rien n'est demandé par exemple spécifiquement au membre de
l'équipe scrum concernant l'utilisation de son temps. Il fait parti de
l'équipe et il participe (constamment) au projet. Pourtant on comprend
bien qu'il devra saisir à un moment ou à un autre son temps passé sur
tel ou tel projet dans un outil de consolidation groupe. Je me rappelle
aussi d'un projet Scrum qui avait très bien marché et sur lequel on me
demande une consolidation tâche/planning dans un cadre CMMi. En forme de
pied de nez j'ai remis les photos des radiateurs au fil des jours (c'est
une bonne pratique de prendre ses radiateurs en photo tous les jours)
mais cela n'est pas vraiment une bonne réponse à la question qui m'était
posée j'en conviens car c'est assez inexploitable en l'état... Il faut
donc que l'agilité ait l'intelligence de fournir à des modèles comme
CMMi les informations nécessaires (je pense que tout l'outillage XP est
un premier pont important. Il va donner des métriques importants). En
contrepartie il ne faut pas que l'inertie, le côté statique du modèle,
vienne rompre le rythme et le dynamisme du projet Agile.

Il faut essayer de réaliser des convergences c'est tout l'enjeu. CMMi
sera demandeur, et Scrum fournisseur. A mes yeux on a un pris le chemin
en sens inverse : aujourd'hui c'est Scrum qui vient dynamiter l'inertie
de CMMi dans les sociétés. C'est plutôt CMMi qui devrait venir
consolider les processus Scrum déjà en place. Consolider en donnant des
indicateurs forts qui permettront de mieux mener les projets agiles sans
les déranger par ailleurs. Une convergence riche d'enseignement mais qui
tient sur un équilibre probablement fragile.
