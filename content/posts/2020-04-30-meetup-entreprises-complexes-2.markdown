﻿---
date: 2020-04-30
slug: meetup-entreprises-complexes-2
title: Meetup Entreprises complexes, partie 2 
---

Avant de se lancer dans l'atelier de ce deuxième *meetup* sur le sujet des entreprises complexes, je vous propose un peu de matière. D'une part un regard sur cet élément historique sur lequel Laurent s'est appuyé : les structures d'organisations par Mintzberg, d'autre part un regard "intime" sur BENEXT en vous dévoilant un document élaboré l'année dernière sur une projection de BENEXT sur la pyramide de Dilts. Ce document a été partagé l'année dernière à tout le monde en interne, et le temps filant à toute vitesse, sera revu à la sortie de cette "crise". Cela va vous permettre d'avoir de la perspective sur nos propositions d'archétypes, et d'avoir une mise en œuvre concrète au travers de l'outil "Pyramide de Dilts".   


# La vidéo du meetup

{{< youtube id="Iq0byH0lR-w" >}}

## Et le *jamboard* partagé à l'occasion

{{< image src="/images/2020/04/jamboard-ec.jpg" title="Jamboard session Entreprises Complexes" >}}

# Retour arrière sur la structure des organisations par Mintzberg


{{< image src="/images/2020/04/mintzberg-1.jpg" title="Mintzberg structures" >}}


Voici un descriptif des éléments des structures des organisations par Mintzberg qui a servi de base au travail de [Laurent Morisseau](http://www.morisseauconsulting.com/).

## Les éléments de la structure

J'ai repris les mots d'un article de [HRMag, les parties de base de l'organisation selon Henry Mintzberg](https://www.hrimag.com/Les-6-parties-de-base-de-l-organisation-selon-Henry-Mintzberg)

#### 1 - Le centre opérationnel

À la base de toute organisation, on trouve ses opérateurs, c’est-à-dire ceux qui effectuent le travail de base de produire les biens ou de délivrer les services. Ces opérateurs (hommes et/ou femmes) constituent ce que Mintzberg appelle le centre opérationnel de l’organisation.

#### 2 - Le sommet stratégique

Selon Mintzberg, il n’y a pas une organisation, même parmi les plus simples, qui ne requiert au moins un manager à plein temps pour occuper, ce qu’il appelle, le sommet stratégique. C’est à partir du sommet stratégique qu’il est possible d’avoir une vue d’ensemble du système.

#### 3 - La ligne hiérarchique

Lorsque l’organisation grandit, elle a alors besoin de plus de managers — et dans ce cas non seulement des managers pour superviser les opérateurs, mais aussi des managers pour superviser les managers. Une ligne hiérarchique se trouve ainsi constituée, c’est-à-dire une hiérarchie d’autorité entre le centre opérationnel et le sommet stratégique.

#### 4 - La technostructure

Au fur et à mesure que l’organisation devient plus complexe, elle réclame encore un nouveau groupe de spécialistes, que Mintzberg appelle les analystes. Eux aussi remplissent des tâches administratives — planifier et contrôler le travail des autres —, mais d’une nature différente, que recouvre souvent le vocable de « staff ». Ces analystes forment ce que Mintzberg appelle la technostructure, cette dernière se situant en dehors de la ligne d’autorité hiérarchique.

#### 5 - Le support logistique

Selon Mintzberg, un grand nombre d’organisations ajoutent encore des unités de staff d’un type différent, afin de fournir différents services internes qui peuvent aller d’une cafétéria ou d’un service postal à un conseil juridique ou à un département de relations publiques et, etc. Mintzberg appelle ces unités et la partie de l’organisation qu’elles constituent le staff de support logistique.

#### 6 - L’idéologie

Selon Mintzberg, chaque organisation active est finalement composée d’une sixième partie qu’il appelle idéologie (ce terme est l’équivalent du terme « culture » qui est très populaire dans la littérature spécialisée). L’idéologie selon Mintzberg se nourrit des traditions et des croyances d’une organisation ; c’est ce qui la distingue d’une autre organisation et c’est aussi ce qui lui insuffle une certaine existence à travers le squelette de sa structure. L’idéologie est représentée sous la forme d’un halo qui entoure le système entier.


## Les différentes formes de structures

Au regard de cela Mintzberg observe différentes formes de structures. Je reprend leurs descriptions depuis [Wikiversity](https://fr.wikiversity.org/wiki/Structure_et_management_des_organisations/)


{{< image src="/images/2020/04/mintzberg-2.jpg" title="Mintzberg structures" >}}

#### La structure simple

C’est généralement une organisation de petite taille, relativement jeune où le mécanisme de coordination prédominant est l’ajustement mutuel ou la supervision directe. Le pouvoir est très centralisé, la ligne hiérarchique est très réduite, voire inexistante. La composante la plus importante de l’organisation est le sommet stratégique.

#### La bureaucratie mécaniste

Elle se rencontre dans les organisations âgées et d’une taille importante. Le mécanisme d’ajustement est la standardisation. Elle est adaptée aux environnements stables (possibilité de prévoir des standards) et simples.

#### La bureaucratie professionnelle

Elle se rencontre dans les organisations où le centre opérationnel est composé de professionnels très qualifiés dont la formation conduit à une standardisation des qualifications. Elle est relativement décentralisée en raison de la marge de manœuvre dont bénéficient les professionnels. Le centre opérationnel est la composante la plus importante de l’organisation. Elle est adaptée à des environnements stables mais complexes.

#### La structure divisionnalisée

Les très grandes organisations opérant sur des marchés différents sont généralement éclatées en division relativement autonomes les unes par rapport aux autres. Le mode de coordination dominant est la standardisation des résultats. Les décisions opérationnelles sont très décentralisées au niveau de chaque division alors que les décisions stratégiques sont centralisées.

#### L’adhocratie

C’est la configuration des organisations innovatrices. C’est une organisation de petite taille composée de spécialistes de différentes disciplines dont le mode de coordination est l’ajustement mutuel. Le centre opérationnel est constitué de professionnels dont le travail est peu formalisé. Elle est adaptée à des environnements dynamiques instables et complexes.

#### L’organisation missionnaire

Dans cette configuration l’idéologie définit les normes et croyances qui vont standardiser les comportements et assurer une forte coordination.

#### L’organisation politisée

C’est une organisation où les jeux de pouvoir entre les individus dominent le fonctionnement de l’organisation. Il s’agit d’une organisation en crise où le sens de la mission est perdu au profit des jeux de pouvoir. L’intérêt personnel des membres prend le pas sur tout autre considération. 

## Cycle de vie observé, cycle de vie proposé, nos propositions de structures

Voici des réflexions en cours sur nos propositions de nouvelles structures (basées sur celles de Mintzberg), et le cycle de vie associé. Ces trois étapes c'est un peu une photo de nos réflexions/évolutions. 

{{< image src="/images/2020/04/cycle-vie-1.jpg" title="Mintzberg cycle de vie" >}}

{{< image src="/images/2020/04/cycle-vie-2.jpg" title="Cycle de vie 2" >}}

{{< image src="/images/2020/04/cycle-vie-3.jpg" title="Cycle de vie 3" >}}

Avec toujours donc cette consolidation:

{{< image src="/images/2020/04/archetypes.jpg" title="Archétypes" >}} 

* [Slide archétypes / tableau](/pdf/entreprises-complexes/archetypes.pdf)
* [Slides archétypes / détails](/pdf/entreprises-complexes/slides-archetypes.pdf) 

# Toute ressemblance avec des personnes existantes ou ayant existé n’est absolument pas le fruit du hasard.

C'est le titre que nous avions donné à ce document de mars 2019. Je vous le partage "brut de décoffrage", il y a probablement du jargon BENEXT dedans, nos codes. Mais vous y trouverez une application de la Pyramide de Dilts, l'évocation d'un problème de *capacité* à régler au niveau de nos *convictions*. Depuis les choses ont bougé, et nous nous prêterons à nouveau à l'exercice à la sortie de la "crise". Qui "nous" ? Le sommet stratégique pour reprendre les termes de Mintzberg.       

Ce document a été partagé l'année dernière à toute l'organisation. Il fait partie des nombreuses façons de réfléchir et de communiquer sur nous-mêmes, notre sens, etc... (trop nombriliste ?).  

* [Fichier PDF : Toute ressemblance...](/pdf/toute-ressemblance.pdf)

{{< image src="/images/2020/04/toute-ressemblance.jpg" title="Pyramide BENEXT" >}}


## Sources

* [La première partie sur les entreprises complexes](/2020/04/meetup-entreprises-complexes/)
* [La deuxième partie sur les entreprises complexes](/2020/04/meetup-entreprises-complexes-2/)
* [Wikiversity sur Mintzberg](https://fr.wikiversity.org/wiki/Structure_et_management_des_organisations/La_typologie_des_structures_d%27organisation_par_Mintzberg)
* [HRMag sur Mintzberg, 2019](https://www.hrimag.com/Les-6-parties-de-base-de-l-organisation-selon-Henry-Mintzberg)
* [Les formes organisationnelles selon Henry Mintzberg, Alain Dupuis, Richard Déry](/pdf/formes-organisationnelles-mintzberg.pdf)
* [Un modèle pour l'entreprise agile, par Laurent Morisseau](http://www.morisseauconsulting.com/2019/09/28/entreprise-agile/)
* [Les entreprises complexes, partie 1](/2019/09/modele-des-entreprises-complexes-partie-1/)   
* [Les entreprises complexes, partie 2](/2019/09/modele-des-entreprises-complexes-partie-2/) 
* [Pyramide de dilts et harmonisation de l'organisation](/2019/04/pyramide-dilts-harmonisation-organisation/)  
* [Slides du meetup 2019](https://pablopernot.fr/pdf/meetup-modele-entreprises-complexes.pdf)
* [Fichier PDF : Toute ressemblance... sur la pyramide de Dilts chez BENEXT en 2019](/pdf/toute-ressemblance.pdf)
