---
date: 2023-03-11
slug: frugarilla 
title: "Frugarilla"
draft: false
---

Je voulais signaler par cette missive à mon réseau que j'avais embarqué depuis peu (décembre 2022) avec un petit équipage dans une aventure qui se nomme **Frugarilla**, de la concaténation de "guerilla" et "frugal". L'objectif de ce véhicule est d'explorer les possibilités d'un sujet épineux : le futur du numérique, dans ce qu'il devra être de radical pour être un compatible avec l'environnement des années à venir. Notre ligne éditoriale se nomme justement "les numériques essentiels 2030". 

L'équipage se compose d'un noyau coeur de quatre personnes qui me donne de l'entrain tellement nous nous complétons bien. Deux sont les sachants, les experts, Sara et Alexis. Dominique joue le rôle de sponsor, de facilitatrice dans les tentacules de notre réseau d'entreprises (n'oublions pas que nous sommes une filiale d'un cabinet de conseil américain), et aussi, disons-le de garde-chiourme à mon égard afin que je ne sabote pas trop tous ces processus. Pour ma part j'en suis l'entrepreneur : faire, donner consistance, aboutir. Le groupe va vite un peu s'élargir.

Le plaisir que j'en tire est de deux sortes. D'une part, travailler sur le sujet du numérique responsable, des enjeux des limites planétaires (le fameux "donut") : difficile de faire mieux question sens, et donc d'engagement. Cela m'oblige à interroger mes contradictions et je n'en sors pas souvent gagnant. D'ailleurs est-ce que le numérique est compatible avec le futur, cela reste à prouver. 

L'agilité ? L'organisation ? Sont toujours présents et sont des facteurs essentiels de cette conduite du changement. J'observe depuis que je suis plongé dans ce sujet un petit monde de gens qui se connaissent et qui sont confrontés aux problématiques classiques du changement. Mais avantage ou inconvénient, la transformation que j'évoque, nous serons tous obligés de nous y atteler, et espérons, de la réussir. 
Ça va nous changer des pseudotransformations agiles ou digitales que le marché a vidé de sens.  

Le site : https://frugarilla.fr (vous y trouverez nos podcasts, événements, séminaires, accompagnements, etc.)





