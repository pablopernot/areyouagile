---
date: 2025-02-06
slug: petit-manuel-de-pensee-organisationnelle
title: "Petit manuel de pensée organisationnelle"
draft: false
symbol: 7
---

J'ai profité d'un temps qu'on m'a accordé l'année dernière en échange de bons procédés pour consolider, réécrire, me projeter, dans un monde en 2052, qui se baserait sur toutes nos connaissances actuelles en organisation, enfin les miennes. J'espère que vous me ferez le plaisir de l'acheter et de le lire, et s'il vous plait, d'en parler autour de vous. 

Trouver le livre (EAN 9782342377484): 

{{< livres show="livres/livres.csv" >}}

{{<br>}}

*Le petit manuel commence à apparaitre dans les libraires (cf liens ci-dessus), où vous trouvez aussi l'édition électronique. Mais vous pouvez aussi le commander dans votre librairie de quartier (la mienne c'est l'Utopie dans le 11e).*

**Un manuel pour s'organiser ? Comment faire les groupes ? Comment prendre les décisions ? Comment communiquer ? Cela reste très accessible (pas de jargon). Et c'est la somme de 15 ans de blogging finalement.**

{{< image src=/images/2025/01/petit-manuel-pensee-organisationnelle.png alt="Petit manuel de pensée organisationnelle" width="600">}}

L'année dernière, j'en avais partagé un chapitre : [Qui prend les décisions](/2024/01/qui-prend-les-décisions/)

Vous avez aussi sur la page *publibook* un autre extrait. 

Trouver le livre : 

{{< livres show="livres/livres.csv" >}}

{{<br>}}

* Mettre un retour sur [Babelio](https://www.babelio.com/livres/Pernot-Petit-manuel-de-pensee-organisationnelle/1800104)  

{{< figure src=/images/2025/01/couverture-petit-manuel-pensee-organisationnelle.jpg >}}

{{< figure src=/images/2025/01/sommaire-petit-manuel-pensee-organisationnelle.jpg >}}






