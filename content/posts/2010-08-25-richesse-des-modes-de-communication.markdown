---
date: 2010-08-25T00:00:00Z
slug: richesse-des-modes-de-communication
title: Richesse des modes de communication
---

J'ai pu récemment mettre en oeuvre un atelier très intéressant ["Offing the Offsite" de James Shore](http://jamesshore.com/Presentations/OffingTheOffsiteCustomer.html)
. 
{{< image src="/images/2010/08/communicationModes-300x258.gif" title="Communications" >}}

Son but est de mettre en évidence la richesse de la conversation (sous
entendu générée par une user story dans le mode agile) face à la
complexité d'écrire ou de lire une spécification. Je résume l'exercice
mais je vous encourage à aller voir la page de [James
Shore](http://jamesshore.com/Presentations/OffingTheOffsiteCustomer.html).
Il fournit tous les éléments en PDF, ainsi qu'un scénario. On délivre à
la moitié des participants une feuille A4 avec un dessin tarabiscoté, on
leur demande d'écrire la spécification qui permettra de réaliser ce
dessin. 10mn passe, on appelle l'autre moitié des participants et on
leur demande de réaliser les spécifications, ils ont 10mn. Dans
l'exercice 2 on prend soin de changer les dessins tarabiscotés. Puis on
demande aux personnes de décrire à leur partenaire, par le biais d'une
conversation, les spécifications du dessin. Les "développeurs" ne
peuvent naturellement pas voir le dessin (10mn à nouveau). Sans surprise
on percevra l'évidente richesse d'une communication orale sous forme de
conversation, à celle froide d'un document écrit (je renvoie aussi ici
au diagramme proposé par **Scott Ambler** placé ci-dessus).

{{< image src="/images/2010/08/original1-212x300.jpg" title="Original" >}}

{{< image src="/images/2010/08/photo2-212x300.jpg" title="dessin 1" >}}

{{< image src="/images/2010/08/photo1-212x300.jpg" title="dessin 2" >}}

Sur le second c'est encore plus flagrant (en cliquant sur les images,
vous pouvez avoir les versions grandes tailles). J'ai du charger la
luminosité ou les contrastes afin de faire ressortir les traits (d'où
l'effet baveux...)

{{< image src="/images/2010/08/original2-212x300.jpg" title="Original" >}}

{{< image src="/images/2010/08/ex1des2-212x300.jpg" title="dessin 1" >}}

{{< image src="/images/2010/08/ex2des2-212x300.jpg" title="dessin 2" >}}

Durant cette session j'ai pu faire ressortir deux écueils présents lors
de la phase de spécifications écrites qui disparaissaient avec la
conversation :  des problèmes de terminologie ("mais qu'a-t-il voulu
dire par là ???"), des problèmes de déséquilibre entre la sur-qualité de
certains éléments : on s'applique à faire quelque chose que l'on pense
avoir compris, et l'absence d'éléments que l'on a omis. La conversation
permet la reformulation si il y a un problème de terminologie. Elle
permet aussi de dire : "c'est bon, c'est assez, inutile d'aller si
loin", et "tu as oublié ceci ou cela". C'est évident mais toujours très
instructif.

Élémentaire mon cher Watson.
