---
date: 2014-12-02T00:00:00Z
slug: sarah-journal-de-bord-jour-3
title: Sarah, journal de bord, jour 3
---

Voici la suite du journal de bord de Sarah, ma fille, qui en troisième
effectue son stage d'observation. Vous trouverez les deux premiers jours
[ici](/2014/12/sarah-journal-de-bord-jours-1-et-2/). Là, à gauche, le
gang des agilistes ([Raouf](http://agillys.com/) & Sarah).

{{< image src="/images/2014/12/agilistes.jpg" title="Agilistes" >}}

## Jour \#3, Stage d'observation de 3ème, Sarah P.

Nous sommes chez "N", producteur d'énergie numérique. J'aime bien leur
logo simple : un cube en volume. À l'intérieur du bâtiment de
l'entreprise c'est "sympa" : très accueillant. Il y a des
[Nerfs](http://en.wikipedia.org/wiki/Nerf), les poufs sont des pièces de
puzzle. Dans cette entreprise il y a l'air d'avoir une bonne "ambiance".
Il y a plein de post-it sur les murs, une boite à idée.

{{< image src="/images/2014/12/storm.jpg" title="Le storm" >}}

Sinon ils ont l'air très organisés.

Mon père en arrivant dit bonjour à tout le monde, puis commence à
travailler sur son ordi : il travail dessus tout le temps. Papa
interview des personnes dans les différentes "teams" pour après faire un
rapport : dire ce qui va et ce qui ne va pas.

J'assiste à une "démo". Cette réunion était une réunion de fin de
"sprint" : ils ont fait un récap de ce qu'ils ont fait durant ces trois
dernières semaines de travail. Mon père prend des notes, il commente.

{{< image src="/images/2014/12/retro.jpg" title="La rétro" >}}

Les locaux de "N" sont très "protégés", il faut un badge pour entrer et
sortir. Certaines choses restent confidentielles, pour que les
concurrents ne leur piquent pas leurs idées.

J'assiste à une "rétrospective" : une réunion dans laquelle ils vont
parler de comment ils pourraient s'améliorer ou arrêter de faire des
choses qui ne marchent pas. On pourrait faire des rétrospectives au
collège à la fin de chaque trimestre. Lorsque l'on fait des réunions au
collège, on dit beaucoup ce qui ne va pas mais pas ce qui va bien. On ne
s'écoute pas les uns les autres. On n'ose pas de dire certaines choses
lorsqu'un professeur est présent. À la fin de l'année on pourrait faire
un graphique sur l'évolution de la classe.

{{< image src="/images/2014/12/avecgilles.jpg" title="Avec Gilles" >}}

*La soirée se finit avec tonton Gilles, Godfather of Office365, dans un
bon italien du Marais. Demain formation à Neuilly pour un groupe connu
de l'énergie en France, et soirée old time music au O'Connolly's Corner
dans le 5ème. On vient de nous annoncer la grève des trains pour
vendredi...*

## Les liens

-   [les deux premiers jours](/2014/12/sarah-journal-de-bord-jours-1-et-2/)
-   [le troisième jour](/2014/12/sarah-journal-de-bord-jour-3/)
-   [le quatrième jour](/2014/12/sarah-journal-de-bord-jour-4/)
-   [le dernier jour](/2014/12/sarah-journal-de-bord-jour-5/)