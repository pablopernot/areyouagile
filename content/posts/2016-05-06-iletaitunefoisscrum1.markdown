---
date: 2016-05-06T00:00:00Z
slug: il-etait-une-fois-scrum-1
title: Il était une fois scrum 1
---

Pour un client, récemment, j'ai écrit la petite histoire de Scrum. Juste
une retranscription de mon discours lors de mes présentations. C'est
sans ambition, quelques pages, et ma façon de procéder. Pour avoir
quelque chose de plus solide, lire le livre de
[Claude](http://www.aubryconseil.com/), qui j'en sûr trouvera des
variations ici. [Dragos](http://www.andwhatif.fr/) aussi ne fait pas
exactement la même chose. Mais avec ces deux là je m'accorde assez pour
savoir que ces variations demeurent des détails. En tous cas vous pouvez
tous avoir vos variations sur ce thème. Disons que je trouvais dommage
d'avoir écrit cela et de ne pas le publier... voici donc ces quelques
petits articles sans ambition pour démarrer avec scrum.

Et autant transformer cela en histoire autour de Peetic.

-   2012 : [Episode 1](/2012/11/peetic/)
-   2012 : [Conversations](/2012/12/peetic-conversations/)
-   2014 : Un article de [l'idée au plan de
    livraison](/2014/06/de-lidee-au-plan-de-livraison/)
-   2015 : [Un peu de
    croquettes](/2015/05/peetic-2015-un-peu-de-croquettes/)

Enfin ce texte viendra compléter le guide de survie à l'agilité :

-   [Guide de survie à
    l'agilité](http://areyouagile.com/pdf/guiderapide.pdf) (glossaire,
    descriptions courtes des rituels, bibliographie, etc.).

## Ma petit histoire de scrum

Ce mois d'avril, grands chantiers, on démarre en Scrum chez Peetic. On a
défini nos objectifs, ils sont simples et clairs. On sait que beaucoup
d'argent gravite autour de ces petites bestioles (chiens, chats), et on
se dit que faire un moteur de rencontre pour animaux peut nous permettre
de vendre pas mal de services et de biens autour. Les six premiers mois
on veut 2000 inscrits. Les six mois suivant 30000€ de vente. Avoir des
objectifs clairs c'est essentiel si on souhaite que tout le monde les
comprenne. Il s'agit des objectifs avec le plus de valeur : les plus
significatifs. Mais il faut donner du relief, six mois c'est déjà long
pour nous, on va se lancer dans des *releases*, des mises sur le marché,
de trois mois. Car toutes nos idées demeurent des hypothèses avant de
vraiment être confrontées à la réalité. "Aucun plan ne survit au premier
contact avec l'ennemi" disait Von Moltke.

## La phase d'étude, de préparation, des fois appelée sprint zéro

Comme toujours on veut tout, nos sponsors veulent tout, tout de suite.
Mais pour réussir un produit et apprendre rapidement, sortir des
hypothèses pour de vrais apprentissages terrains, il faut donner du
relief à nos réalisations. Souvent on fait cela lors d'une phase
d'étude, ou sprint zéro.

Fin avril, on prend 5 jours ouvrés sur une période de un mois pour
aligner, harmoniser les points de vue et les attentes des différents
acteurs. Normalement le critère clefs de succès est déjà défini comme
annoncé plus haut. Cette période c'est le sprint zéro ou la phase
d'étude, ou de préparation, ou l'avant projet, ou... 1 mois calendaire,
5 jours de travail, pour 6 mois de Scrum, humm, ça parait correct.

Pour harmoniser les points de vue il faut des représentants de
différents types d'acteurs, ainsi on se retrouve avec coach, product
owner, équipe, designer/UX, experts métiers, sponsors, utilisateurs,
bref tous les gens qui seront influencés et impactés ou qui tiennent à
influencer le projet. Sur les 5 jours à passer on garde tout le monde
les trois premiers. Il s'agit de faire de gros *brainstorming* pour que
chacun amène sa vision : technique, métiers, ergonomie, etc. Que chacun
amène ses contraintes et ses attentes, ses idées, ses craintes, son
savoir. Et surtout que chacun entende l'autre et comprenne plus tard tel
ou tel point de vue. Ce qui ne veut pas dire que tout le monde décide de
tout.

Oui hein, je voulais dire, dans Scrum on parle de "dev team" dans
l'équipe scrum, et beaucoup pense qu'il ne s'agit que des développeurs
au sens codeurs, pas du tout, c'est l'équipe qui développe au sens
large, autant de la documentation, du design, etc, que du code.

En savoir plus sur la constitution de l'équipe et la co-localisation :

-   [Penser son organisation sur le
    terrain](/2014/02/penser-son-organisation-sur-le-terrain/)
-   [La horde agile](/2014/01/mini-livre-la-horde-agile/)

Un gros *brainstorming* divergent ce premier jour. Que tout soit dit,
affiché. Le coach va demander durant le 2ème jour de commencer à
converger, et ultimement, c'est le *product owner* ou le *product
manager* (en tous cas celui qui porte le produit) en accord avec la
vision qui priorisera (si besoin le coach va utiliser des outils pour
que les débats ne s'enveniment pas). L'important est de mettre du relief
: on fera tout le périmètre fonctionnel ? peut-être. On essayera de
faire le mieux possible avec la capacité octroyée. Grâce à cette mise en
relief, à cette priorisation, on va mettre en évidence des chemins
clefs, des points stratégiques clefs. Toute cette priorisation, cette
mise en relief est de la responsabilité du *product owner*, qui peut
naturellement écouter chacun des acteurs, mais les coachs agiles se
tourneront vers lui pour trancher. Il n'est pas seul, il peut bien
s'entourer. Mais on a besoin de choisir à la fin.

Inutile de parler de tout ce que va impliquer le produit (cela ne serait
que des hypothèses, et beaucoup se révèleraient fausses), il suffit de
mettre en évidence les points clefs et ensuite de les détailler pour
travailler lors de ces trois premiers mois. Idéalement ensuite on
sortira en production pour valider nos hypothèses : ce que nous
développons fait bien croitre le taux d'inscription (et pérennise ces
inscrits).

Ce n'est pas le cas ? Et bien on aura *seulement* consommé trois mois
pour se rendre compte que nos hypothèses ne sont pas bonnes. Ouf, au
lieu de le réaliser à la fin du programme. On a réussi à avoir déjà pas
mal d'inscriptions ? Super. On a déjà commencer à atteindre une partie
clef des attentes du produit, et toute la suite du développement se
basera sur moins d'hypothèses et sur plus sur une vraie connaissance
(autant technique, que des retours d'informations sur les envies, les
besoins et les comportements utilisateurs).

Les deux jours suivants se font en comité plus réduit, disons l'équipe
coeur : on décline les chemins stratégiques mis en évidence précédemment
avec un plan de bataille terrain : on va développer cela, puis cela,
etc. On propose une première projection sur le contenu à venir des trois
mois mais cela reste une
[estimation](/2016/04/les-estimations-ne-sont-pas-uniquement-liees-a-la-complexite-bordel/)
assez floue (comme toutes ces estimations réalisées au tout début d'une
réalisation). De toutes les manières grâce à notre priorisation par
valeur on aura le mieux possible pour le temps imparti. Si on estime que
ce n'est pas assez on va le voir venir et on prendra une décision
(changer le périmètre ? Changer la date de sortie ?).

-   Plus sur les estimations : [Elles ne sont pas uniquement liées à la
    complexité](/2016/04/les-estimations-ne-sont-pas-uniquement-liees-a-la-complexite-bordel/).
-   Plus sur les estimations : [La malédiction du jour
    homme](/2012/03/la-malediction-du-jour-homme/).

A la fin de ces cinq jours ouvrés on a a) une fiche de cadrage global,
avec 1 ou 2 premiers critères de succès clefs mesurables, b) des
*personas* qui nous disent quelles sont les attentes,quels sont les
comportements, les caractéristiques des profils cibles, c) un *impact
mapping* qui met en évidence des chemins stratégiques clefs de
réalisation, d) un *user story mapping* qui décline de façon
opérationnelle (comment on réalise ?) les chemins stratégiques relevés
précédemment. On pensait mettre en relation des possesseurs d'animaux
façon club de rencontres, patatras, de nos conversations a émergée
l'idée plutôt de de gardiennages en commun, et de balades en commun. On
spécifie aussi e) [une définition de
fini](/2016/04/nul-nest-cense-ignorer-la-definition-de-fini/) qui
détermine nos critères de qualité : il faut la garder compact qu'elle ne
sorte de la tête de personne. Et enfin f) Une première estimation
globale qui vaut ce qu'elle vaut comme toutes ces premières estimations
(réalisée par l'équipe), et du coup g) un premier *plan de release* :
quel contenu devrait-on avoir et quand ? Comme agile est assez binaire
dans sa gestion d'une réalisation : c'est ou ce n'est pas fait, c'est
fini ou ce n'est pas fini (et surtout pas : "on a 80% de la
fonctionnalité F" ce qui on le sait ne veut rien dire), c'est très
facile de lire les *plans de release* et de se projeter sur des
ensembles qui font sens sur le plan "business". On connaît l'équipe
puisqu'elle a participé à cette phase d'étude que l'on appelle parfois
par abus de langage : sprint zéro. Et c'est elle qui estime (pas les
managers, ni uniquement les techos, l'équipe et les profils hétérogènes
qui la composent). On a aussi pris soin de commencer à préciser les
éléments du haut de notre liste de fonctionnalités à développer (ce que
l'on appelle notre *backlog*). Comme toujours c'est le *product owner*
qui tranche et arbitre les débats, il peut s'appuyer sur toutes les
compétences qu'il veut mais il porte le **quoi** et le **pourquoi**.
L'équipe porte le **comment**. Le scrummaster et/ou les coachs agiles
facilitent, ils sont donc neutres, et s'occupent du cadre pour permettre
à chacun de s'y émanciper, de prendre l'espace.

-   Plus sur la définition de fini : [Nul n'est censé ignorer la
    définition de
    fini](/2016/04/nul-nest-cense-ignorer-la-definition-de-fini/)
-   Plus sur les [apprentissages des rôles de scrummaster, product
    owner, et
    équipier](/2015/09/trois-apprentissages-clefs-dune-approche-agile/).

### un "persona" (clique dessus pour agrandir)

{{< image src="/images/2016/05/jessica.jpg" title="Persona Jessica" >}}

### Un impact mapping (clique dessus pour agrandir)

{{< image src="/images/2016/05/impact-mapping.jpg" title="Impact Mapping" >}}

### Tiens un autre impact mapping (clique dessus pour agrandir)

{{< image src="/images/2016/05/impact2.jpg" title="Impact Mapping 2" >}}

### Ma pyramide à moi de l'expression du besoin agile

{{< image src="/images/2016/05/expression-besoin.jpg" title="Expression du besoin" >}}

### Une User Story Mapping

{{< image src="/images/2016/05/user-story-mapping.jpg" title="User Story Mapping" >}}

## Le début des itérations

On est prêt, on démarre la première itération (de deux semaines, elles
feront toutes deux semaines, c'est important cette régularité pour nos
calibrages. Estimation, engagement, et planification : répétition d'une
cadence observée). Un calibrage, un réglage, cela prend trois à quatre
itérations (quelques soit la durée de l'estimation... c'est vivre un
cycle qui est important), donc autant les itérations sont courtes mieux
c'est. On se calibre plus vite, on apprend plus vite.

Lundi la matinée est consacrée au **sprint planning**. Le *Scrummaster*
s'est assuré que les invitations sont bien parties, que la salle est
réservée, que tout le monde est au courant du contenu de la réunion, et
que le matériel nécessaire est bien là (post-it, feuille, projecteur,
etc). On va avoir une conversation sur les fonctionnalités du backlog.
Une par une : dans l'ordre du backlog, celui de la valeur pour le
*product owner*. Jusqu'à que le temps soit écoulé, ou presque.
L'objectif est de bien détailler les attendus, de lever les inconnues,
les quiproquos. On utilise les conversations autour de l'estimation pour
les rendre évident, pour clarifier les points de vue de chacun. Le PO
n'estime pas. Ni le scrummaster qui est neutre (il ne fait rien pour
avoir le pouvoir de libérer la productivité de l'équipe). Les débats
sont passionnants pour tous. Le métiers, le PO (product owner), découvre
que les *techos* ne cherchent pas à les entuber, et comprend pourquoi
parfois quelque chose de simple ne l'est finalement pas, et pourquoi
parfois quelque chose de compliqué se révèle simple des fois. Les
*techos* comprennent que le métiers, le *product owner*, n'a pas de
problème de santé mentale et ne change pas d'avis pour les embêter, mais
pour de bonnes raisons. Quinze minutes avant la fin de la réunion le
*scrummaster* alerte tout le monde : il va falloir se projeter sur ce
qui est réalisable dans les deux semaines à venir. Le *product owner*
choisi l'ordre des *user stories*, des fonctionnalités, l'équipe annonce
jusqu'où elle pense pouvoir aller. Ce qu'elle pense pouvoir finir. Car
les agilistes sont des maniaques : ou c'est fini, ou cela ne l'est pas
(autant ils sont perçus comme des [cowboys
hippies](/2014/11/les-cowboys-hippies/), autant ils sont binaires sur ce
point fini/pas fini). Naturellement finir c'est respecter la définition
de fini (les aspects qualité, techniques, etc), et les aspects
fonctionnels métiers sur lesquels la conversation avec le *product
owner* vient d'avoir lieu et dont l'arbitrage final reste au PO. Donc
c'est le moment où l'on demande à l'équipe : jusqu'où penser aller ?
Disons que l'équipe nous dit les 5 premières. C'est le *sprint backlog*,
le périmètre imaginé de l'itération.

Une règle importante : on veut éviter les effets tunnels. Ainsi on ne
veut des *user stories* qui puissent se finir dans le temps imparti, les
deux semaines, et on en veut au moins trois. Cela induit une règle : ne
peut être pris dans l'itération que des user stories dont l'effort est
inférieur à un tiers de la durée de l'itération. Lors du sprint 0 la
première estimation aura permis d'alerter le PO sur des user stories
importantes dont l'estimation dépasse allègrement cet effort maximum (je
parlerai des estimations un autre jour). Pour le PO c'est un message
important : il doit découper la user stories en plusieurs user stories
plus petites (et croyez moi il n'aime pas ça) si il veut que le
scrummaster autorise l'équipe à pouvoir les réaliser.

Voilà on a le contenu de l'itération : les cinq premières user stories
du backlog. Elle quitte le backlog (qui appartient au PO) pour tomber
dans l'escarcelle de l'équipe. L'équipe passe l'après-midi (deux
nouvelles heures) à découper en tâches les user stories : quelles seront
les tâches nécessaires l'aboutissement de ces user stories selon les
attentes de la définition de fini et les attentes métiers discutées
précédemment avec le product owner : du code, de la documentation, des
tests, du design, des jeux de données, etc etc etc. Autant le PO n'aime
pas découper ses fonctionnalités (il veut tout naturellement ! mais on
aura tout on veut découper pour avancer sereinement, ressentir nos
progrès, aborder mieux les questions, avoir du feedback, etc.), autant
l'équipe n'aime pas découper en tâches non plus ! Réfléchir avant de
faire c'est une habitude à prendre car cette découpe c'est du *design*,
de la conception finalement. Et cela va nous permettre de savoir où nous
en sommes ("reste à faire") durant les deux semaines au travers des
*burndown* (de tâches) et *burnup* (de stories)

Voilà c'est parti, l'itération débute. Qu'est ce qui va se passer durant
ces 9 journées restantes ? Les membres de l'équipe vont essayer de
produire, réaliser ce qui aura été défini durant le sprint planning.
Tous les jours ils vont montrer l'avancé de leurs travaux avec un mur
visuel de post-its. Ce mur sert à les synchroniser entre eux et à
communiquer au monde extérieur. Ils vont aussi pour suivre leurs travaux
réaliser un *burndown* : une courbe descendante du travail restant à
faire (pour le burndown je compte les tâches et je décrémente quand
elles arrivent à "fini"). Et un *burnup*, une courbe ascendante, des
validations réalisées par le Product Owner. Car en effet on espère, on
souhaite, que le product owner valide les user stories, les éléments
définis lors du sprint planning, au fil de l'eau, pour limiter les
risques et la pression. En validant au fil de l'eau pas de surprise, pas
de pression. On obtient régulièrement du feedback, et on lève
régulièrement des hypothèses. La qualité vient de la régularité.

Des compléments ici ou ailleurs :

-   [La matinée typique d'un
    scrummaster](http://www.aubryconseil.com/post/Une-matinee-typique-d-un-ScrumMaster),
    par Claude
-   [L'après-midi révélatrice d'un
    scrummaster](http://www.aubryconseil.com/post/Une-apres-midi-revelatrice-de-ScrumMaster),
    par Claude
-   [Le jour où j'ai découvert que j'étais un mauvais
    scrummaster](http://www.theobserverself.com/revelations/mauvais-scrum-master/),
    mensonge éhontée de Géraldine.
-   [Du scrummaster au coach
    agile](http://www.areyouagile.com/2016/03/du-scrummaster-au-coach-agile/),
    un cheminement classique
-   [Comprendre le
    MVP](http://frank.taillandier.me/agile/2016/01/28/comprendre-le-mvp/)
    par Frank qui a traduit un texte de Henrik Kniberg
-   [Introduction à la stratégie du product
    owner](http://www.areyouagile.com/2012/06/introduction-a-la-strategie-et-la-tactique-du-product-owner/),
    toujours d'actualité avec Alexis, de Praxeo, ma période Jerry
    Garcia.
-   La suite avec [Le product owner
    contre-attaque](http://www.areyouagile.com/2012/09/le-product-owner-contre-attaque/),
-   Et la dernière partie sur les [stratégies du product
    owner](http://www.areyouagile.com/2013/01/retours-sur-la-strategie-du-product-owner/)
    (toujours dans ma période Jerry Garcia)
-   Vous pouvez toujours jeter un oeil au
    [Scrumshot](/images/2012/11/scrumshot_1_0.svg) (c'est du svg)
-   En savoir plus sur les [cowboys
    hippies](/2014/11/les-cowboys-hippies/)

### Tiens un plan de release (clique dessus pour agrandir)

{{< image src="/images/2016/05/release-plan-large.jpg" title="Plan de release" >}}

### Un mur scrum

{{< image src="/images/2016/05/taskboard.jpg" title="Taskboard" >}}

### Un burndown et un burnup

{{< image src="/images/2016/05/burnup.jpg" title="Burndown et burnup" >}}

Les trois épisodes de la série et le guide de survie :

-   [Il était une fois scrum \#1](/2016/05/il-etait-une-fois-scrum-1/)
-   [Il était une fois scrum \#2](/2016/05/il-etait-une-fois-scrum-2/)
-   [Il était une fois scrum \#3](/2016/05/il-etait-une-fois-scrum-3/)
-   [Guide de survie à
    l'agilité](/pdf/guiderapide.pdf) (glossaire,
    descriptions courtes des rituels, bibliographie, etc.).
