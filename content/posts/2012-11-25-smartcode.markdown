---
date: 2012-11-25T00:00:00Z
slug: smartcode
title: SmartCode
---

Aujourd'hui je souhaite faire la promotion (!) de l'offre **SmartCode**
dont [SmartView](http://www.smartview.fr) (ma société) a lancé
l'initiative en septembre. Rien de forcément original, juste un
assemblage qui me semble assez rare d'une bonne approche que j'apparente
à l'agilité, et dont je suis fier.

{{< image src="/images/2012/11/smartcode-baseline-clair-quadri-300.jpg" title="SmartCode" >}}

Faisant de l'accompagnement projet depuis de longues années, on pourrait
dire une quinzaine, mes compagnons et moi (chez 
[SmartView](http://www.smartview.fr)) nous nous étions jurés de ne
jamais -au grand jamais- réaliser de projets au forfait (engagement de
résultat) tant cette promesse est un leurre dans une relation
client/fournisseur, avec toutes les frustrations que cela induit.
Spécifiquement orienté conseil & formation
[SmartView](http://www.smartview.fr) ne faisait donc pas de projet. Mais
nous ne cessions d'en accompagner, d'en conseiller, d'en aider, d'en
réaliser. Souvent la bataille contre l'engagement de résultat avait
lieue. Puis il nous a semblé que la majorité de nos contacts
s'éveillaient au dégoût du projet classique et de son engagement de
résultat. Trop de souffrance, trop de déni, trop d'absurdité ? Etait-ce
un moment clef ?

Nous avons donc décidé aujourd'hui de re-franchir le rubicon : faire des
projets. Mais naturellement en s'imposant des critères qui leurs
donneront toutes les chances de réussir. Faire réussir un projet c'est
quoi ? Un projet ou un produit réussi c'est lorsque tout le monde est a
minima satisfait, au mieux heureux.

Les contraintes que nous nous imposons à nous et à notre client ne sont
pas nombreuses mais elles sont féroces.

## Important

Les projets **SmartCode** sont des projets importants, sinon pourquoi
les faire ?

## Engagement de moyens

Les projets **SmartCode** sont nécessairement un engagement de moyens
(Le seul contrat que nous aurons stipulera les profils et taux
journaliers associés). L'engagement de résultat est un mensonge : on ne
peut pas prédire un scope, un délai, des moyens même 3 à 6 mois à
l'avance (encore moins si c'est plus). L'engagement de résultat est
néfaste : il fossisile la richesse du projet ou du produit. Nul espace
pour l'innovation, la performance, l'adaptation, l'incertitude. Or
l'incertitude et l'exposition au risque sont nécessaire à une bonne
performance projet, produit. Cette fossilisation génère frustrations et
absurdités.

## Implication

Les projets SmartCode sont nécessairement menés par un représentant du
produit ou du projet (disons le "product owner") qui vient de chez le
client, et qui alloue le temps nécessaire à la réussite du projet ou du
produit. L'implication est forte. (Il sera accompagné d'un facilitateur,
d'une personne qui appliquera nos préceptes méthodologiques, qui portera
notre expérience projet et produit)

## Partage

Les projets **SmartCode** se font nécessairement en équipe mixte : une
partie de l'équipe est proposée par le client, l'autre par **SmartCode**
(interne **SmartView**, ou freelance de notre entourage). Notre volonté
est de faire progresser la partie de l'équipe client vers notre approche
technologie, méthodologique et culturelle. Idéalement l'équipe client
devient autonome.

## En situation

Les projets **SmartCode** se déroulent chez le client, au coeur du
produit ou du projet.

## Equipe dynamique

Une équipe **SmartCode** contient 7 personnes au maximum (cela inclu le
facilitateur et le représentant du produit ou du projet). Pour les
grands projets, nous proposerons une ferme d'équipes.

## Des choses finies

En contre-partie de l'engagement de moyens évoqué plus haut l'équipe
(mixte) **SmartCode** s'engage à délivrer régulièrement (2 à 3 semaines)
des choses finies. Si cela ne convenait pas, le client peut arrêter **à
tout moment** l'ensemble du projet.

## Développement mature

Dans son accompagnement technologique et culturel l'équipe **SmartCode**
met en place un dispositif mature de développement : tests automatisés,
intégration continue, TDD, BDD,etc. Si la totalité de l'équipe n'est pas
mature, nous l'accompagnons vers cette destination. Nous avons
communiqué auprès de pas mal de nos clients et contacts. Les retours
sont bons. Nous continuons à communiquer. Nous pensons lancer le premier
pour le début de l'année.

Le feedback reçu jusqu'à présent ne nous surprend pas.

## Confiance mutuelle

*- Comment cela vous êtes en régie -en engagement de moyens- et vous me
demandez d'être impliqué. Je paye et je suis responsable ?*

Oui, la confiance est indispensable. C'est votre projet. Il est
important. Nous devons aussi avoir confiance en vous, nous sommes une
petite structure et vous proposer d'arrêter **à tout moment** le projet
ou le produit est un risque pour nous (Nous delivrerons ensemble des
choses finies toutes les 2 ou 3 semaines pour vous permettre de juger).

## Coût total

*- C'est cher, avec un projet au forfait classique, j'en aurai pour
beaucoup moins.*

D'apparence le coût est plus élevé. Mais vu la qualité de la valeur
dégagée dans ce type de projet en comparaison avec les pertes
sous-estimées de la majorité des projets au forfait (engagement de
résultat), non ce n'est pas cher. C'est le juste prix pour un vrai
projet important. (Je le rappelle : si il n'est pas important faite le
faire par d'autres au forfait si vous le souhaitez). Avec un projet de
ce type vous pourrez peut-être vous arrêter 2 mois avant la fin avec
plus de valeur sous le coude.

## Implication, encore

*- Je n'ai pas le temps de m'impliquer comme vous le souhaitez (a minima
30% de son temps au responsable du produit ou du projet).*

Je le rappelle à nouveau : si ce projet ou produit n'est pas important
faite le faire par d'autres au forfait si vous le souhaitez. Si c'est
important vous estimerez votre implication normale.

Voilà. A bientôt peut-être autour d'un **SmartCode**.

Je vous rappelle en écho cet article écrit précédemment : [sur la régie
et
l'agile](/2011/03/lagile-pourrait-il-redonner-a-la-regie-ses-lettres-de-noblesse/)

Première évocation officielle de SmartCode, 13 août 2012,
[ici](http://www.smartview.fr/sharepoint-2013-petits-dejeuners-sponsoring-agile-tour-smartcode/).
