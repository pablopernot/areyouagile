﻿---
date: 2020-09-24
slug: quiz-ecole-du-produit
title: "Les quiz de l'école du produit"
---

Ce mardi 22 septembre nous avons réussi ce qui me semble être un tour de force. La conférence *school of product ownership*[^1] a eu lieue, elle a été un succès, et l'innovation dans l'approche a sauté aux yeux de beaucoup. Parmi ces innovations liées aux contraintes sanitaires : une balade avec des quiz la matinée pendant deux heures et demie. L'idée paraît nouvelle pour beaucoup, elle ne l'est pas tant. Je l'avais proposé à [Claude](https://www.aubryconseil.com/) lors des [raid agile](https://raidagile.fr). Et je pense que cela a émergé doucement de nos balades avec conversations du temps de *agile open sud* (2010 ? 2011 ? 2012 ? ). En tous cas ce fut une réussite cette semaine, autant physiologique que psychique :)

[^1]: Que l'on appelle de plus en plus "école du produit". 

Plus d'articles, mots, sur cette journée : 


* [Article de Marion](https://medium.com/benextcompany/organiser-une-conf-c-est-comme-construire-un-produit-1b6cc253b0f0)
* [Article de Nils](https://medium.com/benextcompany/une-conf-product-pas-comme-les-autres-cb1e57af8b78)
* [Petit mot/photos de Souki](https://www.linkedin.com/posts/souki-khamsyvoravong-b39a4a90_si-on-mavait-dit-il-y-a-3-ans-de-cela-apr%C3%A8s-activity-6714457053194153984-e_fy)
* [Petit mot/photos de Dragos](https://www.linkedin.com/posts/dragosdreptate_schoolofpo-activity-6714453287988469760-FIxa)
* [Petit mot/photos de Marion](https://www.linkedin.com/posts/marionlecerf_productmanagement-schoolofpo-activity-6714484023839854592-3LaV)

{{< image src="/images/2020/09/balade.jpg" title="" class="center" >}}

## Les Quiz

Pour ceux qui me les demandent : voici les quiz. 16 questions, 12 questions, 8 questions (la durée des trajets entre les parcs devenait plus courte, et la dernière marche ne sera pas débriefée car nous arriverons à la cité fertile, donc les questions du quiz deviennent très ouvertes).  

Nous discuterons des réponses bientôt (j'ai fait les questions la semaine précédente de la conférence, peut-être un peu vite...). 

[Le quiz 1](/pdf/quiz-ecole-du-produit-1.pdf)

[Le quiz 2](/pdf/quiz-ecole-du-produit-2.pdf)

[Le quiz 3](/pdf/quiz-ecole-du-produit-3.pdf)

{{< br >}}

  
## Le mode opératoire 

Je vous transmets les notes pour tous les PO, UX, SM de benext qui ont accompagnés les groupes de 10 à 12 personnes. 

### Pendant la marche 

*Vous expliquez les règles : en duo ou en trio (ils ne changent pas normalement durant la marche, vous distribuez les questions en déchirant l’agrafe).  On lit une carte QUIZ et on en discute. Ils s’échangent les questions durant la marche. Sachant qu’aux différents lieux où l’on s’arrêtera on débriefera de chaque QUIZ avec tout le groupe. On se passe les cartes durant la marche pour essayer d’avoir discuté de toutes les cartes du quiz. Grosso modo 3 mn par carte.* 

### Aux parcs 

*Vous vous débrouillez pour faire un cercle (pensez distance), et 30 mn de debrief sur les questions. Vous pouvez distribuer à la fin le nouveau quiz et c’est reparti.*

## Remerciements particuliers

* à **Aurélie Rolland** qui a fait tout le *design* de l'école du produit, la *school of po*, cette année. 
* à **Marion Lecerf** qui a organisé et mené d'une main de fer la communication et toute la préparation du lieu (et qui a trouvé le lieu !).
* à **Souki Khamsyvoravong** qui a su donner du peps tout le long, ça compte, et qui a initié les parcours avec le trublion ci-dessous :  
* à **Maxime Lecoq** qui a concocté les bons parcours, et qui a été là quand il le fallait.
* à **Dragos Dreptate** qui sait parler à l'oreille des grands speakers internationaux.  

et bonus pour **Yoann Galand** qui assure son et vidéo le jour J. 

{{< image src="/images/2020/09/equipe.jpg" title="" class="center" >}}
