---
date: 2011-02-14T00:00:00Z
slug: carrieres-dans-un-monde-ou-une-entreprise-agile
title: Carrières dans un monde ou une entreprise agile
---

Ci-joint le résultat d'une petite réflexion sur les carrières dans une
entreprise ou un monde agile. A la remarque : "*ok agile d'accord MAIS
je suis chef de projet, j'ai été développeur 1, puis développeur 2, et
je désirai devenir directeur de projets puis manager de BU... et
naturellement ma rémunération est associée à mon poste...*", je n'ai pas
de réponse miracle. La première chose serait -en France- de beaucoup
mieux apprécier à leur juste valeur les cursus purement techniques, et
ne pas considérer qu'une personne de 45 ou 50 ans souhaitant rester
développeur est une abhération ! Un grand expert technique est une
ressource inestimable qui se paye à sa juste valeur. Vouloir devenir un
guru technique n'est pas une maladie mentale.

Ceci fait, l'agile et l'interaction ou la collaboration qu'il induit
entre techniques et métiers permettrait aussi d'ouvrir plus des
carrières fonctionnelles a des personnes initialement technique.

{{< image src="/images/2011/02/careers_in_an_agile_world.jpg" title="Rendre Heureux" >}}

