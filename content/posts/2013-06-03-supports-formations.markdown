---
date: 2013-06-03T00:00:00Z
slug: liberation-de-supports-de-formations
title: Libération de supports de formations
---

Quelques autres libérations de petits supports de formations (sous un
format demi-journées) : Introduction à Kanban, Optimiser la valeur
(Value Stream Mapping), Vision & Sens, Expression du besoin.

Voici quelques pdfs qui me permettent d'organiser des demi-journées
d'accompagnement. Les supports présentés ici sont assez "light". Ils ne
représentent qu'un outil pour accompagner un discours que j'espère plus
cossu. C'est la face émergée de l'iceberg en quelque sorte. Si cependant
ils peuvent être utiles et surtout vous pousser à me joindre et à
travailler avec moi j'aurais tout à y gagner.

J'encourage d'ailleurs mes confrères à faire de même. Cela me parait
totalement en adéquation avec la pensée agile.

-   [Vision & Sens](/pdf/vision-sens-1-1.pdf), 1.9m, pdf
-   [Expression du besoin](/pdf/expression-du-besoin-1-1.pdf), 956ko,
    pdf
-   [Introduction à Kanban](/pdf/kanban-1-3.pdf) , 2.2m, pdf
-   [Optimiser la valeur](/pdf/optimiser-1-1.pdf) (Value Stream
    Mapping), 683ko, pdf

Tous les supports sont sous licence :

This work is licensed under a [Creative Commons Attribution-ShareAlike
3.0 Unported
License](http://creativecommons.org/licenses/by-sa/3.0/deed.en_US).

N'oubliez pas aussi la libération de cet autre support de formation (3
jours) : [formation agile](/2013/02/formation-agile/).
