---
date: 2011-06-23T00:00:00Z
slug: sudweb-2011-premiere-edition
title: SudWeb 2011, première édition
---

Voilà, [SudWeb](http://sudweb.fr) 2011, J'y étais.

{{< image src="/images/2011/06/macaron_mini.jpg" title="SudWeb 2011" >}}

Pour moi l'histoire commence jeudi 26 mai au soir, dans une
chambre d’hôtel en face des arènes (la vue et la ville sont superbes,
mais j'y suis habitué). C'est le dîner de réception des orateurs et des
organisateurs. Tout le monde apprend à se connaître. Bonne ambiance,
pizza & bières (et [1 décapsuleur que je vous recommande
vivement](http://www.amazon.co.uk/dp/B0015RYLM0/ref=asc_df_B0015RYLM03318307?smid=A3P5ROKL5A1OLE&tag=googlecouk06-21&linkCode=asn&creative=22206&creativeASIN=B0015RYLM0)
). Mais je suis crevé de ma journée, l'un des vieux et donc je me retire
rapidement. D'autant que j'habite à une trentaine de kilomètres, donc,
pas question de passer la soirée -avancée- à l'hôtel.

[SudWeb](http://sudweb.fr) a été une belle manifestation, avec une
organisation impeccable, bien supérieure aux standards habituels. Tout a
été prévu, organisé, et s'est déroulé aux petits oignons, sans nuire à
l'aspect convivial.

Non mais vraiment tout. Même les petits détails qui participent aux
grandes réussites (goodies, les gommettes, les stickers, la qualité de
la nourriture -[Prouheze](http://www.prouheze-saveurs.com/) !-,
traduction anglais français à la volée par rudy -red bull- rigot, etc.).
Difficile de nommer et de féliciter tous les organisateurs donc, mais
chapeau à vous tous. Et aussi à tous les photographes présents (dingue !
c'était Cannes à Nîmes), à qui je pique sans vergogne toutes ces photos
(vous les trouverez toutes là :
[flickr](http://www.flickr.com/groups/sudweb2011/)).

Donc j'y participe pour animer une session qui se veut un retour
d'expérience sur l'agilité. En quelques mots (j'espère pouvoir redonner
cette session, c'était sa première sortie) : je ne souhaitais pas
expliquer comment on est agile. Il me semble que tout le monde trouve
cela assez facilement sur le web (même si je crois avoir laissé du monde
sur la route... [cf le commentaire sur le post de
Brice](http://pelmel.org/dotclear.php/post/2011/05/30/Retour-sur-SudWeb-2011-ShareTheLove)
ou sur la vidéo à venir la question de Corinne -et ses yeux bleus-). Je
souhaitais un retour d'expérience orienté décideurs, ou si vous voulez
une sensibilisation des décideurs aux risques, contraintes, enjeux et
ROI d'un déploiement agile. Vous trouverez [les slides de cette session
là](http://www.slideshare.net/pablopernot/anatomie-dune-mission-agile)
(ok !?). Et bientôt l'enregistrement live, le site
[Sudweb](http://sudweb.fr) l'indiquera (ok !?). Là j'ai plus
d'inquiétude. Même si les feedback ont été bons, je me suis trouvé assez
brouillon (notamment au début) et pas forcément toujours dans le
rythme (ok !?). Et comme me l'a si bien fait remarqué Franck je n'ai
cessé de *okéiser*(ok !?).

*Homo egocentricus* dans l'âme merci encore à tous les photographes pour
ces photos qui me permettent d'intenses sessions d'auto-flagorneries.

{{< image src="/images/2011/06/marc.jpg" title="Marc Lipskier" >}}

A ce sujet je me vois mal me lancer dans une description de toutes les
sessions car je ne les ai pas toutes suivies (j'ai été embarqué dans pas
mal de discussions off, et [Brice l'a fait
ici](http://pelmel.org/dotclear.php/post/2011/05/30/Retour-sur-SudWeb-2011-ShareTheLove)).
Mais cependant eu le plaisir de voir la session de Olivier Mansour
concernant l'achat de prestation web, et je fus soulagé de voir que nous
allions dans le même sens dans la défense de la régie (là aussi, le
contrat agile n'a pas été évoqué dans ma présentation, je fuis cette
arlésienne). J'ai apprécié le rythme de la journée et la façon dont elle
a été pensée : avec son approche du global vers le concret,  et ses
sessions de moins en moins longues jusqu'aux lightning talks. A ce sujet
très bien l'approche "langue" de Nicolas Dubois (toutes les sessions
m'ont convaincues, je reviens sur celles qui m'interpellent plus). La
journée s'est achevée avec la session de Marc Lipskier, qui au delà du
fond, intéressant, a été une expérience dans la forme. En effet le
physique, la voix, le rythme de Marc ne semblent pas provenir de la même
personne. Cela opère une distance et donne donc une profondeur
supplémentaire à son discours. Bel exploit, surtout quand on est la
session de clôture (Au passage Marc m'a expliqué comment on se tenait
vraiment dans un scrum -celui du rugby- cela reste entre nous Marc).

En fait la journée s'est réellement achevée au Zinc à déguster des tapas
et à parler de tout et de rien avec chacun. Un moment très agréable à
nouveau.

Merci encore aux organisateurs.

Une réussite, à vous d'y être l'année prochaine.

{{< image src="/images/2011/06/public_sudweb.jpg" title="Sudweb" >}}

Il y a ici des photos de Nicolas, Brice, Olivier, Adrien et d'autres.
Merci pour eux, allez voir les originales sur
[flickr](http://www.flickr.com/groups/sudweb2011/).


