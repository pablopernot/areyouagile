---
date: 2021-09-23
slug: cultiver-imperfection 
title: "Cultiver l'imperfection"
--- 

Nous devrions espérer l'effondrement de certaines grandes entreprises. 

Si l'on repense à l'espérance de vie des entreprises, qui a drastiquement réduit depuis un siècle, on reste encore sur notre faim en regardant autour de nous. 
Car on attend encore que les vieilles grandes institutions s'effondrent. Est-ce leurs liaisons avec l'État en fait des interlocuteurs pour l'instant résistants ?

Pourquoi souhaite-t-on leurs effondrements ? 

## Négativisme et perfection

Car elles se verraient remplacées par des entreprises modernes (comme les nombreuses qui heureusement pullulent aujourd'hui autour) et 
les aberrations administratives, les aberrations liées à la politique interne, le blocage sur la modernité de tout un pan de notre vie serait levé. 
D'autant plus que beaucoup de gens qui les habitent souffrent. La preuve en est la difficulté de ces grandes et vieilles organisations à recruter aujourd'hui. 
Seuls le pouvoir et l'argent (qu'il s'agisse de flamber ou de se sécuriser), la carrière, animent une bonne partie de ces personnes, l'autre partie s'étant résignée à profiter des avantages de cette fossilisation avancée, quitte à passer à côté de leurs vies. Souvent un système a été créé et toutes ces personnes sont enfermées dedans.
Je ne me lance pas plus loin dans le débat, car plus on avance plus il se complexifie et plus les avis tranchés sont inadéquats. 

Je souhaite le bien de toutes ces personnes individuellement. Le système de ces grandes et vieilles institutions les broie, leur fait du mal, tout comme à nous. 

C'est lui dont j'espère la chute.    

Je suis toujours optimiste, même si Jeff Patton vient de me rappeler un douloureux débat en évoquant dans la school of product 2021 que le changement 
serait annoncé en quelque sorte dans la rubrique nécrologique (expression piquée à Dragos). Il faisait référence à une expression anglo-saxonne qui dit : "le changement suit le corbillard", et qui en français était devenue "le coffre-fort ne suit pas le corbillard". Ainsi en français le sens est : après le décès de la personne, l'argent redevient accessible. En anglais : après le décès de la personne, le changement redevient possible. 

Dur constat que de se dire qu'il faut dans bien des cas attendre : soit la fin, au sens propre, de certaines organisations, soit de leurs dirigeants.

Pas le temps d'avoir une pensée positive, car une autre personne me glisse aussitôt : "bof, chez les jeunes je vois aussi plein de personnes qui ont pris le mauvais pli du management à l'ancienne (celui du siècle dernier, industriel), et de la politique (dans le sens de l'intérêt personnel avant celui du groupe)" (je paraphrase). 

Dans ces grandes institutions, pour préserver l'ordre établit, le pouvoir et l'argent, on norme, on sécurise, on cache (avec un millefeuille organisationnel ou une jungle de processus), cela devient un enfer. L'objectif n'est pas de faire avancer les choses, mais de réussir en interne, ou de se sécuriser pour s'assurer que rien ne change. 

Pour cela **dans ces entreprises on exècre l'incertitude et l'insécurité**. Et cela nourrit un cercle vicieux. Cela nourrit une volonté illusoire de perfection.  

Dans cette recherche d'absolutisme, de perfection (qui garantirait que rien ne change, que rien n'est mis en danger), deux exemples : 

- L'envie de ces personnes dans ces organisations d'avoir tous les cas de figure et toutes les réponses avant de se lancer. On prépare tout. Or vous savez déjà que tout préparer, répondre à tout n'a pas de sens dans notre monde incertain.  

- Le désir de fabriquer des équipes dont l'ensemble des compétences est prédisposé à répondre parfaitement au besoin, mais dont le fonctionnement est devenu impossible en raison du trop grand nombre de personnes. 

## Optimisme et imperfection

Pour voir la face optimiste et positive des choses : oui il y a de nombreuses organisations qui sont florissantes, modernes, et dans lesquelles il fait bon travailler. 

**Leurs dirigeants savent que l'imperfection est un composant essentiel de leur réussite**.  

Dans ce monde dit "agile" c'est connu, c'est décrit, et c'est "équipé", cette façon d'appréhender l'imperfection, cette réalité d'un monde qui demande une adaptation beaucoup plus soutenue qu'auparavant. 

Aujourd'hui deux façons de faire (deux premiers pas ?) pour appréhender cette imperfection qui me paraissent importantes (en contrepoint des deux exemples plus hauts): 

- Les *real options* : savoir laisser des questions sans réponse, ne pas s'astreindre à avoir des réponses à tout. 

- Prioriser le bon fonctionnement des équipes, avant de s'interroger à leur capacité de réponse. En d'autres termes, savoir s'astreindre à constituer d'abord les équipes de façon à ce que la dynamique de groupe fonctionne bien, quitte à avoir des équipes pas totalement adaptées au besoin. 

Deux efforts importants, mais gratifiants.  

### Real Options

Les *real options* (options réelles) pour les prises de décisions. Je pense qu'une très bonne source en la matière est [Chris Matts](https://twitter.com/PapaChrisMatts), dont voici [un des articles](https://www.infoq.com/articles/real-options-enhance-agility/) (en anglais). 

Je me permets d'en traduire quelques extraits ici (enfin je demande à [deepl](https://www.deepl.com)) : 

*Pour toute décision à prendre, il existe trois catégories de décisions possibles, à savoir une "bonne décision", une "mauvaise décision" et "aucune décision". La plupart des gens pensent qu'il n'y en a que deux : soit vous avez raison, soit vous avez tort. Comme nous ne savons normalement pas quelle est la bonne ou la mauvaise décision, la décision optimale est en fait "aucune décision", car elle reporte l'engagement jusqu'à ce que nous disposions de plus d'informations pour prendre une décision plus éclairée.*

*Cependant, si nous observons le comportement de la plupart des gens, nous constatons qu'une aversion à l'incertitude fait que les gens prennent des décisions tôt. Les options réelles répondent à cette aversion pour l'incertitude en définissant la date exacte ou les conditions à remplir avant de prendre la décision. Au lieu de dire "pas encore", l'approche des options réelles dit "Prenez la décision quand.....". Cela donne aux gens une certitude quant au moment où la décision sera prise et, par conséquent, ils sont plus à l'aise pour retarder la décision. Le fait de retarder les engagements donne aux décideurs une plus grande flexibilité, car ils continuent à avoir des options. Il leur permet de gérer le risque/l'incertitude en utilisant leurs options.*

C'est le "décider le plus tard possible" du Lean, "au dernier moment responsable". 

### Privilégier le bon fonctionnement des équipes avant le scope fonctionnel ou technique  

Vous voulez répondre au besoin de votre sujet. Vous constituez une équipe "sur-mesure". Vous êtes 10. Elle ne fonctionne pas. Tout tombe à l'eau. 

Vous vous astreignez à respecter les lois de la dynamique de groupe, et ne pas avoir d'équipe constituée de plus de 6 ou 7 personnes, voire 8 exceptionnellement. Vous êtes un peu en risque, car l'équipe ne couvre pas complètement le périmètre de votre sujet. Mais la dynamique d'équipe fonctionne bien. Votre équipe va pouvoir délivrer des choses.  

C'est difficile cette astreinte, mais il faut savoir se faire violence. 


## Cultiver l'imperfection 

C'est en cultivant une certaine forme d'imperfection, une imperfection en quelque sorte acceptée, raisonnée, et 
donc contrôlée que nous réussissons le mieux aujourd'hui : réponse repoussée à plus tard pour répondre mieux, équipe incomplète, mais qui fonctionne bien par elle-même, etc. 


### Mes petits principes de management

* [L'intention première](/2021/06/intention-premiere/)
* [Vers le simple, vers le complexe](/2021/07/simple-complexe/)
* [Ma cible est mon point de départ : appréhender l'émergence](/2021/07/point-de-depart/)
* [Cultiver l'imperfection](/2021/09/cultiver-imperfection/)





