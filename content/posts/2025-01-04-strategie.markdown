---
date: 2025-01-04
slug: retour-sur-la-strategie-du-product-owner
title: "Stratégie du product owner, manager, leader, *er"
draft: false
---


Il y a 13 ans, en novembre 2012, je relatais une conversation (qui était aussi une conférence) que j'avais eue avec [Alexis Beuve de chez Praxeo](http://praxeo-fr.blogspot.com/) (Alexis que j'avais connu chez un client que j'ai beaucoup accompagné à l'époque). Cette conversation était sur la **stratégie du product owner** (à l'époque tous les titres *product owner, product manager, head of product, chief product officer, product leader* n'avaient pas encore vu le jour). 

Alexis est un spécialiste des jeux de stratégies (go, échecs) entre autres, et aussi un éditeur et auteur. Nous avions réfléchi à l'application de la stratégie de ces joueurs transposée à une réflexion pour les *product \*er*. Je l'ai trouvé intéressante, et je trouve qu'elle est tombée trop vite dans un certain oubli, c'est pourquoi je réécris dessus aujourd'hui (je n'ai pas vu [Alexis](https://www.linkedin.com/in/alexis-beuve-60338b/) depuis dix bonnes années et je le salue à cette occasion). 

D'abord si vous voulez attaquer la montagne par son côté le plus ardu (mais probablement le plus enrichissant) vous pouvez vous plonger dans toute la série d'articles que Alexis avait consacrés au sujet ici : [Stratégies](http://praxeo-fr.blogspot.com/2012/12/strategies-le-sommaire.html). 

## La stratégie ? 

La stratégie c'est une question de management, c'est une question de prise de décision, c'est une question de prise de risque, et c'est l'acceptation de l'incertitude. La question que nous nous posons est : comment les champions prennent les décisions ? On parle ici des champions d'échecs et de go par exemple. 

## Une stratégie est adaptative

Dans ces jeux on observe que toute stratégie est adaptative. Toute stratégie est une réaction à une situation. On n'a pas une stratégie *par avance*. On ne calcule pas une stratégie *par avance* contrairement à ce que l'on croit. On a une vision d'une direction, d'une volonté, on a une stratégie qui s'adapte à la situation, et une tactique de mise en œuvre. 

Les gens imaginent d'ailleurs que les joueurs d'échecs calculent des dizaines voir des centaines de coups en avance. À cela Alexis répondait en citant [Richard Retti](https://fr.wikipedia.org/wiki/Richard_R%C3%A9ti), un champion hongrois d'échecs du XIXe siècle, l'un des meilleurs de 1910 à 1920, qui disait : *"les amoureux des échecs qui me demandent combien de coups je calcule à l'avance quand je fais une combinaison sont souvent très surpris quand je réponds en toute franchise : ma règle est de ne jamais calculer au-delà d'un coup"*.   

En recherchant cette citation je suis tombé par hasard sur une autre qui lui fait écho.  C'est [Samuel Reshevsky](https://fr.wikipedia.org/wiki/Samuel_Reshevsky), un autre grand maître entre 1930 et 1970, qui dit *"Les jeunes joueurs calculent tout en raison de leur inexpérience"*.  

Si j'en reviens à nos *product \*ers*, mais je pense qu'ils le savent déjà, la posture est d'accepter l'incertitude.  

## Les 5 étapes de Garry Kasparov 

Alors comment se traite cette incertitude ? Comment se traite cette application de la stratégie ? Par la prise décision.  Alexis évoque cinq étapes dans le raisonnement de [Garry Kasparov](https://fr.wikipedia.org/wiki/Garry_Kasparov) (dont normalement vous avez tous entendu parler) qui mène à la prise d'une décision stratégique.  

Ces cinq étapes sont : 

* Identification du moment clef
* Évaluation de la position
* Analyse conjoncturelle
* Scénarios d'exposition aux risques
* Décision

### Identification du moment clef 

C'est un moment clef : une bascule, une orientation, sans retour possible. C'est une décision sur laquelle on ne pourra pas revenir. Tout cela émane d'un ressenti, pas d'un calcul. On sait que l'on est dans un moment clef, on ne le calcule pas. 

### Évaluation de la position

Là-dessus, on reprend les dimensions utilisées par le domaine militaire : dimension matérielle, dimension spatiale, dimension temporelle. 

Nous pourrions dire concernant le *product \*er* : La dimension matérielle concerne notre produit, qu'est-ce que l'on a déjà ? Quelles technologies on utilise ? Quel est l'état de la dette technique ? Etc. 
La dimension spatiale : où nous trouvons-nous sur le marché vis-à-vis de la concurrence ? 
La dimension  temporelle : sommes-nous en retard ou en avance ? 

Là c'est bien calculatoire comme dirait Alexis, et non pas basé sur un ressenti.

### Analyse conjoncturelle : 

Dans quel système sommes-nous : nous allons y venir plus bas c'est un point particulier. 

### Scénarios d'exposition aux risques

En fonction des points précédents quels sont les scénarios d'exposition au risque qui s'offrent à nous. 
Il y a incertitude, il y a risque. Si on savait déjà par avance, on jouerait notre coup et puis voilà. Mais non ce n'est pas le cas, les joueurs d'échecs parlent de "brouillard". 

### Décision

Quand on a vu tous les points précédents, on décide. C'est un acte franc et définitif. 

## Retour à l'analyse conjoncturelle

Revenons à notre analyse conjoncturelle. Alexis, à la tête de sa maison d'édition spécialisée, reçois des manuscrits des champions que l'on évoque. À un moment il en reçoit deux simultanément, l'un d'un grand champion de go, l'autre d'un grand champion d'échecs, qui tiennent des discours complètement opposés.

Au go, le champion explique qu'en retard (évaluation de la position), il attaque, il augmente les risques. Aux échecs, le champion explique qu'en retard il consolide. Entre les deux c'est une inversion totale ! Pourquoi ? 

Il y a une différence fondamentale entre le go et les échecs. Le go est un système de comptage : on compte les points amassés (ou pas). Les échecs sont un système de mort subite : on gagne quand le roi est pris, peu importe l'état du jeu par ailleurs. Ceci explique l'inversion dans leur réponse. Mais alors dans l'organisation avec les *product \*ers* ? 

Alexis fait le lien avec la trésorerie de l'entreprise. Si la trésorerie n'est pas en risque, on est dans un système de comptage, si la trésorerie est en risque et peut soudainement sonner le glas de l'entreprise, on est dans un système de mort subite. Je pense que cela peut être rattaché à d'autres choses que la trésorerie. Notamment plus individuellement votre carrière : si dans les risques associés à la fabrication de votre produit vous écornez votre carrière sans la mettre en danger véritable que vous êtes dans un système de comptage, si vous mettez en jeu votre carrière vous êtes dans un système de mort subite. Pour revenir à l'idée de trésorerie, j'utiliserai le terme "encaisser", si le risque peut être encaissé vous êtes en système de comptage, si le risque ne peut pas être encaissé, vous êtes en système de mort subite. 

### Stratégie associée à une position de retard

Et ainsi si vous êtes en système de comptage (vous pouvez encaisser le risque), et que vous êtes en retard, comme le joueur de go, il est conseillé d'attaquer. Une grande entreprise qui est en retard et qui peut encaisser le risque devrait ainsi attaquer. Attaquer cela voudrait dire quoi pour un *product \*er* ? Essayer de nouvelles façons d'imaginer son produit ?  Essayer un nouveau public ?  Essayer un nouveau partenariat ? Essayer une nouvelle image ? Essayer un nouveau type de ... ? On augmente les risques.  

À l'inverse si vous êtes dans un système de mort subite (l'organisation ne peut pas encaisser le risque), comme le joueur d'échecs, votre priorité c'est l'évitement de la mort subite. Vous consolidez, vous sécurisez. Pour un *product \*er* vous vous assurez de délivrer les éléments les plus attendus, les plus fondateurs, de délivrer un socle, l'essentiel. On essaye de diminuer les risques. 

## Système mixte

Il y a bien souvent un système mixte qui démarre avec un système de comptage (on peut encaisser le coût), et qui finit par un système de mort subite (on ne peut pas encaisser de coût). Entre les deux, une zone d'équilibre. 

{{< image src="/images/2025/01/zone-equilibre.jpg" class="center" alt="Courbe entre système de comptage et système de mort subite" width="600">}}

Un système mixte c'est un système qui démarre par un système de comptage (on augmente le risque), qui finit par un système de mort subite (on limite au maximum le risque), et qui passe par une zone d'équilibre. Imaginez une manche de ping-pong, en 11 points dorénavant, au début vous êtes en système de comptage, pas de drame à perdre des points, ainsi vous pouvez prendre des risques, mais à un moment on arrive à une zone d'équilibre puis on bascule en mort subite, si vous êtes en retard, à tout moment vous pouvez définitivement perdre. Plus question de s'ouvrir aux risques. 

Pour un *product \*er* au début de son produit, au début de son incrément, au début de la nouvelle phase autour du produit, il est probablement en système de comptage, puis vient une bascule (zone d'équilibre) qui le fait passer en système de mort subite, il faut maintenant assurer des choses, sécuriser.

### Valeur, impact et risque

Mais pourquoi prendre des risques ? Parce qu’à chaque risque est associée une augmentation de l'impact, de la valeur. C'est la ligne au milieu de notre schéma plus haut. Elle représente l'augmentation de chance de gain (valeur, impact) en augmentant le risque. 

Le bon *product \*er* saura donc maximiser cette capacité à augmenter ses chances de gain en usant de la bonne stratégie au bon moment. 

### Comment détecter la bascule ? 

Est-ce que c'est assez simple de savoir si on est encore en système de comptage ou si on est passé en système de mort subite ? À l'époque nous proposions (je n'ai pas changé d'avis) de se décaler un peu dans le temps pour percevoir où l'on se situe. On fait varier un paramètre pour mieux ressentir où on se trouve vis-à-vis de l'équilibre. Par exemple je travaille sur un produit et j'hésite à me lancer dans une nouvelle démarche sur un nouvel usage. Si je suis en système de comptage, autant le faire. Mais le suis-je encore ? Est-ce que l'évènement E associé au produit n'est pas trop proche ? Pour le ressentir je me projette dans le temps, j'essaye ce nouvel usage (je prends ce risque), et finalement cela ne marche pas, est-ce que je me retrouve dans une situation convenable (pas de mort subite) ? Oui ? Je suis encore en système de comptage (je peux augmenter mes risques). Non ! Je suis en fait déjà passé en système de mort subite (je dois limiter mes risques). Je ne sais pas ! Je suis en plein dans la zone d'équilibre, et là, mauvaise nouvelle, il n'y a pas de réponse. 


## Agilité et gestion des risques

À mes yeux l'agilité se défend comme un système de gestion des risques. Par exemple tel que je le détaille dans [La disparition](/2024/07/la-disparition-le-texte/). Dans ce cadre aussi -- la stratégie -- l'agilité fait ses preuves. Son approche bout en bout, droit au but, maximisation de la valeur en minimisant l'effort, son travail sur l'émergence, est une façon d'optimiser notre capacité à prendre des risques (en les atténuant). Les risques seront beaucoup moins douloureux et on peut en prendre beaucoup plus. On augmente ainsi notre chance de gain (valeur, impact). L'agilité maximise le système de comptage, et repousse le système de mort subite. 

## Tempérament

Alexis finissait notre conversation en évoquant le tempérament nécessaire au *product \*er*. Du tempérament, il en faut pour s'ouvrir aux risques, allez le plus loin possible, sans aller trop loin (tout ceci est aussi très proche des *real options*, et du *dernier moment responsable* du lean). 

Aujourd'hui j'associerai cette notion de tempérament avec le [Skin in the game](https://en.wikipedia.org/wiki/Skin_in_the_Game_(book)) de Nassim Nicolas Taleb. On va ressentir le passage de comptage à mort subite, on va avoir le tempérament d'avancer la prise de risque jusqu'au dernier moment responsable si et seulement si on est engagé, si et seulement on joue sa peau, sinon la mort subite n'a pas de sens, et on restera dans le brouillard. 
















