---
date: 2010-08-22T00:00:00Z
slug: fiche-de-lecture-karmic-management-roachmcnallygordon
title: 'Fiche de lecture : Karmic Management, Roach/McNally/Gordon'
---

Autre lecture cet été, **Karmic Management**, de Roach, McNally et
Gordon. 150 pages qui se lisent en 1 ou 2 heures chez le coiffeur ou sur
la plage, le soir dans le lit.

{{< image src="/images/2010/08/karmic-management-by-geshe-michael-roach.jpg" title="Karmic Management" >}}

Bon allons-y avec des pincettes. Il s'agit d'appliquer des préceptes
bouddhistes au management. Gasp. re-gasp. Si j'ai lu ce bouquin c'est
que l'un de mes associés me l'a conseillé. Il retrouve en partie dans
ces préceptes notre façon de faire. Je ne peux pas le contre-dire. Si
lui-même a lu ce livre c'est que l'un de nos clients lui a conseillé :
"vous faites du Karmic Management !". Avant de se sentir honoré il
fallait jeter un œil à cette chose.

Si j'exclus les côtés mystiques mais rigolos, notamment les sagesses
antiques qui ouvrent les chapitres :  "Pas cela, ni cela non plus", "la
ligne qui nous sépare est artificielle", "tes problèmes sont ta voie",
"aucune action n'est jamais perdue" ; ou les aspects mercantiles qui
achèvent le livre (tel séminaire, tel livre, tel session, voire une
hot-line karmic, etc.), j'y trouve des choses intéressantes. Notamment
l'idée forte du Karmic Management qui veut que toutes les réussites que
vous allez pouvoir promouvoir, générer, réaliser autour de vous
augmenteront votre propre réussite.

{{< image src="/images/2010/08/karmic-300x226.jpg" title="Karmic Management" >}}

Je ne sais pas si cela "marche". Je sais que je fonctionne ainsi et que
j'ai souvent pu avoir la bonne surprise de voir des succès découler de
ces engagements. Autre idée formulée de façon intéressante : les
problèmes sont vos amis. Car ils mettent en évidence les obstacles qui
vous empêcheront de réussir vos projets. Il faut donc les embrasser
pleinement et dès qu'ils se présentent. Ce point est à rapprocher de
l'introduction de Scrum dans une organisation : c'est un révélateur des
problèmes sous-jacents de l'entreprise (j'essaye d'être assez agile pour
retomber sur mes pieds... ).

[amazon](http://www.amazon.com/Karmic-Management-Around-Comes-Business/dp/0385528744)
?

