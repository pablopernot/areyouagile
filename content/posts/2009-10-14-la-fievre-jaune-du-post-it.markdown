---
date: 2009-10-14T00:00:00Z
slug: la-fievre-jaune-du-post-it
title: La fièvre (jaune) du post it
---

Une des phrases qui m'avait le plus intriguée à la première lecture du
célèbre [Scrum & Xp From the
Trenches](http://www.infoq.com/minibooks/scrum-xp-from-the-trenches)
était celle concernant les fichiers excel : oubliez, arrêtez de les
employer, ça pue. Personne ne les utilise, personne ne les lit.
Appartenant -toujours- en ce moment à une société avec une grosse
culture *CMMi*, lire ça c'est un peu comme lire un roman érotique au
milieu d'une abbaye, une révélation autant qu'un parjure.

{{< image src="/images/2009/postit.jpg" title="Post It F(or)ever" >}}

Mais les faits sont là. Les *radiateurs* (gros tableaux faussement
bordéliquessur lequels on déploie les postits) Scrum possèdent d'énormes
bénéfices. **Visibilité** : au sein de l'équipe, mais aussi beaucoup
auprès des autres : sur quoi l'équipe travaille, à quel rythme, qu'est
ce qu'elle a déjà réalisé, etc.**Usabilité** : C'est étonnamment
pratique ce petit bout de papier jaune : facile à accrocher, facile à
déplacer, à la bonne taille pour nous obliger à aller à l'essentiel dans
nos descriptions.Si on écrit trop dessus, c'est le signe qu'il en faut
plusieurs et pas un. Si on a des agrégats trop importants c'est le signe
qu'il faut les déployer, disperser, repenser. Autre instrument essentiel
: l'appareil photo. Il permet d'*historiser* le projet.  Je me rappelle
un *top level high board manager* m'ayant glissé : c'est bien tout cela
mais a) tu me fais des trous dans le mur et ça va nous coûter cher en
cloison si on déménage (je suis passé au scotch), b) comment je fais
pour accéder à l'historique de la consommation de charge du projet ?
(d'où les photos).

Le post-it n'est d'ailleurs vraiment pas l'apanage des projets Agiles.
Ses propriétés (concis, agrégation visible, souplesse d'utilisation)
sont exploitées dans de nombreux cas. Les Web agencies par exemple... et
le [tri sur
cartes](http://www.ergolab.net/articles/tri-cartes-ergonomie-web.php).
Même causes, même effets : on découpe une arborescence, un rubriquage
par fonction, par contenu, on opère des regroupements logiques. Les
gros" pavés", les déséquilibres émergent. On organise aisément,
visuellement, les ensembles, les sous-ensembles. L'équilibre est visuel.

Voilà pour ce petit post (it). J'y retrouve aussi un pied de nez très
plaisant à tout cet outillage dispendieux et pléthorique derrière lequel
on cherche trop souvent à se cacher. Mais surtout, surtout, ça marche.
c'est **efficace**. c'est **productif**.
