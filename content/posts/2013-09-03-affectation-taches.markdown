---
date: 2013-09-03T00:00:00Z
slug: affectation-des-taches-dans-un-projet-scrum
title: Affectation des tâches dans un projet scrum
---

Rapidement, une bafouille sur l'affectation des tâches dans une équipe
**Scrum**. J'observe trop souvent des tâches avec des **noms dessus**.
C'est une mauvaise pratique (dans ma compréhension et proposition
concernant Scrum). Une équipe de dev Scrum (ne pas limiter "dev" à
production de code, mais à développeur de tout ce qui est utile pour
parvenir à "fini"), donc une équipe de dev Scrum est **auto-organisée**.
Il faut l'appréhender comme un système global, comme un collectif. Et de
fait ne pas affecter, assigner les tâches à une personne. L'équipe est
auto-organisée car c'est ainsi que l'on obtiendra de sa part la
meilleure performance.

L'assignation existe, mais elle doit rester tacite, au sein de l'équipe.
L'équipe sait qui prend quoi, mais elle ne l'affiche pas.

## Pourquoi garder l'assignaton de façon tacite, sous-entendue et non pas explicite ?

Car je ne souhaite entendre en tant qu'élément externe à l'équipe :
"nous n'avons pas abouti sur cette *user story* parce que X ou Y n'a pas
achevée sa tâche". Sous-entendu : moi j'ai fait mon boulot, pas mon
équipier. La réponse adéquate à mes yeux -et c'est très important- c'est
"vous êtes une équipe, je ne comprends pas un discours autre que
collectif ; l'équipe n'a pas réussie (pour de bonnes ou mauvaises
raisons) d'achever la user story". (Rappelez vous [les stades
forming,norming,etc..](/2010/12/management-agile-le-modele-tannenbaum-schmidt/)
des équipes).

**Postulat** : l'équipe s'est engagée ou a eu l'intention en tant que
**collectif** de travailler sur ces *user stories*. Il n'est pas
compréhensible que ce collectif se déchire plus tard.

**Effet** : l'équipe doit apprendre à travailler ensemble. L'équipe doit
comprendre qu'elle est auto-organisée. Cela brise les silos invisibles
qui pouvaient garder chacun dans son pré carré, et va libérer cette
dynamique d'associations inattendues, d'engagement, qui va porter
innovation et productivité.

## D'ailleurs, à quoi sert-il de mettre des noms sur des post-its de tâches ?

Si ce n'est pour blâmer ?

Quelle autre raison justifie-t-il cela ? Je n'en vois pas. Donc il faut
donc drastiquement l'éviter.

Si vous avez une raison valable envoyez-la moi, je la publierai, mais
attention j'en ai entendu beaucoup, aucune n'a été justifiée à ce jour.

## Quelques craintes infondées

-   On ne saura pas qui travaille sur quoi ?

C'est faux on sait pertinent quels travaux sont en cours, et c'est bien
cela l'essentiel. Si on souhaite savoir qui travaille sur quoi pour de
bonnes raisons il suffit d'assister au *daily scrum* ou de demander à
l'équipe, tout simplement. Au sein de l'équipe, pas de souci, tout le
monde sait qui bosse sur quoi.

-   On ne va pas archiver, compter le temps passé ?

Le cumul du temps si on vous le demande se déroule habituellement dans
un système parallèle. J'ai passé X% de mon temps sur le projet Y.
Vouloir suivre très finement le temps passé sur chaque tâche est aussi
illusoire (si ce n'est encore pour blâmer) et aussi une vraie perte de
temps. Ne venez pas m'expliquer que c'est pour avoir une réponse toute
faite si la situation se reproduit. Depuis que j'entends cet argument je
ne l'ai jamais vraiment croisé, c'est le Dahu. Les contextes changent,
un contexte totalement identique c'est une illusion. Et pire, sachant
que son activité est tracée à la tâche le comportement de l'équipier [va
changer pour respecter cette traçabilité et non plus le bienfait du
projet](/2011/04/la-loi-de-parkinson-par-asterix-obelix/), de l'équipe.
A proscrire donc !

-   On ne vas pas savoir s'organiser ?

Les tableaux physique Scrum de management visuel remplissent très
efficacement ce rôle : on connait les tâches en cours, à faire, finies,
les *daily scrum* journaliers parachèvent le partage de cette
information.

-   Les équipes dispersées sur plusieurs lieux ?

Parfait cela va les pousser à plus communiquer.

## Cela ne veut pas dire

-   Que les individualités sont gommées dans l'équipe. Le système équipe
    est une ensemble d'individualités, avec des noms différents, des
    salaires différents, des compétences différentes, des intitulés de
    poste différents, etc. Nous ne gommons pas les individualités, mais
    l'équipe est un collectif. Comme une équipe de sport, elle gagne ou
    elle perd en tant qu'équipe (qui est pourtant composée
    d'individualités). Il n'y a pas la moitié de l'équipe qui gagne un
    match et pas l'autre.

## Outil électronique

-   A utiliser le moins possible pour le suivi du "tableau" d'une
    itération.
-   Créer un compte "équipe" pour affecter les tâches...à l'équipe.

PS : on peut aussi décider de ne pas laisser l'équipe s'auto-organiser,
affecter les tâches, prendre l'engagement pour l'équipe, blâmer, et
ainsi rester bien tranquille. C'est tellement reposant, mais
dangereusement inefficace pour la création de valeur de l'organisation.
