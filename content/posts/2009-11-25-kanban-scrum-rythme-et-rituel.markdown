---
date: 2009-11-25T00:00:00Z
slug: kanban-scrum-rythme-et-rituel
title: Kanban, Scrum, rythme et rituel
---

Ce billet prend naissance au travers de la lecture de cet excellent
article : [The philosophy of Kanban is Kryptonite to Scrum](http://consultingblogs.emc.com/simonbennett/archive/2009/11/10/the-philosophy-of-kanban-is-kryptonite-to-scrum.aspx)
. Je n'ai pas mis en oeuvre KanBan. Je n'en ai pas eu à ce jour
l'occasion (cela ne saurait tarder), mais prenez donc cet article avec
des baguettes car je n'ai pas de *feedback* terrain à ce sujet.

Ma problématique :

1.  Scrum et les méthodes agiles connaissent un succès exponentiel en ce
    moment : il suffit de jeter un oeil au [Google
    Trends](http://www.google.fr/trends?q=scrum+-rugby&ctab=0&geo=all&date=all&sort=0)...
2.  Les SSII veulent manger leur part du gâteau, les entreprises se
    ruent dessus
3.  Il n'est pas aisé pour les SSII ou les entreprises de mettre en
    place Scrum pour -à mes yeux- deux principales raisons :
    contractualisation, stabilité/pérénité de l'équipe (la troisième
    serait "responsabilisation" du product owner, et donc du client quel
    qu'il soit)

A la lecture de l'article cité ci-dessus je me dis que la solution
réside peut être dans KanBan. Il ne règle pas la question de
contractualisation mais il peut régler celle de la stabilité de
l'équipe. Pourquoi ? Scrum apporte beaucoup de souplesse en réponse aux
besoins d'un projet, mais il n'introduit pas nécessairement beaucoup de
souplesse au sein de l'organisation. Il nécessite un rituel fort et
contraignant : les sprints,  les daily scrum, les retrospectives, le
tout organisé autour d'une équipe très stable et très impliquée. Pour
une SSII (ou une entreprise) ce n'est pas si simple à mettre en oeuvre.
Rare sont les moments où une équipe peut effectivement entièrement se
consacrer à son sprint. Rare sont les projets ou une personne/évènement
externe ne vient pas bousculer le planning, les priorités. KanBan
propose un cadre moins rigide (oups scrum méthode agile serait rigide,
non ne me faites pas dire cela), en tous cas il propose moins de
recommandations d'une part (pour reprendre les termes de Henrik Kniberg
(cf les liens ci-dessous)), et ne l'assujetti pas à des *timeboxes* et
un rituel régulier (stand up, etc..). Mais le cycle de Kanban se
différencie de la vélocité de Scrum. Le rythme/"flow" de Kanban me
parait beaucoup moins contraignant. la vélocité de Scrum est rythmée par
le rituel, mise en exergue par les burndown chart, on se doit de
respecter l'itération : si une user storie n'est pas achevée à la fin du
sprint elle n'apparaitra pas dans la démo, elle a été hors du rythme et
donc exclue. Là où Kanban -il me semble- va être plus docile : on déduit
le rythme des faits, on ne s'y soumet pas.

Bref, l'exercice de suivre la rythmique de Scrum n'est pas aisé, je me
répète, d'où aussi l'importance de la sensibilisation du management au
respect des règles d'un projet Scrum. Mon propos est aussi de dire que
des fois ce n'est pas possible et qu'il s'agit peut-être là finalement
de plutôt se tourner vers Kanban (sans opposer les deux, ce en quoi je
ne fais que redire ce que l'article cité ci-dessus explique bien mieux).

Autre point qui m'amène à évoquer KanBan, il se peut (double
conditionnel alto arrière..) que je sois amené à coacher des équipes
composées de 1 à 2 personnes dans un prochain temps. Avec 1 personne
Scrum perd de sa substance...  On se retrouve simplement dans le mode
*héro* (oyez CMMi niveau 1...), en espérant que cet acteur solo a une
bonne culture de la gestion d'un projet informatique, des outils qu'il
peut mettre en place, etc, mais nulle question à mes yeux de réaliser un
daily stand up seul, surtout si une seconde personne vient de façon
épisodique compléter l'équipe.

Là encore KanBan avec son approche -apparemment- plus simple, et
beaucoup moins ritualisée, va peut-être pouvoir répondre à ce besoin. En
introduisant un simple "radiateur" KanBan je peux amorcer une gestion
rythmée du projet, je n'ai pas de sprint à respecter, je peux introduire
une seconde personne de façon épisodique. Bref je me répète :  je peux
rythmer plus simplement le projet  et qui dit rythme, dit charge et
planification et donc indicateurs et prévisions, etc.

Pour finir et afin de le rappeler une nouvelle fois, je n'ai pas encore
mis en oeuvre Kanban à l'inverse de Scrum, excusez moi par avance si
j'avance ici des grossièretés à vos yeux de Kanbanmaster (?). Je veux
simplement souligner pourquoi KanBan serait une piste intéressante. J'en
décèle deux : impossibilité d'appliquer le rituel de Scrum et de
garantir la stabilité de l'équipe, l'un étant souvent le corollaire de
l'autre.

Un autre article très intéressant : [Kanban and Scrum a practical
guide](http://blog.crisp.se/henrikkniberg/2009/11/19/1258614240000.html)
et surtout le PDF associé qui compare Scrum à KanBan, celui avec la
citation de Myamoto Musashi, ...

[Kanban and Scrum - a practical guide](http://blog.crisp.se/henrikkniberg/2009/11/19/1258614240000.html)
