---
date: 2025-02-02
slug: index-du-travail-a-distance-c-est-quoi-ton-score
title: "Index du travail à distance : c'est quoi ton score ?"
draft: false
---

Cinq années sont passées depuis le fameux [16 mars où la France s'est enfermée derrière ses volets](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000041728476) lors de la pandémie de COVID. 

Je vous propose un questionnaire comme dans certains magazines pour évaluer votre capacité à travailler à distance, votre index de travail à distance. **Le résultat du questionnaire est à revoir toutes les semaines**. 

Voici le questionnaire version 1.0 : *index de travail à distance*. 

---

## Puis-je travailler à distance ? 

### Questionnaire principal (max 250 points)

* Je suis expérimenté : cela veut dire que je sais m'organiser seul, j'ai de la discipline, j'ai de la rigueur, du focus. Si vous avez le moindre doute, la réponse est NON. **+60 points**. 

* J'ai accumulé assez d'information pour travailler seul un moment. Besoin de personne pour savoir. **+40 points**

* J'ai assez accumulé de culture et de sens de mon organisation ou de mon groupe pour me sentir appartenir à celui-ci. Pas de vide social, pas d'isolement, pas de solitude. Attention c'est souvent difficile à détecter. Et j'ai une vie sociale à côté du travail : des connaissances, des interactions, des conversations, etc. **+30 points**

* J'ai assez d'expérience pour me nourrir intellectuellement de façon autonome (introspection, veille) : dans le sens où je n'ai besoin de personne pour progresser et où j'ai observé que je pouvais progresser seul. **+30 points**

* Personne n'est censé m'encadrer pour mon bien (me faire progresser, m'aider ), bref je n'ai aucune relation hiérarchique au-dessus de moi.**+30 points**

* J'ai les infos maintenant et je peux produire seul toute la semaine : développer du code, faire des maquettes, monter les éléments, et je peux le faire seul (mon métier me le permet). Besoin de personne pour le faire. **+30 points** 

* Tout le monde fait du travail à distance de façon identique dans mon environnement (tout le monde même rythme, même pratique). **+30 points**. 

<!-- 250 points max -->

### Quelques bonus (max 90 points)

* Tous les jours, je passe une heure minimum avec connexion conversation partage sur le contenu de ce que nous fabriquons, envisageons, etc. Il ne peut pas s'agir de réunion (elles ne doivent pas compter dans le temps passé). Je parle ici de *mob programming*, de *pair* quelque chose, etc. **+20 points.** 

* Je me suis super bien équipé : écran, fauteuil, caméra et surtout bon son. **+20 points**

* Pas de souci je laisse toujours ma caméra allumée, et on me voit vivre dans mon chez moi. **+10 points**

* J'ai un lieu dédié au travail à distance (aucun autre écran, aucune autre personne). **+10points**

* Ajouter **+40 points** si vous avez un système de conversation type slack, mattermost, etc., et que celui-ci est très actif.(Très actif j'ai dit ! Dizaines ou plusieurs dizaines de messages par jour auxquels vous trouvez un intérêt).  

* J'ai un co-working **+30 points** dans lequel je vais régulièrement. 

<!-- 60 points max -->

### Autres bonus (max 80 points)

* Ajouter **+20 points** si vous passez 1 jour de présence par semaine (pas de travail à distance) et que cela permet de rencontrer et avoir des interactions au moins 10 personnes ou 50% de mes camarades OU :  
* Ajouter **+60 points** si vous passez 2 jours de présence par semaine (pas de travail à distance) et que cela permet de rencontrer au moins 10 personnes ou 50% de mes camarades OU : 
* Ajouter **+80 points** si vous passez 3 jours de présence par semaine (pas de travail à distance) et que cela permet de rencontrer au moins 10 personnes ou 50% de mes camarades. 

<!-- 80 points max -->

---

## J'ai accumulé ... points : 

#### Au moins ou plus de 250 points.  

Super. Vous pouvez continuer ainsi jusqu'à la semaine prochaine et refaire alors le test. 
Si ce score persiste plusieurs semaines d'affilé, il serait intéressant que vous évaluiez ce score pour vos collègues de travail et que vous vous assuriez que ce soit le même cas pour eux. Si ce n'est pas le cas, vous perdez **-100 points**. 

#### Entre 150 et 250 points. 

Super. Tout va plutôt bien. C'est juste plus lent. Tout est plus lent. Mais peut-être (et c'est un autre débat) que c'est exactement ce qu'il faut à nos vies et au monde. 

#### En dessous de 150 points

Mieux vaut passer la semaine sur site (naturellement à condition que vous ne soyez pas seul, mais c'est un autre débat). 
J'ai besoin d'apprendre, de progresser, d'avoir des interactionsi, d'être observé. 
  









