---
date: 2014-10-23T00:00:00Z
slug: des-bidonvilles
title: Des bidonvilles
---

Lors de ma dernière conférence ([les organisations vivantes](https://speakerdeck.com/pablopernot/les-organisations-vivantes))
j'ai essayé d'expliquer pourquoi il ne me semblait pas adéquat d'appréhender votre organisation et son expansion avec un organigramme
mais un schéma de pensée. Pour cela j'avais essayé de trouver des références dans ce qui nous entoure, la nature, deux extrêmes : le
cristal pour ce qu'il est d'organisé au sens cartésien, fractal, du terme, et à l'autre bout la termitière, plus grande construction
naturelle (hors de celles des hommes) qui démarre par le hasard, et se bâti sur l'émergence (Je reviendrai sur toutes ces réflexions dans un
mini livre qui fera je l'espère la suite de "la horde agile").

voir [cet article concernant le cristal](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/).

Pour expliquer ces différentes topologies et formes, ainsi qu'un certains schéma de pensée, je me suis appuyé sur la théorie des
catastrophes : [voir ce précédent article sur la théorie des catastrophes](/2014/01/penser-son-organisation-theorie-des-catastrophes/).
Cette théorie s'appuie sur des dynamiques et des images très intuitives : la déchirure, le pli, coudre, engendrer, etc.

Imaginez une ville à l'image de votre organisation : un quartier grandit soudainement il se scinde, telle dynamique provoque qu'un quartier se bouche, ou se vide, qu'un nouveau quartier est engendré, etc... je reprends ainsi toute cette terminologie de la théorie des catastrophes.

Mais si on la poussait sans raison ou sans sens en quelque sorte et en reprenant l'image de la ville, on pourrait très bien aboutir aux
bidonvilles qui semblent respecter ces dynamiques parfaitement.

{{< image src="/images/2014/10/bidonvilles2.jpg" title="Bidonvilles" >}}

## Bidonvilles

Personne ne souhaite vivre dans un bidonville : l'insalubrité, l'insécurité, etc. sont aux antipodes de nos aspirations. Cependant
certains aspects des bidonvilles peuvent nous intéresser : une façon de s'adapter au mieux à l'espace, une exploitation maximum de la matière
première offerte, (et peut-être des effets de communication très intéressants à observer).

Autre source d'intérêt concernant les bidonvilles : leur capacité à être très grands. Pas mal de réponses amenées à ceux qui s'intéressent aux organisations à grande échelle.

{{< image src="/images/2014/10/ken_kibera_large.jpg" title="Bidonvilles" >}}

On retrouve peut-être là au niveau de l'organisation (topologie, forme, architecture) ce qui a fait le succès du Lean et de l'agile : une
nécessité d'optimiser la valeur. Mais encore, nul ne souhaite vivre dans un bidonville. Au contraire. Je ne vous propose qu'une réflexion en
cours mais ce que j'évoquais comme piste lors de cette conférence c'est peut-être la recherche du meilleur des deux mondes en quelque sorte :
l'optimisation organique et spatiale des bidonvilles sans l'insalubrité et l'insécurité, avec du sens et de la création de richesse.

## Les leçons des favelas brésiliennes

{{< image src="/images/2014/10/slum.jpg" title="Bidonvilles" >}}

[Slum upgrading lessons from Brazil](http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf)

En fouillant autour des bidonvilles je suis tombé sur un rapport consolidé au sujet des apprentissages liés à l'amélioration des
bidonvilles brésiliens : les leçons retenues des bidonvilles qui ont réussi à aller mieux. En lisant ce rapport : [Slum upgrading lessons from Brazil](http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf) je suis un peu tombé de ma chaise tellement j'avais l'impression que les dîtes leçons semblaient provenir tout droit d'un manuel d'organisation Lean/Agile. Mais écoutez plutôt :

## Comment faire qu'un bidonville aille mieux ?

Pour s'en sortir il faut :

-   Autonomie administrative
-   Formation continue
-   Diversité des habitats
-   Installation incrémentale de bassins d'eau
-   Proximité entre l'habitat et le lieu de travail
-   Equipe multidisciplinaire
-   Changement étape par étape

## Comment faire pour maintenir un meilleur état ?

Maintenir cette organisation en bon état c'est :

-   Encourager la socialisation
-   La résolution de problème doit être décentralisée
-   Création de communautés
-   L'implication vient de la transparence
-   Utiliser de vrais métriques
-   La vision est collective, par le consensus
-   Incorporation de nouvelles têtes, de regards externes

## Donner du sens, la communication est clef

En fait on observe plus globalement que la communication et la dynamique de groupe sont clefs pour donner du sens et avoir une dynamique
positive. La communication c'était aussi le point le plus important dans ma conférence sur concernant l'élément qui constitue nos organisations : nous.

## Grandir ce n'est pas se massifier, ni se répéter

Pour finir et pour revenir à ce fameux débat sur des organisations à grandes échelles. Je n'ai rien contre des organisations qui veulent
grandir, l'important à mes yeux, si je me réfère encore à une analogie avec la nature, c'est d'éviter l'inertie, l'immobilisme, tout change,
tout le temps. Mais si donc vous voulez grandir une dernière leçon des bidonvilles brésiliens, le rapport nous dit que grandir convenablement
c'est :

-   Grandir ne veut pas dire grossir, massifier (*scale up versus mass*)
-   Grandir ne veut pas dire répéter (*scale up versus repetition*)
-   Effet pervers de la réduction des coûts

On touche là deux points centraux de l'agilité :

Ne croyez plus à linéarité, ni à la répétabilité. Le monde change trop vite et la linéarité ou la répétabilité sont un leurre (même si des fois je m'interroge si le changement permanent n'est pas une manière confortable de ne pas affronter les vrais problèmes ?). Petite
parenthèse si aujourd'hui les gens utilisent de plus en plus le mot *Lean* c'est parce que cela fait bien, c'est sérieux, c'est l'industrie
derrière qui se profile au travers de Toyota, la répétabilité. Ils se trompent lourdement dans ce cas : l'époque n'est plus à ça mais à
l'adaptation constante prônée par l'agilité qui est simplement le *Lean* moderne.

Concernant la réductions des coûts je ne citerai que cette phrase (dont je pense qu'elle est de Peter Drucker, mais je ne suis pas sûr) :
*si vous suivez les coûts, vous générez des coûts, si vous suivez la valeur, vous générez de la valeur*.
