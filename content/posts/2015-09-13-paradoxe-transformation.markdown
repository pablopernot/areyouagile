---
date: 2015-09-13T00:00:00Z
slug: paradoxes-des-transformations-agiles
title: Paradoxes des transformations agiles
---

Toute l'émergence agile joue sur un paradoxe toujours difficile à
maîtriser. Celui-ci se décline d'un côté sur le besoin d'alimenter
l'émergence d'une réponse contextuelle, et de l'autre d'apprendre par
une pratique respectueuse. Malheureusement très souvent sous couvert
d'une adaptation au contexte on est irrespectueux de la pratique, et
bien souvent cette adaptation au contexte cache la résurgence de nos
habitudes plutôt qu'un vrai changement, qu'une véritable adaptation.

## Système complexe émergent

Les gens et les compagnies ont raison : chaque Agile aura sa
personnalité, sa propre définition, selon quand et où il est implémenté.
C'est l'idée même de l'agilité que cette adaptation au contexte qui tend
vers la réponse la plus adéquate, celle portant le plus de valeur pour
le minimum d'effort (quelque chose de très naturelle ce minimum
d'effort, bien souvent dans la nature, les éléments sont fainéants,
beaucoup de formes naissent d'une économie d'énergie). Ce constat parait
simple, il ne l'est pas. Cette adaptation au contexte sous-entend aussi
que quand le contexte change, l'adaptation change aussi. Et croyez moi
le contexte change sans arrêt. Mais revenons à la transformation agile :
nous ne pouvons pas nier que chaque transformation, chaque contexte,
possède une réponse (agile) adéquate. Et aussi nous ne pouvons pas
réfuter un constat que font tous les agents du changement : on n'emmène
pas des personnes ou une compagnie où elles ne souhaitent pas aller. On
n'en a pas envie, et ce n'est pas possible.

Donc, comme tout système complexe émergent, il est par définition,
émergent, donc adapté.

## Apprentissage

Problème récurrent : cette adaptation devrait provenir d'un
apprentissage, cette adaptation devrait remettre en cause certaines
pratiques d'une doctrine, une référence ; malheureusement bien souvent
on ne prend pas le temps d'apprendre, on court-circuit derechef. Bien
souvent les personnes et les compagnies décident qu'elles connaissent
déjà les adaptations nécessaires, et c'est souvent une erreur. Oubliez
le naturel il revient au galop, c'est bien souvent la mémoire du muscle
qui parle. D'autant que Agile c'est un vrai saut dans un autre
paradigme, il est difficile de se projeter sans le vivre avant.

Cette émergence à souvent besoin de se nourrir d'un apprentissage
orthodoxe.

D'où l'évocation régulière dans le mouvement agile de l'idéogramme
Shu-Ha-Ri. Cette imagerie provient des arts martiaux.

{{< image src="/images/2015/09/shuhari.jpg" title="Shu-Ha-Ri" >}}

### SHU

Fait ce que le mentor te demande de faire sans sourciller. Applique la
règle, pratique, et pratique encore.

### HA

En faisant tu as vraiment compris comment cela fonctionnait et
**maintenant** tu peux faire des adaptations contextuelles.

### RI

Tu as intégré l'enseignement, tu le fais intuitivement (entorse ou non à
la règle).

Donc d'un côté on ne peut pas nier l'émergence d'une réponse
contextuelle, de l'autre le besoin de répondre dans un premier temps de
façon très orthodoxe, ou plutôt de baser cette adaptation sur une
connaissance réelle du nouveau paradigme. Cet apprentissage de
l'orthodoxie (du nouveau paradigme) est bien trop souvent ignoré. Soit
parce que l'on en comprend pas l'importance, soit car il est impossible
à mettre en œuvre (pour de bonnes raisons).

Il faut en comprendre l'importance, je ne peux pas apporter d'autres
réponses sur ce point. Sans référence vous avancez les yeux fermés, avec
un changement de paradigme ce n'est pas qu'une image. Concernant les
difficultés de mise en œuvre c'est souvent l'utilisation de
"démonstrateur" (une ou plusieurs équipes auxquelles on donne les moyens
de l'orthodoxie et de l'apprentissage). Il faudrait savoir **suspendre
ses croyances** pour laisser émerger les bonnes réponses, sans croire
trop vite les avoir déceler.

L'intention derrière la transformation agile est donc naturellement
clef. C'est l'intention qui octroie les moyens.

## Changer

Le dernier point de blocage (après la sensibilisation à l'importance de
l'apprentissage de l'orthodoxie, et la capacité à se donner les moyens,
suspendre ses croyances) c'est la capacité à pouvoir revenir en arrière.
Ce dernier point est très étranger à nos habitudes sociétales : quand
une loi est prononcée on n'a pas l'habitude d'entendre dire :
"finalement elle ne marche pas bien on revient en arrière". C'est bien
dommage. Or pour faciliter la conduite du changement, la capacité à
essayer vraiment, à vraiment suspendre ses croyances, il est important
d'être protégé (par des démonstrateurs comme évoqué plus haut, ou
l'utilisation d'une technologie comme [open agile
adoption](/2014/04/histoires-dopen-agile-adoption/), etc.) et donc de
pouvoir revenir en arrière. Sans protection on va se protéger soi-même
en résistant au changement. Il faut pouvoir faire un pas en arrière pour
rapidement en faire deux en avant. Là aussi l'intention derrière la
transformation agile est clef, si elle est valide elle saura se donner
les moyens de ces pas en arrière.

## Nos paradoxes

-   **L'émergence s'appuie sur un apprentissage orthodoxe (et pas sur
    nos habitudes ou nos désirs)**.
-   **S'adapter à un contexte qui n'attend pas pour évoluer (donc on
    s'adapte constammment)**.
-   **Avancer dans une direction en suspendant nos croyances (essayer,
    oser)**.
-   **Savoir changer c'est s'autoriser à revenir en arrière (un pas en
    arrière peut en amener deux en avant)**.

De fait de son acuité sur tous ces points, **Agile est aussi devenu une
technologie de conduite du changement tout terrain**.

En décidant tout récemment de changer d'organisation et d'aventure, en
quittant officiellement la boite que j'avais fondé avec Gilles et
Christophe, pour rejoindre David chez
[beNext](http://benextcompany.com), -- croyez moi -- le changement, je
pratique. C'est salvateur pour un acteur comme moi, je ressens tous les
sentiments que j'évoque plus haut. J'en tire un enseignement essentiel :
la changement c'est d'abord un fantastique accélérateur d'apprentissage.
(Démarrage officiel le 1er octobre).

Je pense que cet article est à rapprocher des suivants :

-   [Chemin d'une transformation agile](/2015/06/chemin-dune-transformation-agile/)
-   [Qu'est ce qu'une transformation agile réussie](/2015/05/quest-ce-quune-transformation-agile-reussie/)
