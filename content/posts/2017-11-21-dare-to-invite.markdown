---
date: 2017-11-21
slug: dare-to-invite-openspace-agility
title: Dare to invite, Openspace Agility
---

Je profite d'une session hier autour de **openspace agility** et de la venue de [Dan Mezick la semaine prochaine](https://osa.areyouagile.com) pour remettre des slides que j'avais réalisé pour NCrafts 2016, ne les trouvant jamais quand je les cherche.  

En anglais, sur la conduite du changement, l'engagement et **openspace agility**. 

{{< slideshare id="KGINVlqEgL4hq2" >}}

Enfin, la vidéo de la session. 

{{< vimeo id="167722770">}}

Merci [ncrafts](http://ncrafts.io/). 




