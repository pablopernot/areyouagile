---
date: 2013-05-01T00:00:00Z
slug: adn-agile-feedback
title: ADN Agile feedback
---

\[ La nouvelle et dernière version du jeu s'appelle "La scierie à
pratiques" et se trouve [là](/pages/la-scierie-a-pratiques.html)\]

Voici un premier feedback sur cette tentative de jeu agile [ADN Agile](/2013/04/agile-adn/). Suite à diffusion de ce [premier
article](/2013/04/agile-adn/) j'ai eu pas mal de retours. Merci à tous, je les résume rapidement ici, et je donne aussi le feedback d'une première longue partie hier soir à la "Baraka Jeux" dans Montpellier.
Naturellement une nouvelle version du jeu est livrée dans la foulée,
disons celle du 1er mai (un jeu ce jour là c'est bien) estampillée
"Baraka Jeux".

## Feedback du groupe agile games

Donc d'abord pas mal de retours sur le groupe de discussion [agile games
france](https://groups.google.com/forum/?fromgroups#!forum/agiles-games-france),
de Pierre, Alfred ou Jacques. Merci à vous. J'en retiens pour l'instant
(le jeu a besoin de se rôder pour avoir un véritable avis):

-   Pas mal de propositions de sources pour les pratiques. Les pratiques
    proposées sont celles qui me sont apparues comme les plus évidentes.
    Naturellement les cartes évolueront comme les pratiques évoluent
    elles-mêmes. 36 est un bon nombre (je le confirme mieux depuis hier,
    36 me parait un bon nombre de départ).
-   Gérer des familles. Une idée à développer que je ne capture pas pour
    l'instant. Car classer les pratiques par famille induit déjà une ...
    classification. Or j'ai bien revu hier (comme à Agile Open Sud)
    qu'il y avait moultes variations dans nos compréhensions des
    pratiques, et notamment selon le contexte (j'y reviendrai). Faire
    des familles cela reste un arbitraire que je ne souhaite pas pour
    l'instant intégrer. Mais forkez libre.
-   On évoque le fait d'utiliser le jeu en rétrospective ou en
    formation. Bonnes idées. Si quelqu'un a des feedback je suis
    preneur.

Bref je résume très rapidement et je vous encourage à rejoindre le
groupe de discussion pour parler jeux agiles en France.

## feedback de Frédéric

{{< image src="/images/2013/05/ffaure32.jpg" title="Agile ADN" >}}

Mes commentaires sur ton nouveau jeu : Jeu super simple à mettre en
place et vraiment utile pour ouvrir la discussion. Quelques remarques :
- le découpage et tri des cartes est un peu laborieux mais bon, ca fait
partie du jeu ;) - Il y avait 5 votants et le 1er vote n'a fait
ressortir que 3 pratiques. Les présents étaient sans doute hétérogènes.
On a élargi le mode de sélection en conservant ceux qui avaient 4 voix
sur 5. On s'est retrouvé avec 8 pratiques, un bon nombre à mon goût,
pour la discussion. On les a réunies en 3 grandes familles (quotidien,
backlog et management visuel) et on a discuté pour garder un élément de
chaque famille. - Attention, "Estimates" apparait 2x et pour moi, il
manque "impediment backlog" comme pratique.

### Mon retour

D'abord merci, et oui y'a un petit bug : deux cartes "estimates", je
corrige en ajoutant : "Agile Game" et je supprime le sous-titre
"elevator pitch" qui limite trop "vision". Ensuite nous hier nous étions
4 et on est arrivé d'accord sur 4 pratiques (unanimité), et 10 avec 3
voix sur 4. Cela me semble une indication déjà instructive : nos deux
groupes ne sont peut-être pas vraiment alignés. En fait chez nous une
personne en particulier avait une approche différente, et on a pu
rapidement l'observer, donc potentiellement réaligner tout le monde (ou
pas d'ailleurs). Nous avons décidé de nous lancer sur une discussion sur
les 10 pratiques communes au 3/4. On aurait peut-être du se focaliser
sur les 4 qui ont émergées mais bon, j'y reviens plus bas. Oui c'est un
bilan de groupe qui n'est pas personnel. C'est le bilan du groupe dans
ce **contexte**. J'y reviens plus bas aussi. merci en tous cas. Enfin,
je confirme découper les cartes c'est c\*\*\*t. Et je vais ajouter très
bientôt une petite glyphe sur les jeux de façon à vite les reclasser...

{{< image src="/images/2013/05/barakajeux-1.jpg" title="Agile ADN" >}}

## Feedback de la soirée

D'abord juste une description : Stéphane, Vincent & Bertrand m'ont fait
le plaisir de venir tester. Espace détente, bières, cartes, bonne
ambiance, et on se lance. 4 pratiques apparaissent communes au premier
"round" : Retrospective, Validation (par le PO), Visual Management,
Daily Meeting/Scrum. On élargit aux pratiques sélectionnées par 3 des 4
participants, on ajoute : Backlog, Sprint Planning, Timebox,
Prioritization, Review, Definition of done. Suite à une bonne heure de
discussions (entrecoupées par divers sujets), on arrive à Retrospective,
Sprint Planning, Validation (par le PO). C'est notre bilan, ce jour, ce
lieu, cette heure, ces personnes, ce contexte.

Je vais synthétiser mon retour, mille trucs à dire sinon.

### Contexte est essentiel

Le contexte est essentiel, nous nous sommes rendus compte hier au bout
de 20mn que la discussion et les choix changeaient du tout au tout selon
dans quel contexte on se projetait. **il est donc essentiel de préciser
le contexte avant le début de la discussion** et évidemment les avis
changent selon le contexte.

Je propose dans la nouvelle version une série de contextes "types", mais
vous êtes libre naturellement de proposer le votre au début du jeu, par
contre c'est essentiel. Entre aborder ces pratiques comme mettant en
oeuvre une philosophie de vie, ou au contraire se projeter dans le cadre
d'une société qui ne maîtrise pas bien la culture agile et qui cherche à
se transformer les choix, les priorités, les analyses sur ces pratiques
seront différentes. Cet écart est d'ailleurs très significatif.

### Détecter un groupe aligné ou pas

Parmi nous il y avait une personne un peu différente (dans tous les bons
sens du terme). Et donc au lieu d'avoir naturellement 7 ou 8 pratiques
communes, on en avait que 4. C'est une indication qui me plait. Elle
permet de déceler les groupes pas forcément alignés (ce n'est pas un
drame mais c'est mieux -forming, storming, norming, performing, de
[Tuckman](/2010/12/management-agile-le-modele-tannenbaum-schmidt/)-), et
elle met *de facto* en évidence que plus un groupe est grand plus il
sera dur de l'aligner. On le savait déjà ? ok. Juste cela confirme et
c'est intéressant de le percevoir dans une discussions sur les
pratiques.

Pour la suite on a décidé d'intégrer les pratiques avec 3 voix sur 4. On
aurait peut-être du rester sur les 4 communes avec une discussion plus
courte et encore plus intense (j'ai de nouveau beaucoup aimer les
discussions de hier -comme à Agile Open Sud- : pratiques du surface ou
de fond ? pratiques nécessitant confiance ou pas, c'est quoi *vraiment*
la rétrospective, c'est quoi l'acte de valider, etc etc.)

### "Decider", "Sociocratie", "Conversations bienveillantes", etc.

Intéressant aussi hier d'avoir essayer une version très très *light* du
[decider](http://www.mccarthyshow.com/online/) de Jim McCarthy, et une
version plus approfondie avec les conseils de Stéphane (@langlois\_s)
sur la sociocratie. Cette phase de concordance nécessaire au milieu de
discussions âpres met en place des outils utiles. Je pense que c'est
probablement la partie la plus "en devenir" (si devenir il y a) de ce
jeu. Stéphane si tu pouvais nous écrire un petit billet là dessus ? (que
je publie ici si tu le souhaites, et dans le jeu ?). Ces outils doivent
servir à empêcher que l'on s'arcboute trop lors des discussions.

### Pratiques et pas principes

Non, naturellement il ne s'agit pas de cartes principes ou valeurs, mais
bien de cartes pratiques. Oui c'est bien là tout l'intérêt. D'une part
c'est justement le jeu de découvrir toute la valeur ou les principes,
soit la culture, derrière des pratiques, le jeu amène ce que je m'échine
à essayer de faire durant mes missions. D'autre part attaquer une
discussion sur les valeurs ou les principes c'est étouffant. Personne ne
va faire de concession sur des valeurs ou des principes, alors que sur
des pratiques...

## Feedback sur le nom

On me passe un message que je résume : ce nom c'est nul. Une certaine
population s'en plaint, cela rappelle *le marketing à deux balles que
toutes les agences de comm' à deux balles sortent systématiquement dans
toutes les réunions*. Mais que voulez vous ADN ne me rappelle pas les
dérives (à vérifier, on est toujours le "à deux balles" d'un autre) que
certains évoquent, mais bien l'ADN, la moelle substantifique, les
chromosomes évoqués par la lecture de [la revanche du
rameur](/2013/04/lectures-automnehiver-2013/) de Dominique Dupagne, les
notions que Stéphane évoquait hier : la peau la surface, puis dessous,
plus profondément, l'os, la moelle. Bref, **jusqu'à une meilleure
proposition** (au début je voulais appeler ce petit jeu "practices
poker") je garde ce nom, libre à chacun de forker, blasphémer, se
moquer, ou de proposer mieux.

Voilà, la prochaine fois j'espère y jouer au Scrumboat (avec Pierre N.
?) ou à Agile France 2013 ? J'en saurais plus. Merci à ceux qui me
feront d'autres retours.

[Le pdf -VERSION 1er mai 2013 "Baraka Jeux" - avec les règles et les cartes "pratiques"](/pdf/130501-agile-adn.pdf)

## Feedback reçu depuis !

### Le feedback de Claude

"Bonjour mon Pablo, j'ai fait mon feedback sur le jeu adn-agile (
j'espère qu'il aura bientôt un autre nom) : <http://www.aubryconseil.com/post/Les-pratiques-qu-on-kiffe> A bientôt.
Claude"

Salut Claude, merci pour ce retour. J'apprécie. Je note aussi au passage
que le nom ne fait vraiment pas l'unanimité. Bon sur le nom et le côté
galvaudé, je vous ai dit ce que j'en pensais. Et bon, justement comme je
le disais au travers de chose "de surface" (pour reprendre l'expression
de Stéphane) : les pratiques on aborde des choses plus "profonde" (enfin
c'est son ambition). Donc ADN me semble bien (vous voulez dire que je me
suis encore trompé ?). Disons qu'après Stéphane, puis toi, je ne suis
pas loin de basculer. Encore un coup de butoir, et je romps sur le nom.
Tu proposes "Pratikonkif", j'avais pensé "pratiques poker", laisse moi
un peu de temps..

Pour les termes anglais, c'était plutôt une question de temps, je compte
traduire les règles quand elles seront stabilisées, et naturellement il
y aurait eu une traduction dans un sens ou dans l'autre. Sur la liste
des pratiques, c'est rigolo que tout le monde veuille proposer *sa*
liste. Bon c'est normal aussi. Je n'ai jamais pensé la liste que j'ai
proposé comme étant LA liste, loin de là. Que chacun fabrique la sienne
avec plaisir.

Enfin concernant les "fiches" vierges, j'ai hésité sur au doublon sur
"estimation" détecté hier a en laissé une vierge pour les mêmes raisons
que tu décris, mais finalement je ne l'ai pas fait me disant qu'elle
aurait un poids directement très fort. Mais cela demande a être testé.

Par contre je ne suis pas du tout d'accord avec ton classement "inclus
dans". Cela a été un grand débat hier soir, et justement c'est un débat
à avoir. Inclure validation avec définition de fini me parait osé par
exemple. Cette approche "inclus dans" a été notre façon d'éliminer
certaines pratiques, mais on se rend compte que déjà définir une
inclusion est très arbitraire et dépend du contexte. Donc je ne te suis
pas vraiment là dessus. Mais cela n'a aucune conséquence, libre à chacun
d'arriver avec son jeu de pratiques.

Pareil Scrummaster & Product Owner sont des rôles, plus des pratiques.
Cela va rendre les choses ambigües non ?

Enfin oui et oui : 32 cartes c'est suffisant (éprouvé hier soir), et oui
donc le contexte comme l'objectif sont très importants : ne pas avoir de
contexte nous a sauté à la figure hier.

Merci beaucoup pour ce feedback, et surtout je crois qu'il faut le
tester et laisser du temps. Après on verra si il tient la longueur ou
pas.

Merci

### Le feedback de Bruno

Feedback ce matin (2 mai) de Bruno sur le nom. Et je crois que j'aime
beaucoup son idée de "**Agile Gestalt**".

{{< image src="/images/2013/05/brunobord.jpg" title="ADN Agile" >}}

