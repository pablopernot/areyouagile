---
date: 2012-04-23T00:00:00Z
slug: mix-it-sudweb
title: Mix-it & Sudweb
---

## Sudweb 2012

{{< image src="/images/2012/04/LogoSudWeb-300x206.jpg" title="SudWeb" >}}


Vive le printemps comme dirait
[Bruno](http://jehaisleprintemps.net/blog/fr/2012/03/15/alea-jacta-sudweb/)
avec qui j'aurais le plaisir d'animer un atelier à haut risque lors du
prochain [Sudweb 2012](http://sudweb.fr/2012/) (le 26 mai à Toulouse).
Cet atelier est à haut risque car c'est un jeu, et un jeu cela a besoin
d'être rodé. Et c'est un jeu ambitieux : 1h30 autour d'un [scénario
machiavélique](http://sudweb.fr/2012/talk/le-projet-web-dont-vous-etes-le-heros-agile/)
à la façon d'un *livre dont vous êtes le héros*.

Pour en savoir plus
l'article de [Bruno](http://jehaisleprintemps.net/blog/fr/2012/03/15/alea-jacta-sudweb/) ou
la [description de
l'atelier](http://sudweb.fr/2012/talk/le-projet-web-dont-vous-etes-le-heros-agile/)
:

On compte sur vous pour venir participer et avoir votre avis. Cet
atelier se déroulera dans le cadre de la journée "élaboratoire"  :
faisons le constat que la majorité des gens sont saturés des powerpoint
et qu'il faut un apprentissage participatif !

A cette occasion voici le petit texte pondu pour Sudweb (je le replace
ci-dessous)
: \`<http://sudweb.fr/2012/cette-drole-de-journee-quest-lelaboratoire-vue-par-pablo-pernot/%60_>

## Aux idées, citoyens

Quand Sébastien ou Frank du staff Sud Web évoquent avec moi la journée
participative du samedi, je ne peux que jubiler. Une journée d’ateliers,
de participation, de co-création, une promesse d’énergie, de plaisir, et
peut-être d’innovation brute. Dans la mouvance
des barcamps, code/coach-retreat ou desopenspaces les formats
participatifs ont le vent en poupe car, pas de secret, ils proposent un
très bon environnement pour apprendre :

> *Tell me, and I will forget. Show me, and I may remember. Involve me,
> and I will understand.*
>
> – Confucius, 450 B.C

Il faut donc se plier à une condition : faire participer.

Essayons de bien distinguer 3 états :

1.  L’écoute passive : un orateur avec des slides.
2.  La participation : l’orateur fait participer le public, et du coup
    le contenu de son discours n’est plus prédéterminé.
3.  Enfin la co-création : il n’y a plus de différence entre l’orateur
    et le public. On passe d’un état très prédictible à un état très
    imprévisible. (C’est d’ailleurs la crainte de certains : comment ne
    pas prendre le risque de finalement ne déboucher sur rien. Cela
    arrive rarement et si cela doit arriver prenez vos deux pieds et
    changez de session).

Pour cette journée participative que Sud Web a décidé de joliment donner
le nom d’ “élaboratoire” on oubli donc l’état 1, l’écoute passive
de slides. Cela ne nous intéresse pas dans ce cadre (je n’ai pas dit :
pas de slides, je suggère : pas majoritairement “d’écoute passive”). Ce
qui intéresse Sud Web c’est que vous veniez avec des idées de formats
répondant à des objectifs de participation ou co-création
(les coding-dojo sont finalement un exemple).

A mes yeux le meilleur véhicule est probablement le jeu, une approche
ludique. Il pose des contraintes claires, partage un problème avec les
participants, laisse s’établir des relations sociales au sein du groupe,
les aspects répétitifs et le feedback constant en fond une source de
progression palpable. C’est aussi le terreau de l’innovation : un monde
complexe (non prévisible) au sein d’un conteneur : des “contraintes
libératrices” comme dirait Paul Valery.

## Mix-it 2012

{{< image src="/images/2012/04/mixit-banner-300x101.jpg" title="MixIt" >}}

Plus tôt, dès cette semaine (le 26 avril à Lyon, mais toutes les places
sont prises il me semble) j'aurais l'occasion de donner une session à
[Mix-It 2012](http://www.mix-it.fr/). Un évènement qui allie plusieurs
centres d'intérêts : Agility,Techy,Trendy,Gamy,Weby. Mélangeons ! il en
sort toujours quelque chose d'intéressant. J'essayerai de donner une
petite suite à "Anatomie d'une mission agile", une saison 2 en quelque
sorte.
[Session](http://sudweb.fr/2011/post/Anatomie-d-une-mission-agile.html)
que j'avais d'ailleurs proposé en 2011 à ... Sudweb.

A ce sujet Romain de Mix-It me propose quelques questions concernant le
buzz agile actuel...
: \`<http://www.mix-it.fr/article/13/rencontre-avec-pablo-pernot-coach-agile%60_>

Je reproduis l'article ci-dessous.

## Agilité & Buzz

**Rencontre avec Pablo Pernot, coach agile**

**Nous nous sommes entretenus avec Pablo Pernot qui animera la session
"Anatomie d'une mission agile, saison 2"**

>  \**L'équipe Mix-IT : Peux-tu nous dire qui tu es ?*\*
>
> [Pablo Pernot](http://www.mix-it.fr/profile/pablopernot) : Qui suis-je
> ? ouh là. Euh ... dans le contexte de Mix-IT je suis surtout un
> intervenant "agile" qui vient faire part de son expérience, d'une
> expérience en tous cas. Mais sinon je suis plutôt un homme heureux,
> père de deux merveilleux enfants, amoureux de sa femme, ayant beaucoup
> de plaisir à cultiver une attirance pour quelques instruments "has
> been" comme la flute irlandaise ou le banjo old time. J'ai eu la
> chance d'avoir un parcours atypique qui m'a permis de faire des
> lettres et de la philosophie plutôt que des maths ou ce genre de
> trucs. En ce moment je concrétise la liberté que j'ai pu gagner en
> faisant grandir une petite société de conseil avec Christophe Monnier
> & Gilles Pommier [Smartview](http://www.smartview.fr/).
>
> **L'équipe Mix-IT : L’agilité vient de dépasser la phase de buzz, mais
> tout le monde ne choisit pas ces méthodes. Quels sont encore les
> freins que tu rencontres au quotidien ?**
>
> [Pablo Pernot](http://www.mix-it.fr/profile/pablopernot) : Attention,
> si le succès de l'agile a fait disparaître certains freins il en a
> fait apparaître d'autres. L'effet de mode est source de confusion.
> Cependant il est certains que le succès aidant, nous sommes allés plus
> loin avec agile. Et donc des questions plus vastes se posent : comment
> concilier l'état d'esprit agile avec l'organisation, sous-entendu
> comment changer le management ou quels sont nos rapports avec les
> aspects budgétaires à grandes échelles : actionnariat, plan 3/5/7ans,
> stratégie d'entreprise, etc. ?
>
> Au quotidien la question que je me pose le plus fréquemment
> actuellement est : comment motiver les gens, ou plutôt comment changer
> l'état d'esprit des gens ("agile is a mindset"), et encore : faut-il
> vouloir changer l'état d'esprit des gens, n'est-ce pas là une
> intrusion trop violente ? Donc où s'arrête la méthode orientée vers la
> réussite des projets, où commence une forme de manipulation
> psychologique pas forcément très/toujours judicieuse ...
>
> **L'équipe Mix-IT : Quels sont pour toi les facteurs de réussite d’une
> transition agile ?**
>
> [Pablo Pernot](http://www.mix-it.fr/profile/pablopernot) : Mais c'est
> l'un des sujets de [ma
> session](http://www.mix-it.fr/session/12/anatomie-d-une-mission-agile-saison-2) :)
> passez voir. En quelques mots : de véritables raisons pour prendre le
> chemin agile, une réelle sensibilisation et un soutien fort du
> management, du piment pour provoquer le changement : du sang neuf, un
> œil extérieur, etc. Mais le terme "réussite" est souvent un piège. Ce
> n'est pas soit une réussite soit un échec, c'est toujours plus "gris",
> et surtout ce n'est jamais définitif : c'est en mouvement constant.
>
> **L'équipe Mix-IT : On entend parfois que “l’agile ce n’est pas
> possible dans mon contexte”, que leur réponds-tu ?**
>
> [Pablo Pernot](http://www.mix-it.fr/profile/pablopernot) : Pour
> l'instant je n'ai jamais rencontré, ni entendu évoquer un contexte qui
> ne serait pas réalisable en agile. Je caricature : "Waterfall" (modèle
> en cascade) estime que le monde est parfait (puisque l'on peut tout
> préparer, anticiper), et agile que le monde n'est pas parfait (puisque
> l'on devra/pourra s'adapter). Donc si vous avez un monde parfait je
> veux bien croire que agile ne s'y prête pas.
