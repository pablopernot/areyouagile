---
date: 2017-01-25T00:00:00Z
slug: cartographie-plan-action
title: 'Cartographie de plan d''action : le User Story Mapping'
---

Ces temps-ci j'utilise énormément deux outils assez connus du monde du
*design thinking*, du *lean startup*, de l'entrepreneuriat, ou plus
globalement du monde agile : la cartographie de plan d'action, ou
tactique, et la cartographie de la stratégie. Soit le *user story
mapping* et l'*impact mapping*. J'avais déjà pas mal abordé ces outils
au travers de [différents articles](/tag/userstorymapping/) mais a)
j'ai besoin et envie de clarifier mon propos avec des articles b) ma
pratique a continué d'évoluer depuis les précédents articles. Je vais
donc essayer une petite série (5 articles ?) sur ces cartographies. Une
présentation de la cartographie de plan d'action, la *user story
mapping*, puis une présentation d'un exemple d'utilisation détournée,
tordue, mais utile par exemple autour d'une *roadmap*. Une présentation
de la cartographie de stratégie, *impact mapping*, puis une présentation
d'une variation qui fait appelle à sa cousine la *strategy map* et des
*OKR* du management 3.0. Enfin un article sur le mariage éminemment
utile de ces deux approches, l'une enrichissant l'autre.

*ps: ce n'est pas la user story mapping de Jeff Patton à la lettre. Par
exemple je n'utilise pas les "personas". Vous pouvez lire le livre de
Patton, il est bien, rigolo, gouilleur, et les 90 premières pages
suffisent, les autres ne sont que des redîtes pour faire livre.*

## À quoi ça sert ?

Une cartographie de plan d'action cela à définir le ... plan d'action.
C'est à dire tactiquement (opérationnellement, sur le terrain, dans la
gadoue, les mains dans le cambouis), **comment on va mener concrètement
la mise en œuvre de notre stratégie, de notre idée**. Généralement c'est
dédié à la création d'un produit ou d'une offre. **L'objectif est de
découper en petits morceaux qui font sens pour rapidement avoir quelque
chose qui nous permette d'apprendre et/ou de bénéficier de notre
réalisation au plus tôt**. Si nous sommes capable d'ordonner par valeur,
et de délivrer ainsi par petits morceaux qui font sens nous avons une
gestion du risque efficace. Soit nous échouons rapidement et minimisons
les coûts/investissement de cet apprentissage, soit c'est un succès et
le risque s'atténue ou disparait, et nous pouvons bénéficier tôt de nos
efforts.

Je peux reprendre ici la belle maxime de **Jeff Patton : "Minimiser
l'effort, maximiser le résultat"**.

Une cartographie de plan d'action, une *user story mapping*, cela se
réalise en atelier de plusieurs heures, en plusieurs fois selon le
scope, avec idéalement les personnes impactées par l'idée, le produit,
autant ceux qui portent le besoin que ceux qui vont le réaliser. Chacun
nourrit l'autre. Un grand mur ou une grande table sont indispensables.

## Étape 1 : Déterminez votre histoire et ses chapitres

Une cartographie de plan d'action **c'est d'abord et avant tout une
histoire**. L'histoire de votre produit, de votre idée, de votre
fonctionnalité, de votre stratégie. Une histoire chapitre après chapitre
donc avec une approche chronologique. Cette chronologie peut être
logique : je lance les invitations à ma grande fête après avoir trouvé
le lieu adéquat. Comme cette chronologie pourrait être basée sur une
importance, une approche stratégique : je décide de lancer une
communauté autour des chiens : on va d'abord proposer des balades en
commun, puis du gardiennage, puis du conseil d'expert, enfin on pourra
vendre des produits dérivés autour de cette communauté.

**Si j'exprime les choses sous la forme d'une histoire je clarifie ma
pensée, et je la communique bien mieux**. Les gens s'y projettent bien
plus, ils mémorisent bien mieux (22x mieux dit-on quand on est capable
de raconter des histoires).

Je commence donc par bâtir une colonne vertébrale qui s'articule autour
des chapitres de mon histoire. (Comme toujours dans ces ateliers : si on
oublie quelque chose on peut le rajouter ensuite). **Il faudrait pousser
la personne en charge à raconter son histoire à haute voix en parcourant
de gauche à droite ces chapitres**.

Les codes couleurs sont nos amis. On va essayer de définir une couleur
pour cette colonne vertébrale. Le blanc disons. Le plus simple, des
pages A4 coupées en deux. Soyons pratiques.

Je reprends ici mon "[peetic](/tag/peetic/)" fétiche : un site de
mise en relation de possesseurs d'animaux (chats & chiens) qui va me
permettre de vendre des produits dérivés. Mon histoire : "j'ai d'abord
un **possesseur** qui a donc un **animal**, il gagne du temps et se
socialise en faisant des balades, en faisant faire des **balades**, ou
en ayant un **gardiennage** adapté par échange (là il peut même partir
en vacances), enfin il est ravi car son animal se porte bien grâce aux
**conseils** prodigués par le service, ainsi avec nos inscrits on va
pouvoir commencer à faire de la **publicité** puis vendre par la
**boutique** des produits dérivés". Cette histoire émergera des *impact
mapping* que je vous présenterai plus tard. Elle aurait pu être plus
micro : "je parcours les produits sur le **catalogue**, je mets des
produits dans mon **panier**, je profite des **promotions**, je dois
m'inscrire en tant qu'**utilisateur**, puis je **paye**, enfin je
consulte mes **commandes** et mes **factures**".

{{< image src="/images/2017/01/usm1.jpg" title="User Story Mapping 1" width="800" >}}

## Étape 2 : Déterminez les composants de vos chapitres

Quand la première étape est achevée on prend chapitre par chapitre et on
essaye verticalement, en mode colonne, d'y placer tous les composants
nécessaires à la bonne réalisation du chapitre. C'est simple. Élément
par élément. Mais **la cartographie de plan d'action, le user story
mapping, c'est simple**. La piste noire en image plus haut c'est quand
les choses paraissent impossibles on les attaque étape par étape. Merde
je suis en train de citer une partie de la méthode de Descartes). Mais
là où je me détourne de Descartes : un petit ensemble des éléments
suffit à faire sens (on ne recherche pas l'intégralité, la complétude),
on commence par ce qui a le plus de valeur (et pas le plus facile dans
la Méthode du monsieur).

On utilise le code couleur jaune ? Des stickers ?

Ici c'est une petite cartographie du plan d'action, une petite *user
story map*, vous pouvez en avoir des très grandes... (toujours
[peetic](/tag/peetic.html)).


{{< image src="/images/2017/01/usm2.jpg" title="User Story Mapping 2" >}}

## Étape 3 : Les points de vigilance

Déterminer les composants des chapitres c'est souvent l'occasion de
faire participer tout le monde, l'occasion de discussions, de
réflexions, l'occasion de se dire à haute voix les points de vigilances.
Souvent des adhérences avec d'autres groupes, des inconnues. On ne va
pas les oublier, on va placer un sticker, un post-it quoi.

Le code couleurs ? Rouge ou rose, c'est quand même un point de
vigilance. Si le même point apparaît deux fois, il suffit de le stipuler
une seule fois, le plus à gauche, dans l'ordre de l'histoire.

{{< image src="/images/2017/01/usm3.jpg" title="User Story Mapping 3" >}}

## Étape 4 : Définissez un premier ordonnancement par valeur par colonne

Exercice plus difficile pour le porteur du besoin : on va désormais lui
demander de prioriser par importance, par valeur, les éléments à
l'intérieur de chaque colonne, colonne par colonne. Il devra malgré tout
tenir compte des dépendances : je ne vais pas envoyer mes invitations
avant d'avoir la liste des invités à ma méga soirée...quoique... en tous
cas j'ai d'abord besoin d'un début de liste avant d'envoyer quoi que
cela soit. Donc le ou les porteurs du besoin ordonnent par valeur (et
potentiellement il ou ils sont soumis à des dépendances) les éléments de
chaque chapitre. En haut les composants avec le plus de valeur, les plus
importants, ceux qui nous intéressent le plus, ce qui nous font le plus
apprendre.

À tout moment des éléments peuvent apparaître, disparaître, changer, au
fil de la conversation, pas de souci, vous les intégrez au bon endroit.

(cliquez pour agrandir)

{{< image src="/images/2017/01/usm4.jpg" title="User Story Mapping 4" >}}

## Étape 5 : Définissez un deuxième ordonnancement assez global

Maintenant on a une cartographie assez complète. Il est temps de
**donner du relief**. On va ajouter une colonne à gauche pour y placer
une échelle. Cette échelle devrait être l'importance, la valeur. On ne
peut pas envisager une échelle trop fine, on va donc imaginer trois ou
quatre niveaux. Vous pourriez appeler ces niveaux MoSCoW : Must
(obligatoire), Should (on devrait faire ça), Could (on pourrait faire
ça), Won't ou Would selon le contexte (Est-ce utile de faire ça ? On le
fait pas ? ou On aimerait faire ça mais est-ce important ?). Mais cela
peut bloquer certains porteurs du besoin : "Comment ça mais tout est
obligatoire" (oui oui c'est ça...). Vous pouvez alors appeler ces étapes
"lot 1, lot 2…", "étape 1", "étape 2", mais bon cela sous-entend une
complétude or il faut imaginer que nous ne fassions que le "lot 1" ou
l'"étape 1" et qu'elle pourrait se suffire à elle même. Essayez au mieux
on reviendra sur ces intitulés en fin de parcours grâce à
[Dragos](http://andwhatif.fr).

En tous cas : **a) c'est souvent un acte difficile pour les porteurs du
besoin, b) il devrait se dégager visuellement quelque chose
d'intéressant**.


{{< image src="/images/2017/01/usm5.jpg" title="User Story Mapping 5" >}}

## Étape 6 : Faîtes glisser vos porteurs du besoin dans l'inconfort

Dans la gestion produit, dans la gestion de nombreux projets, de
nombreuses activités, le monde complexe changeant dans lequel nous
vivons nous impose de valider rapidement nos hypothèses, car toutes ces
pondérations, priorisations, toute cette valeur, restent des hypothèses.
Des hypothèses à valider, et dans notre gestion du risque, de notre
effort, de nos investissements, à valider le plus rapidement possible.

Vous aurez remarqué que **les porteurs du besoin rechignent souvent à
descendre des éléments vers des priorités plus basses**. Quoi de plus
normal. Pourtant il faudrait **oser se mettre dans l'inconfort pour
valider rapidement**. Difficile de les pousser dans l'inconfort sauf si…
vous libérez l'espace pour une ligne horizontale, un nouveau niveau, au
dessus des autres, sur lequel vous pourriez écrire : "j'ai mal mais ça
marche", sous-entendu "ouhla c'est vraiment le minimum minimum je ne me
sens pas à l'aise mais effectivement cela validerait déjà mes premières
et plus importantes hypothèses, composants".

{{< image src="/images/2017/01/usm6.jpg" title="User Story Mapping 6" >}}

## Étape 7 : Définissez les histoires dans l'histoire

Pour que cette **mise en relief prenne encore plus du sens** j'ajoute
une dernière retouche, la [Dragos](http://andwhatif.fr) *'s touch*,
l'homme sens par excellence (des fois il devrait faire des trucs
incohérents pour voir). Essayez de transformer vos titres de colonnes de
gauche (Must, Should, ... Lot 1, Lot 2..., Étape 1, Étape 2...) en
**phrases racontant des petites histoires. Donnez de la cohérence à vos
sous ensembles**. Cela délivre une grille de lecture très intéressante
qui peut vous pousser à encore bouger quelques stickers/post-its. Pas de
souci.

Histoire \#1 : *ouverture avec connexion Facebook qui permet de
connaitre des balades ou du gardiennages entre possesseurs. Nous
récoltons nous mêmes les infos.*

Histoire \#2 : *on réintègre les utilisateurs dans notre système et on
permet la création dynamique d'offre communautaire de balades ou de
gardiennages*.

Histoire \#3 : *on lance les conseils d'expert et une première pub
(monétisation)*.

Histoire \#4 : *on étoffe et on lance la boutique*.

Histoire \#5 : *faut pas rêver on aura d'autres idées avant celles-ci*.


{{< image src="/images/2017/01/usm7.jpg" title="User Story Mapping 7" >}}

Attention la somme d'effort par étape n'est pas forcément la même ! **On
veut surtout dégager une approche tactique qui permette de découper en
histoires faisant sens et de petites tailles (surtout au début) pour
rapidement valider nos hypothèses**.

La prochaine fois je vous tord une cartographie de plan d'action, une
*user story map*, pour l'utiliser comme outil de *roadmap*, de
portefeuille projets, entre autres exemples.

## Les articles de la série

1.  [Cartographie de plan d’action : le User Story
    Mapping](/2017/01/cartographie-plan-action/)
2.  [Cartographie de plan d’action(user story map)
    revisitée](/2017/02/cartographie-plan-action-revisitee/)
3.  [Cartographie de stratégie, l’impact
    mapping](/2017/02/cartographie-strategie-impact-mapping/)
4.  [Cartographie de stratégie, l’impact mapping, hors des sentiers
    battus](/2017/02/cartographie-strategie-impact-mapping-hors-sentiers-battus/)

