---
date: 2022-07-16
slug: produit-de-demain-preambule
title: "Produit de demain, préambule"
draft: false
---

Aujourd'hui c'est hier, on le sent bien. On vit dans le monde d'avant. On parlait de "Great resignation" ces derniers temps, j'ai plutôt l'impression de "La grande réalisation", dans le sens où tout le monde, moi le premier (ou le dernier), semble réaliser l'importance de notre écoresponsabilité. Pour un acteur du produit digital le paradoxe est douloureux, tout dans notre monde tourne autour de la technologie, rien de la technologie ne semble aller dans le sens du monde. Et pourtant les produits façonnent notre monde. La question des produits écoresponsables est donc cruciale. La bonne nouvelle ? Émergence, pensée complexe, approche systémique sont les outils qui nous permettent, **aujourd'hui**, d'avancer sur la résolution de notre problème écologique. Et cette façon de penser nous y sommes entrainés.  

Avant (dans un prochain article) de brosser les axes de réflexion qui m'habitent sur le produit écoresponsable, j'essaye de tracer ce qui a été clef ces dernières années à mes yeux. Pourquoi ? Pour savoir d'où je viens, pour mieux savoir où je veux aller. Ainsi avant de s'interroger sur le *produit de demain*, trois regards : disruption, expérience et valeur. 

Je situe la période d'avant, celle d'hier et malheureusement encore celle d'aujourd'hui, de la fin des années 90 à maintenant. Pourquoi la fin des années 90 ? On a basculé dans un autre monde. C'est probablement lié à la diffusion d'internet qui nous a fait entrer dans une autre période, un autre type d'industrie, un autre type d'entreprise, un autre type d'innovation, une autre façon de travailler. Ça coïncide aussi sans surprise avec ces fameuses approches agiles. 

De cette période on ne peut pas tout retenir et je vais me focaliser sur les deux choses qui me paraissent significatives. Deux phénomènes. Le premier c'est celui de la disruption, le second c'est celui de l'expérience. 


# La disruption

La disruption c'est quoi ? C'est de tout temps, quand Ford propose une voiture face à un cheval, c'est une disruption, un changement, une rupture. Pareil quand Fosbury fait son saut en hauteur. Il y a un livre qui a fait pas mal parler de lui à la fin des années 90 (justement), de Christensen, et qui s'appelait : "le dilemme de l'innovateur" (face à la disruption). Quel est ce dilemme de l'innovateur face à la disruption ? On pourrait comprendre une crainte. On a beaucoup parlé de Kodak, de Yahoo, de plein de marques assez symboliques et qui ont été « disruptées" comme on dit. On parle aussi de disruption avec des changements complets de paradigme des marchés, l'uberisation est une forme de disruption. On perd soudainement un marché il y a une rupture dans les usages, Uber, AirBNB. Ainsi « disruption" fait peur autant que cela excite. 

Mais dilemme ? Quel serait-il ? Premier point, nous explique Christensen, quand vous avez un produit il vit une courbe en S. Cela démarre doucement le temps que vous trouviez votre marché, et puis cela s'accélère, puis vous êtes en croissance, vous êtes puissant, puis à la fin on retrouve la courbe du S qui vous ramène sur un plat. Ce produit aura donné ce qu'il a eu à donner. Et ainsi l'effort attendu dans la gestion du produit intervient au moment de cette pleine puissance : il faut penser à déclencher un renouvellement du produit, une relance du produit avec une nouvelle version, des améliorations, des changements, ce qui fait que quand vous allez atteindre le zone plate de cette version du produit, la fin de la croissance, vous allez avoir la version suivante qui va arriver. Elle aura démarré doucement dans l'ombre de la version précédente, et elle sera à plein régime quand la première s'épuisera. Et donc vous enchainez les versions avec une amélioration constante de votre produit, et vous continuez à surfer sur une pleine puissance. C'est ce que l'on recherche (dans le monde d'hier et d'aujourd'hui). Avec tout cela on entretient son marché, on est bien avec son marché. Mais on est aussi hypnotisé par son marché : quand on est en pleine puissance, avec autant d'attente difficile de regarder ailleurs.


{{< image src="/images/2022/07/conventional-technology-s-curve.jpg" title="Approche conventionnelle" >}}


Quand vous êtes en train de nourrir ce marché, avec votre produit, vous n'êtes pas en train de nourrir le futur marché de demain, de penser le produit de demain. Et pendant que vous êtes hypnotisé par votre base installée, par votre puissance, par le fait d'enrichir votre produit (et vous même par la même occasion),  il y a des petites entreprises à côté qui se créent des niches avec une approche complètement autre. Et quand leurs niches grandissent et arrivent à leur tour à avoir de la puissance et elles peuvent provoquer une rupture sur le marché et vous rendre soudainement obsolète. Peu importe la façon dont vous avez enrichi et maintenu votre produit. 

{{< image src="/images/2022/07/disruptive-technology-s-curve.jpg" title="Approche disruptive" >}}


Et ainsi le dilemme apparaît : est-ce que je dois vivifier, enrichir mon produit, ou est-ce que je dois l'oublier, et préparer d'autres produits qui eux seront à pleine puissance plus tard, mais qui aujourd'hui n'ont peut-être pas de lien, ni avec ma base clients, ni avec mon image. Le dilemme de travailler avec son énorme base installée, ou de s'intéresser au peu nombreux *early adopters* d'un nouveau produit. 

(Notez que cette problématique semble déjà très liée avec nos interrogations sur les produits de demain).  

Faire grandir et fructifier mon produit, ou me projeter sur une rupture. Vous pourriez faire les deux. 

Vous pourriez avoir des politiques de rachats. Vous pourriez racheter des entreprises qui elles ont pris le temps de monter un marché sur des produits complètement disruptifs auquel je n'étais pas préparé et qui transforme mon marché sans ressembler à mon produit. Mais racheter ce n'est pas si simple, et c'est interdit par la loi justement pour laisser du souffle à l'innovation (la loi antitrust américaine par exemple). Pour rappel Zuckerberg qui dit "Je m'achète du temps en achetant Instagram " (Écouter : ["Peut-on encore croire aux bienfaits de l'innovation - Radio France"](https://www.radiofrance.fr/franceculture/podcasts/entendez-vous-l-eco/peut-on-encore-croire-aux-bienfaits-de-l-innovation-2766858)). 

Et racheter cela n'entraine pas du tout votre muscle de la réinvention. Car vous l'avez saisi d'un côté on parle de renouvellement, de l'autre de réinvention. Le dilemme est la tentation de simplement se renouveler car on garde sa position, sa puissance. Se réinventer est tellement plus déstabilisant. 

Ces ruptures, ces disruptions, nous les avons pas mal observés dans ces années de grandes complexités, la fameuse époque VUCA. Et aussi parce que les technologies par la puissance de la technologie de remettre en jeu bien des marchés. Il n'y a que très peu de disruption non technologique, et tout acteur qui ne pense pas technologie prend le risque de ce voir exclure de son marché. C'est l'un des mojos de Marty Cagan dans "Empowered" : "Pas de disruption produit sans disruption technologique". 

# L'économie de l'expérience  

Autre regard : l'économie de l'expérience, le produit "expérientiel". 

Il y a ici aussi une sorte de texte fondateur à la fin des années 90 (tout comme cette agilité qui émerge aussi là), sur l'économie de l'expérience. L'économie de l'expérience est une notion mise en évidence par James Gilmore, et qui nous dit qu’ aujourd'hui les produits les plus puissants sont des expériences. Voir : [Welcome to the experience economy, James Gilmore](https://hbr.org/1998/07/welcome-to-the-experience-economy)

Il donne un exemple très simple : avant vous achetiez des fraises pour fabriquer votre gâteau, ça ne coûtait pas très cher, vous achetiez des fraises et de la farine puis vous faisiez votre gâteau. Et puis plus tard vous avez plutôt acheté directement le gâteau à la pâtisserie, ça vous coûte plus cher, mais c'est plus pratique et avec le temps les gens ont acheté les gâteaux plutôt que les faire eux-mêmes. Tout cela c'est pour l'anniversaire de vos enfants. Et puis est venu le service. Vous vous faisiez livrer le gâteau le jour de l'anniversaire, pendant l'anniversaire. Ça coûte plus cher, mais c'est encore plus pratique. Et puis il y a une sorte d'*entertainement*, de sensations, d'émotions qui vient en plus s'ajouter à cela qui est que vous voulez vivre une expérience et plutôt que de faire l'anniversaire chez vous vous amenez les enfants dans un lieu fait pour les anniversaires dans lesquels il y a un clown, il y a des jeux, il y a de la musique, il y a des danses, il y a des activités, il y a un gâteau, mais le gâteau est très loin d'être la chose la plus importante, et ça coûte beaucoup plus cher. Et tous les enfants vont réclamer votre produit, cette expérience. 

Pourquoi beaucoup d'entreprises sont friandes de l'expérience, car soudainement on bascule une relation qui est beaucoup renforcée, beaucoup plus dense avec le client que l'on appelle dès lors invité. C'est lui qui vient, ses émotions, sa sensibilité, qui va avec c'est la sienne. Et ainsi sa mémorisation est bien meilleure, et ainsi l'invité revient beaucoup plus souvent, il est bien mieux fidélisé. Le service devient une commodité, les commodités ne captent pas.  

"Les entreprises doivent aménager des situations marquantes pour leurs clients, et l'expérience est une tentative de construction de relations denses avec eux" nous dit **Jean-Louis Frechin ("Le design des choses à l'heure du numérique")**. 

Le slogan de IBM c'était "IBM means service", celui de McDonald cela a été "Venez comme vous êtes".         

{{< image src="/images/2022/07/economic-distinctions.jpg" title="Distinctions économiques" >}}


En musique avant j'achetais un disque, après j'ai eu un service de streaming, et puis je pourrais dire je vais à un concert. D'ailleurs aujourd'hui la musique ne coûte plus rien, en m'abonnant pour 15 euros par mois j'ai tous les disques de la terre, et le disque n'existe plus (on va parler des vinyles après). Mais si je vais à un concert de Bruce Springsteen comme il va y en avoir un à Paris, la plus mauvaise place c'est 170 euros, une année de musique en ligne et la meilleure place c'est 720 euros, cinq à six années de musique en ligne. Pourquoi ? Par l'expérience, c'est la mienne j'y suis, c'est unique, mémorable. Mais si je devais rendre expérientiel mon outil de streaming en ligne de musique ? Deux façons nous dit James Gilmore : **la participation et la connexion**. Avec qui je partage ma musique, quelles sont les musiques qui me sont proposées à moi et à moi seul parce que c'est mes goûts. Je reviens au vinyle, car désormais un disque se vend plus cher qu'un mois de musique en ligne. Mais parce que c'est une communauté privée à laquelle vous appartenez. Vous êtes prêt à dégrader certains aspects de la musique (tourner le disque toutes les vingt minutes) parce que cela implique des sensations physiques, la main, une sorte moment sensuel comme la cérémonie du thé, et l'appartenance à une caste de sachants, ceux qui savent où le son est le meilleur. Pareil avec le son 3D dans les oreilles. Donc le prix que vous mettez (et c'est ça aussi l'impact de l'expérience personnalisée) dit des choses sur vous. Je paie pour une musique qui me ressemble. J'ai quitté Spotify pour Tidal pour suivre Neil Young. On est dans l'émotion, dans la mémorisation, dans les convictions. Avec Tidal je paie mieux les artistes me dit-on.       


Là aussi les technologies jouent un rôle fondamental. Avant nous faisions des objets. Puis, avec l'industrie on les a fabriqués en série, puis avec le numérique on a réussi à les cloner à l'identique, donc plus qu'en série, à l'identique, pour enfin avec cet internet des objets on atteint une sorte d'ubiquité, le multicanal, c'est partout (Là aussi lire "Jean-Louis Frechin, Le design des choses à l'heure du numérique"). Ça change complètement nos usages, d'où l'idée que la grande majorité des disruptions sont technologiques, et cela permet une expérience renforcée, c'est autour de nous, nous sommes au milieu, invité. 

## Conclusion

À ces deux phénomènes, vous pouvez ajouter l'importance du focus valeur. 

Car nous nous sommes vite rendu compte que pour aller vite, il ne faut pas aller vite, mais aller droit au but. 

Pour aller droit au but, on s'est demandé pourquoi on faisait les choses, ce que l'on voulait, ce que l'on recherchait. 

Et puis finalement de façon vertueuse quand on s'est demandé ce que l'on recherchait vraiment, quelle valeur on désirait, les premières solutions évidentes sont apparues comme des précipitations erronées. En se demandant ce que l'on voulait on a découvert que l'on avait des idées puissantes sur comment y aller de façon totalement inattendue. 

Ainsi est venu tout un travail sur la maximisation de la valeur et la minimisation de l'effort. Et avec, un travail de découpage, d'accélération des cycles entre l'hypothèse et la validation. 

Mais voilà tout ceci c'est le monde d'avant, d'hier et malheureusement encore d'aujourd'hui. 

Pour faire de demain aujourd'hui il parait évident aux yeux de tous que les choses doivent changer. Mais nous sommes dans une société de consommation. Société de consommation dont je sais très bien profiter. Cependant je ne suis pas sourd et aveugle. Le produit façonne nos vies, pour reprendre une expression de Jean-Louis Frechin. Et par conséquent il parait évident que nous allons devoir changer nos produits. 

[La suite](/2022/09/produit-de-demain/)
