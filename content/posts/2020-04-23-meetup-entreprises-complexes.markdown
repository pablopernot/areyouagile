﻿---
date: 2020-04-23
slug: meetup-entreprises-complexes
title: Meetup Entreprises complexes 
---


Je profite de ces jeudis confinés pour passer en revue des contenus en ajoutant la parole sur le texte. Je vous propose de reprendre aujourd'hui le meetup sur les entreprises complexes proposé avec [Laurent Morisseau](http://www.morisseauconsulting.com/) en 2019, et que j'ai pu creuser un peu à la conférence Flowcon 2019. 

Ici il s'agit d'un aide-mémoire, d'un accès aux articles et pdf utilisés lors des meetups et des ateliers, du fil rouge du meetup. 

## La vidéo


{{< youtube id="e9c4hu_lg-Y" >}}

## Les entreprises complexes ? 

Je vous rappelle notre "mojo" : 

Il existe aujourd’hui un juste milieu entre un accompagnement totalement émergent ou le contextuel règne sans partage, et une démarche avec des méthodes simplistes de type “recette”. Nous ne souhaitons pas le tout émergent afin de ne pas se poser toutes les questions, mais les bonnes questions, réduire le champ des possibles. Nous réfutons les recettes simplistes : il faut se poser des questions. ​

Notre démarche est intermédiaire, entre la carte de métro entièrement codifiée où il suffit de cocher des cases sans réfléchir, et la carte “terra incognita” où tout semble à redécouvrir et à réinventer, nous plaçons le symbole l’astrolab : cet instrument donne des directions, des distances, en topographie permet de mesurer des distances, aide au relevé de plan, dans les grandes lignes.

Nous cherchions un instrument qui donne des directions, mais sans définir complètement la carte ni prétendre connaître a priori le territoire.

* [Slides entreprises complexes](/pdf/entreprises-complexes/slides-entreprises-complexes.pdf)


## Focus sur les archétypes

{{< image src="/images/2020/04/archetypes.jpg" title="Archétypes" >}}

* [Slide archétypes / tableau](/pdf/entreprises-complexes/archetypes.pdf)
* [Slides archétypes / détails](/pdf/entreprises-complexes/slides-archetypes.pdf)

## Focus sur une grille de lecture, les niveaux logiques de Bateson/La pyramide de Dilts

{{< image src="/images/2020/04/pyramide-de-dilts.jpg" title="Pyramide de Dilts" >}}

C'est souvent au niveau supérieur que l'on règle les dissonances du niveau auquel se situe le problème. Un exemple avec benext 2019. 

{{< image src="/images/2019/04/pyramide-dilts-3.jpg" title="Niveau supérieur" >}}

## Mettre en musique, l'atelier

Il s'agit de : 

* Percevoir l'archétype de l'entreprise ?
* Quels sont les symptômes ? 
* Est-il fonctionnel ou dysfonctionnel ? 
* Faut-il une revitalisation ou une réforme ? 
* En fonction de son archétype quel cycle de vie est-il proposé ? 
* Quelles sont dès lors les propositions d'actions à mener ? 

### Fiche atelier

{{< image src="/images/2020/04/atelier-flowcon.jpg" title="Atelier flowcon" >}}

### Focus cycle de vie 

{{< image src="/images/2020/04/cycledevie.jpg" title="Cycle de vie" >}}

* [Intitulé des problématiques pour l'atelier](/pdf/entreprises-complexes/flowcon2019.pdf)

## Sources

* [La première partie sur les entreprises complexes](/2020/04/meetup-entreprises-complexes/)
* [La deuxième partie sur les entreprises complexes](/2020/04/meetup-entreprises-complexes-2/)
* [Wikiversity sur Mintzberg](https://fr.wikiversity.org/wiki/Structure_et_management_des_organisations/La_typologie_des_structures_d%27organisation_par_Mintzberg)
* [HRMag sur Mintzberg, 2019](https://www.hrimag.com/Les-6-parties-de-base-de-l-organisation-selon-Henry-Mintzberg)
* [Les formes organisationnelles selon Henry Mintzberg, Alain Dupuis, Richard Déry](/pdf/formes-organisationnelles-mintzberg.pdf)
* [Un modèle pour l'entreprise agile, par Laurent Morisseau](http://www.morisseauconsulting.com/2019/09/28/entreprise-agile/)
* [Les entreprises complexes, partie 1](/2019/09/modele-des-entreprises-complexes-partie-1/)   
* [Les entreprises complexes, partie 2](/2019/09/modele-des-entreprises-complexes-partie-2/) 
* [Pyramide de dilts et harmonisation de l'organisation](/2019/04/pyramide-dilts-harmonisation-organisation/)  
* [Slides du meetup 2019](https://pablopernot.fr/pdf/meetup-modele-entreprises-complexes.pdf)
* [Fichier PDF : Toute ressemblance... sur la pyramide de Dilts chez BENEXT en 2019](/pdf/toute-ressemblance.pdf)
