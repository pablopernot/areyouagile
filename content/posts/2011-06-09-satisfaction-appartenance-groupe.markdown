---
date: 2011-06-09T00:00:00Z
slug: appartenance-a-un-groupe-et-niveau-de-satisfaction
title: Appartenance à un groupe et niveau de satisfaction
---

Je reviens sur cet excellent article de Mary Poppendieck "[Before there was management](http://www.leanessays.com/2011/02/before-there-was-management.html)"qui
met en évidence certains chiffres clefs : le 150 de Robin Dunbar, le 100
magique de Steve Jobs. Et aussi le niveau de satisfaction d'appartenance
à un groupe selon la taille de ce groupe issu des recherches
de Christopher Allen. Tout l'article -en anglais-est passionnant et je
vous encourage à le lire mais c'est surtout sur ce dernier point que je
veux brièvement revenir. Mary Poppendieck place un graphe qui résume
cette satisfaction en rapport avec la taille du groupe.

{{< image src="/images/2011/06/Double-peak.jpg" title="Peak" >}}

On y note deux crêtes "positives" dont la première qui correspond de
façon franche avec la taille attendue d'une équipe agile. Il n'y a pas
de secret (ni de hasard !!!) le "peak" est à ...7. Je pense cependant
que le graphique proposé  et notamment l'échelle sur l'abscisse ne met
pas vraiment en exergue le risque à avoir une équipe trop grande. Que la
pente est bien plus glissante qu'il n'y parait et le regain de
satisfaction plus long à venir. Voici donc le graphisme mis à jour avec
le support et soutien indéfectible de Edouard G.

{{< image src="/images/2011/06/group_satisfaction.jpg" title="Group Satisfaction" >}}

Pensez donc à organiser vos équipes selon ces tailles là ! I can't get
no ! Satisfac...

update :
[Ici](http://www.fabrice-aimetti.fr/dotclear/index.php?post/2011/06/09/Avant-qu-il-existe-un-management)
la traduction de l'article de Mary Poppendieck par Fabrice Aimetti.
Merci à lui.

 
