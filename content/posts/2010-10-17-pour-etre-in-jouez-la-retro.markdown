---
date: 2010-10-17T00:00:00Z
slug: pour-etre-in-jouez-la-retro
title: Pour être 'in', jouez la 'retro'
---

Pour ceux qui ne seraient pas encore au courant à la fin d'un sprint
agile on fait (on doit faire ! on devrait faire !) la rétrospective. Son
but : mettre en évidence dans le contexte de l'itération que l'on vient
d'achever : ce qui a marché, ce qui n'a pas marché, ce que l'on pourrait
améliorer.

En voici une (il s'agit d'un mix d'extraits entre plusieurs clients, que
j'ai anonymisés), histoire de rendre les choses concrètes. Vous pouvez
la juger, la dénigrer, l'utiliser, etc.

Comment : on regroupe toute l'équipe (équipe, scrummaster, product
owner). On fixe 4 grands axes : "les plus", "les moins", "les
optimisations à envisager", "le plan d'action". Le plan d'action étant
constitué par les optimisations que l'on souhaite mettre en oeuvre dès à
présent, ou qui concerne un évènement pour le prochain sprint. Presque
une antichambre : si nous envisageons régulièrement la même optimisation
il faudra se décider à la basculer dans "plan d'action". De même on peut
rapidement glisser dans "les moins" les éléments du plan d'action que
l'on aura finalement pas ou mal mis en oeuvre. Chacun évoque les points
qu'il souhaite mettre en évidence. Je n'interviens (en tant que
ScrumMaster) qu'à la fin, mais j'ai mon mot à dire et si je peux
influencer dans tel ou tel sens je n'hésite pas mais je ne tranche pas.

Mes commentaires en italiques.

## Les plus

-   Backlog et stories plus complets *(Stories plus complètes : au
    niveau des tests d'acceptation et/ou des scénarios de validation)*
-   Réorganisation de l'espace, salle, post-it, etc. *(chez ce client un
    espace dédié a été aménagé pour l'agilité durant le sprint).*
-   Motivation de l'équipe *(se passe de commentaire, mais j'explique
    qu'il est normal avec Scrum d'avoir des équipes plus motivées. Par
    contre il est essentiel de faire signe quand la motivation n'est
    plus au rendez-vous, et surtout pourquoi. Tout le monde ne peut pas
    être motivé 100% du temps.)*
-   Intégration des tests unitaires dans la notion de fini *(se passe de
    commentaire)*

## Les moins

-   Mauvais découpage des stories et des tâches *(trop de dépendances
    !)*
-   Ponctualité *(se passe de commentaire)*
-   Un expert externe peut interférer sur le périmètre du sprint sans
    faire partie de l'équipe. *(il faudra résoudre ce point si celui-ci
    se répète, c'est un point de vigilance)*
-   Cadence pas assez régulière (coup de collier sur la fin). La
    validation des stories doit être régulière. *(se passe de
    commentaire, un point du plan d'action est dédié à ce problème)*
-   Difficulté à concilier le focus du sprint et les demandes externes
    *(il faut protéger cette équipe)*

## A envisager/optimiser ?

-   Intégrer la revue de pair dans la notion de fini *(c'est à dire que
    dans ce cas elle s'applique sur toutes les stories).*
-   Meilleurs jeux de données.
-   Plus/Mieux utiliser des normes de codage/développement/nommage.
    *(XP...le pair programming va suivre...)*

## Plan d'action

-   Validation régulière des stories (pas de coup de collier final)
    *(sur le prochain sprint -3 semaines- on se fixe les premières
    validations sur la seconde semaine)*
-   Chaque membre de l'équipe annonce aux autres au début du sprint le
    nb de jours qu'il estime pouvoir accomplir sur le projet *(des
    soucis d'estimation liés notamment au temps "réel" passé sur le
    sprint)*
-   Chart température équipe *(un des participants sort d'une formation
    Scrum, on lui a parlé d'un chart décrivant la courbe du moral des
    membres de l'équipe, il souhaite le mettre en oeuvre. je vais lui
    passer le relais en tant que ScrumMaster pour prendre du recul.
    C'est sa première "mission" de ScrumMaster : établir ce chart et
    surveiller le moral des troupes)*
-   préparation en amont de l'arrivée d'un nouveau membre de l'équipe
    *(annoncée le mois prochain il faut préparer cette arrivée, la
    dernière fois on a perdu un temps fou !)*

Voilà. La rétrospective dure une heure max. J'affiche au mur (radiateur)
uniquement le plan d'action. Naturellement plus celui-ci sera concis
plus il sera suivi *\[révision 2012 : le plan d'action ne devrait
comporter qu'un seul item !!!\]*.

