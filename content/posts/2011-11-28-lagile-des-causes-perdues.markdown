---
date: 2011-11-28T00:00:00Z
slug: lagile-des-causes-perdues
title: L'agile des causes perdues
---

C'est l'automne, les vidéos se ramassent à la pelle. Celle-ci est la
table ronde de clôture de l'agile tour Montpellier 2011. J'ai eu le
plaisir d'interroger sur leurs pratiques agiles plusieurs représentant
de sociétés basées en Languedoc-Roussillon : Messieurs Cyrille Le Roux
(DSI Courrier / La Poste), Christophe Champion (IOcean) et Grégoire de
Jabrun (La Compagnie du Vent). Je parle de l'agile des causes perdues,
pour en savoir plus il faut regarder la vidéo !

[VIDEO](https://video.umontpellier.fr/agile-tour-2011-a-lum2-table-ronde-et-cloture-2/)

<video src='https://video.umontpellier.fr/wp-content/uploads/2011/11/conf_agile_tour_table_ronde_cloture_v2.mp4' width='580' controls='controls' poster='https://video.umontpellier.fr/wp-content/images/vignettes/image-9923.jpg'></video>

Cette vidéo a été enregistrée par la télévision/web de l'école Polytech
qui a hébergé ce premier agile tour Montpellier. 
