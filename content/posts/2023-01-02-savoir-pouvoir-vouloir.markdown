---
date: 2023-01-18
slug: savoir-vouloir-pouvoir 
title: "Savoir, vouloir, pouvoir"
draft: false
---

J'ai écrit et réécrit cet article. Franchement écrire sur ChatGPT ça m'a paru intelligent au début, me questionner. Et puis devant la litanie, la déferlante, cela a commencé à m'apparaître comme complètement inutile. D'autant qu'au fil du temps le retour a été : il sait faire quantité de réponses justes, mais insipides à l'image du flot de contenu de basse qualité qui inonde nos médias. Peut-être, pour faire appel au concept de Nassim Nicholas Taleb, que cette intelligence artificielle a une défaillance majeure : elle  n'a aucun *skin in the game*, aucune de ses réponses ne l'engage elle. 

Je reprends cependant cet article pour mettre en évidence la différence entre savoir, vouloir et pouvoir qui me semble apparaître de façon flagrante autour de ChatGPT, et qui fait écho à nos problématiques autour des enjeux de civilisation que nous vivons. 

Alors oui tout le monde en parle, de l'intelligence artificielle ChatGPT. Je n'ai pas essayé, et je n'essayerai pas pour des questions de convictions (Peter Thiel, Elon Musk, comme investisseurs initiaux, une volonté d'être opensource rapidement oubliée et clôturée, des investissements de microsoft). 

Mais je ne suis pas aveugle cela va avoir un gros impact, notamment sur mon travail de manager, de leader, de coach. 

## Savoir

Ce que cette intelligence artificielle démontre à mon avis ? 

Que savoir n'est souvent pas le souci. Je ne doute pas qu'elle puisse nous dire avec assez de détails comment organiser, réorganiser, communiquer telle ou telle organisation. Qu'elle puisse modéliser les systèmes compliqués et complexes et nous dire les impacts globaux de chaque petit mouvement, ou les répercussions, ou la vitesse de propagation des habitudes, des savoirs, etc. Elle mettrait en évidence toute la dynamique des dépendances, des adhérences, etc. 

Elle pourrait nous permettre de jouer des simulations dans des systèmes complexes au fil de l'eau, au rythme de l'émergence. 

Mais savoir n'a jamais été fondamentalement le souci principal du management, du leadership, du coaching. 

Nous observons depuis de nombreuses années que les organisations, les associations, les entreprises, pour la majorité, vont moins vite que nos connaissances sur comment organiser, comment manager, etc. Et c'est normal. 

## Vouloir 

Mais est-ce que nous voulons ce que nous savons ? Et est-ce que nous savons ce que nous voulons ? (Vous avez deux heures). 

Jusqu'à présent ce n'est pas savoir qui freine les organisations, c'est la clarté dans le vouloir.  

Pour qu'une intelligence artificielle puisse nous répondre encore faut-il que nous lui posions les vraies questions qui nous taraudent, et que nous la nourrissions des "vraies" informations. 

On comprend déjà que de parler de "vraies" informations est un problème en soi. Ce qui est vrai pour l'un, l'est-il pour l'autre ? 

Sommes-nous prêts à lui dire : ok propose une réorganisation, mais n'oublie pas mon plan de carrière ou ma rémunération ? Est-ce que l'on veut expliquer à l'intelligence artificielle que Mireille veut le poste de Jean, mais que Cyril est en embuscade pour prendre le poste sauf Antoine rate son projet en cours. 
Et ça c'est le point de vue d'une personne dans le système, l'autre dirait peut-être autre chose. 

Cela soulève plein de questions politico-philosophiques : je veux me réorganiser pour quoi ? De la valeur ? Définissons valeur. Valeur au niveau individuel, de l'équipe, de l'organisation, d'un ensemble d'organisations, d'une civilisation ? Les formes vont complètement changer. 

L'intelligence artificielle répond à une question. Une question veut résoudre quelque chose. 

Les réponses proposées à la résolution d'une question m'apparaissent forcément politisées.    

Pour redevenir plus terre à terre, dans nos accompagnements ou dans le management, le problème n'est généralement pas de savoir, mais de clarifier ce que l'on veut, pourquoi on le veut, vers quuoi on souhaite aller. J'énonce une lapalissade ? Oui, mais je vous indique que l'intelligence artificielle ne saura pas résoudre cela pour nous.  

## Pouvoir

Plus évident, vouloir n'est pas nécessairement pouvoir. 

On retrouve ici toutes les problématiques habituelles : conduite du changement, communication, émergence, adaptation, etc, je ne vais donc pas m'étendre sur la question. Juste rappeler encore que vouloir n'est pas pouvoir. Et que nous l'avons vu savoir n'est pas vouloir, ni donc pouvoir. 

## Politique

Alors qu'une intelligence artificielle nous donne des réponses hors sols, ou théoriques (et j'aime la théorie), très bien. Qu'elle sache passer probablement effectivement haut la main les certifications de coaching, tant mieux. Que ses propositions ne l'engagent en rien (pas de *skin in the game*) et qu'elle débite des citations de Gandi du meilleur aloi, encore mieux. Une ribambelle de coachs et de managers peuvent effectivement s'en inquiéter. 

Pour l'instant ce qu'elle met en évidence c'est que ses réponses sont forcément une interprétation d'une vision de la civilisation. On devrait se demander quelle vision ? Quelle civilisation ? 

Mais n'est-ce pas toujours la question que nous nous posons au final ?   
