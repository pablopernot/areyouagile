﻿---
date: 2020-10-22
slug: triangulation
title: "Triangulation"
---

On vient de me poser la question "que pensez-vous d'une direction produit qui cherche à commencer des sujets plus qu'à les finir pour essayer le plus de chose possible ?". On m'avait interrogé sur "comment rapidement faire passer une équipe dans la phase de *storming* pour vite atteindre le *norming*[^1] ?". 

[^1]: C'est une référence aux phases de [Tuckman](https://fr.wikipedia.org/wiki/Bruce_Tuckman).  

À toutes ces interrogations la sagesse veut que l'on réponde "cela dépend du contexte". Cette sagesse n'est pas le fruit du hasard, ou une pirouette pour éviter les réponses. Même elle peut laisser souvent nos interlocuteurs sur leurs faims. En fait c'est surtout un *leitmotiv* pour nous rappeler combien le contexte, le système en place, est important, et qu'une approche ou qu'une solution qui fait sens pour l'un, n'est peut-être pas faite pour l'autre. 

Ainsi d'une part il est préférable de ne pas extraire un élément de son tout. 

D'autre part il est préférable de ne pas articuler une conversation sur un moyen, qui n'est que le véhicule, mais vers un but. 

Sur l'exemple d'une équipe que l'on souhaite vite voir avoir une dynamique de groupe (*forming*, *storming*, *norming*, potentiellement *performing*). Si le but de ce moyen (l'équipe) c'est réussir une campagne marketing pour la sortie d'un produit : à mes yeux le mieux serait préférable de directement travailler sur le but (réussir cette campagne marketing) en sachant que cela aura un impact sur la dynamique d'équipe et l'adresser dans ce contexte. Si l'équipe est tout juste créée, elle se forme pour réussir la campagne marketing. Autant que l'étape de formation s'articule autour de ce but. Si l'équipe est dans la phase où cela grince (*storming*) qu'elle a besoin de se calibrer autant que ce calibrage vers un bon fonctionnement (*norming*) se réalise autour de ce but (réussir la campagne marketing). X et Y se disputent sur leurs périmètres qui se juxtaposent au regard du but à atteindre, et c'est en fonction de ce but à atteindre que la bonne solution sera trouvée pour articuler cette dynamique. On va donc éviter les phrases : "l'équipe ne s'entend pas" ou "X et Y laissent un abîme entre eux" pour plutôt : "l'équipe ne s'entend pas pour réussir cette campagne marketing" ou "Il y a trop d'incompréhensions entre X et Y pour la bonne mise en oeuvre de cette campagne marketing". Cela ancre la solution dans le contexte, dans la bonne intention : réussir cette campagne marketing. Et on perçoit que le problème n'est pas un problème fondamentalement de personne mais de situation, de système. 

### Triangulation

On me dit que cela s'appelle la triangulation. On ne parle pas à l'équipe de l'équipe, on parle à l'équipe de l'équipe au regard de l'objectif. Cela changent beaucoup les dialogues. Cela les ancrent. Cela matérialise le contexte. Il n'y a pas de bonne ou mauvaise équipe, il y a des bonnes ou mauvaises équipes au regard d'un objectif à atteindre. La conversation que vous avez avec elle devrait intégrer cet objectif (une triangulation, vous, le sujet, la cible). 

J'ai pris l'exemple de l'équipe, c'est vrai avec tout. Ce produit est bon pour ce que l'on veut lui faire faire. Il n'est pas juste bon. "Comment est-ce qu'on l'améliore pour qu'il fasse encore xxx ?"  et pas "comment est-ce qu'on l'améliore ?". Cela ne vous paraît peut-être qu'un jeu de sémantique, de syntaxe, mais toutes nos réponses, nos réflexions sont conditionnées par la forme que l'on emploie. L'univers est structuré par la façon dont on le décrit.  

### Changement de posture

Naturellement cela peut permettre de remettre en jeu le moyen : a) "J'aimerais que cette personne soit plus ferme", b) "J'aimerais que cette personne soit plus ferme pour faire délivrer plus régulièrement ses équipes" c) "Il faut accompagner cette personne pour l'aider à faire délivrer plus régulièrement ses équipes".            
Cela change la posture : je ne souhaite pas faire passer cette équipe de *storming* à *norming*, je souhaite faire avancer cette équipe vers la réussite de cette campagne marketing (et je sais que cela passera probablement par des phases de *storming* et *norming* pour lesquelles nous agirons en conséquence). Ces phases seront d'autant mieux surmontées que l'on ne cherchera pas à régler les problèmes de l'équipe mais on cherchera à régler les problèmes de l'équipe au regard de la réussite de cette campagne marketing. C'est beaucoup plus concret et moins prétentieux. Et aussi moins culpabilisant. Cela rend les choses actionnables.  

### Contexte et intention 

Contexte et intention sont trop souvent absents des conversations. 

"Que pensez-vous d'une direction produit qui cherche à commencer des sujets plus qu'à les finir pour essayer le plus de chose possible ?". Cela dépend du contexte, quel est le but ? Quel est le contexte, quelle est l'intention ? On trouve les réponses à l'intention en fonction du contexte. 

Attention à vos tournures de phrases. J'ai repris récemment quelqu'un lors d'une intervision : 
* Lui : "d'accord alors au lieu de dire *il faut faire comment cela*, je dis *je préconise ça*." (genre tu joues sur les mots Pablo). 
* Moi : "Non, *je préconise cela* a la même brutalité que *il faut faire comme cela* ; plutôt : *je préconise cela au regard de l'objectif que nous voulons atteindre* (décrire l'objectif) *dans le contexte qui est le notre* (décrire ce qui est nécessaire du contexte) et éventuellement : *pour telle raison* (décrire la raison)". 

C'est long ? C'est pompeux ? Non. D'une part ce n'est pas si long si vous savez bien conceptualiser ou vulgariser ou synthétiser. Sinon il faut apprendre c'est important : vous savez "si tu ne sais pas dire à un enfant de 8 ans ce que tu veux c'est que tu ne sais pas ce que tu veux" -- Einstein). D'autre part vous rendez palpable le système, le contexte, le paysage avec vos propos dans lequel toutes ces actions s'incrivent. 

Je ne sais pas si cet article peut vous être utile, mais cette triangulation je l'évoque constamment dans mes coachings ou mon mentoring. D'ailleurs si vous voulez ce genre de conversations dans votre *contexte* n'hésitez pas : [mentorat](/mentorat/).  



