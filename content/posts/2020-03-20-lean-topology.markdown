﻿---
date: 2020-03-20
slug: topologie-lean
title: Topologie Lean
--- 

Je parlais complexité du monde, frugalité du monde (faire attention aux ressources), et immatérialité (travail à distance), dans une conversation (peut-être avec moi). Et je me suis rendu compte que je n'avais jamais publié les slides de ma conférence à Lean Kanban France 2015 : "Lean Topology" où ces sujets sont abordés.

Cliquez sur l'image pour le pdf

[{{< image src="/pdf/lkfr-2015-pablopernot-topologielean.pdf" title="Lean Topology](/images/2020/03/lean-topologie.jpg)" >}}


Nouvelles formes d'organisation, travail à distance : complexité, frugalité, immatérialité. Juste des réflexions posées en 2015. On connaissait tous la réponse, mais il a fallu un choc pour la mettre en œuvre dans la plupart des organisations. Répétabilité, stigmergie, sont aussi de la partie. 

Vous avez la vidéo à cet endroit :  

https://www.infoq.com/fr/presentations/lkfr-pablo-pernot-topologie-lean/

Merci 