---
date: 2011-11-22T00:00:00Z
slug: anatomie-dune-mission-agile
title: Anatomie d'une mission agile
---

Alors que [Sudweb](http://sudweb.fr/) version 2012 lance son [appel à orateurs](https://docs.google.com/spreadsheet/viewform?formkey=dGI2TTRseEZQeFpGbHpoUC1IN3h1cXc6MA),
certains enregistrements des sessions de l'année dernière émergent dont
mon intervention "anatomie d'une mission agile". Voici donc la vidéo
(merci [Sudweb : l'annonce de la session en 2011](http://sudweb.fr/post/Anatomie-d-une-mission-agile)) et les
slides.

*Vous avez aussi la version 2 (saison 2) dans les liens en pied de page,
vidéos & slides.*

## La vidéo

{{< vimeo id="53349009" >}}

## Les slides

{{< speakerdeck id="4f9cf100b3f2f3001f00cb92" >}}


