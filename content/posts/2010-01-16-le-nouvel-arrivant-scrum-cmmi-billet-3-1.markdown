---
date: 2010-01-16T00:00:00Z
slug: le-nouvel-arrivant-scrum-cmmi-billet-3
title: Le nouvel arrivant, scrum et cmmi billet 3
---

Toute ressemblance avec des personnes existantes ou ayant existé est
purement fortuite.

Imaginons Cinthia, fraichement diplômée, qui se lance dans sa vie
professionnelle. Elle est développeuse .Net et deux belles entreprises
ont répondues positivement à sa candidature. Dans la première, on lui
vante les mérites de CMMi, que la société décline jusqu'au niveau 4 (ce
qui est excellent); dans la seconde on lui dit qu'elle sera "agile" avec
XP & Scrum.

Je vais essayer de ne pas être négatif du tout pour faire ressortir que
les bons côtés de chaque approche. Je vous fais confiance pour imaginer
tout ce qui pourrait ne pas se passer ainsi dans la réalité.

Imaginons qu'elle opte pour la première. Le très évident avantage
qu'elle va tirer d'une société CMMi est l'encadrement méthodologique sur
lequel elle va pouvoir s'appuyer, les gardes-fous qu'on va lui proposer.
On va vérifier qu'elle a pris connaissance de tous les éléments du
projet sur lequel elle va travailler. Elle verra clairement les tâches
qu'on lui affectera (avec des specs très précises), ainsi que la charge
que l'on estimera (avec elle si c'est possible). Elle va clairement
tracer dans son code sur quelle partie du projet elle travaille, et à
quelle demande, quelle exigence du projet elle répond ainsi. Son code
sera soumis de temps en temps à des revues de la part d'experts, on va
s'assurer qu'elle a bien connaissance de la configuration du projet
(serveur de dev, de staging, de prod, arborescence du code, etc etc.) et
son planning. Mieux, on lui signale que le taux d'anomalies qu'elle a
relevé ne correspond pas aux moyennes de ce genre de projet dans ce
contexte et qu'elle devrait refaire une passe dessus pour valider que
vraiment tout est ok. Bingo, elle découvre une belle série d'anomalies
qui lui avaient échappées.

Pour elle c'est parfait, le côté bordélique et fofolle qu'elle a bien
pris soin de cacher en entretien d'embauche, est gommé par le cadre
méthodologique qu'on lui propose. Dieu est chef de projet ou le chef de
projet est Dieu. En tous cas le chef de projet est l'interface avec le
reste du monde : le client, le management, le responsable qualité, etc.
Elle est tranquille dans son coin, avec son casque sur les oreilles dans
lequel elle écoute à fond Neil Young chanter "Heart Of Gold" et elle
jubile, car, ça y est, elle "pisse" du code.

Imaginons qu'elle opte pour la seconde. La voici qui débarque. On lui
présente François, Martial & Yves. Ils ne payent pas de mine mais ils
ont l'air sympas. C'est l'équipe lui dit-on. Son équipe. Et voici
Pierrot le ScrumMaster, il s'arrange pour qu'elle se sente à l'aise et
s'assure que tout roule. Demain le projet démarre elle fera une journée
avec l'équipe, son équipe, et le client, ils vont parler du projet, de
ce que le client veut, de ce qu'il est possible de faire. Ca y est le
projet a démarré. Tous les jours on parle : qu'est ce qui marche, qu'est
ce qui ne marche pas, du coup elle entend et comprend tout, et n'hésite
pas à poser des questions si quelque chose lui échappe. Elle apprend
vite et si elle a un souci, Martial vient développer avec elle. De toute
façon on lui demande de faire des tests automatiques tout le temps, du
coup ça la rassure : elle sait qu'elle ne casse rien, elle sait que son
code marche. ouf. Son côté bordélique et fofolle qu'elle a caché lors de
l'entretien d'embauche n'est pas très problématique car le périmètre est
finalement très précis, et limité dans le temps avec des itérations
relativement courtes. Elle n'a pas l'occasion de s'oublier dans des
dérives stériles. Tout est là, à l'esprit, clair. D'ailleurs on présente
régulièrement au client le résultat du boulot et celui oriente l'équipe
ou précise ce qui ne va pas. Incroyable le client a apprécié une de ces
propositions et son idée sera déployée dans le produit final. Le projet
avance bien et on fête ça ce midi en partageant tous une megapizza et en
gueulant comme des fous "*T N T, I'm dynamite !!!*".

Au prochain post, je parle de Caroline, chef de projet fraichement
arrivée d'une entreprise sans aucune méthode et qui se fait embaucher
dans les deux même sociétés et j'essaye de résumer ma pensée (un peu
bordélique et fofolle).
