﻿---
date: 2019-09-28
slug: modele-des-entreprises-complexes-partie-1
title: Modèle des entreprises complexes, partie 1
--- 

Mercredi 25 septembre j'ai eu le plaisir et la chance de présenter avec [Laurent Morisseau](http://www.morisseauconsulting.com/) une réflexion menée ses derniers mois (voire années) sur l'entreprise agile, et particulièrement sur un modèle des entreprises complexes. Comme avec le [livre Kanban, l'approche en flux pour l'entreprise agile](/2019/06/kanban-le-livre/) nos deux cerveaux, bien que pointant dans une même direction, fonctionnent de façons différentes et se complètent. C'est donc toujours pour moi, un effort, mais surtout, un apprentissage. 

Et alors que j'écris cet article Laurent m'indique qu'il vient d'écrire aussi un texte qui fait une belle introduction : [Laurent Morisseau : un modèle pour entreprise agile](http://www.morisseauconsulting.com/2019/09/28/entreprise-agile/). 

Je vais résumer le propos de notre meetup du 25 septembre chez [CFM](https://www.cfm.fr/).       

Que souhaitions-nous réaliser avec ce meetup ? Partager nos réflexions, présenter notre modèle sur les entreprises complexes, initier une dynamique qui puisse boutir à un *think tank* d'entrepreneurs, de managers, de pme, tpe, eti, etc.

## Notre conviction​

Il existe aujourd'hui un juste milieu entre un accompagnement totalement émergent ou le contextuel règne sans partage, et une démarche avec des méthodes simplistes de type “recette”. Nous ne souhaitons pas le tout émergent afin de ne pas se poser toutes les questions, mais les bonnes questions, réduire le champ des possibles. Nous réfutons les recettes simplistes : il faut se poser des questions. ​

Notre démarche est intermédiaire, entre la carte de métro entièrement codifiée où il suffit de cocher des cases sans réfléchir, et la carte "terra incognita" où tout semble à redécouvrir et à réinventer, nous plaçons le symbole l'astrolab : cet instrument donne des directions, des distances, en topographie permet de mesurer des distances, aide au relevé de plan, dans les grandes lignes. 

Nous cherchions un instrument qui donne des directions mais sans définir complètement la carte ni prétendre connaitre a priori le territoire.

{{< image src="/images/2019/09/astrolab.jpg" title="astrolab" >}}

## Une conversation

Un savoir, des principes existent. 

Tout l’intérêt de cette démarche est de les faire connaitre pour susciter la réflexion au bon niveau : pas dans tous les sens possibles, pas de façon simpliste. 

Les principes et les modèles que nous utilisons ont pour but d’articuler nos/vos réflexions, de donner de la matière, des points d’ancrage pour ne pas parler sans se comprendre, et pour avoir un espace pour cette réflexion. 

Nous voulons avoir les moyens d'une véritable conversation.  

## Archétypes

Cette connaissance qui existe, qui est déjà présente, pour avoir navigué au travers des entreprises, nous l'avons de façon empirique. C'est là que le cerveau gauche de Laurent intervient, il ne s'est pas contenté de cela, il a eu besoin d'un savoir plus structuré. Il s'est lancé dans la lecture de mille grimoires. Pour le propos, il met une théorie en évidence, celle de la contingence de Mintzberg. De l'histoire et de toutes ces observations, il constitue une série d'archétypes qui représentent les entreprises. Ses archétypes nous les faisons avancer jusqu'à l'époque actuelle pour vous proposer un modèle d'entreprises complexes.   

{{< image src="/images/2019/09/archetypes.jpg" title="archétypes" >}}

## Structures des archétypes et ses caractéristiques

Chaque archétype propose sa structure managériale et ses caractéristiques. Nous en donnons deux en focus durant le meetup : archétype produit et archétype adhoc. Le focus se fait sur trois niveaux : stratégie, tactique, et opérationnel.  

{{< image src="/images/2019/09/structures.jpg" title="structures" >}}

Toutes ces informations proposent un modèle. Nous savons des choses. Il s'agit d'avoir des éléments pour une véritable conversation. Nous avions besoin d'éléments de l'entreprise agile. Où vous situez-vous ? Dans quel archétype projetez-vous votre entreprise, ou ce département ? Soyez attentifs dès lors à ce que nous savons de ces archétypes. Correspond-il bien à ce que vous voulez être ? Sa mise en oeuvre est-elle cohérente ? Comme toujours c'est un modèle, il est faux, mais il permet d'avoir une conversation constructive.   

Tout cela donne un cadre de discussion qui va peut-être vous permettre de parler, par exemple, outil (d'un côté) ou sens (de l'autre), en réduisant le champs des possibles, en s'articulant sur des choses plus concrètes. 

{{< image src="/images/2019/09/caracteristiques.jpg" title="caractéristiques" >}}

## Cycle de vie, attraction, forces externes

Point clef, ces archétypes ont un cycle de vie. Une attraction les pousse vers la droite, cela peut les pousser à mourir comme à se réformer. Il existe aussi une suite logique qui peut être remise en cause par une force externe, pas uniquement en interne.  Une force externe ? Une crise économique, un changement de direction, un rachat, une bascule du marché, etc. 

{{< image src="/images/2019/09/cycledevie.jpg" title="structures" >}}

## Quelles observations pour les transformations (agiles) ?

Nous appelons *revitalisation* une entreprise qui se réapproprie une bonne façon de fonctionner, de dysfonctionnelle à fonctionnelle, sans changer d'archétype. Nous appelons *réforme* une entreprise qui change d'archétype, car sa nature a changé, ou doit changer. 

* Les organisations naturellement changent d'archétypes. 
* On ne peut pas passer de n’importe quel archétype à n’importe quel archétype
* Cela est rare et arrive lorsque l’organisation n’est plus en phase avec son environnement extérieur et que la revitalisation ne suffit plus
* Cela questionne notre intention quant aux transformations organisationnelles (agiles  ou non) : est-ce lié à ces changements ou décorrélé? Doit-on mettre le système en crise?

## La partie 2 

Dans la deuxième partie du meetup (prochainement ici), nous nous interrogerons avec cet instrument des archétypes sur comment lire et comment répondre à la revitalisation ou à la réforme des entreprises avec des exemples.  

## Slides, le diaporama

Plus de slides, de diapositives, mais moins d'explications ci-dessous. Si cela fait sens pour vous et pour nous n'hésitez pas à nous joindre pour réaliser à nouveau ce meetup. 

[{{< image src="/pdf/meetup-modele-entreprises-complexes.pdf" title="Slides](/images/2019/09/slides.jpg)Les slides du meetup modèle des entreprises complexes" >}}

## Merci 

Un grand merci à [CFM](https://www.cfm.fr/) pour son accueil, son buffet, sa terrasse, ses bières. Merci à toutes les personnes présentes, pour les sourires et les conversations. 

{{< image src="/images/2019/09/trio.jpg" title="trio, avec Nursel" >}}

## Sources

* [La première partie sur les entreprises complexes](/2020/04/meetup-entreprises-complexes/)
* [La deuxième partie sur les entreprises complexes](/2020/04/meetup-entreprises-complexes-2/)
* [Wikiversity sur Mintzberg](https://fr.wikiversity.org/wiki/Structure_et_management_des_organisations/La_typologie_des_structures_d%27organisation_par_Mintzberg)
* [HRMag sur Mintzberg, 2019](https://www.hrimag.com/Les-6-parties-de-base-de-l-organisation-selon-Henry-Mintzberg)
* [Les formes organisationnelles selon Henry Mintzberg, Alain Dupuis, Richard Déry](/pdf/formes-organisationnelles-mintzberg.pdf)
* [Un modèle pour l'entreprise agile, par Laurent Morisseau](http://www.morisseauconsulting.com/2019/09/28/entreprise-agile/)
* [Les entreprises complexes, partie 1](/2019/09/modele-des-entreprises-complexes-partie-1/)   
* [Les entreprises complexes, partie 2](/2019/09/modele-des-entreprises-complexes-partie-2/) 
* [Pyramide de dilts et harmonisation de l'organisation](/2019/04/pyramide-dilts-harmonisation-organisation/)  
* [Slides du meetup 2019](https://pablopernot.fr/pdf/meetup-modele-entreprises-complexes.pdf)
* [Fichier PDF : Toute ressemblance... sur la pyramide de Dilts chez BENEXT en 2019](/pdf/toute-ressemblance.pdf)