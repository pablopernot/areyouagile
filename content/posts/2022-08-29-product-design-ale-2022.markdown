---
date: 2022-08-29
slug: product-design-ale-2022
title: "Product Design at ALE 2022"
draft: false
---

To welcome my friends from [ALE](https://agilelean.eu/), I will write this post in english. I did set up a workshop with [Alberto](https://www.eventstorming.com/) and [Olaf](https://trustartist.com/). The idea was : on the backbone incarnated by the event storming / process modeling from Alberto, I bring product design, and Olaf brings meta reflection about value based on impact mapping. I'll summarize the product design stuff I brought during the workshop here.  

*(I'm trying to make a very low-tech, low consumption website, if you want/need the real, nice quality image, you need to click on it)*

{{< image src="/images/2022/08/3caballeros.jpg" title="3 caballeros" class=center >}} 



As we see in my last post (in french) : [the product of tomorrow](/2022/07/produit-de-demain-preambule/), I love to make the point that two things are major for a product this last years : creativity/innovation/disrupty (called it as you wish), and the experience (the product as experience) it brings. The last but not the least point I now would like to add is : sustainability, and even may be regenerative design. 

As we have a modeled process, due to Alberto's event storming workshop, we could envision where creativity is needed, if there is an experience to set up somewhere, where to work on sustainability, etc. The event storming modeling draw a map of your product and helps you to apprehend it. 

## About the creativity/innovation/disruption 

I don't want to fall into the conversation about the differences. It is a large specter between adjacent ideas and disruption, but usually the answer is the same : break the way your brain is usually functionning, a.k.a. the well known mojo "think different" or "out of your confort zone".  

But first a step back, as explained by our master at us all, John Cleese ([see one of my previous post / in french](/2016/08/transformation-digitale-et-creativite/)), creativity needs : time (in fact two phases : one brainstorming of maximum 1h30, and a way to get back to the outcome to work again on it), space (meaning a protected space), humor, and confidence. Because as we need to break the way the brain is usually working, we probably need to be ridiculous, hence we need a protective space and confidence. 

One of my favorite story about this ([here in french in a previous post](/2014/09/histoires-que-je-raconte-en-ce-moment/)) : A large Canadian consortium is responsible for the power lines that run through the country's forests. Problem: the weight of the snow threatens to make these lines collapse. How to solve this problem? They brainstorm, without hesitating to rave, we don't know what will come out of it, but well. Let's see. Suddenly an incongruous idea comes up: What if we lean on the bears? The bears? yes, by making them shake the posts? (really this meeting is a mess). Ok and how do we get the bears on the poles? By placing honey pots on top of the poles, on the lines? Attracted they will come and shake the poles. Ok, and how do we get the honey pots on the poles? Can't we place thousands of honey pots? Hmmm, what if we drip honey on the lines with a helicopter? ...Bingo: the organization solved its problem.... by passing a helicopter over the lines, the blades of it make the snow fall.

You probably have to be ridiculous, to drastically exagerrated things, it's one path to new ideas. Of course at some point you bring back the idea to something possible.  

...and we need the break the way brain works to get that crazy ideas.

I showed two ways during the day. 

### Crazy 8 adapted

First an adaptation of the [crazy 8 workshop](https://designsprintkit.withgoogle.com/methodology/phase3-sketch/crazy-8s). Adaptation ? Yes in no way we want an user interface, but we want new ideas. So I asked for ideas, and to make obvious the constraints, I asked to shift to another case every 20 seconds, so 2mn and a half in total. I also used another strategy to think different which is the "...and" : and you have to add something to the idea. To summarize : fold your paper in 8, on each drop ideas regarding what you are looking for. We did the whole workshop on the recrutment process... Every 20 seconds move to another idea / another part of the paper with the "...and" in mind. What's important ? The rush, the constraint, no time to think because we don't want you to reason, we want spontaneity. Your background, your experience, your weirdness will do the trick. 

In the workshop no time to really dig what we get. In real life, dig it. that's key. 

{{< image src="/images/2022/08/ale-2022-1.jpg" title="ALE 2022" class=center >}} 

### The 3 random

The other one is one of my thing in my toolbox and I never gave it a proper name, so let's go for "the 3 random". We look for something similar : break the way the brain is working by applying a constraint. Here I propose 3 lists of words (nouns and adjectives), the first two ones are [randomly generated](https://randomwordgenerator.com) (and a little bit adapted if necessary). The third list came from a list of words describing great recrutment experiences from the participants. 

The list were : 
- (A) (nouns) strategy, wedding, product, throat, health, maintenance, music, basket, discussion, town, entertainment, skill, library, meaning, green, method, satisfaction, policy, payment, communication (I made a mistake removing one and putting "green" instead which is an adjective)
- (B) (adjectives) reasonable, afraid, empty, tawdry, funny, tall, popular, astonishing, temporary, calculating, sable, humdrum, permissible, accessible, volatile, wakeful, wandering, reminiscent, questionable, tough
- (C) their own words about great recrutment memories.... and....errr shit I didn't get them. 

And then, randomly again, you pick one in each column : for example, "maintenance, popular, playful", and then you get X minutes, a brainstorming, to bring me back an idea regarding our recrutment process involving "maintenance, popular, playful". And again : be confident, have humor, no fear to be ridiculous. Here too the idea is by applying a constraint to avoid you to reason. «You can't be spontaneous within reason», Alan Watts. Give a random group of 3 words to every team (max 4/5 people).  

In companies it could be useful to create liste with competitors advantages, and company’s values, principles, ways of working, etc. instead of those random lists. 

{{< image src="/images/2022/08/ale-2022-2.jpg" title="ALE 2022" class=center >}} 

## About the experiential product 

All the idea is here : https://hbr.org/1998/07/welcome-to-the-experience-economy by James Gilmore and Joseph Pine. 

And the important story is there : 

" *How do economies change? The entire history of economic progress can be recapitulated in the four-stage evolution of the birthday cake. As a vestige of the agrarian economy, mothers made birthday cakes from scratch, mixing farm commodities (flour, sugar, butter, and eggs) that together cost mere dimes. As the goods-based industrial economy advanced, moms paid a dollar or two to Betty Crocker for premixed ingredients. Later, when the service economy took hold, busy parents ordered cakes from the bakery or grocery store, which, at $10 or $15, cost ten times as much as the packaged ingredients. Now, in the time-starved 1990s, parents neither make the birthday cake nor even throw the party. Instead, they spend $100 or more to “outsource” the entire event to Chuck E. Cheese’s, the Discovery Zone, the Mining Company, or some other business that stages a memorable event for the kids—and often throws in the cake for free. Welcome to the emerging experience economy.* "

If you get the importance of having an experiential product you can check throught your event storming representation if you're more about services than experience or maybe even goods (rarely the case regarding recrutment). 


{{< image src="/images/2022/07/economic-distinctions.jpg" title="Distinctions économiques" class=center >}} 

We spend time brainstorming what could be a more experiential product, knowing that we need **connection** and **participation**. You can project your product on this diagram : 
*(I'm trying to make a very low-tech, low consumption website, if you want/need the real, nice quality image, you need to click on it)*

{{< image src="/images/2022/08/realms-of-experience.jpg" title="Realms of experience" class=center >}} 

## Sustainability and regenerative design 

Being creative and setting up an experiential product is key for you, for me, for us. No matter your product is a company, an off the shelves product or whatever. In my mind everything is a product. And product shapes our world. Hence is it key to think them sustainable or even better with a regenerative design.

That was the third part of the product design within this workshop. 

The difference ? With sustainability you limit the damage done. With a regenerative design you repair what's have been done to the planet the last hundred years. 

{{< image src="/images/2022/08/cuningham-design-graph.jpg" title="Sustainable and regenerative" class=center >}} 

The image came from [sustainable brands](https://sustainablebrands.com/read/product-service-design-innovation/the-shift-from-sustainable-to-regenerative-design). 

So the idea is to brainstorm (again) about the sustainable part and maybe the regenerative part of your product design (here the recrutment process of the company). 

Three points of view helped us during these brainstorming : 

### For digital companies...think about the three horizons 	

For digital companies thinking about being regenerative is a challenge. We all look at being sustainable. We may imagine being regenerative in a whole system (due to the digital part the whole system could be regenerative). But being regenerative by ourself is quite impossible, and that is demoralizing.

About this I share the [idea of the three horizons](https://medium.com/activate-the-future/the-three-horizons-of-innovation-and-culture-change-d9681b0e0b0f). I got this from a very good MOOC with Daniel Christian Wahl, the MOOC is [From sustainability from regeneration](https://www.edx.org/course/worldviews-moving-from-sustainability-to-regeneration), which I recommend a lot. 

{{< image src="/images/2022/08/3horizons.jpg" title="3 horizons" class=center >}} 

In my company I suggest we tag every projects with "red, blue, green". It is starting point to make us aware where we really are. And the "blue" line is very conforting. By looking for something perfect "green line" we may be very frustrated, and never really what is needed for the next step ("blue" line).  

Red is business as usual, blue is opportunities to change (regarding the planet crisis), green is the path to regenerative design. 

So in the recrutment process of our workshop we think about "red", "blue", "green" category, and what we could enhance. 

### For digital companies think about the triple bottom line 

Another way to work on the first steps is the triple bottom line. It means for every company not only to consolidate his finance, but also **its social impact** and also **its impact on nature**. In the future (this will be apply to Europe in future years, maybe 2 or 3 years), you will not only need to have a good financial balance, but also a good social balance, and a good balanced impact regarding nature. 

Example of triple bottom line from greenbuoyconsulting : 

{{< image src="/images/2022/08/Triple+Bottom+Line.jpg" title="Triple bottom line" class=center >}}

During this workshop a team walk throught the modeled process (txs to the event storming output) and try to consolidate the finance / social / nature aspect of the process. Bonus and malus. 

### For digital companies think about biomimicry 

Eventually, in order to put ourself in a good position to be regenerative nature could be a really good helper. Again from the [Mooc From sustainability from regeneration](https://www.edx.org/course/worldviews-moving-from-sustainability-to-regeneration) I found [Michaela Emch](https://www.linkedin.com/in/michaelaemch/) quite interesting. She drove me to [asknature.org](https://asknature.org). 

{{< image src="/images/2022/08/asknature.jpg" title="Ask nature" class=center >}}


During this workshop a team walk throught the ask nature website and try to find ways from nature to introduce interesting stuff about our recrutement process. For example not a pair to supervise the newcomers but [a group of four like flamingos](https://asknature.org/strategy/flamingos-form-friendships/).

At this point we loop with the creativity stuff :) and that was the end of my part. 

## Conclusion

Of course we  (Alberto, Olaf and I) can do this workshop about you real processes : having meta perspective about the impact needed, having the modeled process to share it, improve it, etc, and especially improve it on a product design layer. 

## Conclusion 2 

With all the conversation about climate crisis, the 9 planetary limits, etc, the anxiety it could legitimately generate : we, "agile people", are in a way gifted. Taking care of the planet now is all about system thinking, emergent design. Let's use our skills ! 






