---
date: 2017-01-21T00:00:00Z
slug: blues-du-coach
title: Le blues du coach
---

Coach un peu cabossé. Ne saurait répondre précisément à vos demandes,
mais pourrait aider. Saura trouver de la valeur si vous le mélangez à
votre intelligence collective. Pourrait apporter un peu de sel. Occaze à
saisir. **Alimentation en feedback nécessaire**.

Une réflexion rampante depuis quelques temps dans mes conversations :
l'usure de notre métier, la difficulté du *feedback* pour les "coachs
agile". Notre métier est difficile. Difficile car complexe, sans réponse
simple. Difficile car usant, passionnant mais usant. Je ne cesse de
répéter aux gens que je croise que le *feedback* est essentiel pour
rester impliqué, engagé, voire motivé. Il est nourrissant de savoir ce
que génère nos actions, nos choix, nos décisions. Que le résultat soit
bon ou mauvais, c'est toujours mieux que le silence, l'ignorance ou
l'absence de résultat. Or notre métier de "coach agile" n'est pas adapté
à cette boucle de *rétro-information* car il a propose tellement de
ramifications que les messages retours sont flous.

## Je jalouse les "coach craft"

{{< image src="/images/2017/01/monkey-grooming2.jpg" title="Grooming moment" >}}

Déjà je jalousais les "coach craft" (ces coachs experts artisans de la
matière code) car ils avaient la faculté de transmettre leur savoir
assez directement, ne serait-ce que par mimétisme. Ce qui me parait déjà
bien plus ardu pour un coach organisationnel. En effet, il est difficile
de dire : fait comme moi, car nous sommes tous tellement différents. Le
matériau n'est pas le code, l'outil, mais la personne elle-même. Et je
ne parle pas de coaching individuel, mais je parle de communication, de
dynamique de groupe, etc. Donc ainsi il n'est pas aussi facile de
transmettre que dans de nombreuses autres professions. On peut montrer
comment on s'y prend, expliquer sa démarche. Mais chaque personne va
faire avec sa nature et son contexte. Le résultat sera forcément
différent. Pas de répétabilité.

**L'incertitude c'est passionnant mais ce n'est pas reposant**.

## Nous sommes des affamés de feedback

Non content de ne pouvoir transmettre aisément notre savoir, nous
pouvons aussi être assez aveugle ou inquiet sur notre faculté à apporter
de la valeur. Nous sommes des affamés du feedback. En fait pas plus que
les autres. Mais le feedback est essentiel à notre motivation, à notre
engagement, comme tout le monde. Nous en avons donc besoin. Pour un
coach agile, pour reprendre l'étiquette que l'on nous colle, il est
difficile d'avoir ce sentiment de progrès, sentiment d'avoir un impact.
En dehors de tous les aspects liés à l'ego qui ne sont pas mon sujet,
c'est quand même agréable d'avoir un quelconque feedback sur lequel se
reposer. Être coach agile c'est usant. Je ne me plains pas. Cela me
passionne. Mais c'est usant. Notre matière est l'intelligence
collective, l'organisation, la dynamique de groupe, etc. Tout cela est
bien immatériel. Nous ne modelons pas notre matière nous l'accompagnons
et jamais seul. Quand ça marche ce n'est jamais exclusivement ou même
principalement grâce à nous, c'est le fruit d'une combinaison de
personnes, de sens, de chance, de moments opportuns. Ce n'est pas une
question d'*ego*, c'est une question de savoir, d'amélioration,
d'introspection.

## Je jalouse le fleuriste, le bucheron, le codeur, le trader, le maitre nageur, le chirurgien

{{< image src="/images/2017/01/monkey-grooming3.jpg" title="Grooming moment" >}}

Ils vendent des fleurs ou non. Les plantes poussent ou pas. L'arbre
tombe ou pas. Le code compile ou pas. On refactorise le code ou pas.
Cela marche mieux ou pas. L'action se vend à meilleur prix ou pas.
Quelqu'un se noie ou pas, quelqu'un apprend à nager ou pas. Le poumon
est recousu ou pas. J'ai cherché des professions proches de la notre.
Toutes celles intellectuelles me dit on. Le romancier, le professeur, le
journaliste, l'acteur, le philosophe. Mais le journaliste et le
romancier sont faces à eux mêmes. Ils peuvent être torturés mais ils ont
les cartes en main, tout comme l'acteur. L'acteur de théâtre a une
matière plus compliquée : elle fluctue. Son souci c'est plus d'être ou
de rester inspiré, talentueux. Le professeur a une relation plus
bidirectionnel avec sa classe. Il demeure le professeur. Nous
travaillons avec des dynamiques de groupe qui nous englobent, dans un
monde [systémique](https://fr.wikipedia.org/wiki/Syst%C3%A9mique),
holistique, global, qui ne peut se réduire à une sous-optimisation.

## À vot' bon cœur

Quelques bribes de feedback ? Des miettes d'encouragement ? J'en ai
reçu, assez souvent finalement, mais dans la masse de mes activités
elles se perdent. Les gens qui font mon métier ne sont-ils pas les
champions de l'introspection et de la remise en question ? Sûrement et
tant mieux. Alors apportez leur votre feedback.

*Oh yeah, baby, 
J'suis pas fleuriste, ni bucheron
Je suis le vampire de l'apprentissage 
J'suis pas chirurgien, ni horloger 
Le bootleneck de ma guitare coince l'orga, bebabelula, 
J'suis pas le grand nageur qu'est devenu acteur 
J'ai le blues du coach agile, le spleen du facilitateur, 
ma came c'est le feedback, 
tchtchi chak*

