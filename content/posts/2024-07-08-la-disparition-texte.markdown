---
date: 2024-07-08
slug: la-disparition-le-texte 
title: "Texte: La disparition"
draft: false
---

Voici un texte écrit à la suite de conférences à Niort, Toulouse et Lille (voir [les vignettes là](/2024/07/la-disparition/)). Je voudrais remercier les gens qui m'ont fait des retours, notamment [Rachel Dubois](https://www.linkedin.com/in/duboisrachel/). Son retour était en même temps critique, encourageant et constructif, c'est ce qu'il me fallait.

C'est la version 1.0, peut-être des aménagements et corrections verront le jour suite à vos retours.

[Voici le pdf](/pdf/2024-la-disparition.pdf). merci.

{{< image src=/images/2024/07/la-disparition-toc.jpg title="La disparition, sommaire" alt="La disparition, sommaire" width="600" >}}
