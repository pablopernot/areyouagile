---
date: 2016-08-28T00:00:00Z
slug: transformation-digitale-et-creativite
title: Transformation digitale et créativité
---

Ci-joint une courte intervention (\~15mn) que j'ai pu réaliser dans le
cadre des Universités d'été de l'un de mes clients. Il s'agit d'évoquer
la transformation digitale, mais surtout nos capacités d'adaptation et
créatives. C'est un texte très inspiré par un discours de John Cleese
que je voulais traduire depuis belle lurette et que vous retrouvez en
bas de page.

## Transformation digitale ?

"Transformation digitale", la belle affaire !

"Transformation digitale", tout de suite les grands mots !

Qu'est ce que c'est une transformation digitale ? Une capacité à
appréhender tous ces réseaux sociaux ? Oui moi aussi j'ai installé
"snapchat" pour faire apparaître le logo sur mon téléphone et faire
croire à ma fille que j'étais "in". Mais je n'ai fichtrement aucune idée
de comment m'en servir. Et pourtant je ne suis pas un troglodyte de
l'android, j'ai déjà envoyé plus de 13000 tweets. Est-ce que je suis un
non digital ? non transformable ? has been ?

Une capacité à appréhender tous ces nouvelles technologies ? Ces
nouveaux usages ? Blockchain ? Cloud ? Bigdata ? Microservices ? AirBnB
? Uber ?

Ah... toutes ces idées reçues et ces fausses intuitions sur la
"transformation digitale".

Se projeter en 2026 ? Dans 10 ans ? C'est comme si nous revenions 10 ans
en arrière en 2006... l'iphone et le smartphone n'existent pas encore,
les taxis parisiens règnent sans partage, quand je me déplace je vais
dans des hôtels, et je n'ai pas peur d'avoir un accident de voiture
parce qu'un autre chauffeur a vu un pokemon virtuel sur ma voie. Alors
parler de digital dans 10 années, la bonne blague. 70% des métiers
auront changés dans 10 ans. Non dans "transformation digitale" c'est
"transformation" qui est important.

L'expertise c'est bien savoir faire quelque chose, une expertise
digitale dans 10 ans n'a pas de sens. C'est d'abord une transformation.
Une capacité à évoluer, comme depuis l'aube des temps. Ce n'est pas un
savoir faire que nous recherchons dans cette perspective, mais un savoir
être.

Soyez d'abord prêt à vous transformer. À vous adapter, à penser
différemment. Dans "digital" les gens projettent une idée liée à la
technologie. À une expertise. Ces fameux "digital natives" sont surtout
une génération née dans un monde où le changement est palpable avant
d'être un monde numérique. Il s'agit d'une génération qui sait, qui vit,
le fait qu'en dix ans Yahoo ou Nokia se créent, dominent le monde un
temps, et disparaissent. Sa clef ce n'est pas le numérique, mais le
changement, l'adaptation, et l'innovation.

Se placer dans une perspective de transformation digitale ce n'est pas
être un expert, c'est savoir comprendre et accepter le changement, et
idéalement en être un acteur. Être soi-même créatif.

## Créativité

Vous connaissez **John Cleese** ? vous savez John Cleese, le gars des
Monty Python, celui qui représente le ministère des démarches stupides,
ou le professeur amoureux d'un poisson nommé Wanda, bref ce John Cleese
là.

Etre créatif nous dit John Cleese, c'est se placer dans un état d'esprit
particulier,...une façon d'opérer...qui permet à notre créativité
naturelle de fonctionner. **MacKinnon** (un chercheur de la Harvard
Business School sur la créativité) décrit cette capacité comme une
facilité pour le jeu. Et donc il observe que les plus créatifs sont
comme des enfants. Ils sont capable de jouer avec une idée, de
l'explorer, pas dans un objectif pratique immédiat mais juste pour
s'amuser.

{{< image src="/images/2016/08/cleese.jpg" title="John Cleese" >}}

Cleese et lui ont travaillé ensemble et sont devenus fascinés par le
fait que l'on puisse décrire les gens dans leur travail au quotidien
selon deux modes : "ouvert" ou "fermé". Ce que nous dit Cleese c'est que
la créativité n'est pas possible en mode "fermé". Ce mode "fermé" est
celui dans lequel les gens se retrouvent la plupart du temps au boulot.
Ce moment où au fond de nous on ressent l'idée qu'il y a beaucoup à
faire, qu'il faut le faire, et qu'on va le faire. C'est une posture
active, probablement un peu anxiogène, mais juste ce qu'il faut pour
nous exciter et nous donner plaisir à cet achèvement. Dans cet état nous
sommes un peu impatient, et pas seulement avec nous même. Cet état est
une tension, on tend vers quelque chose, et pas un relâchement comme le
rire, ou l'humour. C'est un état qui peut-être très bénéfique, mais
c'est un état où l'on peut vite être très stressé, peut-être un peu
maniaque, mais sûrement pas créatif.

À l'inverse l'état, le mode "ouvert" est relaxé, détaché, sans objectif
clair, plus contemplatif, plus enclin à l'humour, plus enclin au jeu.
C'est une humeur où la curiosité a du sens car on ne cherche pas à
atteindre un but rapidement. On peut jouer, et c'est ce qui permet à
notre créativité naturelle d'émerger.

Cleese raconte que quand Alexandre Fleming a découvert ce qui allait
devenir la pénicilline, il devait être dans un état "ouvert". Le jour
précédent il laisse une série de bacs de culture pour laisser pousser
des champignons ou autres bactéries dessus, de la moisissure. Le jour en
question il observe ces cultures et découvre que sur l'une d'elles
aucune moisissure n'est apparue. Si il avait été dans un mode, un état
"fermé", il serait resté axé sur l'idée : je fais pousser des
champignons et des bactéries, des parasites dans des plats, et voyant
que l'un des plats n'en possède pas il l'aurait simplement jeté.
Heureusement il était dans un état "ouvert", et ainsi sa curiosité a été
piqué par le phénomène. Cette curiosité l'a mené à la pénicilline.

Dans un état "fermé", le plat sans parasite n'a pas de sens, dans un
état "ouvert", c'est une clef.

Dans quel mode êtes vous au travail ? Dans quel mode doit-on se trouver
pour plus comprendre, intégrer, s'adapter, anticiper, amener le
changement et l'innovation ?

Cleese et Mackinnon estiment que nous avons besoin de cinq choses pour
atteindre le mode "ouvert".

De l'**espace**.

Du **temps**.

Encore du **temps**.

De la **confiance**.

Et de l'**humour**.

De l'**espace**, un certains isolement, pour s'autoriser cette posture
contemplative, détachée, qui peut divaguer et ne pas se faire rattraper
par la cadence du monde qui nous entoure, celle du mode "fermé". Cela me
fait penser à IBM, lorsqu'il crée le PC (le *personal computer* dans les
années 80), ils isolent une équipe sur la côté Est, alors que tout IBM
est sur la côté Ouest. Dans un espace délimité, protégé.

Du **temps**. Pour utiliser cet espace, il faut aussi délimiter une
période de temps bien précise allouée à ce moment de créativité. Un
historien hollandais du nom de **Johan Huizinga** explique "que le jeu
se distingue de la vie ordinaire, les deux ont une durée et une
localisation, mais le jeu nécessite un isolement, et une période limité
de temps. Le jeu démarre et à un certain moment fini, sinon ce n'est pas
jouer". Il faut donc que cet état de jeu ai une durée bien spécifique,
démarre et s'arrête. John Cleese indique que son expérience des Monty
Python le laisse penser qu'une heure et demie est une bonne durée, après
il faut une pause.

Du temps encore, on observe que dans ce monde d'idées émergentes il ne
faut pas se précipiter sur les solutions qui apparaissent. Il faut
savoir tolérer l'inconfort et l'anxiété un peu plus longtemps,
s'autoriser à jouer un peu plus longtemps, les solutions sont plus
créatives. Décidez le plus tard possible, au dernier moment responsable.
Quand vous pensez avoir trouvé, essayez de vous interroger encore :
est-il nécessaire de déjà prendre cette décision ? Nous sommes
typiquement dans un système émergent, pas d'expertise. Encore une fois
si nous étions dans l'expertise il nous aurait fallu trouver les bons
experts et nous aurions eu notre réponse. Comme fabriquer une Ferrari,
trouvons les bons experts et cela marche. L'émergence est plus comme la
mayonnaise, cela va marcher, probablement, à quel moment, hummm, parle
là je pense... Et souvent dans l'émergence il ne faut pas se précipiter
sur la première réponse apportée, il faut donner du temps à cette
émergence.

De la confiance. Ce cadre, cet oasis comme le décrit Cleese, d'espace et
de temps, vous protège du sarcasme extérieur, c'est comme un bac à sable
dans lequel on peut s'amuser, en toute confiance. On peut être
vulnérable, on est protégé. C'est idéal pour être outrageux dans ses
idées, et il ne faut pas s'en priver ! En restant positif on escalade
dans notre liberté d'élucubrer : "cela serait encore mieux si...", "et
si ?", "Ok imaginons que...". C'est l'essence même du jeu, pas de souci,
pas de mal, à aller dans une mauvaise direction. Le cadre du jeu est un
espace protégé, de confiance, où l'on peut essayer. **Vous ne pouvez pas
être spontané avec raison dit Alan Watts**. Il faut oser le ridicule.
Pas de crainte d'erreur, l'espace est protégé.

Enfin de l'humour, c'est la meilleure façon de passer du mode "fermé" au
mode "ouvert". L'humour nous place rapidement en état relaxé, de jeu, de
spontanéité. La créativité vient quand vous faites s'entrechoquer deux
concepts étrangers, comme souvent avec l'humour ou le comique, deux
références viennent se croiser et laissent place à quelque chose de
nouveau qui s'évacue dans le rire. Le rire estiment certains penseurs
est cette énergie soudaine et inattendue dont on ne sait que faire et
qui s'évacue ainsi. Deux concepts qui se mélangent et nous surprenent,
des projections outrancières, etc

Un exemple.

Un gros consortium canadien est responsable des lignes électriques qui
parcourent les forêts du pays. Problème : le poids de la neige menace de
faire s’effondrer ces dîtes lignes. Et c’est long et compliqué à
traiter. D’autant plus que c’est dangereux, les forêts canadiennes
hébergent une palanquée d’ours. Comment résoudre ce problème ? Ils
"brainstorment", sans hésiter à délirer, on ne sait pas ce qui en
sortira, mais bon. Voyons. Soudain une idée incongrue surgit :

> Et si on s’appuyait justement sur les ours ? Les ours ? Oui ils
> pourraient venir gratter les poteaux? Ça suffira de gratter pour faire
> tomber la neige? Ou les secouer? Avec du miel? Oui en les faisant
> secouer les poteaux on peut imaginer que la neige tombe. (vraiment
> cette réunion c’est n’importe quoi). Ok mais il faudrait que le miel
> soit en haut des poteaux pour les pousser à secouer ou au moins à
> monter sur les poteaux, non? En plaçant des pots de miel en haut des
> poteaux, sur les lignes ? Attirés ils vont venir secouer les poteaux.
> Ok, et comment on place les pots de miel sur les poteaux ? On ne peut
> pas placer de milliers de pots de miel? En plus en haut... Hummm, et
> si nous faisions couler du miel sur les lignes en passant avec un
> hélicoptère? Facile pour un hélicoptère de passer sur les lignes pour
> déposer le miel sans risque!

Bingo : l’organisation a réglé son problème... en faisant passer un
hélicoptère au dessus des lignes, les pales de celui-ci font tomber la
neige.

Soyez disruptif, pensez outrageusement, dans un espace protégé, sans
vous précipiter. Toute cette innovation est une question de sérendipité.
Pensez aux **surréalistes**, aux objets introuvables de **Carrelman**
(voir la photo bannière de cet article par exemple). Pensez au mode
"fermé", et surtout au mode "ouvert".

Essayons.

## Atelier 15mn espace des mèmes.

Pour la suite, avec le public j'affiche 3 ensembles de mots et nous nous
amusons à faire émerger de nouveaux concepts. Je reprends en cela
l'atelier de [l'espace des mèmes](/2015/07/lespace-des-memes/).

Exemple d'ensembles de mots :

### Trentaine Marques/Usages mots geek

iTunes, Spotify, Netflix, Amazon, Tesla, NASA, Playstation, Minecraft,
AirBnB, Uber, Angry Birds, Pokemon, Instagram, Youtube, Whatsapp,
Tinder, Happn, Mobile, Tablette, Objet connecté, Drone, Occulus rift,
3D, Lego, Playmobil, Snapchat, GoogleMaps, Waze, Twitter, Voiture sans
pilote, IOT, Réalité augmentée, 3D Print, Blockchain, Robot, Wearables,
Machine Learning

### Trentaine adjectifs, mots, objets (on peut en ajouter !)

Vert, Bleu, Insensé, Germe, Médicament, Moisissure, Prompt, Arrondi,
Instantané, Inflexible, Coloré, Inversé, Retourné, Invisible, Apparent,
Superficiel, Virtuose, Renflé, Connexe, Rangé, Propre, Granuleux,
Adhérent, Visqueux, Rebondi, Beau, Long, Court, Souterrain, Volant,
Télépathie, Télékinésie, Flocon d'avoine, cuillère, Fourchette,

Ajoutez à cela une trentaine de mots liés au métier de mon client.
Mélangez, touillez, laisser reposer.

## John Cleese

La superbe intervention de John Cleese sur la créativité (que je n'ai
jamais eu le temps de traduire pour ce blog) :
<http://genius.com/John-cleese-lecture-on-creativity-annotated>
