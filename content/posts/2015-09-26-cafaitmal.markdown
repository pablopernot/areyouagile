---
date: 2015-09-26T00:00:00Z
slug: trois-apprentissages-clefs-dune-approche-agile
title: Trois apprentissages clefs d'une approche agile
---

Je comptais vous proposer une toute petite adaptation à la *user story
mapping* de Jeff Patton. Je vais le faire, mais en écrivant je me suis
retrouvé à mettre en évidence trois apprentissages clefs des trois
populations les plus fréquentes d'une transformation agile dans mon
environnement. Et je pense désormais que ce retour d'information, ce
feedback, a plus de valeur que la petite adaptation que je réalise
autour de la *user story mapping* de Jeff Patton, mais dont je vous
parlerais quand même.

## Les apprentissages

Les apprentissages que je souhaite évoquer sont clefs dans le sens où
ils changent les postures communément admises ou rencontrées. Ce ne sont
pas nécessairement les plus importants, mais ils sont importants car ils
sont représentatifs des changements induits par une approche Agile
moderne.

### Apprentissage du métiers : savoir découper

Dans mon déroulé disons classique d'une définition de besoin après des
ateliers de type *design thinking*, *lean startup* ou *innovation game*
(lean canvas, persona, remember the future, empathy map, etc.), après
ces ateliers j'articule deux cartes (voir cet
[article](/2015/05/peetic-2015-un-peu-de-croquettes/)). L'*Impact
Mapping* vient donner les axes stratégiques, la *User Story Mapping*
vient définir une approche tactique. Mais les deux cherchent à obtenir
le même type de résultat : prioriser (valeur) de petits ensembles
autonomes (choses finies) qui font sens et permettront d'avoir
rapidement du feedback. C'est l'objectif des gens du métiers :
**valoriser et valider au plus tôt leur proposition de valeur**. Ainsi
il faut apprendre savoir prioriser, mais surtout savoir **découper en
petits éléments qui font sens. Découper au niveau de la fonctionnalité,
découper au niveau d'un ensemble cohérent à proposer**.

### Apprentissage de la technique : savoir ne pas faire de la technique une complication

L'apprentissage de la technique est de **ne pas faire de la technique
une complication**, c'est à dire offrir à la création de valeur un socle
qui lui garantisse de s'exprimer sans se soucier de la technique
justement. Dans les faits, et dans les milieux informatiques, cela se
traduit concrêtement par un apprentissage des plateformes d'intégration
continue, du *continuous delivery*, des tests automatisés, du *test
driven development*, etc. C'est peut-être l'apprentissage le plus dur
car bien souvent il est contrarié par l'existant.

### Apprentissage du management : savoir laisser faire : auto-organisation et incertitudes

L'apprentissage du management est bien connu, c'est le changement de
posture entre chef, *manager*, *boss*, et *servant leadership*,
facilitateur. Cela prend deux formes : la facilitation (aider,
supporter, conseiller, etc.), et le laisser faire, celui là est le plus
dur à apprendre car souvent il est un changement de posture assez
marqué. Ce "laisser faire" va devoir s'exprimer principalement de deux
manières : **laisser faire l'auto-organisation et accepter les
incertitudes** (des budgets, des délais, du monde, de la complexité, de
l'environnement, etc.).

## User Story Mapping

Et si je reviens à cette *user story mapping*, la difficulté des métiers est le découpage en petit morceaux faisant sens (que l'on priorise par valeur, mais cela c'est moins dur). Généralement les étapes d'une *user story mapping* sont : on décline une ligne principale de grandes fonctionnalités, ou d'étapes comportementales ou chronologiques, de gauche à droite. Le premier sens de lecture est donc de gauche à droite, c'est l'axe principal. Puis on décline chacune des briques de cet axe verticalement : de haut en bas, par ordre d'importance, on aligne les éléments qui vont permettre de constituer plus ou moins l'élément chapeau de la ligne horizontale. On a donc ensuite deux sens de lecture : principal de gauche à droite, secondaire de haut en bas : le détail
nécessaire à la réalisation de l'élément principal. À tout moment on peut placer au dessus des éléments principaux horizontaux des alertes, des points de vigilance, des questions ouvertes, qui nous sensibiliserons pour la suite.

### Ca fait mal mais ça marche

L'étape d'après est cruciale : il s'agit -- par exemple au travers d'un
ordonnancement MoSCoW (Must have, Should have, Could have, Would have,
c'est à dire : je dois avoir, je devrais avoir, je peux avoir,
j'aimerais avoir) -- de découper la matrice qui est apparue. On découpe
par petit ensemble à livrer qui font sens. Souvent difficile pour les
gens du métiers, qui en veulent plus qu'ils ne peuvent avoir.
Initialement il est difficile de les sensibiliser à l'importance de ne
pas aller trop loin avant d'avoir du feedback, de ne pas aller trop loin
avant de profiter de la valeur déjà créée.

Donc difficile de leur demander de descendre des éléments de "must have"
à "should have". Beaucoup de choses restent dans le "must have", qui
souvent reste trop "gras", trop "empâté", pour être assez efficace. Je
me suis échiné à vouloir faire descendre des éléments dans la ligne
"should have" expliquant qu'on les ferait quand même, juste plus tard,
et avec entre temps les ffedback et la valeur de l'"essentiel". Souvent
en vain.

Et puis juste avec un petit truc cognitif j'ai libéré des situations :
il fallait les faire monter les éléments, et non pas les faire
descendre. Depuis que j'ai compris cela quand la *user story mapping*
touche à sa fin je cite le fondateur de LinkedIn qui disait -- je crois,
de mémoire -- "si vous vous sentez à l'aise quand vous sortez votre
produit c'est que c'est trop tard", et puis je libère un espace **au
dessus** du bloc "Must Have" que j'appelle "**Ca fait mal mais ça
marche**" et je demande aux métiers de placer là ce qui fait mal mais
qui marche quand même. Au dessus. Le mouvement est très différent. Et
effectivement **cela fait mal mais ça marche**.

Exemple de User Story Mapping (qui vient de cet
[article](/2015/05/peetic-2015-un-peu-de-croquettes/)), cliquez pour
grossir (considérer les versions produit comme les "must have", "should
have", etc...):

{{< image src="/images/2015/05/user-story-mapping.jpg" title="User Story Mapping" >}}
