---
date: 2022-02-03
slug: sur-le-produit
title: "Sur le produit"
---

J'intègre OCTO. Grande aventure. Beaucoup de curiosité. Plein d'opportunités.

Des dialogues se déclenchent, sur le *product management*. Je me permets de partager une conversation qui vient de démarrer, avec plaisir. Ça me permet 
de clarifier une sorte de profession de foi sur le **produit**, le **product management**.  

On m'interroge :  

Elle - "On peut parler du cloud comme un accélérateur business, c'est certes une réalité, mais c'est déjà là depuis longtemps (Airbnb, Netflix, Engie, tout le monde en fait). Tout le monde n'y est pas, mais peu il y a peu d'inconnus sur les opportunités qui permettent d'accélérer. Le vrai sujet, c'est la création de nouveaux modèles business, c'est là les vraies opportunités qui se jouent aujourd'hui. J'ai plein d'exemples, à commencer par les 3 géants qui après être devenus fournisseurs d'infrastructure puis de services, sont maintenant des marketplaces pour que d'autres vendent leurs softs. Ils se rémunèrent grâce à ça (fees entre 3 et 20%), un nouveau *business model* qui augmente dans leurs revenus. C'est une opportunité pour tout le monde. Les banques pour la lutte contre la fraude par exemple, AWS l'a fait récemment alors que les banques se regardaient, elles auraient pu sortir un service/plateforme de lutte contre la fraude. Comment ne pas louper l'opportunité, est-ce cela la *product company* ? Et si oui quel est le chemin ?"" 

Moi - J'aurais envie de partir de cet article de John Cutler : [the product system](https://medium.com/@johnpcutler/the-product-system-a5bbd83424a6). 

Première conclusion (article de 2017) : on ne peut plus penser produit de façon linéaire. Il se joue tellement de choses sur le marché que le produit
se doit d'être constamment à l'écoute de ce qui se déroule, et savoir réagir, s'adapter[^2]. Ces notions d'émergence, d'adaptation, tout le monde les connait. Mais qui les applique vraiment ? Pas tant de monde que cela. Il faudrait commencer par là. 

Et l'article que j'évoque met en évidence une dichotomie importante : "il y a produit, et il y a l'organisation faîte pour le produit"[^1]. 
Ainsi, et j'en suis convaincu, pas de produit sans une organisation produit. 

Alors quel est le chemin ? 

Je crois qu'il faut commencer par s'interroger. Trop souvent on observe la suprématie de la solution du *delivery*. On fabrique, on délivre, on fabrique, on délivre, comme des Shadoks qui pompent sans plus savoir pourquoi. Il faut s'interroger. J'entends par exemple régulièrement "nous mettons en production tous les quinze jours". J'imagine que cela met en évidence un gage d'excellence technique, mais on ne peut pas s'arrêter là. D'abord pourquoi mettre en production tous les quinze jours ? Est-ce que le rythme est le bon ? Que souhaite le client ? Qu'est-ce qui nous rend service ? Et puis ensuite : lorsque l'on met en production on a l'impression que l'on est arrivé au bout alors que c'est justement le début ! Ce qui arrive en production qu'en advient-il ? Qui l'utilise ? Comment ? 

La première étape c'est de mettre en oeuvre tout un dispositif de récolte d'apprentissage sur ce qui passe pour nos produits, de *feedback*. (peu à peu, ne replongeons pas dans un grand processus lourd et qui ne soit pas bout à bout, et sans émergence, un *big up front*). Dans l'article que je prends comme point de départ cette importance du feedback est mise en avant. On parle de l'importance d'améliorer la compréhension et la récolte du feedback, mais aussi d'étoffer la finesse de sa lecture et des données qui nous proviennent.  

Il y a donc véritablement une conversation qui change dans l'entreprise. On s'interroge **d'abord** sur la réception notre produit, la complexité du contexte dans lequel il évolue, la concurrence, les besoins, etc., et on s'interroge **ensuite** sur comment. Tout le regard est projeté vers l'extérieur et pas vers l'intérieur. C'est souvent dur pour les entreprises qui sont devenues politiques (cycle de fin de vie énoncé par Mintzberg) et dont toutes les questions ne se tournent que vers l'intérieur (Que vais-je devenir ? Comment monter en grade ? Comment ne pas être mis en défaut ? Comment répondre en interne ?). Et si il y a quelque chose à fabriquer c'est cet outil de réception et de récolte du feedback (mais lui aussi évolue constamment).  

La seconde étape (concomittante ou pas cela dépend du contexte) c'est de pouvoir se réorganiser pour délivrer le produit, en approche produit. L'idée est d'avoir une capacité à délivrer un impact, des retombées, une influence (pour essayer des mots plus français) sur notre marché. Pour cela il faut s'astreindre aux principes dit agile de réponse à la complexité : des équipes bout à bout qui savent délivrer régulièrement des morceaux autonomes de produit que l'on va pouvoir mettre dans les mains des utilisateurs pour les valider ou les invalider. Plus la capacité de changer, de moduler, de valider rapidement nos hypothèses est forte, plus on aura de la souplesse, plus on sera performant, car notre produit grandit, s'invente, ou se nourrit de cet apprentissage, de ces boucles constantes de fabrication, observation, adaptation, et découverte. 

Ce chemin agit donc sur la structure de l'organisation, car avoir des équipes de bout en bout n'est pas chose aisée quand ce n'était pas le cas depuis plusieurs années ou dizaines d'années. Il est aussi très difficile de donner assez d'autonomie à ces équipes pour quelles puissent se sentir assez libres de faire toutes ces hypothèses, de tester sans cesse le marché pour trouver les voies. Le *cloud* que tu évoques est un outil de simplification pour tester et porter tous ces produits et les hypothèses qu'ils embarquent constamment. Ma conviction est que d'une façon ou d'une autre il faut rendre la main aux équipes produit de bout en bout, de l'idée à la mise en production (aux organisations de sécuriser leurs infrastructures et leurs flux de déploiement en fonction).          

Donc une réorganisation des équipes, la mise à disposition des équipes d'un cadre de bout en bout, une conscience de l'importance du *feedback*, de l'apprentissage constant, et de l'observation constante de ce qui se passe avec mon produit et autour. 

Cette réorganisation est potentiellement aussi constante que les turbulences du marché sur lequel on évolue. Ça peut secouer, mais cela amène de positif aussi, cette fameuse résilience (encore une fois ce sujet est abordé dans l'article cité).  

Cette souplesse générale, cette agilité, doit bien se comprendre, elle doit s'intégrer culturellement. Le chemin que je suggère est celui des *real options*. 

Je pense qu’une très bonne source en la matière est Chris Matts, dont voici un des articles (en anglais, [real-options-enhance-agility](https://www.infoq.com/articles/real-options-enhance-agility/)).

Je me permets d’en traduire quelques extraits ici (enfin je demande à [deepl](https://www.deepl.com/translator)) :

Pour toute décision à prendre, il existe trois catégories de décisions possibles, à savoir une “bonne décision”, une “mauvaise décision” et “aucune décision”. La plupart des gens pensent qu’il n’y en a que deux : soit vous avez raison, soit vous avez tort. Comme nous ne savons normalement pas quelle est la bonne ou la mauvaise décision, la décision optimale est en fait “aucune décision”, car elle reporte l’engagement jusqu’à ce que nous disposions de plus d’informations pour prendre une décision plus éclairée.

Cependant, si nous observons le comportement de la plupart des gens, nous constatons qu’une aversion à l’incertitude fait que les gens prennent des décisions tôt. Les options réelles répondent à cette aversion pour l’incertitude en définissant la date exacte ou les conditions à remplir avant de prendre la décision. Au lieu de dire “pas encore”, l’approche des options réelles dit “Prenez la décision quand…..”. Cela donne aux gens une certitude quant au moment où la décision sera prise et, par conséquent, ils sont plus à l’aise pour retarder la décision. Le fait de retarder les engagements donne aux décideurs une plus grande flexibilité, car ils continuent à avoir des options. Il leur permet de gérer le risque/l’incertitude en utilisant leurs options.

C’est le “décider le plus tard possible” du Lean, “au dernier moment responsable”. 

Les bienfaits des *real options* c'est d'une part de se donner du temps de la granularité et donc de la souplesse, et deuxièmement cela met de nouveau en évidence la question de l'apprentissage, de l'observation, du feedback. Avant de répondre : observons, apprenons, jusqu'à la prochaine décision. 

Dans ton exemple sur les banques je ne sais pas ce qui a manqué. Cela peut être l'absence de regard vers l'extérieur et que tout ce qui compte désormais c'est comment je vais m'en tirer personnellement, au sein de l'organisation. Comme dans les organisations dites politiques (à la Mintzberg). Donc personne n'a peut-être été réceptif à cet enjeu extérieur : la concurrence et l'intérêt d'un marché sur les questions de fraude. Cela peut être l'absence de compréhension de l'émergence dans ce monde incertain, de souplesse, personne n'a été capable (sauf Amazon donc) de déclencher un dispositif assez léger et de bout en bout : pour apprendre, pour tester, pour faire des hypothèses et découvrir qu'un produit était manquant (et qu'elles avaient tout en main pour y répondre).   

Est-ce que cela répond à ta question ? 


[^1]: *"Today’s product management dynamic in many ways defies structure, organization, and the linear-oriented, waterfall processes of the past.*" Annie Dunham
[^2]: *"The idea of linear flows that you expect in product development are incomplete because current product environments are significantly more complex, dynamic, and “alive” than previous development environments."* John Cutler


Elle - Je retiens l'orga produit, même si tu ne la décris pas. 

Moi - Une organisation produit est une organisation qui est pensée pour délivrer quelque chose qui puisse être utilisé. Bon cela, ça ne diffère pas *a priori* de toutes les organisations. Et effectivement j'ai omis l'essentiel : une organisation composée d'ensembles, et chaque ensemble possède un élément autonome qui fait du sens, et en tant que tel peut être livré bout à bout. 

Pour mieux comprendre ce que l'on recherche, le mieux est d'expliquer qu' "avant", les organisations étaient (ou sont toujours) composées par des équipes sur des expertises, qui fournissent des composants. Par exemple l'équipe de la charte graphique, l'équipe du stockage des données, l'équipe des calculs bancaires, etc. Le souci quand on veut voyager léger pour apprendre vite de son marché lorsqu'il faut faire appelle à toutes ces équipes cela devient une corvée. Et pour accorder ses violons sur un calendrier, c'est l'enfer, le plus lent gagne toujours. Et aussi, chacun travaille dans son coin sur ce qu'il croit être pertinent, mais quand on regroupe tout c'est un peu les [cadavres exquis](https://fr.wikipedia.org/wiki/Cadavre_exquis). 

Alors pour avoir cette approche produit, qui signifie fabriquer en écoutant tout ce qui se passe autour de nous et saisir toutes les possibilités d'amplifier la valeur, on a commencé à penser en équipes dédiées fonctionnalités (*feature teams*). Je suis l'équipe responsable du moteur de recherche par exemple, et je peux dès que je veux mettre à jour le moteur de recherche, inclure de nouvelles choses, en enlever le concernant. Ou je suis l'équipe du "club utilisateur", etc. Il y a toujours des équipes transverses ("la charte graphique", "les composants d'encryption", etc. ), mais elles sont au service et se doivent de proposer aux autres équipes fonctionnelles d'exploiter d'utiliser comme elles le souhaitent ce qu'elles exposent. L'équipe "moteur de recherche" vient piocher et respecter les règles de l'équipe "charte graphique", mais en aucun cas elle ne doit attendre ou être bloquée. Toutes les évolutions technologiques vont dans ce sens : microservices, docker/kubernetes, cloud, etc. on cherche l'autonomie dans le tout. 

Le dernier mouvement en date est d'aller vers des "impacts team", des équipes dédiées impacts. C'est une couche de plus. Les composants au service des équipes des fonctionnalités, les équipes des fonctionnalités au service des équipes dédiées impacts. La raison ? On peut envisager d'améliorer le moteur de recherche, ou les algorithmes bancaires, on a donné de l'autonomie aux équipes, mais cela peut rester morcelé dans l'approche. À la charge des *product managers*, des *head of products*, des *stratèges* de donner une cohérence à toutes ces équipes, cependant cela peut avoir des limites. Et chaque équipe qui avance ne voit pas aussi facilement elle le tout. On se tourne donc vers des équipes dédiées impacts. Elles sont en charge de délivrer un impact, qui va probablement se composer de modification, d'un travail, sur un ensemble de fonctionnalité, sur un parcours. En tous cas on se donne encore plus de liberté : on peut toucher à tout. Et on se met en miroir de ce que l'on recherche : on fabrique l'équipe selon le besoin. 

Là il peut y avoir un jeu de chaises musicales : changement fréquent de la composition des équipes ce qui amène des avantages et des inconvénients. Comme un assemblage mixte d'équipes composants au service d'équipes fonctionnelles au service ou qui laissent les équipes impacts venir dans leur domaine quand c'est nécessaire. Et cela demande une technologie avancée qui autorise cette flexibilité. Comme un sculpteur qui a beau avoir toutes les meilleures idées du monde, il faut que sa matière lui permette la mise en oeuvre.  

Elle - J'entends la notion de feedback rapide. Mais ainsi, livrer souvent = avoir du feedback souvent (même si c'est pour tester des hypothèses) non ? 

Oui on se dit que si on livre souvent, ou rapidement dès le début de la fabrication, on aura régulièrement ou rapidement un apprentissage, des mesures, on sortira rapidement et/ou régulièrement de nos hypothèses pour avancer. C'est vrai. Mais j'entends trop souvent "il faut livrer tous les quinze jours" comme un mantra sans queue ni tête. Il faut *a minima* s'interroger "pourquoi ?", et ainsi *a minima* mesurer : livrer sans mesurer l'impact peut rapidement ne pas avoir de sens (sauf si c'est pour valider une faisabilité technique). Livrer pour livrer n'a pas de sens. Livrer pour mesurer, apprendre, créer de la valeur (à valider), cela a du sens. 

Et oui si tu livres tous les mois tu prends le risque de te tromper sur un mois. Si tu livres tous les quinze jours tu prends le risque de te tromper sur quinze jours. C'est mieux. (Rappelez-vous la prise de risque des projets à un an ou plus...). Il faut que cela reste pertinent pour le service, l'utilisateur, l'usage auquel cela répond. Et il faut faire attention que la pression induite par une livraison la plus régulière possible (on parle de quinze jours, on pourrait livrer tous les jours) ne viennent pas déstabiliser les équipes. 

Mon point de vue est qu'avec le temps et l'expertise technique l'objectif à atteindre n'est pas de se dire que l'on peut livrer très régulièrement (tous les quinze jours, tous les jours, etc.), mais que l'on peut livrer dès que cela fait sens. La technique, l'expertise de "livrer" est devenue une commodité qui ne bloque en rien la décision métier sur le sens de livrer. On livre quand cela fait du sens. Et donc oui si il y a des modifications très régulières et que l'on souhaite au plus tôt bénéficier de celles-ci et apprendre, on livre régulièrement et au plus tôt. Mais cela ne fait pas toujours sens. Donc, n'inversons pas : il faut un outil fluide qui permette de ne plus s'embêter à se demander quand il faut livrer et si on peut livrer. On doit pouvoir livrer quand on veut. La régularité amenant un gage de solidité et de qualité. 


Moi - Je retiens équipes autonomes de bout en bout sur tout le produit jusqu'à sa mise à disposition aux utilisateurs et aux feedbacks.

Oui c'est ce que j'expliquais plus haut. 

Elle- "Aux organisations de sécuriser ces infrastructures", à ces équipes tu veux dire, non ?

Moi - Je veux dire que ces équipes doivent avoir la faculté de pouvoir travailler jusqu'aux clients, jusqu'à l'usage de ce qu'elles développent. Et que le travail des équipes de sécurité, d'infra, d'IT, est de leur mettre à disposition (dans les grosses structures, dans les petites oui elles font tout) toute l'infrastructure à disposition (et aux normes). Et surtout que ces équipes IT, infra, de sécurité se sentent bien au service de la fabrication du produit, elles doivent rester intransigeantes sur leurs critères de qualité, mais cela ne doit pas ralentir ou gêner la prise en main opérationnelle par l'équipe qui fabrique. On évite de changer d'acteur : quand soudainement c'est une autre équipe qui est à la manoeuvre pour la mise en production, cela casse toute la dynamique, tout l'engagement. L'équipe du produit le porte jusqu'à l'usage (la mise en prod et la prise en main par l'utilisateur), les autres équipes mettent à disposition les outils et façons de faire.  

Elle - hum, c'est clair effectivement. Je vois énormément de principes d'agile et de lean startup en effet. Pour ma part, je pense justement que les entreprises "traditionnelles" se posent beaucoup trop de questions avant de se lancer. En gros, quand tu es Amazon, que tu as une infra et des services, des équipes qui sont bien organisées et qui comprennent comment faire vite et bien, ça ne coute pas très cher de lancer et voir. Quand tu es une entreprise "traditionnelle", lancer un "test d'hypothèse" comme celui-là, sachant que rien n'est organisé (infra, services, devOps, etc.) pour faire vite et bien, ça peut coûter tellement de temps et d'argent et de ressources (et d'autres projets qu'ils ne font pas à la place) qu'ils ne se lancent pas dedans. Combiné à cette histoire de "pas le droit à l'erreur sinon je perds ma place" donc regard intérieur et perso, ben bingo, même si tu vois l'opportunité, t'y vas pas...
En gros, tu peux faire du produit comme tu dis, si tu es confortable organisationnellement ET techniquement pour faire vite et bien.
Accelerate : la vitesse conditionne l'excellence.
T'en penses quoi?

Moi - Je suis complètement en phase. Tu cites Amazon. Bezos (que l'on aime ou pas) avait tapé du poing sur la table et mis en demeure toutes les équipes d'exposer une API, c'est-à-dire de permettre de dialoguer à tous moments avec chaque équipe qui laisse toutes les autres venir exploiter ses services. Il a libéré l'organisation de nombreuses contraintes techniques ainsi. Oui il faut un courage managérial, ou une saine panique, pour déclencher ces chantiers. Qui à mes yeux ne se résout pas non plus en "big bang", mais au fil de l'eau.   

Elle - Ce que je veux dire par elles se posent trop de question, c'est que creuser le besoin, voir leur cible, etc. Je pense que ce sont des organisations qui passent beaucoup trop de temps à le faire. Le message aussi derrière livrer toutes les 2 semaines, c'est pour aller vers de l'agilité, tout dépend d'où on vient.

Moi - On est tous un peu façonnés par une idée cartésienne que tout se prévoit, se calcule, se réfléchit. On oublie trop facilement : a) que tout ne cesse de changer et donc que chaque réflexion devient extrêmement rapidement obsolète, b) que les choses sont beaucoup plus complexes et qu'il est impossible de penser à tout. Ainsi dans notre monde la réflexion se nourrit de l'émergence, et donc de l'action sur le terrain. On essaye, on observe, on apprend (je cite un peu là le modèle de pensée [cynefin](/2020/04/meetup-cynefin/)). On gagnerait énormément de temps à d'abord essayer PUIS à se mettre à penser. Mais on a vraiment des freins à cela : le regard des autres, l'idée que la pensée est supérieure à l'action, etc. Il y avait une citation je crois du président de Linkedin qui disait : si tu es content de ton produit quand tu le sors, c'est que tu le sors trop tard. C'est un très bon indicateur, même si il n'est pas valable pour tous les milieux (comme toujours). Laissons-nous surprendre.  

Elle - Un autre point qui me parait essentiel : Tu peux tester quand tu sais arrêter un service/produit. Y a un concept qui s'appelle le cimetière des services Google. Et il est très rempli. Ne pas savoir tuer dignement un produit c'est aussi un frein à en lancer. Mais quand tu sais faire vite et bien, en lancer plusieurs et bien manager les indicateurs notamment d'utilisations, ben tu peux en tuer c'est pas grave, tu passes vite à autre chose. Know how to abort :)

Moi - Oui encore une fois je pourrais citer Linkedin, soi-disant 85% de ce qu'ils ont sorti a disparu, car ils ont vu que cela ne marchait pas. C'est une belle forme de maturité de savoir enlever des choses. C'est signe que l'on s'intéresse vraiment à son produit et à l'expérience associée.  

La conversation continue avec plaisir. 


