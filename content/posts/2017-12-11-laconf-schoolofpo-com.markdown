---
date: 2017-12-11T00:00:00Z
slug: la-conf-school-of-po
title: laconf.schoolofpo.com
---

En novembre 2016 nous lancions avec [Dragos](http://andwhatif.fr) la [school of po](http://schoolofpo.com). Un an après nous sommes ravis de son succès, et de toutes les conversations enclenchées autour de la communauté de PO. Nous avons voulu aller au delà et fort de notre expérience de conférenciers ou d'organisateurs de conférence (ALE 2016, LKFR), avons décidé, avec les chocottes, de proposer "la conf de school of po" : [laconf.schoolofpo.com](http://laconf.schoolofpo.com). 

## Matinée : Inspiration

La matinée nous avons désiré faire une seule piste (pas de session en parallèle) pour avoir une meilleure mémoire collective, que nous vivions tous la même expérience, avec chacun notre perspective, avoir une base commune pour nos conversations, nos souvenirs. Nous avons aussi souhaité que cette matinée soit une matinée d'**inspiration** : nous puisons dans des domaines parallèles pour inspirer nos responsables produits, nos *product managers*, nos *product owners*.  

[{{< image src="/images/2017/12/inspiration.jpg" title="inspiration](/images/2017/12/inspiration.jpg)" >}}

### Alberto Brandolini : learning is key 

[Alberto](https://laconf.schoolofpo.com/speakers/alberto-brandolini/) ouvrira la journée avec son charme, sa gouaille, sa connaissance aiguisée du milieu du *product owner*. L'idée clef qu'il développera durant l'ouverture (seule présentation en anglais) se focalise sur l'importance vitale de l'apprentissage. Apprendre pour optimiser ses options et ses choix. 

### Lamine Gueye : inventer ses limites 

[Lamine](https://laconf.schoolofpo.com/speakers/lamine-gueye/) a inventé son propre parcours, là où les autres voient des limites, il voit des opportunités. Fondateur de la fédération sénégalaise de ski, il décide d'être le premier skieur africain à participer aux jeux olympiques...avant même de savoir skier. Lamine va vous pousser dans le #nolimit. 

### Laure Chouchan : concrétisation

Spécialisée dans les jeux télévisés, directrice des jeux chez Fremantlemedia pendant 10 ans, *Questions pour un champion, Famille en Or, Que le meilleur gagne , Mot de Passe...* , productrice de *Fort Boyard*, conceptrice de formats télés, [Laure](https://laconf.schoolofpo.com/speakers/laure-chouchan/) met désormais son savoir et sa créativité au service de la *gamification*. Elle viendra mettre du concret dans vos idées. 

### Lyvia Cairo : intuition 

Entrepreneuse, poète, coach, [Lyvia](https://laconf.schoolofpo.com/speakers/lyvia-cairo/) suit son *flow*, son intuition, son torrent d'idées. Est-ce que l'on marche sur le bord du précipice en avançant ainsi ? N'y a-t-il pas plein de choses à découvrir ? Plein de choses qui s'enclenchent comme par magie ? L'univers qui joue à vos côtés ?  

**Apprendre, ne pas s'imposer de limite, rendre concrètes vos idées, et suivre votre intuition** voilà ce que nous souhaitons pour nourrir les réflexions qui nous amènerons à l'après-midi des ateliers.  

## Après-midi : Interaction (ateliers)

L'après-midi est consacrée à l'**interaction**, aux **ateliers**, à mettre les mains dans le cambouis, à sentir les difficultés, à plonger dans la mélasse, à apprendre joyeusement à plusieurs, à essayer, refaire, comprendre, intégrer.   

[{{< image src="/images/2017/12/ateliers.jpg" title="interaction](/images/2017/12/ateliers.jpg)" >}}

Du *mapping* en veux-tu en voilà : ateliers sur l'[impact mapping](https://laconf.schoolofpo.com/sessions/impact-mapping-dans-le-xvi-eme/), sur le [user story mapping](https://laconf.schoolofpo.com/sessions/user-story-mapping-pour-le-meilleur-et-pour-le-pire/), et l'[example mapping](https://laconf.schoolofpo.com/sessions/example-mapping-par-la-pratique/). Tout cela avec beaucoup de pratique. 

Du [Storytelling](https://laconf.schoolofpo.com/sessions/scarabee-story-telling/), de l'[UX et du Design Thinking](https://laconf.schoolofpo.com/sessions/ux-thinking-pour-po/), et une perspective sur votre produit au travers des [Wardley Maps](https://laconf.schoolofpo.com/sessions/contextual-awareness-avec-les-wardley-maps/). Tout cela toujours avec les mains dans la glaise. 

Encore quelques ateliers doivent être annoncés (le programme est toujours en phase de fabrication). 

## Ressenti ?

Pour nous ce n'est pas simple, au milieu de notre activité, de lancer cette conférence. Pour le plaisir de tous on espère la réussir. On aime notre programme (heureusement) et notre façon d'aborder l'apprentissage : inspiration puis pratique au travers des ateliers avec de l'interaction. Cette approche s'est développée dans les meetups de la [School of po](http://schoolofpo.com) et nous a enthousiasmé. On compte sur vous pour participer à cette trajectoire. 

[laconf.schoolofpo.com](http://laconf.schoolofpo.com)













