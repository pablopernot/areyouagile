---
date: 2023-08-03
slug: notes-de-conversation-transformation-agile
title: "Notes de conversation : prérequis pour une transformation agile"
draft: false
---

Voici les notes de conversation que j'ai préparées pour une petite intervention en visio avec d’anciennes connaissances du Maroc. Merci à elles et eux. En voici les diapositives. 
Le format est un peu aride. C'est surtout un aide-mémoire pour trente minutes de présentation et trente minutes - une heure de conversation ensuite. 

*cliquez sur l'image pour les diapositives en pdf*

[{{< image src=/images/2023/08/2023-sgmaroc.jpg alt="Diapositives" width="600" >}}](/pdf/2023-sgmaroc.pdf)

