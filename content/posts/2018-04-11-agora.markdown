---
date: 2018-04-10
slug: agora-et-faq
title: "Agora & FAQ"
---

[L’Agora d'Athènes était, durant l'Antiquité, la place principale de la ville d'Athènes, lieu de rendez-vous des flâneurs et la grande place du marché : elle servait au commerce et aux rencontres](https://fr.wikipedia.org/wiki/Agora_d%27Ath%C3%A8nes). Les flâneurs et la grande place du marché, ça ne vous rappelle rien ? Moi si. Notre outil de communication sociale interne (*slack* pour ne pas le nommer), dans l'organisation, ou chez nos clients. On flâne, on discutaille, on observe, on fait savoir, on apprend, on questionne, on s'amuse, mais aussi on ne se comprend pas ou on s'invective, des fois. Il semble que cela vive quoi. 

Et il n'empêche : même si je suis un défenseur de l'interaction et de la colocalisation, il faut bien inventer des espaces de communication, des espaces sociaux qui répondent à notre monde moderne. 

Une interrogation permanente est : Comment les initier ? Comment les faire vivre (avec du sens) ? Comment les rendre utile ? [Alexandre](https://twitter.com/AlexandreThib) est un acharné de *Slack*. Il le propage partout. C'est sa façon de tisser sa toile. De bâtir cette Agora. En plein accompagnement avec une organisation sur deux continents j'ai cherché comment bâtir des conversations comme sur la place grecque. Un autre de mes centres d'intérêts est de révéler les questions non dites. Dans cet accompagnement on a tranquillement depuis quelques mois bâtit une FAQ Agile. 

Ah oui FAQ c'est en anglais *Frequently asked questions* et en français *Foire aux questions*. 

### Une FAQ Agile.     

Fait mille fois ? On s'en fout. Elle se bâtit avec les gens qui sont là. **Elle permet une lecture asynchrone, tranquillement, dans un autre rythme**. Et elle permet **une lecture dans son coin, sans devoir rendre des comptes**. Elle permet d'entendre plusieurs voix. De se rendre compte que des "coachs" ne vont pas répondre d'une même façon. Une réalité sur la complexité de nos contextes. Les questions viennent du terrain, avec leurs coins écornés, elles remuent comme des poissons sortis de l'eau. C'est ce qui se murmure, là, en ce moment.  

Il me parait très important : 

* Garantir l'anonymité des poseurs de questions
* De dater les réponses (et de renseigner qui répond) pour contextualiser la réponse (les choses évoluent, oui).   
* Régulièrement abonder et informer les gens sur la mise à jour de la foire aux questions. Bien marquer que c'est un document vivant me dit [Alexandre](https://twitter.com/AlexandreThib). 
* Les réponses ont le droit d'être hasardeuses. Elles ne sont sûrement pas parfaites. 

Cela fait parti de ces (petites) choses sans prétention, qui marchent bien, qui aident la conversation : un slack, une FAQ, une communauté.

la **FAQ** (à ce jour) : 
[faq-agile.pdf](/FAQ/faq-agile.pdf) <span class="icon icon--pdf">
<svg viewBox="0 0 512 512" width=32 height=32><path d="M438.548,307.021c-7.108-7.003-22.872-10.712-46.86-11.027c-16.238-0.179-35.782,1.251-56.339,4.129
                c-9.205-5.311-18.691-11.091-26.139-18.051c-20.033-18.707-36.755-44.673-47.175-73.226c0.679-2.667,1.257-5.011,1.795-7.403
                c0,0,11.284-64.093,8.297-85.763c-0.411-2.972-0.664-3.834-1.463-6.144l-0.98-2.518c-3.069-7.079-9.087-14.58-18.522-14.171
                l-5.533-0.176l-0.152-0.003c-10.521,0-19.096,5.381-21.347,13.424c-6.842,25.226,0.218,62.964,13.012,111.842l-3.275,7.961
                c-9.161,22.332-20.641,44.823-30.77,64.665l-1.317,2.581c-10.656,20.854-20.325,38.557-29.09,53.554l-9.05,4.785
                c-0.659,0.348-16.169,8.551-19.807,10.752c-30.862,18.427-51.313,39.346-54.706,55.946c-1.08,5.297-0.276,12.075,5.215,15.214
                l8.753,4.405c3.797,1.902,7.801,2.866,11.903,2.866c21.981,0,47.5-27.382,82.654-88.732
                c40.588-13.214,86.799-24.197,127.299-30.255c30.864,17.379,68.824,29.449,92.783,29.449c4.254,0,7.921-0.406,10.901-1.194
                c4.595-1.217,8.468-3.838,10.829-7.394c4.648-6.995,5.591-16.631,4.329-26.497C443.417,313.113,441.078,309.493,438.548,307.021z
                 M110.233,423.983c4.008-10.96,19.875-32.627,43.335-51.852c1.475-1.196,5.108-4.601,8.435-7.762
                C137.47,403.497,121.041,419.092,110.233,423.983z M249.185,104.003c7.066,0,11.085,17.81,11.419,34.507
                c0.333,16.698-3.572,28.417-8.416,37.088c-4.012-12.838-5.951-33.073-5.951-46.304
                C246.237,129.294,245.942,104.003,249.185,104.003z M207.735,332.028c4.922-8.811,10.043-18.103,15.276-27.957
                c12.756-24.123,20.812-42.999,26.812-58.514c11.933,21.71,26.794,40.167,44.264,54.955c2.179,1.844,4.488,3.698,6.913,5.547
                C265.474,313.088,234.769,321.637,207.735,332.028z M431.722,330.027c-2.164,1.353-8.362,2.135-12.349,2.135
                c-12.867,0-28.787-5.883-51.105-15.451c8.575-0.635,16.438-0.957,23.489-0.957c12.906,0,16.729-0.056,29.349,3.163
                S433.885,328.674,431.722,330.027z M470.538,103.87L396.13,29.463C379.925,13.258,347.917,0,325,0H75
                C52.083,0,33.333,18.75,33.333,41.667v450c0,22.916,18.75,41.666,41.667,41.666h383.333c22.916,0,41.666-18.75,41.666-41.666V175
                C500,152.083,486.742,120.074,470.538,103.87z M446.968,127.44c1.631,1.631,3.255,3.633,4.833,5.893h-85.134V48.2
                c2.261,1.578,4.263,3.203,5.893,4.833L446.968,127.44z M466.667,491.667c0,4.517-3.816,8.333-8.333,8.333H75
                c-4.517,0-8.333-3.816-8.333-8.333v-450c0-4.517,3.817-8.333,8.333-8.333h250c2.517,0,5.341,0.318,8.334,0.887v132.446H465.78
                c0.569,2.993,0.887,5.816,0.887,8.333V491.667z"></path></svg>
</span>

### La FAQ sur GitLab

Je ne sais pas peut-être faut-il que cela se propage, s'ouvre à d'autres questionneurs, à d'autres pourvoyeurs de réponses ? Je souhaite aussi que cela garde son sens. Si cette intiative vous intéresse, si vous pensez être intéressé, pour l'instant le mieux est de me contacter et que j'ouvre peu à peu les accès sur les écritures du git si cela vous dit. Et si vous avez d'autres idées je suis preneur.   

C'est écrit en *markdown* (donc simple, lisible, efficace, et **surtout éditable directement dans gitlab**) avec un chouïa de *latex*. 

<span class="icon icon--gitlab">
<svg viewBox="-2 -2 16 16">
<path d="M 11.976562 6.792969 L 11.304688 4.726562 L 9.976562 0.628906 C 9.945312 0.535156 9.855469 0.476562 9.757812 0.476562 C 9.660156 0.476562 9.570312 0.535156 9.539062 0.628906 L 8.210938 4.726562 L 3.789062 4.726562 L 2.460938 0.628906 C 2.429688 0.535156 2.339844 0.476562 2.242188 0.476562 C 2.144531 0.476562 2.054688 0.535156 2.023438 0.628906 L 0.695312 4.726562 L 0.0234375 6.792969 C -0.0351562 6.980469 0.03125 7.1875 0.191406 7.304688 L 6 11.527344 L 11.808594 7.304688 C 11.972656 7.1875 12.039062 6.984375 11.976562 6.792969 "/>
</svg>
</span>
Les **sources et pdf sur GITLAB** : 

https://gitlab.com/pablopernot/areyouagile/tree/master/areyouagile-hugo/static/FAQ

La commande pandoc (*in case*) :

	pandoc faq.markdown --latex-engine=xelatex --template=faq-template.tex -o faq-$(date +%Y-%m-%d).pdf
