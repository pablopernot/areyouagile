---
date: 2023-07-10
slug: deception-satisfaction-experience
title: "Déception, satisfaction, expérience"
draft: false
---

Qu'il s'agisse de boulot, d'équipes professionnelles, de groupe de copains pour la musique, ou le sport, je me suis forgé une idée de l'imposition du management. Ce n'est peut-être qu'une conviction basée sur un biais, car plus je vieillis plus je me trouve constellé de contradictions (à chaque réponse que je donne à une question, dans l'instant qui suit une réponse très différente, voire contradictoire, me vient à l'esprit...). Je dirais cependant que celle-ci, cette pensée, me revient régulièrement, et, même si tout cela reste empirique, je suis prêt à la défendre. D'ailleurs il existe probablement un modèle théorique quelque part qui m'échappe et qui met cela noir sur blanc, preuve à l'appui, bien mieux que moi. 

Si je grossis le trait, j'observe souvent trois types de situation. Qu'il s'agisse encore une fois de management dans un cadre professionnel, ou de quelque chose d'autre dans un cadre non professionnel. En vous écrivant cet article, je me dis que je réécris pour la centième fois la même chose, mais n'est-ce pas là le *job* que de passer et repasser ses réflexions au tamis ?  

Trois situations. Une situation d'échec, une situation de réussite, une situation de réussite avec un vécu dense, une expérience. Ces trois états je les observe et je les vis. J'ai pris mon stylo et j'ai gribouillé. 

{{< image src="/images/2023/07/deception-satisfaction-experience.jpg" title="Déception, satisfaction, expérience" class=center >}}

## Ne réponds pas au besoin

* Management sans espace, micromanagement, ou absence de management
* pas de rigueur, pas d'intérêt, oisif, dilettante, procrastination  
* Résultat : KO, pas au niveau, frustrant, décevant, ce n’est pas bon, c'est dommage

## Réponds au besoin

* Management directif, (re)cadrage, peu de tolérance, recherche d'expertise
* Rigueur, Précision, Rythme (régularité du travail), Stress, Attentes
* OK, efficace, opérationnel, ça marche, c'est bon

## Dépasse le besoin 

* Rigueur sur le cadre, Liberté dans le cadre, Vivre quelque chose, Inspirer
* Recherche d'une expérience, d'un sens,  d'une idée que l'on se fait  
* Innovant, étonnant, on a vécu un truc, bluffant 

## Les passages  

* S'élever de **déception à satisfaction** : Plus de règles, plus de cadre, plus de feedback, plus de sens
* S'élever de **satisfaction à expérience** : Lâcher prise sur les façons de faire, plus d'attention sur le résultat global et sa valeur. Donner de l'espace. 
* Retomber de **expérience à satisfaction** : Reprise en main sur les façons de faire, Attention aux détails, Plus de contrôle 
* Chûter de **satisfaction à déception** : Moins de règles, moins de cadre, moins de feedback, moins de sens
* S'embourber de **expérience à déception** : Plus assez de règles, plus assez de cadre, plus assez de feedback

## Quelques réflexions

* Faut-il nécessairement passer par satisfaction avant expérience ? Il me semble, mais ce n'est pas une certitude. 
* Ce qui est difficile, entre satisfaction et expérience : lâcher prise quand la phase d'avant demandait l'inverse. Le côté contradictoire entre la fermeté du cadre et de la vision, et la liberté et l'espace nécessaire en son sein. 
* Toujours se demander : où suis-je ?   

# Conclusion

J'ai jeté cela en vrac dimanche soir. Cela ressemble à plein de choses déjà écrites par d'autres et par moi. Mais j'avais besoin de le reposer à nouveau. Peut-être différemment. Encore une fois je ne cesse pas de manier cette matière pour de temps en temps découvrir quelque chose de nouveau ou réviser ma pensée.  






