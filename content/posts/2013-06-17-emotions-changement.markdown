---
date: 2013-06-17T00:00:00Z
slug: les-emotions-sont-la-source-du-changement
title: Les émotions sont la source du changement
---

Voici quelques temps que je m'interroge sur la notion de "culture
hacking". Expression difficile à traduire sous laquelle je place
personnellement cette action de détourner, pirater, la culture d'une
organisation pour la faire évoluer vers une autre, que l'on souhaite
meilleure.

Si vous accompagnez des entreprises vous savez la difficulté à faire
changer la culture de celles-ci. Vous savez les efforts -parfois vains-
de répandre un état d'esprit, agile par exemple, dans l'ensemble de
l'entreprise, dans toutes ses strates, des opérateurs terrains au
management en "haut" de la hiérarchie. Exemple typique : on demande de
passer l'organisation à l'agile, vous avez démarré par quelques équipes,
et cela marche. Les ennuis commencent. Comme cela marche on répand de
plus en plus la "bonne parole". Mais ce qui était viable un temps pour
une entreprise : héberger en son sein 2 ou 3 projets agiles, devient un
réel re-positionnement quand il s'agit d'aller plus loin : parcours RH,
changement de type de management, clarification des valeurs et des buts
de la structure, etc. Peut-être est-ce simplement une erreur initiale :
cette entreprise ne voulait tout simplement pas aller vers l'agile mais
suivait uniquement, sans réfléchir, l'effet de mode actuel. A ce sujet
je clarifie désormais systématiquement cette question : voilà ce que
cela va impliquer, êtes-vous toujours partant ? Mais même une entreprise
volontaire pour se lancer dans une telle démarche va potentiellement
souffrir.

Concernant les défis qui nous sont lancés actuellement : collaboration
au lieu de hiérarchie. Autonomie, responsabilisation, et même amour,
disons le, le chemin s'annonce long. Et l'on voit bien que ce n'est pas
les pratiques qui changeront réellement quelque chose, mais la culture
(ok les pratiques sont l'émanation de la culture et vice-verça me direz
vous, peut-être).

## Comment faire ? quelle voie pour changer une culture ?

Ou tout simplement, changer.

Et bien voilà où en sont mes réflexions. **Mes observations indiquent
que les émotions fortes sont la source du changement**.

D'abord il s'agit d'une expérience personnelle. Je traverse une période
avec des émotions fortes et j'observe qu'elles me permettent de penser
différemment. L'émotion forte me permet de sortir du cadre habituel de
ma réflexion. J'en reviens là à une idée forte du livre de [Damasio,
Descartes'error](http://en.wikipedia.org/wiki/Descartes%27_Error)
(l'erreur de Descartes, pas très facile ni agréable à lire mais quelques
idées fortes) qui exprime que la raison sans émotion n'est rien. Aucun
raisonnement n'est valable si il ne contient pas une dose d'émotion.

On peut donc imaginer qu'avec un fort afflux émotionnel, nos
raisonnements soient différents (dans le bon sens du terme). Ce **fort**
afflux émotionnel permet de rompre le cadre trop strict de nos habitudes
et de notre éducation. C'est ce que j'ai pu observer à différents
moments.

C'est aussi ce dont traite le très bon livre de Dan Heath, *Switch*. Dan
Heath utilise une métaphore qui fonctionne très bien : nous sommes un
éléphant et son cornac (le conducteur de l'éléphant). Le cornac répond
aux raisonnements logiques, à l'information claire, il est cérébral, il
veut savoir pourquoi & comment. L'éléphant répond à son instinct, à ses
émotions. Il faut les deux pour avancer. le cornac est impuissant si
l'éléphant refuse d'avancer dans une direction. Aujourd'hui j'estime que
la part de l'émotion est quasiment égale à la disproportion physique
entre un éléphant et son cornac, un être humain.

{{< image src="/images/2013/04/switch-heath.jpg" title="Switch" >}}

Un autre exemple de *Switch* qui m'a beaucoup interpellé ([lectures de
cet automne](/2013/04/lectures-automnehiver-2013/)), d'après mes
souvenirs : un homme découvre que son entreprise manufacturière, dont
l'activité nécessite des gants, achète un nombre incalculable de gants,
dont certains identiques, à des prix très différents. Il rédige de ce
pas un mémo (imaginez un excel avec une ligne plein de chiffres) à sa
direction. Celle-ci en prend bonne note et décide qu'une action sera
lancée lors du prochain vote du budget. Il décide donc de provoquer le
changement différemment : il invite la direction à une réunion. Il
prépare la salle de réunion en plaçant sur un bureau central l'ensemble
des différentes paires de gants achetées par l'entreprise, à chacune il
agraffe son prix (pour montrer les différences même pour des paires
identiques). Lorsque la direction entre dans la salle son émoi est
d'envergure : il faut **immédiatement** changer les choses. Ce n'est pas
tenable...

## Les ateliers ou jeux agiles

Ils participent du même mécanisme. Vous aurez beau exposer que les
explications orales ont mille qualités que les explications écrites
n'ont pas, il sera dur d'aller à l'encontre de toutes ces d'années
d'éducation et d'apologie de l'écrit. Faîtes un atelier/jeu, permettez
aux gens de le vivre, de le sentir (une émotion), et d'observer les
résultats que eux mêmes ont produit (une autre émotion). Là un
changement est possible (j'entends assez régulièrement des retours du
genre "plus jamais je ne ...").

## Comment méthodiquement susciter l'émotion

Telle est donc la question qui me taraude actuellement. Au delà des jeux
et ateliers qui ont la limite d'être souvent des métaphores hors du lieu
du travail. Je veux dire : comment susciter l'émotion directement au
sein de l'organisation sans la protéger par le scénario d'un jeu ou d'un
atelier (repensez à l'histoire extraite de *Switch*). Comment saisir les
occasions, les contextes, les interstices pour provoquer l'émotion qui
permettra aux raisonnements des gens de fonctionner temporairement
différemment et de s'ouvrir ainsi à d'autres options, d'autres voies,
etc.

ma réflexion continue, mais si une occasion de susciter l'émotion passe,
saisisser là !
