﻿---
date: 2020-01-28
slug: comment-sortir-le-mouvement-agile-de-son-atermoiement
title: Comment sortir le mouvement agile de son atermoiement
--- 

Ceci est un simple texte pour exprimer mon ressenti vis-à-vis du mouvement agile. Il s'agit de présenter mes observations avec mes analyses, mes catégorisations, mes convictions, mes propositions. Je vous laisse exprimer les vôtres où bon vous semble. Je n'entends pas que les miennes sont meilleures, juste qu'elles existent, les voici. 

Par "mouvement agile" j'entends la dynamique des entreprises et des organisations à savoir bien évoluer dans le monde complexe actuel. La question est donc, à mes yeux, comment profiter des observations et des connaissances que nous avons pour permettre aux organisations d'avoir plus d'impact, aux personnes d'avoir une vie meilleure, car plus porteuse de sens. 

Aujourd'hui ce "mouvement agile" est moribond. 

Il se singe lui-même. 

D'un côté il perd complètement son sens en s'affublant d'une palanquée de certifications sans queue ni tête et surtout dont le contenu est antinomique avec la pensée qui sous-tend cette adaptation au monde complexe. Ces certifications s'accompagnent de *framework*, socles, pour vendre et rassurer. (voir [Pourquoi faut-il se méfier de SAFe](/2017/11/pourquoi-faut-il-se-mefier-de-safe/)). C'est un échappatoire en or pour les organisations pour garantir l'homéostasie, l'immobilisme. Une bonne nouvelle malgré tout : la supercherie commence à se voir, car le manque flagrant de réussite a du mal à passer inaperçu, et la farce commence à durer depuis trop longtemps pour se dissimuler. 

De l'autre côté, une sorte de *hara-kiri*, *seppuku*, des accompagnants agiles qui, ne trouvant de prise dans les organisations[^1], perdent toute crédibilité en se laissant happer par de la pensée magique, ou une originalité pittoresque, mais vide à ce jour (les alchimistes, les jardiniers, les cultivateurs, les permaculteurs, les circulaires, les épanouisseurs, les ouvreurs, etc.) car elle ne porte pas de fruits. Enfin encore les joueurs, les animateurs de centres aérés, qui ne jurent que par les jeux. Avec les années (car moi aussi j'en ai beaucoup fait, *mea culpa*) j'estime que les jeux agiles sont agréables à réaliser, un support rassurant, intellectuellement un peu pétillant, mais ne débloquent pas de situations, ne provoquent pas de prises de conscience, n'amènent pas de conversations puissantes. Ils se situent dans le domaine du jeu, et pas ailleurs, contrairement à ce que l'on veut faire croire. Comme dans le cas des certifications et des *frameworks* recette magique, les jeux, les dérives magiques, rien n'a porté ses fruits. Aucun résultat en vue. Donc, soit nous sommes très mauvais, soit cela ne marche pas. 

Dans les livres qui compilent les nouvelles entreprises et qui sont chers à cette communauté (*Freedom Inc*, *Reinventing Organizations*, etc.), pas de jeux, pas de *frameworks*, pas de certifications.          

[^1]: Je pense que les jeux agiles existent pour sauver de la folie mentale les accompagnants qui se prennent les systèmes de plein fouet. Une sorte d'échappatoire onirique. Comme le détournement qui a eu lieu sur le "sketching".

## Ce n'est pas le bon sujet de conversation

Le "mouvement agile" se porterait bien mieux aujourd'hui à mes yeux s'il ancrait toutes ses conversations dans le réel, dans la réalité. Qu'il parle le langage des personnes qu'il accompagne. Un langage simple et direct. Je suis avec un entrepreneur je parle des sujets et du contexte de l'entrepreneur. Je suis avec un éducateur je parle des sujets et du contexte de l'éducateur. Etc. Leurs sujets, leurs cadres, leurs réalités. Pas de jargon. Pas d'évasion. Pas d'imposition. Nous venons avec nos convictions, et nos expériences pour enrichir leurs sujets. Leurs sujets sont centraux.  

Ramener la complexité à un langage simple, à des questions simples. Pas simplistes. Ça veut dire aussi ramener les décisions à des choix structurants, puisque ramenés à leurs essences. Si on ramène les questions à leurs fondamentaux, les choix réalisés ont nécessairement de l'amplitude.

Imaginez que nous proscrivions de toutes ces conversations les thèmes du jeu quand le jeu n'est pas le sujet, de la bienveillance quand la bienveillance n'est pas le sujet, du *framework* quand le *framework* n'est pas le sujet, etc. 

Je souhaite des conversations puissantes, des confrontations constructives, qui ne sont possibles que si l'on parle des choses, des vrais sujets. Elles existent, elles sont noyées par d'autres thèmes.

Donc **1) Toutes les conversations parlent concrètement du sujet pour lequel l'accompagnement est requis, avec les mots du contexte. Des mots simples, adéquats, pas de chemins détournés.**  

## La conversation ne se déroule pas au bon endroit. 

Parler du cœur du sujet c'est parler au cœur de l'entreprise. C'est souvent les dirigeants, mais pas toujours. On doit essayer coût que coût d'adresser les vrais sujets dans les vrais lieux. Ce n'est pas facile. Mais il faut le signaler. Indiquer clairement que c'est cette conversation qui devrait avoir lieu. 

**Point 2) Parler au cœur de l'entreprise. Là où se prennent les décisions.**  

Mieux vaut perdre son temps à essayer de faire cela que d'essayer de faire autre chose. 

## Sans intention point de salut 

En clarifiant le propos, en étant concret, on observe rapidement si le désir d'aller dans le sens défini est présent ou non. Ça ne veut pas dire aller vite ou pas. Cela veut dire on valide l'intention. **Point 3) Si l'intention n'est pas là, retirez-vous**, vous reviendrez peut-être quand elle sera prête.   

Encore une fois je ne parle pas de vitesse, l'intention peut être là, et la vitesse très lente. Mais vous savez juger si l'intention est là ou pas. Si il s'agit d'un mensonge (conscient ou pas) ou d'un réel désir. Dans le doute vous pouvez aussi mettre cette interrogation au milieu de la conversation. 

S'il se révèle que l'intention n'est pas là. Merci de vous retirer. En jouant le jeu de la supercherie, en maquillant, en restant présent, vous floutez tous les signaux, tous les messages, toutes les responsabilités. Décider de cela c'est quelques mois (pas quelques semaines, c'est trop court pour savoir). Ne jouez pas le court terme, et le cycle vicieux, jouons le long terme et le cercle vertueux : ne nourrissez pas la bête.  

## 3 points pour sortir de l'atermoiement 

Ne pas faire semblant, ne pas se cacher derrière du ludique ou du magique (pour reprendre les mots de [Laurence](https://www.linkedin.com/in/laurence-wolff-524a261b/?originalSubdomain=fr)).

**1)** *Toutes les conversations parlent concrètement du sujet pour lequel l'accompagnement est requis. Des mots simples, adéquats, pas de chemins détournés*.

**2)** *Parler au cœur de l'entreprise. Là où se prennent les décisions*.

**3)** *Si l'intention n'est pas là, se retirer*. 

