---
date: 2012-01-23T00:00:00Z
slug: coach-retreat-paris-2012
title: Coach retreat paris, 2012
---

Un petit retour sur le "coach retreat paris" 2012 organisé par Oana
Juncu (@ojuncu) avec Yves Hanoulle (@yveshanoulle). Je ne vais pas
revenir sur le déroulement de la journée ni des ateliers que nous avons
pu réaliser car Sat l'a déjà fait, et bien :
[ici](http://satphilora.com/agile-coach-retreat-paris/) (même si c'est
en anglais) ou
[Yves](http://www.hanoulle.be/2012/01/what-is-a-coach-retreat/) (et
encore en anglais). Oui mais alors quoi ? Je souhaite faire un focus sur
3 points : des réflexions sur le format (c'est quoi un "code retreat" ?
, pourquoi "imaginer" ?) et une interrogation (coach/consultant).


{{< image src="/images/2012/01/coachretreat-300x225.jpg" title="Coach retreat" >}}


## D'abord

un détail (mais le diable se cache dans les détails) "code retreat" a
deux significations : tout le monde a en tête la "retraite" ("retreat")
car les mots dans les deux langues sont très proches. Retraite
spirituelle d'un groupe de personnes dans un lieu isolé pour méditer ou
se retrouver. Mais c'est aussi et originellement la "re-treat", traiter
à nouveau. On prend un sujet et on le traite, le re-traite, le traite à
nouveau. Voilà une ambiguïté levée.

## Ensuite

un point que j'ai relevé dès le début de notre journée. Nous nous sommes
projetés sur des situations imaginaires, même si on les savait très
proches de la réalité. N'est ce pas dommage ? Je m'interroge. Pourquoi
choisit-on des situations imaginaires ?

Hypothèse 1 : pour ne pas se focaliser sur la situation de certains ? Ne
pas les stigmatiser, ne pas les mettre mal à l'aise, ne pas révéler leur
situation (on est  entouré de concurrents même si le niveau de confiance
est bon) ? Personnellement cela ne me génerait pas de parler précisément
de certaines de mes situations quitte à me mettre en porte à faux :
l'enseignement n'en serait que meilleur. Quant à la question de la
concurrence, je peux ne pas prononcer le nom de mes clients, si les gens
veulent savoir, ils savent.

Hypothèse 2 : pour ne pas s'enfermer dans une situation bien précise qui
ne parlera réellement qu'à celui qui l'évoque ? Nous avons fait du
*dot-voting* sur ces situations imaginaires, nous aurions pu le faire
sur des situations connues pour être réelles. Quoiqu'il en soit je ne
crois pas qu'une situation ne m'intéresse pas même si je ne l'ai jusqu'à
présent jamais croisée. Je pense que l'on retrouve toujours un
enseignement, un point intéressant que l'on peut mettre en parallèle
avec son expérience. Enfin l'évocation d'une situation réelle portée par
un "product owner"  me parait bien plus tangible et puissante.

Hypothèse 3 : (qui rejoint l'hypothèse 2) les réponses évoquées,
envisagées, dans une situation concrête ne seront valables que pour
celle-ci. Le contexte étant fondamental. Oui. Et alors ? Ce qui
m'intéresse ce n'est pas tant les réponses envisagées, que la façon
d'envisager ces réponses. Quel positionnement, quel raisonnement, quel
mécanisme d'analyse mettons nous en oeuvre, quelles actions proposons
nous ? Voilà la raison de cette retreat/re-treat. Pour ceux qui me
suivent sur Twitter je suis très influencé par
[Cynefin](http://www.cognitive-edge.com/) ces temps-ci. Ce qui
m'intéresse c'est donc les bonnes pratiques ou les meilleurs pratiques
que mes camarades ont pu faire émerger ou utiliser (et c'est d'ailleurs
aussi ce que l'on nous a proposé durant la journée : "clickrewind",
"solution focussed", "crucial confrontations", "appreciative inquiry",
etc. sont des bonnes pratiques). Les moments où ils ont bousculés les
habitudes de leurs clients pour les placer dans un environnement
émergent. Pourquoi, comment, effets observés, succès, échec.
Discutons-en. Sur du concret même si je sais qu'il n'est pas
reproductible c'est la façon de faire (de coacher) qui m'intéresse.

J'ai profité du "Freeplay: no rules. You do as you wish" pour proposer
au groupe d'avoir une approche plus concrête. Pour essayer. Je ne sais
pas si cela est réellement profitable. Mais j'ai fait choux blanc.

## Enfin,

pour finir, l'éternelle question sur le positionnement coach ou
consultant est revenue plusieurs fois sur le tapis. Si je caricature
(volontairement), sommes nous des coach qui murmurons à l'oreille de nos
clients lesquels intègrent, digèrent comme ils le souhaitent, si ils le
souhaitent ? Ou sommes nous des consultants qui indiquons la meilleure
marche à suivre, les processus à déployer, les positionnements à prendre
? Nous sommes les deux. Dans l'absolu (=joker) oui il faut murmurer à
l'oreille de ses clients : nous ne pouvons décrêter qu'ils sont
convaincus, ou motivés. Nous ne sommes pas en mesure non plus de
connaitre la meilleure réponse. C'est lui qui la connait et qui doit
l'exprimer. (J'ai pu -dans une conférence- oser le rapprochement entre
le coach et le psychanalyste). Mais comme les équipes que nous
accompagnons nous avons des "deadlines", des attentes des clients (qui
nous sollicitent) et un produit opérationnel à proposer à intervalles
réguliers, des processus à déployer. Ne cherchons pas à séparer ces deux
facettes de notre métiers. Coach ? Consultant ? Je ne crois pas que l'on
puisse avec Agile être soit l'un, soit l'autre sans y perdre quelque
chose.

Merci à Oana (que j'adore) pour l'organisation de cette belle journée,
merci à Yves pour sa présence.

 
