﻿---
date: 2019-02-10
slug: fabriquer-son-job
title: Fabriquer son job 
--- 

Question lancinante parmi les personnes qui m'entourent, notamment les plus jeunes : comment fait-on pour trouver le job, le projet, la mission, le produit, l'environnement, etc., idéal. Celui que l'on va *kiffer*, aimer, dont on sera fier, pour lequel on se lèvera le matin ? 

Comment le trouver ? Le dénicher ?

En fait la réponse est probablement comment le fabriquer ! Comment le concevoir !

Quand je croise les *benexters* (les personnes de [beNext](https://benextcompany.com)), cette question est récurrente. Je les interroge, 
quel serait le job idéal à leurs yeux ? Et qu'est-ce qui aujourd'hui les empêche de l'obtenir ? Si ils n'avaient aucune contrainte, qu'est ce qu'ils  changeraient aujourd'hui ? Je parle principalement à des *product owners*, des *scrummasters*, des *graines de coach*, des *coachs*, des *développeurs*, des *rh*, des *bizdev*. Je ne sais pas si mon point de vue peut aider dans d'autres environnements, mais je sais que dans le leur cela doit être le cas, je me suis appliqué ces principes depuis longtemps, et j'entends dire que j'ai "fabriqué mon job". 

### Ne pas avoir d'*a priori*

J'ai cessé de demander aux gens dans quel contexte ils aimeraient travailler. J'ai compris que tout cela était une illusion. Quand certains me disent je ne veux pas travailler pour une banque, mais dans les médias ou la culture, je ne veux pas travailler pour une grosse structure, mais pour une startup, je leur explique que j'ai appris au fil du temps que l'on ne pouvait pas savoir ce que nous réservait un environnement avant d'y passer du temps. Que les apparences peuvent se révéler trompeuses. Que le département d'une compagnie n'est pas forcément à l'image du département voisin. Que la startup ou l'organisation média/culture n'ont peut-être pas des organisations à l'image de leur métier, et vice versa. Donc, pas d'*a priori* sauf naturellement si le sujet d'une entreprise est contraire à vos convictions et vous ne souhaitez pas y participer, ce que je peux aisément comprendre.  

Est-ce que tu as essayé ? Et qu'est-ce que cela a donné ? 

### Faire et ne pas faire la leçon

Avant d'avoir de grandes théories sur votre poste idéal, et ce que devrait être l'organisation et ses dynamiques, essayez de mener à bien des actions qui procurent des résultats concrets. Essayez d'entreprendre des choses jusqu'au bout, pour apprendre, pour mieux comprendre. Trop souvent on réclame autre chose sans même avoir essayé de participer à quelque chose jusqu'au bout. Pour avoir un vrai regard sur les choses. Pour mesurer un véritable impact, et apprendre de ce cheminement.  

Est-ce que tu as mené quelque chose jusqu'au bout (même si le bout c'est l'échec) ?  Et qu'est-ce que tu as appris ? 

### Se sentir autorisé

Le plus important pour fabriquer son propre job c'est probablement de s'y sentir autorisé. Et pour s'y sentir autorisé, le mieux est de ne pas avoir à demander l'autorisation. Que l'autorisation soit acquise. Et pour que l'autorisation soit acquise le plus simple c'est de faire quelque chose qui ne nécessite pas d'autorisation. Pour cela le mieux est d'[expérimenter](/2019/02/experimenter/). C'est-à-dire d'avancer à partir d'essais courts sur lesquels on reviendra si ils ne sont pas concluants. Et si ils sont concluants, et bien vous avez commencé à bâtir votre job tel que vous pensiez qu'il devait être fait. Et une autre expérimentation suivra celle qui a fonctionné et qui deviendra un acquis, et puis une autre, puis une autre. Vous aurez constitué beaucoup plus vite que vous le ne pensez un ensemble qui fera sens. On ne construit pas son job du jour au lendemain, on le bâtit au fil de l'eau.    

### Clarifier et sécuriser les autres pour se libérer

Les contraintes proviennent souvent des gens avec qui vous travaillez. Il y a deux postures qui me paraissent importantes pour ne pas être englué dans les sables mouvants d'une relation qui freinerait votre apprentissage. 

La première consiste à clarifier les choses. On vous demande quelque chose qui à vos yeux n'a pas de sens, n'amène pas de valeur, est impossible à réaliser dans le délai imparti. La personne qui demande cela est généralement votre chef. Il est contre-productif, inutile, désagréable, de tricher, d'éviter, ou de saboter. Cela ne vous avancera pas à grand-chose si ce n'est de rendre les choses obscures et plus dures. Par contre je vous suggère de clarifier votre position à ce sujet : d'énoncer clairement que vous pensez que a) cela n'amène pas de valeur, ou b) que c'est inutile, ou encore c) que c'est impossible à réaliser dans le délai donné. Mais en ajoutant : vous êtes mon chef, donc je vais essayer de faire de mon mieux et suivre cette demande. Soit vous vous trompez et vous aurez beaucoup appris. Soit vous ne vous trompez pas et la personne qui vous avait demandé la tâche saura le reconnaître et la relation que vous bâtirez au travail sera probablement riche. Un vrai dialogue semblant être instauré, c'est aussi le meilleur chemin pour fabriquer son job. Mais si la personne ne le reconnaît pas vous resterez néanmoins beaucoup plus audible la prochaine fois que la situation se produira. Personne ne sera dupe, personne ne pourra être faussement dupe, en fait personne n'est jamais dupe, mais là cela deviendra flagrant. Cependant tout le monde à le droit de se tromper. Ce que je veux exprimer ici c'est que la première des choses pour construire une dynamique avec les personnes qui vous entoure c'est de clarifier la situation pour mieux y répondre. En n'étant pas claires, les situations s'enlisent. Avec de la clarté (pas de bravade, pas de provocation) vous saurez si vous souhaitez partir, quand et pourquoi, et si vous souhaitez continuer, et pourquoi.     

La seconde consiste à sécuriser les personnes qui vous entourent. Pour acheter votre liberté, laissez-leur leur tranquillité. Généralement il s'agit de lui éviter d'être responsable. De quoi cette personne a-t-elle peur ? De quoi cette personne a-t-elle besoin pour ne pas se sentir en danger ? Si vous savez répondre à ces questions, vous allez probablement gagner de l'espace pour vous. 


### Mise à jour : 16 juin 2020 

Hier, en sortie de crise du covid j'ai écrit un texte à tous les membres de l'entreprise. J'ai réalisé qu'il était le miroir de ce texte. 

> ...J’entends cependant la frustration qui pointe le bout de son nez : nous sommes sortis du confinement et pourtant les contraintes persistent. Malheureusement c’était prévisible : le lent retour à la normale. Cela va durer probablement quelques mois mais je ne saurais être catégorique. Parmis ces contraintes “ce projet ne me plait pas” je ne veux pas y aller. Je n’impose rien, je vous demande simplement de comprendre que nous sommes encore dans les turbulences. Mais au delà de cette situation j’aimerais encore une fois écrire une de mes croyances : Il n’y a pas de règle généralisable, mais j’ai beaucoup appris que l’on ne pouvait pas juger d’un projet par avance. Je ne veux pas être PO mais PM ? (Vous vous reconnaitrez) Naturellement le meilleur moyen de devenir PM est d’être PO dans la structure, naturellement il y a des PO qui finalement agissent comme des PM et … vice-versa. On ne peut pas savoir comment les choses vont se dérouler avant d’être dans le contexte. Les agilistes répètent à l’envie : “Aucun plan ne survit au premier contact avec l’ennemi”, pourquoi est-ce que cela serait différent avec les descriptifs des postes et les postes eux-mêmes (dans un sens comme dans l’autre : mieux, pire). 
Autre chimère contre laquelle je me bats : je veux être PO dans une startup au coeur du produit dans un vrai environnement startup. Laissez-moi rire. Soit la startup a 4 mois d’existence, soit la politique l’a rattrapée, soit l’absence de cash rend la liberté impossible. Bref c’est pareil. Le poste du PO idéal je ne sais pas où il est. Et une startup n’indique pas que cela sera mieux. Ni moins bien. Encore une fois : “you can’t judge a book by its cover”. Je fais le vieux qui radote ? Donneur de leçons ? Peut-être, made in 1971. le vieux perroquet. Old school boy. C’est mon expérience, j’essaye simplement de vous en faire part. Faîtes-en ce que vous voulez. Je parle de PO/PM, ce n’est pas pareil pour les autres postes ? Bien sûr que si.  Encore une fois : ne cherchez pas le job de vos rêves, fabriquez-le. Vous cherchez le projet de vos rêves, fabriquez-le (sur place). 