---
date: 2014-09-09T00:00:00Z
slug: petit-rappel-historique-ford-toyota
title: 'Petit rappel historique : Ford & Toyota'
---

Au début du XXème siècle, quand John Ford annonce "les gens peuvent
choisir n'importe quelle couleur pour la Ford T, du moment que c'est
noir", il a raison, personne ne songe encore à tous les progrès et les
variations qui suivront. Ford est en train de révolutionner le marché
industriel de la voiture, qui devient à partir de ce moment le meilleur
ami de l'homme. D'autres traits caractérisent cette époque et cette
façon d'aborder le travail : la main-d'œuvre est très bon marché, en
surnombre, pas formée, on va donc baser notre force sur le quantitatif
et non le qualitatif ; c'est l'heure de gloire du taylorisme.

{{< image src="/images/2014/09/taylor.jpg" title="Taylor" >}}

Chez Ford on observe aussi que la dernière étape avant la sortie de la
fameuse Ford T, c'est l'assurance qualité, le test, la recette en
quelque sorte. Et on tri : cette voiture sort, celle-ci non. Rien de
bien choquant pour l'époque, la matière première ne manque pas.

Et quand une voiture sort il faut savoir qu'elle s'accompagne d'un guide
concernant les 150 anomalies les plus probables ou fréquentes. Là aussi,
rien de choquant, en ce début de siècle il suffit de se plonger dans ce
manuel, de s'armer d'un bon marteau, et le tour est joué. La réparation
de la voiture n'est pas un casse-tête chinois, et demeure accessible au
commun des mortels.

50, 60 ans après, chez Toyota les choses ont bien changées. C'est un
pays détruit par la guerre que l'on découvre. La matière première est
devenue un élément critique. On ne peut pas gaspiller ou stocker
exagérément. Les machines outils sont aussi une denrée rare, il va
falloir les exploiter au maximum de leurs possibilités. En un certains
sens, tout comme les hommes, qui, à la demande du gouvernement pour
redresser le pays, sont embauchés à vie par Toyota. La firme japonaise
est donc aux antipodes de la situation de Ford. Enfin la technologie a
aussi changée, et elle continue de changer. Un marteau ne suffit plus
pour réparer sa voiture. Autres époques, autres mœurs.

La réponse de Toyota est révolutionnaire elle aussi : pour tirer le
meilleur de ses employées, qui vont l'accompagner toutes leurs vies, la
firme comprend que le meilleur moyen est de les respecter et les
responsabiliser au maximum. C'est d'ailleurs en les responsabilisant
qu'elle tire le meilleur parti de ses machines-outils. L'homme de
terrain démontrant une capacité indéniable à adapter ces machines et à
avoir des idées pour les exploiter au mieux. La réflexion pour
l'amélioration continue devient une pratique courante chez eux. Pour
palier au problème du stock, Toyota découvre que le meilleur coût
proviendra de deux choses : a) la qualité intrinsèque : dès qu'une
anomalie est détectée on stoppe la chaîne, l'impact et la correction de
cette anomalie seront moindres, et le stock touché minime ; b) une
gestion par flux tendu régulée par une participation active des
employés, c'est eux qui tirent le flux (ils vont chercher et déclencher
leurs actions qui s'adaptent ainsi parfaitement à leurs rythmes et à
leur compétences), et ce n'est pas un flux poussé (là les employés
subissent le rythme).

Entre Ford et Toyota, autres époques, autres mœurs. Observez vos
organisations, de qui s'inspirent-elles ?

Pour en savoir plus je vous recommande la lecture de : **The machine
that changed the world de Womack, Jones et Roos**.

{{< image src="/images/2014/09/machine_that_changed_the_world.jpg" title="the\_machine\_that\_changed\_the\_world" >}}
