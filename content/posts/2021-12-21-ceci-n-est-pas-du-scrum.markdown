---
date: 2021-12-27
slug: ceci-n-est-pas-du-scrum 
title: "Ceci n'est pas du scrum (ou de l'agile)"
---


Ni du scrum, ni de l'agile d'ailleurs. Après une grosse année qui s'achève qui voit la fin d'une aventure ([benext](https://benextcompany.com)) et le début d'une autre ([octo](https://octo.com)), j'ai participé à quelques événements. Le cadre ? Transmettre, partager, la vision, la ligne éditoriale de notre approche agile. Je comprends que cela puisse paraître prétentieux. J'y travaille. Encore une fois je le fais pour moi. Et encore une fois en le faisant j'ai réalisé plein de choses. Voici donc les slides que j'ai utilisés récemment, avec les commentaires associés. 


### Compléments 


* [This is not scrum / agile, la conférence](https://www.youtube.com/watch?v=cXK0T-8R73g) (vidéo/english)
* [Agilité, regard critique sur les dernières années pour bien débuter les suivantes](/2021/02/agilite-regard-critique-sur-les-dernieres-annees-pour-bien-debuter-les-suivantes/) (texte antérieur aux interventions)


---


## 1 Ceci n'est pas du scrum

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.001.jpg" title="ceci n'est pas du scrum" >}}

Vous comprenez vite l'allusion au tableau de Magritte ("ceci n'est pas un pipe"). 

	« La fameuse pipe, me l’a-t-on assez reprochée ! Et pourtant, pouvez-vous la bourrer ma pipe ? Non, n’est-ce pas, elle n’est qu’une représentation. Donc si j’avais écrit sous mon tableau « ceci est une pipe », j’aurais menti ! » -- Magritte

---


## 2 Love/Hate agile

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.002.jpg" title="love/hate agile" >}}

Cela fait un bon moment que nous entendons d’incessantes cassandres qui veulent nous dire que Agile, ou Scrum (qui correspond à un certain environnement agile), sont morts. 
Non toujours pas, tant que le monde n'a pas véritablement changé de nature, agile, comme outil de performance, est bien présent. 

Mais il est certain qu'il y a aujourd'hui peu d'"agile" tel que l'on aimerait en voir. Devenu *mainstream*, Agile est galvaudé. 

Et comme "il faut" être agile, beaucoup s'en drapent en le mimant pour rester dans le coup.  

---

## 3 Ne pas évoquer des "basiques"

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.003.jpg" title="ne pas évoquer les basiques" >}}

Dans cet agile *mainstream* on nous serinne : *back to basics*. À chaque fois que je l'entends, je découvre que cela cache : "faisons semblant !", "donnons-nous l'apparence de ...". Les basiques sont trop souvent dans la bouche de ceux qui en appellent à eux des simulacres pour se simplifier (dans le mauvais sens) la vie. Ne pas faire l'effort, ne pas se donner les moyens de la démarche, même, et surtout, l'éviter, la simuler. 

---

## 4 Les basiques ne sont pas essentiels 

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.004.jpg" title="les basiques ne sont pas essentiels" >}}

Les basiques tels qu'ils sont formulés par la plupart de ceux qui en appellent à eux ne sont pas essentiels, puisqu'ils ne sont que du *décorum*. Passer toute son énergie à faire semblant est source de déception. Ces basiques ne sont que des étiquettes. 

---

## 5 Les fondations sont essentielles

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.005.jpg" title="les fondations sont essentielles" >}}
 
Les fondations sont essentielles à une bonne démarche agile. Mais quelles sont-elles ? 

---

## 6 Basique ou fondation (1) ? 

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.006.jpg" title="basique ou fondation (1) ? " >}}

Le daily standup est quelque chose de basique. L'importance tient dans la synchronisation et le feedback au sein de l'équipe. Souvent le daiuly scrum n'est qu'un affichage. Peu importe avoir un *daily standup* si régulièrement l'équipe se synchronise et se fait du feedback. *Daily standup* est une convention (et un rituel quotidien pour favoriser sa régularité et sa réalisation). À l'inverse Avoir une équipe auto-organisée ne s'affiche pas. Mais là on parle subitement de fond (et non plus de forme). Avoir une équipe auto-organisée est fondamental d'une approche agile. 

---

## 7 Basique ou fondation (2) ? 

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.007.jpg" title="basique ou fondation (2) ? " >}}

Afficher la présence d'un *product owner* est basique, et n'augure de rien. Encore une fois c'est de la forme pas du fond. Au contraire souvent dans un dispositif comme scrum il est fondamental de bien partager et de bien équilibrer les responsabilités en ceux qui décident du QUOI et du POURQUOI et ceux qui décident du COMMENT. Appelez cela comme vous voulez, mais c'est cela qui est important dans une approche façon scrum. 

Dans le fameux assemblage marketing appelé SAFe qui a piqué et assemblé sans queue ni tête tout ce qu'il pouvait pour faciliter cette caricature d'agile, il y a ce PI Planning (qu'on appelait avant Quarterly Planning par exemple). Dans ce *PI Planning* il y a un tableau visuel des dépendances. Et tout le monde de se féliciter que ce tableau est bardé de fils dans tous les sens. Le basique c'est de réaliser ce tableau. La fondation c'est de travailler à la réduction de ces dépendances (qui est un enjeu majeur de l'agile à l'échelle).  

---

## 8 Syndrome du dilettante

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.008.jpg" title="le syndrome du dilettante" >}}

Autre agacement, le syndrome du dilettante, vous pourriez piocher comme bon vous semble dans l'arsenal des outils et rituels agiles, pour constituer votre propre petit kit. C'est faux tel que vous l'entendez. Plus bas je vous dirais que c'est vrai, mais avec une approche très différente de celle que je rencontre dans la très grande majorité des cas. Piocher où bon vous semble apparaît sur le terrain surtout comme une façon d'éviter de s'y mettre vraiment, de saupoudrer, et encore une fois juste dans la forme. C'est pas nouveau, cela ne change pas. 

---

## 9 Cohérence

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.009.jpg" title="cohérence" >}}

*A minima* il convient de piocher avec cohérence. Et la cohérence ne vient pas de la forme (les basiques), mais du fond (les fondations). Et les principes s'équilibrent, se complètent. Pour une vraie performance, il convient de convier à votre petite fête un petit ensemble non négligeable de fondations. L'auto-organisation n'a pas de sens si il n'y a pas une capacité à donner un cadre et une direction, et non pas des directives précises ; l'émergence n'a pas de sens si on n'est pas capable d'introduire une priorisation par valeur, etc.   

---

## 10 Copier n'est pas...

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.010.jpg" title="copier n'est pas..." >}}

Grande faiblesse du management (de la chefferie) nous dit cette étude de 2006 : "les gens (managers, étude dédiée aux managers) copient les pratiques les plus évidentes, les plus visibles, et qui sont fréquemment les moins importantes". 

Voilà tout est dit : pas de retour aux basiques (qui est une façon de grimer ces pratiques les moins importantes, de faire passer la pillule).  

Tout est dit : on fait semblant. 

---

## 11 Pourquoi faire du factice ? 

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.011.jpg" title="pourquoi faire du factice ?" >}}

Mais alors, pourquoi passer cette énergie à faire semblant ? Mais parce que toutes ces actions de surface permettent à ses entreprises de faire semblant sans rien changer. C'est **sans rien changer** qui est important et qui est recherché. 

---

## 12 Et pourquoi est-ce un piège malgré tout ?

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.012.jpg" title="et pourquoi est-ce un piège malgré tout ?" >}}

Malheureuse faire semblant, ne rien changer, simuler, être des charlatans. Et bien cela demande du temps, de l'argent, de l'inventivité même parfois ! Ce n'est pas neutre en énergie. Mais rappelez-vous *ne rien changer*, ce n'est pas une absence d'objectif, c'est un objectif ! Quand on ne change rien, on pense avoir beaucoup plus de chance de savoir ce qui va arriver. Ce n'est pas vrai, mais c'est compris ainsi.   

---

## 13 Pourquoi ne pas s'attaquer aux fondations ? 

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.013.jpg" title="pourquoi ne pas s'attaquer aux fondations ? " >}}

Parce qu'en plus de l'énergie et du temps que cela demande, c'est difficile. Simuler est malgré tout beaucoup plus facile (même si long et coûteux). On paie pour s'acheter du non-changement. Changer les fondations c'est une autre histoire. On ne sait pas ce qui va arriver. Cela remet en question notre socle. C'est ce que l'on évoque parfois dans le changement en parlant de [liminalité](https://en.wikipedia.org/wiki/Liminality) (dans un rite de passage). Ce moment inconfortable de l'entre deux états. En changeant les fondations, c'est là-dedans que l'on se précipite.  

---

## 14 Compliquer à expliquer ? Facile à faire. 

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.014.jpg" title="compliquer à expliquer ? Facile à faire. " >}}

Toutes ces réflexions sur la différence entre le basique et les fondations mettent en évidence un effet intéressant. Ce qui est compliqué à expliquer est souvent facile à réaliser. Comme un meuble IKEA, dont il faut parcourir la notice point à point, avec minutie, pour le voir émerger. Mais c'est relativement facile à réaliser. Comme, à nouveau, ce SAFe, qui nous est présenté sous la forme d'un grand poster dont il faut suivre avec précisions chaque élément (ce que personne ne fait) et qui semble contenir l'ensemble de la mythologie agile. Toute cette complication pour vous permettre de faire des choses superficielles en vous donnant l'impression de vous délivrer du fond. Ou mieux encore en vous délivrant ce qui fera croire que vous faites du fond, car il sature l'espace sur la forme. 

Qu'est-ce que c'est rassurant ! 

---

## 15 Facile à dire, difficile à réaliser

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.015.jpg" title="facile à dire, difficile à réaliser" >}}

À l'inverse, dans notre monde de nombreuses choses faciles à dire se révèlent difficiles à réaliser. La complexité sous-jacente n'est pas visible de prime abord. Et comme toujours répondre à la complexité demande de l'émergence, et la solution sera imprédictible. "Laissons émerger", "Donnons de l'autonomie", "engageons les équipes", etc., etc., c'est loin d'être facile. Mais c'est là que réside souvent le vrai levier de performance. Comme on répond à une complexité, c'est difficile à appréhender, et imprévisible. 

Deux raisons de refroidir certaines personnes. 

---

## 16 Les "entreprises de plus de vingt ans"

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.016.jpg" title="les basiques ne sont pas essentiels" >}}

Ces personnes hostiles à la complexité peuplent souvent les entreprises de plus de vingt ans (c'est une définition arbitraire de ma part). Ces entreprises qui sont souvent nées dans une autre époque, où la complexité n'était pas aussi présente. Des entreprises avec un autre type de culture, une culture qui prévoit un parcours balisé et prévisible pour ces personnes. Et soudain tout s'effondre, la complexité investit l'espace. Pour ces entreprises et les personnes qui les peuplent, trois types de réponses : on évite (en simulant souvent avec des solutions factices et basiques), on simule, on contrôle (là aussi en appliquant des solutions factices et basiques). Pour des exemples : [Premier brief de ce contenu : Agilité, regard critique sur les dernières années pour bien débuter les suivantes](/2021/02/agilite-regard-critique-sur-les-dernieres-annees-pour-bien-debuter-les-suivantes/) . 

---

## 17 Le stade "politique"

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.017.jpg" title="Le stade politique" >}}

Grâce à des *meetups* avec Laurent Morisseau, j'ai découvert Mintzberg (sur le tard donc). Sur [ce schéma (qui vient d'ici)](/2020/04/meetup-entreprises-complexes/) ce qui m'importe surtout c'est de mettre en évidence que le cycle de vie des entreprises potentiellement s'achève avec le stade "politique". Au départ une entreprise s'intéresse à porter un message, à révolutionner un domaine, puis elle s'oriente vers une approche service, ou produit, ou industrielle, pour finalement potentiellement basculer dans le stade "politique". Quand vous êtes une entreprise de services, vous vous interrogez sur vos services, une entreprise produit sur vos produits, une entreprise avec une approche industrielle, sur votre approche industrielle, quand vous êtes devenu une entreprise politique, vos interrogations se portent sur l'interne : qui va prendre quel pouvoir ? Que va-t-on penser (en interne) de la réussite ou de l'échec de ce projet, comment m'assurer que ma carrière (interne) avance convenablement, comment m'assurer qu'après mes trois ans de présidence je puisse rebondir avec aisance, etc. Dans ce cadre toute l'approche agile et la complexité du monde entre en conflit avec l'intérêt politique interne. Et de nouveau comment éviter, simuler, contrôler...

La bonne nouvelle ? Un choc peut faire basculer les entreprises politiques vers des stades antérieurs (service, produit, industrie, etc.). Un choc c'est quoi ? Le covid ? Un rachat ? Un changement de management ? Une concurrence bouleversée ? etc.   

---

## 18 Vous devriez vraiment essayer

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.018.jpg" title="vous devriez vraiment essayer" >}}

C'est triste, car les entreprises de plus de 20 ans pourraient vraiment tirer parti d'une approche, d'une culture plus agile. C'est bien encore aujourd'hui la réponse adaptée au monde dans lequel nous vivons pour être performants. 

Alors pourquoi n'essayent-elles pas vraiment ? 

---

## 19 Courage ? 

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.019.jpg" title="courage ?" >}}

Manque de courage ? Je ne crois pas. Dans l'adversité ou dans leur volonté de NE PAS aller vers ces approches, il faut déjà du courage. En tous cas je n'ai pas observé de cas où soudainement le courage changeait la donne. Je dirais plutôt que ces *top managers* ([car nous sommes bien d'accord c'est les grands chefs qui ont les clefs](https://open.spotify.com/episode/2jWqBst2sxUQA7T7TZKg0A?si=ce284ab411264a73)) se doivent d'être au pied du mur, confronté au risque. Si je caricature : le *middle management* doit être rassuré (car c'est lui dont le schéma est le plus bouleversé), le *top management* insécurisé (sinon rien ne le fera lâcher ses prérogatives). 

---

## 20 Désapprendre

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.020.jpg" title="désapprendre" >}}


Pourquoi se focaliser sur les "entreprises de plus de vingt ans". Une conviction que j'ai apprise auprès de Keith Wyatt, "mon" prof de guitare chez [artist works](https://artistworks.com), désapprendre est bien plus dur qu'apprendre. Désapprendre est la chose la plus dure, aller à l'encontre de ce que l'on apprit, de ce en quoi en croit. Voilà pourquoi c'est bien les entreprises marquées par des convictions, et un passé qui sont sujettes à cette fuite, cet évitement, cette négation. Pour elles l'effort est énorme. Changer constamment pour une entreprise comme a pu l'être benext ce n'est pas si difficile : on grandit, on s'étoffe, nos pratiques et notre culture évoluent de concert. Imaginez benext à 1000 personnes et avec 15 ans derrière nous et demandez-nous de changer, cela serait une autre histoire. 

D'autre part les entreprises de plus de vingt ans comme je l'avais évoqué sont apparues alors que la complexité du monde n'était pas encore aussi marquée et ne demandait pas encore une réponse aussi marquée "agile" qu'aujourd'hui. 

---

## 21 Suspendre

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.021.jpg" title="suspendre ces croyances" >}}

Une chance de ne pas s'enfermer dans ses convictions et de désapprendre malgré tout c'est de savoir suspendre ses croyances.
Pour cela le mot "expérimentation" est clef. Une expérimentation cela reste sur un temps assez court et si cela ne fonctionne pas on sait ou l'on pense que l'on peut revenir en arrière. On ose ainsi suspendre ses croyances, une expérimentation l'autorise. 

---

## 22 Paradoxe

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.022.jpg" title="paradoxe" >}}

Plus haut nous disions que vous ne pouviez pas, vous ne devriez pas piocher où bons vous semblent vos pratiques si cela n'est pas fait avec cohérence. Voilà quelque chose d'assez paradoxal pour présenter une approche qui se veut émergente, adaptative et qui prône l'autonomie. Voilà bien notre paradoxe. On aimerait dire aux personnes : non non vous n'en faites pas ce que vous voulez, et en même temps on leur explique l'émergence et l'adaptation. De quoi devenir fou. C'est une injonction paradoxale dont il est difficile de se sortir. Idéalement on aimerait que chaque groupe, entité, département, essaye "à la lettre" l'approche pour vraiment en comprendre les principes, et **ensuite** libre à lui de les adapter à ce qu'il est. Malheureusement il faut souvent adapter immédiatement pour apprivoiser les contraintes du groupe, de l'entité, du département. 

Mais si vous picorez dans ce qui vous arrange sans vraiment chercher une cohérence ni percevoir les principes sous-jacents...bonne chance ! 

---

## 23 Chance

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.023.jpg" title="chance" >}}

De la chance justement en faudrait-il ? Oui dans cet enchevêtrement il en faut bien. 

---

## 24 Les "bons" coaches, les "bonnes" lectures

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.024.jpg" title="les bons coaches, les bonnes lectures" >}}

On parle beaucoup des entreprises qui évitent, simulent, tentent de contrôler, on parle moins de ce que nous offrons nous en accompagnement. Ce n'est pas plus glorieux. 
Oui vous pouvez tomber sur de mauvais coachs. Pas des mauvaises personnes, de mauvais coachs. Il y en a plein (comme il y en a plein de bons). 

Heureusement on n'est jamais mauvais coach tout le temps et pour toujours, soit c'est cyclique, soit on peut changer, franchir des étapes, évoluer.

Ce qui me dérange plus c'est les dérives associées aux blocages engendrés par les entreprises. Imaginez les murs que les coachs prennent dans les entreprises. C'est un job très difficile si il veut être fait avec exigence. Mais les entreprises simulent, évitent, tentent de contrôler. La position devient intenable il faut se ménager, se trouver un échappatoire. Et fleurissent alors le sketchnoting, les *icebreakers*, les titres absurdes, une logorrhée incompréhensible de gamification à tout va. Je le comprends, mais je ne le soutiens pas. Cela détourne de notre rôle, de notre objectif. Cela entache notre image et donc notre crédibilité. Oui pour des *icebreakers* à bon escient, et pour qu'ils remplissent le rôle de *icebreaker* et pas plus. Non pour qu'ils deviennent l'alpha et l'oméga de notre action et de notre discours, pareil pour le reste, *serious game* et consorts.   

Vous prendrez cela comme une attaque, c'est une défense, nous sommes usés et nous avons honte. 

---

## 25 Trop de substances tuent la substance

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.025.jpg" title="trop de substances tuent la substance" >}}


D'accord entendre que vous avez besoin de vous échapper, d'accord pour entendre que vous avez besoin de vous démarquer, d'accord pour comprendre que vous vous sentez mieux si vous dites que vous êtes si particulier. Mais de grâce, évitons ces titres pompeux qui nous décrédibilisent sur le terrain. En voici un florilège dont je faisais partie ce qui indique que tout n'est pas perdu :)  

---

## 26 Sketchnoting

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.026.jpg" title="sketchnoting" >}}


Voilà à quoi ressemblait du sketchnoting tel que déclenché par Dan Roam (je vous recommande ["unfolding the napkin"](/2012/10/lectures-printempsete-2012/)), un exercice visuel pour articuler les conversations sur ses points essentiels. 

---


## 27 Galimatias

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.027.jpg" title="galimatias" >}}

Voici ce que c'est devenu : un galimatias incompréhensible et inutile (sauf pour celui qui le dessine, pour sa mémoire, je suppose). Encore une fois : à quoi cela sert ? Quelle est l'exigence derrière ? Quelle est la fondation ? 

J'y vois un cri de désespoir devant le mur des entreprises factices qui se dressent face aux coachs. 

---


## 28 Corbillard

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.028.jpg" title="corbillard" >}}

Tout ceci n'est pas très optimiste. Et je ne vais pas aider en racontant ma conversation avec Jeff Patton lors de la [School of Product 2021](http://schoolofpo.com/). Il trainait dans les couloirs comme moi, et [Dragos](https://twitter.com/ddreptate) nous avait fait nous rencontrer. Il se tourne vers moi et me demande : "et vous comment vous vous en sortez", je souffle et je dis "pfff c'est dur". Il explique que pour lui aussi. Nous nous heurtons nous aussi au même mur. Il en vient à me demander d'essayer de traduire avec lui cette maxime pour sa présentation: "le changement viendra avec le corbillard". Oui une bonne partie du changement viendra de la disparation de certains dirigeants qui bloquent trop la situation. 

---


## 29 Espérance de vie des entreprises

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.029.jpg" title="espérances de vie des entreprises" >}}

Un autre facteur similaire pourrait jouer son rôle, l'espérance de vie des entreprises. Les chiffres semblent indiquer qu'elle est passée de 60 ans à 20 ans en un siècle. Mais vingt ans c'est encore long. Et peut-être que les grosses structures de plus de vingt ans que nous évoquons sont trop grosses pour échouer, qu'elles sont devenues structurellement trop importantes pour l'État (ce qui serait dommageable) ? 

Je me dis qu'elles pourraient imploser, car réussissent-elles encore à embaucher ?  

(Une parenthèse sur l'agilité qui une réponse parfaite au capitalisme, à ce que les entreprises répondent bien au monde capitaliste qui nous entoure. Ne soyons pas dupes de nous-mêmes. Et alors que cette pensée me taraude, je tombe sur le livre [Scarlett Salman](https://www.franceculture.fr/personne/scarlett-salman)). 

---

## 30 Points clefs

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.030.jpg" title="points clefs" >}}

Ainsi si je devais me recentrer sur quelques points clefs. 

---

## 31 Regard sur les 3 ou 4 derniers mois

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.031.jpg" title="regard sur les 3 ou 4 derniers mois" >}}

J'estime qu'avant d'avoir un avis il faut du temps, et généralement trois ou quatre mois me semblent une durée adéquate. Vous pouvez avoir un premier avis en quelques jours, mais pour un véritable avis trois ou quatre mois me paraissent nécessaires. Je regrette toujours quand un coach se drape d'orgueil et morgue et décide de partir au bout d'un mois (ou d'une semaine), car "l'autre", "le client", n'est pas "à la hauteur". Évoluer dans la gadoue : dans un environnement loin d'être parfait c'est notre quotidien, mais c'est aussi notre raison d'être dans 90% des cas, aider à sortir de cette gadoue. Et ainsi il me parait que trois ou quatre mois sont nécessaires pour se faire une idée. Si vraiment l'attitude et l'intention ne sont pas au rendez-vous, vous pouvez quitter au bout de trois ou quatre mois. Et vous maintenez cette tranche de temps de façon glissante : si les derniers trois ou quatre mois n'ont rien montré comme avancée, vous quittez. 

Il faut savoir partir quand on n'amène rien, ou quand l'environnement n'est pas à l'écoute et ne propose rien. C'est la loi des deux pieds que l'on s'applique. En même temps (pyramide de Maslow) je ne peux pas en vouloir à ce qui reste coûte que coûte, quitte à devenir un *bullshit jobs* (lisez David Graeber !) quand il s'agit avant tout de préserver une base : toit, nourriture, relation sociale, etc.  


---

## 32 Basique != fondation

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.032.jpg" title="basique != fondation" >}}

Encore une fois, une exigence me paraît indispensable. Celle de s'interroger constamment sur ses paroles, ses actes, et de voir au-delà des apparences pour se focaliser sur les fondations. Encore une fois, vous pouvez réaliser plein de choses superficielles si elles sont nécessaires au chemin vers quelque chose, si elles sont le passage et pas la destination. Mais que ce chemin ne dure pas trop. Comme un plâtre sur une jambe... 

---

## 33 Trop de pathos

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.033.jpg" title="trop de pathos" >}}

Trop souvent on oppose bienveillant et confrontant, bien et mal. C'est trop simpliste. Je vous recommande la lecture (et ce n'est pas pompeux) de "Par delà le bien et le mal" de Nietzsche et celle de "Charmide" (par exemple) de Platon. Dans le premier il rappelle que les choses sont complexes (avec une écriture dynamique, simple), et que le bien ne vient pas forcément de faire le bien, qu'être confrontant, ou en opposition, ou en ne cherchant pas le réconfort, le doux, le propre, amène son lot de bien. Que le "bien" amène aussi son lot de mal. Personnellement je ne cherche pas à ce que mes interventions soient bien, ou réussies, ou bienveillantes, je cherche à les faire atteindre leurs objectifs. Et bien souvent je découvre qu'après un séminaire musclé, cela a bien mieux fonctionné qu'un séminaire indulgent et complaisant. Encore une façon d'appeler à la fin de la tyrannie de ces messages dégoulinants de pathos autour de la bienveillance. Dans le second, on observe Socrate au travers des mots de Platon. Socrate, la maïeutique, l'art de faire accoucher par les paroles. Et bien, n'oubliez pas qu'un accouchement c'est un moment de souffrance, avec du sang et des cris. Dans les propos de Socrate beaucoup de taquineries, voire de la moquerie, des provocations, des brimades. Ce n'est pas le fleuve tranquille que semblent évoquer les gens qui se drapent dans des injonctions à la sympathie. Socrate désarçonne, Nietzsche désarçonne. 

Soyez confrontant. 

---

## 34 Interrogations

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.034.jpg" title="interrogations" >}}

Pour répondre à cette exigence, je me pose certaines questions. Je vous les propose. L'idée est vraiment de **relever notre niveau d'exigence**. 


---

## 35 Pour qui ? 

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.035.jpg" title="pour qui ?" >}}

Pour qui est-ce que je déclenche tel ou tel atelier ? Telle ou telle action ? Telle ou telle proposition ? Qui est le vrai sujet ? 

**Moi**, moi-même le coach ? 

**Client**, mon client ? 

**L'objectif** de l'accompagnement ?

Je peux avoir besoin de déclencher des choses pour moi, ou pour mon client (pour gagner sa confiance, pour lui donner de l'assurance, etc.), mais je devrais très majoritairement essayer de travailler pour l'objectif.  


---


## 36 À quel niveau ? 

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.036.jpg" title="à quel niveau ?" >}}

À quel niveau j'interviens, pour quel niveau j'interviens ? Mieux : où est-ce que je souhaite avoir une caisse de résonnance ? 

**Local** : au sein d'une équipe. 

**organisation** : au niveau de l'organisation, du département, d'un ensemble d'équipe. 

**global** : globalement, partout, au-delà de l'organisation.  

Par exemple : je pourrais vouloir avoir un impact global : pour appuyer mon action locale, pour appuyer le sponsoring. Mais si ce n'est que cela j'ai un souci. 

---


## 37 Matrice embryonnaire

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.037.jpg" title="matrice embryonnaire" >}}

À partir de ces réflexions qui sont au stade embryonnaire, j'ai essayé de m'interroger sur une matrice et sa lecture. Encore une fois c'est l'interrogation sur mon exigence qui m'aide. Voici les quelques premières propositions, elles sont très perfectibles. 

(repris de [Agilité, regard critique sur les dernières années pour bien débuter les suivantes](/2021/02/agilite-regard-critique-sur-les-dernieres-annees-pour-bien-debuter-les-suivantes/) )

#### 1 
Je cherche à me faire du bien à moi en m'appuyant sur un contexte local ? Soyez vigilant ! Trop de principe de plaisir, et de facilité possible. Attention à la complaisance ? Exemple : je fais mon atelier X parce que je l'aime. 
#### 2 
Je cherche à me faire du bien en m'appuyant sur un contexte organisation ? Principe de plaisir ? Quel est l'intérêt ? Est-ce du gaspillage ? Exemple : je fais mon atelier X parce que je l'aime et je le répands dans l'organisation.
#### 3 
Je cherche à me faire du bien en rayonnant sur un plan global ? Principe de plaisir, je reproduis ce que je vois ailleurs pour moi ? Est-ce un abus de position ? Exemple :  je fais cet atelier X que j'aime et qui me fait du bien sur le plan global : je m'inscris dans le mouvement global en communiquant dessus (blog, twitter, etc.). 
#### 4 
Je cherche à faire du bien au client en m'appuyant sur un contexte local ? Grande vigilance, trop de facilité possible ? Suis-je en train de le séduire sans avoir de valeur ajoutée ? Exemple : Je réalise cette action, cet atelier pour satisfaire les attentes du client même ce n'est pas de cela dont il a besoin. 
#### 5 
Je cherche à faire du bien au client en m'appuyant sur un contexte organisation ? Marketing ? Pub ? Est-ce que je fais ma promotion ? Exemple : Je réalise cette action, cet atelier pour satisfaire les attentes du client dans toute l'organisation même si ce n'est pas de cela dont elle a besoin.  
#### 6 
Je cherche à faire du bien au client en rayonnant sur un plan global ? Cargo cult ? Je réplique sans comprendre ou adapter, pour satisfaire. Est-ce de l'imitation rassurante ? Une recette appliquée sans plus ? Exemple : Je réalise cette action, cet atelier pour satisfaire ce qui se fait par ailleurs dans les contextes dont j'entends parler.    
#### 7 
Je cherche à faire du bien à l'objectif en m'appuyant sur un contexte local ? Même à petite échelle c'est bien de servir l'objectif. C'est un petit pas et nul ne sait si sa valeur ne sera pas colossale. Exemple : j'essaye de réaliser une action, un atelier, qui fait avancer un peu vers l'objectif.  
#### 8 
Je cherche à faire du bien à l'objectif au niveau d'un contexte organisation ? Une vraie approche systémique. Exemple : j'essaye de réaliser une action, des ateliers, qui font bouger l'organisation vers l'objectif.  
#### 9 
Je cherche à faire du bien à l'objectif en rayonnant sur un plan global, au-delà de l'organisation. Difficilement compréhensible. C'est plus le mouvement global qui va bénéficier de l'atteinte de l'objectif. Évangélisation ?  

---

## 38 Merci

{{< image src="/images/2021/12/ceci-n-est-pas-du-scrum/ceci-n-est-pas-du-scrum.038.jpg" title="merci" >}}

Merci !











