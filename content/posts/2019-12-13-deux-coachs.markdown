﻿---
date: 2019-12-13
slug: hier-deux-coachs
title: Hier deux coachs
--- 

Hier deux coachs agiles débarquent inopinément (pour un whiskey). On relate les événements de la journée. Le BCG, McKinsey, les conférences frelatées, les coachs qui doivent bien manger ou qui aiment l'argent avant les idées, la grande *comedia del arte* politique chez certains de nos clients. Bref, du whiskey irlandais, Connemara. Un peu tourbé. L'un de ces coachs pense que c'est juste a) pisser dans un violon b) à celui qui pisse le plus loin. 

Mais je ne suis pas de cet avis. Ne pas vouloir ce franc-parler ce vrai débat c'est faire prospérer ce mal. Se taire, c'est être d'accord. Dire que l'on va utiliser le système contre le système, c'est mentir, et nous prendre pour des idiots. Il convient de dire les choses quand elles se produisent. Sans bagarre, sans vouloir sauver le monde, ni se battre. Juste le dire. Sobrement ou avec lassitude, enfin comme vous le souhaitez. 

Si vous le ne faites pas vous serez surpris des incompréhensions, de fausses idées diffusées. À laisser passer toutes ces petites choses, une grosse chose hideuse se nourrit cachée. 

Un métier sain, des idées saines, des savoir-faire et savoir être se bâtissent sur la durée. BCG, McKinsey, SaFE, les charlatans, tous se nourrissent de vos hésitations et de votre fausse pudeur.

Certains crient "maintenant ! maintenant ! Il faut le dire !", pourquoi ce silence assourdissant avant leurs cris ? Commençons tout petit, mais commençons.  
