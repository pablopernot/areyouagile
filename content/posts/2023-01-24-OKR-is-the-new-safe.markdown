---
date: 2023-01-24
slug: okr-is-the-new-safe 
title: "OKR is the new SAFe"
draft: false
---


Il y a deux ou trois ans je tweetais **"OKR is the new SAFe"** (je ne sais plus exactement quand, @tiphanie nous dira on en rigole encore, compte twitter supprimé depuis, retrouvez-moi sur : https://toot.portes-imaginaire.org/@pablopernot). Comprenez bien : cette parodie de titre de série télé met en évidence que SAFe est une supercherie et qu'il faut à OKR faire attention de ne pas tomber dans le même piège. 

Aujourd'hui je confirme avec naturellement avec moins de négativisme que SAFe. Ce dernier c'est un vrai attrape-nigaud, un vrai "snake oil" de vendeurs / charlatans. Mais peu importe SAFe, finalement on s'en fout. 

Il va sombrer et emporter *agile* avec lui, et on s'en fout aussi, car *agile* n'est qu'un mot. 

En 2017 : https://pablopernot.fr/2017/11/pourquoi-faut-il-se-mefier-de-safe/ 

## Le risque pour OKR ? 

Que les clients l'avalent pour la simplicité à le détourner : mettre des chiffres partout qui n'aident finalement en rien. C'est bien il y a des chiffres ! c'est sérieux ! Et il y a des objectifs ! Chiffrés !!!  Qu'est-ce qu'on est bons !!! NON NON NON... non... non.... non (désespéré, fatigué). 

J'en suis même arrivé il y a quelques semaines à proposer à un prospect avec la sempiternelle demande iconique et dramatique, abscon, "mon grand chef veut que nous soyons agiles tous (1200 personnes) dans l'année". Que lui répondre sinon la vérité : cela n'a pas de sens. Il le savait déjà : comme toujours ! Par Jimmy Page (= Dieu) que cette comédie dure et plait ! 

Bref j'en suis arrivé à lui dire : gagnons du temps le plus intelligemment possible : mettons des OKR partout. Cela nous donnera des repères. Cela orientera bien les conversations, cela donnera du temps. Et comme **OKR is the new SAFe** pour le grand chef vous serez tous "agiles". 

Pétard mouillé sans surprise : cela ne s'est jamais déclenché. 

Mais vous voyez le piège.

Et aussi pourquoi il y a aussi un peu d'espoir. 

## La chance pour OKR ? 

Effectivement l'irruption de ces chiffres est comme un cheval de Troie. Les chiffres qui mesurent personne n'est contre. 

Surtout quand on a l'habitude de *ne rien mesurer* mais de mettre des objectifs. OUI d'ailleurs c'est malheureusement le O de OKR. Tiphanie parle d'*ambitions*, bien mieux ! Je prends. 

Si cela marche : l'irruption de ce cheval de Troie permet d'orienter les conversations sur les bons sujets, et de faire réaliser l'importance de la mesure (MESURE avant OBJECTIF ou plutôt l'objectif n'a de sens que si il y a mesure). 

Si cela foire : on énonce des chiffres, on se gargarise, rien n'est mesuré, donc il n'y a pas de réels leviers, de réelles actions et les conversations disparaissent. 

Quand les ingénieurs comprendront-ils que sans terrain leur science n'est qu'une fadaise ? 

J'en profite pour remercier tous ceux qui font preuve de courage pour mettre cela en évidence dans leur accompagnement, au risque de décevoir et d'être dégagé. Je ne dis pas que je remercie ceux qui font des bras d'honneurs et disant que le client est un nul, ceux-là non, je suis contre. Je remercie ceux qui parlent vrai, qui se mettent en risque (*Skin in the game !* Lisez le livre). 

La vérité est un concept épineux. 

Parler vrai c'est comprendre que les OKR sont un cheval de Troie pour les bonnes mesures et les bonnes conversations. Le mensonge c'est juste laisser le client poser plein d'objectifs chiffrés. 

## Le milieu OKR 

Autre chance pour les OKR, le "milieu" me parait plus sain : plus de déontologie que par ailleurs. À suivre : [Tiphanie Vinet](https://www.linkedin.com/in/tiphanie-vinet-a7856143/) et [Laurence Wolf](https://www.linkedin.com/in/laurence-wolff-524a261b/?originalSubdomain=fr) et [leurs livres blancs](https://tiphanievinet-coaching.medium.com/sorganiser-pour-avoir-de-l-impact-124e2a37046d), [Laurent Morisseau](https://www.morisseauconsulting.com/a-propos/) et son [Rising Goal](https://risinggoal.com/), [Guy Lerat](https://www.linkedin.com/in/guy-lerat-jr/) qui lui emboite le pas. 

Ah merde je réalise que c'est un gros parti pris, je suis/fut le manager et j'ai fait grandir Tiphanie et Laurence SI SI !!! Je suis dans le *board* de Rising Goal, et Guy a fait parti de mes cycles de mentoring. 

Alors plutôt : je compte sur vous !  
