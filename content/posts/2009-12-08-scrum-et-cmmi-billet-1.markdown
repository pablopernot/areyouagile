---
date: 2009-12-08T00:00:00Z
slug: scrum-et-cmmi-billet-1
title: Scrum et cmmi, billet 1
---

Je souhaite réaliser une petites séries de billets dont le but est une
mise en perspective entre Scrum et CMMi. Ces billets n'ont pas pour but
de préconiser l'un plutôt que l'autre mais de faire partager mon humble
expérience à ce sujet : je viens de passer plus de 3 années au sein
d'une structure dont CMMi a été l'épine dorsale. A ce titre et ayant
œuvré en tant que consultant ou chef de projet j'ai mis en œuvre des
pratiques CMMi jusqu'au niveau 4. J'ai abordé Scrum il y a 3 ans mais
cela n'a pas été retenu alors dans cette structure, et seulement depuis
fin 2008 les projets Scrum sont déroulés. La question revenant
régulièrement sur le tapis, écrire des billets à ce sujet me permet
aussi d'organiser ma pensée.

Naturellement la première chose que je vais entendre (ou lire) c'est :
CMMi est un modèle et Scrum une méthode. On ne peut donc pas les
comparer. Je rejette cet argument dans le sens où chaque société
implémente CMMi "à sa guise", et donc je compare l'implémentation de
CMMi (telle que je l'ai connu) à l'implémentation de Scrum. Il est
d'ailleurs assez amusant de constater que cette défense de CMMi vis à
vis de Scrum prolifère récemment alors que l'implémentation de CMMi est
-très- sérieusement bousculée par l'implémentation de Scrum.

Oui il y a une opposition philosophique entre ces deux méthodes, c'est
pourquoi l'une bouscule l'autre à mon sens. Par contre il est évident
que les deux peuvent dans bien cas se conforter.

Je dois dire qu'au sein de l'organisation dans laquelle j'ai déroulé les
pratiques CMMi l'arrivée de Scrum est réellement vécue comme une bouffée
d'air frais. Surtout en raison d'un monopole trop totalitaire de cette
doctrine (CMMi). Je suppose que dans quelques années un monopole de
Scrum pourra tout aussi bien être bousculé par une nouvelle approche.

Les points que j'espère aborder :

-   Le couple : manager/CMMI leader/Scrum (je tire cette terminologie
    (manager/leader) de l'excellentissime: [Lean Software Developpement,
    an agile
    toolkit](http://www.amazon.fr/Lean-Software-Development-Agile-Toolkit/dp/0321150783)
    (il se lit comme un roman je vous le recommande chaudement)).
-   Pour le nouvel arrivant, le débutant, quels sont les bénéfices et
    inconvénients de CMMi et Scrum.
-   Les bénéfices et les risques CMMi & Scrum, je vais essayer de dire
    quand est-ce que je m'appuierai plutôt sur Scrum ou plutôt sur
    CMMi,et dans quels cas les deux peuvent cohabiter voire se
    renforcer.
-   d'autres peut-être !

J'espère ne pas me perdre, ni être trop ambitieux et surtout vous donner
mes arguments de terrain (donc forcément il seront "teintés").
