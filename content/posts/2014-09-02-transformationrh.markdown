---
date: 2014-09-02T00:00:00Z
slug: transformation-rh
title: Transformation RH
---

Les RH sont morts ! Vive les RH ! Cette formule historique prononcée par
le Duc D'Uzès est représentative du débat qui m'anime avec [Jas
Chong](http://thedignityofwork.com/). C'est vrai que notre rencontre fut
insolite : elle assista à ma [conférence avec Oana sur Open Agile
Adoption lors du Scrumday 2014](https://vimeo.com/104987420), et me
déclara tout de go (c'est une asiatique après tout) : tu as été trop
condescendant. Peut-être aussi parce que j'avais débuté sa session
*openspace* sur les RH par *les RH doivent disparaître* ?

## Les RH doivent-ils disparaître ?

Qu'est ce qui se cache sous cette formule à l'emporte pièce ? Les RH que
j'ai pu croiser ont perdu leur sens. Ils devraient être les grands
facilitateurs de l'organisation, ils n'en sont que les nettoyeurs, ou
pire les exécuteurs des basses œuvres. Grands facilitateurs de quoi ? de
la culture de l'organisation, des interactions, des dynamiques. Or ce
rôle de méta-facilitateur c'est aussi celui que j'attribue dans une
organisation moderne au *top management*.

Dans nos organisations modernes les dirigeants ont perçu l'inversion des
rôles opérée ces dernières années : ils n'oppressent plus, ils
émancipent, ils ne dominent plus, ils soutiennent. Émanciper, soutenir,
n'est-ce pas là le rôle des RH ? Ainsi ils disparaissent car leur
fonction se fond avec celle du dirigeant de l'organisation.

## Quel sens d'avoir un RH réduit aux basses besognes ?

C'est sûr ce point que le discours de Jas et le mien se rejoignent quand
elle fait appel au terme de *dignité*, ou qu'elle évoque une *crise
d'identité*. L'état actuel des RH est une très mauvaise nouvelle. Les RH
sont morts, vive les RH ! Leur marge de progression est énorme.
Doivent-ils disparaître ? Peut-être pas si ils saisissent l'opportunité
de ce nouveau positionnement de méta-facilitateur au sein de
l'organisation ; laissant aux dirigeants les aspects opérationnels.

## Transformation RH

Fruit de cette réflexion et de discussions non moins achevées, nous
proposons donc une approche commune que nous avons appelé -- dans un
éclair d'originalité -- [Transformation RH](http://transformationrh.fr).

Vous pouvez nous croiser ce mardi 16 septembre à l'hôtel Westin Vendôme
(Paris) pour une matinée qui éclairera nous l'espérons ces réflexions,
nous fera partager nos points de vues, échanger, et nous donnera, je
l'espère, un premier plan d'action au travers d'un atelier. Vous pourrez
aussi nous croiser à *Agile Tour Brussels* fin octobre, et nous espérons
aussi dans d'autres événements.

<p><span class="fa fa-group"> Le site 
<a class="reference external" href="http://transformationrh.fr">Transformation RH</a></span>, et 
<span class="fa fa-map-marker"><a class="reference external" href="https://www.weezevent.com/transformation-rh-matinee"> l'inscription pour la matinée du 16 septembre</a>.
</span></p>

C'est là qu'intervient l'agilité. Jas s'intéresse à l'agile, je
m'intéresse aux RH. Nous savons tous les deux que la culture et les
pratiques agiles sont tout à fait en ligne avec l'idée que nous nous
faisons des RH.

## La culture agile

Qui conçoit mieux que la communauté agile/lean l'importance actuelle de
la culture d'entreprise ? Quelle action plus critique pour une
organisation que l'embauche ? (Les gens généralement enclins à associer
l'agile avec une sorte de bienveillance naïve ont-ils noté
l'inflexibilité justifiée des organisations agiles à se séparer des
personnes n'étant pas conformes à leur culture ?). Quelle action plus
critique pour une organisation que le maintient de sa culture, ou
l'adaptation de sa culture aux nouveaux défis ? Quel meilleur levier
pour une organisation que sa culture ? Mais que veut dire culture ? Et
comment cela s'adresse-t-il ?

## Outillage agile

L'outillage agile va surtout servir à replacer les RH dans le rythme de
l'organisation. L'agilité va permettre une concordance et une résonance
entre l'organisation et ses RH. Concordance dans sa capacité à évoluer
et faire évoluer l'organisation et les gens au même rythme que l'objet
de l'entreprise elle-même (d'où ma préférence sans conteste pour les
termes "People & Organisation", "les gens et l'organisation", plutôt que
le classique "Ressources Humaines"...). Résonance en s'appuyant sur
l'importance du *feedback*, en adaptant constamment ses pratiques à
l'organisation.

Vous aurez plus de détails ici : [Transformation RH](http://transformationrh.fr)
