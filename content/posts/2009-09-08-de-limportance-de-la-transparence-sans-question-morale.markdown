---
date: 2009-09-08T00:00:00Z
slug: de-limportance-de-la-transparence-sans-question-morale
title: De l'importance de la transparence
---

La notion de *transparence* est très importante dans les méthodes agiles
type Scrum (mais pas uniquement là). C'est une façon d'être (être
transparent) qui devrait être appliquée dans tous les cas (Agile, CMMI,
qu'importe même aux projets sans méthode). Chez Scrum on associe la
notion de transparence a celle de *courage* il me semble, sous-entendu :
qui aurait envie d'aller se confronter à un client pour lui annoncer une
très mauvaise nouvelle, ou qui a envie de prendre le risque de dénoncer
le fait qu'il a échoué, etc, ce n'est jamais simple mais c'est
essentiel.

Le mot courage est assez parlant, il faut l'associer dans notre cas à la
maturité, l'expérience. A mon avis le courage de la transparence vient
avec les années, avec l'expérience. Mais ici nulle question de moralité
contrairement a ce que pourrait sous-entendre *transparence* que l'on
rapprocherait de *honnêteté*, mais juste de réalisme économique, bref
lorsque l'on travaille dans le privé : de maturité ! En effet, et c'est
pourquoi j'insiste sur le fait que cela n'est pas exclusivement lié aux
projets agiles, le fait de *ne pas*être transparent avec son client
aboutira systématiquement par une perte de productivité et d'efficacité.
Au plus tôt une anomalie est décelée, au plus tôt elle doit être
traitée, au plus tôt un quiproquo voit le jour, au plus tôt il faut
lever celui-ci. Tout ce qu'on laisse en chemin de façon délibérée
(opaque) se retrouvera nécessairement sur notre route plus tard, et
devra être traité avec des coûts plus élevés sans nul doute. C'est pour
cela que je parle de productivité et d'efficacité au travers de la
transparence. C'est une notion révélée par les méthodes agiles car elles
nous obligent -par une constante itération et par mise à nu fréquente
des résultats- à être transparent.

Suis-je clair ? :)

Je rebondis en fait sur un commentaire réalisé ce jour [ici](http://generationagile.blogspot.com/2009/08/lhonnetete-de-le-dire.html).
