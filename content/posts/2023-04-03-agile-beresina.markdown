---
date: 2023-04-03
slug: la-berezina-de-nos-organisations-et-de-l-agilite 
title: "La Bérézina de nos organisations et de l'agilité"
draft: false
---

La Bérézina est une bataille perdue par Napoléon. Elle symbolise le retrait et l'épreuve humaine. Elle reste dans les mémoires comme symbole de la fin d'une époque glorieuse, et le début d'une dislocation. 

Je crois que c'est où nous en sommes actuellement quand on parle d'agilité en France (et je présume en Europe, et je ne regarde pas plus loin). Comme je l'écrivais très récemment, je suis effaré du niveau catastrophique de l'agilité et des agilistes que j'observe désormais autour de moi, sur les réseaux, dans les conférences, etc.
Le fond n'est pas mort, mais l'idée certainement, jusqu'à ce qu'elle resurgisse peut-être un jour. 

# Fin du syndrome de l'imposteur et de jouer sur les mots

Mais alors je parle d'agilité ou d'organisation ?  C'est pareil mon petit monsieur.

Alors pourquoi ? Et d'abord est-ce vrai ? 

Oui c'est vrai. 

J'ai beau être un vieux(?) con(!), je vous confirme que nous sommes en plein marasme.

Est-ce que j'ai de la légitimité pour décréter cela : oui. J'assume et j'en ai marre du syndrome de l'imposteur.

À ceux qui disent l'agilité blah blah blah : je suis navré pour vous que, contrairement à ce que vous croyez, vous ne l'ayez jamais croisée. 

Alors d'où cela vient-il ? Là, ce matin voilà ce qui me vient : 

# Le virus de la facilité

D'abord j'imagine que c'est un peu comme une maladie qui se répand, à la façon des zombies dans un film d'épouvante. Vous avez d'un côté une personne qui prend le temps de réfléchir, qui possède de la rigueur, qui est persistant, etc, cela prend du temps. Vous avez de l'autre côté des gens qui passent des certifications, ou qui s'annoncent coach agile sans aucun recul sur la profession. (N'en déplaise aux coachs pro le coaching agile est un métier bien à part qui demande un véritable
apprentissage). Alors sans surpris la personne qui travaille avec qualité se fait complètement débordée par celles qui sont là pour autre chose. 

Je suis le premier à avoir dit aux gens : vous voulez être coach agile ? Facile faite un blog avec ce titre, et c'est réglé. Mais faire un blog sur le sujet c'est déjà autre chose que de passer une certification de deux jours. Comme si je vous mettais une méthode de guitare dans les mains pendant deux jours et que le lendemain vous deviez faire un concert pour toute une organisation. C'est un peu ce qui se passe. 

Je n'en veux pas, enfin pas trop, aux personnes. Elles doivent d'abord vivre, survivre. Et une certification n'amène rien sauf malheureusement pour l'agile, heureusement pour ces personnes, un tarif journalier plus haut.    

La nature plébiscite la flemmardise, la simplicité, une certif' en deux jours, un simulacre d'accompagnement, tout échoue, mais personne ne le râle, alors allons-y franco se dit-elle !  

# Le déni des organisations

La majorité des organisations se prennent en pleine poire : la fin des ressources, la pandémie, la folie du capitalisme (à outrance, ou du capitalisme tout simplement), la fin des idéaux. Elles sont paumées. En tous cas les gens qui les gouvernent le sont. Pris de panique leur mot d'ordre est surtout de ne rien changer. On arrive prêt du mur, on ne sait pas réagir, on ne sait que freiner (mais on va quand même se prendre le mur). Il y va du "à quoi bon". 

Et ne rien faire c'est plébisciter l'inaction, les processus vides, les slides, les certifications vaseuses, en un mot, les simulacres.  

Voilà comment on se retrouve avec des organisations qui font tout pour éviter de véritablement s'interroger et se transformer, et que l'on retrouve une palanquée de personnes dont, soit l'incompétence, soit la naïveté, soit le manque de discipline de travail et d'esprit, nourrissent le vide.   


# La bonne nouvelle

Même si des mots deviennent lourds à porter, les gens qui délivrent de la qualité sortiront toujours du lot. Libre à vous d'en faire partie.
Pas de stress, j'observais une demande pour un accompagnement et des centaines de personnes y répondrent : pas de stress, c'est l'effet de masse habituel. Ne le craignez pas. Seul 4 ou 5 sont de vrai(e)s candidat(e)s sérieux/sérieuses. Et si ils ne sont pas pris en compte, la demande est un simulacre, donc pas de regret.  

Un dernier mot, la qualité ne se juge pas à l'expérience, mais à l'intention et à l'attitude. C'est donc tout à fait dans les cordes d'un débutant. Après un débutant aura les forces d'un débutant (un certain enthousiasme), et quelqu'un d'expérience, et bien aura de l'expérience. 


