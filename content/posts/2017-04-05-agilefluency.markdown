---
date: 2017-04-05T00:00:00Z
slug: perspective-agile-fluency
title: 'Perspective plutôt que framework : Agile Fluency'
---

Alors que la grande conversation autour des framework agiles (*Less,
Safe, Nexus*, etc) semble battre son plein je reste convaincu qu'une
approche avec une perspective de principes comme *Agile Fluency* est
bien meilleure. C'est tout récemment en évoquant le duo *Agile Fluency*,
*Open Agile Adoption* (c'est à dire *OpenSpace Agility* comme l'a
renommé Dan Mezick) que je me suis mis à revoir la présentation de Diana
Larsen. C'est ce visionnage de sa keynote à Agile India 2015 qui me
pousse à réécrire sur ce sujet ([chemin d'une transformation agile](/2015/06/chemin-dune-transformation-agile/)).

Au risque de me répéter donc ([chemin d'une transformation
agile](/2015/06/chemin-dune-transformation-agile/)) je vais rappeler les
messages clefs de la présentation de Diana Larsen lors de Agile India
2015. Rappeler que à mes yeux associer cette approche, avec une conduite
du changement menée par invitation, autorisation et clarification du
cadre comme le suggère *OpenSpace Agility*, est probablement ce à quoi
je crois le plus, et ce que j'observe comme de plus juste et de plus
pertinent sur le terrain.

Ici je complète un peu ce qu'est *Agile Fluency*, surtout en reprenant
les bases données par Diana Larsen dans sa conférence. Dans un prochain
billet j'espère expliquer pourquoi il fait un binôme très intéressant
avec *Open Agile Adoption* a.k.a *OpenSpace Agility*.

## Questions des niveaux

{{< image src="/images/2015/06/fluency.jpg" title="Agile Fluency" >}}

Agile Fluency se base un cheminement que l'on associe rapidement à des
niveaux. J'ai pu détailler les niveaux précédemment (encore [chemin
d'une transformation
agile](/2015/06/chemin-dune-transformation-agile/)). Je les résumerais
succinctement ici (puis plus en détails en traduisant le document de
Diana Larsen).

### Pas d'étoile : on code

Avant les étoiles, on produit, on code, on fabrique. On ne se pose pas
de questions.

### 1 étoile : focus sur la valeur

Questionnez votre organisation/équipe : est ce que l'on pense et on
mesure la valeur ? Et décide selon la valeur ?

Ce niveau c'est celui de l'**agile classique, fondamental**.

### 2 étoiles : délivrer de la valeur

Est ce que l'on délivre au marché quand cela fait sens pour lui ? Avec
de la qualité (pas de bug) ?

Ce niveau c'est celui de l'**agile soutenable, durable**.

### 3 étoiles : optimiser la valeur

Est-ce que l'on sait délivrer, mesurer, adapter, optimiser. Est-ce que
l'on sait supprimer ce qui n'a pas assez de valeur ? Est-ce que l'on
sait maximiser (maximiser la valeur et minimiser l'effort). Est-ce que
vos équipes sont pluridisciplinaires pour répondre convenablement à ces
questions ?

Ce niveau correspond à ce que les gens espèrent quand ils pensent agile,
celui des **promesses de l'agile**.

### 4 étoiles : optimiser le système

Est-ce que l'on sait délivrer, mesurer, adapter, optimiser, supprimer,
maximiser avec toute l'organisation en perspective ?

Ce niveau c'est celui du **futur du l'agile**, il répond à ceux qui se
demandent ce qui viendra probablement ensuite.

## Maturité ?

On va vite parler à tord de niveaux comme si il s'agissait de niveaux de
maturité, façon CMMi. Pas du tout. Lisez plutôt ces niveaux comme des
**niveaux d'investissement et de bénéfices attendus, pas de maturité**.
Si vous êtes prêt à investir quatre étoiles, vous pourriez obtenir
quatre étoiles de bénéfices. Mais, et avec le mot *étoile* on peut
rapidement faire le lien avec ce guide pour les restaurants, vous
pourriez certains jour vouloir un simple mais délicieux et très adéquat
jambon/beurre, et d'autres jours vouloir déguster un plat bien plus
élaboré. Pour *Agile Fluency* c'est pareil rien ne dit que votre
organisation a envie de ce plat élaboré, et peut-être qu'elle se sentira
bien mieux avec un simple jambon/beurre. Il n'y a donc pas de niveaux
dans le sens d'une progression, mais plutôt d'un lieu qui vous convient.
Et le lieu des deux étoiles est peut-être bien plus adéquat pour votre
organisation que celui des quatre étoiles.

Gardez aussi en tête que se rendre au lieu des quatre étoiles demande
des investissements de type quatre étoiles. Le bénéfice est à la hauteur
de votre investissement dans la démarche. Comme on le dit si souvent, et
comme on l'oublie si souvent : il faut se donner les moyens de la
démarche.

## Transparence, aboutissement, alignements, les bénéfices et lectures de l'Agile Fluency

<table border="1" class="docutils">
<colgroup>
<col width="14%" />
<col width="29%" />
<col width="28%" />
<col width="29%" />
</colgroup>
<tbody valign="top">
<tr><td><strong><span class="caps">AGILE</span>
<span class="caps">FLUENCY</span></strong></td>
<td><p class="first"><strong><span class="caps">TRANSPARENCE</span></strong></p>
<p class="last"><em>Métriques clefs et
réduction du&nbsp;risque</em></p>
</td>
<td><p class="first"><strong><span class="caps">ABOUTISSEMENT</span></strong></p>
<p class="last"><em>Productivité et <span class="caps">ROI</span></em></p>
</td>
<td><p class="first"><strong><span class="caps">ALIGNEMENT</span></strong></p>
<p class="last"><em>Productivité et&nbsp;satisfaction</em></p>
</td>
</tr>
<tr><td rowspan="3"><p class="first"><strong>Focus
sur
la&nbsp;valeur</strong></p>
<p class="last"><strong>*</strong></p>
</td>
<td><strong>Métrique clefs</strong>
L&#8217;équipe reporte
régulièrement son avancement
sur un plan valeur <em>business</em></td>
<td><strong>Productivité</strong>
L&#8217;équipe s&#8217;observe, améliore
et ajuste son fonctionnement</td>
<td><strong>Productivité</strong>
La collaboration au sein de
l&#8217;équipe réduit les
incompréhensions et les
intermédiaires.</td>
</tr>
<tr><td><strong>Réduction du risque</strong>
Le management sait quand
l&#8217;équipe construit quelque
chose de non adéquat, et peut
changer la direction.</td>
<td><strong><span class="caps">ROI</span></strong>
L&#8217;équipe donne la priorité
aux 20% qui font 80% de la
valeur.</td>
<td>&nbsp;</td>
</tr>
<tr><td><strong>Réduction du risque</strong>
Le management sait si
l&#8217;équipe progresse.</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr><td rowspan="3"><p class="first"><strong>Délivrer
de la&nbsp;valeur</strong></p>
<p class="last"><strong>**</strong></p>
</td>
<td><strong>Métrique clefs</strong>
L&#8217;équipe démontre qu&#8217;elle
peut régulièrement délivrer</td>
<td><strong>Productivité</strong>
Avec un taux de défauts très
bas, l&#8217;équipe se focalise
sur la réalisation de
fonctionnalités.</td>
<td><strong>Productivité</strong>
La qualité du travail
améliore le moral et la
productivité.</td>
</tr>
<tr><td><strong>Réduction du risque</strong>
Avec un délai court entre
l&#8217;idée et la livraison,
l&#8217;équipe met en évidence
les défauts systémiques
(<em>voir le tout</em>) rapidement</td>
<td><strong>Productivité</strong>
Une faible dette technique
permet un meilleur coût
de développement et des
livraisons plus fréquentes.</td>
<td><strong>Satisfaction</strong>
Les livraisons sont prévues
quand cela fait sens
pour le marché et le
management.</td>
</tr>
<tr><td><strong>Réduction du risque</strong>
L&#8217;équipe est toujours
en capacité de délivrer</td>
<td><strong><span class="caps">ROI</span></strong>
L&#8217;équipe délivre au rythme
du marché et capte de la
valeur plus fréquemment.</td>
<td>&nbsp;</td>
</tr>
<tr><td rowspan="3"><p class="first"><strong>Optimiser
la&nbsp;valeur</strong></p>
<p class="last"><strong>***</strong></p>
</td>
<td><strong>Métrique clefs</strong>
L&#8217;équipe reporte des mesures
<em>business</em> comme le <span class="caps">ROI</span>,
la satisfaction utilisateur
ou le profit par employé.</td>
<td><strong><span class="caps">ROI</span></strong>
L&#8217;équipe délivre des
produits en fonction des
objectifs <em>business</em> des
besoins du marché et des
besoins utilisateurs.</td>
<td><strong>Productivité</strong>
Une expertise large et
partagée permet d&#8217;éliminer
les temps d&#8217;attente et
d&#8217;accélerer la prise de
décision.</td>
</tr>
<tr><td><strong>Réduction du risque</strong>
Les projets ou produits avec
peu de valeur sont arrêtés
assez tôt.</td>
<td><strong><span class="caps">ROI</span></strong>
L&#8217;équipe apprend des retours
marchés/terrains comment
créer de nouvelles
opportunités <em>business</em>.</td>
<td><strong>Productivité</strong>
Une confiance mutuelle
entre l&#8217;équipe et
l&#8217;organisation permet
d&#8217;accélérer les négociations.</td>
</tr>
<tr><td>&nbsp;</td>
<td><strong><span class="caps">ROI</span></strong>
Une expertise large et
partagée permet
une optimisation des
décisions effort/valeur.</td>
<td>&nbsp;</td>
</tr>
<tr><td rowspan="2"><p class="first"><strong>Optimiser
le&nbsp;système</strong></p>
<p class="last"><strong>****</strong></p>
</td>
<td><strong>Métrique clefs</strong>
L&#8217;équipe reporte comment
son action impact le système,
l&#8217;organisation.</td>
<td><strong><span class="caps">ROI</span></strong>
L&#8217;équipe et le management
travaillent conjointement
pour améliorer le flux
de création de valeur.</td>
<td><strong>Productivité</strong>
Pollinisation croisée
des perspectives et
opportunités au sein de
l&#8217;organisation.</td>
</tr>
<tr><td><strong>Réduction du risque</strong>
Les projets à fort potentiel
mais à coût élevé sont
abandonnés rapidement.</td>
<td><strong><span class="caps">ROI</span></strong>
L&#8217;équipe participe au succès
de l&#8217;organisation au travers
du succès du produit.</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>


## Alignement méthodes

De fait *Agile Fluency* est agnostique. Mais si on voulait lui donner
une lecture au travers de méthodes et au regard des différents niveaux
qu'il propose, voici ce qui semble couler de source :

**1 étoile** : Scrum, Kanban et consorts.

**2 étoiles** : Extreme Programming, Software Craftsmanship, Devops, et
consorts.

**3 étoiles** : Lean Software, Lean Startup, Design Thinking, et
consorts.

**4 étoiles** : Systèmes adaptatifs complexes, Beyond Budgeting,
Holacratie, Sociocratie, et consorts

## Liens

### Agile Fluency

-   [Dancing Along the Agile Fluency Path by Diana
    Larsen](https://www.youtube.com/watch?v=OlPP3kQmpV8)
-   [Your Path through Agile
    Fluency](https://martinfowler.com/articles/agileFluency.html)
-   [Agile fluency project](http://www.agilefluency.org/)
-   [Chemin d'une transformation
    agile](/2015/06/chemin-dune-transformation-agile/)

### OpenSpace Agility

-   [OpenSpace Agility](http://openspaceagility.com/)
-   [Histoires d'Open Agile
    Adoption](/2014/04/histoires-dopen-agile-adoption/)
-   [Darefest 2015: Open Adoption](/2015/11/darefest-2015/)

