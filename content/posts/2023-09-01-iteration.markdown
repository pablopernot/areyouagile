---
date: 2023-09-01
slug: pourquoi-des-iterations 
title: "Pourquoi des itérations ?"
draft: false
---

Avec pour objectif de redorer les approches agiles, qui ne sont pas rouillées -- mais ce qu'on en a fait est assez pathétique -- , je vous propose de nous poser quelques questions simples, mais importantes. 

Une question : pourquoi faites-vous des itérations ? Tout le monde parle itération. Cela semble un indicateur d'une approche agile. Ma question, je la répète, est donc : pourquoi faites-vous des itérations ? 

Pourquoi agile propose de faire des itérations et en quoi cela le distingue d'une approche projet classique (*waterfall*, cycle en V, etc.) ? Et quand les projets cycle en V, *waterfall*, classique vous disent *on fait des itérations* en quoi diffèrent-elles drastiquement ? (ou alors en quoi ont-ils une approche dite agile sans le savoir ?)

Prenez cinq ou dix minutes de réflexion avant que je ne vous influence par mes propos, puis je viendrais à mes propositions. 

5mn ? 

10mn ? 

...

...


Les itérations servent à **minimiser les risques** et à **optimiser ce que vous fabriquez ou ce que vous faites**. Je vais détailler et cela va impliquer certaines **conséquences**. 

## Minimiser les risques

**Minimiser les risques** en ayant un regard plus tôt sur ce que vous êtes en train de faire. Si vous fabriquez quelque chose et que vous faites "tada le voilà" au bout d'un an, vous prenez un risque de un an. Si vous fabriquez quelque chose qui est constitué d'un assemblage de douze fois un mois, vous prenez douze fois un risque d’un mois. Un échec d’un mois, ça se rattrape beaucoup plus facilement qu'un échec de un an, et cela a d'autres avantages.    

Mais **conséquence** : pour cela il faut que chaque chose qui sorte lors de chacun de ces mois que vous puissiez la jauger véritablement. Cette chose qui se fabrique, qui se fait, en douze morceaux tous les mois ne peut pas soudainement vous apparaître à la fin de l'assemblage sinon, sans le vouloir, vous avez pris un risque de un an et c'est justement l'option que vous vouliez éviter. C'est généralement l'erreur des projets classiques (*waterfall*, cycle en V) qui vous disent faire des itérations, mais dont le véritable "tada le voilà", n'est bien qu'à la fin du cycle.  La difficulté est donc de penser ce que vous fabriquez pour avoir une capacité à faire "tada le voilà" tous les mois, ou toutes les deux semaines, ou tous les jours, etc. L'approche agile permet de minimiser les risques : plus vous faites des efforts pour raccourcir ces délais, moins vous avez de risques, moins vous faites d'efforts, plus vous avez des risques. Mais il faut savoir faire ce découpage particulier, différent (ce n'est pas le sujet de cet article, mais vous avez pas mal d'articles sur le sujet dans ce blog). 

Et autre **conséquence** qui découle de la première, il faut pouvoir jauger votre "tada le voilà", est-ce que ce que vous avez réalisé convient ? Car de quels risques parle-t-on ? Nous parlons du fait que ce que vous faites soit utile, ait de la valeur. Et donc comment jauger à intervalles réguliers et -- espérons -- courts que ce que vous faites ai de la valeur, soit utile ? Là aussi c'est à vous de placer le curseur, mais une approche agile va vouloir valider (donc éliminer des risques) en mettant en œuvre ce que vous faites ou ce que vous avez fabriqué en condition réelle : est-ce que c'est utilisé ? Est-ce que cela marche ? Est-ce que cela plait ? Est-ce que cela correspond ? Etc., etc. Plus vous faites ce test en conditions réelles : par exemple en commençant à distribuer ce que vous avez fabriqué et à le faire utiliser, plus vous apprenez et vous savez si vous avez réussi ou échoué, plus vous réduisez vos risques.
Si vous décidez en regardant sur votre écran de développement (par exemple) de dire que cela marche, vous êtes éloignés des conditions réelles, vous apprenez moins, vous ne savez pas vraiment si c'est un succès ou un échec, et plus vous faites persister le risque, et celui-ci grossit au fil du temps. 

**Observations** si donc vous ne testez pas le plus possible "en conditions réelles", ou si même vous ne regardez pas le résultat de vos itérations, d'une part vous n'avez pas une approche une agile (ce n’est pas très grave) mais surtout : vous prenez de gros risques d'aller à la déception plus tard. Enfin faire des itérations sans bénéficier de son intérêt c'est surtout mettre les équipes ou vous-même en stress sans raison, et le stress conduit à de mauvaises choses. 

## Optimiser ce que l'on fabrique ou ce que l'on fait 

**Optimiser ce que l'on fabrique ou ce que l'on fait** en ayant ce regard régulier évoqué plus haut. Un véritable regard, sur quelque chose dont vous pouvez jauger la pertinence. C'est comme si vous ouvriez un livre tous les mois (pour reprendre l'exemple plus haut), plutôt que tous les ans. Vous découvrez tous les mois (dans notre exemple) si vos choix sont les bons, s'ils fonctionnent, et cela vous permet de changer ou d'adapter tous les mois (ou toutes les deux semaines, ou tous les jours, etc., et pas tous les ans). Quand on se lance, on part sur des hypothèses (plus ou moins fiables), et quand on est arrivé à destination nos hypothèses sont devenues des réponses (bonnes ou mauvaises). Si vous avez des itérations, vous avez des réponses beaucoup plus régulièrement, vous n'avez pas la peine d'attendre le "voilà tada !!" au bout d'un an. 

Mais cela n'est vrai encore une fois que si les **conséquences** évoquées plus haut sont prises en considération. Vous devez pouvoir jauger de la pertinence de ce que vous délivrez à intervalles réguliers. Ce qui fait qu'une **approche non agile** est un assemblage dont le résultat est utilisable uniquement en toute fin de cycle. Et ce qui fait qu'une **approche agile** le résultat devrait être utilisable à intervalles réguliers et qu'il s'étoffe au fil du temps, car non, pas de magie, on ne peut pas avoir en un mois ce qu'il faut un an à réaliser. 

**Conséquence** : après l'apprentissage, l'optimisation possible à chacun des intervalles, ce que vous faites ou fabriquez, change potentiellement (souvent) au fil de temps et ne ressemble pas complètement (ou pas du tout) à ce que vous aviez envisagé au tout début. 


Voilà, j'espère que nous sommes en phase, et il doit être facile de vous dire maintenant si vous utilisez les itérations à bon escient, et de la bonne façon (si votre objectif c'est d'avoir une approche agile, non, pardon, le terme agile on s'en fout, plutôt : si votre objectif est de diminuer les risques et d'optimiser la valeur ou l'utilité de ce que vous faites ou réalisez). 



