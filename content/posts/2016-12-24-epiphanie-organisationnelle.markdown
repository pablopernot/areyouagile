---
date: 2016-12-24T00:00:00Z
slug: epiphanie-organisationnelle
title: Epiphanie organisationnelle
---

L'épiphanie (du grec ancien ἐπιφάνεια, epiphaneia, « manifestation,
apparition soudaine ») est la compréhension soudaine de l'essence ou de
la signification de quelque chose. Le terme peut être utilisé dans un
sens philosophique ou littéral pour signifier qu'une personne a « trouvé
la dernière pièce du puzzle et voit maintenant la chose dans son
intégralité », ou a une nouvelle information ou expérience, souvent
insignifiante en elle-même, qui illumine de façon fondamentale
l'ensemble. Les épiphanies en tant que compréhensions soudaines ont
rendu possible des percées dans le monde de la technologie et des
sciences. Une épiphanie célèbre est celle d'Archimède, qui lui inspira
son fameux Eurêka ! (J'ai trouvé !), raconte
[wikipedia](https://fr.wikipedia.org/wiki/%C3%89piphanie_(sentiment)).

Au vingt troisième siècle avant Jimmy Page c'est effectivement une
épiphanie que vit Archimède en sautant de son bain, en criant "Eurêka",
en réalisant la fameuse poussée d'Archimède. Au début du XVIIe siècle,
avec le [procès de
Galilée](https://fr.wikipedia.org/wiki/Galil%C3%A9e_(savant)), c'est
peut-être une épiphanie collective qui frappe une partie de la
population européenne. Il se peut que la terre tourne autour du soleil,
puisque l'on en parle, que les gens ne sont pas d'accord, qu'un procès
autour de l'héliocentrisme copernicien a lieu. Des esprits vivent
probablement des "AHA moment", des moments "eurêka". En 1963, Dick
Fosbury s'élance et saute d'une façon complètement nouvelle. Pour les
autres compétiteurs c'est une épiphanie. Ils ne pourront plus sauter
comme ils le faisaient. Leur compréhension de leur sport est
révolutionnée.

{{< image src="/images/2016/12/fosbury.jpg" title="Dick Fosbury" >}}

C'est ce genre de révélation qu'il nous faudrait au niveau
organisationnel. J'avais une conversation avec
[Bruno](https://brunoboucard.com/) sur la régression intellectuelle que
nous vivons ces dernières années, et plus précisément sur celle de la
posture managériale. Nous envisageons de faire de nos conversations une
session, et [Bruno](https://brunoboucard.com/) évoque sa lecture de Kant
et du procès de Galilée comme une soudaine réalisation de la raison
envers la foi. Ce moment de la conversation me fait vivre lui-même un
petit "aha moment". C'est cela que je poursuis : faire réaliser,
révéler, de façon introspective, l'écart qui défini le paradigme entre
l'ancien mode managérial et le nouveau, entre l'ancienne culture
organisationnelle et celle que nous appelons de nos vœux et qui se
matérialise de nos jours au travers de l'entreprise libérée, de
l'holacratie, de la sociocratie, les niveaux jaunes et/ou vert de la
spirale dynamique, et de tous ces nouveaux modèles. Mais est-ce
seulement possible ?

Je réalise que nous subissons la révélation copernicienne, ou la
nouvelle technique de Fosbury, mais que le moment Eurêka de Archimède,
ou de Newton recevant la pomme sur la tête sont des découvertes
personnelles. Pour la première c'est une **rupture** flagrante, comme
l'innovation, à laquelle nous sommes soumise, pour la seconde c'est un
cheminement interne, une **révélation**.

## Rupture

Mais est-ce possible d'avoir une rupture perceptible flagrante autour
d'une organisation systémique, à partir de quelque chose qui nécessite
de voir le tout, de penser de façon holistique ? Est-ce que comme le
réchauffement climatique ou les problèmes écologiques, ou d'autres
phénomènes qui ne sont compréhensibles qu'à travers des nombres dont on
ne comprend pas le sens à notre échelle, ou dont on ne perçoit les
effets que trop tard, est-ce possible d'avoir une épiphanie
organisationnelle ? Nous ne percevons pas aisément le mal que nous
faisons quand nous consommons trop, ce n'est pas facilement palpable à
notre niveau, ce n'est pas envisageable par notre cerveau d'*homo
sapiens*, car c'est systémique, holistique, il faut voir le tout. Comme
les irradiations de Tchernobyl c'est un mal invisible dont on ne
percevra que les symptômes que trop tard.

On voit des organisations qui marchent très différemment les unes des
autres et elles nous inspirent. Peut on pour autant s'en servir comme
révélateur ? Tant qu'elles seront minoritaires c'est assez difficile.
Tous les contextes, tous les paramètres qui s'entrelacent. On ne peut
pas reproduire du complexe aisément.Les gens qui attendent de notre
accompagnement de la répétabilité se fourvoient. Pas de rupture avec un
phénomène intangible et complexe. Il faut attendre que statistiquement
la majorité fonctionne ainsi, il n'y aura ainsi pas de révélation mais
un glissement.

Si révélations il y a elles seront probablement personnelles et
viendront de l'intérieur, pas comme les sauteurs en hauteur qui
subissent la démonstration de Fosbury. L'ancien système peut résister
grâce à la difficulté de lecture que nous avons avec cette approche
holistique/systémique du monde complexe. (D'autant qu'il se maquille au
travers de monstres que l'on appelle SAFe -- un faux système moderne
actuellement en vogue --, certifications ou autre et falsifie la
réalité).

## Révélation

Je me suis un peu interrogé au travers de quelques lectures de ci de là
sur les moments eurêka. Les grecques pensaient que c'était les muses qui
pourvoyaient les humains de ces moments soudain et révélateur. Cela peut
marcher, j'en sais quelque chose. Mais les dernières études montrent
surtout que ces moments eurêka, ces épiphanies proviennent d'un long
travail d'intégration d'information, d'une capacité de divergence et de
penser hors de sa zone de confort et que soudain une nouvelle
information vient donner la clef. Ce n'est pas la partie analytique du
cerveau qui fonctionnerait, mais bien la partie droite, émotionnelle,
créative. Enfin les deux mais pas au même moment. Une large part est
faîte à la récolte des données, à l'analyse. Mais la révélation ne vient
pas de ces moments. Elle arrivera subitement par la découverte d'une
nouvelle pièce qui conclura le puzzle. La partie droite du cerveau est
moins précise mais ses ramifications sont plus étendues. Naturellement,
comme toujours, vouloir forcer le moment eurêka, c'est le repousser.

Quelques lectures :

-   <http://www.newyorker.com/magazine/2008/07/28/the-eureka-hunt>
-   <http://lemondeetnous.cafe-sciences.org/2013/09/eureka/>
-   <https://hbr.org/2014/03/how-to-have-a-eureka-moment>
-   <http://bigthink.com/think-tank/eureka-the-neuroscience-of-creativity-insight>
-   <http://uk.businessinsider.com/where-do-eureka-moments-come-from-2015-1>

## La forêt

{{< image src="/images/2016/12/648x415_20mn-10044.jpg" title="Forêt" >}}

Cette partie du cerveau qui nourrit les épiphanies c'est aussi celle du
langage quand il traite des métaphores ou des jeux de mots, il doit
travailler avec une compréhension étendue. "Le langage est tellement
complexe que le cerveau doit le gérer de deux façons différentes en même
temps," dit le chercheur Jung-Beeman. "Il a besoin de voir la forêt et
les arbres simultanément. L'hémisphère droit vous aide à voir la forêt."
(The eureka hunt).

Collusion des "adjacents possibles", sérendipité, "interpréteur
métaphysique", c'est en étendant les ramifications de son cerveau droit
que l'on voit apparaitre les nouveaux paysages qui nous aideront à
percevoir différemment nos organisations, et à comprendre ces nouvelles
étapes à franchir. C'est aussi pour cela que je me sens inextricablement
attiré par ce type de visions : dans les [organisations
vivantes](/2016/10/organisations-vivantes-votre-aide/) j'évoque
bidonvilles, formes (le pli, le papillon, etc.) de la théorie des
catastrophes de Thom, cristaux, termitières, etc. Les réponses viennent
de la confrontation à d'autres champs de recherche m'avait glissé Mary
Poppendieck quand je lui parlais de ces bidonvilles et de ce texte. En
introduisant des termes comme village, tribu, cercle, vous ouvrez des
métaphores, des analogies, dont les ramifications pourront aller plus
loin que vos termes classiques, historiques, comme "équipes" ou
"départements". **Une nouvelle raison de conspuer les systèmes comme
SAFe ou consort : tout cela retarde ou obstrue une épiphanie
organisationnelle générale dont nous avons grandement besoin**.

## Pour résumer

Pour résumer je réalise qu'il sera très difficile d'avoir une épiphanie
collective sur des aspects systémique lié à l'évolution de la
compréhension de nos organisations. Mais que travailler pour avoir des
épiphanies personnelles est vital. Les ingrédients de celles-ci seront
des phases de collecte des informations et d'analyses classiques. Une
invitation sera toujours nécessaire (on ne force pas une épiphanie), une
découverte par l'expérience personnelle (qui renforce notre capacité de
mémorisation). Une propension à s'ouvrir : collusion des "adjacents
possibles", sérendipité, etc. Une révélation qui mettra à jour une
nouvelle perspective en associant un dernier élément qui résoudra le
puzzle. Et un vrai shoot de dopamine et donc une addiction à cette
nouvelle ouverture d'esprit.
