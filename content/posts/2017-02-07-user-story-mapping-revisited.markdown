---
date: 2017-02-07T00:00:00Z
slug: cartographie-plan-action-revisitee
title: Cartographie de plan d'action(user story map) revisitée
---

Une autre histoire sur la *User Story Mapping*, la cartographie de plan
d'action. Une autre histoire ? Le but ce n'est pas de trouver de
nouveaux formats pour trouver de nouveaux formats, le but ce n'est pas
d'inventer pour inventer, mais des fois vous vous trouvez en réunion, en
atelier, en *brainstorming* et il y a un point à résoudre. Vous avez
intégré les principes de la *User Story Map*. Vous avez compris la
capacité de cet outil à proposer une **mise en perspective visuelle
efficace**, sa capacité à proposer aux acteurs un **champ d'action
autant auditif, visuel, que kinesthésique** (on touche, on manipule), la
qualité de son **séquencement soigneusement pensé pour établir du relief
dans la pensée ainsi que dans le déroulé tactique**. Et vous vous
trouvez en réunion client, en atelier, en *brainstorming*, et rien ne
vous semble plus pertinent que d'utiliser les principes de la *User
Story Map*, de les revisiter sous un autre angle en exploitant ses
qualités.

Cet article n'a d'intérêt que si vous connaissez déjà la [cartographie de plan d'action, la User Story Map](/2017/01/cartographie-plan-action/), il en est un complément.

## Une *roadmap* ?

Il m'est arrivé récemment d'utiliser le formalisme de la *User Story
Map*, de la cartographie de plan d'action, pour décrire et séquencer une
*roadmap*, un *portfolio* de projets. La colonne vertébrale s'est
transformée en la liste des grands axes que ce *portfolio* de projets
devait traiter. Puis il a fallu remplir (ou pas) chacun de ces grands
axes. Intéressant de voir les différentes densités de colonne (comme si
il s'agissait de *swim lanes* d'un
[Kanban](/2016/01/portfolio-projets-kanban-partie-1/)). On a pu mettre
en évidence les points de vigilances comme c'est le cas dans une *User
Story Map* classique.

{{< image src="/images/2017/02/user-story-mapping-roadmap-1.jpg" title="User Story Mapping Roadmap  1" >}}

Naturellement on a demandé à ordonner, prioriser par importance et par
dépendance chaque colonne. La *User Story Map* permet à un groupe de
travailler conjointement, en parallèle. Puis on a voulu s'imaginer que
ce *portfolio* devait projeter l'année à venir, et au lieu d'étapes ou
de graduation d'importance (*Must, Should, Could, Would/Won't*) on a
décider d'afficher les quatre semestres à venir. Là c'est devenu très
concret. Je ne suis pas en train de vous dire que les estimations sur la
capacité par trimestre étaient bonnes. Juste en train de vous dire que
cela devenait concret. Il n'était pas question de brimer ce travail
tactique sur des enchainements de projets par des contraintes. Et
naturellement il a fallu disséminer les éléments dans les différents
semestres *de facto*, et ne pas les laisser tous dans le premier...

{{< image src="/images/2017/02/user-story-mapping-roadmap-2.jpg" title="User Story Mapping Roadmap  2" >}}

Cette dissémination dans différents semestres (*quarter*) des éléments
des colonnes a de nouveau donné un jeu visuel intéressant, et aussi mis
à jour des dépendances inter-colonnes, comme c'est souvent le cas avec
une *User Story Map* ("Impossible de faire ce projet deuxième semestre
si cet autre ne commence qu'au troisième semestre dans la colonne
adjacente car ils sont liés").

Cependant cette approche pourrait amener un équilibre entre les
différentes colonnes qui ne serait pas forcément réel, bienvenu,
logique. En effet chaque colonne possède potentiellement des éléments
dans le premier semestre. Comme si on avait une vision à plat et que
chaque élément de la colonne vertébrale ai son lot de priorités
importantes. Comme si par exemple chaque bloc de la colonne vertébrale
était une source de demande, elles vont toutes avoir des priorités
importantes, sans regard les unes vis à vis des autres. Encore une fois
ce n'est pas forcément réel, bienvenu, logique. On pourrait utiliser la
ligne supplémentaire ("j'ai mal mais ça marche"), comme une ligne
"points clefs de la stratégie" et ne pas équilibrer entre les colonnes
mais faire apparaître les points clefs de la stratégie (en provenance
d'un *impact mapping* que nous verrons dans l'article suivant).

{{< image src="/images/2017/02/user-story-mapping-roadmap-3.jpg" title="User Story Mapping Roadmap  3" >}}

Sur chaque élément nous avons pris soin de faire une estimation très
rapide (S, M, L, XL, XXL), de marquer les composants métiers ou
techniques associés (comme sur des fiches
[Kanban](/2016/01/portfolio-projets-kanban-partie-1/)). Cela a donné de
la consistance aux conversations autour de la densité des éléments par
semestre, ou sur les questions de dépendances.

1.  Mettre en évidence la liste des typologies de projets.
2.  Faire la liste des projets
3.  Rappeler les points de vigilances
4.  Ordonner les projets par importance, valeur et dépendance.
5.  Scinder les colonnes en quatre niveaux : chacun représentant un
    semestre.
6.  Compléter les fiches avec des informations d'estimations et marquer
    les composants métiers ou techniques.
7.  Dégager une ligne au dessus des semestres pour piocher dans les
    éléments du premier semestre qui font sens stratégiquement.

{{< image src="/images/2017/02/user-story-mapping-roadmap-4.jpg" title="User Story Mapping Roadmap  4" >}}

J'aurais pu là aussi envisager un sous-titre à chaque semestre qui
définisse au travers d'une petite histoire d'une phrase le contenu et le
sens de chaque ligne.

Exercice intéressant dont je me resservirai car j'y ai (re)trouvé,
quitte à me répéter, une **mise en perspective visuelle efficace**, un
**champ d'action autant auditif, visuel, que kinesthésique**, un
**séquencement soigneusement pensé pour établir du relief dans la pensée
ainsi que dans le déroulé tactique**. La contrainte physique ("on ne
peut pas tout mettre dans la première ligne") joue un rôle clef comme
souvent.

## Un plan d'amélioration ?

Autre usage. Autre contexte. Je découvre des équipes qui doivent me
parler de leur processus, de leurs métiers, de leurs pratiques. Nous
avons peu de temps (1H30, [Frank](https://frank.taillandier.me/) est
avec moi) et nous aimerions faire émerger assez rapidement une
cartographie de leurs pratiques et attentes. Je me dis que je pourrais
utiliser la cartographie de plan d'action, la *User Story Map*. Je sais
qu'elle permet d'avancer rapidement, à plusieurs, qu'elle donne du sens
et du relief.

Je commence par demander aux équipes en guise de colonne vertébrale les
grands domaines de leurs activités, et idéalement sous la forme d'un
cheminement logique : d'abord ça, puis ça, enfin ça. Cela détermine ma
colonne vertébrale.

{{< image src="/images/2017/02/user-story-map-fluency.jpg" title="User Story Map Fluency" >}}

Je peux leur demander ensuite de noter toutes leurs activités par
colonne. Ici pas encore question d'ordonnancer par importance. Un
travail de groupe permet d'avancer rapidement et avec intelligence.

Puis je propose un découpage en ligne : pratiques/activités
acquises/maitrisées, pratiques en cours d'acquisition, pratiques à
acquérir, pratiques que l'on juge inutile. En cela je m'inspire
directement de la proposition de l'atelier autour de [Agile
Fluency](https://martinfowler.com/articles/agileFluency.html) que nous
utilisons [Claude](http://www.aubryconseil.com/) et moi au [raid
agile](http://raidagile.fr) notamment. C'est aussi le moment d'ajouter
la ligne "points de vigilances" au dessus de la colonne vertébrale. Nous
avons eu une colonne "globale" pour les pratiques globales ou très
transverses. Nous avons utilisé les regards croisés des personnes entre
les différentes colonnes pour enrichir.

Comme la lecture verticale ne propose de suite logique, je demande de
mettre en évidence les pratiques jugées les plus importantes en
proposant de mettre des points de couleurs ou un signal visuel sur les
pratiques jugées importantes et en essayant de limiter cette distinction
à maximum 30%.

Dernière étape, l'objectif de ce groupe est d'aller vers/renforcer sa
pratique du *devops*, j'ajoute une ligne "à acquérir pour devops" au
dessus des autres pour mettre en évidence les pratiques et activités.
C'est finalement là encore une ligne qui met en évidence la stratégie,
plutôt que habituellement dans une *User Story Map*, une mise en
évidence du MVP (produit viable minimum).

(merci [Frank](https://frank.taillandier.me/) pour la photo très bonne
pour mon égo que je m'échine pourtant à résorber, j'étais ravi de cette
journée avec toi).

Donc :

1.  Décrire les grandes typologies d'activités
2.  Décliner les activités par typologie
3.  Déclinaison en : pratiques acquises, pratiques en cours
    d'acquisition, pratiques à acquérir, pratiques non désirées.
4.  Afficher les points de vigilance.
5.  Mettre en évidence les pratiques jugées les plus importantes.

## Comique de répétition

Puis-je me répéter une dernière fois ? **Mise en perspective visuelle
efficace**, **champ d'action autant auditif, visuel, que
kinesthésique**, **séquencement soigneusement pensé pour établir du
relief dans la pensée ainsi que dans le déroulé tactique**. La
cartographie de plan d'action, la *User Story Map* n'est pas réservé
exclusivement au produit émergent. Vous pouvez utiliser certaines de ses
qualités dans d'autres domaines.


## Les articles de la série

1.  [Cartographie de plan d’action : le User Story
    Mapping](/2017/01/cartographie-plan-action/)
2.  [Cartographie de plan d’action(user story map)
    revisitée](/2017/02/cartographie-plan-action-revisitee/)
3.  [Cartographie de stratégie, l’impact
    mapping](/2017/02/cartographie-strategie-impact-mapping/)
4.  [Cartographie de stratégie, l’impact mapping, hors des sentiers
    battus](/2017/02/cartographie-strategie-impact-mapping-hors-sentiers-battus/)

