---
date: 2011-06-06T00:00:00Z
slug: ale-2011-a-berlin-7-9-septembre-une-conference-spontanee-autogeree
title: ALE 2011 à Berlin 7-9 septembre une conférence spontanée autogérée
---

Je n'ai pas trouvé de traduction évidente pour le terme
"[unconference](http://en.wikipedia.org/wiki/Unconference)" qui décrit
des conférences organisées et menées par les participants eux-mêmes. La
première conférence du [ALE Network](http://alenetwork.eu/) (Agile Lean
Europe) ne pouvait pas mieux porter son titre. En effet depuis XP2011 à
Madrid (et bon sang, il me faut écrire mon *feedback* du 12 mai car il
en traite beaucoup) et le lancement de la première conférence [ALE 2011
à Berlin](http://ale2011.eu/) sous l'impulsion -énorme- de [Olaf
Lewit](http://hhgttg.de/blog/)z on peut voir littéralement émerger une
conférence dont la cible est 200 participants, avec 3 mois de
préparation et 40 organisateurs. Un défi. Impensable sur la papier, mais
les faits montrent que cela semble marcher. Tout est géré au travers du
"cloud" (avec Basecamp, Mindmeister, Google apps & Skype). La débauche
d'énergie que l'on constate est ahurissante. On a l'impression qu'il y a
des appels skype au travers de toute l'Europe tous les soirs. C'est
proprement hallucinant. Je disais à certains que cela me donnait
l'impression d'être dans un peloton de cyclistes dans un sérieux col, et
soudain d'être complètement décroché par une accélération du groupe (car
j'essaye humblement de participer, mais je vous assure que par faute de
temps je ne suis pas à la hauteur).

{{< image src="/images/2011/06/ALE-faces-300x187.jpg" title="ALE" >}}


La nature et l'engouement autour de cet évènement me pousse à vous
encourager à vous y inscrire et à y participer (même si naturellement il
faudra comprendre l'anglais, notre *esperanto* bon gré mal gré).

Pour cela vous pouvez vous pré-inscrire ici :
http://ale2011.eu/pre-registration/\_et avoir une réduction de 50 euros
(et pour info je me suis pris un vol Paris/Berlin A/R pour 115 euros
...).

Vous pouvez aussi proposer votre propre présentation (en anglais aussi)
: http://ale2011.eu/call-for-papers/

Enfin les sponsors sont naturellement les bienvenus
: http://ale2011.eu/sponsors/ (déjà Nokia, Agile42, JBBrains,
Pair Coaching et Agile Alliance se sont portés candidats).

Je vais essayer de traduire en français les textes les plus importants
(et les documents pour la presse) n'hésitez pas à me communiquer les
informations qui pourraient vous intéresser à ce sujet.
