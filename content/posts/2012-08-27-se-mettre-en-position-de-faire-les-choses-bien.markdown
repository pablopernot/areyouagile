---
date: 2012-08-27T00:00:00Z
slug: se-mettre-en-position-de-faire-les-choses-bien
title: Se mettre en position de faire les choses bien
---

Grosso modo toutes mes missions démarrent ainsi. Le premier objectif que
je me donne (même inconsciemment car je ne le formule que maintenant, en
écrivant) c'est d'assainir la situation, de la simplifier. Etant naïf et
généralement payé pour faire réussir les projets des organisations, je
me consacre à cela, et pas à d'autres choses.

## Essayer ce n'est pas sorcier

Naturellement ce que je m'applique à moi même, je demande à mon client
de se l'appliquer (ou l'inverse). Cela me parait bénéfique, on ne vas
pas chercher midi à quatorze heure. On essaye simplement de bien faire
les choses, ce n'est pas sorcier. Mais c'est déjà un postulat disruptif.
On peut rapprocher cela du **Shu** de
[ShuHaRi](http://fr.wikipedia.org/wiki/Shuhari)

## Faire bien les choses cela veut dire quoi ?

Premièrement dans la tradition *lean* : se mettre en position de
respecter les personnes (pas de philanthropie, soyons cynique, c'est bon
pour le business), de rechercher une amélioration continue, et se
focaliser sur la question "pourquoi fait-on cela ?". "Se mettre en
position" me parait être la bonne définition : c'est à dire tourner son
esprit vers ces préceptes, s'assurer que l'on est en accord avec
ceux-ci, et que l'on souhaite bien les appliquer. Puis quand les
occasions se présentent, les appliquer.

Deuxièmement, accompagnant dans la grande majorité des cas une démarche
**Scrum**,  il faut essayer *a minima* de bien appliquer ses
fondamentaux, son "conteneur" : rôles, responsabilités, évènements
(planning, daily, review, retrospective). Avec **Kanban** c'est encore
plus simple : établissons l'état des lieux, et *mettons nous en
position* de nous améliorer, de nous interroger, etc.

Et souvent pas plus pour démarrer (et déjà...).

## Un conteneur

Le conteneur que j'évoque (rôles, responsabilités, évènements) est un
instrument surprenant. C'est un terreau fertile. Dedans vous êtes
libres, profitez en. Mais ce conteneur et ces preceptes (respect,
amélioration continue, pourquoi je fais les choses -donc la recherche de
valeur-) il faut les respecter. Faire les choses bien, c'est essayer de
les respecter au mieux. Se mettre en position de faire les choses bien
c'est se donner les moyens de les respecter au mieux.

## On ne se raconte pas d'histoire

Et vous savez très bien si vous le faites ou non. Vous pouvez raconter
des histoires aux autres mais pas à vous même. Se mettre en position de
faire les choses bien c'est d'abord arrêter de se mentir.

## Le mieux est l'ennemi du bien

Cependant, en accord avec l'adage qui veut que le mieux est l'ennemi du
bien, se mettre en position de faire les choses bien ce n'est pas aller
au clash. Comme je le dis quand vous vous mettez en position : il s'agit
d'un mouvement, mais ce mouvement doit être tenable (et donc durable).
La frontière avec le "trop" est d'ailleurs difficilement descriptible.
Certains détails paraissant anodins se révèlent parfois être trop, une
attitude radicalement disruptive ne l'est parfois pas.

{{< image src="/images/2012/08/starting_block_org-300x300.jpg" title="Starting ?" >}}

## Pas de regret

Si vous faîtes les choses bien, une autre chose un peu magique c'est que
vous n'aurez pas de regret. Vous aurez donné sa chance au projet, au
produit, à la démarche. Il peut échouer mais vous aurez fait ce qu'il
fallait, et c'est là l'essentiel (à mes yeux en tous cas).

## Accepter les difficultés

Se mettre en position de faire les choses bien est particulièrement
important pour le *product owner* ou le *product manager* qui sont au
coeur de la création de valeur : ils la définissent et la priorisent. Se
mettre en position de faire les choses bien pour ces personnes c'est
souvent avant tout regarder la vérité en face et sortir de sa zone de
confort : "nous n'aurons pas le temps de tout faire", ou "il faut
admettre que nous sommes dépassé par le marché et nous lancer dans cette
nouvelle branche", "il faut faire des choix", etc.

J'entends généralement dans la bouche de ces personnes : "il faut se
couper un bras" (en faisant allusion à la célèbre scène du chevalier
noir dans [Sacré Graal](http://fr.wikipedia.org/wiki/Monty_Python_:_Sacr%C3%A9_Graal_!)),
ou [Alexis](http://praxeo-fr.blogspot.fr/) (avec lequel nous continuons
à discuter stratégie) qui dirait : "un bon stratège doit savoir sacrifier sa section faible".

Faire les choses bien c'est donc d'abord commencer par accepter les
échecs passés et ceux à venir. Et ensuite se mettre en position de ne
pas avoir de regret pour les échecs et succès futurs. Et là rien
n'indique que vous n'allez pas grandement réussir.
