---
date: 2022-04-07
slug: l-enigme-rh
title: "L'énigme RH"
draft: false
---

J'ai le plaisir d'avoir participé à un petit ouvrage sur les RH. 

Il y a 8 ans je lançais ["transformation RH"](/2014/09/transformation-rh/) : des meetups, un petit contenu, qui matérialisait l'importance des RH (pas des personnes nécessairement mais du sujet) dans les organisations et dans cette dynamique agile. C'était modeste mais cela avait le mérite d'exister. Après pas mal de cheminement, et un parcours ... disons agile ... je reviens grâce à [Pauline Garric](https://www.le-lab-de-pauline.com/) et [Elsa Thomas-Pinzuti](https://www.linkedin.com/in/ACoAAA-iSVMBoK26FTZANGfztaHHXxOLv0rM73s) à formaliser quelque chose sur le sujet. J'espère y jouer ce vieux rôle d'agent provocateur.   


# L’énigme RH

Comment passer de bonnes soirées avec mes ami(e)s même en étant RH ou *96 questions pour changer notre vie de RH*

Le pdf : [L'énigme RH](/pdf/enigme-rh.pdf) 

{{< image src="/images/2022/04/enigme-rh.jpg" title="Enigme RH" >}}


Merci à [Aurélie Rolland](https://www.linkedin.com/in/aur%C3%A9lie-rolland-elia/) pour la mise en page. 
Bonne lecture !