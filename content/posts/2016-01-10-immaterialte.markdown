---
date: 2016-01-10T00:00:00Z
slug: immaterialite
title: Immatérialité
---

Le nouveau défi c'est l'immatérialité. L'immatérialité de la valeur,
l'immatérialité des espaces, et presque – pourrait-on dire –
l'immatérialité des matériaux.

On a bien compris déjà avec l'arrivée peut-être du télégraphe au XIXème
siècle (lire le passionnant "The Information" de James Gleick), qui a
fait si peur aux journalistes en transformant le monde, en éliminant les
distances, que la valeur n'était plus physique, mais savoir,
information. La vitesse de transmission, les moyens, ont radicalement
changés depuis les dernières années, et ce changement ne fait que
s'accentuer, frénétiquement.

## Espace et temps

Les espaces ne sont plus les mêmes: on travaille de n'importe où, avec
n'importe qui. Les nouveaux outils (En ce moment : Slack, Google apps,
Trello, Gitlab, Skype, Whatsapp, et plein d'autres) sont venus rendre
possible une dynamique de groupe qui s'affranchit des limitations
physiques que nous avons pourtant évoqué précédemment. L'ubiquité
réservée dans nos légendes à nos Dieux n'est pas loin: on est partout à
la fois, on sait tout immédiatement.

### Rendre opérationnel ?

Comment revenir à quelque chose d'opérationnel quand on a longtemps dit
(et que l'on continue de à le penser énormément) que la colocalisation
et le traitement attentif de tout l'espace physique autour d'une
proximité, d'une autonomie visible étaient essentiels, et que, par la
suite, l'immatérialité du monde qui s'ouvre à nous nous dépossède de
cela?

Le télétravail est certainement un élément important de notre époque,
inéluctable, cela paraît un progrès de civilisation, dommage voire
impossible de s'en passer. Mais comment faire avec ces équipes distantes
ou le télétravail? J'ai observé plusieurs phénomènes. Il est d'abord
important de préserver un même niveau de connaissance, de savoir,
d'information. Les notions d'objectif clair et de règles claires
s'étendent à l'idée d'une connaissance partagée, alignée. Ce que l'on
sait de ces équipes non-localisées c'est qu'il y a une différence
fondamentale entre les équipes dispersées : leur répartition (nombre,
géographie) est déséquilibrée, et les équipes distribuées où leur
répartition est équilibrée. Par exemple pour dispersée vous avez quatre
personnes sur site, et deux sur des sites distants. C'est compliqué à
faire fonctionner car il y a un déséquilibre dans la communication. Mais
si vous avez six personnes, chacune sur six sites distants, il n'y a pas
de déséquilibre. C'est bien la communication qui est clef. En ayant le
même niveau communication, on induit le même niveau de transparence et
donc de confiance, et donc d'implication et de liberté sur lequel bâtir
quelque chose est possible.

Il faut aussi préserver cette notion importante d'autonomie. La
problématique du offshore n'est pas plus la distance que le silotage et
l'aveuglement à l'extrême.

La problématique du télétravail, ou plus globalement d'une activité à
distance, sans regroupement physique c'est l'impossibilité de travailler
sur plusieurs canaux à la fois (visuel, auditif, kinesthésique). Par
kinesthésique entendez le toucher, les sensations du corps. J'ai pu
mesurer l'importance de répéter constamment le même message en utilisant
différentes formulations, et en utilisant différentes formules: écrits,
dessins, post-it, parole, etc. J'ai été frappé de voir comment les
dynamiques de groupe évoluent (dans le bon sens) quand on essayait de
mélanger tous les canaux (visuel, auditif, kinesthésique). Cela manque
si nous sommes perpétuellement à distance, où chacun doit penser à
s'équiper et à agir pour rendre physique ou écrit ou oral des
informations qui arrivent en permanence par un autre canal. Cela demande
une bonne connaissance de soit, et une discipline.

Il est aussi nécessaire de créer un sentiment d'appartenance et
d'objectif collectif. Les équipes des logiciels *opensource* font
régulièrement des *summit*; des rencontres informelles dont le premier
objectif est d'abord de se croiser, de se projeter, de faire vibrer ses
neurones miroir, de constituer cette boucle d'appartenance
amoureuse,tribale, familiale, ancestrale du cerveau
[lombic](https://en.wikipedia.org/wiki/Limbic_resonance).

## Matière

On vient souvent ramener l'Agile au Lean. Souvent car le Lean industriel
rassure. J'espère avoir expliqué [ici](/2014/11/les-cowboys-hippies/) ou
[là](http://www.infoq.com/fr/presentations/lkfr-pablo-pernot-topologie-lean)
([Lean Topologie chez
InfoQ](http://www.infoq.com/fr/presentations/lkfr-pablo-pernot-topologie-lean))
pourquoi cette notion d'industrialisation répétable et linéaire est
obsolète dans la plupart des métiers que je croise. On m'appelle pour me
demander si dans une chaîne de montage on peut être Agile? Ce qui a
marché là, et bien on voudrait le reproduire ici. Mais l'émergence liée
aux systèmes complexes n'a rien à voir avec la répétabilité et la
linéarité des systèmes industriels du siècle dernier. C'est pourtant
l'émergence qui est aujourd'hui nécessaire pour répondre habilement au
monde moderne si concurrentiel et changeant: émergence, modularité,
intelligence collective. Dans le systémique aussi, on ne peut pas
repenser une partie du système, faire de sous-optimisation, injecter de
l'agile quelque part. Il faut souvent repenser l'intégralité du système.
Comme toujours, on ne va pas plus vite, on va différemment.

Pendant de nombreuses années cela paraissait impossible, l'approche
industriel du siècle dernier semblait durablement installée. Difficile
d'expliquer à un fabricant automobile de repenser son système, même
aujourd'hui, et pourtant. Comme toujours la limitation n'était que celle
de notre pensée, *ils ne savaient pas que c'était impossible alors ils
l'ont fait*, disait Mark Twain. Est apparu
[Wikispeed](http://wikispeed.org/). Wikispeed a repensé intégralement la
fabrication de voiture avec un esprit neuf, inspiré du mouvement Agile.
Sans mémoire du muscle ce qui devait se faire, avec la naïveté et la
puissance des enfants. Ils ont démontré et réussi a inventé une nouvelle
voiture, qui se fabrique différemment, qui se pense différemment, et qui
s'utilise normalement. Leurs résultats aux tests de sécurité sont
bluffants. Il fallait oser. Ils l'ont fait. Si donc la fabrication de
voiture n'est pas à l’abri de cette révolution qui le serait?

La matière se pli à notre façon de penser le monde. Pour gagner en
souplesse la matière s'articule en petits morceaux, pas d'émergence avec
des monolithes. Tous les services informatiques commencent à penser
[microservices](http://martinfowler.com/articles/microservices.html)
(nous sommes en 2015/2016 je ne sais pas ce que nous réserve la suite).
Des microservices qui s'articulent et s'agrègent selon les besoins.
Wikispeed a pensé ses voitures en composants qui se montent et se
pensent comme des pièces d'un jeu de construction.

La matière se pli à notre façon de penser le monde et à la capacité que
l'on se donne à la rendre malléable. Mais je crois que cette dernière
barrière avec l'[impression
3D](https://fr.wikipedia.org/wiki/Impression_tridimensionnelle) vient
d'exploser. Avec la démocratisation et le déferlement à venir de
l'impression 3D, la matière a lâche prise sur son diktat du possible, du
faisable. C'est la pensée, le savoir, la connaissance, l'information qui
priment désormais, qui assujettissent la matière. **Émergence,
modularité, intelligence collective**.

La matière garde son importance: comme matière première. Comme source
d'énergie à manipuler. Encore une fois: la matière se pli à notre façon
de penser le monde. Dans le même ordre d'idée, c'est quand on a compris
et su que le monde était rond et non plat avec une fin que les gens se
sont lancés à sa
[découverte](https://www.ted.com/talks/simon_sinek_how_great_leaders_inspire_action).
La barrière était mentale.

### Rendre opérationnel ?

Difficile de se dire qu'il faut penser différemment. C'est une
injonction difficile à respecter. J'avais évoqué un atelier que j'avais
appelé "[espace des mèmes](/2015/07/lespace-des-memes/) " lui même très
inspiré de jeux et d'ateliers connus et reconnus, et qui que je pratique
quand mes clients en comprennent la nécessité et le fondement. Comment
penser différemment. Je vais parler plus d'innovation (croiser des
notions et des idées pour faire émerger de nouvelles visions, de
nouveaux axes de développement) que de création pure (quelque chose de
totalement nouveau).

Habituellement j'accapare une pièce, et je demande à mes contacts de
tapisser un mur de avec des post-it sur lesquels ils indiquent ce qu'ils
sont, leurs valeurs, leurs principes, leurs qualités, leurs forces, ce
qui les définit. Sur un autre mur je demande à ce que l'on mette de la
même façon les qualités que l'on trouve à ses concurrents ou les
qualités des grandes idées et des grands produits qui nous entourent.
Enfin sur un troisième mur, pourquoi ne pas mettre des adjectifs (vert,
long, transparent) des objets qui nous entourent au quotidien (cette
dernière notion est importante, le quotidien est probablement là où
notre impact sera le plus considérable). Après, soit au cours de
sessions dédiées, soit au fil de l'eau dans la semaine (c'est bien de
laisser mijoter), on se lance dans l'exercice de piocher un élément de
chaque mur et de trouver une idée folle qui pourrait en découler.
Gardons donc un quatrième mur pour stocker ces idées folles. Comme dans
tous les *brainstorming* l'exagération est de mise. Je propose de la
soutenir fortement pour ne pas marquer de limite à la pensée. On est
souvent surpris.

J'ai toujours cette histoire que m'a contée Valérie Wattelle, et qui me
sert d'exemple sur l'exagération nécessaire à un bon *brainstorming* (et
que j'avais déjà [raconté
ici](/2014/09/histoires-que-je-raconte-en-ce-moment/)):

Un gros consortium canadien est responsable des lignes électriques qui
parcourent les forêts du pays. Problème : le poids de la neige menace de
faire s’effondrer ces dîtes lignes. Et c'est long et compliqué à
traiter. D'autant plus que c'est dangereux, les forêts canadiennes
hébergent une palanquée d'ours. Comment résoudre ce problème ? Ils
brainstorment, sans hésiter à délirer, on ne sait pas ce qui en sortira,
mais bon. Voyons. Soudain une idée incongrue surgit :

-   Et si on s’appuyait justement sur les ours ? Les ours ?
-   Oui ils pourraient venir gratter les poteaux?
-   Ça suffira de gratter pour faire tomber la neige?
-   Ou les secouer? Avec du miel?
-   Oui en les faisant secouer les poteaux on peut imaginer que la neige
    tombe. (vraiment cette réunion c’est n’importe quoi).
-   Ok mais il faudrait que le miel soit en haut des poteaux pour les
    pousser à secouer ou au moins à monter sur les poteaux, non?
-   En plaçant des pots de miel en haut des poteaux, sur les lignes ?
    Attirés ils vont venir secouer les poteaux.
-   Ok, et comment on place les pots de miel sur les poteaux ? On ne
    peut pas placer de milliers de pots de miel? En plus en haut...
-   Hummm, et si nous faisions couler du miel sur les lignes en passant
    avec un hélicoptère? Facile pour un hélicoptère de passer sur les
    lignes pour déposer le miel sans risque!

...

Bingo : l’organisation a réglé son problème... en faisant passer un
hélicoptère au dessus des lignes, les pales de celui-ci font tomber la
neige.

Toute cette innovation est une question de sérendipité. Rendez la
visible. Beaucoup de jeux de cartes permettant échanges et de mélanges,
de mises en situation incongrues pour penser différemment. Injectez
aussi de nouveaux profils. D'où souvent le grand intérêt du sang neuf
dans les équipes. Ils osent des choses que l'on a oublié d'oser. Ils
pensent des choses auxquels on ne sait plus penser. (Mais que du sang
neuf fait perdre la connaissance).

## Valeur

La valeur est aujourd'hui de moins en moins sur la propriété (espace,
temps ou matière), mais de plus en plus sur le savoir, la connaissance,
l'information. Les grands réussites économiques récentes se fondent sur
cette nouvelle matière qui flotte dans l'air, la recherche (Google), la
mise en relation (Uber, AirBnB, Blablacar, etc). Comme une des grandes
bascules dans la façon de penser Agile est de ne plus s'intéresser au
travail, au temps passé, mais à la valeur créée.

Cela complète ce défi de l'immatérialité: la matière a perdue sa
suprématie. L'information et la connaissance ont pris le devant de la
scène. Apprenons à jouer avec. Apprenons à ne plus juger sur le travail
mais sur la valeur, comprenons que cette modularité qui s'exprime dans
la matière, dans les nouveaux outils, s'exprime aussi dans les
articulations des organisations: équipes distribuées, télétravail,
appartenance dématéralisée, communauté digitale.

### Ubiquité, malléabilité, simultanéité

**Immatérialité dans le sens ou la matière n'est plus une limite, ni
dans l'espace (ubiquité), ni comme matériau (malléabilité), ni presque
au niveau temporel (simultanéité)**.

En complément de cet article : vidéo et slides chez InfoQ : [Topologie
Lean](http://www.infoq.com/fr/presentations/lkfr-pablo-pernot-topologie-lean).

Illustration : [Esther Stocker](http://www.estherstocker.net/)
exhibition, London, 2008

