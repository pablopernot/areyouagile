---
date: 2012-11-22T00:00:00Z
slug: agile-tour-paris-2012
title: Agile Tour Paris 2012
---

Cette semaine j'ai pu participer à l'agile tour paris 2012. J'ai pu y
donner 2 sessions. La première avec **Alexis Beuve**, fondateur des
[Editions Praxeo](http://praxeo-fr.blogspot.fr/), et qui a concrêtisé
notre rencontre voilà deux ans. Cette session a naturellement porté sur
la **Stratégie du Product Owner**. Celle-ci fait suite a deux de mes
humbles articles ([Introduction à la stratégie du Product
Owner](http://www.areyouagile.com/2012/06/introduction-a-la-strategie-et-la-tactique-du-product-owner/),
[Le Product Owner
contre-attaque](http://www.areyouagile.com/2012/09/le-product-owner-contre-attaque/)),
mais surtout à la [quinzaine !! d'articles
denses](http://praxeo-fr.blogspot.fr/2012/10/strategies-episode-i-cliches-et.html)
de Alexis sur son
[blog](http://praxeo-fr.blogspot.fr/2012/10/strategies-episode-i-cliches-et.html).

La deuxième session était la quatrième représentations du [projet dont
vous êtes le héros agile](/heros/).

## Stratégie du Product Owner

<script async class="speakerdeck-embed" data-id="8d0edc20157c0130f5111231381d612b" data-ratio="1.44837340876945" src="//speakerdeck.com/assets/embed.js"></script>

## Projet dont vous êtes le héros agile

Dans un tout autre genre j'ai pu avec
Antoine (et une pensée pour
Bruno) animer un [projet dont
vous êtes le héros agile](http://www.jabberwocky.fr). Toujours un grand
moment de plaisir et d'amusement. Nous avons un peu été frustrés de
devoir finir plus tôt (1h45 au lieu de 2h), et du coup la rétrospective
a été trop rapide. Elle sera mise  en ligne cependant dès que possible
ainsi que [quelques photos](/heros) . N'hésitez pas à jouer vous même vous avez tout sous la main.

## Feedback rapide

Ce que j'ai aimé :

-   Concrêtiser ma rencontre (2 ans) avec Alexis
-   Revoir les copains

Ce que je n'ai pas aimé :

-   Pas de café le matin !
-   Des liaisons dangereuses entre les sponsors et pas mal de sessions
-   Session "héros agile" un peu raccourcie (en dessous de 2 heures on
    souffre).

Ce que j'ai appris:

-   "grand champion" est un pléonasme, "champion" suffit.

