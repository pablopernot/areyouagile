---
date: 2018-03-24
slug: raid-agile-c-est-fini
title: Raid agile c'est fini
---

Voilà, c'est la fin d'un cycle. Le [raid agile](http://raidagile.fr) de juin 2018 sera le dernier pour moi. Le raid agile c'est fini (pour moi). C'était une très belle aventure. Onze raids si je compte celui de juin à venir. Plus d'une centaine de raiders, quatre années. Plein d'apprentissage. 

Pourquoi j'arrête ? La *charge rustique* m'a vaincue. Organiser un raid c'est un vrai gros boulot, une vraie tension, être attentif au moindre détails, on a du lait, du thé, du café ? Combien de vans faut-il pour aller chercher les gens à la gare de Nîmes ? Qui veut bien manger de quoi, quand ? Qui se sent bien dans sa chambre (double ou pas) ? Qui ronfle et qui réveille qui ? Qui s'est fait mal à la cheville pendant la randonnée ? Qui a-t-on perdu sur la route ? Ce groupe vient d'annuler un mois avant le raid, que fait-on ? Etc etc etc. Aujourd'hui j'ai naturellement envie de continuer à réaliser ces sensibilisations, ces formations, ces accompagnements, différents, modernes, hors site, mais en ayant un focus uniquement sur les personnes et pas sur l'organisation, j'essaye de trouver des cadres plus simples, de beaux hôtels, des lieux qui prennent en charge tout sauf le fond. 

J'arrête aussi parce que c'est bien de savoir arrêter. Arrêter quand tout va bien. Quand après quatre années chaque raid m'apparaît comme une réussite, une vraie petite aventure qui --- je l'espère --- a su ouvrir les chakras[^1] de beaucoup de nos raiders. Savoir arrêter c'est sensé, ne pas en faire trop, maximiser la valeur, passer à autre chose.  

[^1]: Note à certains RH : non et encore non, nous ne sommes pas une secte. *A priori* une formation sans moquette grise, sans *slide*, et sans plafond bas est assez déstabilisant pour certains d'entre vous. Faut prendre l'air un peu : http://raidagile.fr/lettre-aux-rh.html.

Alors ce dernier raid il sera collector. 

{{< image src="/images/2018/03/raidagile.jpg" title="Raid agile" >}}