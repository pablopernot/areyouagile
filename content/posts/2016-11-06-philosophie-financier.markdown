---
date: 2016-11-06T00:00:00Z
slug: la-nouvelle-philosophie-du-directeur-financier
title: La nouvelle philosophie du directeur financier
---

Avec [Dragos](http://andwhatif.fr) et sous l’impulsion de l’un de nos
prospects nous avons décidé d’organiser une journée dédiée aux
[CFO](https://en.wikipedia.org/wiki/Chief_financial_officer) (aux
[DAF](http://www.cadre-dirigeant-magazine.com/recruter/definition-fonction/daf-definition-fonction/),
Directeurs Financier) et à l’agilité : [Agile CFO](http://agilecfo.fr).
Quand [Bjarte
Bogsnes](https://www.agilealliance.org/wp-content/uploads/2016/09/2263_BeyondBudgeting.pdf)
affiche une liste d’une trentaine de logos d’entreprises qui se sont
lancées dans cette nouvelle façon de penser le rôle de financier je ne
vois aucune entreprise française, pourquoi pas la vôtre ?

## Pourquoi une journée destinée aux DAF/CFO ?

Le constat demeure le même pour tous les métiers : l’accélération du
temps, la mondialisation et son entrelacement d’informations, la
complexité liée à tous ces critères vient bouleverser la façon
d’appréhender le monde. Les financiers n’échappent pas à la question. Et
soit ils sont ou seront de plus en plus remis en cause par les équipes
qui les entourent, soit ils entament ou entameront par eux-mêmes les
réflexions qui s’imposent.

Une journée pour toucher du doigt cette nouvelle façon de penser notre
monde moderne qui de l’agilité des services IT s’est naturellement
propagée aux métiers et à leur approche produit, pour toucher les RH et
le *Business Development*, et enfin se diffuser aux services financiers.
*Scrum, Kanban, XP* pour les services informatiques ; *Lean Startup,
Design Thinking*, pour les métiers ; *Holacratie, Management 3.0,
Entreprise libérée* pour le management et l’entrepreneuriat ; Ce sont
les mouvements **\#noestimate** et plus encore **“beyond budgeting”**
qui vont trouver échos aux oreilles des financiers.

## Quelles sont les réflexions qui s'imposent ?

Les entreprises pensent qu’elles manquent d’argent et de temps. Elles se
trompent, là réponse ne se trouvent pas dans le contrôle de l’argent et
du temps, mais dans la réalisation des bons produits et l’implication
des personnes.

> “Quand on suit les coûts on génère des coûts, quand on suit la valeur,
> on génère de la valeur” expliquait Peter Drucker le grand guru du
> management.

### Ce n'est pas avoir plus de choses, c'est avoir les bonnes choses

On sait depuis bientôt quinze années que les produits, les
fonctionnalités, proposés aux gens ne sont jamais ou rarement utilisés
dans les deux tiers des cas car ils se révèlent inutiles ou mal conçus.
Il suffit d'observer autour de vous pour s'en convaincre. Ce n'est pas
donc pas de temps et d'argent dont nous avons besoin, mais du bon
produit.

Cela induit toute une réflexion sur la conception des produits, les
besoins ou les problèmes auxquels on répond, une projection sur les
usages et les contextes d'utilisation. Il ne sert à rien d’avoir d'abord
de l’argent pour construire des choses. Il faut avant tout construire
les bonnes choses.

### Ce n'est pas travailler, c'est être impliqué

On dit aussi, même si il n'y a pas de chiffre ou d'étude probante à la
clef, qu'une personne impliquée est très, mais vraiment très largement
plus performante, productive, innovante, active, qu'une qu'il ne l'est
pas. Certains parlent d’un ratio de 1 à 10 d’autres d’un ratio bien plus
important. Il ne sert à rien d’investir dans du temps il faut investir
dans de l’engagement en quelque sorte, il faut financer l’appropriation.

### Ce n'est pas avoir plus de temps, c'est être dans les temps

Cette impression de besoin de temps n’est en fait pas liée à une somme
de temps, mais a une bonne concordance, coordination, avec le marché (je
ne sais pas si cela me plait mais c’est le marché qui domine aujourd’hui
ces questions). Il faut donc une capacité à opérer avec souplesse (par
petits ensembles) et avec beaucoup de pertinence (priorisation,
validation des hypothèses).

**Récemment ce ne sont donc nullement les entreprises qui ont le temps
et l'argent qui florissent. Mais celles qui ont su investir pour
réaliser les bonnes choses, en sachant engager leurs collaborateurs.**

Toutes ces questions devraient être repensées aussi par les financiers
qui peuvent avoir un pouvoir de nuisance comme de support
particulièrement puissant. Il ne s’agit pas de mettre l’entreprise en
danger, il ne s’agit pas d’oblitérer les aspects financiers. Il s’agit
de se mettre dans une posture en concordance avec ce que demande
l’époque tout en garantissant les redevabilités et les responsabilités
que l’on est en droit d’attendre d’un Directeur Financier.

## Les sujets que nous allons aborder pendant cette journée

### À situation difficile, positionnement difficile

La situation est difficile. Il s’agit d’un changement de mode de pensée
considérable car on se retrouve en porte à faux avec une approche
cartésienne particulièrement ancrée chez nous. C’est une situation
difficile car les services financiers sont souvent le rempart aux coups
d’éclats (jugés) intempestifs. Or il va maintenant leur être demandé (et
là dessus je suis assez clair, avec le temps, ils n’auront pas le choix)
de changer cette approche trop rigide pour nos temps modernes, et eux
même de se renouveler. Certains peuvent décider de résister, de ne pas y
croire, les effondrements seront rapides. Durant les cinquante dernières
années la [durée de vie moyenne d’une
entreprise](http://www.onlydeadfish.co.uk/only_dead_fish/2015/09/is-the-life-expectancy-of-companies-really-shrinking.html)
est passée d’une soixantaine d’années à \~18 ans.

### Valider par les chiffres culture, philosophie et valeurs

La bonne nouvelle c’est qu’ils ne perdront pas leur rôle de barycentre
de l’entreprise. Mieux encore, leur prise de risque contrairement aux
apparences sera moindre qu’aujourd’hui où leurs façons d’opérer n’est
plus en phase avec le monde qui les entoure. Mais ils ne peuvent plus
s’appuyer sur les chiffres comme philosophie. Ils devront s’appuyer sur
une culture, une philosophie, des valeurs, qui elles mêmes seront
constamment validées par des chiffres. Le retournement est d'importance.
À l'image du manager moderne qui ne micro-manage plus mais pose un cadre
et des règles claires dans lesquels ses collaborateurs s'émancipent, le
financier posera un cadre et des règles claires dans lesquels
l'entreprise trouvera de l'espace, des leviers.

### Travailler avec des mesures a posteriori

La nouvelle philosophie du directeur financier c'est bien de s'inscrire
dans cette nouvelle façon de penser l'entreprise. D'investir dans
l'atteinte d'objectifs possédant de la valeur à intervalles réguliers
permettant une prise de risque justifiée par un périmètre réduit et
validée par des mesures significatives. Libération d'un espace (choix,
expérimentation, autonomie) dont vont s'emparer les forces de
l'entreprise pour avancer sans s'affranchir d'une redevabilité mesurée
constamment, du moins très régulièrement. Avec de vraies mesures *a
posteriori* et à intervalles réguliers et non pas des engagements sur
des estimations non perçues comme hypothétiques ce qu'elles sont
pourtant.

### Contenu de l'atelier et détails pratiques

C’est de tout cela dont nous parlerons avec Dragos lors de cette
journée. En nous appuyant sur les mouvements “\#noestimate” et “beyond
budgeting” et en essayant de générer de déclencher des premiers pas
opérationnels pour les personnes présentes, en croisant leurs
expériences, en relatant des expériences connues.

La matinée est consacrée à la présentation des concepts, de la culture,
du mode de pensée de ces nouvelles façons d'aborder le monde complexe,
changeant et incertain qui est le notre. Une matinée pour découvrir
comment cela peut avoir un impact ou être intégré à votre activité et à
votre savoir.

L'après-midi est consacrée à des ateliers qui mettront en pratique ces
concepts et permettront des débats entre pairs, des retours
d'expérience.

Vous trouverez plus d'information sur [Agile CFO](http://agilecfo.fr)
(Rendez vous le 13 décembre (nous avons décalé d’une semaine !).

Quelques liens (merci [Dragos](http://andwhatif.fr) !):

 -   [\#noestimate](http://softwaredevelopmenttoday.com/noestimates/) 
 -   [Beyond budgeting](https://www.agilealliance.org/wp-content/uploads/2016/09/2263_BeyondBudgeting.pdf)
 -   [Escape Velocity](http://slideplayer.com/slide/3383673/)

