---
date: 2021-02-19
slug: agilite-regard-critique-sur-les-dernieres-annees-pour-bien-debuter-les-suivantes 
title: "Agilité, regard critique sur les dernières années pour bien débuter les suivantes"
--- 

Voici la première version qui m'a servit de base pour la conférence ["Frug'agile 2021"](https://www.frugagile.org/) en février 2021. La vidéo de la session devrait être disponible sous peu.

[LE PDF](/pdf/2021-regard-agilite-20-ans.pdf) 

{{< image src="/images/2021/02/sommaire-regards.jpg" title="regard critique sur les dernières années" >}}

## Et voici la vidéo de la session

{{< youtube id="ykqERA7a9yk" >}}