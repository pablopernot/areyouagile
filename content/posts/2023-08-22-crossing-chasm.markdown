---
date: 2023-08-22
slug: comment-faire-passer-une-etape-a-ecoresponsabilite-franchir-le-fosse 
title: "Comment faire franchir une étape à l'écoresponsabilité, #1 traverser le fossé"
draft: false
---

La table ronde ou conversation libre que nous avons initiée avec [Tristan](https://www.standblog.org/blog/) dans le [dernier épisode de frugarilla](https://www.frugarilla.fr/contenu/) [août 2023] avait comme point de départ la notion de *tipping point* (point de bascule) entre une communauté impliquée sur un sujet : notre écoresponsabilité, et l'opinion publique, comment et quand une bascule s'opérait pour propager et gagner les gens à une cause. La liberté de la conversation nous a amenés sur plein de sujets intéressants, sans forcément répondre à ce point de départ. 

Je faisais pour ma part le rapprochement avec un livre célèbre, souvent utilisé dans le monde du *lean startup*, qui s'appelle *crossing the chasm* par Geoffrey Moore. Certes le livre date (1991), mais l'édition que je tiens en main est -- elle -- de 2014 et je pense qu'une actualisation a eu lieu. 

Pourquoi faire ce rapprochement ? Car ce livre, pour un autre sujet (les startups californiennes pour la faire courte), aborde la même problématique : provoquer une bascule dans l'opinion pour propager -- en masse -- son produit, son service, ses convictions. Et même s’il n'était pas du tout destiné à une réflexion sur l'écoresponsabilité, il me semble qu'il nous apporte des pistes intéressantes. 

{{< image src="/images/2023/08/crossing-the-chasm.jpg" width="200" >}}

Le fait est que nous constatons un mur invisible entre la majorité des gens qui même s'ils se soucient de l'écoresponsabilité et de notre avenir ne changent pas leurs façons de vivre et de penser le monde, et une minorité qui elle change sa façon de vivre et de penser le monde. Mur invisible, car le premier groupe semble sourd aux demandes, au désir, aux messages du deuxième groupe. 

C'est un débat important autour de l'écoresponsabilité et de l'avenir de la planète. 

# Que pourrait nous apprendre ce livre ? 

Ce livre se base sur la notion du cycle de vie de l'adoption des technologies (*Technology Adoption Life Cycle*). 
Ce cycle et cette adoption se basent sur un schéma, une courbe, "une cloche". 

Et j'observe que la difficulté auquel nous nous heurtons pour que le sujet de l'environnement, de la planète, soit véritablement pris en main, par la grande majorité, la difficulté à convaincre, expliquer, faire passer les messages, se heurte au même problème que celui d'un produit technologique tel que le développe se livre. Nous pourrions ainsi aussi nous inspirer des pistes qu'il propose pour résoudre ce fossé. 

{{< image src="/images/2023/08/talc.jpg" width="600" >}}

Donc ici **plusieurs familles** de personnes qu'un produit conquiert peu à peu (s'il a du succès). 

Les *innovators* : les **enthousiastes**, ils essayent tout avant tout le monde. Ils aiment le nouveau. 

Les *early adopters* : les **visionnaires**, les **agents du changement**, ils ont des convictions sur le sujet, ils s'en emparent. 

La *early majority* : le premier gros morceau de majorité (de massification), Geoffrey Moore les appelle les **pragmatiques**, c'est important, nous en reparlerons. 

La *late majority* : l'autre morceau de la majorité qui vient après, Geoffrey Moore les appelle les **conservateurs**. 

Les *laggard* : les retardataires, il les appelle les **sceptiques**. Ceux que l'on appelle, sans hasard, concernant notre sujet, les climatosceptiques. 


## Le test de la voiture électrique

Puisque l'on parle de hasard : quand il écrit ce livre, il prend comme exemple la voiture électrique pour positionner les gens dans ces différents groupes en les interrogeant sur ce thème. À l'époque c'est encore une technologie très incertaine. Tellement incertaine que sa question démarre par : "Imaginez que ces voitures marchent comme n'importe quelle voiture *(sic)[note de l'auteur]*, et qu'en plus elles soient bonnes pour l'environnement", -- et il conclut -- "Est-ce que vous seriez prêt à en acheter une ?"[^q1] 

[^q1]: *Stepping back a bit from the cool factor, let’s assume these cars work like any other, except they are quieter and better for the environment. Now the question is: When are you going to buy one?*


- Si la réponse est "Pas avant que l'enfer ne gèle !"[^q2] (cocasse quand on sait ce qui arrive aujourd'hui...), vous êtes probablement un **sceptique**. 
[^q2]: *Not until hell freezes over*.

- Si votre réponse est : "Lorsque j'aurai vu les voitures électriques faire leurs preuves et qu'il y aura suffisamment de stations-service sur les routes"[^q3], vous êtes probablement un **pragmatique**.
[^q3]: *When I have seen electric cars prove themselves and when there are enough service stations on the road*

- Si vous répondez : "Pas avant que la plupart des gens aient changé et qu'il devienne vraiment incommode de conduire une voiture à essence"[^q4], vous êtes probablement un **conservateur**.
[^q4]: *Not until most people have made the switch and it becomes really inconvenient to drive a gasoline car*

- Mais si vous voulez être dans les premières personnes à vouloir une voiture électrique -- à cette époque -- parce que vous adorez la nouveauté ou que vous voulez soutenir l'environnement vous êtes certainement soit un **enthousiaste** soit un **visionnaire**. 

## Et ensuite ? Il est où ce fossé à franchir ? 

Il est là ce fossé. Cet endroit est le plus dur à franchir. Entre le petit groupe des **agents du changement**, des **visionnaires**, et celui des **pragmatiques**. Et c'est exactement ce fossé -- il me semble -- que nous devons franchir : [frugarilla](https://frugarilla.fr) dont je fais partie ou d'autres "activistes", ou "personnes engagées", et dans lequel il semble que tous nos efforts s'embourbent. 

{{< image src="/images/2023/08/rtalc.jpg" width="600" >}}

Cette "cloche", cette "courbe", sous-tend l'idée que cela avance d'une étape à l'autre : les enthousiastes s'emparent d'une idée, les visionnaires la nourrissent de sens, les pragmatiques prennent ensuite en main le sujet, les conservateurs sont ensuite conquis, et enfin même les sceptiques cèdent peut-être. Mais le diagramme indique aussi que le passage de *early adopters* (*agents du changement*) à *early majority* est le plus dur, qu'il est souvent un lieu d'échec pour les nouvelles technologies qui cherchent à se vendre. Mais pourquoi est-ce que nos efforts s'embourbent dans ce fossé ? 

Car les **agents du changement veulent du changement** basé sur des convictions, MAIS les **pragmatiques** veulent "une amélioration de productivité, et **minimiser la discontinuité** avec le monde d'avant"[^trad]. C'est l'exact opposé ! Et on sait aussi qu'ils ne lanceront pas sans un exemple de leur communauté, mais personne ne veut se lancer en premier : nous sommes dans une impasse.
[^trad]: *Productivity improvement , minimise the discontintuity with the old days*.

Donc deux difficultés : 

- Les attentes de la *early majority* sont contraires aux attentes des *early adopters*.  Les deux attentes sont opposées et on ne répond pas avec les arguments du premier groupe au second. Sinon, au contraire, probablement, il se braque. 
- Cette majorité ne sera convaincue que si d'autres personnes de son groupe sont convaincues : mais personne ne veut faire un premier pas. Tout le monde se tient par la barbichette. Ils manquent à ces pragmatiques souvent une base de référence de personnes comme eux chez qui cela fonctionne.

## Quelles pistes de réponses cela nous propose ? 

D'abord comprendre que répondre à *leurs* inquiétudes avec les réponses à *nos* inquiétudes ne fonctionne probablement pas. 

D'autre part il faut trouver des gens qui rassurent les pragmatiques, des gens de leurs clans, de leurs milieux. Ce que vous propose le livre, en utilisant la métaphore du débarquement du 6 juin 44, le D day, c'est de trouver une plage pour débarquer. Sous-entendu trouver une cible, assez petite, sur laquelle on peut mettre pied. 

Par exemple, aujourd'hui je ne vois pas d'endroit dans les entreprises où l'on bouscule l'idée que la croissance est opposée à l'environnement et que la croissance est nécessaire. La proposition du livre est de ne pas utiliser nos arguments pour convaincre, mais de trouver une niche, une plage, un ensemble de métiers, d'entreprises ou une réussite pourrait non plus équivaloir à une idée de croissance, mais à un système vertueux, en accord, intégré à la planète. Trouver le bon endroit, normalement petit, mais qui montre que cela marche. Les **pragmatiques** ont besoin de voir que cela marche, que c'est possible. 


À partir du moment ou une niche, un marché est conquis à cette nouvelle idée, et que cela fonctionne : la niche vit, prospère (mais pas dans le sens croissance), les autres **pragmatiques** seront prêts à sauter le pas. 

Et donc le conseil de Geoffroy Moore c'est de bien s'interroger sur quelle plage atterrir. Quel marché est le bon marché pour changer ces règles du jeu que nous aimerions voir changer. Quel marché est la bonne plage de Normandie pour démontrer une nouvelle façon de voir l'entreprise ou le système ? 

C'est une réflexion que je vous propose de lancer. 


ps : c'est le même souci avec ces fameuses transformations agiles : les attentes du *middle-management*, des *pragmatiques* ou des *conservateurs* généralement, sont opposées aux attentes des *early adopters*, des *agents du changement*. Et on se fracasse à répondre aux deux de la même façon.   


### complément (conversation sur mastodon)

{{< image src="/images/2023/08/dialogue.jpg" width="600" >}}
