---
date: 2010-08-18T00:00:00Z
slug: fiche-de-lecture-scrum-guide-pratique-claude-aubry
title: 'Fiche de lecture : Scrum, guide pratique, Claude Aubry'
---

En ce début d'été j'ai pu le lire le livre de Claude Aubry. Je consulte
régulièrement son [blog](http://www.aubryconseil.com/) (j'y retrouve
deux sources d'intérêts : l'agilité ET le rock'n roll), lis ses tweets,
et j'ai pu croiser celui-ci (assez brièvement) lors d'une présentation
Scrum autour de Montpellier.

{{< image src="/images/2010/08/claudeaubryscrum.jpg" title="Scrum" >}}

Ses posts (blog) sont généralement simples (dans le bon sens du terme)
et efficaces. Je m'attendais donc à quelque chose du même acabit. Mais
en me disant : encore un bouquin sur Scrum, sur l'agilité... Il y en
déjà pas mal, et des bons, enfin, la littérature disponible sur le web
est pléthore. Bref l'exercice n'est pas facile.

Mais disons le tout de suite, ce livre est une réussite et l'épreuve est
passée avec brio.

Premier bénéfice évident :  un livre qui aborde et regroupe beaucoup
d'informations concernant Scrum **en français**. Cela manquait et c'est
très bien. Beaucoup de gens ne sont pas forcément à l'aise avec
l'anglais et cela pouvait être une source de blocage ou de quiproquo (et
donc de rejet de Scrum).

Deuxième évidence : Claude a sacrément creusé son sujet. Il ne mégote
pas sur la qualité, mais aussi sur la quantité. Il a beaucoup de
contenu. Beaucoup de sujets sont triturés, fouillés (théorie, pratique).
Il a une approche très cadencée : thème A, point 1, point 2, point 3,
etc. On va donc pouvoir se servir de ce livre comme référence, ce n'est
pas un simple ouvrage pédagogique, d'initiation, d'introduction, c'est
aussi un manuel (...hum... un guide ? ah oui. c'est ça).

Enfin Claude est un bon vulgarisateur (dans le bon sens du terme). Tout
est expliqué clairement et semble couler de source. En cela j'imagine
que Scrum l'a aidé.

Pour finir, la réussite du livre tient dans ces 3 facteurs. On a un
ouvrage qui va permettre au débutant de se plonger facilement et
entièrement dans Scrum, tout en demeurant avec le temps un manuel de
références et de rappel des bonnes pratiques pour quelqu'un de plus
avancé sur le sujet.

J'en recommande la lecture à tous (avec en fond sonore le dernier album
2009 des [Black Crowes](http://www.blackcrowes.com/) qui se transcendent
avec l'âge : "Before the frost").

[Amazon](http://www.amazon.fr/SCRUM-guide-pratique-méthode-populaire/dp/2100540181/ref=sr_1_1?ie=UTF8&s=books&qid=1282113463&sr=8-1) ?

ps : En tant que lecteur j'apprécierai pour son prochain livre soit un
recueil de retours d'expériences ; soit un approfondissement de la
problématique de la conduite du changement à mener dans une organisation
avec l'introduction de l'agilité et de Scrum.

ps 2 : Il trouve le moyen de caser Led Zeppelin dans son livre, j'admire.
