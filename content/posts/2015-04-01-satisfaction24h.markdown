---
date: 2015-04-01T00:00:00Z
slug: satisfaction-en-24h-maximum
title: Satisfaction en 24H maximum !
---

Fécondité, chance, réussite, soigne les organisations, les couples, les
grilles de loto, les différents avec les belles-mères, arrange vos
entretiens annuels, vos primes, protection contre les coachs et les
gurus dogmatiques, désenvoûtement, résout problèmes inconnus, vos
problèmes familiaux, vos ennuis financier et sociaux, examens, solitude,
possède des pouvoirs infaillibles sur le retour de l’être aimé, docteurs
purs et sincères.

Pour le [Scrumday 2015](http://scrumday.fr/) (c'est demain et vendredi
!), enfin toute la vérité sur Agile. Avec le [Docteur
Christophe](http://freethinker.addinq.uy/) (*Agile ? Vous avez dit Agile
? Comme c'est Agile...*) remettons la vérité au centre de la table de la
cuisine. En face des [imposteurs de la coach
clinic](http://scrumday.fr/2015/01/29/la-coach-clinic-est-de-retour/),
de **17h à 18h**, nous distribuerons les **secrets de nos potions**.

## Notre carte de visite

{{< image src="/images/2015/04/marabouts-pub.jpg" title="Désenvoûtement" >}}

## Nos Célèbres Potions

-   “le pouvoir de la coercition”
-   “rendre heureux en oppressant”
-   “réussir sa transformation agile en une semaine”
-   “un agiliste certifié en vaut deux”
-   “un bon coach agile est un coach mort”
-   “enfin l’agile sans sortir du dev”
-   “faites un exemple pour les faire marcher droit”
-   “la sérénité de l’équipe grâce à l’écran de fumée des indicateurs”
-   “distribuer les titres pompeux pour valoriser les personnes”
-   “les bonus : la clef de la productivité”
-   “mesurer la valeur d’un produit au nombre de ligne de code”

et pour finir, l’incroyable, la mystérieuse, l’indéfinissable, enfin
dévoilée en Europe, **“formule secrète des estimations”**.

## Rendez-Vous demain

17h-18h en face de l'ignomineuse "clinique des coachs" soit-disant
agiles. **Venez avec vos dollars \$ \$ \$**
