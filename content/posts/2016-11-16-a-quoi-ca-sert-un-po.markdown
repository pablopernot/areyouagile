---
date: 2016-11-16T00:00:00Z
slug: a-quoi-ca-sert-un-product-owner
title: À quoi ça sert un product owner ?
---

C'était la première de [School of PO](http://schoolofpo.com). Le sujet
était "À quoi ça sert un product owner ?", grisé par les bulles de mon
perrier, lors d'un repas avec [Lucie](http://lucyinthescrum.com) et
[Dragos](http://andwhatif.fr) on décide de basculer vers un format à
risque : le pecha kucha. Risqué mais aussi très rigolo, et surtout court
(20 slides de 20 secondes soit 6mn40 montre en main sans avoir le droit
de trébucher), ce qui allait parfaitement pour notre soirée.

Pour voir les vrais slides avec le défilé en vingt secondes, et les gif
qui s'animent (ça compte) c'est là : [Pecha Kucha School Of PO](/slides/schoolofpo/).

Je vous les commente ici à l'image de ce que j'ai pu dire durant ces
6'40".

## Pecha Kucha "À quoi ça sert un PO ?"

### \#1 : Introduction : school of po

{{< image src="/images/2016/11/pechakucha1.jpg" title="Petit scarabée" >}}

(un peu de flûte à la façon de caine dans la série)

Bienvenue à la premier session de "school of po".

Une communauté dédiée aux Product Owner. Je suis ravi d'ouvrir le bal en
compagnie de Lucie et Dragos Mais c'est une communauté et nous comptons
aussi beaucoup sur vous. Cette première session n'est que le début, et
avant de plonger au fil de nos rencontres dans certains sujets de façon
plus avancée je me suis dit qu'un rapide panorama serait nécessaire.

### \#2 : Cartographier avant d'explorer

{{< image src="/images/2016/11/pechakucha2.jpg" title="Cartographier" >}}

Si je devais aborder le product owner en 6 minutes 40 je voudrais
d'abord évoquer une sorte de cartographie : à quoi sert-il ? qu'est ce
que l'on attend de lui ? quel type de personnes est-il ? Existe-t-il des
product owner type ?

Bref pour citer "Girls" une série documentaire sur l'anthropologie
féminine : commençons par cartographier avant d'explorer.

Tout le monde sait que le product owner doit être mené par la valeur.
Par l'idée de créer de la valeur. Il sert à créer de la valeur. Voilà.
Mais qui est-il et comment s'y prend-il ?

### \#3 : Stratégie : quoi, pourquoi, vision

{{< image src="/images/2016/11/pechakucha3.jpg" title="Stratégie" >}}

A quoi ça sert un PO ? Un PO c'est d'abord le garant d'une stratégie.
d'une vision. Garant de l'explication sur le quoi et le pourquoi. Une
stratégie c'est quand on décide quelle est la cible, quels chemins nous
décidons de prendre pour nous y mener, quelle posture nous décidons
d'avoir. Une stratégie c'est quand on pointe un endroit sur la carte. Et
que l'on décide de quelle manière on va essayer d'y arriver. Un stratège
il donne du sens, une perspective. Il ne discute pas des détails, il
parle de la vision. Il est attaque ou défense, il pense que la posture
doit être frénétique ou posée. Il défini une destination, et ce que nous
emportons avec nous.

Le PO est donc une personne qui doit voir les enjeux. Une vraie
sensibilité.

### \#4 : Faire émerger, valider, étoffer, pivoter, innover

{{< image src="/images/2016/11/pechakucha4.jpg" title="Emergence" >}}

Mais le monde qui nous entoure est mouvant. Ce stratège du produit, de
la création de valeur autour de son produit ne doit pas l'ignorer. Il ne
peut pas avoir une stratégie figée. Sa stratégie doit se nourrir de
l'état de son produit, du temps qu'il a devant lui, et du terrain dans
lequel il se déplace. Il doit observer la concurrence, le marché, faire
de la veille, s'interroger constamment sur ses clients. C'est un
stratège constamment en éveil, comme un maître aux échecs, mais c'est
aussi un stratège créatif comme un artiste.

### \#5 : Tactique, outillage et savoir faire

{{< image src="/images/2016/11/pechakucha5.jpg" title="Tactique" >}}

Le PO est un stratège mais c'est aussi un tacticien. Le stratège pense
le produit en maintenant en éveil son attention sur tout ce qui se
déroule autour de lui, et ce qui pourrait émerger. Mais le PO doit aussi
être un tacticien. Le bras armé du stratège en quelque sorte. Le
stratège donne une perspective, le tacticien c'est celui qui met en
musique sur le terrain, les pieds dans la gadoue. Celui qui suivant la
stratégie s'applique à découper, à organiser, à prioriser, à répondre
aux questions qui surviennent dans l'action.

### \#6 : Expertise

{{< image src="/images/2016/11/pechakucha6.jpg" title="Expertise" >}}

Cette tactique demande souvent une expertise, un véritable apprentissage
: comment énoncer des éléments de valeurs, comment découper les produits
avec des *user story mapping*, savoir faire des *event storming*, des
*impact mapping*, comprendre ce qu'est une bon approche BDD (*behaviour
driven developpment*). S'attacher à se projeter dans des personas.
Réaliser des plans de release, répondre à chaud aux développeurs, faire
des choix soudains mais légitimes. Il est au contact des équipes
naturellement, en leur sein. (le stratège peut aussi utiliser des outils
pour penser sa stratégie néanmoins).

### \#7 : Facilitation : communication, clarification, dynamique

{{< image src="/images/2016/11/pechakucha7.jpg" title="Facilitation" >}}

Plongé au milieu des équipes, après stratège et tacticien, il possède
une troisième face, celle de facilitateur. À ne pas confondre avec le
facilitateur qu'est le scrummaster. Le product owner doit être un bon
facilitateur de son produit. Bien savoir exprimer sa valeur, bien savoir
exprimer ses enjeux, bien savoir exprimer son agenda, l'état des lieux,
les choix qui ont été faits, pourquoi, quels sont les résultats.

S'assurer d'avoir bien intégrer les bonnes personnes dans sa réflexion,
de bien avoir clarifié à chacun les impacts qu'il espérait obtenir. Le
scrummaster est un facilitateur de l'organisation, le product owner est
un facilitateur de la valeur produit.

### \#8 : Coaching

{{< image src="/images/2016/11/pechakucha8.jpg" title="Coaching" >}}

Une bonne partie de cette facilitation va s'adresser à l'équipe. Le
product owner est en leur sein. Il doit constamment reformuler,
anticiper, faciliter l'appropriation de la réalisation par les équipes
et la mise en évidence des aspects valeur, ses choix, ses compromis.
Pour cela il explique, utilise les outils portés par sa face
expert/tacticien. Il est à l'écoute, il crée un lien avec ses équipiers.

Forcément il endosse les résultats, et protège son équipe. Assume ses
choix. Il est important du coup qu'il soit légitime.

### \#9 : Contexte, selon les moments, selon les tailles

{{< image src="/images/2016/11/pechakucha9.jpg" title="Contexte" >}}

Tous ces rôles peuvent paraître beaucoup. Stratège, tacticien,
facilitateur. Peut-être que certains d'entre vous ce sont retrouvés dans
l'une de ces postures, mais pas les trois. Ou pas les trois à la fois.
Tout va dépendre du contexte.

Qui a déjà porté le rôle de stratège ? de tacticien ? de facilitateur du
produit ?

### \#10 : Petite organisation, startup

{{< image src="/images/2016/11/pechakucha10.jpg" title="Petite organisation" >}}

Dans une petite organisation, une pme, ou une startup on est souvent les
trois à la fois. Voire plus on développe, on réalise aussi des fois.
C'est passionnant. Le risque c'est de ne laisser parler que la ou les
identités qui nous ravissent le plus. Le tacticien qui oublie la
stratégie. Le stratège qui oublie la mise en oeuvre, ou celui qui est
incapable de faciliter autour de son produit rendant à néant tout son
travail de stratège et de tacticien. Il n'y a pas vraiment de
saisonnalité. On comprend que l'on facilite tout le temps, ou que l'on
est expert à agencer le bon déroulement de l'action tout le temps. On
pourrait se dire que l'on est stratège qu'à certains moments clefs, en
début de cycle. Mais bien souvent le stratège se nourrit des
apprentissages qui reviennent du terrain pour changer sa stratégie, ou
du marché qui évoluent autour de lui, ou de cette idée qui surgit sans
crier gare. Il est donc lui aussi toujours là.

### \#11 : Grande organisation, product manager ?

{{< image src="/images/2016/11/pechakucha11.jpg" title="Grande organisation" >}}

Et puis l'organisation grandit. Impossible d'être les trois à la fois.
Il va falloir choisir souvent entre le stratège et le tacticien, vous
devez toujours être facilitateur. Dans les grosses structures il émerge
souvent ce que l'on appelle le "product manager". C'est le product owner
stratège. Et il estime qu'il a des product owner qui mettent en
application sa stratégie, les product owner tacticiens. On a donc
souvent un product manager et son équipe de product owner. Et cela
pourrait se répéter. Un product manager qui supervise un produit, une
équipe de product owner affectés à ses grosses branches, eux mêmes les
product managers en quelque sorte d'équipes de product owner. Attention
cependant ne commencez pas à vous enflammer avec cette idée de
réplication comme à l'infinie. Il existe de grandes limites : il faut
pouvoir appréhender la compréhension du montage, il faut pouvoir gérer
la complexité technique à assembler le tout, etc etc. Pas d'illusion sur
la mise à l'échelle.

### \#12 : Grande organisation, équipe de PO ? PO Proxy ?

{{< image src="/images/2016/11/pechakucha12.jpg" title="Grande organisation bis" >}}

Dans les grandes organisations le PO qui vient souvent du métiers (il
pourrait venir du métiers c'est naturel, mais aussi un bon chef de
projet qui comprend les enjeux d'un produit, comme un PMO). Or le
métiers, cette partie qui se veut noble de l'entreprise n'a pas le temps
c'est bien connu.

Survient alors souvent le proxy PO. Qui pallie à l'absence du PO. Quid
de la légitimité ? Quid de la pertinence ? De l'un ou de l'autre
d'ailleurs du Product Owner comme du Proxy PO ? Pourquoi ne pas garder
simplement celui qui a du temps et qui saura faire le job ? Pourquoi
garder dans la boucle cela qui n'a pas le temps ou qui ne saura pas
faire le job ? C'est le temps de la politique qui surgit, des imposteurs
et des impostures.

### \#13 : Imposteurs

{{< image src="/images/2016/11/pechakucha13.jpg" title="Imposteurs" >}}

Des fois proxy PO ça marche, mais alors pourquoi ne pas les appeler PO
directement ? Par ce qu'ils ne sont pas payés par les mêmes personnes ?
Parce qu'ils ne sont pas chapeautés par les mêmes personnes ? Parce que
cela mettrait en défaut certaines personnes qu'ils deviennent PO à la
place du PO ? Mais quid alors de la légitimité ? De la pertinence des
réponses ? De la responsabilité sur les choix et la valeur délivrés ?

Que faire avec un proxy PO qui n'est qu'expert sans savoir. Sans
compréhension du produit. Et donc sans facilitation ?

Où sont les budgets ? Qui sont les vrais porteurs du produit et de la
valeur ? Pourquoi est-ce que les porteurs du produit n'ont jamais le
temps ? Quel sens donnent-ils à leur action ? Quels buts poursuit-on si
ce n'est pas la valeur produit ?

### \#14 : Impostures

{{< image src="/images/2016/11/pechakucha14.jpg" title="Impostures" >}}

Cet animal est le MOA, enfin c'est le megalateryx, de la famille des
MOA. Vous avez bien compris que souvent ce n'est pas la valeur produit,
la stratégie produit qui est défendu par l'organisation, mais bien
l'organisation elle-même qui cherche dans un non sens farfelu à entrer
dans un costume qui n'est pas le sien.

Bien souvent des MOA, des MOE (que ceux qui ne connaissent pas ici ne
cherchent pas à découvrir le sens de ces acronymes), se cachent sous les
titres de PO proxy, de Product Owner, d'Appui PO. On y retrouve de tout.
Des facilitateurs techniques ! De très bons Product Owner contraint à
être proxy pour ne pas mettre en péril l'organigramme précédent, ne pas
en créer justement, de précédent.

En péril la légitimité du product owner, et tout s'effondre.

### \#15 : Product Owner type ?

{{< image src="/images/2016/11/pechakucha15.jpg" title="Product Owner type ?" >}}

Comprendre les méthodes, les outils, mais aussi les aspects
relationnels, et les systèmes complexes. Comprendre que l'on ne peut pas
apprendre à la lettre, comme une organisation ne peut pas répliquer une
autre organisation. c'est passionnant. Il n'y a donc pas de product
owner type. Ils peuvent venir de plusieurs horizons. Mais il est
indéniable qu'ils doivent *in fine* avoir une appétence avec le produit.
Avec le sens, avec la valeur.

Il n'y a pas de bon cas de figure, c'est encore, toujours, l'intention
et l'attitude qui compte. Et bien avoir en tête les différentes facettes
de son métiers.

### \#16 : Les défauts de ses qualités ?

{{< image src="/images/2016/11/pechakucha16.jpg" title="Product Owner type ? bis" >}}

J'ai pu croiser des product owner allergiques aux documents, à la
formalisation, mais qui avaient une précision, un focus, une vision sur
leurs produits, et une capacité à transmettre les bonnes informations
que personne ne réclamait autre chose. Qui endossaient leurs
responsabilités dans les résultats du produit.

J'ai connu des product owner attentifs et précis qui rédigeaient de
belles histoires utilisateurs. De belles explications. Ce qui en soit
facilitait l'équipe, sans les étouffer. Ils savaient remettre en
question leurs écrits. Ils savaient retourner la table quand soudain
c'était nécessaire.

J'ai aussi connu des product owner qui ne faisaient pas grand chose et
pour cause ils n'avaient pas grand chose à dire ou à faire. Et
laissaient les équipes dans un noman's land de sables mouvants. Ou
d'autres très besogneux à cracher des documents ultra précis, obsolètes
dans la seconde, qui fossilisaient les conversations, statufiaient les
équipes.

### \#17 : Fier / Valeur

{{< image src="/images/2016/11/pechakucha17.jpg" title="Fier / Valeur" >}}

On a vu qu'il pouvait être stratège, tacticien et facilitateur. Dans sa
posture de facilitateur du produit, qu'il appuie sur sa face stratège ou
tacticien. Le product owner devrait être fier d'apporter sa pierre à
l'édifice. Il apporte de la valeur, il en est fier. Fierté ne veut pas
dire perfection. On peut être fier d'un petit morceau de plante verte au
milieu d'une immense décharge à ciel ouvert.

Fierté, frustration, abattement, des sentiments qui démontrent une
implication. Un product owner ça sert à construire des cathédrales,
pierre par pierre.

### \#18 : Apprendre

{{< image src="/images/2016/11/pechakucha18.jpg" title="Apprendre" >}}

Dans cette quête de la valeur, on apprend sans cesse. Un product owner
ça sert à faire apprendre son organisation, à valider ses hypothèses. A
transformer hypothèses en valeur sonnante et trébuchante. A confirmer
que sa stratégie est la bonne.

### \#19 : Apprendre au plus tôt

{{< image src="/images/2016/11/pechakucha19.jpg" title="Apprendre au plus tôt" >}}

Et à la confirmer au plus tôt ! donc à moindre coût d'une part, et à
bénéfice plus rapide d'autre part. Il lui faudra donc savoir découper en
petits ensembles qui font sens. Ce n'est pas un mince apprentissage.

De façon plus avancée il pourra intégrer des choses aussi comme le coût
du délai. Combien coûte une fonctionnalité que je n'ai pas livré plus
tôt ? Au sujet de laquelle je n'ai pas appris plus tôt ?

### \#20 : Merci

{{< image src="/images/2016/11/pechakucha20.jpg" title="Merci" >}}

Merci...

> [Pecha Kucha School Of PO](/slides/schoolofpo/).

