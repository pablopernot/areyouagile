﻿---
date: 2020-10-30
slug: hors-sujet-musique-confinement
title: "[hors sujet] Musique & confinement"
---

{{< image src="/images/2020/10/setup.jpg" class=center >}}

En ce jour un du deuxième confinement de l'an vingt vingt je me permets un hors sujet complet aux thèmes de ce journal : installation musicale pour passer du temps agréablement en restant chez soi. J'espère que vous pardonnerez cet écart et j'espère que ce témoignage donnera envie ou servira à d'autres musiciens. L'installation dont je parle m'a été elle-même soufflée par [N1k0](https://nicolas.perriault.net/), merci ! Ainsi donc deux objectifs pour cet article hors sujet : partager, et me faire un aide-mémoire.  

{{< image src="/images/2020/10/komplete.jpg" class="left" alt="Komplete studio 2" title="Komplete studio 2"  >}}

Sur la photo au dessus : une guitare (en l’occurrence une Gibson Firebird série Tikibird 2013), un ordinateur portable avec Linux Ubuntu, une carte son usb externe (avec deux entrées XLR pour instruments ou micro) Komplete Studio 2. Pas dans la photo : un copain qui enregistre lui sa part de batterie de son côté. Du mien : un "enregistreur, table de mixage multi-pistes" : [Reaper](https://www.reaper.fm/). Il marche très bien sous Linux Ubuntu, il est bien au delà de Audacity, il est beaucoup plus compréhensible et fonctionnel que Ardour.   

{{< image src="/images/2020/10/reaper.jpg" class=center alt="Reaper" title="Reaper" >}}

Pour accéder à une multitude d'effets et de sons on ajoute une couche appelée [Tonelib GFX](https://tonelib.net/) (idem des paquets sont disponibles et marchent bien pour Linux Ubuntu). Par exemple ci-dessous j'ai choisi l'effet "de base" : *classic blues* (mon trip c'est le **mississippi hill country blues**, essayez **R.L. Burnside** ou **Junior Kimbrough**).

{{< image src="/images/2020/10/tonelib-2.jpg" class=center alt="Tonelib" title="Tonelib" >}}

Ainsi j'ai une guitare, j'ai la piste batterie d'un copain (ou j'utilise Hydrogen sous Ubuntu Linux, une boite à rythme), je branche la guitare sur la carte son Komplete Studio 2 qui envoie le signal dans ToneLib GFX qui me place une couche "voilà l'effet que tu voulais", qui ensuite part du côté de ma table de mixage multi-pistes (Reaper). Je peux chanter (si si) en même temps dans un micro qui entre dans la deuxième entrée de ma carte son. Et qui lui entre directement dans la table de mixage multi-pistes sans passer par la couche effet. 

## Deux petits challenges 

### Son en temps réel : JACK & QJackCtl

Important : il faut une gestion du son en temps réel. Sinon vous jouez de la guitare, puis le processeur passe aux travaux sur les effets, puis aux travaux sur le multi-pistes, puis il revient à la guitare, et tout devient complètement désynchronisé, inaudible. Il faut que le son soit prioritaire et que tout ce qui touche au son reste synchronisé. Pour cela on utilise [Jack](http://linuxmao.org/Jack). Jack permet une gestion en temps réelle, synchronisée, de toutes ces sources sons. 

{{< image src="/images/2020/10/jack-1.jpg" class=center alt="QJackCtl" title="QJackCtl" >}}

Galère sous Ubuntu Linux il va falloir jouer entre PulseAudio (système son de Ubuntu), et Jack (qui permet le temps réel et plus). Il faut une bonne config pour passer simplement de l'un à l'autre. QJackCtl permet une gestion simplifiée de tout cela.  

Là ci-dessous j'ai bien coché "temps réel" et j'ai baissé les échantillons et la période assez bas pour avoir une latence maximum de 17.4ms comme indiqué. Comprendre une désynchronisation maximum de 17.4 millisecondes.  

{{< image src="/images/2020/10/jack-4.jpg" class=center alt="Jack : réglages" title="Jack : réglages" >}}


Là ci-dessous j'ai ajouté l'argument "pasuspender -- jackd" : quand le démon Jackd est lancé il suspend PulseAudio. Et vous voyez que le périphérique d'entrée est K2,0 (Komplete Studio 2 branché en usb)

{{< image src="/images/2020/10/jack-3.jpg" class=center alt="Jack : réglages" title="Jack : réglages" >}}

Quelques autres options au cas où : 

{{< image src="/images/2020/10/jack-2.jpg" class=center alt="Jack : réglages" title="Jack : réglages" >}}


### Faire tous les bons raccords 

Autre petit challenge, faire tous les bons branchement : de la carte son piste 1 faire les effets puis vers la table de mixage, et de la carte son piste 2 directement vers la table de mixage, faire sortir la table de mixage sur la sortie standard (pour moi). 

Ici le *système capture* le son (depuis le Komplete 2). J'envoie le 1 vers JuceJack (qui est l'interface de Tonelib), et le 2 (le micro chant) directement vers Reaper. J'envoie ToneLib vers Reaper (la guitare + l'effet). Si j'utilisais Hydrogen comme boite à rythme, elle apparaitrait (si je lui dis d'utiliser Jack comme système son), et je l'enverrais aussi vers Reaper. Puis Reaper sort sur la sortie standard (mon casque quoi). 

{{< image src="/images/2020/10/jack.jpg" class=center alt="Jack : brassages" title="Jack : brassages" >}}

{{< image src="/images/2020/10/reaper-2.jpg" class=center alt="Reaper réglage son" title="Reaper réglage son" >}}

{{< image src="/images/2020/10/tonelib-3.jpg" class=center alt="Tonelib réglage son" title="Tonelib réglage son" >}}


Et c'est parti !! Ah non, maintenant je dois apprendre à chanter... 


### Conclusion 

Ce n'est que le début mais c'est vraiment beaucoup de choses facilitées et qui deviennent passionnantes. Un vrai petit studio sur le coin de votre bureau. 
Merci Nicolas pour la découverte. Mais est-ce que tu fais les choses différemment ? 

## Update !

Si je copie ToneLib-GFX.so dans \~/.vst où si j'indique le bon chemin dans la configuration (/usr/lib/vst/ToneLib-GFX.so pour moi) : je peux gérer ToneLib directement dans Reaper. Beaucoup plus pratique. Merci N1k0 !

{{< image src="/images/2020/10/reaper-3.jpg" class=center alt="Tonelib dans Reaper" title="Tonelib dans Reaper" >}}


