﻿---
date: 2019-11-14
slug: conversations-du-moment
title: Conversations du moment
--- 

Je vous livre ici quelques conversations et messages que je porte actuellement dans nos communautés agiles et tech (https://coaching.benextcompany.com). 

## Présupposé sur la qualité du contexte

Qu'ils soient tout nouveaux dans le rôle de *product owner* ou de *scrummaster*, ou plus anciens, j'entends régulièrement dans la bouche des gens : les banques ce n’est pas sexy comme environnement, les médias cela me plaît beaucoup plus. C'est totalement infondé. Ce n'est pas la matière de l'entreprise qui va vous donner sa saveur. Naturellement vous pouvez décider selon vos convictions de ne pas accompagner une compagnie pétrolière, un armateur, ce que je comprends et conçois très bien, mais ce n'est pas le sujet que j'évoque là. On imagine que les grosses structures, bancaires souvent, ou les assurances, sont moins intéressantes que les startups, ou les médias, etc. La réalité que je connais dément que l'on puisse par avance prédéfinir cela. Dans un grande entité, tel département sera passionnant, l'autre non. Le même département pourrait-être excitant à ce moment, et ne le sera plus l'année suivante. Ce qui peut paraître de loin excitant (travailler pour un grand média français) et peut se révéler une déception (pas d'espace, pas de capacité à avoir un impact, pas de créativité, etc.). Je ne sais absolument pas vous répondre par avance où vous allez tomber. Je ne défends ni les uns ni les autres. J'essaye de faire comprendre cela.   

## Temporalité du coaching n'a rien à voir avec de la cause à effet

Le plus gros choc que se prend le nouveau venu dans le monde du coaching organisationnel c'est le changement de temporalité. Chez nous les graines de coachs ne sont pas toujours préparées a cela. Il n'y a plus vraiment de cause à effet, il y a un ensemble de dynamiques qui interagissent toutes les unes avec les autres d'une façon ou d'une autre, par effet miroir, par rebond, par écho, etc. Il y a de grands vides, et soudain des ruptures, des déchirements, ou rien, ou des césures. Mais cela n'est pas dicté par une quelconque dynamique, mais plus par des vibrations qui a un moment (ou jamais) font vaciller les positions, et les postures. Pour certaines de nos graines de coachs, et plus largement pour les coachs, c'est une douleur. C'est un rôle difficile, finalement assez solitaire et sans assez de reconnaissance palpable. Nous ne trouvons pas comme dans du coaching de développement personnel dans un cadre ou une demande est exprimée (des fois clairement) et mieux encore, ou le coaching est désiré. Il faut être prêt-à cela.

Ils veulent soudainement partir, car leurs actions ne semblent pas avoir d'effet. Je n'en sais rien, mais la dureté de la tâche les projette souvent trop vite dehors. Il faut savoir être patient. Toute la difficulté est de ne pas être trop patient. Décider au dernier moment responsable reste un bon adage. Et il faut apprendre à vivre dans ce *no man's land* (je ne parle des personnes qui abusent des systèmes pour végéter dans un coin).     

## Qui est le client ? 

C'est très flou dans le coaching organisationnel. Il n'y a pas de cadre comme dans un coaching en développement personnel, ni une demande aussi claire, ni un désir aussi affirmé. Et je ne crois pas -- à ce jour -- que cela puisse être le cas, le vrai client étant l'organisation à mes yeux. Sa demande passe par l'entremise d'une personne qui applique son filtre. Voire plusieurs personnes, le demandeur, le payeur (le vrai payeur est l'entreprise). Que demande l'organisation, que souhaite l'organisation me semble être la vraie question. Mais c'est un jeu dangereux, en donnant cette demande à un être qui n'existe pas j'ouvre la porte à la possibilité de faire plein d'interprétations fausses. D'apposer nos convictions et de s'autoriser à ne pas écouter les personnes. Mais les personnes véhiculent avec elles une couche de politique et d'habitudes qu'il faut aussi filtrer. On est pris dans des difficultés de toutes parts. Néanmoins il me semble que prendre le recul et le parti que l'organisation soit le vrai client donne un espace et amène des questions plus pertinentes. Et n'est-ce pas cela le rôle de la confrontation que d'interroger de façon pertinente ?   

## Et la suite ? 

Et après agile ? On s'en fout non ? Actuellement la difficulté est que agile n'est jamais la cible et que trop souvent il le devient. Et peut-être plus important : le rythme de la réflexion et de l'évolution des observations sur les entreprises, ce rythme me semble plus dynamique que celui de l'appropriation par les entreprises de ces nouvelles disciplines, savoir-faire et savoir être. D'où le grand sentiment de tourner en rond dans la communauté dite agile. Mais peut-être arrivons-nous dans une période d'un effondrement de masse des entreprises qui n'ont pas su se réformer. Je le souhaite. Je ne suis pas du tout collapsologue, cet effondrement serait une bonne nouvelle (même si malheureusement il entraînerait peut-être une sacrée casse sociale).   


