---
date: 2017-10-02T00:00:00Z
slug: etre-engage-2
title: Être engagé ? (partie 2)
---

Quand Dan Mezick m'a parlé de ces idées sur l'engagement inspirées par
le monde des joueurs de jeux vidéos, courant 2013, il enchaînait souvent
en citant aussi des enseignements de Tony Hsieh le leader de Zappos
(quand Zappos était symbole de réussite). Ces quatre autres éléments que
je vais vous donner faisaient complément, renforçaient les précédents
points ([être engagé - partie 1](/2017/09/etre-engage-1/)). C'est avec
les années que j'ai vraiment compris que cette seconde liste équilibrait
autant la première : garde-fou, résonance.

Voici les quatre points en résonance de ceux évoqués précédemment. Pour
être impliqué, engagé, il faut :
 
## Un sentiment de contrôle

C'est le garde-fou des règles claires évoquées dans la première série
sur l'engagement ([être engagé - partie 1](/2017/09/etre-engage-1/)).
Dans celle-ci je vous parlais d'un cadre, lié à un objectif clair, et
des règles claires qui aidaient à prendre l'espace, à s'impliquer, à
s'engager. Elles impliquent à la condition qu'elles n'entravent pas trop
l'autonomie. Elles sont là pour donner de l'autonomie, pas la
contraindre. Il s'agit d'un sentiment de contrôle de son outil de
travail, pas de contrôle de l'autre. Imaginer un individu dans un groupe
qui démarre son activité et qui doit demander à tel autre groupe l'accès
aux ressources, aux machines, à tel autre groupe le droit de sécurité,
mise en production, accès à telle donnée... qui ne peut pas manipuler
véritablement son outil de travail car sa juridiction est morcelée. Pas
de sentiment de contrôle de son outil de travail, perte de son
implication. C'est pour cela que souvent dans un monde agile on
recherche des "feature teams", des équipes pluridisciplinaires, elles
possèdent une capacité d'autonomie, elles contrôlent leur périmètre. Les
gens doivent pouvoir s'approprier leur périmètre. Des règles claires
mais qui préservent l'autonomie.

## Un sentiment de progrès

Là c'est simplement l'écho de l'idée du feedback que j'évoquais
précédemment. Il susciter l'engagement il faudrait régulièrement savoir
ce que l'on produit, génère, obtient. Il faudrait (très) régulièrement
mesurer. Pour cela il faudrait (très) régulièrement finir des choses. Et
ainsi à travers ce besoin de sentiment de progrès je voudrais
sensibiliser les gens à ne pas travailler sur trop d'éléments à la fois.
Le multitâche est très nocif à notre productivité. Voyez ce schéma :

{{< image src="/images/2017/10/concurrent_assigned_tasks.jpg" title="Tâches concurrentes" >}}

avec une tâche je suis normal, avec deux je suis meilleur car je peux
basculer de l'une à l'autre pendant les délais incompressibles. Avec
trois ma productivité est déjà généralement en dessous de celle que j'ai
en traitant une tâche à la fois. Si je passe à quatre ou cinq cela
devient la bérésina.

J'en viens à dire aux organisations : "cessez de travailler sur dix
projets à la fois, focalisez vous sur les trois plus importants". Des
fois on me répond, narquois, "mais Pablo c'était le 8,9,10 qui
permettaient de te payer". Alors je reprends (eh eh) : "arrêtez le
septième vous le finirez plus vite". C'est une approche cohérente
globale : si vous savez découper en petits morceaux vos
projets/produits, vous saurez délivrer une moelle substantifique dans un
laps de temps plus court, et passer au suivant. Vous démarrez le suivant
plus tard, mais avec un focus beaucoup plus fort, et aussi cette
capacité à condenser la valeur dans un scope plus réduit. Et ainsi de
suite.

Pour être engagé, il faut garder un sentiment de progrès, nourrit par le
feedback, il faut savoir être focalisé sur un nombre réduit d'activités.

## Appartenance à une communauté

Avec l'invitation on entre dans un groupe, une famille, une équipe.
C'est une bienvenue. L'appartenance à une communauté parait une évidence
pour se sentir engagé, impliqué. Cela l'est, une évidence. J'en profite
pour revenir sur ce qui fait un groupe, une équipe, sur quelques
chiffres importants. Ces chiffres je les évoquais dans [la horde
agile](/2014/01/mini-livre-la-horde-agile/), en 2013.

### Le chiffre de Dunbar

Dunbar est un sociologue/anthropologiste, il constate que les villages
de la préhistoire se scindent autour de 150/220 personnes, laissant
penser que notre physiologie nous interdit d'avoir plus de 150/220
réelles connections sociales. Si donc je veux encourager un sentiment
d'appartenance à une communauté je vais limiter mes
"divisions,départements,agences" à 150/220 personnes.

### Plaisir d'appartenance à un groupe

{{< image src="/images/2017/10/satisfaction.jpg" title="Niveau de satisfaction" >}}

Avec les recherches de Christopher Allen, on évoque le plaisir
d'appartenir à un groupe, [quel chiffre déclenche cette satisfaction](/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/) ? Le tableau ci-contre indique que le meilleur sentiment de
satisfaction, qui appuiera un sentiment d'appartenance, se ressent
autour de 7 et 50. Si donc je suis un dirigeant je vais découper mon
organisation en divisions de 150/220, puis en départements de 50, puis
en équipe de 7.

Souvent c'est le moment où quelqu'un dit qu'on ne peut ainsi pas faire
de "gros projets", de "grands produits" avec une équipe de 7 par manque
de capacité. Peut-être mais avec 7 équipes de 7 on le peut.
Naturellement les "gros projets" entraînent un besoin de synchronisation
(c'est tout l'enjeu du *agile at scale*, **culture et
synchronisation**), mais il est bien plus facile de synchroniser 7
équipes de 7, que de travailler "en rateau" avec 50 personnes.

### Communications interactives

{{< image src="/images/2017/10/combinaisons.jpg" title="Charge / Combinaisons" >}}

Enfin le chiffre 7 est corroboré par l'observation de Fred Brooks (dans
le vieux et célèbre "homme mois mythique"), qui démontre visuellement
que le nombre d'interactions devient insoutenable dès que l'on franchit
la barre des 7/9 personnes. C'est à dire qu'une vraie communication et
dynamique d'équipe se bâtit... de 4 (à partir de quatre sensation
d'équipe, avec c'est encore des individualités, ce n'est pas négatif
cependant), jusqu'à 8. Au delà de 8 on observe que les communications
interactives se scindent en deux groupes.

Ainsi donc à la recherche d'un sentiment d'appartenance je vais décliner
mon organisation en groupes de 150/220, puis 50, enfin des équipes de 4
à 8. Ainsi mon implication et mon engagement seront meilleurs.

### Courbe de Allen

{{< image src="/images/2017/10/allen-curve.jpg" title="Allen Curve" >}}

Puisque l'on parle qualité de la communication, je précise souvent que
la co-localisation est clef. Cette observation est confortée par les
études "officielles" (de IBM !) qui décrivent qu'au delà de 15m la
communication s'effondre. Aleister Cockburn utilise la métaphore
suivante : au delà de la distance d'un bus la communication s'effondre.

Ces dernières années je pourrais remettre en question cette question de
distance et de co-localisation car les nouveautés technologiques peuvent
la rendre invisible. Sous couvert de plusieurs conditions, mais c'est
probablement l'objet d'un autre article.

## Travailler pour quelque chose qui nous dépasse, de plus grand que nous

Nous avions parlé d'un objectif clair pour engager les joueurs de jeux
vidéos. Dans "la vraie vie" -- comme on l'entend souvent -- celui doit
être habité par du sens. C'est la fable du tailleur de pierre, je suis
engagé si je ne taille pas simplement une pierre, ou même si je ne fais
que cela pour nourrir ma famille, je suis engagé, impliqué, si je vois
la cathédrale que nous batissons au travers de ma taille.

## Conclusion de cette série

Je reprends comme je le disais dans cette série des écrits passés, qui
me semblent toujours d'actualité avec des conversations actuelles et
récentes. Je vais continuer ce travail de réfection.

Pour conclure sur l'engagement : **un objectif clair qui soit habité par
du sens, des règles claires mais qui laissent assez d'espace pour
l'autonomie et le sentiment de contrôle, du feedback qui permette un
sentiment de progrès régulier, une invitation et les conditions d'une
dynamique de groupe.**

Dans la série :

-   [être agile](/2017/07/etre-agile/)
-   [être engagé - partie 1](/2017/09/etre-engage-1/)
-   [être engagé - partie 2](/2017/10/etre-engage-2/)

