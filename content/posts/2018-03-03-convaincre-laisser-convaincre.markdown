---
date: 2018-03-03
slug: convaincre-ou-se-laisser-convaincre
title: Convaincre ou se laisser convaincre ?
---

Vous avez tous vécu le repas de famille qui tourne au débat politique. Des fois c'est bien, haut en couleurs, égayé par les rires et le vin rouge qui coule à flots. Des fois cela fini au couteau, on se dit : "plus jamais ça". On y cherche à convaincre, cela tourne à la blague ou à l'insulte. Mais personne n'est jamais convaincu. En tous cas personne n'est jamais convaincu sur le moment, comme si c'était mathématiques, 1+1=2, on ne sera pas convaincu, on le sait. **On n'est jamais convaincu. On se laisse convaincre**. C'est nous qui décidons, par une réflexion personnelle, une introspection, de prendre en considération cette idée, ou pas. Mais personne ne nous convainc jamais, nous nous laissons convaincre. C'est nous qui décidons.


**Les organisations, les personnes, ne vont pas où elles ne souhaitent pas aller**. 

### Forcer ?

Alors c'est sûr on peut les forcer. Ben oui. C'est quand même le plus simple. Ils n'ont qu'à y aller, là où on veut les mener. On décide pour eux. C'est facile. Reposant pour le cerveau. Politiquement on sait comment cela s'appelle. Pour les organisations cela leur permet d'être médiocre. Beaucoup estiment que c'est suffisant. Peut-être est-ce le cas ?

{{< image src="/images/2018/03/courbe-changement.jpg" title="imposition du changement" >}}

### Convaincre ? 

Entre forcer et convaincre avec insistance, quand on est du côté du pouvoir, la différence est faible. Mais entre convaincre et forcer on a déjà franchit une frontière importante. On commence à saisir que le choix, que la décision se fait du côté de l'autre. Mais cela reste ambigu. On imagine qu'en mitraillant l'autre d'arguments irréfutables il ne pourra que céder. C'est comme une bataille où l'on ne convainc pas l'autre mais juste où on le vainc. Comme si on avait compris que c'est l'autre qui avait la décision mais dans les faits on le nie encore. Comme si l'autre n'avait toujours pas son mot à dire.   

Dans le subversif "Rules for radicals", Saul Alinsky raconte l'histoire du gouvernement mexicain qui, dans un geste altruiste, pour le bien du peuple, décide par un décret de restituer à leurs propriétaires et gratuitement toutes les machines à coudre immobilisées dans les [monts-de-piété](https://fr.wikipedia.org/wiki/Mont-de-pi%C3%A9t%C3%A9) (organismes de prêt sur gage). Tout le monde applaudit, est heureux. Trois mois après toutes les machines à coudre sont revenus dans les monts-de-piété. **La morale : même le bien ne s'impose pas**.  

Sans travail de réflexion personnelle, sans introspection, il n'y a pas de progrès. Ce qu'exprime Saul Alinsky en écrivant : 

> "L'éducation doit être utilisée de toutes les façons possibles comme un mécanisme éducatif, un apprentissage en tant que tel, mais l'éducation n'est pas de la propagande.(...). Sans processus d'apprentissage, la transformation (fabrication) d'une organisation devient simplement la substitution d'un groupe de pouvoir par un autre." --- Saul Alinsky, Rules for radicals [^saul]

[^saul]: "Education has to be used in every possible sense as an educational mechanism, but education is not propaganda.(...) Without learning process, the building of an organization becomes simply the substitution of one power group for another." -- Saul Alinsky, Rules for radicals.

En forçant, en cherchant à convaincre, on substitut une organisation par une autre, sans conscience, par imposition, par propagande. Il est nécessaire d'avoir un processus d'apprentissage, une réflexion personnelle, une [éducation](https://fr.wikipedia.org/wiki/%C3%89ducation). Éducation dont le sens étymologique est "guider hors de". 

Encore aujourd'hui, tout d'un coup quand je vois que j'essaye de convaincre, je sais que c'est que j'ai pris le mauvais chemin. 

Ainsi la véritable question, qu'il s'agisse de discussions politiques lors d'un repas de famille, ou de transformer une organisation[^orga], est : comment susciter cette introspection ? 

[^orga]: Je parle d'organisation comme une personne, mais j'ai appris que lorsque l'on cherche à transformer des personnes bien souvent on transforme des organisations, et quand l'on cherche à transformer des organisations, bien souvent on transforme des personnes. 

## Comment susciter cette introspection ? 

### Inspirer 

Rappelez-vous qu'on se laisse convaincre. Le mouvement vient de la personne qui va chercher ce qu'elle désire. Elle tire à elle, on ne lui pousse pas, et si on pousse, elle prend ce qu'elle veut. Une des façons de faire est de laisser les autres piocher en vous ce qu'ils y voient et ce qu'ils y désirent, vous ne poussez pas, vous offrez. A vous d'incarner ce à quoi vous croyez, et d'inspirer. Cela vous permet de faire, d'être dans l'action. Et d'être une sorte de source pour ceux qui le souhaitent (à votre charge aussi d'expliciter la cohérence nécessaire entre vos différentes actions). 

Vous permettez cette introspection chez l'autre en incarnant votre récit. Il vous voit agir et se projette, se compare. Libre à lui de se laisser influencer. Comme l'effet miroir qu'évoque Matthew Lieberman : 

> "Si vous voyez quelqu’un frapper un clou avec un marteau, les zones de votre cerveau liées à cette activité sont en éveil ; les histoires provoquent un effet miroir" --- "Social : Why Our Brains Are Wired to Connect", Matthew D. Lieberman . 

C'est une forme du **mentoring**. La personne compare son histoire à la votre, et peut s'en inspirer. 

### Générer un sentiment d'urgence ?

Une autre façon de susciter l'introspection c'est de susciter un **sentiment d'urgence**. "Cela ne peut plus durer ainsi". C'est ce que l'on retrouve dans les propositions de [John Kotter sur la conduite du changement](https://fr.wikipedia.org/wiki/John_Kotter). Dont le point #1 est : 

> "Générer un sentiment d'urgence".  --- John Kotter

On lie facilement urgence et insécurité, **sentiment d'insécurité**, de crainte pour la survie de l'entité, pour soi, de l'emploi, des acquis, etc. A mes yeux, la peur n'est jamais bonne conseillère. On fait rarement les bons choix sous l'emprise de la peur. Les bons choix se font par envie, par désir (notamment car du coup il n'y a pas de regret). Si ce sentiment d'urgence est imposé, poussé par l'extérieur, cela devient une urgence, une insécurité difficile à vivre, non positive. Si elle est née d'une introspection elle peut être positive, comme par exemple celle de Simon Wardley, qui perdu, changera sa destinée avec un élan né d'un sentiment d'urgence. 

> "J'ai demandé aux jeunes ce qu'ils pensaient de la stratégie. Ils m'ont répondu "ça semble ok". Mon cœur n'a fait qu'un bond. (...)Je n'avais aucune réponse. J'étais un imposteur comme Directeur Général ! Je devais apprendre vite avant que quelqu'un ne s'en rende compte. Mais comment ?" --- Simon Wardley, [Wardley Maps, Chapter "on being lost"](https://medium.com/wardleymaps/on-being-lost-2ef5f05eb1ec)[^wardley]

[^wardley]: "I asked one of my juniors what they thought of our strategy. They responded “seems fine to me”. My heart sank. Unlike that confident executive in the lift of the Arts hotel who was testing some junior, I still hadn’t a clue. I was an imposter CEO! I needed to learn fast before anyone found out. But how?" --- Simon Wardley, [Wardley Maps, Chapter 1](https://medium.com/wardleymaps/on-being-lost-2ef5f05eb1ec)


### Générer un sentiment de sécurité ? 

La seconde c'est de se sentir assez sécurisé pour essayer des choses, avoir l'espace pour s'autoriser cette réflexion. Se sentir assez sécurisé c'est l'inverse du sentiment d'urgence : rien n'est en péril, je n'ai rien à y perdre, je pourrais revenir en arrière si besoin. C'est ce que l'on retrouve dans la mouture ["modern agile"](http://modernagile.org/) qui essaye de rajeunir le manifeste agile et qui indique : 

> "un cadre sécurisé est un prérequis". --- http://modernagile.org/ [^modern]

[^modern]: "Make Safety a Prerequisite" --- http://modernagile.org/ 

La recherche d'un sentiment de sécurité est ainsi facilement compréhensible : en libérant des contraintes, des stress, des peurs, on laisse réapparaitre l'envie, le désir, un espace pour le questionnement. Ils peuvent s'exprimer car ils sont sécurisés. Cela ouvre à l'expérimentation. 

**Dans ma démarche je vais donc chercher à incarner et à faire ce à quoi je crois, en espérant inspirer. Et je vais m'interroger sur comment sécuriser les personnes avec qui j'évolue pour leur donner cet espace nécessaire à leur propre expérimentation et réflexion**. 

...Sauf dans les repas de famille vous l'aurez compris.
