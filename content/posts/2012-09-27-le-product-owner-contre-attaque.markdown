---
date: 2012-09-27T00:00:00Z
slug: le-product-owner-contre-attaque
title: Le product owner contre attaque
---

Le temps a filé, comme d'habitude, et Alexis n'a pas chomé, comme
d'habitude. Depuis notre dernière conversation il a travaillé sur un
ebook qui ne tardera pas à sortir. Mais je dois vous redonner un peu le
contexte. Alexis écrit depuis de longue date au sujet du poker, des
échecs ou d'autres jeux, comme [Mémoire 44](http://www.daysofwonder.com/memoir44/fr/content/guide/) (**© Days of
Wonder 2011**) (jeu de stratégie de type *wargame*). Non content
d'écrire il édite: [Praxeo](http://praxeo-fr.blogspot.fr/), sa maison
d'édition est reconnue dans son domaine, je vous encourage à passer sur
son [blog](http://praxeo-fr.blogspot.fr/). Quand nous nous sommes
croisés au cours de l'une de mes missions, les discussions autour de la
stratégie et de la tactique du product owner ont ravivé un longue
article enfoui qu'il n'avait jamais achevé...jusqu'à présent. Le
croisement de nos deux univers : management de produits en agile,
management d'organisation, et stratégie et tactique de grands champions
(de jeux, de champs de bataille, etc.) n'est pas nouveau (beaucoup de
citations de
[Sun-Tzu](http://fr.wikipedia.org/wiki/L%27Art_de_la_guerre) un peu
partout), mais l'approche proposée par Alexis est originale. (Alexis me
glisse : Sun-Tzu est un joueur de GO, Clausewitz un joueur d'échec, leur
approche est donc inverse, allons au delà).

## ebook

Nous voilà donc 3 mois plus tard. Excités par nos débats, Alexis s'est
replongé dans cet article inachevé qu'il a transformé en ebook (jusqu'à
présent 80 pages passionnantes, probablement plus au final car je
détiens une version encore incomplète). (Je tweeterai naturellement dès
sa parution officielle). Cet ebook est encore très orienté jeux (go,
échec, mille bornes (!), etc.) mais on y trouve déjà d'autres pistes :
management d'entreprise, gouvernance politique, etc.

[Dans le précédent article](/2012/06/introduction-a-la-strategie-et-la-tactique-du-product-owner/)
Alexis a proposé de ramener ses analyses à deux systèmes :[le système de
comptage et le système de mort subite](/2012/06/introduction-a-la-strategie-et-la-tactique-du-product-owner/).
Aujourd'hui j'aimerais rappeler ces éléments en essayant de trouver des
exemples évocateurs et quelques scénarios ; puis évoquer un élément qui
a fait partie de nos discussions (et qui est abordé de façon plus
précise et plus efficace dans cet ebook) : le point d'inflexion.

## Exemples de système de comptage

**Premier cas de figure : trésorerie solide et domination du marché.**

Si je projette les propositions de Alexis le monde de l'entreprise, dans
mon monde agile, un product owner avec une bonne visibilité (c'est à
dire une trésorerie en bon état, pas "d'urgence") est assimilé à un
joueur de GO, il joue avec un système de comptage (c'est celui qui aura
le plus de points qui gagnera, il n'y a pas de "but en or"). En position
de force il consolide son emprise sur le marché. Par exemple Microsoft
fin 90, en retard sur le web mais en position dominante et sans risque
financier, se rattrappe avec IE alors que Netscape lui taille des
croupières : il consolide.

**Deuxième cas de figure : trésorerie solide mais pas de domination du
marché.**

Le product owner de Google au milieu des années 2000 n'a pas non plus de
problème de trésorerie mais à l'inverse il n'est pas dominant sur le
marché (pas encore...). Nous sommes toujours en système de comptage mais
nous sommes en retard, il faut prendre des risques : on essaye de
nombreuses pistes (google wave, gmail, google desktop, google maps,
google answers, etc.), certaines vont fonctionner d'autres non. Mais la
prise de risque est indispensable, et permise par le système de
comptage/la trésorerie (pas de mort subite en vue).

## Exemples de système de mort subite

Vous êtes en système de mort subite quand votre trésorerie ne vous
permet pas d'avoir une bonne visibilité. La faillite est

potentiellement au bout de la route.

**Troisième cas de figure : trésorerie à risque (faillite possible), et
domination du marché.**

Troisième cas de figure donc, vous êtes dominant sur le marché mais
votre trésorerie n'est pas assurée (nous sommes aux échecs, le roi peut
se faire prendre or il faut à tout prix empêcher la prise du roi). Si
vous êtes dominant et sans garantie financière, vous devez rester
dominant. Continuer à faire la course en tête. A l'inverse du système de
comptage il faut donc continuer à prendre des risques, continuer à tenir
le marché, on garde son avance. Réflechissez à ces startups qui font
émerger de nouveaux secteurs (et donc prennent une position dominante
dessus) sans avoir de sécurité financière. Elles doivent continuer à
prendre des risques, continuer à tenir le marché jusqu'à que leur
trésorerie leur permettent de passer en système de comptage. Facebook ?
(est probablement passé depuis en système de comptage...), Linkedin (est
passé en système de comptage comme le prouve sa nouvelle interface
"twitter", dominant et avec de la trésorerie, désormais Linkedin
consolide), "Les copains d'avant" (n'a pas réussi à passer en système de
comptage et s'est fait doublé, n'est plus dominant sur le marché),
Realplayer ? Flash ? qui est dominant sur le marché mais n'a pas
forcément de trésorerie associée devrait attaquer beaucoup plus...sauf
si nous avons déjà basculé dans le dernier cas de figure.

**Quatrième cas de figure : trésorerie à risque (faillite possible) et
pas de domination du marché.**

Dans ce dernier cas de figure notre entreprise n'a pas de garantie
financière (le roi peut se faire prendre), et n'est pas dominante sur

le marché : elle doit consolider, elle doit survivre. On doit d'abord
s'assurer de la survie du roi avant d'envisager une prise de risque.
J'évoquais Linkedin, Viadeo est son pendant : il n'est pas dominant sur
le marché et a du/doit s'assurer un fond de trésorerie pour passer en
système de comptage avant de repartir à l'assaut de Linkedin.

Imaginons que vous êtes le product owner de Viadeo (tout ceci n'est que
supposition, je ne connais pas cette société) vous avez d'abord
consolidé vos acquis pour permettre à votre compagnie de solidifier sa
trésorerie, puis vous vous êtes lancé à l'assaut du leader (Linkedin)
par le biais de l'innovation/prise de risque. Si cela a échoué il est
probable que cela a mis à mal vos finances et vous devez peut-être à
nouveau consolider vos acquis. Si cela a réussi vous êtes devenu leader
et vous devez consolider ce qu'il manque à votre portefeuille de
fonctionnalités (observer les non dominants et leurs prises de risque
qui aboutissent).

## Le point d'inflexion

Comme vous l'avez noté on comprend qu'une entité passe d'un système à
l'autre. Mais même dans un cycle court, et dans vos projets, cette
bascule a lieu. C'est par le biais du mille bornes et d'un dialogue
entre enfants que Alexis va introduire une notion bien connue ou à
défaut souvent ressentie par les product owners : le point d'inflexion.
Il insiste sur le côté intuitif de cette inflexion (et quoi de plus
intuitif que des enfants ?), je constate aussi cette intuition chez les
product owners.

Si je résume la démonstration de Alexis, lors d'une partie de mille
bornes vous êtes en système de comptage. Donc dominant vous consolidez
(mettre des bornes), non dominant vous prenez des risques (attaque :
cartes qui freinent, entente entre les joueurs, trahisons, etc, tout ce
qui désarçonne, fait changer le cours du jeu, permet de changer les
positions -car vous n'êtes pas dominant-). Au début tout le monde est a
égalité et se sent dominant, tout le monde consolide en plaçant des
points. C'est quand l'un des participants se détache franchement que
cette stratégie est remise en cause. Mieux vaut alors le freiner :
système de comptage en étant non dominant : prise de risques (comme
évoqué ci-dessus, n'importe quoi qui remette en question la domination
du leader). Mais si cela devient urgent (notion temporelle !) c'est que
l'on est probablement passé en système de mort subite. **On a passé le
point d'inflexion**. Si désormais on ne stoppe pas le leader on perdra.
Et donc désormais on consolide. il ne faut pas mourrir. Les alliances
deviennent évidentes : tous contre un. Les priorités aussi : tout pour
stopper le dominant. Il n'y a pas/moins de prise de risque : les
réponses sont évidentes, il faut consolider.

Naturellement vous avez vécu cela si vous êtes product owner. Au début
du projet/produit on prend des risques, on prend des positions, on test
des pistes, des fonctionnalités, des technologies/approches innovantes.
Mais vient le moment de la release (notion temporelle !) où il faut
consolider. On est passé d'un système de comptage à un système de mort
subite. Vous n'êtes pas dominant sur un marché : vous devez prendre des
risques, innover, tenter, surprendre, etc. Mais quand approche le temps
de la release il faut consolider vos acquis. Il faut "**sacrifier la
section faible**" pour reprendre une expression que Alexis affectionne,
solidifier, consolider, quitte à abandonner, trancher. Risque de mort
subite, nous ne sommes pas en position de force, on consolide.

Product owners vous devez vous interroger : êtes vous en système de
comptage ou de mort subite ? Etes vous dominant sur votre marché ?
(votre marché pouvant se définir de bien des façons, on peut imaginer le
marché interne de l'entreprise). Quelle est donc la bonne stratégie à
utiliser ? A quel moment devrait se situer le point d'inflexion ?

Dans le prochain article (le retour du product owner) sur cette
thématique j'évoquerais "le calcul de Pascal" tel que me l'a décrit

Alexis (et peut-être le mode Berserker des startups, une discussion
hilarante entre nous !). Mais naturellement je vous encourage à vous

ruez vous sur le ebook que j'évoquais dès qu'il sera disponible
(surveillez [Praxeo](http://praxeo-fr.blogspot.fr/) à ce sujet).

L'article précédent était  : [Introduction à la stratégie du product owner](/2012/06/introduction-a-la-strategie-et-la-tactique-du-product-owner/)
