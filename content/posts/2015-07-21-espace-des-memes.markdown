---
date: 2015-07-21T00:00:00Z
slug: lespace-des-memes
title: L'espace des mèmes
---

Ces derniers jours, Mike et Ivana me
proposent de tenir avec eux l'open space ([Open Space de Harrison Owen](http://elementaleducation.com/wp-content/uploads/temp/OpenSpaceTechnology--UsersGuide.pdf)) de la prochaine conférence [ALE2015](http://ale2015.alenetwork.eu/)
(conférence qui demeure, à mes yeux, dans mon coeur, ma favorite chaque année : j'y retrouve des potes et des gens passionnants
([Mike](http://agileexperts.at/blog/), [Olaf](http://trustartist.com/),
Alberto, Pawel, Stephen, Duarte, etc.), concernant mes sujets de
prédilection, le tout mélangé dans une grande sauce européenne.

## Open Agile Adoption &gt; Open Space

Bref, Mike me propose te tenir l'Open Space avec Ivana. Ma première réaction est
mitigée. Je suis un peu usé des Open Space en conférence compte tenu de
l'intensité et de l'impact que je vis au travers des [Open Agile
Adoption](/2014/04/histoires-dopen-agile-adoption/) (proposé et fondé
par Dan Mezick voilà deux ans). Le côté concret, opérationnel, les
cycles, font des [Open Agile Adoption](/2014/04/histoires-dopen-agile-adoption/) des choses bien plus
stimulantes que les Open Space, que j'aime pourtant beaucoup.

En fait ce n'est pas que Open Agile Adoption soit meilleur, car il se
base sur l'Open Space, mais qu'il le complète parfaitement et que le
cadre d'une organisation lui donne une dimension nouvelle.

Du coup je propose à Mike et Ivana de tendre vers un Open Agile Adoption
(un Open Space avec des éléments supplémentaires qui me paraissent
clefs) : donner un thème, donner des contraintes, consolider un cahiers
(pdf) à la fin, se donner rendez-vous l'année prochaine, et aussi mes
quelques ajustements.

### Quelques ajustements à l'Open Agile Adoption

Faire des sessions de 1heure dont 40mn de brainstorming et 20mn de
restitution commune avec tous les groupes (j'ai vu que cette pratique
différait de celle de Dan et qu'il s'y intéressait).

Je propose aussi depuis peu de "histoires du changement" pour consolider
de façon un peu plus normalisée (pas trop non plus pour ceux qui me
connaissent bien, normaliser, cela m'est assez étranger) ce qui a émergé
des *brainstorming*.

[Histoire du changement](/pdf/histoire-changement.pdf), ou en anglais
(merci Mark Sheffield), [Change Story](/pdf/change-story.pdf)
(directement inspiré pour le format par notre session avec Oana sur le
*storytelling*, et qui m'a semblé couler de source quand j'ai cherché à
normaliser les émergences des *brainstorming*).

### Mais c'est ALE, bon sang

Mais difficile d'envisager cela avec ALE : il ne s'agit pas d'une
entreprise, mais d'un groupe d'énergumènes qui se dissipe (le groupe)
pour se reformer tous les ans. On peut poser ces contraintes salvatrices
car créatrices (le conteneur libère, un paradoxe) à une entreprise
(thème, contraintes, etc.), cela devient coercitif dans un autre
environnement. Je doute donc de leurs pertinences dans des conférences
ouvertes, encore plus à [ALE2015](http://ale2015.alenetwork.eu/) qui est
totalement ouvert, et dont la maturité des acteurs est assez élevée.

## L'espace des mèmes

Mais alors que cette question d'Open Space à ALE me trotte dans la tête,
je me réveille un matin la semaine dernière avec l'idée suivante :
pourquoi ne pas utiliser l'Open Space comme a pu le faire Dan Mezick au
sein de son Open Agile Adoption, mais dans une tentative pour voir les
choses différemment. Voir les choses différemment c'est beaucoup plus et
mieux lié à [ALE2015](http://ale2015.alenetwork.eu/). Utiliser l'Open
Space pour des ateliers genre *Design Thinking* ou *Lean Startup* c'est
en plus très tendance.

### Parenthèse sur le succès actuel du *Design Thinking*

Les entreprises, les structures ont du mal avec Agile, pour plein de
raisons valables, malheureusement la plus courante ne l'est pas : elles
ne se donnent généralement pas les moyens d'y aboutir, tout simplement
parce qu'elles ne le souhaitent pas vraiment ; cela remet trop souvent
en question beaucoup de ce qu'elles sont et de ce qu'elles ont été, de
ce que ses hommes et femmes sont, et ont été. Du coup tout le monde se
lance dans le *Lean Startup* et surtout maintennt le *Design Thinking*
en mode laboratoire, en mode cellule, en mode isolé, protégé, où là on
peut se permettre d'oser, d'essayer. Malheureusement sans intégrer
l'entièreté de la structure cela va se révéler souvent creux.

Ce succès me va cependant si on est conscient que cela ne restera pas un
laboratoire, *Design Thinking* c'est encore une façon différente
d'appeler la même chose. *Agile, Lean Startup*, etc.

## L'espace des mèmes (reprise)

Donc ce matin je me dis : utilisons l'*Open Space* (de Harrison Owen)
qui a montré combien il était source de richesse. Introduisons des
éléments de *brainstorming* destructurants pour mieux penser
différemment, ramenons les choses à un conteneur de sortie un peu plus
normalisé comme j'ai pu le faire avec les "histoires du changement"
(voir plus haut). Et ainsi j'ai proposé à
[ALE2015](http://ale2015.alenetwork.eu/): L'*espace des mèmes* (*The
Meme Space*).

### Qu'est ce qu'un [Meme](https://en.m.wikipedia.org/wiki/Meme) ?

Un [Meme](https://en.m.wikipedia.org/wiki/Meme) c'est -- à l'instar des
gènes -- le plus petit porteur de sens de nos cultures et de nos
réflexions. Si les gènes portent nos caractéristiques physiques, les
mèmes transmettent nos caractéristiques culturels. Comme les gènes, ils
évoluent, se transmettent, certains ont des anomalies qui les font
disparaître ou au contraire devenir dominants, etc.

Wikipedia nous dit (en [anglais](https://en.wikipedia.org/wiki/Meme), en
[français](https://fr.wikipedia.org/wiki/M%C3%A8me)) : "c'est un élément
culturel reconnaissable répliqué et transmis par imitation du
comportement d'un individu par d'autres individus". Un élément culturel
transmis autrement que par la génétique. Richard Dawkins, qui est le
père de l'expression, précise : "Unité d'information contenue dans un
cerveau, échangeable au sein d'une société". Les cultures évoluent comme
les êtres vivants. Élément culturel c'est à dire que cela peut être une
idée (le *scrumboard* par exemple), comme un comportement (les cheveux
longs des groupes de hard rock des années 80, comme autre exemple).

**L'objectif de cet espace est d'essayer de faire muter les mèmes de
notre culture.**

En vous expliquant les étapes ci-dessous j'estime que vous êtes
accoutumés aux *Open Space* de Harrison Owen, voire même des [Open Agile
Adoption](/2014/04/histoires-dopen-agile-adoption/) de Dan Mezick.

### Etape 1 : "meme board"

A l'image de jeux comme [The Big
Idea](https://boardgamegeek.com/boardgame/696/big-idea) ou [Give them a
hot tub](http://www.innovationgames.com/hot-tub/) on prépare un mur de
memes, un "meme board", avec trois colonnes. Dans la première colonne on
place des mèmes de notre culture (*scrum, kanban, invitation,
visualisation, équipes, réunion, valeur, intégrité, transparence*,
etc.), dans la seconde des adjectifs (*rapide, lent, facile, dangereux,
sérieux, triste, puissant, rouge, vert*, etc), dans la troisième des
objets et des lieux de notre monde (*téléphone, escalator, mur de Chine,
voiture*, etc). N'hésitez pas, comme tout *brainstorming*, à ne pas
limiter vos dérapages et délires. Disons dans chaque colonne 50 à 100
éléments. J'imagine que l'on peut arriver avec une liste toute faite,
mais la faire compléter par les personnes présentes me semble naturel.

### Etape 2 : place de marché du forum ouvert

Tout le monde prend le temps de bien regarder le mur des mèmes, vous
demandez aux gens qui le souhaite (on garde naturellement le plus
important des élément des Open Agile Adoption : l'invitation) de choisir
une triade (un élément de chaque colonne). Ce choix doit se faire **par
intuition**, avec les tripes, à l'instinct, pas question de pré-calculer
une mutation. Quand quelqu'un à une intuition il prend la triade et la
place sur la place de marché.

Ainsi comme les [cadavres
exquis](https://fr.wikipedia.org/wiki/Cadavre_exquis_%28jeu%29) de nos
chers surréalistes, laissons d'abord apparaître la créature, le sens
viendra après. (Quitte à se retrouver avec un objet du célèbre catalogue
des objets introuvables de [Jacques
Carelman](https://fr.wikipedia.org/wiki/Jacques_Carelman))

### Etape 3 : les *brainstorming*

Des pistes d'Open Space de 1 heure, qui en réalité durent 40 minutes, et
demandent, comme lors des Open Space que j'accompagne : une restitution
"flash" (1 or 2mn) globale à la fin de la piste. Par exemple 10 groupes
qui viennent chacun 1mn sur le devant de la scène résumer le fruit de
leur réflexion. J'ai noté que cela nourrissait les réflexions suivantes,
voire générait l'abandon ou la relance de sujets.

Par ailleurs, mon conseil pour ce genre d'exercices : les triades ayant
été choisies à l'instinct et le groupe (je vous ai dit que je poussais à
un maximum de 8 personnes car au délà la dynamique change ?) s'étant
composé à la volée, je suggère de démarrer par simplement la liste de ce
que cela a évoqué chez chacun avant d'aller plus loin.

### Etape 4 : La consolidation et le canevas de mutation (mutation canvas)

Comme je peux le faire depuis quelques temps avec les Open Space, je
propose un canevas de sortie des *brainstorming*, ce qui me permet de
très rapidement (le soir ou le lendemain) distribuer un pdf un tant soit
peu cohérent entre les différentes sessions. C'est encore trop récent
(dimanche matin), je dois méditer ce "mutation canvas". Mais j'imagine
qu'il doit être assez léger (4/5 champs max) : milieu ? Modification de
l'information (modification, substitution, création, suppression), quels
éléments ? Dans quel but ? Élément clef de la mutation ? Acteurs ?

### Etape 5 : cycle et viabilité de la mutation

Comme dans un [Open Agile
Adoption](/2014/04/histoires-dopen-agile-adoption/) une approche
cyclique permettrait de tester, d'essayer la viabilité de la mutation.

Pour finir, toujours comme dans un Open Agile Adoption, on peut imaginer
un thème et des contraintes, même si c'est plus étranger à la notion
d'accident génétique, et donc d' "accident mémétique".

### ALE 2015 ?

Je ne sais pas si Mike et Ivana vont adhérer, et donc je ne sais pas si
je pourrais essayer le premier "meme space" à Sofia (en Bulgarie) cet
été, en août, (il reste des places :
[ALE2015](http://ale2015.alenetwork.eu/)). Cette "destructuration
orientée mutation" (concept inventé spontanément sans substance
particulière) me semble pertinente, sur le tableau en tous cas, voyons
comment elle se comporte en la plongeant dans le bouillon de la vraie
vie.

PS : on pourrait presque appeler cela un "Mutation Park"

PS2 : imaginé sous l'influence du coffret : Live at Felt Forum (New York
1970) des Doors

