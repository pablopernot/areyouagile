---
date: 2021-09-30
slug: accompagner-les-cadres-dirigeants 
title: "Faut-il accompagner les cadres dirigeants ?"
--- 

Vous avez tous envie de crier oui, je vous entends, car c'est flatteur, lucratif et intéressant. Intéressant ? Des fois oui des fois non.[^1]

[^1]: Des fois, disons un article sur 5 ou 10, je propose à ma compagne de lire avant de publier. Ce fut le cas cette fois, je devais avoir des doutes sur ce que je peux infliger à des lecteurs, cela n'a pas raté : "Eh ben, ce n’est vraiment pas ton meilleur". Voilà, dans la gueule. Bon "que cela soit mon meilleur" n'a pas de sens, je n'écris pas pour que cela soit bon, j'écris comme catalyseur de mes pensées, pour les malaxer, les former, les confronter. J'ose presque le parallèle avec une séance de psychanalyse où le fait d'exprimer ses pensées permet de les voir, les observer, les comprendre différemment, etc. Sur ce je vous laisse. 

D'autant plus qu'il n'y a pas de grands changements organisationnels et donc produit, technique, etc. sans l'implication et l'action des dirigeants de haut niveau des entreprises. C'est donc un passage obligé. (Cf [Un café craft de Thomas Wickham: La transformation commence par celles des managers](https://open.spotify.com/episode/2jWqBst2sxUQA7T7TZKg0A?si=ce284ab411264a73))

**Faut-il accompagner les cadres dirigeants qui ne le veulent pas ?**

Les cadres dirigeants qui refusent notre accompagnement. Ceux-là, non, naturellement, 
vous avez la réponse dans la question.  

Mais faudrait-il accompagner les cadres dirigeants qui font appel à nous, 
mais qui font tout pour ne pas nous écouter, mais qui font tout pour ne rien entendre de nos conversations ?
Pour lesquels nous ne sommes finalement qu'un simulacre ou pour lesquels nous apparaissons finalement comme trop éloignés de leur monde, et donc sans valeur ? 
 
Comment faire la différence entre quelqu'un qui pense vouloir, mais qui ne le veut pas vraiment, quelqu'un qui fait véritablement semblant, quelqu'un qui veut véritablement ? 

Et faut-il faire la différence ? 

C'est une épineuse question que je me pose régulièrement. 

Pas de baratin (a.k.a. #nobullshit) les simulateurs, les gesticulateurs, les hautains, je n'en peux plus. 

Je n'ai pas envie de perdre du temps et de l'énergie avec ceux qui font semblant, qui ne veulent pas vraiment. Pourtant parmi ceux-ci il peut y avoir des moments de réalisation, des épiphanies, des moments "ahah" qui changent leurs perspectives et qui en font des acteurs des mouvements que nous accompagnons. Moi aussi je peux avoir ce type de moments, des apprentissages inattendus, une expérience qui s'enrichit. 

**Le problème c'est que l'on ne sait jamais si ces moments vont se produire, et quand.** 

Enfin, un simulateur peut être quelqu'un qui observe et quand il est convaincu c'est un pilier fort. 
Un gesticulateur peut être quelqu'un qui soudain saisit la situation et attrape un focus, il possède alors une énergie redoutable. 
Un hautain peut être quelqu'un doté d'une forte expérience, qui si il est enclin à l'utiliser, se révèlera de grande valeur.

En entretien d'embauche, je délivre certaines de mes convictions : 

* On évolue principalement dans de la gadoue, c'est une métaphore pour indiquer qu'il faut s'extirper des contingences politiques, techniques, sociales. Et surtout qu'il faut aimer cet effort, que c'est en partie l'objectif que de se sortir de la gadoue. 
* Que l'on a beaucoup plus d'espace qu'on l'imagine. Que l'on peut [fabriquer son job](/2019/02/fabriquer-son-job/) beaucoup plus qu'on ne l'imagine.  
* Que l'on ne peut pas juger le livre par sa couverture et qu'il faut vivre dans une situation pour vraiment la comprendre. 

Bref, tout peut arriver. C'est complexe. 

Quand c'est complexe, simplifions ([Vers le simple, vers le complexe](/2021/07/simple-complexe/)).  

Comment simplifier ? En mettant une durée limite de péremption pour ainsi dire une durée butoir de non-évolution à tout accompagnement. Mais une durée assez conséquente pour laisser le temps au contexte de se révéler au début puis d'évoluer ensuite.  

La gestion du temps est ardue sur le sujet du changement. Il n'y a pas de cause à effet, mais des besoins de sédimentation, de décantation, des boucles, des échos, etc. 

Je me donne, disons quatre mois maximum et trois mois minimum (à titre indicatif) pour laisser chacun (eux, moi) faire ses preuves. Preuves ? Preuves d'intention qui se transforment en réalisations. Des petits actes, des petites décisions, des détails, mais significatifs sur des prises de position. Pas uniquement des paroles, des petites décisions, des petits actes (la taille ne compte pas). Que l'on avance *a minima*. 

Cette période de temps est glissante. Est-ce que nous avons avancé les trois ou quatre derniers mois ? 

Je ne crois pas que cela soit une durée courte à l'échelle de l'entreprise, une entreprise qui ne bouge pas pendant quatre mois quand on parle de petits actes, de petites décisions, mais significatifs sur le fond, c'est une marque de blocage, de fossilisation.  

Parenthèse : cela n'exclut pas à certains moments de grands actes forts, ou des décisions d'ampleurs, etc. 

Si une période inconsistante se produit durant trois ou quatre mois c'est le moment d'aller voir ailleurs. Des fois, souvent, c'est la première période, dès le début. C'est pour cela que j'aime le conseil, je peux aller voir ailleurs, je ne suis pas enfermé dans un contexte. Je ne parle pas de fuite, d'évitement, je parle juste de ne pas insister quand ce n'est pas désiré. D'ailleurs je n'aime pas du tout les fuites, les évitements, trop facile, c'est pour cela que trois ou quatre mois (à titre indicatif) me paraissent un minimum. J'aime me coltiner le contexte, la gadoue, avec naïveté et candeur. Je me donne aussi trois à quatre mois pour faire mes preuves, pas question d'être un goujat qui n'entend rien lui non plus et se drape d'une morgue pour quitter le champ de bataille dès que les choses réelles démarrent. 


Des fois les fuites se cachent sous des aspects héroïques : vous ne voulez rien entendre je pars. Non je ne suis pas en phase avec cela, la gadoue ce n'est pas noir ou blanc. 
C'est l'intention de ces cadres dirigeants qui va compter, pas l'état des lieux. 

Après une dernière question vient : faut-il confondre les falsificateurs ? 

Ne perdez pas votre temps, faites les choses bien, passez à autre chose même si des fois cela vous démange beaucoup.

Et des fois cela me démange beaucoup, mon "sauveur" a envie d'envoyer un courrier vengeur exhortant tel ou tel "falsificateur" (à mes yeux) à utiliser son potentiel pour faire "le bien". Comprenez qu'entre le "sauveur", le "falsificateur", et "le bien" j'ai trois chances de commettre une erreur.  

**Pour conclure** 

* Pas d'accompagnement d'entreprise qui englobe toute l'entreprise, pas d'accompagnement global, sans les cadres dirigeants. 
* Oui nous sommes très souvent confrontés à des falsificateurs, simulateurs, des gesticulateurs, des manipulateurs. Des fois, ils en sont eux-mêmes conscients, des fois non. 
* Il faut se donner le temps, mais pas trop non plus, pour véritablement voir se révéler la situation et les leviers en présence ou non. À la louche cet intervalle de temps glissant que je me donne pour juger qu'un contexte *est (toujours,encore) vivant* oscille entre trois et quatre mois.  
* Se draper d'une posture de sachant et refuser d'aller creuser, ou de se plonger dans  "la gadoue" n'est pas digne à mes yeux.  
* Tout le monde peut changer, toutes les situations peuvent changer. 
* Inutile de perdre du temps à "faire la leçon" ou "le sauveur", on ne contrôle pas, on ne sait jamais vraiment : il faut lâcher prise. 









