---
date: 2023-08-10
slug: armee-banque-et-vous
title: "L'armée, les banques, et vous ?"
draft: false
---

Je n'aurais pas dû être surpris que l'armée soit déjà à préparer "la suite". Et pourtant la phrase choc dans la bouche d'un des plus hauts gradés de France (je ne suis pas spécialiste) m'a marquée : "Est-ce que j'ai tout fait pour être prêt ?"   

## L'armée...

Thierry Burkhard, Chef d'état-major des armées (clip de 2'03" récupéré sur twitter, mars-juin 2023 (pas de date précise))[^precis] :

{{< quote text="...Des temps très durs sont devant nous, (...) le changement climatique qui se vit tous les jours avec plus d'intensité va avoir des impacts considérables. Il va provoquer des affrontements nouveaux et nous obliger à changer notre manière de faire. Notre capacité à encaisser ces menaces et à relancer l'action par une manoeuvre coordonnée est essentielle. Tous les chefs, chacun à leur niveau, doivent décliner cette intention en passant toutes leurs actions au filtre des exigences de la haute intensité. J'ai besoin que chacun se dépense sans compter et se pose la question \"est-ce que j'ai tout fait pour être prêt ?\" " >}}

[^precis]: Si quelqu'un a un lien sur une vidéo officielle (ni sur twitter, ni sur youtube, etc.). Si quelqu'un veut vraiment l'original, je l'ai téléchargé (mais je ne veux pas mettre une vidéo de 13mo sur mon site, et je ne dois pas avoir le droit de la proposer ?). 

*Tous les chefs, chacun à leur niveau* : il doit bien parler de tous les chefs d'entreprises françaises aussi, non ? Cela doit les inclure. 

Dans la foulée, en écho, Clément Isaia me fait passer un document [\[Changement climatique, quels enjeux stratégiques pour les armées\]](https://www.defense.gouv.fr/sites/default/files/dgris/PLAQUETTE_DGRIS_CHANGEMENT_CLIMATIQUE.pdf). Mars 2023. 

Une phrase à nouveau me marque :  "Pour mieux comprendre les impacts du changement climatique, le renforcement des connaissances et des capacités d'adaptation est primordial." Le document se conclut par ce rappel : 

{{< image src=/images/2023/08/changement-climatique-armee.jpg alt="4 étapes pour l'armée en 2023" width="800" >}}

Et **nos entreprises ?** Ont-elles mesuré l'importance du renforcement des connaissances et des capacités d'adaptation ? Ont-elles déclenché des actions d'anticipation (si tant est que nous n'y soyons pas déjà) ? 

Concernant l'armée vous avez aussi cette page : https://www.defense.gouv.fr/dereglement-climatique-quels-enjeux-armees. 


## Et les banques...

Qui font partie de nos entreprises. Un rapport ubuesque, hallucinant, sort sur la Deutsche Bank (une banque allemande que l'on va donc associer à discipline, rigueur, précision, mais c'est un rapport fait par des Londoniens *a priori* :) ). Le rapport s'intitule "*The age of disorder*" (l'âge du désordre), son objectif est de préparer les ultra-riches au monde que décrit justement l'armée ci-dessus. Il est repris et [commenté en français ici](https://escapethecity.life/rapport-confidentiel-banque-crise).  

En quelques mots, je reprends juste les grands titres de l'article pour les pressés : 

- "*Un guide de survie économique pour ultra-riches*", 
- attention pour eux, à un "*vieillissement et manque de main-d’œuvre docile et pas chère*", 
- au "*retour de l'inflation va rogner les gros patrimoines*", 
- le plus glaçant : "*croissance VS environnement, la nouvelle guerre froide*", 
- et pour finir : "*stratégie : le populisme au secours des possédants*". 

Et puis il y a cette intervention que me conseille Sébastien Roccaserra de Daniel Rushkow ([lien youtube](https://www.youtube.com/watch?v=z83nXqbEy3U)), ce passage laisse pantois : 

{{< quote text="J'ai été invité pour faire une une conférence devant de riches investisseurs dans la tech et technologues, et il s'est avéré qu'ils ne voulaient pas que je fasse une conférence. Ils ont juste fait rentrer cinq hommes dans la pièce verte où je me préparais et ils ont commencé à me poser des questions sur leurs bunkers, leurs bunkers pour l'éventualité d'un effondrement.">}}

Le rapport est fou. L'idée de ces hommes qui utilisent Rushkow aussi. Mais. Mais. Cela indique que les banques, et les ultra-riches se préparent. 

Ils se préparent.

**Et nos entreprises ?** 

## Les entreprises

L'armée, les banques, il ne s'agit plus des activistes tant décriés dans certains milieux. 

Il est temps que nos entreprises aillent plus vite dans cette voie. On imagine bien qu'être avant-gardiste à un coût réel. Comme auparavant, avec pourtant la possibilité de je ne sais pas quelle *uberisation* de leur *business*, les entreprises semblent réticentes alors que le danger est là. La disruption liée au changement climatique, au déséquilibre des limites planétaires, est certaine. Si vous ne changez pas, vous serez le prochain Kodak (bon tant pis à vrai dire, mais nous serons les prochains dinosaures, c'est plus embêtant). 


- Avez-vous un plan stratégique lié à une inversion probable et proche de la phrase "croissance VS environnement" en "Pas de croissance sans intégration dans l'environnement" ? 
- Avez-vous identifié de nouvelles pistes, de nouvelles formes, d'expression de votre entreprise compatible avec une empreinte carbone massivement réduite de vous et de vous clients ou usagers ? 
- Avez-vous observé et exploité les instruments légaux qui vous permettent des investissements et de compenser des pertes ? 
- On sait que la compatibilité CSRD arrive à grands pas (et tant mieux), êtes-vous prêts ? 
- En cas d'incident soudain (voir les [scénarios de Frugarilla](/pdf/2023-SICT-D1.pdf)) comme la chute de vos datacenters ou l'impossibilité de remplacer votre *hardware* informatique (rares sont les entreprises dont l'un des cœurs névralgiques n'est pas l'informatique), avez-vous un plan de sortie de crise ? Avez-vous seulement évalué les pertes ? 
 

Anticipez, tracez des pistes stratégiques, préparez l'adaptation. 





