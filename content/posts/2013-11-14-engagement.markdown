---
date: 2013-11-14T00:00:00Z
slug: engagement-et-motivation
title: Engagement et motivation
---

Trois ou quatre petites saynettes (oui il semble que cela s'écrive
ainsi) concernant l'engagement et la motivation qu'il m'arrive de jouer
régulièrement. L'engagement et la motivation c'est tout le sens de
l'*invitation* si chère à Dan Mezick. Pour la première, c'est une scène
récurrente. Elle s'est déroulée chez plusieurs clients. Contexte : Scrum
ou genre.

## Conversation

début de la conversation.

Lui : "Je suis embêté, depuis ton passage les équipes s'engagent moins".

Moi : "c'est à dire ?"

Lui : "Et bien généralement avant elles s'engageaient entre 8 et 12
*user stories*, désormais c'est plutôt 5 ou 6..."

Moi : "Ah"

Blanc.

Moi : "J'ai observé qu'elles ne s'engageaient pas vraiment, que **vous
leur donniez un engagement**."

Lui : "Oui, on faisait les estimations et les engagements".

Moi : "Et *avant* elles délivraient 1 ou 2 *stories* finies ? n'est ce
pas ?"

Lui : "Oui, c'est vrai... parfois ! d'autres fois c'était bien 10 ou 12
!!".

Moi : "Oui mais dans ce cas elles se révélaient de mauvaises qualités,
pas vraiment finies ? hum ?".

Lui : "...oui."

Moi : "Ok. Donc tu as raison. revenons à un **engagement imposé**.
Recommence à leur donner un engagement de 8 ou 12."

Moi encore, agacé : "D'ailleurs comme du coup comme elles en délivrent 1
ou 2 à la fin, je te suggère de les faire s'engager sur 43 *stories*"

Moi encore, cette fois, goguenard : "Et d'ailleurs pourquoi pas 578 ? De
toutes manière on en 1 ou 2 à la fin."

Moi encore, narquois : "En fait on est bête autant viser **4563 *user
stories* par itération** ! Champion du monde !! puisqu'à la fin on en 1
ou 2 !! Ouuais"

Moi, apaisé : "Tu peux aussi continuer à les laisser s'engager et
délivrer des choses finies, de qualité, car leur implication n'aura rien
à voir. Et oui autour de 4 à 6 par itération d'après ce que l'on observe
actuellement".

fin de la conversation.

**Mots clefs** : Retour à un vrai engagement. Retour de la vérité.
Retour de la qualité. Retour d'une vraie planification. Retour à une
communication honnête. Retour de la motivation. etc etc etc

## Complément

-   Tu me demandes de faire un truc en 3 jours là où j'estime qu'il en
    faut 5. Comme tu es mon chef je le fais en 3, mais donc je fais de
    la m\**\**e, ben oui, il faudrait 5 jours pour le faire ! Dommage...
-   Tu me demandes de faire un truc en 5 jours là où j'estime qu'il en
    faut 2. Comme tu me demandes des choses incohérentes par ailleurs,
    et en me basant sur la [loi de
    Parkinson](/2011/04/la-loi-de-parkinson-par-asterix-obelix/), je le
    fais en 5 jours. Dommage...
-   Tu me demandes combien de temps il faudrait pour réaliser ce truc.
    Je te dis 3 jours. Tu me demandes si je me sens capable de le
    réaliser dans les conditions que je décris. Si je dis oui mon
    engagement et la qualité du travail délivré n'auront rien à voir
    avec les cas observés plus haut. La planification et la motivation
    seront aussi grandement améliorés.

## Complément 2

On parle souvent d'un ratio de "productivité" de l'ingénieur avec un
facteur entre 1 et 10. Cela vient du bouquin ["Mythical Man Month" de
Fred Brooks](http://en.wikipedia.org/wiki/The_Mythical_Man-Month). Même
si [certains le remettent en
question](https://medium.com/about-work/6aedba30ecfe) de façon
pertinente. J'y crois. Qu'est ce que cela veut dire ?

-   Pensez à une tâche que vous aimez faire, quelque chose sur lequel
    vous vous régalez : observez votre "performance", "engagement",
    "motivation", ainsi que la "qualité délivrée".
-   Pensez à une tâche que vous n'aimez pas faire, quelque chose sur
    lequel vous vous faîtes c\*\*\*r : observez votre "performance",
    "engagement", motivation", ainsi que la "qualité" délivrée.
-   Repensez au "ratio de productivité" évoqué plus haut. Moi je me dis
    que c'est vrai et que cela concerne tout le monde. Etre productif =
    Aimer faire. Et donc : faire aimer, motiver = rendre productif.

Oui les compétences entrent aussi en jeu. Mais (je ne sais plus d'où
vient cette citation) : "l'enthousiasme fait les compétences". En
(grande) partie en tous cas.

Se reporter au dernier complément.

## Complément 3

Dans les formations agiles, on arrive souvent aux principes du manifeste
agile. Et on lit "faîtes des projets avec des personnes motivées". Là,
la salle s'esclaffe.

Moi, faussement surpris : "Pourquoi riez vous ? "

Eux : "Ah les bisounours ! trop facile ! évidemment que les projets
marchent avec des personnes motivées".

Moi : "Ok, complètement d'accord, on arrête la formation sur XXX, vous
voulez réussir vos projets ? motivez les gens".
