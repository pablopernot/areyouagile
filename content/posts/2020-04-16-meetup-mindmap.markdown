﻿---
date: 2020-04-16
slug: meetup-impact-map-strategy-map-mindmap
title: Meetup Impact Map Strategy map Mind map 
---

Je prends un petit temps ce jeudi pour parler des cartes heuristiques, des *mindmap*, notamment des deux que j'utilise le plus souvent, l'*impact map*, et la *strategy map*. Ce petit article me sert de support pour le meetup qui a lieu ce jeudi 16 à 14h. 

## La vidéo


{{< youtube id="X1DlbPInh1s" >}}

## Pourquoi j'utilise ces outils

* Un des rares outils dont l'utilisation électronique fonctionne bien depuis longtemps. Elle met en oeuvre autant notre sens de la précision avec les mots, que notre vision plus globale, que notre approche plus intuitive avec des formes et des couleurs. 
* Cela donne du sens aux choses. Cela propose une lecture avec du sens, nous faisons cela pour obtenir tel usage, résultat, de façon à atteindre tel objectif.   
* L'objectif est de ne pas faire toutes les branches, mais de mettre en évidence vos hypothèses pour les valider ou les invalider au plus vite. 

## Points de vigilance 

* Ainsi ne pas se précipiter sur les solutions, sortir les acteurs du *brainstorming* des solutions, d'abord s'interroger sur les impacts, le sens, la cible. 
* Bien comprendre qu'il vaut mieux ne pas faire toutes les branches. L'idée est de bien rappeler que l'on fera tout si c'est nécessaire, mais qu'il faut d'abord se focaliser pour avancer, apprendre, avoir des réponses, de l'engagement. D'où l'idée d'utiliser des priorités, des pourcentages (limitation de la capacité, peu importe le détail). 
* Attention à l'effet papillon, votre objectif de départ joue un grand rôle dans les déclinaisons dans la carte, si vous le changez (et vous devez si c'est nécessaire !) cela impact probablement beaucoup de branches.     
* Certains outils électroniques permettent de changer le sens de lecture de la carte : de gauche à droite, pour de droite à gauche (oui on essaye de garder ces cartes horizontales). C'est pratique, car vous allez vous rendre compte que la moitié des gens préfèrent un sens par rapport à l'autre (on fabrique la carte de gauche à droite, mais beaucoup aiment la lire de droite à gauche). 

## À rappeler

* À réaliser tous les 3 mois ?
* Si durant la séance ce n'est pas simple : avancez, vous reviendrez sur les éléments compliqués par la suite avec plus d'information, un regard qui aura changé.  
* Vous pouvez être moqueur des fois, si, pour rappeler la valeur des choses. Que veut-on ? "Finir le produit !" "C'est bon c'est fini alors. Voilà. Produit fini. On passe à autre chose". En montrant clairement que vous ne faites rien. En montrant délibérément que ce n'est pas le statut "fini" qui vous intéresse, que c'est les impacts du statut "fini" qui vous intéressent. "Ah bah non ce que l'on veut c'est que les utilisateurs l'utilisent." "Ah bah faîtes un logiciel de rencontres dans l'entreprise, ou de présentation de recettes". Montrez aussi que "les utilisateurs l'utilisent" n'a pas de sens en tant que tel. Quel est le sens de l'organisation : générer des rencontres ? Stocker des bonnes recettes de cuisine ? Probablement pas. Avec un peu d'humour, il faut amener les gens vers une meilleure expression de l'objectif.  
* Utilisez des questions pour formuler le peuplement de chaque étape : quel impact faut-il avoir sur les acteurs de façon à atteindre tel objectif ? Quelles sont les conditions de succès pour activer le levier ? Comment mettre en oeuvre cette condition du succès ? 


## Impact Map


{{< image src="/images/2020/04/coach-agile-impact-mapping.jpg" title="Impact Map 1" >}}

{{< image src="/images/2017/02/10000inscrits-6.jpg" title="Impact Map 2" >}}


## Strategy Map

ps : agile42 me fatigue avec leur copyright sur des évidences. 

{{< image src="/images/2020/04/agile-strategy-map.jpg" title="Strategy map 0" >}}

{{< image src="/images/2017/02/strategymap-3.jpg" title="Strategy map 1" >}}

{{< image src="/images/2017/02/approche-agile.jpg" title="Strategy map 2" >}}

## On innove

{{< image src="/images/2017/02/doublons.jpg" title="Data map" >}}

## On s'amuse à essayer de réussir notre confinement ? 

*Atelier durant le meetup*. 


## Mes sources

* [Maximum Impact, minimum effort, Gojko Adzic, School of PO 2019](https://www.youtube.com/watch?v=zac2fOlu6p4)
* [Impact mapping workshop Gojko Adzic](https://gojko.net/news/2016/05/09/open-impact-mapping.html)
* [agile strategy map par agile42](https://www.agile42.com/en/all-agile/agile-strategy-map/)
* [agile strategy map par agile42- PDF](https://www.agile42.com/documents/55/agile42_Strategy_Map_w_graphics.pdf)
* [*Impact Map*](/2017/02/cartographie-strategie-impact-mapping/).
* [*Strategy Map*](/2017/02/cartographie-strategie-impact-mapping-hors-sentiers-battus/). 
