---
date: 2013-09-12T00:00:00Z
slug: rentree-2013-pourquoi-jai-mange-mon-pair
title: Rentrée 2013, pourquoi j'ai mangé mon pair ?
---

En octobre j'aurais le plaisir de venir vous parler de la horde agile,
que j'avais commencé à évoquer [ici](/2013/02/la-horde/) et qui a
manifestement bien plus aux *Agile Tour* auxquels j'ai envoyé ma
candidature puisqu'ils m'ont conviés, tous les trois, à vous proposer
cette réflexion personnelle en tant que *keynote* (session plénière
d'ouverture).

Sachez que je suis touché, flatté et stressé à ce sujet.

## Pitch

Qu'est ce qui fait de l'homme, l'homme ? Observons nos ancêtres qui
parcouraient la savane. Nous n'avions ni dents acérées, ni muscles
proéminents, encore moins une rugueuse carapace. Et pourtant nous avons
émergé et finalement dominé le monde. Pourquoi ? parce que nous avions
cette culture de l'adaptation en milieu complexe.

Avec Descartes et les Lumières nous avons un peu oublié cela. Nous avons
une réponse cartésienne à chaque question. Et nous pensons que le monde
marche ainsi. Mais ce n'est plus le cas : interdépendance, connectivité
totale, immédiateté, simultanéité, globalisation, le monde est de
nouveau imprédictible.

Aujourd'hui que peut nous enseigner notre vieille horde ?

## Agile tour

Je vous attends donc avec plaisir pour discuter toute la journée à :

-   [Agile Tour Marseille
    2013](http://at2013.agiletour.org/en/at2013_marseille.html) le 3
    octobre
-   [Agile Tour Toulouse 2013](http://tour.agiletoulouse.fr/#keynotes)
    le 10 octobre
-   [Agile Tour Montpellier 2013](http://agiletour-montpellier.fr/) le
    18 octobre

Je n'ai pas postulé au Nord.

## Scierie à pratiques

J'aurais aussi le plaisir de co-animer avec l'irrésistible **Stéphane
Langlois** deux sessions de [Scierie à
pratiques](/pages/la-scierie-a-pratiques.html) à Marseille & Toulouse
(et peut-être un hack à Montpellier mais chuuutt). Je crois aussi que la
scierie va se débrouiller pour jouer un rôle à Agile Tour Rennes, mais
malheureusement je n'y serai pas.
