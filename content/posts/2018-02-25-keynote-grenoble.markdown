---
date: 2018-02-25
slug: keynote-agile-grenoble
title: Etre agile ? Keynote Agile Grenoble 2017
---

Automne 2017 j'ai eu le plaisir d'être invité à Agile Grenoble, une des conférences les plus célèbres du monde agile français, pour y donner l'une des keynotes. J'ai essayé d'y reprendre, en diesel, mon contenu [être agile](/2017/07/etre-agile/), [être engagé (1)](/2017/09/etre-engage-1/),[être engagé (2)](/2017/10/etre-engage-2/).  

{{< youtube id="pjH0x21pYA0" >}}
