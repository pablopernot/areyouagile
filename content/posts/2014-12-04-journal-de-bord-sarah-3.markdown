---
date: 2014-12-04T00:00:00Z
slug: sarah-journal-de-bord-jour-4
title: Sarah, journal de bord, jour 4
---

Voici la suite du journal de bord de Sarah, ma fille, qui en troisième
effectue son stage d'observation. Sinon [les deux premiers
jours](/2014/12/sarah-journal-de-bord-jours-1-et-2/), et [le troisième
jour](/2014/12/sarah-journal-de-bord-jour-3/).

{{< image src="/images/2014/12/eiffel.jpg" title="La belle de Paris" >}}

## Quatrième jour : une formation

Toutes les personnes qui sont là sont des employés de G. Ils apprennent
ce que c'est que l'agilité. Mon père leur explique la manière de
fonctionner "agile". Il a fait un atelier avec des pièces pour voir
qu'elle est la meilleure méthode de travail. Si c'est petits morceaux
par petits morceaux ou tout d'un coup. Pour gagner du temps la première
méthode est préférable.

{{< image src="/images/2014/12/offing.jpg" title="Offing" >}}

*Attention Sarah va révéler des choses sur des ateliers, ne pas lire si
vous devez les faire.*

Dans l'après-midi j'ai pu participer à un autre atelier. Cet atelier
nous a appris que la communication était plus compréhensible à l'oral
plutôt qu'à l'écrit. Grâce aux informations données par notre partenaire
(oral ou écrit) nous devions reproduire des dessins. Ces dessins sont
plus ressemblant lorsque c'est à l'oral parce que l'on peut se situer et
avoir un feedback. C'est pour cela qu'en "agile" on favorise l'oral.

Un autre atelier en fin d'après-midi : le marshmallow challenge ! Le
tout est de faire une tour avec des spaghetti, du fil, du scotch et un
chamallow (qui doit être au sommet de la tour). Tout ça en 18mn. On
pense que tout va bien durant les dix premières minutes mais à la fin du
temps on se rend compte que le chamallow est lourd, et souvent cela
finit par tomber ! (Les plus forts à ce challenge sont les enfants).

Le but de ce challenge est de voir l'esprit d'équipe, leur organisation
(certains font un plan), et ce que pense les équipes de leur projet
durant le sprint. J'adore ce challenge, je le trouve intéressant et fun
(j'y ai participé plusieurs fois).

Les personnes de la formation ont un rôle de PO dans leur équipe. C'est
à dire qu'ils ont à valider les releases et à prioriser : ils ont a
prioriser ce qu'ils pensent être le plus important pour travailler
dessus en premier.

{{< image src="/images/2014/12/banjo.jpg" title="Banjo" >}}

Ce soir là, après une visite à la tour Eiffel pour la voir s'illuminer,
nous avons mangé un bon hamburger maison au Cantal avec Stéphane, et
nous sommes allés à un jam dans un bar irlandais. Avec Aurélie nous
avons joué aux fléchettes.

Pour répondre à [Géraldine](http://girlzinweb.com/author/geraldine/) :
C'est vrai que j'ai remarqué qu'il n'y avait pas beaucoup de femmes.
Durant la rétrospective de l'équipe de "N", il y avait seulement une
femme. Aujourd'hui il y a plus de femmes. Je ne sais pas si c'est parce
qu'elles ne sont pas intéressés par cette filière ou qu'elles ont du mal
à être embauchées. Moi je ne suis pas très intéressé par cette filière
car l'informatique ce n'est pas mon truc.

{{< image src="/images/2014/12/dart.jpg" title="Dart" >}}

## Les liens

-   [les deux premiers jours](/2014/12/sarah-journal-de-bord-jours-1-et-2/)
-   [le troisième jour](/2014/12/sarah-journal-de-bord-jour-3/)
-   [le quatrième jour](/2014/12/sarah-journal-de-bord-jour-4/)
-   [le dernier jour](/2014/12/sarah-journal-de-bord-jour-5/)
