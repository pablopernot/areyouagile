---
date: 2012-04-04T00:00:00Z
slug: lecture-lempire-des-coachs-de-gori-le-coz-2006
title: 'Lecture : l''empire des coachs de Gori & Le Coz, 2006'
---

Voilà un livre qui a eu un impact sur moi. Il ne s'agit que d'une
critique, violente, exclusivement à charge, contre les coachs.


{{< image src="/images/2012/04/empire_coachs-193x300.jpg" title="Empire des coachs" >}}

"Nous avons accumulé suffisamment d'éléments à charge , nous disent les
auteurs, dans notre dossier sur le coaching pour éviter de tomber dans
le piège de la critique nuancée. Les coachs sont les premiers à avancer
que le coaching comporte des dérives, qu'il faut savoir distinguer entre
les bons et les mauvais coachs. Il n'est pas de pire moyen pour
combattre le coaching que de dire qu'il faut faire acte de vigilance
contre ses mésusages, comme s'il existait une essence pure du coaching
seulement souillée del'extérieur par quelques imposteurs. Nous
préconisons, quant à nous, le rejet en bloc de cette soupe sportive
remixée à la sauce managériale".

## Charge violente

Pour moi, lire ce livre c'est comme si j'avais demandé à un ami proche
connaissant bien mes activités de mettre en exergue tous les aspects
négatifs, les risques ou les dérives de celles-ci. Cet ami proche ne
prend pas de gant, il m'assène des arguments indéniables, il ouvre des
plaies qui auront du mal à se refermer. Nonobstant mon côté sado-maso,
quel plaisir, ou plutôt quelle source d'amélioration ! Voici dressés à
moindre coût bon nombre d'écueils dans lesquels j'ai pu tomber, ou que
j'aurais à éviter. C'est donc une lecture que je recommande à tous ce
qui ont un lien de près ou de loin avec le coaching, en l'occurence dans
mon cas le coaching agile.

Si je résume la pensée des auteurs, le coaching n'est finalement qu'une
sorte de mystification psychologico-managériale dont l'objectif caché
est une uniformisation néolibérale des personnes sous couvert d'une
pseudo émancipation humaniste.Rien que ça.

"Un conditionnement individualiste au service d'un conformisme
généralisé" nous disent les auteurs.

Ou encore (je cite là les titres de chapitres) "Le coaching comme
falsification du rapport éthique à autrui". Et encore "la croissance
individuelle soluble dans la croissance économique". Ou pour finir: "La
question de fond est de savoir si le coaching n'est pas un remède pire
que le mal qu'il prétend conjurer".

Ce qui est très perturbant dans cette lecture c'est que les
explications, analyses, cas concrets mis en exergue par les auteurs me
sont familiers. Comme si ils analysaient une partie de mon travail mais
uniquement sous un aspect négatif. C'est passionnant, je ne peux pas
démentir que leurs analyses soient fausses, mais elles sont
exclusivement négatives.

## Nuançons

Car il faut nuancer. L'un des tords de ce livre est justement qu'il
n'est qu'à charge, à sens unique, qu'il ne place en exergue que les
aspects négatifs. Or même si les auteurs insistent pour ne faire qu'une
critique à charge et surtout de ne pas laisser une once de crédit à ce
rôle, on reste dubitatif devant un tableau aussi noir. Si oui j'ai pu
tomber dans les travers évoqués par moment, et à mon corps défendant,
cela s'accompagnait aussi de pas mal de choses positives (j'ose
espérer). Donc il faut peut-être nuancer malgré tout (relisez la
première citation en début d'article pour voir que je ne suis alors pas
d'accord avec les auteurs).

La deuxième chose sur laquelle apporter de la nuance c'est justement la
définition du coach. Dans l'esprit des auteurs me semble qu'ils
interpellent les "vrais" coachs. Ceux qui sont coachs avant d'être autre
chose. Pour ma part je suis agiliste, ou informaticien, bien avant
d'être coach. Coach n'est que la formulation qui jusqu'avant la lecture
de ce livre me semblait convenir le mieux à cette glue qui s'étend tout
autour du coeur de mon métier : valeurs et principes agiles.

Et puis oui nous sommes là pour aider des organisations à réussir leurs
projets.

Enfin le discours est un peu affaibli par le côté revanchard qui perce
concernant les aspects financiers. L'un des auteurs (Gori),
psychanalyste, ne cesse de fustiger les tarifs exorbitants des coachs
(et rien qu'en lisant les tarifs je sais du coup que je ne suis pas
vraiment un coach). On devine une vraie rancoeur sur cette "psychanalyse
de comptoir à l'américaine" (c'est mon expression pas celle du bouquin)
qui se paye cher et détourne des "clients" de la vraie psychanalyse.
C'est dommage.

## Psychologie & conformisme

L'un des reproches forts du livre est l'annexion du domaine de la
psychologie par les coachs

A mon avis oui il faut aborder dans notre métiers les aspects
psychologiques mais avec beaucoup de précaution. Peut-on s'en passer ?
probablement pas. Il s'agit de relations humaines, donc de psychologie
et pas d'autres sciences plus froides. Mais ce n'est pas mon métier, il
faut donc faire très attention et surtout ne pas devenir un apprenti
sorcier.

Un autre reproche fort est celui de gommer les différences au profit
d'un conformisme pratique aux entreprises. Je suis très en phase avec
cette critique. Il faut accepter la différence, et surtout la préserver.
Halte au conformisme.

## Philosophie

J'ai apprécié les rappels philosophiques (le deuxième auteur est
philosophe -Le Coz-): le relookage de la maïeutique de Socrates par les
coachs. Oui il faut connaitre cela quand on fait de l'agile. Ou les
rappels autour de Descartes, Nietszche ou Heidegger : l'importance du
langage. C'est sur dernier point que je souhaite -comme le
livre-conclure.

## Puissance du langage

Je cite le livre :  "L'homme, écrit Heidegger, se comporte comme s'il
était le créateur et le maître du langage, alors que c'est celui-ci au
contraire qui est le et demeure son souverain". Gori & Le Coz complètent
: "Le langage est la matrice et non l'outil de la pensée". ou plus loin
ils citent Victor Klemperer : "\[...\]Les mots peuvent être comme de
minuscules doses d'arsenic : on les avale sans y prendre garde, ils
semblent ne faire aucun effet, et voilà qu'après quelques temps l'effet
toxique se fait sentir.".

Les mots, le langage construisent notre monde. Ils nous y enferment
aussi potentiellement. Ils peuvent l'empoisonner donc.

Il faut s'interroger sur le mot coaching ou coach et ce qu'il renferme.
Pour ma part j'y vois une inversion des objectifs : Si je suis seulement
coach j'ai perdu le sens de mon action. Le sens de mon action c'est
d'essayer d'appliquer les valeurs et principes agiles. Pourquoi : pour
que les projets réussissent. Et un projet qui réussit se mesure aussi et
naturellement au plaisir et à la satisfaction et à l'émancipation de
toutes les parties prenantes.Toutes.

J'espère être agile avant d'être coach. Et ce livre me pousse à penser
que coach n'est plus le mot adapté. Et d'ailleurs "agile" l'est-il aussi
encore compte tenu de la confusion qui l'accompagne désormais avec
l'engouement dont il est le sujet ?

Bref je vous recommande chaleureusement la lecture de cet ouvrage. Il ne
peut que vous pousser à vous interroger si vous êtes dans ma situation.
Ce n'est donc que bénéfique. Pour ma part je me lance dans la quête des
bons mots, du bon langage.

## Vous reprendez bien encore un peu de lance-flammes ?

"Une analyse critique du phénomène du coaching doit déboucher sur une
attitude d'insubordination radicale. Délivrons nous des coachs ! serait
l'expression naturelle qui nous inspire cette nouvelle potion
idéologique aux senteurs opiacées. La présente contribution appelle à un
sursaut d'orgueil collectif. Elle entend provoquer un vrai débat de
société sur le sujet. Il y a bien des raisons de se montrer sans
concession à l'égard du coaching. A commencer par cette sinistre
anthropolgie managériale qu'il véhicule derrière sa prétention à prendre
appui sur une psychologie humaniste".

Bonne lecture.
