---
date: 2014-09-03
slug: conferences-printemps-2014
title: Conférences printemps 2014
---

Quelques liens sur mes conférences lors du printemps 2014, dans l'ordre
chronologique, le [ScrumDay 2014](http://www.scrumday.fr), [Agile France
2014](http://2014.conference-agile.fr/agile-france-2014-en-mots-et-en-images.html)
et [Sudweb 2014](http://sudweb.fr/2014/). Disons que c'était ma période
pull bleu et slides verts. J'espère redonner certaines sessions,
notamment "les organisations vivantes" (merci
[Fabrice](https://twitter.com/fabriceaimetti) pour le nouveau titre) cet
automne. C'est un peu la suite de [la horde
agile](/2014/01/mini-livre-la-horde-agile/) pour moi, et je me lancer
aussi dans un nouveau petit mini livre.

{{< image src="/images/2014/09/sudweb.jpg" title="Sudweb 2014" >}}

## ScrumDay 2014 : Open Agile Adoption

J'ai pu réaliser une session avec [Oana](https://twitter.com/ojuncu) sur
Open Agile Adoption, suite à notre extraordinaire rencontre avec Dan
Mezick.

Les slides "[open agile adoption](https://speakerdeck.com/pablopernot/open-agile-adoption-scrumday-2014)".

{{< vimeo id="104987420" >}}

## Agile France 2014 : Les organisations vivantes

Le titre a changé, le contenu commence à changer, et j'espère redonner
ces sessions à l'automne. Voici les slides de Agile France 2014 :

{{< speakerdeck id="ce1eff00c5410131946106305ec17342" >}}

## SudWeb 2014 : Épanouissement technique ?

Enfin une intervention rock'n roll à SudWeb 2014, dont j'ai déjà pas mal
parlé : [4 malaises et un très bon
moment](/2014/05/4-malaises-et-un-tres-bon-moment/).

{{< vimeo id="103403865" >}}

