---
date: 2010-03-20T00:00:00Z
slug: les-non-dits
title: Les non-dits
---

Dans le meilleur des mondes il n'y aurait aucun non-dit. Qu'il s'agisse
de la vie en générale, ou des projets informatiques (c'est le sujet ici
!). Aucun non-dit est un idéal, mais cela n'arrive jamais (comme tous
les idéaux). Il y a toujours quelque chose que l'on évite sciemment de
dire à son client, son partenaire, son prestataire, etc. Généralement on
cache une incapacité, une ignorance, une simple incertitude, etc.
J'essaye, et je pense que cela est fructueux, d'éviter au maximum les
non-dits. C'est toujours, toujours, toujours, un risque que vous faites
courir inutilement à votre projet. Cela ne fait que complexifier la
réalisation de celui-ci. Cela introduit un flou dans l'expression ou la
résolution des besoins. Cela empêche de bien organiser le projet, etc. A
partir du moment ou vous jetez une zone d'ombre sur le projet ne soyez
pas surpris d'en subir les conséquences.

{{< image src="/images/2010/03/speaknoevil.jpg" title="Speak No Evil" >}}

Sachant qu'au final souvent personne n'est dupe et que le non-dit a un
prix : Amplification du stress ? Sur une partie que vous ne dominez pas
vraiment mais sur laquelle vous vous êtes vendu ? Amplification du
dommage, si vous laissez trainer un aspect technique ou fonctionnel que
vous savez pertinemment défaillant ? Amplification de la charge ? l'un
des membres de l'équipe n'est pas à la hauteur ? Il faut lui dire. Il
devra faire l'effort de se mettre au niveau ou devra quitter le projet,
mais dans ce deuxième cas, il aura été informé, et la blessure sera
moins forte et plus compréhensible, etc, etc.

N'est-il pas évident qu'un jeune sans expérience répond oui à tout (oh
ces CVs que je vois passer ! Messieurs les professeurs d'IUT, d'école
informatique, ayez la décence d'expliquer à vos étudiants de ne pas
mettre le glossaire de technipedia dans leur CV, ni -au passage- de se
proposer Chef de projet des leur première année professionnelle), alors
q'un vieux singe expérimenté dira plutôt non,  gage de confiance...
(trouvez moi une maxime de Samuraï qui exprime cela en 4 mots).

Ne laissez pas trainer des anomalies, ne laissez pas trainer du code
sale, ne laissez pas trainer des incompréhensions, des quiproquos, ils
vous reviendront à la figure. Je sais que ce n'est pas simple, je suis
le premier à me défausser parfois devant la tâche, mais vous aurez la
surprise de voir se renforcer la relation que vous entretenez avec votre
interlocuteur, de voir s'éclaircir la gestion du projet, de rendre les
choses plus SIMPLES.

On peut rapprocher cela du ***courage*** des méthodes agiles. Rappelez
vous que *Lean* demande de ne pas laissez trainer une anomalie. etc.

Le non-dit n'est pas nécessairement de votre fait. Essayez de persuader
vos clients de ne pas en abuser, de comprendre où ils se cachent.
Devinez les vrais enjeux qui se cachent derrière certaines demandes,
derrière certains blocages, etc.

**Joker**pour les parties commerciales d'avant-ventes : pour ma pomme
ces choses sont beaucoup plus simples depuis que je suis consultant
(avec mes 2 associés chez [SmartView](http://www.smartview.fr)), nous
faisons du conseil : la vérité, la franchise sont donc essentielles et
naturelles, et surtout je m'engage en mon nom, sur mon travail. Je ne
m'engage pas sur une hypothétique équipe qui va réaliser le projet. Mais
je ne peux pas jeter la pierre aux commerciaux des sociétés types SSII.
Si la partie contractuelle n'est pas toujours juste, ni fair-play  : on
va choisir le moins cher sans tenir compte de la qualité, il peut être
compréhensible que la réponse ne soient pas toujours vraie. On comprend
bien que de trop nombreux projets sont viciés dès la phase de vente.
D'où à mon avis le succès des méthodes agiles aujourd'hui.

Je ne vous demande pas non plus de clamer partout et à haute voix tout
ce qui se déroule sur le projet.  Je fais appel à votre intelligence pour bien comprendre où se situe la
frontière.

Les non-dits dans les projets informatiques, c'est comme les secrets de
familles, à la fin ça brise les gens, les dynamiques, les équipes, les
projets.

