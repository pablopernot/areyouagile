---
date: 2022-09-16
slug: organisation-vertueuse-ou-calamiteuse
title: "Organisation vertueuse ou calamiteuse ?"
draft: false
---


C'est depuis un weekend paisible dans la campagne, pas très loin du Mans, au [domaine de la Richardière](https://domainelarichardiere.com/)[^1], que j'écris. Dans un cercle de connaissances, un débat a émergé. Celui de l'état calamiteux, ou pas, de certaines grandes administrations françaises, ou certaines grandes organisations françaises. S'en est suivi une série de dialogues qui opposent privé et public : à cela je ne crois pas. On va trouver de très belles organisations et des organisations désastreuses des deux côtés. Mais il est certain que la taille et l'histoire ont un poids dans ces dérives. 

[^1]: Petite publicité pour ce domaine paisible et artistique. 

Qu'est-ce que seraient une organisation "calamiteuse", et une organisation "vertueuse" à mes yeux. 

Pour me faire un avis, j'observe trois éléments : 

## Politique ? 

L'organisation est-elle devenue "politique", au mauvais sens du terme. Politique dans le sens du modèle de Mintzberg, (se reporter à [Meetup entreprises complexes](/2020/04/meetup-entreprises-complexes-2/)), c'est-à-dire où les questions, les conversations, les objectifs sont orientés vers l'interne : est-ce que je vais accéder à l'échelon supérieur, est-ce que mon projet va être bien vu par mes pairs ou mes supérieurs, ou mes collaborateurs, est-ce que cela aura un impact sur mon positionnement, ou le notre, dans l'organisation, etc. C'est pour moi le signe d'une organisation calamiteuse. C'est une organisation tournée vers l'interne, sur elle-même. 

Dans une organisation vertueuse, les conversations sont orientées vers le but de l'organisation. Par exemple: comment pourrions-nous faire la prochaine fois pour mieux servir XXX, qu'attendent nos utilisateurs, citoyens, agents concernant le déploiement de XXX, qu'est-ce que nous pourrions faire pour rattraper notre retard sur le sujet XXX, etc. Ce regard porté sur ce pour quoi l'organisation existe, ce regard porté vers l'extérieur, est le signe d'une organisation vertueuse (non pas qu'elle ne doit pas s'occuper de ses collaborateurs, mais c'est un autre sujet, et cela se présente différemment).  

Il est facile de faire un diagnostic : écoutez de quoi sont faites les conversations. 

## Distribution de la décision, autonomie, responsabilisation ?

Toutes les décisions sont centralisées ou les décisions se déclinent au bon niveau à chaque endroit dans l'organisation. Si personne n'est maître de comment il applique un objectif, si personne ne décide de comment il met en oeuvre l'atteinte d'une cible, mais que les décisions sont centralisées et les instructions arrivent comme des manuels IKEA où il faut appliquer, suivre, sans réfléchir, c'est pour moi le signe d'une organisation calamiteuse. 

Si un espace de prise de décision sur le comment est laissé à chaque niveau de l'organisation concernant son niveau, c'est le signe d'une organisation vertueuse. 

Cette liberté dans la mise en oeuvre, cette autonomie, engage les personnes, et peut faire émerger de nouvelles idées riches. Attention autonomie ne veut pas dire autarcie, et autonomie ne veut pas dire que vous n'êtes pas responsable, que vous ne devez pas rendre des comptes. 

Il est facile de faire un diagnostic, il suffit de lister toutes les décisions prises dans l'entreprise et voir si elles sont centralisées ou déclinées au bon niveau dans l'organisation. 

Un bémol cependant sur les organisations mécanistes : où c'est la répétition d'un geste à l'identique qui amène de la valeur (Les fastfoods, les chaines, etc.). Ici il s'agit de répéter sans réfléchir. Pour ma défense elles sont de moins en moins nombreuses (la complexité du monde aidant), et j'ai peu d'appétence pour elles.  

## Compréhension de la "systémie" ? 

Plus difficile à diagnostiquer, plus difficile à saisir. J'essaye d'observer dans les conversations si les optimisations sont locales et ne se concentrent que sur le local, ou si on pense que tout est lié, que l'on prend du recul et que l'on regarde le tout. 

Est-ce que vous jugez de votre efficacité sur un plan local : ce processus marche bien, est optimisé ; ou sur toute la chaine globale : nous délivrons le service attendu, la valeur attendue, en combinant tous ces processus, toutes ces articulations. Et en comprenant que certains processus ne doivent pas être optimisés pour eux-mêmes, mais pour le tout. Par exemple : ce service n'a jamais marché aussi bien, mais par contre tous les autres autour subissent cette performance à leurs dépens. 

Ce n'est pas facile à diagnostiquer. Des fois c'est les silos d'expertises qui empêchent de voir le tout, mais ce n'est pas garanti (d'où cependant la mouvance d'organisations structurées par expertise vers les organisations structurées pour avoir des impacts).  


## Écoresponsabilité

Un quatrième élément fait aujourd'hui irruption dans mon jugement entre calamiteuse ou vertueuse : celui de l'écoresponsabilité. Là non plus ce n'est pas si facile que cela à juger (dans certains cas). Entre ne pas réaliser, ne pas se sentir concerné, être conscient, essayer, réussir, faire semblant, limiter les dégâts, réparer, faire grandir, il y a de nombreux cas de figure. 

Pour le diagnostiquer peut-être faut-il commencer par établir une triple comptabilité : financière, humaine (sociale), et écoresponsable (nature) ([voir l'article précédent](/2022/09/produit-de-demain/)). 


### Pour conclure 

Êtes-vous une organisation politique ? Où les décisions sont prises au bon niveau ? Où chacun optimise son coin de terre ? Ou êtes-vous une organisation orientée vers sa raison d'être ? Où les décisions sont déclinées au bon niveau avec autonomie et responsabilisation ? Où l'on comprend que tout est lié dans son action ? 






