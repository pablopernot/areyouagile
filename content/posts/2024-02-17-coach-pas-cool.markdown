---
date: 2024-02-17
slug: coach-pas-cool 
title: "Coach pas cool"
draft: false
---

Le nombre de gens qui deviennent coach perso et qui ne devraient pas me parait assez impressionnant. 

Il n'y a pas une semaine sans qu'une ou deux connaissances proches ou lointaines, ami(e)s, copain/ines, n'affiche sur son profil de réseau : *Je suis devenu coach pro (ou perso) !*, *Je veux devenir coach pro (ou perso) !* Misère. 

Misère à mes yeux, et selon mon interprétation. 

Toute ressemblance avec des faits et des personnages existants ou ayant existé n'est absolument pas fortuite et n'est pas le fruit d’une pure coïncidence.

Misère ? Car j'observe dans la grande grande majorité des cas des personnes que je connais de mon entourage que le but premier de leur coaching en développement personnel est d'abord une thérapie pour eux ou elles. Ça, c'est bien, cela serait peut-être encore mieux si c'était un peu plus conscient, peut-être, je ne sais pas. 

Misère alors pourquoi ? Parce qu'ils viennent nourrir la manne des vendeurs de pelles de la certification du coaching qui leur disent qu'ils sont bons et géniaux, certain(e)s rares le sont. Or, dans la grande majorité, ils ne le sont pas concernant le coaching en développement personnel. Et on se retrouve avec une palanquée de personnes dangereuses sur un marché ultra-saturé. Ah j'ai peut-être besoin de préciser : je ne suis pas coach en développement personnel, je ne compte pas l'être. 


{{< image src="/images/2024/02/batman-slapping-robin.jpg" class="center" alt="CNV ET TOLTÈQUE DANS TA GUEULE" width="600" >}} 

Misère pourquoi ? Parce qu'ils vont probablement être malheureux/euses en plus de n'être pas forcément compétents. Le marché est ultra saturé, et ce n'est pas ce job qui rapporte le plus ou assez. 

Et misère surtout, car ils se détournent de leurs vraies compétences, de leurs vraies expertises. Ils sont ingénieurs, coachs organisationnels, ou consultants, ou autre. Et j'en vois des bons/nes qui se détournent de cela. (Je ne vous parle pas ici de tous ces dirigeant(e)s qui sont certifié(e)s coach en développement personnel parce que cela en fait de *meilleurs leaders*, c'est à pouffer de rire. Note : c'est vrai s’ils prenaient l'enseignement au sérieux, pas comme un macaron obligatoire dans leurs CVs). 

Cette file sans fin de candidat(e)s à ce titre est due probablement à la dureté du métier et de nos environnements en entreprise actuellement. Quel sens effectivement dans le monde quand on voit Total faire autant de bénéfices et l'opposant russe se faire assassiner en prison ?  Quel sens quand on voit les guerres de par le monde ? Quel sens quand on voit si peu de grandes entreprises se comporter intelligemment dans l'amélioration de leurs pratiques et de leurs produits ? Quel sens quand on observe le retard, pire le retour arrière de nos organisations qui freinent si fort devant l'obstacle qui arrive : le dépassement des limites planétaires et la grande transformation à venir qui en découle ? 

Alors oui plutôt que se prendre les murs de cette absurdité, on préfère devenir coach en développement personnel, dans un petit coworking à siroter un jus de gingembre en mangeant des petits gâteaux et à discuter avec une personne de ses soucis. Une personne en demande, et juste une. Tout est plus facile. En tous cas tout est plus facile au début, ou plus facile comme cadre de travail. Après que cela soit plus facile de véritablement avoir un impact certainement pas, et vous êtes peu à l'avoir réellement. 

Alors d'accord continuez à devenir coach en développement personnel, mais assumez que cela commence par une volonté de se soigner soi-même (c'est très légitime), que c'est une posture haute facile (je te coach ô humain) et travaillez-là, rappelez-vous que vous n'êtes pas forcément doué pour cela (votre certif vous l'avez achetée) et que vous oubliez en quoi vous êtes doués peut-être. Rappelez-vous que vous faites peut-être du mal. Rappelez-vous que vous pouvez être dangereux (n'oubliez pas de prendre une assurance). Et si besoin, arrêtez de l'être. Ayez cette hauteur de vue, cette force. 

On a flingué bien des beaux métiers à les saturer de charlatans ou d'incompétents. 

Ce que j'aimerais ? Que cette aliénation qui vous pousse tant dans les chakras au patchouli du coaching en développement personnel, on lutte contre. Qu'on ne lutte pas contre le coaching en développement personnel, il y en a de très bien. Qu'on lutte contre la source de votre aliénation : ce délitement de la société, des rapports humains, de la conscience citoyenne, etc. Et que cela démarre par ne pas abandonner là où vous avez de la valeur pour vous enfuir dans cette mascarade. Que vous renforciez votre discipline et votre volonté et que chacun nous fasse avancer. 

