---
date: 2015-07-05T00:00:00Z
slug: sur-la-piste-des-organisations-vivantes
title: Sur la piste des organisations vivantes
---

Voilà bientôt un an, voire plus, que je tourne autour de ce sujet, sans
réussir à le mettre en forme réellement. Je vais essayer encore une fois
cet été durant mes vacances, j'aime me prélasser, lire, et écrire. Cela
sera l'occasion. Les organisations vivantes c'est l'idée que à l'instar
de l'univers dont 65% de la matière nous échappe, on l'appelle ["matière
noire"](https://fr.wikipedia.org/wiki/Mati%C3%A8re_noire), une grande
partie des organisations nous échappent quand nous les réduisons à nos
organigrammes, à nos diagrammes de processus, à nos hiérarchies et nos
feuilles de chiffres. Voici dans les grandes lignes ce dont j'espère
parler, et je compte sur vous au travers de ce post pour me faire vos
retours avant qu'il ne soit trop tard.

L'objectif des organisations vivantes est de donner d'autres outils aux
organisations d'aujourd'hui et à ceux qui les gouvernent ou y
travaillent, de rendre visible et palpable cette matière noire pourtant
essentielle à leur compréhension et à leur dynamisme.

## Pistes pour les organisations vivantes

Quand on me demande de penser une organisation, d'accompagner une
transformation (agile ou pas), à quoi fais-je référence ? En voici le
**brouillon**, j'insiste je vous délivre un peu ce qui sort en vrac en
ce moment et que je dois organiser (!). En cliquant sur l'image vous
obtiendrez une grosse image de 5mo pour mieux y voir.

{{< image src="/images/2015/07/consolidation.jpg" title="Consolidation pistes les organisations vivantes" >}}

Je vais essayer d'aborder les organisations sous trois axes :
**structure**, **valeur**, **changement**. La structure est composée de
deux thèmes : les composants, ce qui compose l'organisation, et la
structure qui assemble ces composants. La valeur de l'organisation
évoque ses richesses : la connaissance, la culture, les produits,etc.
Enfin la conduite du changement, comment se déroule les dynamiques de
changement en son sein.

## Structure

La structure se base sur des composants. Les composants des organisations c'est nous. Comme j'ai pu l'évoqué dans [la horde
agile](/2014/01/mini-livre-la-horde-agile/) nous souffrons de limitations et d'avantages. Parmi ceux-ci pour penser les organisations : les tailles des groupes (plaisir d'appartenance, capacité de communication constructive), les distances entre les personnes pour la communication, les questions de communautarisme, l'importance de la socialisation, le management visuel en contrepoint de l'ambiguïté des mots, mémorisation au travers du *storytelling* (neurones miroir ?), etc.

La structure elle-même prend des formes : sont-elles soumises à la théorie des catastrophes ? Si oui selon les contextes elles se
**déchirent**, se **recousent**, se **plient**, l'utilisation du **verbe** pour gérer les dynamiques de la structure me paraît essentiel. Qui utilise des verbes pour indiquer son organisme en marche ? Mais aussi l'inclusion des idées de la thermodynamiques et du vivant qui hérite de l'approche systémique (par exemple le Macroscope de De Rosnay, ou certains ouvrages de Laborit) dans la vision de l'organisation moderne. Le double lien sociocratique (que l'on nomme aujourd'hui holocratique) pour gérer les groupes de travail.

En contrepoint de cette approche très organique, l'idée que dans la nature le cristal a réussi à optimiser au maximum la transmission d'information (on l'utilise du coup pour transmettre l'électricité dans nos processeurs) en répliquant à l'identique le même motif. Une répétition, une redondance qui fait du cristal le cristal. Trouver donc un milieu entre la facilitation de la transmission de l'information, et la capacité organique adaptative des systèmes complexes.

## Valeur

A l'instar des termitières dont l'objectif est d'étendre la capacité de
stockage du corps des termites elles-mêmes, (voir *the extended
organism* de Scott Turner), nos organisations stockent, sauvegardent,
maintiennent, de la richesse, de la valeur. Les plus évidentes sont les
produits qu'elles créent, les connaissances qu'elles génèrent, la
culture de l'organisation, etc.

Aujourd'hui n'importe quel gouvernant comprendra l'importance de la
création et du maintient en bon état de la **culture** de son
organisation, de la **connaissance** de celle-ci.

Les outils de l'agilité sont un bon exemple pour permettre la création
de cette valeur (notamment produit et solution), *Scrum, Kanban, Lean
Startup*, un rapide tour d'horizon sera indispensable.

Mais comment stocker et maintenir en bon état culture et connaissance ?
(la propagation de celle-ci appartient au chapitre sur la conduite du
changement). Il y a d'abord probablement les outils traditionnels (ECM,
GED, et consorts). Et puis il y a les propositions non traditionnelles,
en voici deux :

-   Faire des **séances** de *storytelling* sur l'**histoire et les
    anecdotes de l'organisation** : comme les [brown
    bag](https://en.wikipedia.org/wiki/Brown-bag_seminar) ou autre
    séances de [coding dojo](https://fr.wikipedia.org/wiki/Coding_dojo)
    sauf que le sujet n'est pas métiers, ni technologiques, il doit être
    relatif à l'histoire de l'organisation, sa culture, ses anecdotes,
    son **authenticité**.
-   Avoir une **lecture régulière en groupe** : je suis né à Cuba, non
    pas par hasard, mes parents y ont vécu et travaillé deux ans, ils en
    ont profité pour m'y faire naître. Trop jeune pour le voir, j'ai
    cependant gardé en tête les histoires de lecteurs dans les fabriques
    de cigares. Tout le monde travaille, un lecteur raconte des
    histoires : l'histoire, la politique, de grands textes. J'aime cette
    idée de lectures, dans un environnement qui nous conviennent (les
    gens que je croise ont généralement besoin de calme et d'une sorte
    de bulle pour avancer dans le chaos ambiant).

Et puis il faut regarder ailleurs, pour améliorer les
[bidonvilles](/2014/10/des-bidonvilles/) on évoque la formation
continue, l'injection de *sang neuf* à intervalles réguliers, diversité
et intelligence collective, prise de décision décentralisée, etc.

## Changement

Les améliorations des [bidonvilles](/2014/10/des-bidonvilles/) nous
fournissent des pistes pour le changement dans nos organisations.
L'accompagnement au changement est clef. Les bonnes intentions ne
résistent pas à une mauvaise approche dans ce domaine. L'accompagnement
est pensé étape par étape, le petit fait le grand, par niveau de
maturité ([Chemin d'une transformation agile (juin
2015)](/2015/06/chemin-dune-transformation-agile/)). Le changement
inclus les acteurs de celui-ci, il ne s'agit pas d'être coercitif pour
aller vers les organisations que nous décrivons, les technologies de
type [Open (Agile) Adoption](/2014/04/histoires-dopen-agile-adoption/)
sont indispensables.

La présence et l'affirmation de l'**émotion** sur l'**analytique** est
aussi clef. La transformation de *Unilever* au travers d'événements
artistiques émotionnels est évocateur (*To the desert and back,
Mirvis*).

Le changement se nourrit de socialisation et de *storytelling* (encore),
les *summit* : rencontres globales historiquement du monde *open source*
ont à y jouer un rôle.

## Votre retour

Votre retour sur ce brouillon m'intéresse beaucoup. Ainsi donc pendant
mes vacances en Crête (là où ce joue un important moment de l'histoire
de notre pensée, un vrai choix de civilisation, encore à nouveau dans ce
berceau de la démocratie ) j'aurais plaisir à vous lire en essayant de
rédiger enfin ce nouveau petit document.

Petit historique des articles sur ce thème
------------------------------------------

-   [Agile à grande échelle c'est clair comme du cristal
    (novembre 2013)](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)
-   [Penser son organisation
    (janvier 2014)](/2014/01/penser-son-organisation-introduction/)
-   [Penser son organisation et la théorie des catastrophes
    (janvier 2014)](/2014/01/penser-son-organisation-theorie-des-catastrophes/)
-   [Penser son organisation sur le terrain
    (février 2014)](/2014/02/penser-son-organisation-sur-le-terrain/s)
-   [Penser son organisation : les bidonvilles
    (octobre 2014)](/2014/10/des-bidonvilles/)
-   [Le changement en organisation par les émotions
    (avril 2015)](/2015/04/emotion-energie-et-conduite-du-changement/)
-   [Qu'est ce qu'une transformation agile réussie ?
    (mai 2015)](/2015/05/quest-ce-quune-transformation-agile-reussie/)
-   [Chemin d'une transformation agile
    (juin 2015)](/2015/06/chemin-dune-transformation-agile/)
-   [Sur la piste des organisations vivantes
    (juillet 2015)](/2015/07/sur-la-piste-des-organisations-vivantes/)

