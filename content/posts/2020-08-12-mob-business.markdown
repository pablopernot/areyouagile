---
date: 2020-08-12
slug: mob-business-development
title: "Mob Business Development"
---

"Mob Business Development" c'est quoi ? C'est une façon de faire du *business*, du commerce, en équipe, ensemble, qui s'est révélée passionnante à vivre et dont je souhaite vous faire part ici. Quitte à ce que la concurrence s'en empare ? "life is great" disait *tribal leadership*. Après une pandémie, et Ubu roi d'une superpuissance, je me sens assez léger pour partager ces victoires, ces apprentissages, à qui que ce soit, et comme j'ai observé que cela faisait grandir pas mal de monde, le monde justement n'en serait que meilleur.

Je travaille dans une agence, un cabinet, une esn, une ssii, une startup, un peu tout ça, en tous cas une entreprise qui fait du service et qui loue les services de ses experts, de ses sachants, de ses coachs. Un des points critiques pour la bonne marche de notre entreprise c'est d'éviter trop d'intercontrats. C'est-à-dire de personnes sans projet. Une réussite autant humaine que économique (l'un nourrit l'autre et vice-versa). (Le sujet est de vous expliquer comment je pense avoir mis en oeuvre un procédé qui fait grandir tout le monde autour de cette problématique et pas de savoir le bien ou le mal de ce type de posture, votre philosophie de vie ou entrepreneuriale. Je trouve globalement tous ces débats décevants, car binaires, je ne me lance pas dedans ici).

### Des débuts cahin-caha

Ça commence par l'équipe [conseil, management, coaching](https://coaching.benextcompany.com). Cette équipe prône l'autonomie, la responsabilisation. C'est les piliers de leurs propos. Avec l'aide de **Pauline Egea** (je pense que cette dynamique de "Mob Business Development", qui est le nom pudique, nous l'appelons "commando biz" aux risques d'affoler les pacifistes, bref, cette dynamique est le fruit de notre duo, merci [Pauline](https://www.linkedin.com/in/pauline-egea-b9786813/)). Ainsi nous les, nous nous, prenons au mot. Soyons autonomes. Réalisons le commerce en sus de tout le reste. Et le commerce, c'est d'abord des prises de contact, du réseautage. Et des prises de contact, c'est d'abord des coups de fil. Et bon sang donner un coup de fil intéressé à un ou une inconnu(e) ce n'est pas simple pour tout le monde et c'est très compréhensible. Nonobstant nous nous lançons. 

Et ça commence très mal. Ça commence avec les bougons. Ça freine dans tous les sens. Ça avance à hue et à dia. Mais j'entraperçois quelque chose. Je sens quelque chose. L'idée pour résumer c'est de se mettre à quatre ou cinq autour d'une table (virtuelle d'une visioconférence) et donner un après l'autre (et tout le monde écoute) des coups de fil pour prendre des contacts et faire avancer le schlimblick. Cette première fois il a fallu pousser le groupe comme si on était embourbé dans des gadoues gigantesques. Mais comme je vous le disais, cette reculade de la plupart indiquait à mes yeux qu'il se passait quelque chose d'important. Je ne pense pas qu'avoir une idée soit une force, tout le monde a des idées. Ce que j'essaye de faire c'est de comprendre parmi toutes les idées lesquelles sont de vraies avancées, et quelles sont les deux ou trois clefs qui la constituent. Et ici je le sentais. 

### Petite libération

La seconde tentative est libératrice. Le groupe est différent et motivé. Tout le monde se prend au jeu. Et tout se met en place : 

* La bienveillance du groupe et la sécurité psychologique de l'exercice (tout le monde sait que ce n'est pas facile de donner un coup de fil, et tout le monde va y passer, donc...). 
* La tension, celle de devoir appeler, observé par tous les autres, génère une émotion qui scelle le groupe. 
* L'apprentissage : chacun écoute avec beaucoup d'humilité l'autre.                
* L'efficacité de la démarche : c'est directement le sachant qui prend contact avec vous et pas un intermédiaire. Ce sachant il prend contact à sa façon avec ses mots. Il est acteur de son avenir. 

### Extension 

Moi, ce qui m'intéresse en tant que dirigeant d'entreprise, c'est que tout le monde se comporte de façon intelligente. Pour cela j'essaye de leur donner le maximum d'informations, de motifs d'apprentissage, de responsabilité. Jusque là le commerce est la zone des commerçants (les *business developers*) et le savoir-faire est la zone des sachants (les *business developers* sont les sachants du commerce pas de la finalité de la valeur apportée aux clients). Après les premières réussites de ce "commando biz", de ce "Mob Business Development", qui confirme mes soupçons de bonne idée, je l'étends à des équipes qui se revendiquent moins autonomes, ou qui justement ne veulent pas nécessairement faire cela (tout le monde étant tranquille dans cette dichotomie d'un côté le commerce, de l'autre le savoir-faire). 

Pour mettre le pied à l'étriller deux arguments : le premier, personne n'est obligé de donner un coup de fil (celui-ci je le regrette, car je pense que la situation rend le choix difficile, mais je le rappelle tout le monde est épaulé, personne n'est moqué, tout le monde comprend la situation ). Donc le premier, personne n'est obligé d'appeler (ce qui est vrai, car cela s'est produit, notamment avec les bougons rappelez-vous). Le second : "si tu maîtrises une capacité à solliciter et à créer ton réseau tu deviens complètement autonome pour ton avenir, si par exemple un jour tu deviens indépendant" (là aussi le débat n'est pas entreprise versus indépendant, je lis de vastes conneries binaires sur le sujet je n'ai pas envie d'ajouter les miennes). En fait nous les rendons autonomes, nous réalisons notre promesse (*be your potential*), et cet argument marque beaucoup de points. C'est donc parti avec une population très différente : autour de la table virtuelle deux ou trois *techs*, codeurs, développeurs, et deux commerciaux alias *biz dev* (et moi). 

Avertissement :  j'ai été complètement bluffé. 

### Révélations

J'ai vu des révélations de personnes. J'ai vu des gens grandir en quelques points "commando biz". J'ai vu que d'un coup les techs comprenaient bien mieux les biz qu'avant, et vice-versa (jusqu'au moment où l'on demande au *bizdev* de penser à se plonger dans le code et de commencer à préparer son dossier de compétence : les regards en disaient long sur le voile que l'on soulevait, et l'apprentissage dans l'inversion des rôles). J'ai eu la confirmation qu'un potentiel client qui reçoit un appel d'un sachant et pas d'un intermédiaire a une écoute bien meilleure et cela est compréhensible. J'ai eu la confirmation que tous les bafouillements du monde avec authenticité étaient mieux qu'un discours factice qui déroule (ce n'est pas ce que font nos *biz dev*, nous nous interrogeons régulièrement sur notre authenticité, #nobullshit). 

Le plus important, j'ai aussi eu des gens en pertes de confiance, parce qu'en intercontrats, qui redevenaient maîtres de leur avenir : [il pouvait contrôler leur sort, ils connaissaient les règles, ils avaient du feedback, et nous les invitions à participer](https://www.youtube.com/watch?v=pjH0x21pYA0).

J'ai vraiment vu des gens ravis, les yeux pétillaient tout autour de la table. Tout le monde se regarde différemment ensuite. 

Ceci n'est possible que si l'entreprise a un vrai discours responsabilisant, où une véritable conversation peut avoir lieu, où tout le monde se respecte.     
C'est possible chez benext et j'en suis fier. 

### Rappel du mode opératoire

* 1h tous les deux ou trois jours. 
* Petit groupe de 4 à 6 personnes maximum.
* Nous avons introduit une option : nous lançons une sélection aléatoire parmi les personnes suivantes et ensuite, quand la personne a été sélectionnée, nous cherchons qui elle pourrait bien appeler (avec du sens). La sélection est faite grâce à [https://www.dcode.fr/random-selection](https://www.dcode.fr/random-selection). Merci les jeux de rôles. 
* Tout le monde, quelque soit son profil, va donner un coup de fil (oui je sais on n'est pas obligé, je veux dire : tout le monde autour de la table est concerné, pas certains écoutent et d'autres font). Naturellement une règle naturelle est apparue : quand on n'a pas envie que la personne réponde, elle répond, et quand on en a envie elle ne répond pas (et on prend contact autrement, message, mail, etc.), c'est rigolo.  
* On discute un peu avant le coup de fil pour se dire ce qui va être dit, et pourquoi, et comment, on s'entre-aide, on se dit ce qui pourrait faire sens. Chacun est différent et aucun appel ne ressemblera à un autre, c'est toujours un moment et une combinaison unique. 
* On se félicite après le coup de fil, car c'est toujours une tension.  

Un vrai moment de construction de groupe et d'émancipation de chaque individu. 

Merci beaucoup donc à **Pauline Egea**. Et aussi à **Nils, Yannick, Benjamin, Florent, Jean-Christophe, Mike**, qui ont très bien joué le jeu. Et à **Quentin et Elise** qui ont aussi vite compris l'intérêt de cette démarche. 

### Feedback depuis notre forum de conversations 

Quelques *feedback* des intéressés glanés cette semaine suite à l'article. 

{{< image src="/images/2020/08/mbd-1.jpg" title="" class="center" >}}


{{< image src="/images/2020/08/mbd-2.jpg" title="" class="center" >}}


{{< image src="/images/2020/08/mbd-3.jpg" title="" class="center" >}}