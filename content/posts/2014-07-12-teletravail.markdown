---
date: 2014-07-12T00:00:00Z
slug: teletravail
title: Télétravail
---

Une question importante, le télétravail, et que l'on pose souvent, ou
que je me pose souvent à moi-même. Voici les réponses courtes que
j'amène, et que je vous livre pour rebondir sur vos retours.
(Personnellement je n'ai pas encore trouvé la clef dans mon activité
pour basculer en télétravail, un de mes seuls regrets).

## Faut-il du télétravail dans nos organisations modernes ?

Est-ce que la question se pose vraiment ? non, probablement pas. Prenons
comme postulat que l'on fait avec le télétravail, que la question ne se
pose pas vraiment, que pour une multitude de raisons (économiques,
écologiques, etc.) c'est l'avenir. Ce dont je suis convaincu.

## Pallier les inconvénients ?

Naturellement on perçoit rapidement certains désavantages liés à la
dynamique de groupe et à la communication. Mais ils ne sont pas si
lourds que cela et peuvent être contournés. Comment ? Je suggère :

-   Un "summit" au lancement du projet : une phase où tout le monde est
    ensemble, pendant plusieurs jours.
-   Présence aux évènements clefs (à vous de juger ce qu'est un
    évènement clef).
-   Une limite du télétravail à 60%, c'est à dire 3 jours sur 5 de la
    semaine ; et cette limite c'est juste probablement mes anciennes
    croyances qui se débattent encore un peu, donc vous pouvez oublier
    ce point...

Il me parait très important :

-   Un même niveau de communication entre tous les acteurs : qu'ils
    soient sur site ou à distance : tout le monde prend part au
    *hangout* ou *skype* ou genre : pas une *pieuvre* (de *confcall*)
    avec trois personnes autour et deux au téléphone. Tout le monde au
    même niveau de communication, c'est important. On cherche à avoir
    des équipes "distribuées" (même niveau de communication, de
    confiance, de partage) et non pas "dispersées" (inégalité dans la
    communication, la confiance, le partage).
-   Vidéo conférence plutôt que audio conférence afin d'éviter les
    conversations dans le vide et avec le micro en mute... On se parle,
    on se regarde, 70% de la communication n'est pas orale.
-   Donc un très bon réseau physique (je parle de bande passante, de
    matériel de conférence). Oui c'est un investissement nécessaire.
-   Et une bonne organisation "à la maison" ou en "co-working".

Voilà les éléments clefs.

## Performances

Mais oui il faut aller vers le télétravail : la performance sera à la
hauteur de la motivation et du plaisir de la personne, et le télétravail
va l'aider dans ce sens si elle le réclame.

Et contrairement aux croyances la personne qui télétravaille n'en fait
pas moins. Bien au contraire. Je pense même que le télétravail va
obliger une réflexion sur soi-même, sur ses aspirations, une
clarification de ses attentes et de ses valeurs, et ainsi provoquer un
encore meilleur engagement ainsi qu'une pensée plus globale, systémique.

Aujourd'hui pour les employeurs et entrepreneurs modernes c'est aussi
une vraie façon de capter les talents.
