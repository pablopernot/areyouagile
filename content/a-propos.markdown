---
type: section
layout: apropos
title: À Propos
url: /a-propos/
sidebar: true
alchemy: 6
---

{{< image src="/images/pablo.jpg" title="pablo pernot" width="400" >}}

## Me joindre ?

* Avec un courrier : <pablo@projetwinston.fr>.  
* Par mon compte [linkedin](https://www.linkedin.com/in/pablopernot/).
* Vous pouvez aussi me joindre via le [Projet Winston](https://projetwinston.fr).

## Licence sur le contenu

<https://creativecommons.org/licenses/by-nd/4.0/>

- Vous pouvez partager : copier et redistribuer le matériel sur n'importe quel support ou dans n'importe quel format, à n'importe quelle fin, même commerciale.
- Vous ne pouvez pas modifier et partager : si vous remixez, transformez ou développez le matériel, vous ne pouvez pas distribuer le matériel modifié.
- Vous devez attribuer : vous devez donner le crédit approprié, fournir un lien vers la licence et indiquer si des modifications ont été apportées. Vous pouvez le faire de manière raisonnable, mais pas d'une manière qui suggérerait que le donneur de licence vous approuve ou approuve votre utilisation.

Photo portrait : [Lucile Grémion](https://www.instagram.com/lucile_g_/?hl=fr)

## Newsletter

Pourquoi une newsletter en plus de mon blog ? Le blog est mon outil pour poser de façon institutionnelle mes réflexions. La newsletter est plus éphémère, elle me permet d’évoquer des choses du moments, des événements que j’organise : meetup, séance d’intervision, table ronde sur un sujet, conférence ; elle me permet de donner une tonalité plus dans l’actualité, ce que j’entends du métier, du marché, des outils, des démarches que je découvre, etc. C’est autant pour les gens qui font mon métier, que pour ceux qui font appel à moi.

[S'INSCRIRE A LA NEWSLETTER](http://eepurl.com/iZPT62)

## À mon sujet

Après une maîtrise sur les Monty Python, un DEA sur le non-sens et l’absurde, l’entame d’un doctorat sur les comiques cinématographiques, il paraît normal que je me sois lancé dans le management et la conduite du changement, c’est -finalement- une suite logique. Depuis quinze ans, je suis considéré comme un acteur expérimenté en développement des organisations (*holacratie, sociocratie, agile, lean, kanban*). Je suis avant tout un entrepreneur. En 2010, après du conseil dans l'IT, je suis devenu dirigeant en fondant avec deux amis la société de conseil Smartview. Puis j'ai rejoint comme associé/DG Benext en 2015. Benext est vendu à Accenture/Octo en 2021. Je participe à son intégration un temps puis je reprends ma liberté.

326.4 ppm  

## Petite histoire professionnelle

- en 2024 je lance le **[Projet WINSTON](https://projetwinston.fr)**
- en 2021 **ACCENTURE/OCTO** rachète benext et je les rejoins en tant que *Managing Director*.
- En 2015 je rejoins **BENEXT**, en tant que directeur général à Paris.
- En 2010 je crée **SMARTVIEW** (j'apporte l'agilité) avec Christophe Monnier et Gilles Pommier à Montpellier.
- En 2006 je rejoins **SQLI** à Aix En Provence.
- En 1999 je rejoins **ALBERT** à Montpellier.
- En 1998 je rejoins **NEXUS** technologies à Aspères.
- En 1995 je fais mon service national en coopération sur les îles du **CAP VERT**.
- En 1995 je commence une thèse sur les **COMIQUES CINEMATOGRAPHIQUES** dans les crises (jamais finie).
- En 1994 je finis mon DEA sur le **NON-SENS & ABSURDE** (voir plus bas).
- En 1992 je finis ma maîtrise d'Histoire de l'Art en me spécialisant sur les **MONTY PYTHON**.
- En 1989 je finis mon baccalauréat **ARTS & PHILOSOPHIE**.

## Encore mieux me connaître ?

Pour mieux me connaître, jetez un dé, et piochez la citation :

1. *Aucun plan ne survit au premier contact avec l'ennemi* -- Von Moltke
2. *Je refuse de faire partie d'un club qui m'accepterait comme membre !*  -- Groucho Marx
3. *Ils ne savaient pas que c'était impossible, alors ils l'ont fait* -- Mark Twain
4. *Trop de sages-femmes, le bébé est mort* -- proverbe roumain
5. *Ne dites pas les faits ! Dites la vérité !* -- Maya Angelou
6. *Ne comptez pas vos poulets avant qu'ils soient éclos* -- Esope

## Remerciements

- Merci à l'Université de Caen pour son [dictionnaire de synonymes en ligne](http://www.crisco.unicaen.fr/des/).
- Merci à [HUGO](https://gohugo.io/) et son incroyable vitesse de génération pour générer ce blog.

## La crypte

Bienvenue dans ma petite crypte. Je range ici des vieilles choses qui
n'ont pas de liens avec le blog si ce n'est que c'est moi qui les ai
réalisées. Elles ne sont pas non plus liées à l'agilité, quoique...

### Zepablo's Led Zeppelin

[Site](http://pablo.pernot.free.fr) maintenu de 1997 à 2003 concernant
**Led Zeppelin**. Le premier site français sur Led Zeppelin.

### DEA non-sens & absurde chez les comiques cinématographiques

Un DEA (Diplôme d'Etudes Approfondies) en Histoire du cinéma
(communication & audiovisuel) daté de 1994. Eh oh, un peu de retenue, je
n'avais que 23 ans... (je vais l'OCRiser et le "porter" en html asap).
Il manque aussi les illustrations (les feuilles que j'ai retrouvé ne les
avaient plus), je me charge de les retrouver.

*Je vous recommande l'utilisation du clique droit*

- [introduction](/pdf/01-nonsens-absurde-intro.pdf) \[4.2mo\]
- [première partie : le non-sens et l'absurde](/pdf/02-nonsens-absurde-premierepartie.pdf) \[9.0mo\]
- [deuxième partie : le burlesque muet](/pdf/03-nonsens-absurde-deuxiemepartie.pdf) \[17.4mo\]
- [troisième partie : le burlesque parlant](/pdf/04-nonsens-absurde-troisiemepartie.pdf) \[13.9mo\]
- [filmographie-bibliographie-annexes-index-sommaire](/pdf/05-nonsens-absurde-annexes.pdf) \[15.4mo\]

### Mémoire maitrise sur les Monty Python

... que je ne retrouve plus.

### Dossier pour le baccalauréat 1989 Arts & Philosophie option cinéma & audiovisuel

C'est très naïf, mais je ne renis rien.

- [Travaux sur Star Wars, année scolaire 1988-1989](/pdf/dossier-baccalaureat-star-wars-1989.pdf) \[3mo\]
