---
title: Mentorat
url: /mentorat/
alchemy: 7
---

## POURQUOI DU MENTORAT ? 

Avec des séances de mentorat[^mentorat] vous accédez à une nouvelle conversation autour de vos pratiques. Je parle de *mentorat* pas de coaching. Le *mentorat* : je partage mon expérience, mes pratiques, mon regard. Vous y prenez ce que vous souhaitez. Nous abordons les sujets dans vos contextes, dans vos systèmes. 

Les **impacts attendus** sont :

* Avoir un lieu d'écoute qui permette de s'ouvrir, de se rassurer, d'avoir du *feedback*.  
* Un dialogue qui permette un apprentissage, trouver des solutions, approfondir des réflexions. 
* Accéder à mes connaissances et mes pratiques qui ont été appliquées et qui ont vécu. 

## QUI POURRAIT ÊTRE INTÉRESSÉ ? 

* Les coachs : organisationnels, agiles, individuels, exécutifs, du changement, etc. 
* Les managers. 
* Les dirigeants.

{{< br >}}

		"L'empathie ce n'est pas du tout la confiture affective qu'on vous propose, l'empathie c'est la résultante de quelque chose qui est l'identification à un autre, c'est à dire que je peux m'identifier à l'autre sans cesser d'être moi-même." -- Roland Gori.

{{< br >}}

## POURQUOI JE POURRAIS ÊTRE LA BONNE PERSONNE ? 

Je suis mentor, coach, leader, manager, les appellations sont souvent peu claires. Je suis directeur général chez [benext](https://benextcompany.com), j'encadre particulièrement le [groupe de coaching](https://coaching.benextcompany.com). Je fais de l'accompagnement organisationnel et individuel en entreprise. 

En [savoir plus sur moi](/a-propos/) ou sur le [contenu de mes réflexions](/articles/). 

## CYCLE DE 10 MOIS 
 
Je réalise cette activité à titre individuel, et non pas avec benext (l'entreprise dans laquelle je suis directeur général) pour éviter toutes collusions. Toutes nos conversations, mon écoute, mes questions, mes suggestions, sont celles que je ferais sans filtre, sans question de concurrence ou non, avec toujours le maximum d'authenticité et de vulnérabilité.    

Une inscription inclut une session de une heure par mois, durant dix mois (cela peut s'arrêter cependant à tout moment si l'une des deux parties le souhaite, avec une séance de clôture), nous consoliderons un suivi autour de vos réflexions, vos options, de ce qui émerge de nos conversations. Le cycle démarre par une session gratuite qui permet de se dire "allons-y" ou "n'y allons-pas". 

Le mentoring peut se faire en présentiel comme à distance (meet, zoom, etc.). Ce mentoring est une conversation tête-à-tête. 

La **confidentialité** est de mise et nous signerons tous les deux un document nous engageant en ce sens.  

**Je suis moi-même supervisé** pour pouvoir exercer plus sainement et sereinement cette activité.

## TARIFS

La tarif varie selon si :

* Vous venez à titre personnel (150 € HT par séance soit 1500 € HT pour le cycle de 10 séances) 
* Au travers d'une PME/TPE (250 € HT par séance soit 2500 € HT pour le cycle de 10 séances), 
* Avec l'appui d'une grande entreprise (350 € HT par séance soit 3500 € HT pour le cycle de 10 séances).  

Il y a une **première séance gratuite pour valider que nous avons tous les deux envie d'aller plus loin et poser le contexte**.  

## ME CONTACTER 

* Au travers de l'un des liens en bas de page (twitter ou linkedin)
* En utilisant le [formulaire de contact](/contact/) 


