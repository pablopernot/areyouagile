---
type: section
layout: apropos
title: À Propos
url: /a-propos/
sidebar: true
alchemy: 6
---


{{< lowimage src="/images/202001-pablo-low.png" title="Pablo Pernot" >}}

## À mon sujet 

Après une maîtrise sur les Monty Python, un DEA sur le non-sens et l’absurde, l’entame d’un doctorat sur les comiques cinématographiques, il paraît normal que je me sois lancé dans le management et la conduite du changement, c’est -finalement- une suite logique. Actuellement considéré un acteur expérimenté en management organisationnel (*holacratie, sociocratie, agile, lean, kanban*), j'accompagne de grands groupes et leurs dirigeants dans leurs trajectoires.

## Auteur 

Co-auteur avec [Laurent Morisseau](http://www.morisseauconsulting.com/) de [**Kanban**, *l'approche en flux pour l'entreprise agile* chez Dunod](https://www.dunod.com/sciences-techniques/kanban-approche-en-flux-pour-entreprise-agile). EAN
9782100781058, juin 2019.  

{{< lowimage src="/images/kanban-low.png" title="Kanban Dunod" >}}

*Kanban est un mot japonais qui signifie "étiquette". Le système Kanban est né dans l’industrie automobile au Japon dans les années 1950 pour gérer les flux, notamment sur les chaînes de montage. Puis il a été adapté dans les années 2000 à l'univers du développement informatique où il est devenu l'une des principales méthodes agiles. Aujourd'hui Kanban élargit son champ d'application et peut aider n'importe quelle organisation à s'améliorer.*
	
*Ce livre reprend des parties de celui de L. Morisseau "Kanban pour l'IT"  paru chez Dunod mais qui s'adressait aux acteurs du monde de l'informatique. Son contenu a été modifié pour s'adapter au monde de l'entreprise en général. La première partie permet de découvrir Kanban. La deuxième permet de repenser une organisation avec cet outil, la troisième de mettre en place des outils de mesure de l'amélioration et la quatrième aborde la question du management. Un roman d'entreprise permet d'illustrer le discours en montrant des situations du quotidien dans une entreprise fictive qui pourrait être la vôtre.*

## Petite histoire professionnelle

* en 2021 je rejoins **[OCTO](https://octo.com)**, en tant que *Managing Director* (galaxie [Accenture](https://accenture.com)). 
* En 2015 je rejoins **[BENEXT](https://benextcompany.com)**, en tant que directeur général à Paris. 
* En 2010 je crée **SmartView** avec Christophe Monnier et Gilles Pommier à Montpellier. 
* En 2006 je rejoins **SQLi** à Aix En Provence.
* En 1999 je rejoins **Albert** à Montpellier. 
* En 1998 je rejoins **Nexus** technologies à Aspères.
* En 1995 je fais mon service national en coopération sur les îles du Cap Vert.
* En 1995 je commence une thèse sur les comiques cinématographiques dans les crises (jamais finie).
* En 1994 je finis mon DEA sur le non-sens et l'absurde (voir plus bas).
* En 1992 je finis ma maîtrise d'Histoire de l'Art en me spécialisant sur les Monty Python.
* En 1989 je finis mon baccalauréat Arts & Philosophie.

## Encore mieux me connaître ?

Pour mieux me connaître, jetez un dé, et piochez la citation :
       
1. *Aucun plan ne survit au premier contact avec l'ennemi* -- Von Moltke
2. *Je refuse de faire partie d'un club qui m'accepterait comme membre !*  -- Groucho Marx
3. *Ils ne savaient pas que c'était impossible, alors ils l'ont fait* -- Mark Twain
4. *Trop de sages-femmes, le bébé est mort* -- proverbe roumain
5. *Ne dites pas les faits ! Dites la vérité !* -- Maya Angelou
6. *Ne comptez pas vos poulets avant qu'ils soient éclos* -- Esope

## Me joindre ? 

Par mon [compte twitter](https://twitter.com/pablopernot) ou [linkedin](https://www.linkedin.com/in/pablopernot/).

## Remerciements

* Merci à l'Université de Caen pour son [dictionnaire de synonymes en ligne](http://www.crisco.unicaen.fr/des/).
* Merci à [HUGO](https://gohugo.io/) et son incroyable vitesse de génération pour générer ce blog. 

## La crypte

Bienvenue dans ma petite crypte. Je range ici des vieilles choses qui
n'ont pas de liens avec le blog si ce n'est que c'est moi qui les ai
réalisées. Elles ne sont pas non plus liées à l'agilité, quoique...

### Zepablo's Led Zeppelin

[Site](http://pablo.pernot.free.fr) maintenu de 1997 à 2003 concernant
**Led Zeppelin**. Le premier site français sur Led Zeppelin.

### DEA non-sens & absurde chez les comiques cinématographiques

Un DEA (Diplôme d'Etudes Approfondies) en Histoire du cinéma
(communication & audiovisuel) daté de 1994. Eh oh, un peu de retenue, je
n'avais que 23 ans... (je vais l'OCRiser et le "porter" en html asap).
Il manque aussi les illustrations (les feuilles que j'ai retrouvé ne les
avaient plus), je me charge de les retrouver.

*Je vous recommande l'utilisation du clique droit*

* [introduction](/pdf/01-nonsens-absurde-intro.pdf) \[4.2mo\]
* [première partie : le non-sens et l'absurde](/pdf/02-nonsens-absurde-premierepartie.pdf) \[9.0mo\]
* [deuxième partie : le burlesque muet](/pdf/03-nonsens-absurde-deuxiemepartie.pdf) \[17.4mo\]
* [troisième partie : le burlesque parlant](/pdf/04-nonsens-absurde-troisiemepartie.pdf) \[13.9mo\]
* [filmographie-bibliographie-annexes-index-sommaire](/pdf/05-nonsens-absurde-annexes.pdf) \[15.4mo\]

### Dossier pour le baccalauréat 1989 Arts & Philosophie option cinéma & audiovisuel

C'est très naïf, mais je ne renis rien. 

* [Travaux sur Star Wars, année scolaire 1988-1989](/pdf/dossier-baccalaureat-star-wars-1989.pdf) \[3mo\]
