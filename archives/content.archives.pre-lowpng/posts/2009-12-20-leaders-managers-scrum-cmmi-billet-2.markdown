---
date: 2009-12-20T00:00:00Z
slug: leaders-managers-scrum-cmmi-billet-2
tags: ['agile','cmmi','scrum','leader','manager']
title: Leaders et managers, Scrum et cmmi billet 2
---

J'entame donc ces petites mises en perspectives entre Scrum et CMMi par
les questions de management de projet. Je ne vais pas mettre -encore une
fois- en avant la dichotomie entre le chef de projet et le scrummaster,
elle ne m'intéresse pas forcément telle qu'elle est présentée et elle
n'est pas forcément aussi vraie que cela. Non pour juste dire ce qui
sépare dans ma pratique de Scrum et de CMMi la notion de chef de
projet/scrummaster je vais plutôt m'appuyer sur un passage de
l'excellentissime "Lean Software Management" de Marie et Tom Poppendieck
(que j'encourage tout le monde à lire) qui lui même cite le livre "What
leaders really do" de John Kotter. Synthétiquement on va séparer deux
profils : les **managers** et les **leaders**.

Du côté du manager : on planifie, on budgetise, on organise, on "staff",
on trace et on contrôle. Du côté du leader on indique la direction, la
vision, on fédère les gens, on motive les équipes.Voilà tout est dit.
Oui je sais j'enfonce allègrement des portes ouvertes.

1ère porte ouverte enfoncée : Rien n'empêche à un chef de projet CMMi
d'indiquer la direction la vision, de fédérer les gens, etc.. au
contraire c'est la panacée. L'inverse pour le scrummaster est aussi
vrai. Seulement il faut bien garder en tête que l'**on va d'abord
demander** à un chef de projet CMMi d'organiser de tracer et de
contrôler, et que l'**on va d'abord demander** à un scrummaster de
fédérer et d'accompagner l'équipe.

2ème porte ouverte enfoncée : Certaines personnes ne seront jamais
compatibles avec l'une de ces deux missions, ce n'est pas dans leurs
natures, cela ne les intéressent pas. J'insiste surtout les notions de
fédération de motivation, de leadership, et là je sors un joker un peu
tarte à la crême : tout cela est aussi beaucoup affaire d'**empathie**
et de **psychologie**.

Quelques paradoxes aussi :

Le management désire des leaders, cela donne de la cohésion à ses
équipes, cela génère des dynamiques, et ils sont généralement des
différentiateurs forts dans les appels d'offres. Car le leader possède
une compétence (souvent technique) forte. C'est souvent là dessus qu'il
assied son leadership. Oui à mes yeux un ScrumMaster doit avoir une
compétence technique conséquente, ce qui n'est absolument pas un
pré-requis pour le Chef de projet (CMMi). Au contraire souvent je vois
pas mal de gens désirer le poste de Chef de projet pour s'extraire des
contingences techniques : ils ne souhaitent plus développer. Ils veulent
prendre "de la hauteur" (faire les spécifications, tracer, contrôler,
etc.). C'est la **verticalité** des rôles **CMMi**. Verticalité exprimée
aussi par la hiérarchisation : le Chef de projet est chapeauté par de le
Directeur de projet. A l'inverse je perçois le modèle **Scrum** comme
**horizontal**. Le Scrummaster, le Product Owner, l'Equipe, tout le
monde est sur le même plan. Je reviens à mon propos : le management
désire des leaders pour tout les raisons évoquées plus haut. Mais cela
l'embête bien. Pour lui c'est plus simple de ne pas valoriser certaines
personnes, c'est plus simple d'interchanger les personnes, de consolider
et de prévoir si il n'y a pas de disparité dans les profils. C'est un
reproche assez fort que je fais à l'implémentation de CMMi tel que je
l'ai vécue : une hypocrisie forte à continuer à présenter les gens comme
égaux, ou tout au moins "assez armé" (grâce aux processus mises en
oeuvre pour répondre au modèle CMMi), pour pouvoir être placés,
déplacés, interchangeables. J'exagère un peu, mais c'était l'idée
dominante dans mon vécu. Dans les faits, tout le monde savait exactement
que c'est le contraire qui était vrai. Le management veut des leaders
mais préfère gérer des "managers". En fait là j'étend la notion de
leader à celle de personne clef. C'est différent, mais dans les
relations que j'ai perçu entre management et équipe, avec scrum ou cmmi,
la réponse était la même. Cela dérange CMMi d'avoir des personnes clefs
car justement le modèle refuse une trop forte personnalisation et se
base sur de fortes pratiques. Or tout le monde sait qu'un projet se
bâtit sur des personnes clefs. Dernière remarque à ce sujet, une
personne clef un jour ne l'est pas nécessairement le lendemain. Je veux
dire par là que ce n'est pas une façon de distinguer des bons et des
mauvais, pas du tout.

La **verticalité** que j'évoque dans le modèle classique CMMI et du chef
de projet, a le mérite de proposer une hiérarchie claire : chef de
projet, directeur de projets, voire top management, et puis les -plus
bas- les développeurs, transverse, les responsables qualité, etc. Il est
clair que pour les développeurs ce n'est pas valorisant. Mais c'est
assez reposant (moins de responsabilité et d'implication). C'est la
hiérarchie qui va encaisser les chocs (quand elle joue correctement le
jeu, et que le Chef de projet n'est pas un fusible). Elle a les moyens
de substituer le chef de projet (ou une autre personne) si quelque chose
coince (pour x raison, ne serait-ce au bout d'un certains que
l'usure...). Chez Scrum c'est plus compliqué, le système est normalement
comme je le dis, **horizontal**. Ce n'est pas simple. Si un membre de
l'équipe ne fait pas l'affaire ? ("on le sort au plus tôt !" nous dit
Extrême Programming, "tu lui en as parlé et qu'en pense-t-il" nous dit
scrum, dans les deux cas de jolies phrases mais peu réalistes).Pas
d'échappatoire, beaucoup moins de leviers sur lesquels jouer. La
mayonnaise prend, ...ou pas, il n'y a pas de prédictibilité. Un projet
Scrum ou la mayonnaise ne prend pas, c'est complexe, que faire sinon
faire le constat de l'échec et : changer les équipes (!!!), passer dans
un autre mode ? Mais au moins on aura rapidement de la visibilité sur ce
problème.

Ce rôle de ScrumMaster que j'associe au leader est d'autant plus
complexe que ce leader doit s'effacer pour que l'équipe et le product
owner prennent leur responsabilité.

Donc,

Le chef de projet de CMMi est d'abord pour présent pour consolider
toutes les infos, traces, planning, etc. du projet. Si quelque chose va
de travers il est sensé anticiper et alerter ou déclencher des actions
pour palier aux risques présents ou aux problèmes qui apparaissent. Il
surveille : le périmètre, les livrables, les compteurs, les équipes,
etc. Il rend compte à sa hiérarchie. Il lutte souvent -presque au corps
à corps :p - avec le client pour protéger le cadre très strict de son
contrat. Il s'assure d'ailleurs de la fiabilité de son contrat et que
son plan d'action et de moyen est clairement défini et applicable. "Le
projet sera délivré en temps et en heure avec toutes les fonctionnalités
définies dans les spécifications", c'est son mojo. A la limite si les
développeurs et le client le haïssent cela simplifie les relations.

Le ScrumMaster est d'abord présent pour consolider l'équipe autour de la
valeur à générer : les users stories, les fonctionnalités à développer,
le vrai besoin du client. Si quelque chose va de travers il doit s'en
occuper ou trouver au sein de l'équipe ou du côté du product owner une
solution. Il s'assure que c'est d'abord de la valeur que l'on produit.
Il collabore avec le product owner et l'équipe et les accompagne quand
ils rendent compte au client (au travers des retrospectives). Il doit
aussi planifier les release et les sprint à venir. Quand son manager lui
demande comment il consolide les données du projet : il lui répond qu'il
n'a qu'à observer les photos des radiateurs (post-it sur le murs) et se
prend une soufflante. "Oui nous n'avons pas couvert tout le périmètre
mais ce que nous avons délivré est exploitable, de très bonne qualité,
et vous amène beaucoup de valeur ; pour vous le rapport qualité/prix est
excellent", c'est son mojo. Si les développeurs forment une équipe
soudée qu'il a su motiver autour des besoins du client il sait que le
projet va réussir.
