---
date: 2013-09-18T00:00:00Z
slug: entretien-dembauche-agile
tags: ['workshop','atelier','seriousgame','scierie','rh']
title: Entretien d'embauche agile
---

Hier suite à une conversation avec un copain qui est aussi un client et
qui souhaite "embaucher agile", une idée m'est venue, du genre "mais oui
évidemment". Je lui ai suggéré d'utiliser la [scierie à
pratiques](/pages/la-scierie-a-pratiques.html) pour connaître le
positionnement, la lecture, l'approche, et la sensibilité agile de ses
postulants.

## Mise en oeuvre

Il faut proposer les cartes "pratiques" au candidat et lui laisser cinq
minutes (ou plus) pour en extraire six qui lui paraissent les plus
importantes. Ensuite lui proposer autour de ces six pratiques une
conversation : si il devait en enlever trois et en garder trois,
lesquelles et pourquoi ?

Il n'y a pas de bonnes ou mauvaises réponses, mais vous devriez vite
voir l'adéquation entre votre pensée agile et celle du candidat.

## Variante

Faites une partie de la [scierie à
pratiques](/pages/la-scierie-a-pratiques.html) en plaçant vos candidats
au sein de vos équipes.

## Feedback & audit

Si vous avez l'occasion de pratiquer la scierie à pratiques pour une
embauche j'attends vos feedback. Je viens d'achever un audit et je
regrette de ne pas y avoir pensé non plus pour ce cadre...

## La scierie à pratiques aux Agiles Tour

Je vais pouvoir animer des scieries à pratiques lors de trois *Agile
Tour* :

-   [Agile Tour Marseille
    2013](http://at2013.agiletour.org/en/at2013_marseille.html) le 3
    octobre
-   [Agile Tour Toulouse 2013](http://tour.agiletoulouse.fr/#keynotes)
    le 10 octobre
-   [Agile Tour Montpellier 2013](http://agiletour-montpellier.fr/) le
    18 octobre

Et cela avec un acteur majeur du marketing agile hexagonal : **Stéphane
Langlois** de chez [scopyleft](http://www.scopyleft.fr).
