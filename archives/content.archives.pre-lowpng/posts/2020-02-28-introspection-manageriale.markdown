﻿---
date: 2020-02-29
slug: atelier-introspection-manageriale-solution-focus-host-leadership
tags: [introspection, manager, atelier, workshop, host, leadership, solution, focus]
title: Atelier d'introspection managériale - solution focus et host leadership
--- 

Très récemment [Pauline](https://twitter.com/PauGarric) a eu l'occasion et la gentillesse de reprendre un déroulé d'atelier que j'utilise quand j'ai besoin de faire réfléchir les managers, les chefs, à leurs rôles. C'est un assemblage d'une partie très classique de *solution focus* (ou [thérapie brève centrée sur la solution](https://fr.wikipedia.org/wiki/Th%C3%A9rapie_br%C3%A8ve_centr%C3%A9e_sur_la_solution)) et de [*host leadership*](/2018/01/hote-ecosysteme/). 

Cela fait écho aussi à l'atelier [la scierie à pratiques](/2016/02/la-scierie-%C3%A0-pratiques-%C3%A9volue/). 

Il est très important que les managers ou les chefs réfléchissent à leur rôle, à leur posture, à leur intention. Pour creuser, un podcast de ma part ["La transformation commence par celle des managers" chez Café Craft de Thomas](https://www.cafe-craft.fr/34). 

Cet atelier dure à peu près une heure, et peut concerner autant de managers / chefs que vous le désirez puisqu'ils vont travailler par paire. Ils doivent être équipés de feuilles blanches et de quoi écrire.

## Première partie : approche de type solution focus

Vous demandez d'abord à chacun des managers de lister la liste de ses activités. Disons que vous en recherchez une quinzaine en espérant en avoir *a minima* une dizaine. 

Je suis régulièrement surpris de voir que certains managers ne savent pas lister leurs activités. Vous pouvez leur simplifier la tâche en leur proposant de regarder leurs agendas, en leur donnant des exemples : "réunion de l'équipe", "recrutement", "codir/comex", "consolidation", etc. 

### Prioriser les activités

Après avoir noté ces activités, vous leur demandez de les prioriser. De la plus importante à la moins importante. À eux de décider comment gérer le critère "importance". Quand cet ordonnancement est réalisé, vous leur demandez : 

- Serait-il possible de faire une expérimentation : ce mot est très important, il permet de se dire que si cela ne marche pas on reviendra en arrière, une expérimentation n'est pas ressentie comme quelque chose de durable, de solide, donc on essaye vraiment avec l'assurance de pouvoir revenir en arrière si besoin. Je reprends. Serait-il possible de faire une expérimentation : pourriez-vous durant les deux ou trois prochaines semaines arrêter de faire les deux dernières activités de votre liste et observer les résultats ? 

- De zéro à dix, d'un point de vue qualitatif, comment jugez-vous les trois premières ? Par exemple, "recrutement", je le juge 6/10. Il faut alors se poser deux nouvelles questions. Grâce à quoi j'ai donné ces points à l'activité ? (Même 2/10, d'où viennent ces deux points, il y a toujours du positif). Comment je pourrais gagner un ou deux points de plus sur ces activités dans les deux prochains mois ? Par exemple comment passer de 6/10 à 7/10 le mois prochain en "recrutement" ?  

Ils ont cette réflexion en tête. Comment je priorise mes activités? Lesquelles je pourrais arrêter momentanément ? Pour quel effet ? Lesquelles sont les plus importantes ? Pourquoi ? Comment je les note qualitativement (alors qu'il s'agit de mes plus importantes activités) ? Comment m'améliorer dans les deux prochains mois ? ). C'est le moment d'échanger avec le binôme et de lui livrer ses réflexions pour avoir son *feedback*, son avis, ses suggestions, son regard.  

## Deuxième partie : approche de type host leadership

Vous demandez ensuite à chacun de placer ces activités (toutes celles listées) dans ce cadran (extrait de [host leadership](https://pablopernot.fr/2018/01/hote-ecosysteme/)). Il convient de prendre 5 à 10mn pour expliquer les lieux de *host leadership* et la pensée derrière ce titre. Je vais essayer de la résumer très brièvement. L’idée de ce management moderne c’est de maintenir l’espace, le créer et le maintenir. Pour cela six rôles et quatre lieux symboliques. Voir l'article ["hôte d'écosystème"](https://pablopernot.fr/2018/01/hote-ecosysteme/).

C'est peu, mais c'est déjà beaucoup à ingurgiter (beaucoup de combinaisons). Pour mettre un pied à l'étrier, je me restreins généralement aux lieux. 

### Lieux 

* Sur la scène, en assumant la responsabilité d'être le centre d'attention et en assumant les messages portés. Faire que les choses avancent en les incarnant. Être sur le front, exposé, assumer. Porter l'étendard, défendre l'intention.  
* Au milieu des gens : un parmi les autres. Se montrer parmi les autres, être dans l'intimité du groupe. Se mêler à l'équipe et ainsi décider en étant sur place (le *gemba* du lean) en apprenant depuis le terrain.  
* Au balcon : en observant et apprenant, prêt à être en appui pour maintenir cet espace. On analyse, on écoute, on observe. On prend du recul. 
* Dans la cuisine : là où les recettes se préparent et qu'on les goûte avant de les servir. Être seul ou en petit groupe pour préparer la suite. Réfléchir aux événements, aux observations, chercher comment s'améliorer.  

![Host Leadership](/images/2018/09/host-leadership.jpg)

Vous demandez ainsi à chacun de placer ses activités sur ce cadran. Cela amène des questions : sont-elles déclinées dans tous les lieux ? Y a-t-il des déséquilibres (trop de *cuisine*, pas assez de *scène* par exemple) ? Quelles activités permettraient de combler ces déficits ? Est-ce qu'il y a des activités qui n'appartiennent à aucun lieu ? Si oui, est-il légitime de les réaliser ?  

Là aussi les binômes peuvent échanger. 

## Pour finir 

Concrètement chaque manager / chef peut repartir avec deux actions : 

* Faire +1 sur ses activités les plus importantes dans les deux mois à venir. 
* Équilibrer, combler, sa répartition sur le cadran du *host leadership*. 

Bon atelier. Merci à vous. 

## Des images de l'atelier  

... de [Pauline](https://twitter.com/PauGarric) qui a fait un parcours fléché. Merci Pauline. 


![Host Leadership 1](/images/2020/02/host-leadership-pauline-1.jpg)
![Host Leadership 2](/images/2020/02/host-leadership-pauline-2.jpg)
![Host Leadership 3](/images/2020/02/host-leadership-pauline-3.jpg)
