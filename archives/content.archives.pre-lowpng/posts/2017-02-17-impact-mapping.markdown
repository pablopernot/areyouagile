---
date: 2017-02-17T00:00:00Z
slug: cartographie-strategie-impact-mapping
tags: ['user','story','mapping','agile','cartographie','leanstartup','lean','startup','impact','impactmapping', 'peetic']
title: Cartographie de stratégie, l'impact mapping
---

Si avec la [cartographie de pland'action](/2017/01/cartographie-plan-action/) , le *user story mapping*,
nous avons évoqué la tactique, avec la cartographie de stratégie,
l'*impact mapping*, nous abordons... la stratégie. Avant même de savoir
comment nous allons mettre en œuvre notre produit, l'objectif de la
cartographie de stratégie est de mettre en évidence les chemins
critiques, les chemins clefs pour valider nos hypothèses. Savoir où on
veut aller, et par où passer pour s'assurer au plus vite que le chemin
est le bon. C'est donc souvent un atelier qui précède (quand c'est
nécessaire) le *user story mapping*, la [cartographie de plan
d'action](/2017/01/cartographie-plan-action/). C'est un atelier associé
au Lean Startup.

C'est un atelier qui peut se révéler bien plus difficile que le *user
story mapping*. Dans le *user story mapping* on va trouver le moyen de
décrire comment mettre en œuvre un objectif ; le *user story mapping*
est plutôt axé sur quelque chose de très opérationnel. L'*impact
mapping* a pour but de décrire ce que l'on veut et qu'est ce qui semble
le plus important pour l'atteindre ; l'*impact mapping* est plutôt axé
sur le *quoi, pourquoi*. Ainsi ne soyez pas surpris si cet atelier ne se
révèle pas facile à manipuler.

[Géraldine](https://www.geraldinegeoffroy.fr/) vient
de réaliser un [très bel article sur le sujet](https://www.geraldinegeoffroy.fr/blog/2017/1/26/impact-map-recette-de-cuisine). Je vous
recommande de le lire. Mais du coup je vais aborder les choses un peu
différemment (mais j'ai besoin d'écrire cet article pour la cohérence de
mon propos, et de mon cerveau). Je vais aborder l'atelier au travers de
questions car de mon vécu c'est la seule façon de clarifier cet atelier
quand on s'y égare.

## \#1 question : Sauriez vous me dire ce que fait votre produit (ou votre projet) en deux phrases et comme si j'étais un enfant de huit ans ?

Naturellement je fais appelle ici à la fameuse citation de Einstein :
*Si vous ne savez pas expliquer ce que vous voulez à un enfant de huit
ans vous ne savez pas ce que vous voulez*.

"hum laissez moi réfléchir..." (je vais continuer à utiliser
[peetic](/tag/peetic/)).

"Je veux créer un communauté dynamique et solide de possesseurs
d'animaux, principalement de chiens et de chats, pour ensuite pouvoir
vendre des produits dérivés ou des services autour de ces animaux".

## \#2 question : Qu'est ce qui vous ferait dire que votre produit ou votre projet est un succès ?

Comme avec les [impacts rétro-futuristes de
Claude](http://www.aubryconseil.com/post/Souvenir-des-impacts) présentés
au [Raid Agile](http://raidagile.fr) je vous propose d'exploiter
l'atelier "remember the future" (qui lui même...). Poussez la personne
qui doit répondre à cette question à se projeter : "nous sommes dans six
mois (ou 3 mois, ou à la fin de l'année, etc.) vous avez le sourire à
propos de ce succès ? Pourquoi ? Pouvez vous me décrire la situation ?"

"hum..."

"Dans six mois j'ai constitué une base communautaire solide, et à la fin
de l'année j'ai déjà fait des ventes".

"J'ai réalisé les fonctionnalités attendues", ou "j'ai livré à la bonne
date" n'auraient pas été de bonnes réponses car ce n'est absolument pas
la garantie d'un succès.

## \#3 question : Comment puis-je mesurer de façon explicite ce succès ?

Des fois ce critère de succès reste très flou. Il faut pousser la
personne en charge à savoir le mesurer de façon explicite. Sa mesure
peut naturellement rester une hypothèse (**en fait elle ne peut
probablement qu'être une hypothèse**).

"Le produit plaît aux utilisateurs" pourrait se mesurer explicitement en
énonçant : "80% de taux de satisfaction utilisateurs ?" ou "les systèmes
alternatifs ne sont plus du tout utilisés (0 utilisation des systèmes
alternatifs)", ou "tous les utilisateurs se connectent au moins une fois
par semaine".

À cette mesure il est utile d'ajouter une durée. Ce n'est vraiment pas
grave si la personne en charge sort la mesure d'un chapeau comme par
magie, c'est surtout un outil pour clarifier la conversation ensuite,
mais l'énoncé d'une mesure rend plus tangible les hypothèses.

Pour [peetic](/tag/peetic.html), "hum..."

"Dans six mois j'ai 10000 inscrits, puis tous les mois sur les six mois
suivant j'ai 10000 euros de chiffres d'affaire".

![impact mapping 1](/images/2017/02/10000inscrits-1.png)

Je vais me focaliser sur le premier objectif : 10000 inscrits en 6 mois.
Ici il y a des critères de succès qui s'enchainent. Mais bien souvent on
va vous proposer **plusieurs critères de succès : il faudra
impérativement les ordonner, les prioriser**. C'est le point de départ
de ma carte de stratégie, l'**OBJECTIF**. (On prend un seul objectif par
carte, mais on peut imaginer plusieurs cartes).

## \#4 question : Qui dois-je impacter pour atteindre ce succès (cette mesure explicite) ?

Cette question peut faire l'objet d'ateliers dédiés comme l'a bien
expliqué [Géraldine](http://www.theobserverself.com/pratiques/impact-map/), en
employant des [personas](https://fr.wikipedia.org/wiki/Persona). À la
suite de ces ateliers la réponse ici serait : Le possesseur urbain, le
possesseur rural, le vétérinaire, la fédération animale... (vous pouvez
retrouver des *personas* ici: [peetic](/tag/peetic.html)). Inutile de
mettre tous les [personas](https://fr.wikipedia.org/wiki/Persona)
uniquement les quatre, cinq ou six les plus importants (pour atteindre
notre objectif).

C'est le second niveau de la carte, le **QUI**.

![impact mapping 2](/images/2017/02/10000inscrits-2.png){

## Pondération

L'idée de l'*impact mapping*, de cette cartographie de stratégie, de
cette carte d'impacts, est de **donner du relief**, de **proposer des
hypothèses estimées comme clefs pour les valider au plus vite**. On va
donc essayer de mettre en avant ces chemins essentiels en demandant à la
personne en charge de distribuer 100% sur cette première série de
branches.

![impact mapping 2](/images/2017/02/10000inscrits-2-5.png)

## \#5 question : Comment dois-je impacter ces personnes/profils pour atteindre mon objectif ?

Cette question est sûrement la plus importante et la plus ardue. Si vous
animez cet atelier faîte bien la liaison en illustrant physiquement le
raisonnement à avoir (ah oui je ne l'ai pas précisé mais c'est encore un
atelier avec stickers/post-it au mur...) : quel impact faut-il avoir sur
cette personne, sur ce profil (vous montrez le profil indiqué au mur),
pour atteindre notre objectif (vous indiquez l'objectif).

Dès que vous avez trouvé un impact vous rejouez cette scène ; en
indiquant l'impact vous énoncez : si je génère cet impact (vous indiquez
l'impact), sur cette personne (vous montrez la fiche personne), cela
poussera à atteindre mon objectif (vous indiquez l'objectif).

Encore plus important : **les impacts ne sont pas des moyens, ils sont
des impacts : des changements d'usage, des changements ou de nouveaux
comportements, des bénéfices**. "Recevoir une notification" n'est pas un
impact, c'est un moyen. "Être alerté rapidement" est un impact. "Faire
des crêpes" n'est pas un impact c'est un moyen. "Manger à sa faim" est
un impact. "Utiliser Citymapper ou Google Maps" n'est pas un impact
c'est un moyen. "Gagner du temps" est un impact.

**On ne va pas se poser la question pour chacun des profils, uniquement
ceux dont la pondération en pourcentage le justifie"**. On ne cherche
pas à traiter tous les chemins, c'est inutiles, **on cherche à traiter
les chemins qui semblent être essentiels pour valider au plus vite nos
hypothèses**.

C'est le troisième niveau de la carte, les **IMPACTS**.

## Pondération *bis-repetita*

Pour se **focaliser sur les hypothèses estimées comme clefs et les
valider au plus vite** on va essayer de mettre en avant les chemins
essentiels en demandant à la personne en charge de distribuer 100% sur
chaque série de branches.

![impact mapping 3](/images/2017/02/10000inscrits-3-5.png)

## \#6 Quelle mise en œuvre pour cet impact est possible ?

Ici on retombe dans quelque chose de plus relaxant : les niveaux
suivants de ma carte sont le **COMMENT**. J'ai traité ici tous les
impacts car ma carte est petite, dans la "vraie vie" (là où vous vivez
tous mais pas moi) **il est fortement recommandé là encore de ne se
focaliser que sur les chemins de valeur**. (Et encore la pondération).

![impact mapping 4](/images/2017/02/10000inscrits-4-5.png)

## \#7 Cette mise en œuvre peut-elle se décliner ? (option)

N'hésitez pas à décliner la mise en œuvre. "Notification" peut avoir
comme branches "sms", "mail", "push mobile", etc. Que vous devriez
elles-mêmes pondérer.

![impact mapping 5](/images/2017/02/10000inscrits-5.png)

## Faire apparaître les hypothétiques chemins de valeur à valider

Tout l'**objectif de cette cartographie de stratégie est de faire
apparaître les chemins estimés clefs pour valider ses hypothèses au plus
vite**. Si ces hypothèses sont confirmées : elles vous permettent
d'engranger rapidement des bénéfices, de valider votre marché, etc. Et
vous continuez à dérouler votre stratégie. Si elles s'avèrent fausses,
vous "pivotez" pour utiliser le jargon du Lean Startup.

L'autre bel intérêt de cette cartographie c'est sa capacité à proposer
naturellement de **petits morceaux autonomes qui font sens**. On donne
du **relief à son cheminement**.


![impact mapping 6](/images/2017/02/10000inscrits-6.png)

## Les questions sont clefs

1.  Sauriez vous me dire ce que fait votre produit (ou votre projet) en
    deux phrases et comme si j’étais un enfant de huit ans ? \[**VISION,
    CLARIFICATION**\]

2.  Qu'est ce qui vous ferait dire que votre produit ou votre projet est
    un succès ? \[**OBJECTIF**\]

3.  Comment puis-je mesurer de façon explicite ce succès ? \[**OBJECTIF
    MESURABLE**\]

4.  Qui dois-je impacter pour atteindre ce succès (cette mesure
    explicite) ? \[**QUI**\]

5.  Comment dois-je impacter ces personnes/profils pour atteindre mon
    objectif ? \[**IMPACT**\]

6.  Quelle mise en œuvre pour cet impact est possible ? \[**MISE EN
    OEUVRE**\]

7.  Cette mise en œuvre peut-elle se décliner ? (option) \[**MISE EN
    OEUVRE**\]

## Autres choses que l'on pourrait se dire ?

-   Vous pourriez avoir plusieurs cartes d'impact, *impact mapping* par
    produits, naturellement.
-   Inutile de dupliquer les branches si elles sont exactement les mêmes
    (mais peut-être que selon le "qui", la mise en œuvre se déclinerait
    différemment ?).
-   Surtout ne cherchez pas à définir toutes les branches, c'est
    inutile.
-   Oui, une cartographie de stratégie n'est pas forcément utile, c'est
    selon votre contexte et l'avancée de votre réflexion.
-   Je propose des pourcentages, vous pouvez plus simplement utiliser
    des étoiles, des carambars, un autre indice de pondération.
-   Des fois il est très intéressant de retourner la carte de droite à
    gauche plutôt que de gauche à droite. Pas mal de gens préfèrent la
    lire ainsi: "si je mets en œuvre cela, alors j'aurais cet impact,
    sur cette personne (ce profil), et j'atteindrai une partie de cet
    objectif". On réalise cependant toujours la carte de gauche à droite
    sinon on déduirait un objectif d'une mise en œuvre...

## La suite ?

La suite ? C'est évoquer une autre façon de faire des cartographies de
stratégie avec des variations sur les thèmes des étapes : "objectif",
"qui", "impact", "mise en œuvre", variations inspirées par la [strategy map de agile42](http://www.agile42.com/en/agile-transition/agile-strategy-map/explained/).

La suite : [Impact mapping hors des sentiers battus](/2017/02/cartographie-strategie-impact-mapping-hors-sentiers-battus/).

La suite c'est aussi décrire le branchement évident entre un *impact
mapping* et un *user story mapping*, ou comment la cartographie de
stratégie nourrit la cartographie de plan d'action.

Un dernier mot : le [contenu du workshop de Gojko Adzic](https://gojko.net/news/2016/05/09/open-impact-mapping.html) est
désormais en libre accès.

## Les articles de la série

1.  [Cartographie de plan d’action : le User Story
    Mapping](/2017/01/cartographie-plan-action/)
2.  [Cartographie de plan d’action(user story map)
    revisitée](/2017/02/cartographie-plan-action-revisitee/)
3.  [Cartographie de stratégie, l’impact
    mapping](/2017/02/cartographie-strategie-impact-mapping/)
4.  [Cartographie de stratégie, l’impact mapping, hors des sentiers
    battus](/2017/02/cartographie-strategie-impact-mapping-hors-sentiers-battus/)

