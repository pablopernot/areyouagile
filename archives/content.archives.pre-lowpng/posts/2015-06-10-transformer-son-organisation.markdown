---
date: 2015-06-10T00:00:00Z
slug: chemin-dune-transformation-agile
tags: ['transformation','agilefluency','fluency']
title: Chemin d'une transformation agile
---

Il y a quelques semaines j'ai essayé de répondre à la question d'une
personne qui m'interrogeait sur la [réussite d'une transformation
agile](/2015/05/quest-ce-quune-transformation-agile-reussie/), comment
la concevoir, comment la juger ? Depuis, accompagné par [Stéphane et
Aurélie](http://www.smartview.fr/societe/equipe/) qui porte l'offre
[Atlassian](http://atlassian.smartview.fr) chez Smartview pour une
présentation sur la mutation d'une entreprise dans son chemin vers agile
(et son outillage afférent, ça c'est la partie de Stéphane et Aurélie),
j'ai creusé la proposition clairvoyante de **Diana Larsen** et **James
Shore** : [Agile
Fluency](http://martinfowler.com/articles/agileFluency.html) (rappelé à
mon bon souvenir par [Claude](http://www.aubryconseil.com/) lors d'un
[raid agile](http://raidagile.fr)).

[Agile Fluency](http://martinfowler.com/articles/agileFluency.html) me
parait une très bonne façon de penser son chemin au travers de
l'agilité. Agilité qui n'est jamais en soi le but d'une organisation,
mais disons la meilleure façon d'avancer dans l'époque actuelle (voir
[Modernité, Agilité, Engagé](/2015/01/modernite-agilite-engage/)).

## Que nous dit [Agile Fluency](http://martinfowler.com/articles/agileFluency.html)

![Agile Fluency](/images/2015/06/fluency.png)

### Se focaliser sur la valeur

Une première étape est de comprendre qu'il faut **produire de la valeur
et non du travail (ou du code)**. la valeur c'est des choses finies, on
réorganise donc les équipes pour qu'elles travaillent ensemble de façon
hétérogène, et de façon à produire tout ce qui est nécessaire pour avoir
quelque chose de fini : d'utilisable, que l'on puisse mettre en
production pour générer de la valeur (apprendre avec du *feedback*).

Généralement dans une entreprise on essaye cela de façon protégé dans un
"démonstrateur", une, deux ou trois équipes dédiées pour essayer ce
nouveau mode de fonctionnement. Mais déjà cela soulève de nombreuses
questions comme qu'est-ce que c'est quelque chose de fini, cela
nécessite de repenser la composition et le rythme de travail desdites
équipes.

La lecture de l'avancée de la création de valeur est drastiquement
facilité (si on est sûr de faire du "done", "fini" : seul moyen d'en
être définitivement sûr : mettre en production). Soit on a quelque chose
soit on ne l'a pas. On oubli les opaques : on fait X% de cette
fonctionnalité ou du travail à faire. On ne mesure plus le temps (là j'y
vais un peu fort) ! On ne mesure plus le travail réalisé ! On mesure la
valeur créée (pour cela : les [métriques
pirates](http://fr.slideshare.net/JurgenDeSmet/ale2012-pirate-metrics-aarrr)
AARRR (leur cri) par exemple, ou mieux le *cash* généré, etc.).

### Délivrer de la valeur

L'étape suivante est d'apprendre à **délivrer de la valeur quand cela a
de la valeur**. C'est une étape qui nécessite une vraie technicité : on
a acquis assez de savoir faire (*Test Driven Development*, *Pair
Programmin*, tests automatisés, plate-formes d'intégration continue)
pour que la question de la livraison soit uniquement une question
métiers/business. Tous les rouages technologies permettent à tout moment
de basculer des éléments. On peut y associer le mouvement *devops*.

Pour les IT, les infra, les DSI, les développeurs, c'est la période de
la sueur. Cet apprentissage n'est pas négligeable. Et cette
infrastructure a un réel coût -- avoir des plate-formes simili-prod dont
les jeux données sont constamment exploités -- mais si vous comparez ce
coût par rapport à ce que vous coûte les infrastructures habituelles,
très statiques, fossiles, en efficacité technique et business, vous
comprenez rapidement que vous y gagnerez rapidement.

Si le démonstrateur a été un succès on songe à déployer plus dans
l'organisation. C'est donc aussi souvent le moment ou Kanban se déploie
dans l'organisation : soit pour intégrer la TMA, soit pour synchroniser
les différentes équipes, départements, etc. C'est le moment douloureux
où l'on se rend compte qu'une approche classique n'est pas du tout
compatible avec une approche agile : autant dans la culture, que dans le
mode opératoire de part son rythme.

A ceux qui imaginent avoir une organisation bicéphale : agile et non
agile, je dis que vous pourrez le tolérer durant un temps, temps de la
transformation, mais que c'est illusoire comme cible. Cela ne veut pas
du tout dire que tout l'agile dans la structure est le même, surtout
pas. Il va prendre une forme différente selon les contextes, mais il
sera mené par une même culture, un même focus valeur.

### Optimiser la valeur

Dès lors que l'on sait générer de la valeur, et la délivrer quand cela a
de la valeur, on va **optimiser en quelque sorte la densité de valeur**,
ou optimiser le compromis effort/valeur. On va maximiser la valeur,
minimiser l'effort. On essaye d'apprendre au plus tôt de la valeur
délivrée pour réajuster si besoin les mouvements suivants. On peut y
associer le mouvement *Lean Startup*.

C'est le moment où les projets et la TMA laissent aussi place à une
approche vision, métiers et équipes. On ne pense plus projet, on a pris
du recul sur la cohérence globale, et replacer la valeur dans un
ensemble plus grand, relié à la vision métiers de l'organisation.

Aujourd'hui beaucoup de structures sont friandes des laboratoires
d'innovation de type *Lean Startup* ou *Design Thinking* (nouvelles
appélations pour agile, qui devient dur à porter). A mes yeux c'est
l'expression d'abord d'un échec : ne pas avoir réussi à déployer cette
culture au niveau global : l'innovation, la culture agile est le sujet
de l'ensemble de l'organisation, sûrement pas d'un seul morceau, surtout
si ce morceau se limite à un laboratoire qui ne gère pas de bout en bout
le cycle de vie d'un produit. On a essayé Agile, on a échoué, pour
certaines raisons, on retombe dans le mode "démonstrateur" : on
sanctuarise un espace où on l'a le droit de penser différemment
(changement de paradigme agile). Espérons que ces nouvelles graines
prennent mieux.

### Optimiser le système

**L'organisation se repense autour de cette création de valeur** : plus
de TMA ou de projets mais des axes métiers, de valeur, que l'on aborde.
De nouvelles façons de penser les produits, les relations entre les
personnes, etc... Si Diana Larsen et James Shore indiquent un quatrième
niveau, pour moi il s'agit plutôt d'une maturation constante : la
nouvelle entreprise émerge doucement. Les questions de hiérarchie, de
XXXcratie, d'organigramme, de RH se posent au fil de l'eau (et nécessite
souvent une double lecture durant la transformation).

### Émotion

Enfin cette transformation à mon sens, ne peut pas se penser sans
s'accompagner de sa dose d'émotion, véritable énergie, force de la
transformation personnelle et d'entreprise ([Émotion, énergie et
conduite du
changement](/2015/04/emotion-energie-et-conduite-du-changement/)).
L'émotion est l'énergie indispensable du changement, sans émotion tout
effort dans ce domaine reste de l'analyse sèche et inutile.

Attention car la transformation ne s'arrête jamais. Mais c'est aussi
tout le plaisir.

Très cher [Fabrice](http://wiki.ayeba.fr/), voudrais-tu traduire le
texte complet sur [Agile
Fluency](http://martinfowler.com/articles/agileFluency.html) ? (Mais
peut-être l'as-tu déjà fait ?).

### Slides

Si vous êtes intéressés par les slides avec [Stéphane et
Aurélie](http://www.smartview.fr/societe/equipe/)
([Atlassian](http://atlassian.smartview.fr) chez
[SmartView](http://www.smartview.fr)), les voici :

<script async class="speakerdeck-embed" data-id="306ad7ba8df244c78e0570982e20a6ad" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>
