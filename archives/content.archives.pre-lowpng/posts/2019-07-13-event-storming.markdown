﻿---
date: 2019-07-14
slug: event-storming-description
tags: [event, storming, eventstorming, peetic]
title: Event Storming, une description
--- 

L'*event Storming* est un atelier très simple. Quelques règles suffisent. Il est aussi très efficace et souvent utile. J'avais pu l'évoquer dans cet [article en 2016](/2016/10/atelier-esprit-dequipe/), mon actualité me pousse à vous en reparler aujourd'hui. 

L'*event storming* est, pour paraphraser [Thomas Pierrain](https://twitter.com/tpierrain), une réelle façon d'*harmoniser les modèles mentaux*, en un mot de s'aligner. Il est fréquent de découvrir la surprise des personnes lorsque leur activité se dévoile au travers du management visuel par l'entremise d'un [système Kanban](/2019/06/kanban-le-livre/). C'est la même surprise que j'observe quand ils se penchent sur un partage de leur processus. Car c'est bien le processus dont l'*event storming* est le révélateur.   

L'idée de l'*event storming* est de rendre visible ce processus. Pour cela, comme le rappelle son créateur, [Alberto](https://twitter.com/ziobrando), il faut de l'espace et de tonnes de stickers. Sur cet espace une ligne de temps est représentée par du papier craft sur quelques mètres. On y collera des des stickers dont les codes couleur seront bien respectés, et sur lesquels on aura pris soin de bien écrire en majuscules. Puis l'objectif sera de rendre palpable le processus, ce qui se déroule dans la vraie vie, sur cette ligne de temps. L'intérêt étant d'en percevoir véritablement les circonvolutions, les creux, les surcharges, tous les signaux que nous ne voyons pas autrement, et surtout les voir tous ensemble, avec tous les acteurs.

## Principes de base

* Ainsi donc une grande ligne de temps représentée généralement par 5 à 10 mètres de papier craft. 
* Des stickers de couleurs.
* Quelques heures devant soi.
* Un groupe de personnes composé par les acteurs en rapport avec le processus défini.   

### Étape 1 : on indique les évènements

Les *évènements* vont être les éléments centraux. C'est eux qui se disséminent sur la ligne de temps. Très important ils sont décrits avec : un adjectif, un verbe au participe passé : "contrats envoyés", "pierres polies", "inscription reçue", "bon de commande généré", etc. C'est important de garder ce formalisme. Il nous aide à faire de ces éléments des marqueurs temporels (sur une ligne de temps), et ils contiennent un impact, une action, qui est achevé.  

On débute donc généralement pendant un bon moment uniquement avec les évènements. Et si vous le pouvez, normalement c'est assez facile, vous demandez d'abord à délimiter le début et la fin : quel est le premier évènement du processus que nous souhaitons observer, faire émerger, et le dernier. Vous balisez le point de départ et le point d'arrivée.  

La ligne de temps se remplit de stickers orange. Avec les gros groupes (12-20 personnes), des sous-groupes se forment rapidement le long du mur généralement. 

La ligne de temps n'est pas qu'une ligne, on y voit les différents chemins d'une même action. 

C'est chaotique. C'est normal. Le processus que chacun a modélisé à sa manière explose sur cet aplat.

De nombreuses conversations fleurissent un peu partout. C'est très bien, c'est l'objectif.  

Vingt minutes comme une heure ou plus peuvent suffire. 

Finissez cette étape par un essai de *storytelling* : demandez à quelqu'un, ou plusieurs personnes, de raconter le processus tel qu'il est apparu. Cela sera probablement l'occasion de le revoir encore, de le préciser, de le changer, de s'aligner. Cela peut vous pousser à prolonger cette première étape. 

Comme facilitateur vous n'avez pas à recadrer énormément de choses. Laissez-les prendre possession de leur espace. N'oubliez pas le *storytelling*. 


*Voici les "restes" d'une explication sur un petit event storming avec "[Peetic](/tags/peetic/)".* 

![EventStorming](/images/2019/07/eventstorming1.png)

![EventStorming](/images/2019/07/exemple-eventstorming-7.png)

![EventStorming](/images/2019/07/exemple-eventstorming-5.png)

![EventStorming](/images/2019/07/exemple-eventstorming-2.png)

### Étape 2 : acteurs, données, adhérences

Quand vous pensez que c'est le bon moment (mais je suggère *a minima* après la première phase de *storytelling*) vous pouvez proposez aux acteurs de placer sur les évènements deux autres types de stickers : les acteurs (sans limites de nombre), les données clefs (limité à 3). On limite les données, car sinon on oublie ce qui compte et c'est important de les remettre en valeur. On ne limite pas les acteurs pour faire émerger les potentielles raisons de points de blocages (trop d'acteurs, ou pas assez d'acteurs).

Enfin vous pouvez demander à faire émerger sur une ligne qui surplombe la ligne de temps les adhérences (ou sur les évènements) : toutes les dépendances auxquels est soumis le processus.   

On obtient au bout de une, deux ou trois heures une jolie fresque qui représente le processus (par exemple : de l'idée d'un nouveau produit jusqu'à sa mise à disposition en magasin). 

Il est encore intéressant de reprendre du *storytelling*, c'est-à-dire raconter les évènements au fil de l'eau, tel qu'on les vit "dans la vraie vie" pour s'assurer que la fresque malgré tous les regards croisés et les conversations, ne s'est pas trop éloignée de la réalité.   
 
![EventStorming](/images/2019/07/eventstorming2.png)

![EventStorming](/images/2019/07/exemple-eventstorming-4.png)

![EventStorming](/images/2019/07/exemple-eventstorming-1.png)

### Étape 3 : à vous de jouer

Selon votre contexte vous pouvez décider d'ajouter d'autres éléments en prenant garde qu'ils ne saturent pas le management visuel et nous permettent toujours de lire des choses : goulots, creux, etc. On peut découper verticalement ou horizontalement des grands ensembles, on peut commenter sur la ligne de temps (écrire des labels). Je propose assez souvent de mettre en évidence les évènements clefs de la ligne de temps et pose toutes les questions qui paraîtront intéressantes. La matière qui s'offre est généralement assez souple pour plein de réflexions et de conversation, d'autant qu'elle est celle du quotidien habituellement. 

Affichez clairement la "légende" quelque part qui indique à quel type correspond la couleur du sticker. Par exemple, orange pour les évènements, jaune pour les acteurs, vert pour les données, rose pour les adhérences.  



![EventStorming](/images/2019/07/exemple-eventstorming-3.png)

![EventStorming](/images/2019/07/exemple-eventstorming-6.png)



*Voici un exemple qui introduit une autre famille de stickers : les déclencheurs. C'est un élément qui existe dans l'event storming original de Alberto. Il ne faut pas oublier que c'est au départ un outil pour rendre l'implicite explicite pour les développements. À l'origine des événements il y a donc des déclencheurs. Chaque élément d'un event storming a pour vocation de représenter un concept de développement. Dans ma pratique les déclencheurs sont aussi devenus des événements, car je ne travaille pas souvent avec le code. Vous observerez aussi des événements temporels.*  

![EventStorming](/images/2019/07/eventstorming-declencheur.png)

## Quand l'utiliser ?   

Quand vous avez besoin d'accorder, d'harmoniser, le point de vue de différents acteurs sur ce qui se déroule dans la réalité sur un sujet. L'*event storming* est une grande conversation autour d'un processus rendu palpable. 

Son résultat demeure éphémère, le processus continue à évoluer, le processus ne se réduit aux choix proposés durant la séance. Mais le jeu de cet atelier en vaut largement la chandelle.  

## Que peut-il initier ? 

* Une meilleure compréhension par chaque acteur de son rôle et du processus. Et ainsi un rapprochement entre acteurs, de meilleures prises de décision ? 

* Un [système Kanban](/2019/06/kanban-le-livre/): vous avez une belle occasion de découper les différentes étapes, de mettre en évidence les cartes qui pourront nourrir ce système Kanban (par exemple "contrat envoyé" peut devenir une carte "contrats" et passer à travers des étapes "à faire", "sélection", "en cours", "fini", pour rester très classique) ?   

* Une recomposition des équipes ? 

* Un travail le processus lui-même : l'alléger ? L'équilibrer ? Le renforcer ? 

## Feedback Twitter

Sur Twitter [Jean-Marie Lamodière](https://twitter.com/JMLamodiere/status/1239909208461651968) rappelle -- à juste titre -- que c'est un "format chaotique, prévoir un facilitateur aguerri (ou de se former)". 