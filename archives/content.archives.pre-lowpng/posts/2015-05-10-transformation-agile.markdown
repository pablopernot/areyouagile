---
date: 2015-05-10T00:00:00Z
slug: quest-ce-quune-transformation-agile-reussie
tags: ['transformation','organisation']
title: Qu'est-ce qu'une transformation agile réussie ?
---

La semaine dernière, il me semble avoir vu passer une déclaration de
Alistair Cockburn (je vous laisse traduire), l'un des signataires du
manifeste agile, et l'une des personnes qui continue de nourrir la
réflexion, ainsi donc une déclaration de Alistair Cockburn qui disait
n'avoir jamais connu de transformation agile réussie de sa carrière,
sous-entendu à grande échelle, dans toute l'entreprise.

Effectivement c'est très difficile à juger, à réussir, à mesurer. On
sait que seul 30% des transformations (en tous genres) depuis 30 ans,
aboutissent. Mais aboutir cela veut dire quoi ? On imagine aisément,
selon les tailles des organisations, que cela ne peut pas être identique
partout.

On ne poursuit pas une chimère sinon on aurait arrêté depuis longtemps
de se battre. On observe les bienfaits de nos approches sinon on aurait
arrêté depuis longtemps aussi. Le jeu en vaut la chandelle. Mais rien
n'est simple, on le savait. Je me suis donc reposé la question, de façon
pragmatique. En fait c'est un client qui m'a posé la question. La
réponse m'a semblé tellement évidente que sur le moment je suis resté un
peu figé. La réponse que je vous fait là n'est donc pas celle d'une
réflexion hors contexte, mais bien concrète à un client, dans un
environnement concurrentiel, dans un milieu très informatisé.

## Qu'est-ce qu'une transformation agile réussie ?

Plus globalement que veut dire agile qui réussit ? A mes yeux, c'est une
capacité pour les entreprises à répondre au monde complexe moderne :
inattendu, entrelacé, changeant. Répondre cela veut dire avoir le
meilleur ratio effort/valeur. De la valeur cela veut dire des choses qui
généralement se vendent, et donc qui sont soit en accord avec les
besoins du marché (pas trop tard, pas trop tôt), soit innovantes (les
premiers), soit reconnues pour leurs qualités (adaptatées, durables,
plus efficaces, etc.). Je rappelle que j'évolue dans 99% des cas dans un
environnement concurrentiel.

Agile, et l'un de ses prédécesseurs, Lean, pense que cela viendra d'une
capacité à avoir du délivrer régulièrement des choses finies
(*feedback*), une capacité à s'améliorer régulièrement. Agile pense
aussi que le levier de performance de tout cela, là où la différence se
fera, c'est dans l'implication et la responsabilisation des personnes.
**La performance, mot honnit, c'est plus prosaïquement, être bien et bon
dans ce que vous faites**.

Une entreprise agile est une entreprise qui sait répondre ainsi
(compromis effort/valeur -&gt; besoins du marché, innovation, qualités)
en mettant en oeuvre feedback régulier et une amélioration continue et
en s'appuyant sur la responsabilisation et l'implication des personnes.

Cette transformation peut s'équiper de plusieurs méthodes : scrum,
kanban, xp, etc. C'est un outillage reconnu et validé qui peut aider à
atteindre ces buts et ces moyens (implication, amélioration continue,
etc.).

## Comment mesurer une transformation agile réussie ?

Il faut savoir mesurer **notre cible** : des produits innovants et/ou
répondant au besoin et/ou avec des qualités. Il faut savoir mesurer
**notre levier de performance (être bien et bon)** : personnes
impliquées et responsabilisées, capacité à délivrer régulièrement des
choses finies, capacité à s'améliorer régulièrement.

### Faut-il mesurer ?

Il faut savoir, avoir du feedback. Ce que l'on appelle mesure, c'est du
feedback et des comparaisons.

## Mesurer la cible

### Si la valeur est le cash

Le ratio vente/coût : c'est très difficile d'avoir des chiffres précis,
il y a beaucoup de jeux avec les chiffres et le coût total est souvent
très difficile à rendre apparent. Par contre on peut avoir de grandes
lignes : par domaine métiers, ensemble du temps passé de la TMA /
développement (idem le calcul de cette charge devrait être macro), etc.
Cela doit rester dans les grandes lignes, car les mesures de temps ne
fonctionnent jamais si elles s'imaginent précises, et produisent un
effet désastreux quand on les suit de près.

### Si la valeur est la qualité des produits

Ces qualités auront un effet immédiat sur le cash/vente. Une enquête
régulière client sur la satisfaction de celui-ci serait un bon moyen.
Une enquête et sur le produit, et sur la relation avec l'entreprise. Une
des qualités du produit est de n'avoir que très peu de bugs : la mesure
des bugs clients est un bon indicateur (Nb de bugs déclarés par les
clients (sans primes associées sinon les arbitrages sur le terme "bug"
vont être manipulés)).

### Si la valeur est l'innovation

Mesurer la capacité à vendre des domaines métiers, ou des comportements
utilisateurs qui n'existent pas encore dans l'offre actuelle (sans
primes associées sinon les arbitrages sur le terme "nouveau" vont être
manipulés).

## Mesurer le levier

**Délivrer des choses finies** (qui sont utilisées et probablement
vendues) : mesurer le temps entre l'idée et la livraison. **S'améliorer
continuellement** : suivi des propositions d'amélioration et de leurs
achèvements.

**Personnes impliquées et responsabilisées** : Pour cela je suggère un
**questionnaire anonyme régulier**. Ce questionnaire s'appuie sur les
propositions de Michael Sahota & Olaf Lewitz, il est nécessairement
anonyme, on va (on commence tout juste) le faire remplir tous les deux
mois, c'est les variations et la direction qui comptent.

Voici les questions (les réponses se basent sur une échelle de 0 à 10).

-   Niveau de confiance : je ne peux pas faire confiance (pas du tout =
    0), je peux faire confiance (totalement = 10)
-   Niveau de sécurité : je me sens insécurisé (complètement = 0) ou
    sécurisé (complètement = 10)
-   Niveau d'ouverture : je peux m'ouvrir, m'épancher, (totalement = 10)
    ou je ne peux pas (du tout = 0)
-   Niveau de connectivité : je travaille avec du monde (plein =10 ), ou
    seul (tout seul =0)
-   Niveau d'entièreté : je m'épanouis complètement (10) ou juste sur
    certains aspects de mon être (pas du tout = 0)
-   Le pouvoir est distribué ou centralisé : de 0 (très centralisé), à
    10 (très distribué)
-   Accès à l'information : de 0 (très limité) à 10 (très transparent et
    facile d'accès)
-   Planification, contrôle ou émergence : de 0 (très
    planifié/contrôlé), à 10 (très émergent)

![tableau-feedback](/images/2015/05/tableau-feedback.png)

Pour cela je me suis basé sur la session avec **Michael Sahota & Olaf
Lewitz** à laquelle j'ai assisté, elle même très inspirée par la
**Spirale dynamique de Clare Graves**. En voici les
[slides](http://www.slideshare.net/michael.sahota/agile-enterprise-reinventing-organizations).
Après vous pouvez utiliser leur tableau pour placer votre organisation
et la voire évoluer au fil des mois.

### Faut-il mesurer le déploiement des méthodes (Scrum, Kanban, XP) ?

Oui et non. Oui car il va être difficile de mesurer autre chose au
début. Non car ce n'est pas du tout un indicateur de la réussite des
attentes de l'entreprise, juste un indicateur, et pas un objectif ou une
ambition.

### Faut-il mesurer les aspects "PMO" ?

C'est à dire : est-ce que les projets vont bien et avancent ? Attention
les projets peuvent "aller bien" et "avancer" pour finalement ne rien
délivrer.

Si un projet délivre régulièrement des choses qui sont utilisées (et
donc que l'on vend) et que l'on a un taux de bugs clients très bas, et
que l'on mesure le cash qui entre, c'est inutile de mesurer autre chose.
Le principal risque c'est de croire que l'on a quelque chose et que l'on
ne l'a pas : ce risque est atténué régulièrement par la livraison en
clientèle (et sinon on obtient un *fail fast*). Mais il est utile de
savoir aussi si un projet ne va pas bien pour prendre des mesures, faire
des arbitrages. On doit mesurer que les projets délivrent bien
régulièrement des choses (jusqu'au bout, et avec des qualités), et qu'il
ne vont pas mal. L'un peut être induit par l'autre.

Effectivement une score card (comme chez
[Spotify](https://labs.spotify.com/2014/09/16/squad-health-check-model/))
peut donner une image macro de l'état des lieux des projets. On peut
faire une sorte de niveau de maturité des équipes agiles.

Car **la vraie réponse PMO c'est de ne pas mesurer les projets mais les
équipes, et notamment leur bien être et leur autonomie**.

