---
date: 2016-07-04T00:00:00Z
slug: les-mots-protocole
tags: ['mot','mots','protocole','coaching','cleanlanguage']
title: Les mots protocole
---

J'évoquais précédemment l'équivoque des mots ([attention et constellations](/2016/07/attention-et-constellation/)), ils sont
beaucoup plus insaisissables qu'on ne l'imagine. Plutôt que d'en déduire
le sens dans une conversation, nous partons de sensations pour en
constituer un mot. Je commence à préconiser à certains clients
l'utilisation de ces mots protocole.

Parfois dans ma vie privée j'ai besoin d'un espace de protection sans
avoir à me lancer dans une grande explication. Quand la situation
devient compliquée pour l'un ou pour l'autre, plutôt que de
maladroitement essayer de la rétablir on se laisse de l'espace. Ou
encore, l'autre n'a pas compris que vous touchiez un point sensible, ou
qu'il avait été involontairement maladroit. Ici aussi besoin d'espace,
de temps. Dans tous ces cas dans ma vie privée j'ai institué ce que
j'appelle des "mots protocole". Un protocole comme en diplomatie,
l'établissement d'une convention. Une sorte de méta-communication qui
envoie un signal simple mais clair : par exemple, *Porcelaine* équivaut
à "grande fragilité, genre de catatonie qu'il vaut mieux éviter d'avoir
simultanément.", *Ballerine* équivaut à "se prendre une ballerine dans
la tronche, ne pas se sentir à la hauteur", ou encore *Piano* équivaut à
"harmonie rompue, accords dissonants. Y aller piano : y aller mollo".

## Madeleine de Proust

Il suffit de prononcer l'un de ces mots pour que chacun embarque son
histoire, son tableau, sa fresque, sa mémoire. On a vécu une situation
une ou plusieurs personnes, on place un mot dessus : comme une odeur,
comme une madeleine de Proust\*. Le mot embarque la sensation du moment,
la mémoire du moment, pour chacun, à sa sauce, à sa façon. Un sens créé
en commun qui se passe d'explications, de précisions. À vouloir être
trop précis, on perd souvent le sens des choses. Ces mots se passent
donc de précisions. Les **mots protocole se nourrissent d'une situation
vécu à deux ou à plusieurs pour établir une convention que l'on
invoquera à moindre coût et qui embarquera une grande précision dans les
sensations, et donc dans sa compréhension**.

-   *Une madeleine de Proust est un élément de la vie quotidienne, un
    objet ou un geste par exemple, qui ne manque pas de faire revenir un
    souvenir à la mémoire de quelqu'un, comme le fait une madeleine à
    celle du narrateur d'À la recherche du temps perdu dans Du côté de
    chez Swann, le premier tome du roman de Marcel Proust*.
    (<https://fr.wikipedia.org/wiki/Madeleine_de_Proust>)

## Coaching d'organisation et mots protocole

J'utilise de plus en plus des saynètes\* pour incarner les
rétrospectives ou les restitutions lors de mes travaux en organisation.
Une vraie catharsis : remémoration affective. Récemment cela a été
l'occasion pour un groupe de discuter de questions de confiance et de
transparence. De ne pas opposer autonomie, responsabilisation et
transparence et *feedback* mais de ne pas pousser la transparence
jusqu'à l'oppression. On a donc réalisé une saynète avec les acteurs
concernés pour évacuer la question et nous avons saisi cette occasion
pour utiliser les **mots protocole en organisation**.

Voici le dernier dialogue de la saynète (jouée en présence de tous lors
d'une restitution) :

"On a qu'à utiliser des mots 'protocole'. Une sorte de
méta-communication. On continue comme avant mais quand on ressent que tu
es trop dans l'intrusion et que l'on a besoin de te le faire savoir sans
te vexer on dit *budweiser* c'est notre mot protocole pour nous donner
un peu d'air :). Quand vraiment on ne te donne pas assez de *feedback*
sans s'en rendre compte, que l'on te fait tourner en bourrique, tu nous
dit *paprika* là on doit savoir sans se vexer que l'on doit s'arrêter un
moment et clarifier la situation du mieux possible. Et puis bon c'est
pas *open bar*, on essaye de ne pas dépasser trois utilisations par mot
et par mois pour chaque groupe. Enfin on regarde ça. Cela devrait rester
exceptionnel. Quand un *paprika* répond à un *budweiser* on doit en
discuter tout de suite hors site, autour d'une bière."

On a vécu des situations et la communication ne réussit pas se dérouler
convenablement. On va donner un cadre et un espace à chaque partie pour
se réapproprier ce moment sans passer par un dialogue qui ne fonctionne
a priori pas. Le rappel de la sensation rappelle le sens. Encore une
sorte de catharsis, une remémoration affective.

Attention comme cela fait appel à une mémoire, **une trop forte
utilisation use et fait disparaître cette mémoire et donc la possibilité
de l'utiliser. Mais une trop faible utilisation fait disparaître cette
mémoire**.

-   Oui c'est la bonne orthographe, avec surprise j'ai découvert qu'une
    saynète c'est historiquement le petit morceau de graisse dont on
    récompensait les faucons à leur retour, espèce d'amuse-gueule :
    *Succès garanti pour qui, dans une dictée de compétition,
    proposerait à ses ouailles une « saynète du regretté Mack Sennett »
    ! Mais, avec ce mot, nous ne sommes pas au bout de nos surprises :
    avant de s’épanouir sur les planches, il appartenait au petit monde
    de la… vénerie ! Ne désignait-il pas – que l’on songe au « sain » du
    sanglier, et aussi à notre « saindoux » – le petit morceau de
    graisse dont on récompensait les faucons à leur retour ? Espèce
    d’amuse-gueule, donc, au même titre que la pièce bouffonne en un
    acte donnée en guise d’entremets pendant l’entracte... -- Bruno
    Dewaele*
    (<http://www.projet-voltaire.fr/blog/regle-orthographe/scenette-ou-saynete>)

## Parenthèse

Petite parenthèse dans tous les modèles qui fleurissent autour des
profils Jungien comme Process comm, Ennéagramme, DISC, les couleurs
(rouge, jaune, bleu, vert) du concept Nova, etc. que l'on aime ou que
l'on aime pas l'idée de fond, ce que j'en tire c'est aussi une
méta-communication. Je le conseille donc parfois comme une facilité de
communication, un point d'ancrage qui évite les dérapages, un cadre sans
précision mais pour au moins établir une communication valable. Ce n'est
pas la même chose que les mots protocole que j'évoque qui se nourrissent
d'une situation vécut pour établir une convention, mais dans le refus
d'imaginer pouvoir expliquer tout avec des mots mais d'instituer un
espace dans une communication souvent bien plus incertaine qu'on
l'imagine cela se ressemble.

