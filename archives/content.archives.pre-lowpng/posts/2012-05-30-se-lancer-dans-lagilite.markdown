---
date: 2012-05-30T00:00:00Z
slug: se-lancer-dans-lagilite
tags: ['agile']
title: Se lancer dans l'agilité
---

La semaine dernière, j'ai pu réaliser une petite présentation de
l'agilité à un petit groupe de startups (ou autre) montpellieraines à
Cap Omega. En voici les slides. L'idée n'est pas -à la grande
frustration des gens habituellement- d'être forcément concret au travers
d'une méthode bien précise, scrum, kanban, xp ou autre. Mais bien
d'aborder l'agile de façon plus globale. L'état d'esprit agile avant la
mise en pratique.

Vous trouverez les slides chez [Speakerdeck](https://speakerdeck.com/pablopernot/) si cela vous
intéresse :

<script async class="speakerdeck-embed" data-id="4fc31fb217744f0022002d83" data-ratio="1.44837340876945" src="//speakerdeck.com/assets/embed.js"></script>
