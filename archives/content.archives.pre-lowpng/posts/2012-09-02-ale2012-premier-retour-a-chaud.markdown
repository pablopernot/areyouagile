---
date: 2012-09-02T00:00:00Z
slug: ale2012-premier-retour-a-chaud
tags: ['conference','ale']
title: 'ALE2012 : Premier retour à chaud'
---

Emotionnellement fort, dérangeant parfois, enthousiasmant souvent, les 3
jours de ALE me ramènent à la maison vidé, imaginatif et plein d'idées
et de savoir (enfin... m'en donne l'impression). Mon premier retour à
chaud : mes "grandes impressions", je détaillerai les sessions/keynotes
et les apprentissages que j'ai pu en tirer dans de prochains posts.

[E viva ALE 2012 ! ](http://ale2012.alenetwork.eu/)

## Jettons sur le papier quelques mots

**Dérangeant :** dans une magnifique église, avec une des keynotes très
"chaplinesque"([the
dictator](http://en.wikipedia.org/wiki/The_Great_Dictator)), des gens
qui chantent ensemble, qui veulent changer le monde, bref... tout d'une
secte, ou pas loin par l'apparence. Attention ! Attention ! pas trop
d'évangélisme. (Mais nous sommes tous des "freaks" me glissent
certains...).

**Complexe et contradictoire :** 3 *keynotes*, chacune d'elles déclenchent des avis très marqués et très contradictoires. Jusqu'à la dernière minute. Une keynote de [Jim McCarthy](http://www.mccarthyshow.com/) (papa des [core protocols](http://www.mccarthyshow.com/download-the-core/)) très enflammée mais potentiellement dangereuse ([Chaplin, The Dictator](http://en.wikipedia.org/wiki/The_Great_Dictator)), une keynote de [Siraj Sirajuddin](http://blog.siraju.com/) très très haut dans les
nuages de la conceptualisation, trop prétentieuse ? (et surtout un speaker qui arrive le matin, et repart après la keynote -je ne supporte pas cela-), et au final à l'inverse une keynote de [Henrik Kniberg](http://blog.crisp.se/author/henrikkniberg) au ras du sol : pas de risque, pas de nouveauté, pas de prise de conscience du public auquel il s'adresse (c'est mon avis !). Malgré tout ces *keynotes* furent intéressantes : la preuve [Duarte Vasco](http://softwaredevelopmenttoday.blogspot.fr/), [Alexis
Monville](http://ayeba.fr/)et moi même en parlons lors du pot de départ : personne ne donne le même tiercé, personne ne place sa favorite sans y voir des défauts, personne n'exclu la moins bien sans lui trouver des qualités.

**Emotion :** Je ne sais pas ce qui se passe, mais l'émotion passe, prend et nous vide de force. La puissance de l'évènement va crescendo. une sorte de maëlstrom qui happe.

![music](/images/2012/09/IMG_20120830_235949-300x300.jpg)

**Déception :** Quelques présentations pas à la hauteur de l'évènement.
Des sujets trop évidents traités de façon trop formelle (des slides
présentés de façon négligée, c'est sans appel). Surtout le premier jour.

**Absence :** celle de [Olaf Lewitz](http://hhgttg.de/blog/). L'esprit
de ALE.

**Enthousiasme :** Des présentations marquantes : celles de Sergeï et
Jurgen, celle de Marc particulièrement mais aussi un peu en deçà celles
de Stephen, de Erin ou Ariadna. J'y reviendrai.

**Force :** De l'openspace -comme toujours-.

**Energie et gentillesse :** des organisateurs dans un lieu incroyable,
une organisation réactive, repas et déjeuner très agréable (et comme
cela compte !!). Bref une organisation et des organisateurs au top.

**Barcelone :** belle et riche (Gaudi, Picasso) mais je n'y vois pas la
crise !

**Doute :** une volonté affichée de vouloir aller plus loin avec le
concept ALE. L'idée est naturellement enthousiasmante, mais vouloir y
aller à marche forcée -comme je le ressens- ne me parait pas adéquat. On
ne change pas le monde en voulant le changer, on le change par
inadvertance (à mon avis).

**Anglais :** le perfectionner pour continuer le dialogue, écrire en :
doubler les articles de ce blog.

**Français :** J'y croise [Alexis](http://ayeba.fr/) et sa famille,
[Jean-François](http://www.jago.fr/agilessence/),
[Franck](http://blog.lookingforanswers.me/),[Oana](http://oanasagile.blogspot.fr/),
[Emmanuel](http://ut7.fr/papotes/), un Julien (?), et naturellement
notre code warrior/agile coach/scrummaster ce que vous voulez :
[Jérôme](http://blog.avoustin.com/). (au passage
[Jason](http://www.jasonrayers.com/) le coordinateur du studio de dev de
ALE 2012, découvre que je suis le "patron" -celui qui paye à la fin du
mois- et me glisse à propos de Jérôme : "bon sang qu'il est bon !").

![coders](/images/2012/09/IMG_20120830_174701-300x300.jpg)

**Code :** bon sang, une salle dédiée pour des codeurs acharnés. Mais où
va-t-on !

**Sourire :** de beaucoup de gens.

**Rêve :** beaucoup de rêveurs, suis-je un vieux con ?

**Famille :** j'ai pu venir avec ma femme et mes deux enfants. Ils ont
participés a des ateliers dédiés, ont profités des repas et des petits
déjeuners, ils sont sortis dans Barcelone avec d'autres (une quizaine
d'enfants). Leur retour est très positif (même si j'ai perdu à leurs
yeux les dernières parcelles de  crédibilité que j'avais encore).

Merci aux organisateurs !

## Des articles sur ALE par les autres frenchies

-   Alexis, sur le [premier
    jour](http://ayeba.fr/2012/08/ale2012-premier-jour/), sur le
    [deuxième jour](http://ayeba.fr/2012/08/ale2012-deuxieme-jour/), sur
    le [troisième
    jour](http://ayeba.fr/2012/09/ale2012-troisieme-jour/).
-   Oana :
    http://oanasagile.blogspot.fr/2012/09/ale2012-change-by-example.html\_
-   Jérôme : "two weeks later"
    : \`<http://blog.avoustin.com/ale-2012-two-weeks-later/%60_>

## Photos

-   https://plus.google.com/photos/118108856336032511715/albums/5783603068917470273?banner=pwa\_
-   http://www.flickr.com/groups/2086322@N21/\_

