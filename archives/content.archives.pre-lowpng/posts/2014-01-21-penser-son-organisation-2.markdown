---
date: 2014-01-21T00:00:00Z
slug: penser-son-organisation-theorie-des-catastrophes
tags: ['organisation','structure','energie','dynamique']
title: Penser son organisation - théorie des catastrophes
---

Jouons donc avec la théorie des catastrophes. Je ne sais si vous aviez
suivi l'[introduction](/2014/01/penser-son-organisation-introduction/),
mais on va imaginer une organisation dont l'apprentissage passe par un
jeu entre *storytelling* et auto-organisation, où la communication
d'informations passe par des formes du *double lien sociocratique* ; une
organisation organique (et pas rationalisée dans le sens industriel,
cartésien) qui prend des formes multiples (comme le cristal ou une
termitière). C'est là que j'en appelle à la théorie des catastrophes de
[René Thom](http://fr.wikipedia.org/wiki/Ren%C3%A9_Thom) : comment
saisir un peu les différentes formes et les différents mouvements que va
opérer cette organisation pour se déployer (puisque le sujet initial
c'est bien les organisations à grandes échelles). On pourra évoquer
plein d'autres points très concrets par la suite comme le télé-travail,
l'organisation des bureaux, le *off-shore*, etc mais là j'ai envie de
commencer une approche quelque peu théorique et risquée..

Risquée car la théorie des catastrophes est sujette à de nombreuses
critiques ; que je ne suis pas du tout du tout mathématicien je vais
donc peut-être faire de grosses erreurs ; que cela parait plus être un
jeu intellectuel qu'autre chose et -- cerise sur le gâteau -- je n'ai
strictement à ce jour aucune idée de l'application concrête de cette
réflexion à mon échelle opérationnelle.

Si vous voulez bien continuer malgré tout, il faut avant toute chose se
plonger un peu dans cette théorie qui a tant fait parler d'elle dans les
années soixante-dix pour presque disparaître depuis. Je vous recommande
de démarrer avec cette introduction à la [philosophie de la théorie des
catastrophes](http://download.springer.com/static/pdf/88/art%253A10.1007%252FBF03189162.pdf?auth66=1390324324_955943cbb6806678609b653c42cd5f75&ext=.pdf),
ou cette
[vidéo](http://www.canal-u.tv/video/universite_bordeaux_segalen_dcam/espoir_et_theorie_des_catastrophes_l_intelligibilite_norme_d_une_science_de_l_acceptable.3900),
finalement peut-être lire ce
[pdf](http://www.math.jussieu.fr/~chaperon/ChaperonUNESCO.pdf) assez
court, ou tout simplement cette
[page](http://virole.pagesperso-orange.fr/cata.htm) web.

## Il faut plutôt comprendre la théorie des catastrophes comme la théorie des changements de forme

Pour avoir quelques billes en main : une catastrophe c'est un évènement
qui surgit, un changement de situation : attractions, forces en présence
qui déclenche un changement de forme. René Thom distingue sept types de
catastrophes élémentaires, et des modèles associés. Une critique forte
contre cette théorie est justement ce qui m'y plait : son ouverture aux
sciences molles, on ne va pas trouver simplement des formules
(lesquelles je suis bien incapable de comprendre), mais aussi des verbes
pour représenter ces forces et changements en action.

Au lieu de catastrophes disons singularités : quand il se passe quelque
chose de singulier la situation change. Selon le nom de paramètres et de
variables, tant que nous restons dans un domaine élémentaire, Thom
indique sept singularités et les décrit, nous les verrons plus bas.
**Zeeman**, un adepte de Thom, propose de nombreuses applications de la
théorie des catastrophes : aux révoltes dans les prisons, au krach
boursier, etc, etc, au comportement du chien en colère ou apeuré. Donc
naturellement sociologie et psychologie se sont emparées de la théorie
des catastrophes, des changements de forme, et nous essayerons de
l'utiliser pour penser les mouvements et changements de forme d'une
organisation. On lit cependant souvent que l'application quantitative de
la théorie des catastrophes aux sciences humaines est un exercice
dangereux, vivons dangereusement.

## Que faut-il y chercher ?

Qu'est ce que je vais puiser dans cette théorie (je vous livre ma pensée
"à chaud" et peut-être que dans quelques mois cette piste se révélera
stérile) ?

-   Je vais y rechercher un ensemble de formes et de mouvements qui nous
    permettent de mieux percevoir l'organisation, et ainsi proposer un
    outillage alternatif à la vision hiérarchique, plate et mécanique
    que l'on nous offre habituellement.
-   Je vais y rechercher des verbes et des mots, des métaphores que l'on
    peut intégrer beaucoup plus facilement comme motifs organisationnels
    (vous verrez ci-dessous le "pli" ou "la fronce").
-   Je vais y rechercher une impression, une esquisse de tableau
    japonais, un côté contemplatif dans les grandes lignes des formes
    des organisations. Je ne tiens pas à retomber dans le travers d'une
    règle qui dit que si telle singularité se produit alors on obtiendra
    nécessairement tel type d'organisation, juste une compréhension
    d'ensemble des dynamiques en jeu. Pour citer Thom : "*cela va à
    l'encontre de la philosophie dominant actuellement, qui fait de
    l'analyse d'un système en ses ultimes constituants la démarche
    première à accomplir pour en révéler la nature. Il faut rejeter
    comme illusoire cette conception primitive et quasi cannibalistique
    de la connaissance, qui veut que connaître une chose exige
    préalablement qu'on la réduise en pièces*".

## Les catastrophes élémentaires et les organisations

![Théorie des catastrophes](/images/2014/01/catastrophes.jpg)

Dans le tableau à gauche qui provient de cette
[page](http://virole.pagesperso-orange.fr/cata.htm) web vous voyez les
sept catastrophes élémentaires. Chaque catastrophe, singularité, est
déclenchée par un certains nombre de paramètres et provoque certaines
variables, formes. On dit qu'il y a sept catastrophes élémentaires,
élémentaires car on considère peu de paramètres en entrée (de un à
deux). Vous observerez aussi-- dans le tableau toujours -- que l'on peut
adjoindre à chacune des catastrophes une interprétation spatiale et
temporelle (par le biais d'un verbe). Thom cherche délibérément -- en
accord avec les présocratiques : Héraclite et sa clique (j'ai osé) -- à
exprimer ses idées au travers de mots et d'images de la vie.

"*Nos modèles*, écrit Thom, *attribuent toute morphogenèse à un conflit,
à une lutte entre deux ou plusieurs attracteurs ; nous retrouvons ainsi
les idées (vieilles de 2 500 ans !) des premiers présocratiques,
Anaximandre et Héraclite. On a taxe ces penseurs de confusionisme
primitif, parce qu'ils utilisaient des vocables d'origine humaine ou
sociale comme le conflit, l'injustice ... pour expliquer les apparences
du monde physique. Bien à tort selon nous, car ils avaient eu cette
intuition profondément juste : les situations dynamiques régissant
l'évolution des phénomènes naturels sont fondamentalement les mêmes que
celles qui régissent l'évolution de l'homme et des sociétés, ainsi
l'usage des vocables anthropomorphiques en Physique est foncièrement
justifié*".

(amusez vous avec ces
[applets](http://l.d.v.dujardin.pagesperso-orange.fr/ct/fr_index.html)
(so 90') sur la théorie des catastrophes)

Par ordre de complexité croissante :

### Le pli

La première des catastrophes est le "pli". Imaginez un drap que vous
pliez. Il y a un "bout", une "fin". On y associe le verbe "finir", ou
"commencer", au pli, cela fini ou commence. Il y a un seul paramètre, on
est d'un côté ou de l'autre du pli, donc un seul résultat, une seule
variable de sortie. Si j'essaye de prendre une image : encore une fois,
c'est cela qui me plaît dans l'approche de Thom, sa volonté de revenir
au *présocratique* qui ramenaient leurs analyses mathématiques à des
images que l'on peut naturellement saisir. Ainsi donc comme image on
observe la communication au sein d'une équipe. Cette équipe grossit,
grossit, grossit : c'est le paramètre, on ajoute 1 membre à l'équipe
régulièrement. Soudain on atteint un point, la catastrophe, la
singularité, et la communication change complètement : c'est le pli. On
est passé de l'autre côté.

Dans les cas simples de **bord**, de **bout**, l'organisation opère un
**pli**, mais un pli net. Il se produit une singularité cela déclenche
un résultat (le pli).

### La fronce

La fronce c'est le pli du tissu dans votre rideau de douche. Mais ce pli
est une courbe. On imagine une bille qui passe d'un côté ou de l'autre
d'une petite colline. Tout dépends des paramètres qui propulse la bille
: elle passera vite ou montera pour redescendre sans passer le rubicon.
C'est avec la catastrophe de la fronce que Zeeman applique la théorie au
chien : entre peur et agressivité : quels paramètres et selon quelles
forces le chien va passer de la peur (la fuite) à l'agressivité
(l'attaque). On entre dans l'aspect mathématiques de la théorie, et je
suis incapable de le maîtriser. Mais je peux m'interroger sur les forces
et les attractions qui me font évoluer sur la fronce et basculer d'un
côté ou de l'autre.

Par exemple : la fronce à deux paramètres en entrée, on pourrait dire
encore la taille d'une équipe, et disons sa co-localisation. C'est à
dire on a plus ou moins de personnes dans une équipe, et ils sont plus
ou moins co-localisés. Avec ces différents paramètres et leur intensité
on décrira une fronce plus ou moins abrupte (une variable en sortie) qui
décrira le fonctionnement de la communication au sein de l'équipe.
Va-t-elle doucement s'effriter, va-t-elle subitement se briser, à quel
moment doit-on engendrer une nouvelle équipe.

Les mots clefs sont **capturer**, **casser** : on franchit le sommet de
la **fronce**, en passant de l'autre côté : on **engendre**, on
**devient**, on **unit**.

Je parle d'équipe et de communication, vous pouvez envisager l'expansion
des filiales au travers d'un continent, les différents départements
d'une solution, la vie d'un produit d'entreprise, etc.

### La queue d'aronde (d'hirondelle)

Penser à la queue d'une hirondelle : au croisement (comme je vous le
disais vous pouvez toujours jouer avec des applets comme [celle-ci sur
la queue
d'aronde](http://l.d.v.dujardin.pagesperso-orange.fr/ct/fr_elem_aronde.html),
ils sont vraiment très utiles), il y a une sorte de superposition, que
l'on peut percevoir comme un **déchirement** ou une -- au contraire--
une **couture**. Il y a trois paramètres en entrée et une variable en
sortie (j'écris cela pour ceux qui y comprennent quelque chose).

On évoque donc des problématiques de superposition, de recouvrement, de
tension, de déchirement au sein de l'entreprise. Deux gammes produit qui
se recouvrent, doit-on les fusionner, les coudre ? A quel moment ce
produit, cette gamme, se **déchire** en deux (idem pour les équipes,
encore). Je ne vais pas aujourd'hui creuser plus et je vous laisse
trouver les paramètres qui vont régir ce changement de forme, avec la
queue d'aronde il doit y en avoir trois.

### Le papillon

Je vais désormais me limiter aux verbes associés, les formes deviennent
compliquées et je n'ai pas encore assez intégré le modèle associé... Ce
qui est intéressant dans le papillon (quatre paramètres ! et toujours
une variable en sortie) c'est l'idée d'une surface cachée, recouverte,
une **poche**, qui **s'écaille**, **s'exfolie**, c'est à dire
s'effeuille par lamelles. Donc elle se **vide** ou se **remplit**.
Encore une métaphore, mais aussi donc une théorie permettant
d'appréhender les mouvements de l'organisation.

Les trois dernières catastrophes élémentaires sont un peu plus complexes
car elles proposent deux variables en sortie.

### La vague, le poil, le champignon

Pareil, pour l'instant j'ai l'impression qu'il devient vain pour moi de
comprendre les aspects mathématiques et le calcul de ces différentes
formes. Je m'intéresse uniquement aux verbes qui peuvent me donner une
façon d'appréhender cette théorie et de la projeter sur mon monde.

-   La vague (ombilic hyperbolique) : Lisez **briser**, **s'effondrer**,
    **recouvrir** pour la **vague**. Pensez au rachat de filiale, au
    recouvrement de secteur, mais aussi là l'effondrement soudain d'un
    vague ayant atteint sa **crête**, attrapez la notion de **voute**.
-   Le poil (ombilic elliptique) : **Piquer**, **pénétrer**,
    **boucher**.
-   Le champignon (ombilic parabolique) : **Briser**, **éjecter**,
    **lancer**, **percer**, **couper**, **lier**, **ouvrir**,
    **fermer**.

### Quand à savoir à quoi cela peut servir

Je n'en sais encore fichtrement rien sur le plan opérationnel. Mais
j'apprécie me voir équipé d'un glossaire pour décrire les mouvements et
changements des entreprises. De ce glossaire je peux tirer des
enseignements, des intuitions et une façon de comprendre le monde et la
vie des organisations. Une vision beaucoup plus réelle et efficace qu'un
tableau de chiffres et/ou une hiérarchie. Car il s'agit bien là de
verbes, d'actions, de mouvements. Si j'en crois Thom, et si sa théorie a
été sacrément secouée, elle n'a pas été anéantie et commence à jouïr
d'un renouveau de considération, il s'agit bien là de la base de toutes
formes de changements dans la nature. N'est-ce pas hypnotisant ?

On parle beaucoup d'**holacratie**, je n'ai pas encore goûté ses joies,
mais c'est typiquement le genre de contexte où cette approche peut
prendre tout son sens : un guide sous forme de verbes et d'images de la
nature pour comprendre et suivre la dynamique d'une organisation, ses
changements de formes.

### Série sur les organisations

0 - prémisses : [Agile à grande échelle : c’est clair comme du
cristal](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)

1 - penser son organisation :
[introduction](/2014/01/penser-son-organisation-introduction/)

2 - penser son organisation : [théorie des
catastrophes](/2014/01/penser-son-organisation-theorie-des-catastrophes/)

3 - penser son organisation : [sur le
terrain](/2014/02/penser-son-organisation-sur-le-terrain/)
