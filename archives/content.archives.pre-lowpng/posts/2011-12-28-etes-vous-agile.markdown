---
date: 2011-12-28T00:00:00Z
slug: etes-vous-agile
tags: ['agilite']
title: Êtes vous agile ?
---

Lors de la [session Agile &
CMMi](http://www.areyouagile.com/2011/10/agile-cmmi-potion-magique-ou-grand-fosse/)
à Toulouse (avec @yassinezakaria) quelqu'un m'a interpellé. Je venais de
présenter les 5 niveaux de maturité de agile tel que définis par Scott
Ambler. Le premier niveau étant l'"agile réthorique". L'agile
"marketing" si vous préferez, celui qui fait vendre, peut importe ce qui
se déroule ensuite. Une personne m'a donc interpellé : "comment savoir
si je fais du *bon* agile ou de l'agile *marketing* ?". Pris dans la
session et surpris par cette question je n'ai pas su répondre avec
précision. La réponse m'a semblée trop évidente. Et pourtant elle était
loin de l'être.

Cette question interroge d'abord sur notre faculté à auditer l'agile.
Est-ce possible d'auditer une approche qui se veut auto-organisée, en
amélioration continue, dans un monde complexe où il n'est pas aisé de
reproduire des schémas pré-établis ?

Qu'est ce qui défini aujourd'hui une bonne approche agile, d'une
mauvaise ? Peut-on auditer une approche agile ? Même si certaines
connaissances autour de moi s'y refusent, je ne partage pas leur avis.
D'abord mettons nous d'accord sur le terme "auditer". Je me base sur la
définition de wikipedia (elle me convient) : "une activité de contrôle
et de conseil qui consiste en une expertise par un agent compétent et
impartial et un jugement sur" ... Pas nécessairement une note ou un rang
ou une certification, mais un jugement, un avis. Exactement comme dans
le cadre d'une rétrospective, ou d'un exercice d'amélioration continue.
Si j'ai cet avis je suis capable de dire à la personne qui
m'interrogeait (et naturellement si je prends le temps de faire un
audit) de lui donner un jugement, un avis.

Comment donc auditer une organisation agile, un groupe agile, un projet
agile ?

On peut se baser sur les 3 niveaux fournis par les différentes méthodes
et l'agilité de façon plus générale :

## Niveau 1, les valeurs du [manifeste](http://agilemanifesto.org/) et des différentes méthodes

Interaction, capacité au changement, collaboration etc. auxquels si nous
sommes dans un environnement scrum on ajoute : transparence, inspection,
adaption ; ou si  nous sommes dans un environnement XP : respect,
courage, feedback, communication, simplicité, ou les deux. On peut aussi
intégrer à ce niveau les valeurs du Lean : respect des gens,
amélioration continue.

## Niveau 2, les principes du manifeste

par exemple : "Simplicity--the art of maximizing the amount of work not
done--is essential.", ou "The most efficient and effective method of
conveying information to and within a development team is face-to-face
conversation", ou des principes du Lean: See the whole, eliminate waste,
flow, pull, etc.

Chacun des principes découlent des valeurs, c'est une mise en oeuvre de
celles-ci. Les valeurs priment donc sur les principes.

## Niveau 3, les pratiques des différentes méthodes

Le daily standup de scrum ou XP, le code refactoring de XP, la
rétrospective de Scrum, la limitation du travail en cours de Kanban,
etc.

Chacune des pratiques découlent des principes et des valeurs. Les
valeurs priment donc sur les principes, qui priment eux mêmes sur les
pratiques.

Tout devrait vous ramener aux valeurs, le lien avec celles-ci dans
chacune de vos activités agiles doit être permanent. Peu importe que
vous détourniez, transformiez, inventiez des pratiques si tant est que
vous respectiez les valeurs agiles auxquels vous vous réferez. Celles-ci
peuvent être réduites à votre seule discipline : Scrum : transparence,
inspection, adaptation ; XP : feedback, respect, courage, simplicité,
communication. Mais celles-ci sont atomiques. On n'imagine pas faire de
l'adaptation sans inspection, on n'imagine pas une inspection sans
transparence.

C'est sur ces points que l'audit agile devient complexe. Il m'est facile
de dire si un *daily scrum* se déroule correctement, ou le *sustainable
pace* de XP est respecté. Il est autrement plus compliqué de dire si une
organisation, un projet est transparent ou non, si une équipe a été
courageuse ou non, si il y a eu assez de respect ou non.

Pour les pratiques je sais, j'ai un savoir ; aussi bien subjectivement
qu'objectivement je suis capable de juger si les gens respectent ou non
les pratiques du *daily scrum* ou du *sustainable pace*.

Pour les valeurs (je laisse les principes : ils se situent entre les
deux) j'ai une opinion et non pas un savoir. Je peux difficilement
affirmer de façon subjective et objective si la valeur de transparence
est respectée, mais j'ai mon opinion. (Sur ces approches, avec le
*savoir* et l'*opinion*, il faut ajouter la *foi* et vous aurez les 3
degrés d'assentiment décrits par *Kant* dans sa [critique de la raison
pure](http://fr.wikipedia.org/wiki/Critique_de_la_raison_pure)).

Pour formuler une opinion j'opère comme le fait Comte-Sponville dans son
"[esprit de l'atheisme](http://www.amazon.fr/Lesprit-lath%C3%A9isme-Introduction-spiritualit%C3%A9-sans/dp/2226172734)"
lorsqu'il s'interroge sur l'existence de Dieu (il fallait bien cela en
ces jours de fête ou tout le monde se baffre à la mémoire d'un petit
gars soit disant né dans une étable) : avec des arguments négatifs. Je
ne sais pas si vous êtes agile, mais je crois détecter quand une chose
ne l'est pas, j'ai mon opinion là dessus. J'ai une opinion sur les
choses qui ne me paraissent pas agiles, en cela je me réfère constamment
aux valeurs de l'agilité ou des méthodes que vous déployez (XP, Scrum,
Lean, etc.). Mon opinion est basé sur ma connaissance (et donc mon
expérience).

J'ai pour opinion que lorsque les tâches des membres d'une équipe sont
affectées nominativement avec une durée précise et très contraignante
par une personne extérieur au groupe nous ne sommes pas agiles car nous
ne respectons pas les personnes, ni leur faculté de s'auto-organiser.
J'ai pour opinion que si une équipe se compose uniquement d'un certains
types de profils dits techniques et qu'ils travaillent en autarcie l'un
des objectifs de l'agile n'est pas atteint en ce qu'il préconise de la
collaboration au travers de différents profils qui doivent apprendre à
travailler ensemble. Le second exemple me parait moins "grave" que le
premier car il enfreint moins de valeurs et de principes. (Si on me
demande de mettre des "notes" à ces opinions j'en suis capable, comme je
suis capable de donner un tarif avec des chiffres à ces missions, et
comme mon banquier me réclame une certaine somme en fin de mois pour ma
maison : les chiffres ne sont qu'une convention).

Avoir une opinion sur votre agilité ? observez vos processus, votre
principes, vos valeurs relativement à ceux et celles proposés par
l'agilité. Essayez de vous projeter par "opinion négative". Avant toute
chose il faut être au courant de ce qu'est l'agilité (c'est probablement
pour cela que la question de l'auditeur (tiens un autre sens au même
mot) de la session Agile/CMMi m'a d'abord surprise).

Pour finir,

Les valeurs sont peu nombreuses, pour essayer de faire une métaphore :
 elles forment des piliers massif d'un immense hall où vous pouvez à
loisir vous organiser comme bon vous semble. Ces valeurs sont les seules
contraintes mais elles sont indispensables, sans elles le hall
s'effondre. Si vous respectez ces piliers, libre à vous de faire comme
il vous chante, et c'est cette liberté qui est source de richesse et
d'innovation, de progrès. Dans l'agile on parlera de contraintes
libératrices (je crois que Thierry @thierrycros qui m'a soufflé
l'expression qui vient de Paul Valery) : "contraintes libératrices dira
le poête comme le géomètre, "les oeuvres à grandes contraintes exigent
et engendrent la plus grande liberté d'esprit" \[[pioché
ici](http://books.google.fr/books?id=qL57XdCb42IC&pg=PA94&lpg=PA94&dq=paul+valery+contraintes+lib%C3%A9ratrices&source=bl&ots=JB8Rhrxj1I&sig=5XH5whAJwul1VxlaPEFesidTSV8&hl=fr&sa=X&ei=aff6Tuv7Io_AtAab763nDw&ved=0CCcQ6AEwAQ#v=onepage&q=contraintes&f=false)\].

Voici donc un complément à ma réponse : pour bien appliquer l'agile ayez
des contraintes fortes liées aux valeurs ou aux principes ou aux
pratiques de vos méthodes agiles et respectez les impérativement. Les
valeurs sont indispensables, faites votre choix dans les principes et
les pratiques mais attention l'atomicité est très importante.

Quelques exemples pour me comprendre cette notion de "contrainte" :

-   Scrum : Le product owner est responsable du backlog, des priorités
    et de la validation. Est-il **vraiment** responsable ?
-   XP : Appropriation collective du code : tous les developpeurs
    peuvent-ils agir sur toutes les parties du code, même celles qu'ils
    n'ont pas écrites ? Le peuvent-ils **véritablement** ?
-   Amélioration continue : y-a-t-il une **véritable** réflexion sur
    l'amélioration continue avec de **véritables** actions prises ?

D'où aussi l'importance prédominante des moments d'interrogations ,d
'analyses et d'amélioration. La rétrospective est l'élément phare et
révélateur d'une pratique agile.

Naturellement on n'est jamais totalement  accompli , on tend vers
l'agile, vers une application idéalisée de toutes les valeurs.
