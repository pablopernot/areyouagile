﻿---
date: 2019-05-14
slug: seminaire-leadership
tags: [seminaire, leadership, olaf]
title: Séminaire Leadership avec Olaf Lewitz
--- 

**Lundi 8 et mardi 9 juillet 2019** nous organisons avec [Olaf Lewitz](https://trustartist.com/) deux journées de séminaire sur le *leadership*, avec comme cadre l'académie *trust temenos* que Olaf mène depuis quelques années. Je croise Olaf depuis 2011 au travers de conférences, de séminaires, de visites entre amis. Nous nous apprécions mutuellement. J'ai pu faciliter à ses côtés faciliter l'Open Space du Scrum Gathering de Prague en 2015 par exemple (600 personnes !). Je suis sûr aujourd'hui qu'il a beaucoup à amener à nos managers, nos products managers, nos coachs. Et j'espère qu'à nous deux nous saurons proposer une forte valeur à nos participants.  

Le séminaire sera en anglais et en français.  

Nous croyons que l'apprentissage le plus précieux et le plus durable se produit dans les interactions humaines, et non dans le temps que vous passez à vous asseoir et à nous écouter. Nous visons plutôt à maximiser la profondeur et l'étendue de l'expérience et à fournir des modèles mentaux pour donner un sens à ce que vous découvrez.

	"Ce cours permet d'allier l'éthique agile - respect des personnes, équipes dynamiques et diversifiées, apprentissage rapide - à une vision éclairée et plus large des cadres de leadership. Il fournit le bon tissage de ces cadres sans être pédant du tout. "

	(Scott Downs, Londres)


![Olaf et Pablo](/images/2019/05/olaf-et-pablo.jpg)

* Je veux être mis au défi et découvrir quel genre de leader je suis.
* Que signifie le leadership pour moi ?
* Quel genre de leadership m'aidera à réaliser ce que je veux ?

Ce cours vise à créer un conteneur pour la diversité et, en quelque sorte, l'ambiguïté. Comment voulez-vous vous améliorer ? "Agile" est la position et le cheminement de l'amélioration consciente comme l'exprime Olaf. Dans cet esprit, nous croyons que le leadership agile est l'amélioration consciente de notre façon de diriger et de suivre. Une clarté de l'intention, une capacité à développer les systèmes dans lesquels nous travaillons et les personnes qui les composent, pour répondre aux besoins de chacun et pour réaliser notre potentiel individuel et collectif. 

	"J'ai appris ce que le leadership signifie vraiment."

Ce séminaire vous invite à donner un coup de pied dans votre parcours personnel. Un changement dans votre organisation mais aussi un changement personnel.

![Séminaire](/images/2019/05/seminaire.jpg)

### Vous repartirez avec 

* Commencez votre voyage vers votre prochain niveau de leadership
* Plus de clarté sur qui vous êtes et ce que vous voulez
* Meilleure connaissance de la façon de gérer les contextes et d'établir des relations.
* Mettre clairement l'accent sur un défi grandissant pour les six prochains mois, avec une idée claire des progrès accomplis.
* Un ensemble de principes qui s'appliquent à votre vie, à votre équipe et à votre organisation
* Expérimenter les effets d'un conteneur de confiance, amener les gens à apporter leur vulnérabilité et leur diversité pour créer une force de changement.
* Savoir ce que l'on ressent quand on est co-leader en tant que NOUS 
* Une longue liste d'inspirations et de recommandations :-)
* De nouveaux amis ! 

### À quoi s'attendre ?

Grandir en tant que personne, grandir dans ses relations. 

Nous minimisons le temps que vous passez à vous asseoir et à nous écouter. Nous maximisons la profondeur et l'étendue de l'expérience et fournissons des modèles mentaux pour donner un sens à ce que vous découvrez.

Nous pourrions vous demander de prendre l'initiative sur un sujet de la formation. Nous avons l'intention de nous défier les uns les autres dans le cours, indépendamment du temps que vous avez passé dans le contexte Agile. Nous voulons créer le contenu avec vous tous. C'est un art de savoir quand diriger et quand suivre. NOus essayerons d'avoir un mouvement gracieux entre les rôles et les responsabilités, la décision de ce qu'il faut faire dans quel contexte et dans quelle situation, cela viendra s'ajouter à votre expérience.

Le premier jour, l'accent sera mis sur votre voyage personnel. Le deuxième jour, nous appliquerons nos apprentissages et nos principes, ainsi que notre modèle et d'autres outils, au cadre des organisations agiles. Soyez prêt à être surpris et à relever des défis. Vous pouvez apporter votre zone de confort, mais vous n'y passerez peut-être pas beaucoup de temps :-)

### C'est pour qui ?

* Cadres dirigeants, *top executives*, managers et leaders de tous niveaux
* Scrum Masters, Responsables de produits, Coachs Agiles
* Chefs de projet et chefs d'équipe 


### Nos participants ont dit lors des autres sessions avec Olaf 

	"Je pense que l'impact principal de ce cours est sur ma propre intégrité et mes priorités internes. Je ne vois pas comment on peut se conduire sans d'abord régler ce problème. Et, bien sûr, vous ne pouvez pas diriger les autres si vous ne pouvez pas vous diriger vous-même. Vous avez vu les résultats de l'enquête V1 au fil des ans, la culture étant le principal obstacle, n'est-ce pas ? Qui est responsable de l'établissement de la culture ? - leaders. Donc, je dirais que chaque fois que j'ai un problème avec les adoptions agiles, j'ai affaire à du leadership. Ce cours pourrait être le début d'un changement pour vous. "De cette façon, ça pourrait vraiment faire une différence." 
	(Kirill Klimov, Singapour)

---

	"Sans surprise, il n'existe pas de guide officiel, étape par étape, du leadership agile". Il y avait, cependant, un espace " sûr " immaculé créé pour apprendre un tas de techniques, de modèles et de façons de penser de grande qualité, soutenus par l'expertise et l'expérience pratique du formateur. L'apprentissage a été renforcé par la participation du groupe, qui est à la fois un crédit pour les participants et un crédit pour la capacité des formateurs à obtenir le meilleur d'eux. TrustTemenos CAL1 est un cours de leadership pour ceux qui ont besoin de diriger et d'influencer ; ce n'est pas un résumé des cadres agiles. Il serait tout à fait impraticable et préjudiciable pour le cours d'essayer d'y parvenir. Si vous souhaitez en savoir plus sur les détails spécifiques d'un framework agile, prenez des cours supplémentaires qui sont spécifiquement conçus à cet effet."
	(Ray Whiting, Londres)


---

# Informations

* Date : lundi 8 et mardi 9 juillet 2019
* Prix : de 600 à 900 euros selon les cas de figures (jusqu'à fin mai 600 euros, jusqu'au 15 juin 750 euros, ensuite 900 euros).
* Lieu : beNext, 12 rue Vivienne, 75002 Paris
* Petits déjeuners et repas midi inclus

[**ACHETER UNE PLACE**](https://www.weezevent.com/seminaire-leadership-avec-olaf-lewitz)

---

![Trust Temenos](/images/2019/05/trusttemenos.jpg)


