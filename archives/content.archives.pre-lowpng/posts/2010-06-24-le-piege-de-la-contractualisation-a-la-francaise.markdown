---
date: 2010-06-24T00:00:00Z
slug: le-piege-de-la-contractualisation-a-la-francaise
tags: ['contrat','agile','jeu','seriousgame','workshop','atelier']
title: Le piège de la contractualisation à la française
---

L'année dernière j'ai été fasciné par les résultats de l'exercice
proposé par [David Barnholdt concernant la réalisation de
spécifications](http://blog.crisp.se/davidbarnholdt/2009/02/18/1234986060000.html)
versus le détail de la demande fournie. David encadre deux groupes de
travail, à chacun il donne 1 mn pour réaliser les spécifications
suivantes,  au premier groupe il demande : "*dessiner une prairie
durant une belle journée d'été, avec des fleurs bleues et rouges dans
l'herbe verte, quelques vaches et quelques oiseaux sous un éclatant
soleil*". Au second  il demande : "*dessiner une prairie durant une
belle journée d'été avec : 10 fleurs bleues avec 5 pétales chacune, 5
fleurs bleues avec 6 pétales chacune, 13 fleurs rouges avec 6 pétales
chacune, 2 vaches avec 3 tâches noires, 1 vache avec 5 tâches noires, 2
vaches avec 4 tâches noires, 2 oiseaux dans le coin gauche en haut, 3
oiseaux au milieu, le soleil à droite avec 5 rayons"*. Le résultat est
stupéfiant mais on aurait du s'en douter :

![Open Ended ?](/images/2010/06/openended1-225x300.jpg)

![Open Ended ?](/images/2010/06/openended2-300x225.jpg)

Les commentaires sont sur le blog de [David Barnholdt](http://blog.crisp.se/davidbarnholdt/2009/02/18/1234986060000.html)
mais on aura compris...

Mon propos est d'extrapoler ce jeu de causes et effets au niveau de la
contractualisation des projets. On pourrait dire que l'on reste dans le
même domaine: expression des besoins (spécifications) que j’étends à la
phase précédente cahiers des charges, etc. et donc de la
contractualisation.

En France la culture du forfait avec un périmètre très défini est forte,
encore plus dans le cadre des appels d'offres publics. C'est à mon avis
l'une des causes majeures des échecs ou des difficultés ressentis dans
les projets informatiques.

D'une part tout le monde sait bien qu'il est impossible de connaitre
100% de son périmètre à l'avance (si vous lisez Popendieck elle
dissémine quelques anecdotes "rigolotes" dont celle des "marines"
(prononcez "marines" avec un cigare dans la bouche) : lorsqu'ils
déclenchent une attaque ils connaissent 70% de leur plan, ils savent que
30% de ce qui va arriver n'est pas "prédictible"). Donc s'engager,
payer, contractualiser sur quelque chose d'inconnu ou pire, de faux, est
une lourde erreur, lourde de conséquences.

D'autre part, non content de ne pouvoir définir 100% du périmètre par
avance, le client, le demandeur, a toujours la crainte de se voir
bafouer, flouer : il va donc sur-spécifier (c'est là que je rebondis sur
l'exercice évoqué précédemment). Il va en demander des tonnes pour être
sûr d'en avoir "pour son argent". Et cela va se révéler contre-productif
: la "sur-spécification", la spécialisation, va probablement entrainer
des développements à façon qui, si ils vont répondre très précisément à
un point, vont interdire et limiter fortement tous les autres aspects
fonctionnels ou techniques.

On ne peut pas les blâmer, les SSII ont pour vocations de faire des
bénéfices et quand il y a contentieux, c'est le cahiers des charges ou
les spécifications qui font foi, donc très rapidement. Bref, c'est une
lutte, une guerre de tranchées, plutôt qu'une collaboration. Et c'est le
serpent qui se mord la queue, car plus les SSII vont jouer bêtement leur
rôle, plus les clients vont sur-spécifier.

Difficile aussi de trouver une voie de sortie : si un client fait
l'effort de ne pas "en rajouter" la SSII va prendre cela pour un effet
d'aubaine et se jeter sur une marge probable à dégager. Je reviens à
l'agilité qui est -là encore- la meilleure solution, itération,
collaboration, enfin du bon sens. On doit rompre avec une
contractualisation basée sur un périmètre complet pré-défini.
Aujourd'hui dans le cadre des marchés publics c'est malheureusement
quasiment impossible.

Deux maximes à méditer : (faîtes OooommmM) "le mieux est l'ennemi du
bien", "All failure comes from misunderstanding" ("tout échec vient
d'une incompréhension", the wheel of life, truc tibétain, 500 ans av JC)
