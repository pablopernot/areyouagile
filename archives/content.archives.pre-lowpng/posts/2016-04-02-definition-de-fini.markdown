﻿---
date: 2016-04-02T00:00:00Z
slug: nul-nest-cense-ignorer-la-definition-de-fini
tags: ['definitiondefini,','dod']
title: Nul n'est censé ignorer la définition de fini
---

La *définition de fini* c'est l'ensemble des critères que l'on se donne
pour considérer en agile que les éléments sur lesquels on travaille sont
finis, ou pas. Le fini est essentiel, car il permet d'avoir quelque chose
de concret. on sort de l'hypothèse, on a quelque chose que l'on peut
vendre, donner, proposer, essayer, etc, on va apprendre. Le feedback est
toujours intéressant, même avant le "fini", mais naturellement avec le
fini il prend beaucoup plus de valeur. Et le fini nous permet d'aller
chercher du feedback ailleurs (utilisateurs finaux, dehors dans le monde
extérieur, etc.). Le fini est aussi binaire, c'est fini ou ce n'est pas
fini. Cette polarité est franche et permet une vraie lecture simplifiée
des réalisations en cours (ça, on l'a, ça on ne l'a pas), et ouvre vers
des arbitrages plus judicieux. Avec cette notion stricte de fini (ou
pas), on évite les "on a 80% de cette fonctionnalité et 70% de celle-ci"
ce qui, vous le savez tous, ne veut rien dire dans la vraie vie. Quand
les gens nous disent "on a 80% de cette fonctionnalité" bien souvent, ils
expriment l'idée que 80% du temps alloué sur la réalisation de cette
fonctionnalité a été consommé. Mais rien n'indique que les 20% restant
ne prendront pas plus que les 80% précédent. Et donc on souhaite du
feedback régulièrement et on souhaite des choses finies régulièrement
pour apprendre, vendre, donner, essayer, etc, le plus tôt possible. Et
donc on retombe dans un besoin d'exprimer des choses finies qui font
sens (sur le plan fonctionnel, usuel, métiers, commercial) avec des
éléments assez réduits (taille, effort de réalisation). Mais ce n'est
pas le sujet de cet article [mais plutôt de
celui-ci](/2015/09/trois-apprentissages-clefs-dune-approche-agile/).

Je reviens à cette *définition de fini* qui est un élément difficile,
peut-être le plus difficile, dans la sphère agile pour les équipes,
quelle forme prend-elle, que contient-elle, à quoi faut-il être
vigilant ? ...selon moi.

-   La *définition de fini* va nous permettre de clarifier notre état
    des lieux projets : ça, on l'a, ça on l'a pas.
-   La *définition de fini* va nous permettre de délivrer et d'apprendre
    : avant on ne sait pas vraiment, quand c'est fini, je délivre et
    j'apprends.
-   La *définition de fini* va nous permettre d'harmoniser nos points de
    vue et contraintes sur ce que "fini" veut dire.

## Accessibilité et intelligibilité de la définition de fini

Pourquoi la *définition de fini* est à mes yeux le plus difficile
élément d'une équipe agile ?

-   Car il faut la respecter sinon à quoi bon l'écrire.
-   Car c'est finalement le niveau de qualité que l'on se donne, c'est
    donc essentiel.
-   Car elle touche différents niveaux : les tâches ? Oui. Les user
    stories ? Oui. Les fonctionnalités ? Oui. Les *releases* ? Oui.
    L'architecture ? Oui. Le *design* ? Oui. Les méthodes et processus ?
    Oui. La documentation ? Oui. La charte ? Oui. etc.
-   Car elle doit faire sens dans une organisation : si la *définition
    de fini* n'est pas un peu liée au milieu auquel elle appartient, à
    quoi bon ? Plus clairement et en prenant un exemple de projet
    informatique : si votre *définition de fini* ne tient pas compte de
    la charge en débit réseau quand vous intégrez votre travail fini
    dans le socle commun ce manquement devient une hérésie. La
    *définition de fini* d'un projet, d'un produit, d'une équipe ne doit
    pas ignorer son écosystème.
-   Car elle doit faire sens pour l'équipe : inutile de donner une
    *définition de fini* à une équipe dont on sait pertinemment qu'elle
    ne pourra pas le respecter. Inutile de demander à une équipe au
    travers la *définition de fini* de respecter des temps de réponses
    qu'elle ne pourra pas respecter (pour X raisons). Vous lui apprenez
    tout simplement à NE PAS respecter la *définition de fini*. Et donc
    tout simplement à ne rien respecter.
-   Et cerise sur le gâteau à laquelle je tiens beaucoup : la
    *définition de fini* doit être claire, partagée, toujours dans la
    tête de chacun, pour cela je demande à ce qu'elle ne dépasse pas ...
    9 éléments. En accord avec le chiffre magique de 7 (-2/+2) dont
    l'[Université de Princeton nous dit depuis 50 ans que c'est le
    nombre d'objets pouvant tenir dans la mémoire de travail d'un
    humain](https://fr.wikipedia.org/wiki/Le_nombre_magique_sept,_plus_ou_moins_deux).
    Pour que chacun soit intransigeant et clair avec la *définition de
    fini* elle doit être constamment présente en tête. **Nul n'est censé
    ignorer la définition de fini**. Personnellement je comprends toutes
    ces définitions : de prêt, de fini, de *release*, de production,
    etc, que je vois fleurir. Je comprends. Intellectuellement elles
    font sens. Mais à mes yeux je préfère la rigueur, la clarté induite
    par une définition courte, réduite à l'essentielle, qu'à la
    complétude intellectuelle de toutes ces définitions diverses et
    variées (je ne vous parle pas même pas des plans qualité avec des
    définitions de fini sur plusieurs pages, je tire mon chapeau à ceux
    qui les respectent ou même les lisent). Avoir la loi en tête, le
    cadre accessible et intelligible renforce l'autonomie et
    l'implication de l'équipe.

Donc mes deux règles qui supplantent les autres, car elles vont bâtir
notre comportement, notre culture, par rapport à cette notion de fini et
de qualité :

-   Pas plus de 9 éléments, car personne ne peut ignorer son importance,
    son existence et son contenu, nul ne peut ignorer le niveau
    d'exigence que nous attendons. Et son accessibilité, son aspect
    compact vont faciliter notre autonomie et notre implication à son
    égard.
-   Elle doit être atteignable par l'équipe. Il est impensable
    d'apprendre à l'équipe à ignorer ou ne pas respecter la *définition
    de fini*. Si cela n'est pas possible : l'équipe ne peut pas
    respecter la *définition de fini*, le produit ne pas être viable
    sans cette *définition de fini*, une vraie réflexion doit avoir lieu
    : changement d'équipe ? Changement de produit ?

## Que contient-elle ?

Si je devais prendre un cahier des charges classiques et que je devais
l'adapter à un contexte agile je dirais : prenez tous les éléments
fonctionnels, métiers, et placez-les dans votre *backlog*, dans votre
expression du besoin, prenez tous les autres éléments et placez les dans
la *définition de fini*. On va y trouver des éléments techniques : il
faut utiliser tel ou tel composant, ou outil ; on va y trouver des
éléments de performances : cela doit marcher avec un million
d'utilisateurs en simultané, avoir de la haute disponibilité, etc ; on
va y trouver des éléments de méthodes : toutes les tâches réalisées
doivent être revues par un autre membre de l'équipe, la validation se
fait sur la plate-forme d'intégration, etc.

Mais cette liste est réduite à 7 (+2/-2) éléments. Ce n'est pas SI
compliqué que cela. Plein de choix sont implicites et non pas besoin
d'être intégré à la *définition de fini*. J'estime que vous avez deux
options : soit ne pas réduire la liste, et qu'elle ne soit pas vraiment
respecter. Soit vous faire un peu mal et la réduire à 9 éléments et que
ceux-là au moins ne soient pas oubliés.

## Qui décide de ce qu'elle contient ?

Hummm. Ben... tous les gens concernés par ce qui va être produit. Avec
un arbitrage du métier, du *product owner*, et un accord sur la
faisabilité par l'équipe. Encore une complication donc.

## Quelle forme prend-elle ?

Une simple liste affichée au mur : nul n'est censé ignorer la
*définition de fini*. Donc de 5 à 9 éléments (je vous rappelle que c'est
mon point de vue et qu'il est minoritaire).

Voici un exemple sur un projet informatique (et elle doit être discutée
à haute voix avec l'équipe et le *product owner* pour bien délimiter ce
que les mots expriment):

-   doit fonctionner sur firefox,chrome et IE
-   doit fonctionner avec *Elastic Search*
-   doit fonctionner avec un million d'utilisateurs simultanés
-   toutes les pages web doivent posséder une aide en ligne
-   toutes les tâches sont revues par un autre membre de l'équipe
-   toutes les validations ont lieu sur la plate-forme de préproduction
-   tous les éléments développés possèdent des tests automatisés
-   rien d'inutile n'est présent

Vous voyez malgré tout que des choses doivent être précisées (tests
automatisés ? etc.). Mais c'est clarifié au sein de l'équipe. Cette
liste est affichée et doit être respectée pour que les éléments (tâches
? Fonctionnalités ? User stories ? Release ?) soient jugés "fini".

## Peut-elle évoluer ?

À vos risques et périls. Si vous dîtes que ce n'est pas un million
d'utilisateurs simultanés, mais finalement dix, vous comprenez que tout
ce que vous avez jugé "fini" auparavant est potentiellement remis en
question. Si vous ajoutez au milieu du gué : toute l'interface doit être
en anglais ET en chinois, tout ce que vous avez réalisé auparavant est
peut-être compromis. Mais elle peut évoluer naturellement.

## Feedback

[Nicolas](https://twitter.com/51Conseils)

*Tout à fait d'accord le définition de fini est un élément important et
beaucoup d''équipes la néglige ou ne la respecte pas. J'ajouterai que
c'est un élément de transparence et que chaque membre de l'équipe doit
pouvoir l'expliquer aux différentes parties prenantes. D'autre part, la
définition de fini donne le LA à l'équipe au moment de définir comment
elle va s'y prendre pour réaliser le produit ou l'incrément. En ce qui
concerne le nombre de critères, je te rejoins, cela n'est pas nécessaire
de faire une liste à la Prevert, perso j'indique toujours de ne pas
dépasser la dizaine. Après ce que contient la liste n'appartient qu'à
l'équipe, une autre équipe dans la même organisation pourrait avoir une
définition différente, car chaque équipe travaille différemment.*

