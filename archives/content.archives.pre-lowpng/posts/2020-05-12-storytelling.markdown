﻿---
date: 2020-05-14
slug: storytelling
tags: ['storytelling','histoire', 'memorisation', 'metaphore', johari]
title: Storytelling 
---

Le *storytelling*, cette capacité à raconter des histoires, nous suit depuis la nuit des temps. Quid de l'oeuf ou de la poule, mais notre cerveau est désormais "cablé histoire", pour reprendre les mots de [Oana Juncu](https://twitter.com/ojuncu) avec qui j'avais fait un [atelier sur le sujet en 2013](/pdf/storytellingbattle.pdf). Le *storytelling* c'est aussi pour moi ces parties endiablées de donjons & dragons durant l'adolescence, et je partage avec [David de benext](https://www.linkedin.com/in/david-robert-188a2a96/?originalSubdomain=fr), l'idée qu'avoir joué à ces jeux et avoir été maître du jeu est un élément non négligeable dans notre construction personnelle vis-à-vis des organisations. 

À quoi va me servir le *storytelling* ? À transmettre et à faire mémoriser. Il se dit (vous savez les stats...) que l'on retient 22 fois mieux une histoire qu'un texte. Raconter des histoires c'est aussi fabriquer ou transmettre ou transformer une culture. 

Comme son nom l'indique, le *storytelling* se raconte plutôt qu'il ne se lit. Mais essayons. Le plus simple pour moi c'est de vous décrire les commentaires et l'accompagnement réalisé quelques fois auprès des ingénieurs commerciaux de benext. Mais naturellement cet exemple est juste un véhicule et les principes soulevés ici sont applicables dans de multiples situations. 

## Le meetup 


{{< youtube id="uOlNwFaqBXA" >}}

## Prendre conscience du ton de sa voix, de ses silences, de sa posture, etc. 

Naturellement au-delà des mots la posture, l'intonation, le rythme, etc. jouent un rôle important. Une première étape serait de rendre conscients les gens de cela. Pour cela je prends une phrase pas trop marquée positive ou négative comme "Bon maintenant on se dit que l'on va essayer de le faire". Et je demande à une personne de la dire avec une intonation bien précise qu'elle note sur une feuille (par exemple : colère, angoisse, peur, hésitation, affirmation, être décidé, etc.). Je demande aux autres de noter quelle intonation ils ont ressentie et pourquoi. Des fois l'écart est tel qu'il déclenche un moment de réalisation, un *ah ah moment*, chez la personne qui prend conscience que ce qu'elle souhaite transmettre n'est pas du tout ce qu'elle transmet effectivement, et c'est le moment de parler du ton, des silences, du rythme, de la posture, etc.   

L'absence de caméra est une posture en visioconférence par exemple. Tout cela continue de compter en télétravail. 

## Prendre un exemple comme point de départ

Ensuite on peut demander aux personnes de raconter comment elles présentent BENEXT. 

Je grossis le trait et résume ce que je peux entendre chez nos ingénieurs commerciaux ou rh (il s'agit d'extraits) : 

	BENEXT est une société innovante regroupant une communauté de spécialistes et de talents passionnés par l’innovation,les technologies et les nouvelles pratiques. Nous accompagnons nos clients aussi bien dans leur recherche d’innovation que dans la mise en place de pratiques plus agiles.

ou encore  

	BENEXT a été créé, en 2014, par des agilistes. Et nous sommes environ XXX benexters, avec un but, un objectif commun => Be Your Potential. 
	Et Be Your Potential, signifie rendre les beNexters et nos clients meilleurs. C’est à dire, leur faire prendre conscience de leur potentiel. 

	Pour cela, nos communautés de pratique ont un rôle à jouer. 
	Si je te parle de communauté, c’est parce que nous nous sommes inspirés de mode d’organisation en Holacratie. 
	C’est-à-dire qu’il n’y a pas de management, pas de directeur de B.U chez BENEXT. Les communautés se retrouvent, une fois par mois pour prendre des décisions et échanger. Et pour faire vivre, encore plus ces communautés, a été mis en place un certain nombre de rituels. 


## Premier ingrédient : les étapes principales du *storytelling* 

Voici les composants de base d'une bonne histoire : 

### Dire cœur du sujet 

De façon succincte et claire ("ce que tu ne sais pas expliquer à un enfant de 8 ans tu ne le comprends" -- Einstein, "ce qui se conçoit clairement s'énonce simplement" -- adage populaire) introduire le cœur du sujet.   

### Décrire les protagonistes 

Qui sont les acteurs de l'histoire. (ou d'abord le décor).  

### Planter le décor

Décrire le contexte, les lieux et/ou l'époque, et/ou etc.. (dans l'ordre désiré protagonistes et décor)

### Expliquer quel est le problème, l'empêchement

L'histoire part d'un point A vers un point B. C'est un récit, un cheminement. Pourquoi a-t-on voulu quitter le point A ? Quel problème est révélé ainsi ?
Comme dans la journée du héros (cette structure décriée, mais qui, par exemple, a servi à *Star Wars*) qu'une critique résume ainsi : "le héros a des problèmes, le héros résout ses problèmes". Vous référez à [Monomythe](https://fr.wikipedia.org/wiki/Monomythe) ou en anglais (plus exhaustif) [Hero's journey](https://en.wikipedia.org/wiki/Hero%27s_journey).

###  Révéler le déclencheur de l'histoire

L'histoire est déclenchée par quelque chose soit qui est devenu intolérable, plus supportable, soit qui a simplement changé la situation. 

### Parcourir le chemin vers la résolution

Décrire les étapes du cheminement de façon concrète, nous y reviendrons. 

### Décrire le dénouement, la nouvelle situation

À la fin de ces étapes, le point B se révèle, quel est-il ? 


Si je m'essaye sur la présentation de BENEXT évoquée plus haut : 

	Je voulais vous parler de benext et pourquoi nous sommes fiers de participer à cette aventure (coeur du sujet). BENEXT est née dans la tête de David sont fondateur (protagoniste). David dirigeait une entreprise somme toute classique du genre que l'on nomme ESN, ou SSII, (planter le décor) et chez qui souvent on dénigre l'avarice versus l'humanisme (problème). Pour David c'était devenu intenable (déclencheur). Il devait retrouver du sens dans ce qu'il entreprenait (déclencheur). Et ainsi il a tout quitté pour créer benext (courte description des étapes). Dont le sens premier est de rendre les humains meilleur, nos benexters, nos clients principalement. Aujourd'hui c'est de cette structure, à laquelle j'appartiens avec bonheur, dont j'ai envie de vous parler (dénouement, nouvelle situation).      


Faisons un détour du côté des fables ou par chez Shrek et Pixar. Emmy Coats avait publié un article très intéressant sur les [histoires chez Pixar](/2013/02/il-etait-une-fois/) que j'avais traduit et commenté. Observez le point 4 (mais beaucoup sont intéressants même si hors de notre scope): 

Once upon a time there was .... Every day, ..... One day ..... Because of that, ..... Because of that, ..... Until finally ...
Il était une fois ...... Chaque jour, ..... Un jour ...... En raison de ..... À cause de ..... Et finalement ..... 

Un décor, des protagonistes, un déclencheur, des étapes, un dénouement.  

## Maintenant il s'agit de creuser les étapes

Les étapes sont le cheminement. Si je reprends la formule "il était une fois" j'ajoute "et" "et" "et" "et" "jusqu'à que"... Tous ces "et" débutent et finissent. Les étapes sont normées. Elles débutent et finissent, une durée, un statut au départ et à la fin. En un mot elles sont "smart", acronyme anglais pour *spécifique* (c'est pas un roman fleuve comme guerre et paix, une encyclopédie ou une somme, il y a un focus),  *mesurable* (on sait si cela a marché ou pas, ou cela en est), *réaliste* (c'est possible même dans le contexte du conte), et *temporellement limité* (on ne se lance pas dans quelque chose sans fin, c'est proche de spécifique). Vos étapes sont concrètes. Essayons. 

	Je voulais vous parler de benext et pourquoi nous sommes fiers de participer à cette aventure (coeur du sujet). BENEXT est née dans la tête de David sont fondateur (protagoniste). David dirigeait une entreprise somme toute classique du genre que l'on nomme ESN, ou SSII, (planter le décor) et chez qui souvent on dénigre l'avarice versus l'humanisme (problème). Pour David c'était devenu intenable (déclencheur). Il devait retrouver du sens dans ce qu'il entreprenait (déclencheur). Et ainsi il a tout quitté pour créer benext (courte description des étapes). Il en a défini les valeurs. Il s'est rapidement entouré d'un petit groupe de personnes servant de gardes-fous. Ensemble ils ont basé le fonctionnement autour d'un modèle très participatif. On a trouvé des locaux de plus en plus agréables. Ils ont mis en place un cadre fort sur l'accompagnement et l'apprentissage (étapes) car le sens premier de BENEXT est de rendre les humains meilleur, nos benexters, nos clients principalement. Aujourd'hui c'est de cette structure, à laquelle j'appartiens avec bonheur, dont j'ai envie de vous parler (dénouement, nouvelle situation).      

## Être authentique : si vous donnez, vous recevrez (karmic communication)

La [fenêtre de Johari](https://fr.wikipedia.org/wiki/Fen%C3%AAtre_de_Johari) est un modèle de lecture de la communication entre deux personnes, ou deux groupes, ou deux ... Elle représente quatre zones. 
* Ce que les deux entités connaissent (la zone publique).
* Ce que vous savez vous, mais que l'autre ne sait pas (zone cachée), par exemple, que vous adorez les BBQ, l'autre ne le sait pas. 
* Ce que l'autre sait mais que vous ne savez pas (zone aveugle), par exemple, qu'il adore le modélisme. 
* Ce que personne ne sait ou n'imagine (la zone inconnue). Par exemple vous êtes tous les deux fans de Led Zeppelin et vous étiez tous les deux au concert de Jimmy Page & Robert Plant en 1998.       

![Fenêtre de Johari](/images/2020/05/johari.png)

Si vous voulez découvrir la zone inconnue qui recèle beaucoup de trésors et de liens, vous devez révéler de votre zone cachée. Si vous révélez des choses sur vous, et de plus si vous mettez en évidence certaines vulnérabilités, vous aidez votre interlocuteur à se mettre en position de lui-même aussi vous découvrir la zone aveugle, et ainsi d'avoir encore plus de chance de ensemble de découvrir cette zone inconnue.  

[J'en parlais ici](/2020/01/bienfaits-de-l-authenticite/).

Essayons 

	Je voulais vous parler de benext et pourquoi nous sommes fiers de participer à cette aventure (coeur du sujet). BENEXT est née dans la tête de David sont fondateur (protagoniste). David dirigeait une entreprise somme toute classique du genre que l'on nomme ESN, ou SSII, (planter le décor) et chez qui souvent on dénigre l'avarice versus l'humanisme (problème). Pour David c'était devenu intenable (déclencheur). Il devait retrouver du sens dans ce qu'il entreprenait (déclencheur) et du plaisir, sous ses airs de *businessman* cela reste un rêveur épicurien (zone cachée). Et ainsi il a tout quitté pour créer benext (courte description des étapes). Il en a défini les valeurs. Il s'est rapidement entouré d'un petit groupe de personnes servant de garde-fous. Ces coachs amènent le chaos ! Cela remue, mais cela amène beaucoup de choses même si ce n'est pas tous les jours facile (zone cachée). Ensemble ils ont basé le fonctionnement autour d'un modèle très participatif. On a trouvé des locaux de plus en plus agréables. Ils ont mis en place un cadre fort sur l'accompagnement et l'apprentissage (étapes) car le sens premier de BENEXT est de rendre les humains meilleur, nos benexters, nos clients principalement. Aujourd'hui c'est de cette structure, à laquelle j'appartiens avec bonheur, dont j'ai envie de vous parler (dénouement, nouvelle situation).      

## Faciliter l'appropriation avec des symboles, des métaphores.

Dans "des métaphores dans la tête" de Lawley et Tompkins il est dit que l'on peut décrire des situations de trois façons : sensorielle, conceptuelle, ou symbolique. Les symboles (ou métaphores) permettent de devenir concret. Le langage conceptuel est par définition conceptuel. Les métaphores parlent à tout le monde, et elles permettent de rendre palpables des choses très fines et difficilement exprimables autrement. Tout le monde les comprend. Pour bien utiliser vos métaphores rappelez-vous cet acronyme : VAKOG (Visuel, Auditif, Kinesthésique -- le touché --, Odorat, Goût), essayez d'être sûr que vos métaphores mettent en évidence l'un de ces sens. 

Par exemple :

### Sensoriel, "me réchauffe le coeur et me donne le sourire". 

* Aujourd'hui c'est de cette structure, à laquelle j'appartiens et qui me réchauffe le coeur et me donne le sourire, dont j'ai envie de vous parler.     

### Conceptuel, "bonheur". 

* Aujourd'hui c'est de cette structure, à laquelle j'appartiens avec bonheur, dont j'ai envie de vous parler.      

### Symbolique, "un grand manteau de fourrure qui me réchauffe et dont chaque poche recèle de surprises". 

* Aujourd'hui c'est de cette structure, qui est comme un grand manteau de fourrure qui me réchauffe et dont chaque poche recèle de surprises, c'est de cette structure dont j'ai envie de vous parler.      

Pour finir donc : 


	Je voulais vous parler de benext et pourquoi nous sommes fiers de participer à cette aventure (coeur du sujet). BENEXT est née dans la tête de David sont fondateur (protagoniste). David dirigeait une entreprise somme toute classique du genre que l'on nomme ESN, ou SSII, (planter le décor) et chez qui souvent on dénigre l'avarice versus l'humanisme (problème). Pour David c'était devenu intenable (déclencheur). Il devait retrouver du sens dans ce qu'il entreprenait (déclencheur) et du plaisir, sous ses airs de *businessman* cela reste un rêveur épicurien (zone cachée). Et ainsi il a tout quitté pour créer benext (courte description des étapes). Il en a défini les valeurs. Il s'est rapidement entouré d'un petit groupe de personnes servant de garde-fous. Ces coachs amènent le chaos ! Cela remue, mais cela amène beaucoup de choses même si ce n'est pas tous les jours facile (zone cachée). Ensemble ils ont basé le fonctionnement autour d'un modèle très participatif. On a trouvé des locaux de plus en plus agréables. Ils ont mis en place un cadre fort sur l'accompagnement et l'apprentissage (étapes) car le sens premier de BENEXT est de rendre les humains meilleur, nos benexters, nos clients principalement. Aujourd'hui c'est de cette structure, qui est comme un grand manteau de fourrure qui me réchauffe et dont chaque poche recèle de surprises, c'est de cette structure dont j'ai envie de vous parler.      


## Compléments 

Pendant le meetup deux ajouts sont venus se greffer. 

* Si vous avez un méchant, faîtes qu'il soit très bon dans sa méchanceté, cela met en valeur votre obstination (et c'est ce qu'on aime dans un héros d'histoire) à le dépasser. Si le méchant est bon l'histoire est bonne. 
* N'hésitez pas à filmer vos entrainements. 
