---
date: 2013-11-02T00:00:00Z
slug: agile-a-grande-echelle-cest-clair-comme-du-cristal
tags: ['organisation','safe','agile','echelle','scaling']
title: 'Agile à grande échelle : c''est clair comme du cristal'
---

Beaucoup de discussions actuellement sur de la "mise à échelle" de
l'agile, scrum ou kanban (chez Kanban, la mode c'est l'évocation des
portfolios de kanban). C'est aussi pour beaucoup l'occasion de ramener
ces égarés de la pensée agile dans le droit chemin : voici le retour des
organisations, et de la difficulté de synchroniser ces grandes dames, on
en profite pour proposer un retour au contrôle, à l'illusion de
contrôle, à l'illusion de pouvoir. C'est la diarrhée des *Scaled Agile
Framework* et consorts qui prouvent si cela était nécessaire qu'ils
n'ont rien compris à la culture agile.

![Flocons](/images/2013/11/snowflakes.jpg)

C'est vrai que depuis plusieurs années, l'agile, disons plus clairement
**Scrum** s'étend (avec XP concernant les pratiques d"ingénieries
*\[1\]* ).

J'observe donc deux phénomènes :

-   le premier, c'est de découvrir que c'est dur, **Scrum**, que cela
    demande un investissement, que cela renouvèle les rôles les
    responsabilités, l'organisation et la culture de la compagnie. Tout
    le monde n'est pas ravi par cela ou tout le monde ne réussit pas.
    Pourtant de nombreuses personnes souhaitent malgré tout être agiles
    -tant mieux- et elles se tournent donc vers **Kanban**. Qui en
    dehors de ses qualités propres (qui sont indéniables), permet aussi
    de se dire agile sans se heurter de façon aussi claire à certaines
    contraintes.
-   Le second, c'est que bon gré mal gré, selon les contextes plus ou
    moins bien, on arrive à déployer Scrum (ou Kanban) à une plus grande
    partie de l'organisation et que des problématiques nouvelles
    apparaissent concernant notamment une "mise à l'échelle".

## Scaled Agile Framework : de la nouille, du flan

Pas mal de structures ou de personnes profitent de cette nouvelle donnée
: la mise à l'échelle. L'occasion est trop belle pour faire resurgir de
vieux démons : une organisation rationnelle, mécaniste. On peut parler
de mémoire du muscle. Il faut sortir de ces schémas qui, non seulement
n'ont ni queue ni tête, renient les fondements de ce nouveau mode
d'organisation auquel nous tenons tant et surtout, ne fonctionnent pas.

"L'homme n'a jamais pu se passer de grilles" (qu'on dise reste oublié
derrière ce qui se dit dans ce qui s'entend. Lacan).

## Une organisation agile à grande échelle

Mon expérience personnelle m'a permise d'être immergé disons à
l'intérieur d'une quinzaine d'équipes agiles simultanément au sein d'une
même organisation (je ne les suivais pas toutes de la même manière, mais
j'étais à l'origine de la transmission agile). Pas plus. Mais c'est déjà
pas mal. Suffisant probablement pour appliquer les approches auxquelles
je crois. Tout nous a appris à ne pas considérer le monde moderne comme
un monde rationnel et mécaniste mais comme un monde complexe donc
imprévisible, contradictoire, voire désordonné. Beaucoup veulent
rationnaliser quand le nombre d'équipes, de projets leur parait grand.
Au lieu de s'inspirer d'une approche cartésienne on devrait s'inspirer
d'une approche complexe. Observons la nature autour de nous, et plus
précisément : le cristal.

C'est en voyant cette image de cristal se formant sur l'eau que j'ai
fait cette analogie. Lire deux ou trois textes sur le cristal a plutôt
conforté mon intuition, je continue donc à penser ainsi. Le cristal est
[émergent](http://en.wikipedia.org/wiki/Emergence). Je vous propose
**quelques analogies avec le cristal**, puis rappelle les idées sur les
**systèmes informationnels ouverts de Laborit**, pour plus communement
**proposer une mise en oeuvre**.

## La cristallisation

![Cristal](/images/2013/11/cristal-sur-eau.jpg)

*La cristallisation est le passage d'un état désordonné liquide, gazeux
ou solide à un état ordonné solide, contrôlé par des lois complexes. La
fabrication d'un cristal se déroule sous le contrôle de différents
facteurs tels que la température, la pression, le temps d'évaporation.*
--
[Geowiki](http://www.geowiki.fr/index.php?title=Comment_se_forment_les_cristaux)

Mon analogie est de dire que la formation d'une organisation agile à
grande échelle pourrait répondre aux observations faites sur le cristal
et la cristallisation. C'est juste un jeu avant toute chose pour me
permettre (et peut-être vous) d'appréhender mieux le "bon" état
d'esprit, la bonne façon de faire de l'agile à grande échelle.

On pourrait ainsi dire que plusieurs équipes organisées différemment
(état liquide, gazeux, solide) mais désordonnées peuvent s'organiser
ensemble (s'ordonner) en appliquant des lois complexes, et pas des lois
mécanistes, compliquées.

### Le cristal est fainéant

*Pour comprendre mieux la raison de la forme d’un cristal, il ne faut
pas ignorer que la nature est fainéante et que lorsqu'on lui laisse le
choix et le temps, elle choisit toujours les solutions qui lui coûtent
le moins d'énergie. Ainsi, la forme d'un cristal témoigne des conditions
physiques qui prévalaient lors de sa croissance, car c'est la forme qui
a coûté le moins d'énergie*. --
[Geowiki](http://www.geowiki.fr/index.php?title=Comment_se_forment_les_cristaux)

Si on accepte l'analogie il faudrait donc toujours laisser l'agile à
l'échelle **prendre la forme qui convient le mieux à l'organisation**.
Et pas une forme pré-déterminée.

### Stabilité du cristal

*Après cette ébauche de raisonnement, il faut savoir que les choses ne
sont pas toujours aussi simples. Une face cristalline est aussi une
discontinuité, une surface où les atomes ne sont pas liés chimiquement
comme les atomes du cœur du minéral. Cela revient en quelque sorte à
casser des liaisons (l'approximation est un peu brutale mais correcte).
Or dans le milieu de croissance, des espèces solubles sont capables de
s'absorber à la surface en se liant faiblement avec les atomes de la
surface. On peut stabiliser des plans qui coûtent normalement très
chers, et en déstabiliser d'autre qui sont normalement facile à payer.
Ainsi on modifie la forme finale du cristal cette fois-ci sans modifier
les conditions physiques. Ces phénomènes de stabilisation peuvent ainsi
induire des morphologies cristallines qui ne devraient pas être
habituellement observées. Quand des espèces chimiques s’absorbent en
surface et donc stabilisent les plans présentés, ces surfaces sont moins
accessibles pour continuer la croissance suivant ces plans. Ainsi ce
sont les plans les moins stables, ceux qui ont peu, voire pas, de
molécule en surface, qui croissent le plus vite. Comme ces plans sont
instables ils finiront par ne plus être représentés lors de la
croissance, privilégiant ainsi la croissance lente des plans les plus
stables.* --
[Geowiki](http://www.geowiki.fr/index.php?title=Comment_se_forment_les_cristaux)

Je continue mon analogie. On sera surpris par la facilité à stabiliser
certains pans de l'organisation, et à l'inverse en déstabiliser certains
-pourtant pensés comme stables- de façon inattendue. La **mise à
l'échelle est plus aisée avec les éléments qui n'ont pas encore été
stabilisés dans l'organisation**. La **dynamique de la mise à l'échelle
se ralentira nécessairement quand toutes les parties seront
stabilisées**. Faudrait-il donc presque **encourager par moment la
destabilisation** ?

### Source de modification : la troncature

*Une troncature c'est le remplacement d'un sommet ou d'une arête d'un
cristal par une face.* --[Geowiki, la
troncature](http://www.geowiki.fr/index.php?title=Troncatures)

*Une des causes modifiant la forme initiale des cristaux est la
troncature.* --[Geowiki, la
troncature](http://www.geowiki.fr/index.php?title=Troncatures)

![Cristal, troncature](/images/2013/11/troncature.jpg)

Faut-il voir là l'idée que c'est en redonnant un pouvoir
d'auto-organisation à chaque groupe, en supprimant la tête, qu'on lui
permet de se s'organiser, d'une façon complexe, de se cristalliser au
mieux avec le reste des parties de la compagnie ?

### Gestion des dépendances : les macles ?

*Selon la position des cristaux on distingue des macles par accolement
et des macles par pénétration (ou interpénétration - plus ou moins
complète). On parle de macles simples lorsque deux cristaux sont
associés, et de macles multiples lorsque plus de deux sous-individus
composent la macle, on en arrive parfois, par multiplication des
sous-individus impliqués, à des macles cycliques, alors que lorsque la
formation de macle se répète à l'intérieur d'un groupe, on obtient des
macles polysynthétiques, (répétées).* --[Geowiki, les
macles](http://www.geowiki.fr/index.php?title=Les_macles)

Je sais que je m'amuse à pousser l'analogie loin, mais comme je découvre
*les macles* ces amas d'équipes associées... et que je trouve que le
vocabulaire utilisé par la science des cristaux me rappelle furieusement
les situations vécues.. pourquoi s'en priver..

## Système ouvert thermodynamique et informationnel

*La structure de la matière vivante lui confère deux caractéristiques
fondamentales : celle d'être un système ouvert et celle de s'organiser
par niveaux de complexité, ces deux caractéristiques étant d'ailleurs
strictement dépendantes l'une de l'autre... systèmes ouverts tant du
point de vue thermodynamique qu'informationnel.* -- [Henri Laborit, La
nouvelle grille](http://fr.wikipedia.org/wiki/La_Nouvelle_grille)

*Les systèmes vivants au sein de la biosphère ont su réaliser des
structures autogérées, et l'on peut s'étonner de ce que, si le
déterminisme aveugle de l'évolution biologique a su réaliser de tels
systèmes, l'homme, dans ses sociétés, n'ait pas pu en faire autant. Nous
tenterons de comprendre pourquoi.* -- [Henri Laborit, La nouvelle
grille](http://fr.wikipedia.org/wiki/La_Nouvelle_grille)

*La finalité de l'ensemble doit être aussi celle de chacun des éléments
qui le constitue.* -- [Henri Laborit, La nouvelle
grille](http://fr.wikipedia.org/wiki/La_Nouvelle_grille)

Voici, cumulons cela à notre formation du cristal :

-   Systèmes autogérés : des équipes, des départements, des sites
    autogérés.
-   S'incluant entre eux (comme des poupées russes) avec une ouverture
    informationnelle et thermodynamique vers les systèmes qu'il contient
    et les systèmes dans lesquels il est contenu : des équipes,
    départements, des sites pour provoquer des actions vers le système
    dans lequel ils sont inclus, et vers les systèmes qu'ils
    contiennent. De façon identique la communication circule dans tous
    les niveaux.
-   L'ensemble est mené par une finalité : de l'organisation au pair
    programming, chaque groupe tend vers la même finalité. La vision,
    les valeurs de l'organisation.
-   Tout cela se base sur des lois complexes.
-   Un élément qui parait fragile peut se stabiliser beaucoup plus
    simplement qu'il n'y parait. A l'inverse un élément qui parait
    simple peut devenir difficile à stabiliser, ou se déstabiliser de
    façon inattendue.
-   C'est en supprimant les arêtes (goulots, séparations, représentés
    par une personne ?) et en les remplaçant par des faces (équipes
    auto-organisées)

## Proposition d'agile à grande échelle

![Termitières](/images/2013/11/termites.jpg)

-   Des équipes qui avoisinent **7 personnes**. Des départements qui
    avoisinent **50 personnes**. Des organisations qui avoisinent **150
    personnes**. Des compagnies ou métaorganisation qui regroupent un
    ensemble de 7 organisations, puis 50 organisations puis 150
    organisations, etc etc etc voir [Dunbar &
    Allen](/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/).
-   J'ai pu drôlement observer qu'au delà de 7 équipes (en fait 5 ou 6)
    travaillant sur un même meta-projet/produit on arrivait à une
    saturation. Un département d'une cinquantaine de personnes avec 5 à
    7 équipes travaillant sur un même sujet est donc tout à fait
    envisageable. Au délà il faut *modifier la forme sans modifier les
    conditions physiques*, comme le cristal.
-   Des groupes de 7 **auto-organisés**, des groupes de 50
    auto-organisés, des groupes de 150 auto-organisés. Cela évoque aussi
    les "mini-site" de [Jean-François
    Zobrist](http://www.youtube.com/watch?v=XCHUhyu9Tt0&html5=1).
-   Une **vision partagée, un objectif partagé** à atteindre qui amènera
    les informations et les actions à bien fonctionner. Et les gens à
    [se sentir bien](/2013/10/deux-amis/).
-   Garantir que chaque groupe/système (poupée russe) peut
    **communiquer** avec son contenant et son conteneur.
-   Garantir que chaque groupe/système (poupée russe) peut avoir des
    **interactions** avec son contenant et son conteneur.
-   Observer constamment les **stabilisations et les instabilités**.
    **Provoquer des destabilisations quand le système est entièrement
    stable**.
-   Laisser l'organisation prendre la forme qu'**elle souhaite ou qui
    émerge**, et la laisser évoluer en l'accompagnant.

[English
version](http://translate.google.com/translate?hl=en&sl=fr&tl=en&u=http%3A%2F%2Fwww.areyouagile.com%2F2013%2F11%2Fagile-a-grande-echelle-cest-clair-comme-du-cristal%2F)
(give a try with google translate)

*\[1\]* pas la peine de m'expliquer que XP ne se limite pas aux
pratiques d'ingénieries.
