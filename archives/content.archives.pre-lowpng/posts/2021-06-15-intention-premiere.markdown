---
date: 2021-06-15
slug: intention-premiere 
tags: [intention, management, leadership, premiere, focus]
title: "L'intention première"
--- 

J'entre dans une période dans laquelle je ressens le besoin et l'envie de poser mon expérience pour la confronter. Qu'est-ce que je sors de ces années d'accompagnement et de direction d'entreprises ? Est-ce que je suis capable d'en déduire des principes, où tout n'est que le fruit du moment, du hasard, de l'intuition et de la chance ? Est-ce que j'imagine que ce qui s'est passé est réellement ce qui s'est passé ou est-ce le fruit de mon imagination ? Si tel est le cas, saurais-je détecter, analyser et reproduire ce qui s'est réellement passé ?

Par où démarrer ? 

Probablement par ce qui me semble être le principe fondateur et que je vais essayer d'appeler "l'intention première".

## Intention première

Le droit à l'erreur n'est pas un appel à l'erreur. Il est une autorisation qui nous permet de vraiment nous focaliser sur le but à atteindre, quitte à échouer. Pour rendre cela possible : des intervalles de temps courts dans l'apprentissage (itérations) ou autres, pour s'autoriser. Si on n'est pas autorisé, on perd le focus sur cette intention première, pour d'abord, ou en même temps, ne pas échouer. Et les comportements changent diamétralement. C'est avec cette découverte, quand on la fait, en vivant ce droit à l'erreur possible que l'on réalise que sa plus grande vertu est de nous permettre de tout orienter vers cette intention première, d'avoir ce focus unique. 

Les équipes de google qui parlent de "psychological safety" (sécurité psychologique) ne parlent à mes yeux pas d'autres choses. Il s'agit de pouvoir entièrement ce dédier à un objectif, et de ne pas craindre les erreurs, les confrontations, les doutes, les troubles. Ce "psychological safety" ne parle pas de bienveillance ou de bien-être, il évoque cette intention première qui devient la reine de toutes les actions et de tous les causes. 

Quand je me suis observé à réussir (on ne va pas se lancer là dans un jeu sur la réussite ou l'humilité svp), ou quand j'ai observé des personnes ou des équipes qui réussissaient j'ai noté qu'il y avait une intention première bien marquée, franche. Cette intention première se doit de ne pas être ambiguë. Voilà ce que je veux, voilà ce que nous voulons. Une et une seule intention première. Je veux réussir à faire une véritable entreprise qui s'appuie sur l'état d'esprit agile ou je veux devenir un acteur incontournable de ce marché ou je veux réussir ma carrière ou je veux réussir la transformation organisationnelle de mon entreprise ou etc. J'utilise beaucoup "je", car le "nous" est la somme des "je" et la qualité du "nous" dépend de l'homogénéité des "je". 

Les questions que je me pose et que je poserai clairement à l'avenir systématiquement (c'est l'idée aussi de cette structuration de ma pensée que j'évoquais au début) : Est-ce que cette intention première existe, et quelle est-elle ? 

Elle se doit d'être unique, elle se doit d'être sincère (naturellement, mais il faut le rappeler malheureusement). 
Je ne mettrais pour l'instant pas d'autres contraintes. Je pourrais préciser ce n'est pas un moyen, mais un objectif, ou un principe, ou un cheminement, etc., mais je ne suis pas sûr que cela soit une contrainte réelle.

Trop souvent : elle n'est pas unique : on veut se transformer, mais en même temps préserver ceci ou cela. Je n'observe pas de réelle réussite si cette étoile du nord, cette intention première et unique n'existe pas. Sans ambiguïté. Oui c'est finalement comme cette *north star* des métriques, mais elle n'est pas réduite à un chiffre, elle peut demeurer une intention.

On peut vouloir préserver ceci ou cela (cette équipe, sa carrière, des aspects économiques), mais uniquement si et seulement si on nourrit d'abord l'intention première. 

Toute la beauté de la chose est aussi de trouver pour ces autres objectifs secondaires une façon élégante de les associer à cette intention première. 

Vous allez faire des compromis, vous allez devoir et pouvoir poursuivre d'autres buts, mais qu'à la condition que ceux-ci soient moins importants et toujours soumis à la prédominance de cette intention première. En cas d'arbitrage je vous encourage fortement à ce qu'elle domine tout le temps. C'est ce qui fait toute la différence. 

## Toutes les routes mènent à Rome

C'est bien là l'objectif. Cette intention première va donner une cohérence ou amener de la richesse, de l'apprentissage à chacune de vos actions. Vous pouvez avoir des actions très diverses et très variées si chacune d'elle poursuit le même objectif commun, par exemple : transformer votre entreprise pour l'adapter au monde moderne, ou autre exemple plus individuel : manager les personnes avec les principes auxquels je crois. Mais vous ne pouvez pas mener les deux de front au même niveau. Si vous voulez transformer votre entreprise pour l'adapter au monde moderne, et que vos convictions vous permettent d'y aller tant mieux. Mais si les deux ne sont pas compatibles, il faudra choisir, avoir une seule intention première. 

Avec cette intention première, chaque cours de guitare m'amène un regard différent sur mon travail de dirigeant, chaque difficulté dans ma relation de parent/enfants, ou chaque plaisir et réussite provoque un apprentissage pour mon intention première. Tout devient relié en un sens.  

On rate les choses à les faire parce qu'il faut les faire, parce que les autres le font, parce que cela fait bien de les faire, mais au fond de soi ce n'est pas quelque chose que l'on comprend ou que l'on désire. Il est impossible de réussir dans ces conditions. Il est nécessaire d'avoir une volonté forte ou un désir fort de quelque chose. Et naturellement dans les entreprises c'est aux personnes qui ont les leviers que je m'adresse, aux hauts dirigeants.  

## L'engagement 

L'engagement si souvent évoqué pour les équipes est fondamental pour les (hauts) dirigeants. 

Si votre intention première est de dégager de la marge, ou d'économiser sur vos coûts de structure, ou de gagner tel ou tel marché, ou d'instaurer le bien-être dans l'entreprise vous mettrez les chances de votre côté de réussir si décidez lequel de ces sujets prédomine sur tous les autres. À l'inverse à vouloir tous ces objectifs vous prenez le meilleur chemin pour les faire tous capoter. 

## Ce n'est pas compliqué ? 

Il m'est apparu que tout ce qui est compliqué à énoncer n'est pas si difficile que cela à mettre en place, et que si cela s'énonce simplement n'est pas si évident que cela à mettre en place. 

Ramener son projet à une intention première est très simple à énoncer. 

Vous pouvez changer d'intention première au fil du temps naturellement. 

## J'ai observé 

* Que quand j'avais cette intention première, les nombreuses choses diverses et variées que je faisais venaient se compléter ou se croiser comme par magie pour donner une cohérence globale à mon action, à mon environnement. 
* Que quand j'avais cette intention première, mes apprentissages comme mes idées avaient des ramifications croisées qui les enrichissaient. 
* Que je prenais de meilleures décisions, car toutes les décisions avaient la même direction. Que j'étais impliqué dans toutes les décisions que je prenais, car dans chacune d'elle résidait un petit morceau de cette intention première. Et que toutes les décisions qui n'étaient pas liées à cette intention première soit je ne m'en occupais pas, soit elles indiquaient que je faisais fausse route.   

J'aimerais que chaque dirigeant fasse l'effort de se dire qu'elle est son intention première et qu'il organise son attitude, ses décisions, et ce qu'il exprime autour.  


### Mes petits principes de management

* [L'intention première](/2021/06/intention-premiere/)
* [Vers le simple, vers le complexe](/2021/07/simple-complexe/)
* [Ma cible est mon point de départ : appréhender l'émergence](/2021/07/point-de-depart/)
* [Cultiver l'imperfection](/2021/09/cultiver-imperfection/)










