---
date: 2013-07-15T00:00:00Z
slug: dan-mezicks-garage-vol1
tags: ['culture','openadoption','oaa','openagileadoption','openspace','openspaceagility','agility']
title: Dan Mezick's Garage Vol.1
---

Dan Mezick va être un des *keynote speakers* du [Scrum Gathering de
Paris](http://www.scrumalliance.org/courses-events/events/global-gatherings/2013/paris-2013)
en septembre. Son thème de prédiclection est le "culture hacking".
Difficile de traduire, disons pirater la culture d'entreprise pour la
changer, mais sans le côté négatif du mot pirate (pensez aux pirates
sympas de Polanski), cela reste cependant un détournement. Un
détournement de culture d'entreprise provoqué par l'inoculation de
nouveaux éléments qui se propagent. Voyez je n'ai pas encore les bons
mots pour définir le "culture hacking", et sans mots clairs, pas de
concepts, pas de véritables compréhensions. Sans mot on ne peut pas
saisir l'idée véritablement. Pour l'instant nous avons une esquisse.

Dan est l'auteur de [The Culture
Game](http://newtechusa.net/about/the-culture-game-book/). Il développe
une théorie qu'il appelle **OAA : Open Agile Adoption** qui repose
beaucoup sur les openspaces. L'**openspace** comme lieu d'émancipation,
de passage et d'engagement vers ce changement culturel.

Nous parlons des openspaces agiles. Il s'agit d'évènements qui
permettent l'auto-organisation "à la volée" de sessions proposées par
les personnes présentes. Ces openspaces ne promulguent qu'une seule loi
: celle des deux pieds (vous pouvez à tout moment utiliser vos deux
pieds pour vous déplacer : vers une autre session, vers la sortie, vers
l'entrée, etc.). Pour en avoir réalisé plusieurs fois au sein
d'entreprises je plussoie le propos de Dan, ce fut des moments
marquants.

Dan fait le constat (que je partage) que de nombreuses adoptions agiles
échouent en raison de la non implication durable de certaines personnes.
Que la contrainte ou l'imposition de pratiques ne sera jamais un vecteur
d'engagement durable. Pour palier à cela, il dévoilera ses conclusions
et son outillage au fil de l'été pour culminer au [Scrum Gathering
(annonce de Dan)](http://newtechusa.net/agile/scrum-gathering/) puis
aussi, pour finir de façon plus décontractée, mais pas moins
intéressante avec deux workshops chez
[Convergenc.es](http://convergenc.es/2013/07/dan-mezick-culture-hacking/).
(Je ne peux assister au Scrum Gathering pour des raisons
professionnelles, et Oana & moi-même avions très envie de croiser Dan).

C'est donc pas peu fier que nous avons organisé (avec Oana) ces deux
workshops avec Dan Mezick (Paris, 26 septembre - Montpellier 28
septembre). Il nous indique que cela sera la première fois qu'il sera
proposé un enseignement formalisé de son approche pour des adoptions
agiles durables. Hey !

Nous limitons à 15 le nombre de participants pour que les séances
restent intéressantes pour tous les présents. Naturellement elles se
feront en anglais. Vous pré-inscrire ou nous envoyer un message c'est
[là](http://convergenc.es/2013/07/dan-mezick-culture-hacking/).

Toutes les infos (programme, tarif, dates, lieux, etc.) ici :
[Convergenc.es](http://convergenc.es/2013/07/dan-mezick-culture-hacking/).

Et venez avec votre tee-shirt de Frank Zappa.

{{< youtube id="lnDqHppWS_Q" >}}

