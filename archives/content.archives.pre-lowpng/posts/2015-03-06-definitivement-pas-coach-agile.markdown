---
date: 2015-03-06T00:00:00Z
slug: je-ne-suis-definitivement-pas-coach-agile
tags: ['coaching','coach','coachagile']
title: Je ne suis définitivement pas coach agile
---

*Je ne suis définitivement pas coach agile, je suis agent provocateur*.
Je participe à des missions dans lesquelles je m'épanouie, je peux
toucher du doigt ce que j'aime. Et j'avais envie d'écrire que ce que
j'aimais c'était l'inter-action.

Ce n'est certainement pas le positionnement défendu par l'image d'Epinal
du coach agile : penseur non intrusif et intellectuel, à l'écoute, et
qui fait évoluer les idées sans les bousculer. Dire que c'est moi, que
c'est ce que j'aime, que c'est cela auquel je crois, serait une grosse
hypocrisie.

Je n'aime pas la *posture basse du coach agile*, je suis pour
l'**émancipation**, comme dans *extreme programming*, une posture qui te
grandit, l'émancipation, la mienne et celle des autres.

Je ne veux pas observer, je veux agir, déclencher, provoquer de l'action
et l'action des autres. **J'aime la dynamique de l'interaction avec les
individus**.

Je n'aime pas le silence et la soumission de l'écoute : je veux que les
gens puissent s'exprimer, fort, et je veux aussi crier ce que je sais.
Car oui, j'ai des avis, des connaissances, de l'expérience, je sais des
choses, et donc je me trompe. Celui qui ne sait pas, ne dit pas, ne se
trompe jamais. Ce n'est pas mon cas. **Comme je me trompe, je change mes
plans (d'actions) plutôt que de suivre mes croyances ou mes lectures**.
Je me base sur l'expérience, du vécu.

Ces interactions, ces changements, bousculent l'inertie, je dois
protéger des personnes, me protéger, avancer avec eux ; ne pas les
oublier pour avoir trop voulu ne pas les déranger (ou déranger l'ordre
établi). **Je collabore avec les autres**, je ne suis pas dans une tour
d'ivoire. Dans l'action, je perçois la chimie qui se diffuse dans mon
corps comme une drogue ; dans l'action on sent la montagne qui bouge,
les esprits qui se libèrent, la renaissance de la coopération de
l'espèce. Je respecte l'intelligence de tous en leur disant ce que je
pense d'**égal à égal**. **Interactions, individus, changements**, je
suis **opérationnel et pas dans l'incantation** ou toute autre
spécifiation qui ne se mouille pas.

J'aime sentir les **flux d'énergie**.

Je, je, je, c'est un texte égocentrique mais le nous n'est que la somme
des "Je".

Je suis [Peter
Grant](https://en.wikipedia.org/wiki/Peter_Grant_%28music_manager%29),
je suis [Gordon Ramsay](https://en.wikipedia.org/wiki/Gordon_Ramsay), je
ne suis définitivement pas coach agile.

Je sens que c'est une belle ânerie, mais j'avais envie d'écrire cela.

Ecrit en écoutant "Noir désir - 666667"

## On m'écrit

*Selon moi :*

-   *j'ai toujours bousculé les idées reçues (sinon j'aurais été heureux
    dans le conformiste cycle en V !)*
-   *le coaching agile est fondé sur la relation humaine, donc sur
    l'interaction*
-   *la posture basse, appelée posture basse stratégique par Paul
    Watzlawick, a justement pour but l'émancipation, car en mettant
    l'autre en position haute, on le rend acteur plutôt que simple
    exécutant de bons conseils.*
-   *tu opposes observer et agir, or pour moi, on observe pour emmener
    (donc dans l'action) ou faire agir. Dans les 2 cas, ce n'est pas la
    pensée seule, mais la pensée avant l'action*.
-   *L'agilité s'inspire beaucoup de la systémique, qui est fondée sur
    les interactions. La dynamique de l'interactions avec les individus
    est la première valorisation du manifeste. Celui qui ne fait pas ça
    a-t-il un rapport avec l'agilité ?*
-   *Tu n'aimes pas le silence de l'écoute. Pourtant, il est très
    puissant pour l'émancipation et la mise en mouvement*
-   *Tu assimiles l'écoute à de la soumission. Tu veux crier ce que tu
    sais. Mais si personne ne t'écoute, à quoi cela va-t-il servir ?*
-   *Tu places le coach agile dans une tour d'ivoire, qui ne collabore
    pas. La collaboration est elle aussi dans le manifeste, et comme
    coacher, c'est accompagner, la tour d'ivoire ne peut être sa
    maison.*

*Oui, je connais aussi des personnes qui se disent coach agile et qui
n'en ont ni les qualités, ni la posture ni les compétences. Lorsque je
les croise, il m'arrive de ne pas vouloir être associé à ce qu'ils sont
et proclament. Pour autant, ça ne me dégoute de vouloir porter les
messages de l'agilité. Après, le titre, je m'en fiche un peu.
D'ailleurs, l'important, c'est la congruence que tu véhicules, jusque
dans ton article !*

Je réponds :

Je ne pense pas opposer observer et agir. je dis qu'il faut les deux. Et
pas nécessairement la pensée avant l'action. Car avant l'action des fois
la pensée tourne à vide. "Aucun plan ne survit au premier contact avec
l'ennemi" Von Moltke.

Naturellement que quand je dis que je n'aime pas le silence de l'écoute
c'est quand il est constant, sans son alter-ego : l'action,
l'implication, le risque. Mais naturellement que le silence de l'écoute
fait parti du jeu. Si personne ne m'écoute, au moins j'aurais crié un
bon coup et je me sentirai mieux. Tant mieux si sa maison n'est pas la
tour d'ivoire. Allez sortez !

On est bien d'accord sur le fond, je n'en doute pas. Je te remercie pour
ces questions pertinentes.

**On me dit aussi**

*&gt;Je n’aime pas le silence et la soumission de l’écoute*

*je n aime pas la soumission non plus pablo. mais je crois que l'écoute
grandit l'autre si elle est empathique ( pas sympathique hein? tu
connais la différence oui ?). elle lui donne de l'espace ( qui souvent
lui manque dans une structure non libérée). En cnv \[Communication Non
Violente, note de pablo\], il y a certes la posture d'écoute empathique,
en plus de l'auto-empathie, et de l'expression authentique. C'est
sûrement celle la que tu préfères, si je te lis bien. :-)*

Merci pour ton petit retour.

**Sur twitter, ça caquète**

![Feedback](/images/2015/03/twitter-coach.jpg)

Merci à tous. Pierre, tu as peut-être raison, je n'en sais rien. Je ne
crois pas que l'on puisse distinguer les choses aussi nettement, comme
toujours cela dépend du contexte, du moment. Pour Alexandre, oui je le
pense sinon j'aurais arrêté depuis longtemps ! Et je crois qu'il **ne
faut pas se fier aux apparences**, ce n'est pas parce que je prône
l'implication, l'action, que j'oblige les gens à quoi que ce soit.
D'aucune façon je ne mandate, contraint, force. Jamais. On n'emmène
jamais les gens ou une entreprise où elle ne veut pas aller, cela ne
marche pas et cela ne m'intéresse pas surtout. Je pense que cet
engagement est plutôt une marque de respect, nous sommes égaux, je suis
avec eux dans l'action. Contrairement aux apparences donc : la posture
du coach me paraît beaucoup plus hautaine, et aussi beaucoup plus
dommageable pour l'entourage. Donc oui ce "bien" que tu décris n'est
possible que parce qu'il est très souvent partagé.
