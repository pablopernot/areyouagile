---
date: 2013-12-06
slug: la-horde-agile-les-videos
tags: ['horde','agile','conference']
title: La horde agile, les vidéos
---

Voici les vidéos de deux des quatre *keynotes* "la horde agile", celles
de Montpellier & Toulouse. La dernière aura lieue cette semaine à
Clermont-Ferrand. Je dois toujours prendre le temps de consolider le
*minibook* qui va avec ( ~ 20 pages), mais je n'ai pas encore trouvé le
moyen d'y parvenir... Probablement durant les fêtes de fin d'année.

## La horde agile

Un mini livre a vu le jour suite à ces conférences : [La horde agile, le mini livre](/pdf/lahordeagile.pdf),
pdf, 42 pages,  ~ 4mo.

### Agile Tour Montpellier 2013

{{< vimeo id="81207687" >}}

<p><a href="http://vimeo.com/81207687">La horde agile, Montpellier, 2013</a> from <a href="http://vimeo.com/smartview">SmartView</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### Agile Tour Toulouse 2013

#### Partie 1

{{< vimeo id="78404401" >}}

#### Partie 2

{{< vimeo id="78405990" >}}

