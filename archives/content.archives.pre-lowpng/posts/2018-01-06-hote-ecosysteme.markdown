---
date: 2018-01-06T00:00:00Z
slug: hote-ecosysteme
tags: ['management','batisseur','hote','ecosysteme','host','leadership']
title: Hôte d'écosystème - Host Leadership
---

Fin novembre j'étais à Agile Grenoble. J'intervenais pour une *keynote*, une session d'ouverture. J'ai tellement de choses qui me bousculent dans ma vie que j'étais vidé, dans le bon sens, bien, car attaché à rien, avec l'envie de faire bien mais sans pression. Cette légèreté qui ne vous fait pas voler à tout va, mais plutôt qui vous suspend au même endroit comme en lévitation, un peu ailleurs, un peu là, mais vous rend insensible aux mouvements qui vous entourent. J'ai fait ma session, et puis, comme je flottais, je suis resté sur place. Je croisais [Laurent Sarrazin](https://twitter.com/bangalaurent) qui présentait le *Host Leadership*. Je n'avais pas prêté attention à ce mouvement même si le titre évocateur semblait couler de source et aller dans le bon sens. J'ai eu la bonne surprise d'une confirmation de cette première impression ainsi qu'un petit moment "ahah" (de révélation) par la justesse des termes proposés pour habiller cette présence que l'on appelle "hôte d'écosystème" (*host leadership*). Rien de nouveau dans ce nouveau concept mais c'est comme si les pièces se mettaient en place soudainement et que cela faisait "clic", comme si cela déverrouillait un mécanisme. 

L'*host leadership* ou ce que je vais appeler l'hôte d'écosystème c'est les rôles et les positions à tenir pour [ce "manager" moderne que j'évoquais plein d'introspection dans mon précédent article](/2018/01/good-work-best-skills-love-believe/). Introspection pour soi, hôte d'écosystème pour son organisation. 

Je vous engage à lire sur ce rôle, peut-être en démarrant par ici ? [Host Leadership agile, chez InfoQ -- en anglais](https://www.infoq.com/articles/host-leadership-agile). 

L'idée de ce management moderne c'est de maintenir l'espace, le créer et le maintenir. 

Je trouve la définition des rôles et des positions éloquente -- le sens est porté par les termes -- (et je traduis là *avec quelques libertés* l'article cité plus haut qui reprend lui aussi les définitions proposées par Helen Bailey et Marc McKergow) : 

### Rôles (à endosser)

* Initiateur : déclenche les premières étincelles qui vont propager une initiative bien plus grande. 
* Inviter :  inviter les gens (ceux qui viendront seront les bonnes personnes et l'engagement sera meilleur). 
* Créer l'espace : et tout son environnement, autant pĥysique que émotionnel, où quelque chose peut réellement se produire. 
* Gardien de l'espace (de la porte) : protéger l'espace créé, le maintenir, parfois en le clarifiant, en le faisant respecter.  
* Connecteur : faciliter la mise en relation de toutes ces personnes et soutenir les potentielles conversations. 
* Co-participer : agir et se montrer comme un élément de l'écosystème, à part entière. 

### Positions (dans l'espace)

* Sur la scène, en assumant la responsabilité d'être le centre d'attention et en assumant les messages portés.  
* Au milieu des gens : un parmi les autres. 
* Au balcon : en observant et apprenant, prêt à être en appui pour maintenir cet espace.
* Dans la cuisine : là où les recettes se préparent et qu'on les goûte avant de les servir.  

Pour reprendre les termes de Harrison Owen, "Sans passion personne n'est intéressé, sans implication rien n'arrive". Cette dualité je la retrouve dans cette [introspection](/2018/01/good-work-best-skills-love-believe/) évoquée la semaine dernière, et cette posture d'hôte d'écosystème (*host leadership*). 
