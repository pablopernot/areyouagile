﻿---
date: 2020-04-09
slug: meetup-cynefin
tags: ['meetup','chaos', 'expert', 'simple', 'obvious', 'complicated', 'complique','emergence','complexite','complexe','cynefin']
title: Meetup Cynefin 
---

Bonjour ceci est un brouillon réalisé (dans l'urgence) pour le meetup de 14h ce jour. Il sera revu lors de la diffusion de l'enregistrement. 

## Cynefin ? 

* Dave Snowden, le pays de Galles, le rugby, l'accent incompréhensible 
* CYNEFIN ? Si nez fin ? Qu'une ne vin ? QU'UNE NE VINGT ? OUI "cunevin" car c'est du gallois et cela respecte l'idée de ne pas se prononcer comme cela s'écrit. Du gaélique quoi. 
* Et d'ailleurs cela veut dire l'habitat en gallois, avec l'idée sous-jacente : dans quel lieu je me trouve et en fonction du lieu où je me trouve comment réagir ? Comment décider ? Comment communiquer ? C'est toute la question autour de Cynefin.  

## Modèle SECI

Une inspiration ? Dave Snowden a fait émerger CYNEFIN avec d'autres chez IBM quand il travaillait dans la gestion de la connaissance (KM). Il m'a tellement répondu rapidement "non, cela n'a pas de lien", que je ne peux m'empêcher de penser qu'il y en a un. On y retrouvez un quandrant, une dynamique, l'idée du connu et de l'inconnu. 

https://en.wikipedia.org/wiki/SECI_model_of_knowledge_dimensions

![SECI MODEL](/images/2020/04/SECI_Model.jpg)

* Tacite vers tacite (socialisation) : La connaissance est là, elle se passe et partage de façon informelle, socialement, des conversations, des événements, etc. 
* Tacite vers explicite (externalisation) : On traduit cette connaissance implicite en connaissance explicite : un livre, un produit, etc. 
* Explicite vers explicite (combinaison) : On agrège ces connaissances autant externe qu’interne pour provoquer un enrichissement. 
* Explicite vers tacite (internalisation) : Une nouvelle connaissance, provoquée généralement par l'action (*learning by doing*) émerge, elle est une force pour l'organisation et se ramène aux individus. Elle émerge grâce à une capacité de voir des connexions, de voir les choses sous un nouvel angle, de faire des corrélations inattendues, etc.  


## CYNEFIN

Je vous propose d'écouter le meetup, les différents éléments visuels que j'utilise ou que j'évoque sont ci-dessous. 

{{< youtube id="nhLkuhTjTAM" >}}

(Si vous avez des problèmes d'affichage, [le lien direct](https://vimeo.com/407467942)

### Cynefin de Dave Snowden

![Cynefin](/images/2020/04/cynefin.png)

### Extrait du jeu Cynefin Lego Game de Agile42

![Cynefin](/images/2013/04/Cynefin-AT.jpg)

### Les grands principes

![Cynefin](/images/2020/04/cynefin-1.png)1

### Comment se déroule la communication (faiblesse ou force de la centralisation, faiblesse ou force de la distribution)

![Cynefin](/images/2020/04/cynefin-2.png)

### Notez l'importance de la question : y a-t-il des inconnus ? Aurons-nous les réponses ? 

![Cynefin](/images/2020/04/cynefin-5.png)

### Extrait du document indiqué en bas de page de la Harvard Business Review de 2007 par Snowden & Boone (et traduit en français dans l'article ci-dessous aussi). 

![Cynefin](/images/2020/04/cynefin-6.png)

Le même tableau en français [ici](/2013/04/cynefin-et-son-lego-game/).

## Dynamiques CYNEFIN


![Cynefin Dynamics](/images/2020/04/cynefin-3.png)

* Dynamique 1 - **"collapse, l'effondrement"** : quand on ne perçoit le changement d'organisation on risque de tomber du "simple, évident" au chaotique. Et on tombe subitement, de façon inattendue. Plus on se voile la face, plus la chute est douloureuse. 

* Dynamique 2 - **"imposition"** : imposition draconienne, très en force, d'une règle et d'une application. Le capitaine à la tête de son bateau qui coule. La situation est tellement catastrophique que des gens acceptent ce qu'ils n'acceptaient pas auparavant. Problème : cela impose une stabilité plus rigide que ce qui existait précédemment. Et cela peut générer un cercle vicieux avec l'effondrement. 

* Dynamique 3 - **"incremental improvement, amélioration incrémentale"** : On améliore les pratiques courantes avec de l'expertise qui elle-même s'améliore. 

* Dynamique 4 - **"Exploration"** : On enlève souvent un point de contrôle, on laisse beaucoup de confiance, et on essaye de faire émerger de nouvelles façons de faire. 

* Dynamique 5 - **"Just In time ou Exploitation"** : On décide de mettre en oeuvre les nouvelles pratiques émergentes de façon prédictible, au bon moment, pas trop tôt, pas trop tard. 

* Dynamique 6 - **"Swarming, fourmillement"** : Quand la sortie du chaos se fait par grand groupe de façon émergente (*a contrario* du *command and control* de l'imposition). On observe quel "attracteur" attire le groupe, quel point fait émerger une dynamique. 

* Dynamique 7 - **"Divergence - Convergence"** : Une façon de dynamiser l'émergence. Pour beaucoup d'organisation dans le domaine complexe, allez et venir dans le chaos est beaucoup facile que de passer dans le compliqué et peut s'avérer enrichissant. 

![Cynefin Dynamics](/images/2020/04/cynefin-4.png)

* Dynamique 8 - **"Entrainment breaking - entraînements à la rupture"** : Périodiquement on bascule volontairement dans le chaos pour observer si de nouvelles façons de faire émergent.

* Dynamique 9 - **"Libération"** : L'organisation essaye de voir si elle peut inventer de nouveaux systèmes. Passer dans le complexe doit amener beaucoup de croisements et de mélanges pour favoriser cette émergence. 

* Dynamique 10 - **"Immunization - Immunisation"** : On secoue nos habitudes temporairement pour voir si elle résiste.   

## Mes sources

* Cet article en 2013 : [Cynefin et son lego game](/2013/04/cynefin-et-son-lego-game/)
* Ce document de [Snowden & Boone](/pdf/Cynefin-Mary-Boone.pdf)
* Ce document de [Snowden & Kurtz](/pdf/snowden-kurtz.pdf)


