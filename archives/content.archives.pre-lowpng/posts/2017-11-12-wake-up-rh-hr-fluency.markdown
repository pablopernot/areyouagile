---
date: 2017-11-12T00:00:00Z
slug: wake-up-rh-hr-fluency
tags: ['wake','up','rh','hr','fluency','agilefluency']
title: 'Wake up rh : HR fluency'
---

Je vous ai déjà pas mal parlé de **agile fluency**, dans le monde agile un modèle qui ne dicte pas comment agir mais qui clarifie les idées. Dans mon accompagnement il m'aide à rappeler plusieurs points essentiels.  

* **Bénéfices / investissements**. Pour tout bénéfice il faut généralement un investissement. Les gens recherchent généralement un bénéfices, sans vouloir investir ou sans lâcher par ailleurs quelque chose. C'est bien de rappeler qu'il faut un investissement. 

* Il y a plusieurs “lieux” dans Agile Fluency, il faut aller dans **celui qui nous correspond** (cela nécessite de clarifier notre intention). Inutile pour une (grosse) banque -- par exemple -- d’avoir la souplesse “timetomarket (réactivité au marché)” d’une startup peut-être car son marché est assez captif. Je dis bien peut-être. Si je me réfère à Agile Fluency, si on veut être bon dans son domaine on peut envisager atteindre par exemple le lieu "délivrer de la valeur quand cela a de la valeur" et organiser son dispositif avec du *near-shore* ou *offshore* puisque c'est/c'était la mode. Mais si on veut être le meilleur dans son domaine, il faut chercher à atteindre le lieu de "optimiser la valeur". Dès lors le *near-shore* ou le *offshore* ne font plus vraiment sens car on recherche une dynamique autour d'un groupe pluridisciplinaire qui permettra d'optimiser la valeur".  C'est utile de rappeler cela aussi. Vous obtenez les bénéfices liés à votre investissement, vous devez donc vous interroger sur votre intention.   

* "Fluency" veut dire parler couramment une langue. Avec **aisance** rappelle pertinemment [Pauline](https://le-lab-de-pauline.com/). Le français je le parle avec aisance, l'anglais moins, l'espagnole il me faut un dictionnaire dans la poche. Mais si je ne parle pas anglais durant plusieurs mois je vais perdre ma connaissance de la langue. Pour nos pratiques c'est pareil, on doit s'interroger si on les parle avec aisance ou pas. On doit se rappeler que l'on peut les oublier, perdre notre facilité. Bien souvent certaines personnes m'expliquent qu'elles pratiquent telle ou telle démarche. Mais le font-elles avec **aisance** ? Aisance cela veut dire quoi dans un quotidien ? Cela représente probablement un aspect qualitatif et quantitatif (récurrence de l'usage). Cette question de l'aisance soulève de nombreuses et bonnes interrogations. Il est très utile dans une démarche "Agile Fluency" de classifier les pratiques que l'on souhaite pour atteindre ce lieu cible. Mais ces pratiques sont-elles acquises ? en cours d'acquisition ? non désirées ? Et que signifie "acquises" ? Préciser tout cela, le suivre, c'est introduire de la **rigueur**, de la **discipline** et de la clarté dans la lecture de notre **trajectoire agile**. C'est utile de rappeler cela.

* C'est important de ramener des gens à des **réalités**. Dans les observations de Diana Larsen et James Shore, il y peu d’entreprises qui bénéficient des promesses de l'agile. Les durées évoquées pour une vraie trajectoire agile sont éloquentes : de 6 mois à 5 ans, et puis rien ne s'arrête. C'est utile de rappeler cela. 

## Wake Up RH 

Au travers de [Wake Up Rh](http://wakeuprh.fr), avec [Pauline](https://le-lab-de-pauline.com/), nous avons récemment décidé d'étendre la lecture "Agile Fluency" aux aspects RH, notamment par une utilisation de cette approche au sein de [beNext](http://benextcompany.com).

Nous proposons une lecture d'un cheminement RH que nous appelons pour l'instant "HR Fluency", très inspiré donc de [Agile Fluency](/2017/04/perspective-agile-fluency/). 

## Identité

Nous avons focalisé notre approche sur l'idée de l'**identité**. Très importante pour une perspective RH. Elle implique l'idée de culture et d'expérience. 

![HR Fluency](/images/2017/11/hr-fluency-0-2.png)

Le cheminement RH que nous proposons dans notre approche "HR Fluency" est le suivant (différemment de Agile Fluency ce ne sont pas nécessairement des étapes forcément liées, nous parlons de celles de HR Fluency plus comme des paliers d'un même cheminement) : 

- Première étape,  **Pas d'identité connue** : les activités RH sont administratives, peu importe l'identité du groupe, la paie, la gestion des absences, etc.    
- Deuxième étape, **l'identité est connue**. On a une identité commune et on cherche à fédérer autour (recrutement, valeurs, etc.). Par exemple par nos messages ou nos façons de recruter, par exemple au travers du suivi de nos collaborateurs, etc. 
- Troisième étape, **l'identité est partagée**.  Cette identité est vécue et transmise à tout moment dans l'organisation (fidélisation, épanouissement, etc.). Par exemple, un suivi régulier des collaborateurs, des événements co-crées par les membres du groupe, la cooptation rendue possible, la pratique de ce que l'on prêche (cercles holacratie, etc.). 
- Quatrième étape, **l'identité est globale, elle dépasse le cadre du groupe**. Cette identité est vécue au delà de l'organisation (plus de frontière, partage, *wholeness*). Par exemple : partage des pratiques à l'extérieur, article de blog personnel parlant de la marque employeur (de l'organisation), événements mixtes internes/externes, etc.      

Là encore nous proposons une grille de lecture qui permet de s'interroger sur où on est est et surtout où l'on souhaite se trouver. Quelles sont les pratiques qu'il faut mettre en œuvre pour parler couramment cette étape ("fluency", avec aisance). Lesquelles sont à acquérir ? Lesquelles sont en cours d'acquisition ? Lesquelles sont acquises et quelles mesures nous permettent de dire que c'est le cas (qualitative et fréquence) ? 

## Lire ses pratiques au regard de HR Fluency


![Board Fluency](/images/2017/11/board-fluency.png)

Où souhaite se rendre votre organisation sur un plan RH ? Voici les étapes de l'utilisation de HR Fluency.  

**État des lieux**

Faisons la liste des pratiques du département RH (et au delà, qui sont rattachées à l'identité, à l'**expérience** de l'organisation). On observe la liste des pratiques qui remontent, on observe où elles sont jugées être (en cours, acquises, etc.). 

**Challenge de l'emplacement des pratiques** 

Il arrive que les gens ne placent pas nécessairement les pratiques dans le bon lieu. On harmonise notre point de vue et notre vision de chaque pratique : investissement, sens, bénéfices. Le recrutement peut être effectué de plein de façons différentes. Met-ils en œuvre l'identité de l'organisation ? La fait-il vivre ? Le recrutement est-il une étape uniquement lié au département RH ou à d'autres groupes ? Est-ce connu ou vécu ? Etc. 

**Définition de en cours, et de fini** 

On acte que des meetups co-construits sont réalisés. Ils dynamisent la structure. Ils épanouissent les personnes. Mais cette pratique est-elle positionnée dans "en cours" ou dans "acquises". La différence est grande. Que signifie avoir de l'aisance sur la co-construction de meetup ? La fréquence ? La multiplicité des intervenants internes ? La qualité ? On note souvent l'absence d'une réelle acquisition de nombreuses pratiques (la grande majorité des pratiques est "en cours"), et ainsi de ne pas **pouvoir bénéficier réellement de celles-ci**.  

Questions précises ou avec de la perspective : on veut par exemple atteindre le lieu "wholeness", comment étendre au delà de l'organisation nos écrits ? Ou nos événements festifs ? 

Il faut un certain nombre de pratiques pour aussi bénéficier des investissements: une seule pratique ne suffit pas pour atteindre un lieu. La densité est importante. Pour reprendre le parallèle avec une langue : il faut assez de mots pour parler une langue, pour constituer la communication. Une **certaine densité**.

**Prendre du recul** 

Ces tableaux de pratiques avec en cible un lieu "HR fluency" aide à clarifier le quoi le pourquoi, mais aussi le comment, et les aspects qualitatifs du comment. Ils permettent d'avoir une mesure intelligente et adapté de l'état des lieux de votre identité, de votre culture. On note les pratiques non désirées et pourquoi. C'est aussi révélateur de votre identité et de votre désir en tant qu'organisation. 

Notre prochain petit déjeuner [wake up rh](http://wakeuprh.fr) va nous permettre d'évoquer toutes ces pratiques avec vous. 

Cliquez sur le tag ci-dessous pour les autres articles "agile fluency"

