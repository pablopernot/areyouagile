---
date: 2013-04-02T00:00:00Z
slug: cynefin-et-son-lego-game
tags: ['atelier','workshop','seriousgame','emergence','complexite','complexe','cynefin']
title: Cynefin et son Lego game
---

Pour continuer dans la série des jeux innovants qui gravitent autour de
l'agile, je souhaite vous proposer cette fois le [Cynefin Lego Game de
Agile42](http://www.agile42.com/en/blog/2011/12/25/cynefin-lego-game/).
J'avais commencé (en 2011 ?) à évoquer Cynefin, mais avec peu de talent,
je m'en rend compte aujourd'hui. Je ne l'avais pas abordé de la bonne
façon. J'estime et espère avoir progressé depuis, et je pense avoir
mieux agencé ces idées dans ma tête. Cynefin est un socle de réflexion
très intéressant qui s'adresse aux managers, leaders ou autres personnes
dans la prise de décision, dans les pratiques à mettre en oeuvre pour
répondre à telle ou telle situation. Je trouve cela assez passionnant.

Pour vous intéresser à la chose, je vous propose de vous plonger dans le
[Cynefin Lego
Game](http://www.agile42.com/en/blog/2011/12/25/cynefin-lego-game/)
(j'ai pour ambition de le proposer à différents évènements dans les mois
à venir, ou dans [mes formations](/pages/formations.html)), car rien ne
vaut un jeu pour apprendre ; Confucius aurait dit : "dis-moi et
j'oublierai, montre-moi et je me souviendrai, implique-moi et je
comprendrai". Ce à quoi Aristote aurait répliqué : "Il faut jouer pour
devenir sérieux".

Pour bien comprendre l'intérêt de ce jeu je vous suggère la lecture de
cette article de [Mary Boone](/pdf/Cynefin-Mary-Boone.pdf)[^mary].
J'en traduis le tableau récapitulatif ci-dessous (et j'en traduirai
l'intégralité dès que possible).

[^mary]: L'article original n'est plus accessible il se trouvait là : http://www.mpiweb.org/CMS/uploadedFiles/Article%20for%20Marketing%20-%20Mary%20Boone.pdf. J'avais récupéré le pdf que vous avez [en lien](/pdf/Cynefin-Mary-Boone.pdf). 

## Traduction 1 : Prise de décisions selon les contextes, le guide du leader

Extrait de l'article : a leader's framework for decision
making[^mary] (Cadre de prises de décisions pour les leaders).

### Simple 

**Caractéristiques du contexte** 

- Des motifs qui se répètent à intervalles réguliers et avec cohérence.
- Des relations de cause à effet évidentes aux yeux de tous. La bonne réponse coule de source.
- Il n’y a pas d’inconnu, la réponse est connue. (known knowns)
- Management basé sur des faits, des indicateurs.

**Le boulot du leader**

- Observer, catégoriser, répondre
- S’assurer que les bons processus sont en place.
- Déléguer
- Utiliser les meilleures pratiques (best practices).
- Communiquer de façon claire et directe.
- Comprendre qu’une communication interactive poussée n’est pas forcément nécessaire.

**Signaux d’alertes**

- Complaisance et confort.
- Volonté de rendre complexe les choses simples.
- Automatismes.
- Attention aux habitudes, à la mémoire du muscle, sans réflexion.
- Rester fixé sur les meilleurs pratiques (habituelles) malgré le fait que le contexte ait changé.

**Réponses aux alertes**

- Créer des flux de communication pour permettre une remise en cause de l’orthodoxie.
- Rester connecté (au courant) sans faire de micromanagement.
- Ne pas s’imaginer que les choses sont simples.
- Comprendre les bénéfices et les limites des meilleures pratiques (best practices).

### Compliqué

**Caractéristiques du contexte** 

- Diagnostiques d’experts sont nécessaires.
- Les relations de cause à effet peuvent être trouvées mais ne sont pas évidentes, demandent de l’expertise. Plusieurs bonnes réponses sont possibles.
- Il y a des inconnus, mais les réponses pourront être connues. (known unknowns)
- Management basé sur des faits, des indicateurs.

**Le boulot du leader**

- Observer, Ou si tu veux analyser, répondre
- Créer des panels d’experts.
- Ecouter les avis contraires.

**Signaux d’alertes**

- Les experts sont trop sûrs de leurs solutions, ou de l’application d’une solution antérieure.
- Paralysie durant l’analyse.
- Pas de consensus au sein du panel d’experts.
- Les points de vues des non experts sont éludés et exclus.

**Réponses aux alertes**

- Pousser les parties prenantes ou les gens externes à remettre en question les avis d’experts, et combattre les habitudes (la mémoire du muscle).
- Utiliser des jeux ou des ateliers pour pousser les gens à penser différemment.


### Complexe

**Caractéristiques du contexte** 

- Flux non prévisibles d’événements, d’informations, etc.
- Pas de bonne réponse, des motifs instructifs vont émerger.
- Il y a des inconnus, on ne sait pas si les réponses pourront être connues. (unknown unknowns)
- De nombreuses idées en concurrence.
- Besoin d’approches créatives et innovantes.
- Management basé sur des motifs, des schémas de pensée, des pratiques.

**Le boulot du leader**

- Essayer, observer, répondre
- Créer des environnements qui permettent à des pratiques d’émerger.
- Augmenter le niveau d’interaction et de communication.
- Utilisation des méthodes qui permettent l’émergence de nouvelles idées : brainstorming, discussions ouvertes, définir un cadre (NdT: pour libérer la pensée à l’intérieur de celui-ci), stimuler les relations, encourager la dissidence et la diversité, gérer les conditions de départ (NdT: proposer un terreau riche à la réflexion), rechercher, traquer les innovations, les idées émergentes.

**Signaux d’alertes**

- Tentation de retomber dans les habitudes de “command &amp; control”.
- Tentation d’observer les faits, les indicateurs, plutôt que de laisser émerger les nouvelles pratiques.
- Volonté d’accélérer la résolution des problèmes ou d’exploiter les pistes émergentes de résolution.

**Réponses aux alertes**

- Être patient et laisser le temps de la réflexion.
- Soutenir les approches qui encouragent une forte interaction et ainsi favorisent les pratiques émergentes.

### Chaotique

**Caractéristiques du contexte** 

- Grosses turbulences.
- Pas de cause à effet, rien ne semble lié, on ne sait pas où trouver la bonne réponse.
- On est dans l’inconnu. (unknownable)
- Beaucoup de décisions à prendre et pas de temps pour réfléchir.
- Fortes tensions.
- Management basé sur des motifs, des schémas de pensée, des pratiques.

**Le boulot du leader**

- Agir, observer, répondre
- Observer ce qui fonctionne plutôt que de chercher des réponses toutes faites.
- Prendre des décisions, agir immédiatement (command <span class="amp">&amp;</span> control), pour rétablir l’ordre.
- Proposer une communication claire, succincte et directe.

**Signaux d’alertes**

- Appliquer une approche “command and control” trop longtemps, plus que nécessaire.
- Culte du leader.
- Rater des opportunités d’amélioration au travers de nouvelles pratiques.
- Persistance du chaos.

**Réponses aux alertes**

- Mettre en place des mécanismes (comme deux équipes en parallèle sur un même sujet) pour tirer parti des opportunités offertes dans cet environnement chaotique.
- S’entourer de conseillers pour remettre en question son point de vue une fois la crise passée.
- Travailler pour basculer le contexte de chaotique à complexe.

Maintenant je vous propose une mise en pratique au travers d'un jeu.

## Traduction 2 : Cynefin Lego Game par Agile 42

Traduction de l'article : [Cynefin Lego Game](http://www.agile42.com/en/blog/2011/12/25/cynefin-lego-game/)

### Contexte

Le jeu Cynefin Lego fait parti de la formation au management agile par
[Agile42](http://www.agile42.com). Il est sous licence Creative Commons
Attribution-Share Alike 3.0.

### Qu'est ce que c'est ?

Un jeu pour vous permettre d'expérimenter les 5 domaines du système de
pensée Cynefin de Dave Snowden.

![Cynefin](/images/2013/04/500px-Cynefin_framework_Feb_2011.jpg)

En utilisant des Lego, vous serez confrontés à 4 exercices dont la
problématique et le contexte sont en relation avec un système simple,
compliqué, complexe ou chaotique. Bien que ce jeu ne vous propose pas un
aperçu complet du canevas "faiseur de sens" (sense-making), il est bien
adapté pour une première impression, et vous poussera peut-être à
vouloir en savoir plus ! Enfin si Cynefin est au coeur de l'atelier, les
conclusions et les discussions ne seront pas nécessairement liées à ce
modèle.

### Pourquoi l'utiliser ?

Quand vous menez ou gérez des transitions agiles, c'est important de
savoir face à quel type de système vous vous trouvez. Jouer à ce jeu
vous permet de savoir décoder ce qui se produit en terme de structures
organisationnelles et de communication dans le système auquel vous êtes
confrontés. Une fois cette analyse opérée, vous adapterez votre
communication en cohérence avec le système ainsi décodé, et vous
proposerez des outils mieux adaptés.

![Cynefin](/images/2013/04/Cynefin-AT.jpg)

### Durée

Le jeu ne devrait pas excéder 60 minutes.

### Matériel et environnement

Des tables pour les groupes de 3 ou 4, à peu près 200 briques de Lego de
6 à 10 couleurs et de différentes tailles (et des briques spéciales :
fleurs, roues, ...) par table.

### Instructions

Dessinez le diagramme sur une feuille (paperboard). Tracer seulement les
lignes au milieu, vous remplirez le diagramme au fil des exercices (les
gens devraient expérimenter et conceptualiser les domaines avec vous).

Vous jouez un exercice par domaine, comme décrit ci-dessous, pour chaque
exercice :

-   Expliquer les règles jusqu'à ce qu'il n'y ai plus de question.
-   Démarrer l'exercice en même temps sur toutes les tables.
-   Chronométrez mais n'imposez pas de temps maximum.
-   Notez le temps le plus court, et le plus long.
-   Quand toutes les équipes ont finies, donnez leur 2 mn de
    rétrospective et de discussion.
-   Partagez les conclusions et faîtes un debrief.
-   Remplissez le domaine dans le diagramme en relation avec les
    conclusions des équipes.
-   Dessinez les modes de communications et les façons de prendre des
    décisions observés durant l'exercice.

### Exercice 1: Simple

#### Le challenge

Ordonnez les briques par couleurs, aussi vite que possible. Créez un tas
dédié à tous les éléments spéciaux. Décidez au sein de votre équipe
quels sont les éléments spéciaux.

![Cynefin](/images/2013/04/Simple-Sorting.jpg)

#### Debriefing

-   Combien de temps passé à se préparer ?
-   Comment s'est déroulée la communication ? Combien de leaders et
    d'exécutants dans l'équipe ?
-   Vous noterez l'apparition de meilleures pratiques (best practices)
    dans chacun des groupes.
-   Analysez les modes de communication et la façon dont les gens ont
    pris des décisions et se sont mis d'accord. Vous devriez noter un
    motif très directif de haut en bas (top/down), une personne a
    probablement proposé "LA" façon de résoudre le problème et les
    autres ont suivis. Pas beaucoup de communication en binome durant
    l'exercice, en tous cas pas concernant comment faire les choses,
    mais plutôt concernant des aspects opérationnels.

### Exercice 2: Compliqué

#### Le challenge

Fabriquer une structure aussi vite que possible selon les règles
suivantes :

-   Au moins 20 briques de hauteur.
-   Un motif régulier de couleurs.
-   Chaque nouvel élément ajouté à la structure doit être plus petit que
    celui qui se trouve en dessous de lui.

![Cynefin](/images/2013/04/Complicated-Structure.jpg)

#### Debriefing

-   Qu'avez vous ressenti comparé au problème "simple" ?
-   Combien de temps passé à se préparer ?
-   Comment s'est déroulée la communication ? Combien de leaders et
    d'exécutants dans l'équipe ?
-   Dans ce cadre vous devriez observer que chaque équipe a adopté des
    pratiques différentes, produisant des résultats différents. Il ne
    s'agit donc pas de "meilleures pratiques" (best practices) mais de
    "bonnes pratiques" (good practices).
-   Analysez les modes de communication et la façon dont les gens ont
    pris des décisions et se sont mis d'accord. Vous devriez voir un
    motif "communication d'expertise" : où chacun essaye de proposer sa
    solution. Des équipes peuvent à ce stade entrer dans une phase de
    paralysie par l'analyse (analysis-paralysis), considérez ceci comme
    une invitation à discuter de l'opportunité d'un rôle de facilitateur
    au sein de l'équipe.

### Exercice 3: Complexe

#### Le challenge

Décider en 30 secondes de fabriquer soit un animal, soit un véhicule.
Après cela vous travaillerez selon les règles suivantes :

-   Comme dans l'exercice 2 vous devrez proposer un motif régulier de
    couleurs
-   Chaque couleur (de brique) sera uniquement manipulée par un seul
    membre de l'équipe.
-   Vous n'êtes pas autorisé à parler !
-   Chaque minute, vous devez changer de table, en prenant avec vous
    votre travail, mais pas le matériel !

#### Debriefing

-   Qu'avez vous ressenti comparé au problème "simple" ou "compliqué" ?
-   Comment s'est déroulée la communication ? Combien de leaders et
    d'exécutants dans l'équipe ?
-   Quel type de retour d'information avez vous eu pour vous guider vers
    la solution ?
-   Est-ce que cela aurait fait une différence si vous aviez eu 5
    minutes pour discuter et vous préparer avant de commencer à
    construire ?
-   Ici vous devriez noter facilement des comportements émergents,
    beaucoup de personnes sont surprises de découvrir que l'interdiction
    de parler leur a permis d'éviter de longues et vaines discussions,
    et à l'inverse que se mettre à fabriquer quelque chose ensemble à
    fait jaillir de nouvelles idées et de l'inspiration à chaque étape.
    Comparez avec le temps passé lors des précédents exercices,
    normalement il ne devrait pas trop différer, malgré le fait que le
    challenge soit complexe.

### Exercice 4: Chaotique

#### Le challenge

L'objectif et le cadre sont les mêmes que dans l'exercice précédent,
mais vous devez construire une maison, un immeuble ou une usine. De
plus, de façon aléatoire, le facilitateur touchera l'épaule d'un membre
du groupe et indiquera une autre table. Cette personne devra
immédiatement rejoindre cette autre équipe.

![Cynefin](/images/2013/04/Chaotic-Half-House.jpg)

Il n'est toujours pas permis de parler. A chaque équipier que vous
faîtes quitter l'équipe, vous devriez en proposer un nouveau qui la
rejoint, mais pas immédiatement. (Cela les conduira à penser qu'il leur
manque une personne pour gérer une certaine couleur, ce qui n'est pas
vrai, si vous lisez les règles. Ne leur dîtes pas que cela n'est pas
vrai).

#### Debriefing

-   Qu'avez vous ressenti comparé au problème "simple" ou "compliqué" ou
    "complexe" ?
-   Comment s'est déroulée la communication ? Combien de leaders et
    d'exécutants dans l'équipe ?
-   Comment avez vous perçu le fait de perdre un membre de l'équipe ?
    Comment vous êtes vous intégré à une autre équipe ?
-   Vous devriez être en mesure d'apprécier que la communication est
    devenue quasiment inexistante, que les personnes ont abandonné
    l'idée de communiquer, elles préfèrent commencer à faire des choses
    (et se font arrêter par d'autres (agir et observer)). Notamment, les
    personnes devraient se sentir -contrairement à l'exercice précédent-
    complètement démotivées et frustrées.

### Notes

L'exercice simple propose habituellement une solution rapide. Un joueur
propose quelque chose d'évident, et les autres suivent.

L'exercice compliqué demande un peu de planification, typiquement tout
le monde suggère une solution, une décision rapide est prise et le
processus est adapté selon ce qui se produit durant la fabrication.

L'exercice complexe : son résultat n'est pas meilleur avec une plus
longue préparation. Le bon processus émerge et ne cesse de s'adapter. Le
plus tôt l'équipe commence à construire, le plus tôt elle se sent à
l'aise. Cela aide si il y a une convergence au sein de l'équipe sur
l'apparence de l'animal ou du véhicule que l'on est en train de
construire.

L'exercice chaotique mène à de drôles de surprises, parfois à de mauvais
résultats (regardez l'immeuble sans toit sur l'image). Les personnes ne
se sentent pas à l'aise tout le temps, la solution met du temps à
émerger. Surtout pour les managers c'est parfois une découverte : "c'est
donc ainsi que l'on se sent quand on change d'équipe..."

### Links

Cynefin framework: <http://en.wikipedia.org/wiki/Cynefin>

Dave Snowden’s blog: <http://www.cognitive-edge.com/blogs/dave/>
