---
date: 2009-09-20T00:00:00Z
slug: scrum-un-ecueil-pour-les-ssii-et-kanban
tags: ['scrum','kanban','ssii','esn']
title: 'Scrum : un écueil pour les SSII ? et Kanban ?'
---

Appliquer Scrum n'est pas simple, surtout pour les SSII. L'un des
principaux facteurs dont je souffre actuellement est la stabilité des
équipes. les impératifs de production, eux-mêmes menés par des
impératifs financiers règnent trop souvent en maîtres, et c'est normal.
C'est aussi le signe me direz-vous que les processus et les méthodes ne
sont pas assez matures puisqu'au moindre coup de vent (un gros contrat
signé, une régie déclenchée pour un élément clef de l'équipe, etc.) ils
volent en éclats. Je ne peux pas vous contre-dire. Je n'ai pas eu la
chance de connaitre une entreprise qui -quelque soit le contexte (la
crise par exemple)-  soit capable de maintenir coûte que coûte
l'intégralité de ses processus tout en les faisant progresser (ces
derniers mots sont importants sinon c'est la fossilisation qui vous
guette).

Aujourd'hui beaucoup de SSII se jettent sur les méthodes agiles car
elles ont le vent en poupe. Elles se heurtent à de nombreuses
problématiques avec, pour n'en citer que quelques unes :

-   la contractualisation (le  problème majeur actuellement, puisque ce
    n'est que le début de leur offre dans ce domaine, et que c'est un
    élément très fondateur,),  ils leur faut trouver une troisème voie
    entre le forfait et la régie (merci Christophe).
-   la redistribution des rôles : le client (product owner) est
    responsable, l'équipe (team) est responsable, le Chef de projet
    disparaît, il était le responsable, le fusible, Atlas qui portait le
    projet (accessoirement avec le directeur de projet) ; le Scrummaster
    apparaît, il est un facilitateur, mais nullement un responsable.
-   la stabilité des équipes durant les sprints, les itérations, est
    nécessaire.
-   la maturité des clients et des équipes, personne n'est
    interchangeable, chaque collaborateur est différent d'un autre, etc.

Les SSII entament donc un difficile périple. Aujourd'hui si je regarde
l'un des projets que je pousse vers l'agilité je me heurte de plein
fouet à la difficulté de la stabilité de l'équipe. Il ne s'agit pas d'un
projet client, mais d'un projet interne. Il est d'autant plus saccadé.
Sa priorité est difficile à établir. Les ressources affluent (fin de
projets clients) et elles refluent (signature de projets clients). Moi
même je ne peux avoir qu'une activité chaotique à son encontre.
Appliquer les fondements de Scrum dans ce contexte devient très
compliqué. Mais l'agilité c'est aussi une dynamique de l'adaptation. Je
me tourne donc aujourd'hui pour ce projet vers Kanban.

Pourquoi ? Il me propose des valeurs qui sont en adéquations avec mes
besoins (certains sont aussi associées à Scrum), ne pas développer de
fonctionnalité que personne ne va utiliser, ne pas écrire plus de specs
qu'il n'y aura de code, ne pas écrire plus de code que je ne puisse
tester, ne pas tester plus de code que je ne puisse déployer. Très
concrètement ne pouvant bâtir une équipe stable, ne pouvant garantir de
*daily scrum* journalier, n'ayant pas assez de garanties sur la
pérennité de mon équipe, etc. je vais me focaliser sur ce que j'imagine
être l'essentiel de la pratique Kanban et à partir de là reconstruire un
processus agile (si cela marche). Soyons clair, il permet surtout de
simplifier excessivement le processus, et si cela fonctionne il me
permettra de rebondir sur quelque chose de plus puissant. Ceci dans le
cadre de ce projet.

Je ne sais plus où (ah si, [ici](http://www.agileproductdesign.com/blog/2009/kanban_over_simplified.html)) 
j'ai lu cet exemple mais en gros, de façon imagée : je pose des portes
sur une voiture (Kanban vient -encore-de Toyota), j'ai un tas de 10
portes devant moi, lorsque j'arrive à la 5ème porte je vois une
étiquette sur la voiture (étiquette = kanban en japonais). L'étiquette
m'indique que je dois prévenir la personne qui fabrique les portes de me
fabriquer 10 portes supplémentaires. Je trouve cette personne et lui
demande 10 portes supplémentaires. Elle arrête la tâche qu'elle était en
train de réaliser, et commence à fabriquer 10 portes supplémentaires.
Elle savait que je passerais, mais travaillait sur autre chose. Je
retourne à mon assemblage. Lorsque je finis presque mon tas de 10
portes, la personne a qui j'avais demandé les nouvelles portes arrive,
elle dépose les portes, et naturellement, sur la 5ème, au milieu du tas,
une petite étiquette est présente.etc.

Ici, sur ce fameux projet interne, je dispose d'un backlog et d'une
équipe fluctuante dont la disponibilité est fluctuante.Ma difficulté est
le WIP (*Work in progress*). Je ne dois pas avoir un membre de l'équipe
qui démarre quelque chose et qui malheureusement ne peut l'achever (pour
x raisons). Sinon je me retrouve avec une tâche de coordination (du
nouveau membre remplacé par l'ancien), de transfert de compétences (du
nouveau ...) , et potentiellement de rework (le nouveau membre de
l'équipe aborde différemment la résolution de sa tâche). Bref 3 plaies
essentielles sources de gaspillage.

Mon objectif est de définir des User Story (ou MMF : minimal marketable
feature) assez compactes et assez indépendantes les unes des autres pour
qu'elles puissent être abordées unitairement et assez rapidement : pour
ne pas risquer de perdre l'acteur qui l'a prise en charge, pour qu'il
puisse appréhender la MMF rapidement. Avoir assez de visibilité sur
l'ensemble des MMF pour qu'elles puissent s'enchainer et constituer de
la valeur ajoutée. Faire des livraisons plus rapidement, plus rythmées.
Quand un flux régulier sera mis en place j'aurais mis en place un
premier niveau d'efficacité. L'objectif de ce flux est de réussir à
créer un rythme et une régulation entre les différentes tâches liées au
projet, en limitant au maximum le "work in progress" car c'est lui qui
m'empêche de livrer régulièrement et qui rigidifie la marche du projet.
Chaque tâche s'emboite avec d'autres, ou peut en déclencher d'autres
sans que les personnes qui aient la charge de ces autres tâches ne
soient impactées. Elles ont donc du "temps libre" pour leurs autres
activités (sachant qu'ici le procédé est inversé, c'est quand elles ont
du temps libre qu'elles se consacrent au projet "interne") . Enfin il
est plus aisé d'intervenir sur le projet quand chaque tâche est plus
compacte (ceci n'est malheureusement pas si simple et pas si souvent
vrai).

D'ici là, et si j'atteins ce but... nous en reparlerons.

Pour en savoir plus sur Kanban, appelez Google, ou utilisez cette page
de cet excellent blog : [targetprocess : kanban](http://www.targetprocess.com/blog/2009/05/lean-and-kanban-software-development.html).
