---
date: 2011-09-10T00:00:00Z
slug: are-you-experienced
tags: ['coach','coaching','coachagile']
title: Are you experienced ?
---

Bon sang que ce billet a du mal à sortir. Il a été démarré en février
2011, repris en août. Et je sèche j'ai juste l'impression d'écrire un
truc totalement inutile. Mais j'ai besoin d'exprimer ces idées, et je ne
sais pas comment cela sera compris. "Tout le monde s'en fout pablo
crache ton truc". ah ok voilà qui me rassure. allons-y donc.

## Passe ton cycle en V d'abord

Pour encadrer des équipes agiles ou faire du coaching il faut de
l'[expérience](http://www.youtube.com/watch?v=zX0XVEmwlfs) (oui j'ai
décidé d'enfoncer des portes ouvertes).

Et mon avis est que l'on est probablement plus efficace en agile si on a
une expérience du projet classique, ou disons celui qu'on appelle en V
(peut-être à tord : voir les billets de Claude
[ici](http://www.aubryconseil.com/post/2007/01/23/162-le-cycle-de-vie-en-v-n-existe-pas)
et [là](http://www.aubryconseil.com/post/2008/08/20/458-a-la-recherche-du-cycle-en-v)
), car celui-ci constitue un socle non négligeable en terme d'expérience
(dans tous les points de vue).

On est bien plus à même de comprendre certaines orientations de l'agile
en ayant un retour d'expérience sur des projets "traditionnels". Nous
percevons mieux la différence entre ces deux façons de faire et donc les
choix opérés par l'agile. N'oublions pas que ce mouvement a été lancé en
réponse à la façon de faire des projets classiques, le manifeste agile
veut montrer "comment mieux développer des projets". Notre compréhension
de l'agile est meilleure car nous en percevons mieux les fondements et
les raisons en ayant connu son point de référence (ce à partir de quoi
il se positionne): le projet dit classique, en V, en cascade, etc. Comme
le fait un mouvement musical, artistique, etc.

Mais est-ce suffisant pour dire qu'il faut avoir passé son cycle en V
d'abord avant d'entamer l'agile ? Que les gens qui n'ont jamais connu
les projets "classiques" sont handicapés pour faire de l'agile ?
Finalement je doute sérieusement de la validité de mon raisonnement. Ne
serait-ce pas là une réaction de mon système immunitaire de consultant
(vieux ? j'ai eu 40 ans en juin) face à cette marée dépareillée de
jeunes loups ? Toutes ces personnes qui feront de l'agile sans avoir
connu d'autres alternatives ne vont-ils pas garder une
spontanéité rafraîchissante ? Pas de "mémoire du muscle" qui se
rappellera à eux ("merde merde je suis en train d'être trop directif et
d'étouffer ces gars"). Oui d'accord ils n'auront pas de référence, ni
d'expérience, mais peut-être que cela va leur laisser une liberté
d'action que nous n'avons peut-être plus ?

Oui oui et oui ...mais aussi non :  je parle d'encadrement,
d'accompagnement, de coaching.  Pour ces rôles la spontanéité etc. sont
des qualités plus dangereuses qu'il faut savoir maîtriser. Donc oui pour
participer à des projets agiles, mais je suis beaucoup plus mitigé pour
les encadrer et les accompagner. Et en fait je ne fais donc
effectivement qu'enfoncer des portes ouvertes : c'est un plus si on a de
l'expérience pour accompagner ou encadrer des projets agiles. Cette
expérience est peut-être encore plus utile si il s'agit d'avoir réalisé
dans le passé des projets classiques.

  Ecrire un post inutile (mais il fallait qu'il sorte car je ne peux pas
écrire autre chose avant d'avoir fini celui-ci).

Je rebondis sur un autre débat qui va me ramener à la même source :

## Ah mes amis l'agile n'est plus ce qu'il était !

Bon ben mon avis là dessus c'est qu'il ne faut pas trop être critique.
Ce succès est un succès. Ah cette honte : c'est devenue la mode, beurk.
Nous sommes plus l'élite underground, gasp ! Vous ne voulez tout de même
pas que l'agile reste une chose isolée et connue d'un seul groupe d'élus
? (C'est presque ce qu'a sous-entendu Marick lors de sa keynote à XP2011
Madrid, et donc même si son discours se voulait anarchiste, sur ce point
c'était très conservateur à mon avis, enfin c'était ma compréhension de
sa keynote).

Naturellement les choses évoluent et l'agile ne fait pas exception à la
règle (cf par exemple la nouvelle version du scrum book, -que je n'ai
pas encore lu-). De part son succès il est confronté a beaucoup plus de
choses et il doit y faire face. Oui.

Et oui il doit garder son identité naturellement (mais ne pas
s'enfermer).

Et oui il y a des dérives, des tentatives de récupération etc etc il
faut être vigilant. Mais de là à crier aussi fort au loup, je ne sais
pas.

Pourquoi autant de craintes ? Probablement car le "ticket d'entrée" de
l'agile est très bas. C'est sa force et sa faiblesse. Lisez un ou deux
ouvrages, emparez vous de quatre grandes valeurs, et vous voilà le
maître de la mêlée ! Et ça oui c'est très dangereux. Beaucoup
d'apprentis sorciers probablement. Et donc, c'est aussi plein de gens
très volontaires et optimistes qui souhaitent apprendre et aider. Parmi
ces gens il y aura des gens très doués qui font faire des merveilles
très rapidement. Ils sont cependant une minorité il ne faut pas se
raconter des histoires.

Donc je retourne à mon idée première : la force de l'agile est la
facilité son apprentissage par son approche "compacte" (pas la peine de
lire Guerre & Paix) mais il faut compenser ce revers de la médaille en
plaçant des gens expérimentés aux postes clefs : accompagnement,
encadrement, évangélisation, etc.

*So, are you experienced?*

*Have you ever been experienced?*

*Well, I have*

*Let me prove you...*

 