﻿---
date: 2019-02-09
slug: experimenter
tags: [experimenter]
title: Expérimenter
--- 

Expérimenter est une chose importante de nos jours. C'est presque comme une sagesse. Une façon de bien savoir aborder le monde qui nous entoure.

### Expérimenter fait gagner du temps.

Si on est prêt à expérimenter, à faire des expérimentations, il n'est plus nécessaire d'avoir réponse à tout avant de commencer. Faire des expérimentations libère de la tyrannie de la perfection. Si l'on expérimente, l'on essaye, il est inutile d'avoir des réponses par avance. Fini les débats sans fin, fini le temps passé à convaincre tout le monde que c'est la bonne solution, fini le temps à s'interroger sur tout ce qui pourrait arriver. On gagne un temps précieux. La condition c'est que l'expérimentation est courte, bornée dans le temps. Deux mois ? Deux semaines ? Deux jours ?  

### Expérimenter permet de se sentir autorisé.

Si on expérimente, l'erreur est autorisée. Tout le monde se sent bien mieux. Une expérimentation peut marcher comme échouer, c'est explicite. Au travers des expérimentations on se libère de nombreuses contraintes. Avec des expérimentations on fait des choses que l'on n’aurait pas faites dans un autre cadre.   

### Expérimenter permet d'avoir de meilleures réponses.

En expérimentant, on repousse nos réponses à un bien meilleur moment. Les réponses viennent après l'expérimentation. À ce moment où grâce à l'expérimentation on en saura bien plus sur la réalité puisque l'on aura essayé. On aura bien moins d'hypothèses, bien plus de résultats concrets, ceux de l'expérimentation. 

### Expérimenter permet de faire grandir. Ce verbe induit l'observation, l'analyse.

Une expérimentation est une expérience qui appelle une observation. C'est aussi explicite que le droit à l'échec qui l'accompagne. Avons-nous réussi ? Avons-nous échoué ? Cette question demande nécessairement une prise de recul, une réflexion sur ce que serait une réussite ou un échec. Quand on fait quelque chose. On le fait ou pas. Le résultat est de l'avoir fait ou pas. Quand on expérimente, on s'interroge sur l'impact de ce que l'on a fait.   

### Expérimenter permet de faciliter l'appropriation et la conduite du changement.

L'expérimentation est courte, bornée dans le temps. Elle appelle une analyse du résultat. Elle sous-entend que l'échec est possible. Et ainsi le retour en arrière. Elle n'a rien de définitif. On essaye donc réellement, sans frein, sans arrière-pensée, car on sait que le retour arrière est possible. Que rien n'est imposé, que l'essai est temporaire. Il est fondamental ainsi que vos expérimentations soient de vraies expérimentations, qu'elles soient destinées à devenir pérennes que si les résultats sont bons. À cette condition l'appropriation de vos expérimentations et leurs chances de succès seront bien meilleures. 


Pour expérimenter l'expérimentation, il suffit d'arrêter de faire et de commencer à essayer. 
