﻿---
date: 2019-03-15
slug: dojo-agile-a-l-echelle
tags: [echelle, entreprise, chaos, safe, less, synchronisation, risque, transformation, vision]
title: Dojo agile à l'échelle
--- 

Lundi dernier, comme tous les lundis, il y a un *dojo* au *vaisseau*. Le vaisseau c'est les bureaux chez [beNext](https://benextcompany.com). Le dojo c'est le lieu où l'on étudie. Tous les lundis on propose aux équipes sur place 1h sur un sujet porté par quelqu'un qui s'y connaît (sur le sujet justement). Lundi dernier donc c'était un dojo sur "agile à l'échelle" parce que l'on nous interroge souvent (malheureusement) sur SAFe. Vous imaginez bien que je me suis proposé. 

Note : aucun participant n'a été maltraité ou certifié durant le dojo.

Voici le teneur du dojo que je synthétise ici, pour moi, pour vous et pour eux : "une heure sur agile à l'échelle". 

## Entreprise agile et agile à l'échelle

D'abord il convient de bien faire le *distinguo* entre entreprise agile et agile à l'échelle. Les deux peuvent se mêler, mais ce n'est pas la même chose. 

* Agile à l'échelle : répandre au plus grand nombre d'équipes une façon de produire de la valeur (impacte potentiellement uniquement l'appareil de production). Se destine aux grandes entreprises (grandes par la taille). 
* Entreprise agile : Fonctionner dans tous les corps de l'entreprise de façon et avec l'état d'esprit agile. Se destine à n'importe quelle entreprise.  

## SAFe, Nexus, Less, Spotify, Scrum@scale, et tout le fatras

Milieu des années deux mille explosion de Scrum et de Agile dans les têtes. Début des années 2010 jusqu'à 2015 : les grandes entreprises jusque là hautaines, Scrum apparaissant uniquement pour les équipes de façon assez unitaire, daignent y jeter un oeil. Mais elles sont différentes, elles sont grandes, elles sont grosses, il faut comprendre à les entendre : "elles en ont". Il leur faut quelque chose à leur hauteur. Des *frameworks* (des socles tout préparés) de Agile à l'échelle sont là en embuscade pour les servir. Soit ces *frameworks* ont émergé d'une entreprise et au travers de retours d'expérience et de conférences sont devenus des appellations protégées (Spotify), soit c'est carrément des assemblages marketing faits pour séduire à n'importe quel prix (SAFe, voir les liens en bas). 

Ces approches ne sont pas reproductibles en l'état et sont souvent des alibis pour se dire agile. Il faut les regarder avec méfiance. 
Ou s'amuser comme Henrik Kniberg le disait : "J'adore SAFe, j'en utilise 30%" ([pourquoi il faut se méfier de SAFe](/2017/11/pourquoi-faut-il-se-mefier-de-safe/) et [la présentation de Henrik Kniberg chez Octo (merci pour l'invitation à l'époque)](https://blog.octo.com/agile-transformation-with-henrik-kniberg-octo-technology/)). 

Donc oui on vous interroge sur ces *frameworks,* mais c'est un mauvais point de départ. Une vraie entreprise à l'échelle se pose la question de la valeur à l'échelle et pas du comment ([podcast agile à l'échelle chez Café Craft](https://www.cafe-craft.fr/20)). 

## Agile à l'échelle 

Comment ramener l'agile à l'échelle à son essence ? 

### Synchronisation

On commence avec une équipe puis plusieurs. Il faut alors synchroniser ces équipes. L'agile à l'échelle c'est d'abord une question de synchronisation. Pour cela depuis longtemps agile propose des "quarterly planning" (ou autres noms). Durant une ou deux journées, tous les trois mois (par exemple), on synchronise les acteurs de ces équipes. Tous les acteurs sont présents dans un même lieu. Ouverture par exemple par le PM (*product manager*) qui chapeaute ses PO (*product owner*), puis chaque *product owner* présente son *stream* (sa partie du produit, que l'on espère dépendante), peut-être que l'équipe des architectes, par exemple, présente les points clefs des trois mois à venir. Et l'après-midi il y a une phase où chaque équipe avance sur son planning des trois mois à venir (idéalement format [user story mapping](/2017/01/cartographie-plan-action/)). La journée s'achève avec une phase de réconciliation des différents plannings, des adhérences ou pas. 

D'expérience à six ou sept équipes on sature la capacité de synchronisation. Mais j'ai vu des *quarterly planning* dont la limite était 150 personnes. C'est une limite intelligente. On doit donc bien penser son organisation en départements organisés autour des produits avec ces limites la. 

Comme toujours pour une approche itérative agile il faut chercher un maximum à éviter les adhérences, à découpler.    

Sur la synchronisation : [Agile à l'échelle : la synchronisation](/2017/11/agilite-a-grande-echelle-synchronisation/)

### La vision d'ensemble 

Qui dit à l'échelle dit grande taille. Il faut avoir une perspective globale avec des niveaux d'agrégation adéquats. Agile à l'échelle nécessite d'avoir une bonne vision sur ses portfolios projets/produits. Pour cela c'est souvent un Kanban la différence ([Portfolio Kanban](/2016/01/portfolio-projets-kanban-partie-1/)). 

Il faut un moyen pour voir les grands ensembles, les besoins en synchronicité, les dépendances, la stratégie globale. 

On découpe souvent un produit en vision, *epic* (grand thème), *feature* (fonctionnalité), *user stories* (histoires utilisateurs), *tasks* (tâches). Vous pouvez avoir des portfolios kanban au niveau des produits ou des projets, des *epic* ou des *features*. L'important est de percevoir la dynamique, la stratégie, les motifs macro.

### Réorganisation des managers et donc de l'organigramme

Une organisation qui essaye vraiment de devenir agile doit souvent repenser sa structure, son organigramme. Bien souvent on en arrive, et c'est très bien, à devoir distinguer le manager du produit, du manager de la compétence. Par exemple Gérard - chef d'équipe dessin - responsable de Natacha - conceptrice dessin - avant la réorganisation. Il lui dit quoi faire et pourquoi et comment. Après la réorganisation il devient son manager de la compétence : comment faire grandir la compétence de Natacha, il accompagne le comment. Il laisse désormais Émilie se charger de lui quoi faire et pourquoi. Cela n'est pas simple pour beaucoup de managers qui ont l'habitude de dire quoi et comment (et qui oublient parfois, souvent, le pourquoi).  

## Questions organisations et transformations

À quel moment et à quel rythme l'organisation ou l'agile à l'échelle se met complètement en œuvre ? C'est à dire véritablement à l'échelle, ou que l'entreprise devient véritablement agile dans son entièreté ? J'ai d'abord pensé qu'il fallait une sorte de *big bang*. Puis j'ai changé d'avis, j'ai soutenu une transformation progressive de toute l'entreprise, et depuis deux années je suis revenu encore en arrière, il faut bien un *big bang* à mes yeux. Non pas un *big bang* pour les 8000 personnes de l'organisation, mais plutôt un *big bang* qui vous fait passer de 200 à 2000 (2000, 3000 c'est probablement une limite). Généralement tout commence par un pilote. Mais les pilotes contrairement à leur nom ne sont pas représentatifs de ce qui va se passer. Ils donnent juste un aperçu. Les pilotes sont réalisés avec les gens intéressés, impliqués, dans des cadres protégés. Et tant qu'ils sont des pilotes, l'organisation laisse faire. C'est quand on passe au niveau au-dessus, à celui du département, de vraies grappes de plusieurs équipes, que les résistances de l'organisation se dévoilent vraiment. Si cela fonctionne à ce niveau, disons d'un département, c'est le signal que vous pouvez basculer à l'échelle. 

Alors pourquoi un *big bang* à ce moment, qui vous ferait passer de 200 à 2000, par exemple ? Parce qu'il y a deux chaos. Le chaos négatif qui vous vampirise votre énergie. C'est celui où l'organisation n'a pas choisi. Les deux approches cohabitent : agile et non agile. C'est source de douleurs, de quiproquo, cela induit une difficulté de lecture et de compréhension. C'est un chaos négatif : vous subissez les choses, vous êtes en réaction, en affrontement. Le chaos positif c'est celui juste après la bascule, juste après le signal du *big bang*. C'est le chaos, mais il est positif, vous savez quelle est la cible, et tous vos efforts sont liés à une réadaptation, à une reconstruction. C'est un chaos qui créé de la valeur. 

Et si vous pensez faire sans chaos, c'est que vous ne changez rien. Le chaos est nécessaire. Vous changez de métier, vous changez de vie, vous changez de lieu de vie, vous changez d'habitude, il y a toujours du chaos.  

## Le risque avec Agile à l'échelle ? 

Le risque c'est que cela se mette à fonctionner jusqu'à un certain périmètre, une certaine étendue. Puis pour une raison vous reveniez en arrière. Un changement de direction, la résistance de l'organisation qui revient à son état précédent, etc. Dans ce cas vous risquez de perdre vos talents. Les gens qui ont goûté à l'autonomie, à la responsabilisation, à l'implication, c'est comme avoir goûté au fruit défendu, c'est difficile de revenir en arrière. Ils seront très frustrés. Et qui aura des facilités pour quitter rapidement le navire si ce n'est vos talents ?     

## Rappel des liens

* [La présentation de Henrik Kniberg chez Octo (merci pour l'invitation à l'époque)](https://blog.octo.com/agile-transformation-with-henrik-kniberg-octo-technology/). 
* [Podcast agile à l'échelle chez Café Craft](https://www.cafe-craft.fr/20)
* [Agile à l'échelle : la synchronisation](/2017/11/agilite-a-grande-echelle-synchronisation/)
* [Pourquoi il faut se méfier de SAFe](/2017/11/pourquoi-faut-il-se-mefier-de-safe/)
* [Portfolio Kanban](/2016/01/portfolio-projets-kanban-partie-1/)
* [User story mapping](/2017/01/cartographie-plan-action/)
* [Les deux chaos](/2018/05/les-deux-chaos/)
* [Séminaire Value Driven Scaling](http://valuedrivenscaling.com/)
 
![Dojo agile échelle](/images/2019/03/dojo-agile-echelle.jpg)
