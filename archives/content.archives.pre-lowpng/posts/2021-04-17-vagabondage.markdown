---
date: 2021-04-17
slug: anti-coaching-anti-psychanalyse 
tags: [coaching, vagabondage]
title: "Anti-coaching, anti-psychanalyse"
--- 

Ceci est un petit vagabondage d'avril. Une pensée mi-réflexion, mi-rêve éveillé. Je me projetais sur une nouvelle histoire d'accompagnement de dirigeants en entreprise, une approche en tête à tête, comme du coaching, comme de la psychanalyse. J'imaginais passer trois mois simplement en observation, sans rien dire, sans rien proposer. Comme par exemple lors de la prise d'un poste. Mais lors d'une approche d'accompagnement. 

Je me disais à moi-même : difficile pour le client, trois mois sans rien dire, cela a un coût de se faire accompagner par des coachs / managers / dirigeants "seniors" (=vieux =avec de l'expérience). Oui cela peut avoir beaucoup d'impact, mais trois mois juste à observer c'est un vrai coût. 

Je m'imaginais alors, pour apporter immédiatement de la valeur, lui proposer un journal de bord comme j'ai pu le faire déjà dans d'autres accompagnements (mais jamais encore véritablement dans une approche tête à tête avec un haut dirigeant). Il s'agit de raconter ce que l'on observe et de le partager.

Dans ce cas cela serait mettre par écrit toutes mes observations et mes pensées, sans filtre, à lui de décider de lire ou pas, d'en retirer ou pas ce qu'il en souhaite. Pour ensuite au bout de trois mois commencer à en discuter et à questionner, définir, essayer. 

Dans ce vagabondage d'avril, cette dernière partie de mise en action est vite apparue comme saugrenue. Tout l'intérêt résidait dans ce silence très verbeux d'une flopée d'écrits, de recueils sans filtres d'observations et de pensées. (Peut-être la pandémie me joue-t-elle des tours d'ailleurs sur le sujet au travers de ce formalisme tranché). Aucune question, aucun conseil, aucune écoute, aucun échange. D'où ce titre bizarre d'anti-coaching, et d'anti-psychanalyse. Juste le flot de mes observations et de mes pensées. À lire ou pas, à intégrer, comprendre, s'agacer, s'interroger, ou pas. Libre à lui.

Je me placerai en position "méta" : je lui donnerai une perspective. Une perspective normalement aiguisée avec de l'expérience dans son domaine.  

On imagine les premières semaines les observations écrites d'un codir (comité de direction) ou je le trouve à chercher l'harmonie à tout prix, et où j'exprime ma perplexité sur les bienfaits et les malfaits de cette approche. Des passages de textes ou j'interroge les raisons de ma présence.

Une temporalité s'exprimerait : deux mois seraient passés, et sa posture d'empathie à chercher l'harmonie apparaîtrait dans mes écrits clairement comme une faille (par exemple). Il pourrait lire dans ces écrits ma lecture de son appropriation de ce journal. Bref tout ce qui me passerait dans la tête durant mes observations serait consigné au fil de l'eau : observations, pensées, références/pointeurs vers de la matière (lecture, séminaire, ateliers, philosophes, écrivains, auteurs, etc.).

Ce journal de bord serait intime : réservé à nous deux. Jamais il n'y aurait "tu devrais", mais il y aurait peut-être beaucoup de "à sa place je" dans ces écrits. Il aurait toute la liberté de prendre ou de laisser, il n'y aurait jamais de jugement sur ce qu'il aura fait de ce journal de bord. Et finalement ces trois premiers mois constitueraient peut-être la somme et l'aboutissement de cet accompagnement silencieux. 

