---
date: 2017-12-22T00:00:00Z
slug: agile-a-l-echelle-equipes-et-management
tags: ['agile','scaling','echelle','safe','nexus','less','auto','organisation','auto-organisation']
title: Agile à l'échelle, équipes et management
---

Par définition l'auto-organisation est auto-organisée. Ce n'est pas donc elle que nous devons organiser, mais le contexte dans lequelle elle pourra apparaître, émerger. L'agile à l'échelle, ou plus globalement les organisations à l'échelle sont le fruit de l'agrégation de l'ensemble de leurs départements (ou villages ?), qui sont l'agrégat de leurs équipes, et les équipes sont idéalement auto-organisées (et sont l'agrégat des individus).

L'organisation à l'échelle est idéalement la somme des groupes qui proposent de la valeur pour elle. J'organise à l'échelle la création de valeur. Pour que cette valeur ait de la valeur je dois pouvoir en bénéficier : c'est à dire elle doit être indépendante ; c'est à dire avoir une valeur intrinsèque, pas d'adhérences nécessaires à avoir avec d'autres parties, même si la somme des parties peut générer encore plus de valeur. Plus elles sont modulaires, indépendantes, plus elles travaillent sur un petit périmètre plus la création de cette valeur sera facilitée, et accélérée.   

Plus on saura minimiser l'investissement et maximiser la valeur mieux cela sera. C'est au niveau de l'équipe que cela peut se produire le plus. Pour plein de raisons c'est l'agrégat le plus dynamique : il sera la plus à même de s'auto-organiser (et donc d'avoir les meilleurs résultats), il sera le plus à même de délivrer de la valeur en maximisant celle-ci et en minimisant les investissements.  

L'organisation agile à l'échelle est ainsi la somme de ses équipes. Et les équipes donnent leurs meilleurs résultats quand elles sont auto-organisées. Et par définition l'auto-organisation est auto-organisée. C'est donc l'agrégation des équipes (la [synchronisation](/2017/11/agilite-a-grande-echelle-synchronisation/), la vision, etc.) qu'il faut organiser. Et c'est le cadre qui permette à chaque équipe de s'auto-organiser : pas la façon dont elle s'organise, mais sur quoi elle se repose pour cela.  

Tout cela c'est le rôle du management (qui peut être porté par les managers, ou par d'autres). 

Cet article a une teinte froide mais les observations terrains sont chaudes : chaque équipe vit différemment, tout est fluctuant, vivant. Pour chaque personne qui fait le management c'est un défi.  

Le premier défi c'est faire du management dans ce monde complexe. Organiser le cadre et pas le geste. Avoir beaucoup d'introspection pour penser soi-même à cette complexité. 

Organiser le cadre c'est d'abord savoir pourquoi : clarifier la cible, le sens. 

Organiser le cadre c'est ensuite le penser de façon à favoriser l'auto-organisation. Penser son organisation pour faciliter l'émergence de l'auto-organisation [comme les favelas brésiliennes évoquées dans l'article précédent](/2017/12/agile-a-l-echelle-liberation-auto-organisation/). 

Kauffman et Stewart (cités par Harrison Owen dans "the practice of peace") ont observé le cadre qui a nourrit la naissance de la vie et de l'auto-organisation sur terre. Rien que çà. On résume cela en cinq points :

* Un environnement sécurisé. 
* Beaucoup de diversité dans l'environnement avec une capacité à avoir des liaisons complexes[^dangereuse].
* Un désir d'amélioration, d'évolution. 
* Des connections (entraves) pré-existantes peu nombreuses.
* Un grain de chaos. 

Et si on regarde cette liste avec depuis nos organisations. On comprend que pour bouger, changer, se transformer, les gens doivent être sécurisé. Sinon ils n'essaient pas vraiment. Ils ne prennent pas la bonne décision, la décision nouvelle, la décision adéquate, mais celle qui les sécurisent. En une phrase : **sans sécurité, ils ne répondent pas à la question qu'on leur pose, mais à la question qu'ils se posent : comment éviter les problèmes, comment survivre.**. Il est donc indispensable de sécuriser l'auto-organisation. 

L'auto-organisation fait émerger quelque chose de nouveau. La richesse de ce quelque chose dépendra de la variété des débats, des conversations, des croisements. Il est donc important de rechercher ces mélanges. 

**Les silos nuisent à l'auto-organisation.** 

Cela peut paraître aussi surprenant que l'idée que l'auto-organisation se met en place toute seule, mais cela n'arrivera que si elle est désirée. C'est peut-être évident à vos yeux. Mais si vous retournez la pièce de l'autre côté : il est inutile de rechercher l'auto-organisation, et donc une mise à l'échelle efficace, sans désir, sans envie. 

**Là question : "de quoi est-ce que vous avez envie (liée à notre écosystème, liée à notre histoire)" n'est donc pas frivole !**

Il faut avoir peu de contraintes pré-existantes. C'est rarement le cas dans les organisations un peu bouffies. C'est même plutôt l'inverse. Il faut donc savoir dé-construire, mais plutôt que de casser, **se sentir autoriser à transgresser l'existant**. Là aussi il faut **être sécurisé pour se sentir autorisé**.

Enfin, pour arriver sur de nouveaux territoires, du chaos est nécessaire. Chaos, conflit, et confusion rappelle Harrisson Owen (toujours "the practice of peace"), sont les signes flagrants que quelque chose est vivant, bouge, avance. Le **chaos est le début nécessaire à l'émergence d'un nouvel état**.    

**Raison de plus pour sécuriser encore l'écosystème**. 

On n'aura pas envie de quelque chose qui nous met en danger, dans un réel danger. Raison de plus pour s'appuyer sur les envies (liées à cet écosystème, liées à cette histoire). Raison de plus pour **lire les envies exprimées comme une mesure de l'autorisation permise et la sécurisation des personnes**.  

Sécuriser. 

Autoriser. 

Casser les silos. 

Suivre les désirs, les envies, liés à l'écosystème, à l'histoire de cette organisation.

Et ce qui émerge de l'auto-organisation est ce qui est désiré. Ainsi l'auto-organisation est une image du futur. "Cette auto-organisation dévoile", selon Henri Atlan[^Atlan], "notre volonté inconsciente (et ainsi le futur, vers quoi nous tendons), puisque nous essayons de créer de l’organisé en partant du chaos selon nos désirs".  

**C'est bien le rôle des managers** ou de ceux qui font le management dont nous parlons là. Pas des équipes qui sauront s'auto-organiser naturellement si on leur propose un espace pour prospérer. 

L'acculturation des managers à savoir créer cet espace, tenir cet espace est donc fondamental. 

**La mise à l'échelle est donc d'abord celle de la culture des managers**. L'auto-organisation suivra assez naturellement.  


## Une petite série sur mise à l’échelle (et auto-organisation).

* [Agile à l'échelle : c'est clair comme du cristal, 2013](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)
* [Agile à l'échelle : synchronisation, 2017](/2017/11/agilite-a-grande-echelle-synchronisation/)
* [Agile à l'échelle : pourquoi il faut se méfier de SAFe, 2017](/2017/11/pourquoi-faut-il-se-mefier-de-safe/)
* [Agile à l'échelle : libération de l'auto-organisation, 2017](/2017/12/agile-a-l-echelle-liberation-auto-organisation/) 
* [Agile à l'échelle : équipes et management, 2017](/2017/12/agile-a-l-echelle-equipes-et-management/)
* [Agile à l'échelle : grandir ou massifier](/2018/02/grandir-ou-massifier/)
* [Agile à l'échelle : Podcast "Café Craft" - "L'agilité à l'échelle"](http://www.cafe-craft.fr/20)

Et sinon l'événement que nous organisons en autour de l'agile à l'échelle avec [Dragos Dreptate](http://andwhatif.fr): 

* [VALUE DRIVEN SCALING](http://valuedrivenscaling.com)

[^Atlan]: vieille lecture (2013, il y a 4 ans tout juste), "[entre le cristal et la fumée (de Atlan)](/2013/12/auto-organisation-et-storytelling/)"
[^dangereuse]: et non pas dangereuses.

