---
date: 2013-10-09T00:00:00Z
slug: deux-amis
tags: ['oaa','openagileadoption','openspaceagility','openspace','agility','adoption']
title: Deux amis
---

A l'initiative de notre petite association : convergenc.es, [Oana](http://www.coemerge.com)
et moi-même avons profité de la venue de **Dan Mezick** au *Scrum
Gathering Paris* pour lui proposer deux interventions supplémentaires à
Paris et Montpellier sous la forme d'ateliers de demi-journées. Ceci n'a
été possible que grâce à l'aide et la gentillesse de Dan Mezick, qui a
bien voulu s'adapter à toutes nos contraintes. Sa présence était
obligatoire pour la bonne marche des ateliers comme il le dirait
lui-même, la notre restait optionnelle. :)

![Dan & Roberta](/images/2013/10/dan-roberta.jpg)

## Les workshops

![Dan Mezick Workshop](/images/2013/10/workshop-danmezick.jpg)

J'ai été ravi par les *workshops*, leur contenu m'a paru de qualité, les
façons de Dan m'ont comblés (autant d'humour que d'inspiration). J'ai
particulièrement aimé avoir des cercles assez réduits (8 à 10 personnes)
qui ont permis une vraie communication, un vrai sentiment d'appartenance
aussi. J'ai aussi été comblé par les petits apéritifs réalisés dans la
foulée, et les repas convivaux. J'ai beaucoup de mal actuellement avec
le formalisme triste de la plupart des évènements.

Quelques règles pour mes évènements à venir (ceux que j'essayerait de
monter) :

-   Limitation du nb de personnes : autour de 10 ou 50.
-   Des lieux inhabituels (la salle de la mutinerie quoique trop
    bruyante était très bien pour cela) qui ajoute de l'épaisseur à la
    rencontre.
-   On rompt le pain ensemble à un moment ou à un autre (les repas ont
    été l'occasion de poursuivre des discussions, d'aborder des sujets
    différemments). *A minima* un apéritif, un moment différent pour
    parler.

J'envisage un évènement "mobile" : rendez vous quelque part puis on se
rend dans un hotel pour passer une soirée ensemble, le lendemain on se
déplace (ballade ? randonnée ?), le soir on est ailleurs, on continue
les débats. Etc.

## Les principales idées

Selon mon point de vue...

L'agile se conçoit comme une culture et un ensemble de pratiques. Les
pratiques sont en fait l'émanation d'outils, de méthodes, Scrum, Kanban,
etc. Dan propose un ensemble intermédiaire entre la culture et les
pratiques, agnostique de la méthode employée : management visuel, être
ponctuel, déclarer ses intentions, etc. C'est bienvenu. J'essayais de le
faire depuis quelques temps : par exemple
[là](http://www.areyouagile.com/pdf/initiation-agilite-0-6.pdf) ou dans
[là aussi](https://speakerdeck.com/pablopernot/being-agile) sans
matérialiser cela aussi clairement. Il faut donc lire son [Culture
Game](http://newtechusa.net/services/culture-game-training/).

**Claude Emond** m'avait suggéré lors de notre rencontre : "tu
n'emmeneras jamais une entreprise où elle ne veut pas aller", cela
m'avait interpellé, c'était une idée qui fourmillait mais que je
n'arrivais pas à formaliser, et donc à manipuler. Dan est venu ajouter
une nouvelle pierre à cet édifice : forcer, obliger les gens est futile,
tout devrait être basé sur l'invitation. Il n'y a AUCUN bénéfice à agir
différemment. Mais nous ne sommes pas des bisounours : si les gens ne
veulent jamais faire quoi que cela soit il faut acter d'un changement
nécessaire : pensez aux [cores protocols et core
commitments](http://www.mccarthyshow.com/download-the-core/) de **Jim
McCarthy**.

Dan rappelle des éléments clefs (là aussi son esprit de synthèse me
bluff) qui permettent l'engagement. Là où j'arrêtais mon discours à : il
faut responsabiliser les gens pour palier au complexe et potentiellement
les motiver. Il ajoute un conteneur pour aider à cette
responsabilisation, à cet engagement :

Cette
[page](http://newtechusa.net/agile/how-games-deliver-happiness-learning/)
sur le blog de Dan me parait essentielle. Je traduis les passages clefs
:

### Cadre du jeu

-   un objectif clair
-   des règles claires
-   une capacité à recevoir du feedback
-   pas d'obligation (être invité) et pouvoir se désengager

### Pour se sentir bien

-   besoin d'un sentiment de contrôle
-   besoin d'un sentiment de progrès.
-   besoin d'un sentiment d'appartenance à un groupe.
-   besoin d'avoir un objectif clairement défini (...voir plus haut)

### L'OpenSpace comme moteur de la transformation

Grande idée que cela, j'avais bien observé leurs bienfaits dans les
différents *openspaces* que j'ai pu réaliser en mission. Dan de nouveau
consolide cela, et met le doigt sur des aspects très importants (qui
furent clefs dans mes *openspaces* réussis, et clefs aussi dans mes
*openspaces* ratés) :

-   Implication du ou des "chefs", notamment lors de l'ouverture
-   Consolidation immédiate ou très rapide des résultats de l'openspace
    (pdf distribués par exemple)

Lors de la présentation Dan ne s'embarrasse pas d'un support
monolithique il papillonne de support en support (twitter, wikipedia,
des slides, certaines pages d'un livre, etc.). C'est épatant et j'adopte
sur le champs cette façon de faire (que là aussi j'exploitais déjà, mais
pas encore assez).

### Consolidation des sessions

Le [document : Dan Mezick 2013](/pdf/danmezick-convergences2013.pdf) qui
consolide nos deux sessions avec notamment les notes *à la volée* de
**Marie-Laure Vie**.

## Deux amis

![Dan & Roberta à la maison](/images/2013/10/dan-roberta-2.jpg)

Mais surtout, surtout, la chose la plus importante qui s'est révélée
durant ces 5 jours (rencontre de Dan & Roberta jeudi, départ lundi
matin), c'est que j'ai reçu un speaker et sa femme (Roberta), et j'ai
laissé partir deux amis. Je vous assure que cette expérience m'a
réellement marquée. Et que Dan et Roberta on vraiment été magnifiques.
Aucune peur de tout ce que j'essayais de leur faire découvrir : huitres,
fromages ultra secs et noirs, saucissons exotiques, etc. Je crois que la
première chose désormais dans l'esprit de Dan concernant la France
c'est... la Chartreuse :) Enfin nous avons passé de nombreux repas
ensemble, au restaurant, en famille au coin du feu de bois, au marché le
matin, avec les participants. Nous avons visité Arles, Les Saintes
Maries de la Mer, Aigues Mortes, Sommières, etc. Roberta & Dan ont été
hébergés dans la maison de famille. Nous avons poursuivi des discussions
endiablées sur des sujets forts (parfois très éloignés de nos
conversations habituelles) tout au long de ces 5 jours. Ce fut
réellement une magnifique rencontre (et je pense que ce sentiment est
partagé).

Je me fous de refaire du boulot avec Dan (même si cela va se faire, et
j'en suis ravi, voire plus bas), je me fous de profiter de
l'enseignement de Dan comparé au plaisir que j'aurais à les croiser à
nouveau, juste parce qu'ils sont des amis et que j'ai adoré passer du
temps avec eux (heureusement d'ailleurs la période est dure : le boulot
reprend en mode ouragan, et j'ai été papa il y a un mois, sans ce
plaisir j'aurais craqué).

## Open Agile Adoption

Cela continue. Dans les calendriers : le 7 décembre une présentation de
l'*Open Agile Adoption* est évoquée pour *Agile Tour Tunisia*. Dan nous
invite (Oana & moi) à un évènement en mars entre New York & Boston. Qui
refuserait ?

pas moi.
