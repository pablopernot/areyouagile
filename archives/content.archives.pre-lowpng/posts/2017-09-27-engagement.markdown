---
date: 2017-09-27T00:00:00Z
slug: etre-engage-1
tags: ['agile','etre','temps','argent','responsabilisation','engagement','openadoption','openagileadoption', 'openspace', 'openspaceagility']
title: Être engagé ? (partie 1)
---

Un peu partout fleurissent les invocations : soyez heureux, soyez motivés, soyez impliqués, soyez responsables, être heureux, happiness chef blabla. Mais cela ne se décrète pas. Concernant l'engagement, concernant l'implication, ce n'est pas une injonction à laquelle on peut répondre. Vous ne pourrez pas décider de rendre telle ou telle personne
motivée. Mais vous pouvez créer un cadre, un contexte, qui permettent à ces personnes d'être motivées, impliquées. On voit régulièrement revenir ce message : une très petite part des employés dans le monde se sentent impliqués.

Dans le manifeste agile il y a un principe où il est écrit : faîtes des
projets avec des gens motivés. Généralement on s'esclaffe à ce moment.
Naturellement que les projets marchent avec des gens motivés. Quelle
idée ! Et bien donc oubliez tout le reste, focalisez vous sur l'idée
d'engager les personnes avec lesquelles vous travaillez.

Certains expliquent qu'entre une personne engagée et une personne non
engagée la différence est énorme. Dans le [Mythical Man Month](https://archive.org/details/mythicalmanmonth00fred) l’auteur
(Fred Brooks) exprime l’idée qu’une personne (en l’occurrence dans son
cadre, un développeur informatique) à un ratio de productivité de un à
dix selon le contexte. Sa capacité fluctuerait ainsi de un à dix selon
le contexte. Mes observations -sur moi même et les autres- tendent à
confirmer cette information. Et je pense que la grande part de cet écart
viens de l’implication de la personne. Observez vous vous même à
réaliser une tâche sur laquelle vous ne vous sentez pas impliqué, et une
sur laquelle vous vous sentez impliqué, que vous vous êtes approprié.
Personnellement je passe de dépité nonchalant peu inspiré à bulldozer
frénétique grandement productif.

On ne peut pas décréter la motivation, ni l’implication, ni
l’appropriation. Pourtant il y a un domaine, sujet d'études, où on
découvre des personnes impliquées, engagées. Avec un haut niveau
d'exigence. Quand quelque chose échoue, elles essayent d'autres façons,
ne se laissent pas abattre, elles aiment chercher des alternatives. Même
les plus introverties d'entres elles appellent des amis à l'aide ou vont
lire la documentation. Car -- faut-il l'évoquer ? -- la documentation en
ligne est pléthorique : guide du "nouveau/newbie", vidéos,
accompagnements, wikis, forums. Mais quel est donc ce domaine ? Les gens
que j'évoque y passe plusieurs par semaine, voire plusieurs dizaines
d'heures, et, cerise sur le gâteau, ils payent pour cela...

Il s'agit des jeux vidéos, et plutôt des joueurs de vidéos. Pourquoi
donc cette implication dans ce domaine ? C'est exactement ce que nous
recherchons dans nos organisations. Tous ces joueurs de jeux en ligne
qui payent pour échouer, recommencer, apprendre, refaire, fournir une
documentation de qualité, ne pourrait-on pas avoir le même engagement en
entreprise ? L’industrie du jeu vidéo est florissante, comprenez qu’elle
implique d’énormes sommes d’argent. Avant donc pour nous même d’en tirer
des enseignements pour l’organisation, elle s’est interrogée sur
elle-même car son succès est liée à l’implication qu’elle va engendrer.
Les observations qui ont découlé de ces recherches sont une mine pour
nous. À voir : [Jane McGonigal](https://www.ted.com/talks/jane_mcgonigal_gaming_can_make_a_better_world?language=fr)
et à lire : *Reality Is Broken: Why Games Make Us Better and How They
Can Change the World* de la même personne.

Mais alors que j’évoque ce sujet, je reçois le bulletin scolaire de mon
fils aîné, Mathieu. Mathieu a de bons résultats scolaires mais le carnet
indique que si Mathieu était impliqué il aurait de très bons résultats
scolaires. Je cherche Mathieu :

-   Mathieu ? Mathieu ?
-   (de l’autre pièce) Oui papa ?
-   Peux-tu venir que l’on parle de ton bulletin scolaire.
-   Attend, là je ne peux pas, je suis en train de construire la
    quatrième tour de mon château fort à
    [Minecraft](https://minecraft.net/en-us/) et elle n’a pas exactement
    les mêmes décorations que les autres, ni la même constitution en
    fer, je ne peux pas la laisser ainsi.
-   Oui c’est exactement de cela que je souhaite te parler...

En d’autres termes Mathieu est très impliqué dans son jeu, et ses
résultats sont très bons. Qu’est ce qui manque à l’école vous récolter
ce même engagement ? Qu’est-ce qui manque aux organisations pour avoir
ce même type d’implication ?

Ce n'est déjà pas la compétition ou l'envie de gagner qui va faire cette
différence. Je ne crois pas que l'on gagne réellement à Tétris ou Candy
Crush Saga, ni réellement à aucun des jeux en ligne de mon fils, il perd
autant qu'il gagne, ce n'est pas là sa réelle recherche. Ce dont nous
avons besoin et ce que ces études sur les jeux vidéos nous enseignent
est désespérément simple à énoncer, mais diablement difficile à mettre
en œuvre.

## Un objectif clair

D'abord un jeu qui engage a un objectif clair. Amusez vous à tester si
votre projet, votre produit, votre organisation a un objectif clair.
Facilement énonçable. Repensez à cette célèbre citation de Einstein :
"si tu ne sais pas l'expliquer à un enfant de huit ans c'est que tu ne
sais pas ce que tu veux". Votre travail de dirigeants, de managers,
c'est cette clarification.

## Des règles claires

Je ne peux pas lancer de sortilège si je n'ai plus de points de mana, je
ne peux pas cumuler le pouvoir de ces deux armes, mais tiens je pourrais
utiliser les maisons pour protéger ma tour et pas l'inverse. Pour
prendre cet espace qui fera toute la différence, il convient de
clarifier la cible, l'objectif, mais aussi de délimiter le cadre dans
lequel on peut s'épanouir, surprendre, imaginer, s'approprier. Vous
devez clarifier les règles du jeu. Naturellement si vos règles sont trop
contraignantes l'espace que vous proposerez sera faible.

## Feedback, un regard en retour

Si je tue ce monstre j'obtiens 100 points, si je prends cette ville je
gagne 10000 écus, ah mince il n'y avait rien dans ce donjon. Dans le jeu
on sait constamment les bénéfices (substantiels ou catastrophiques) que
nos actions entraînent. Dans l'organisation cela devrait être pareil.
J'ai livré cette fonctionnalité mais elle n'a reçue aucun accueil
favorable, alors que l'autre oui, mais pas de la façon attendue. Oui
cette action a été reçue positivement, celle là non, celle-ci a
déclenché cela, l'autre cela. Peu importe le résultat de votre activité,
vous devriez le mesurer et le connaître, le faire connaître. Sans cela
votre implication se réduira. A quoi bon travailler si on ne sait pas ce
que devient le fruit de notre labeur. Le premier pas vers le sens, c'est
ce feedback.

## Invitation

Plus difficile. Aucun des joueurs de jeux vidéos n'est obligé de jouer.
Idéalement aucun des membres de l'organisation ne devrait être obligé de
venir s'impliquer, travailler. Pas facile. On se heurte à une autre
réalité, celle de la pyramide de Maslow, il faut bien d'abord avoir un
toit, manger, bref... avoir une organisation et un salaire avant de
penser engagement. C'est un vaste débat. Mais difficile aujourd'hui
d'inviter à travailler, en tous cas pas simple selon les contextes.
Alors comment utiliser la notion d'invitation ?

Prenons quelques exemples. Jim McCarthy, celui là même des [core protocols](http://www.mccarthyshow.com/online/) dirigeait un temps Microsoft Studio. Face à toutes ses équipes il a essayé l'invitation :
voici 15 projets*[1]*, "proposez les équipes que vous voulez pour que les projets soient des succès". Émoi chez les managers. Les gens vont faire n'importe quoi. Mais non. Nous sommes des systèmes vivants s'entête à rappeler Harrison Owen (l'inventeur de l'openspace), nous sommes naturellement responsables. C'est en déresponsabilisant les gens que vous obtenez parfois des comportements complètement ubuesques. Dans ce cadre et suivant l'objectif clairement édicté : "que les projets soient des succès" les gens ont mixé intelligemment socialisation (les dynamiques de groupes, de copains) et compétences. Mais surtout effet
crucial de l'invitation -- de la transparence sur la réalité des choses : personne ne va se porter volontaire sur certains projets. Si il y a deux projets "qui puent", ils vont se retrouver sans personne. C'est une très bonne nouvelle. On découvre qu'il y a des sujets compliqués de façon flagrante, peut-être avec surprise. Non pas qu'il ne faille pas faire ces projets, mais il devient clair qu'il faut leur donner d'autres moyens.

Autre exemple, inspiré par [Dan Mezick](https://osa.areyouagile.com),
utilisez l'invitation dans vos réunions. Imaginez le grand chef qui
propose une réunion **réellement** sur invitation. Ce grand chef, on va
l'appeler Mesmaeker (Gaston Lagaffe...), invite bien douze personnes.
*Première option*: personne ne se présente. Et c'est une bonne nouvelle
car c'était vraiment une réunion inutile. Tout le monde a gagné du
temps. *Deuxième option* : personne ne se présente or c'était une
réunion utile. L'apprentissage est clair pour Mr Mesmaeker, il doit
retravailler sa communication pour faire comprendre l'importance,
l'enjeu de cette réunion. Là, de façon manifeste, sa communication est
ineffective. *Troisième option*: personne ne se présente or c'était une
réunion utile, et l'importance en a été comprise. Mais peut-être la
priorité n'était pas claire. Des choses importantes on en a tous à
faire. Mr Mesmaeker aurait du préciser : et "c'est une priorité", ou
"c'est plus important que...". Encore une question, problème de
communication rendu visible par l'invitation. *Quatrième option* :
quatre personnes se présentent sur douze. C'est les quatre "bonnes
personnes" car il s'agit de celles qui ont compris l'importance, qui
sont prêtes à s'investir, qui ont priorisé l'importance de façon à
mettre ce sujet en avant (celles qui ont le temps). *Cinquième option* :
celle que vous connaissez tous, ce n'est pas une réelle invitation, les
douze personnes sont là. Elles disent oui "comme il se doit". Mais on ne
sait pas lesquelles ont le temps, ont compris l'importance, ont intégré
la bonne priorité, on ne fait nécessaire d'effort sur le format de la
réunion puisque tout le monde y est contraint. Et tout le monde dit oui
mais on ne sait pas vraiment ce que cela veut dire.

Vous avez compris l'invitation est clef pour l'engagement (et donc la
conduite du changement). Mais pour nos organisations c'est surtout aussi
un formidable outil de transparence.

L'invitation est aussi un des piliers de l'approche **open agile
adoption** ou **openspace agility**. L'idée est d'inviter chacun à un
forum ouvert de façon cyclique, forum ouvert qui possède un objectif et
un cadre (tiens tiens), d'inviter chacun à donner le comment de la mise
en œuvre. On obtient ainsi quelque chose de très engageant et de très
respectueux des personnes et des organisations. Je vous renvoie à
**openspace agility**/**open agile adoption** :
[ici](/tag/openagileadoption/)

Ainsi à nouveau, vous n’avez pas de problème de temps et d’argent. Vous
avez une question d'implication et d'engagement. Et sous ces préceptes
assez simples : **objectif clair, règles claires, feedback et
invitation**, se cache un spectaculaire levier. Mais pour les
dirigeants, les managers, les leaders, c'est un travail constant de
clarifier, de supporter, de faire évoluer, d'accompagner ces objectifs
clairs, ces règles claires, ce feedback et cette invitation.

Voilà pour la première partie sur l'engagement, la seconde bientôt.

Dans la série :

-   [être agile](/2017/07/etre-agile/)
-   [être engagé - partie 1](/2017/09/etre-engage-1/)
-   [être engagé - partie 2](/2017/10/etre-engage-2/)

*[1] Je ne me rappelle plus bien dans les détails de cette conversation
à ALE2012 à Barcelona. Les chiffres sont à la bonne franquette mais cela
ne change pas le propos.*
