---
date: 2014-07-15T00:00:00Z
slug: raid-agile-en-cevennes
tags: ['raidagile']
title: Raid agile en Cévennes
---

Le monde des formations est trop conventionnel (sentiment accentué par
l'arrivée des formations en ligne, vidéos, dont on espère qu'elles
peuvent changer aussi les choses). J'essaye depuis plusieurs années de
transformer les salles de mes formations en salon plus accueillant :
tapis persans, canapés, etc. Les formats ne sont pas non plus supers
excitants : généralement sept heures d'une journée dans un batiment
austère de la Défense ou d'ailleurs et le repas dans le brouhaha d'une
cafétéria de plusieurs centaines de quidams.

Quand on voit l'importance de l'**épaisseur** de l'environnement dans la
mémorisation des choses. Quand on observe les moments clefs qui se
déclenchent hors des sentiers battus, on se dit que l'on devrait essayer
plus souvent de réaliser nos apprentissages dans des lieux marquants.

C'est que nous essayons de faire avec [Claudio](http://www.aubryconseil.com/) en proposant un exercice
d'apprentissage et de *team building* avec [Raid Agile en
Cévennes](http://raidagile.fr).

## Petit historique

Stéphane ([@langlois\_s](https://twitter.com/langlois_s)) convie Claude
aka Claudio ([@claudeaubry](https://twitter.com/claudeaubry)) à monter
un évènement dans un lieu riche (il pense à une belle île du coin si je
ne m'abuse). C'est une idée qui nous trotte tous un peu dans la tête
depuis les *paniers repas agiles* ou les *agile open sud*. Nous nous
connaissons bien tous les trois, et Stéphane commet l'erreur de me
proposer de participer. Je lui avoue mon amour des Cévennes, paysage
destructuré/structurant, et mon expérience des regroupements annuels de
musique **old time** dans ces belles montagnes (c'est de là d'ailleurs
que venais les *paniers repas agiles* ou *agile open sud* aussi, déjà).
Nous nous lançons tous les trois, merci Stéphane. Deux voies s'ouvrent,
la première est portée par Stéphane (et je lui laisse l'exprimer si il
le souhaite). La seconde est celle que nous proposons désormais avec
Claudio (transformation de Claude en Claudio) en *binome* : [Raid Agile
en Cévennes](http://raidagile.fr) (oui nous apprécions l'oxymore, on
fournira aussi des charcuteries Lean).

## Notre pitch

Le raid agile, c'est une vingtaine de personnes qui se retrouvent dans
un gite des Cévennes. Ils partagent leurs expériences, découvrent de
nouveaux outils et élaborent en équipes leur feuille de route pour 6 à
12 mois de développements agiles. Ils sont accompagnés par deux
praticiens chevronnés ([Claudio](http://www.aubryconseil.com/) & Pablo).

![Cévennes](/images/2014/07/cevennes2.jpg)

## Notre *unfair advantage*

-   Un paysage inspirant : on a pas le Grand Canyon mais on a les
    Cévennes. (nous venons vous chercher à la gare et nous vous ramenons
    à la gare de Nîmes ou Montpellier).
-   Les ateliers, les outils et le feedback sur lesquels s'appuient tous
    les jours nos facilitateurs.
-   Balades & repas dans un environnement adéquat. Pour se retrouver et
    se ressourcer, se découvrir et s'aligner au sein de l'équipe. Avoir
    des conversations hors du temps.
-   Réussir à clarifier, aligner, articuler une vision et un plan
    d'actions, une feuille de route pour les 6/12 mois à venir.

## Le premier raid est prévu en octobre !

**C'est le moment de se déclarer intéressé !**, car nous essayons de
consolider une première équipe. Pour cela [contactez
nous](https://docs.google.com/forms/d/1q4uaqjCAAPC7TRaE6yOh6Y6Y0tMoch8lfCG9mmKtR0A/viewform).
Le raid est éligible au droit à la formation (dif).

Vous comprenez bien que bâtir son backlog en équipe dans un gite des
Cévennes lors de soirées où le fumet des ceps habille l'air après une
conversation *impact mapping* lors d'une randonnée en forêt, c'est vivre
une expérience différente.
