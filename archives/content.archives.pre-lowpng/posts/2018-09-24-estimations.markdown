﻿---
date: 2018-09-24
slug: les-estimations-sont-fausses-surtout-si-on-considere-qu-elles-sont-justes
tags: [estimation, estimates]
title: "Les estimations sont fausses, surtout si on considère qu'elles sont justes"
--- 

Oui les estimations sont fausses, surtout si on considère qu'elles sont justes. 

On pourrait dire que les estimations sont un bon repère de maturité d'une équipe, d'un département, dans une approche moderne. 

Au début on estime tout, pour de gros projets. On estime tout et précisément. Et comme vingt années d'observation le confirment, nos estimations sont fausses et bien souvent inutiles. Enfin ce qui est inutile c'est la précision du chiffre et le temps passé à le calculer. La conversation et la réflexion qu'il y a eu autour ont pu nous faire apprendre beaucoup de choses. Mais souvent on apprend plus en essayant qu'en imaginant. Avec le temps donc, l'expérience, ce que j'appelais maturité, on se rend compte que les estimations sont fausses, mais que les conversations et les réflexions engagées ont pu se révéler intéressantes, mais que la réalité n'a jamais été comme nous l'avions imaginé et donc que l'intérêt de ces conversations demeure limité. On se rend compte que l'on perd beaucoup de temps. À estimer, surtout avec précision. À discuter de choses qui se révéleront différentes ("*aucun plan ne survit au premier contact avec l'ennemi* -- Von Moltke"), surtout si ces choses se déroulent dans un futur non proche. 

Autre problème en délivrant des estimations on répond à un besoin de se projeter, compréhensible, mais aussi souvent à un besoin de contractualisation. Celui-ci beaucoup plus incompréhensible dès que l'on découvre que nos estimations ne sont pas justes. Mais l'expérience venant (très rapidement sur ce sujet), on devine aussi que peut importe la justesse de l'estimation, c'est plus un levier de pression et de déresponsabilisation qui surgit ici. Qui sera coupable ? Qui s'engage sur quoi sur un périmètre que l'on ne maîtrise pas ? Ou surtout l'inverse : ce n'est pas ma faute ils avaient dit que...    

C'est toujours un problème quand la réponse ne répond en fait pas à la question. Vous me suivez ? C'est un problème de demander des estimations -- qui seront fausses -- pour savoir "à peu près" le temps et les moyens engagés, quand souvent une partie de la question est : qui sera responsable ? C'est donc toujours un vrai problème d'avoir un service achat. Il ne pose pas la même question. Et la même réponse sert à deux questions très différentes...

Bref, dans un premier temps il sera difficile de faire lâcher ces personnes sur les estimations (qui vont les protéger, qui vont leur permettre de mettre la pression, etc.), on va donc probablement réduire le gros projet en petits projets que l'on va estimer par sous-parties. On garde les estimations. On en arrive à ces itérations agiles. De petits espaces dans lesquels on estime des *user stories* ou autre. Ça permet déjà de ne plus estimer des choses trop inconnues (qui se produiront plus tard), ou trop inutiles (dont nous découvrirons qu'elles ne se feront jamais). C'est déjà un progrès. 

On se met à délivrer (ou pas) des choses régulièrement. Et si justement ce n'est pas régulièrement, cela va gratter... Dans cette dynamique les estimations vont nous servir à découper les choses en petits éléments pour nous permettre de délivrer des choses, éviter l'effet tunnel. Puisque l'on est entré dans une approche incrémentale/itérative, les estimations nous aident à percevoir si c'est gros, moyen, ou petit. Et on tend à viser du "petit", voire du "moyen", pour s'assurer de pouvoir finir les choses. Si on garde des estimations précises, on va se rendre compte que même dans les semaines à venir les surprises et les changements seront présents. **La précision se révèle donc une perte de temps**. Et la précision (ou plutôt le chiffre), poursuit des buts cachés : 

* Quelle équipe fait le plus de points ? (même si cette question est absurde, car isolée comme cela cette notion n'a ni queue ni tête).
* Notre client paie pour tant de jours/hommes, ou tant de points, est-ce que nous les lui fournissons ? (c'est toujours absurde, vous percevez bien comme on fait de quelque chose de très flou quelque chose de très précis, à tort, mais tellement aisé pour négocier).

Ici on peut avoir la chance de sortir des chiffres. Puisque l'intérêt c'est de découper en petits éléments, et d'avoir une conversation, une réflexion sur ce qui doit être fait. Et que l'on a appris que l'on perdait du temps à être trop précis (rappel pour l'étourdi : les estimations sont fausses surtout si on pense qu'elles sont justes). On réussit alors à découper en éléments sans chiffre ni nombre (*tee-shirts* : XXL, XL, L, M, S, *animaux* : Mammouth, Rhinocéros, Ours, Chien, Chat, Souris, *macro* : impensable, très gros, gros, moyen, petit, etc. Inventez le vôtre). Il y a toujours des gens qui se croient malins et qui font un temps des conversions. S’ils ont du pouvoir, vous verrez vite que vous n'êtes pas encore sorti du système précédent (les dialogues tourneront autour du temps, de l'estimation, et pas du contenu, de la valeur). Laissez le temps au temps ? Limitez encore plus la lecture : trop gros, gros, moyen, petit. On gagne du temps à ne pas aller dans des précisions inutiles, et nos estimations sont plus réalistes, car plus floues.   

Si cela fonctionne, vous commencez à délivrer des "moyennes" et des "petites" *user stories* ou autre. En ayant perdu beaucoup moins de temps avec des choses inutiles comme estimer précisément, on lève la tête et on a du temps pour se poser d'autres questions. Comme l'homme préhistorique a pu se lancer dans plein de nouvelles activités, car il s'est mis à cuir sa viande, et donc son temps de mastication et digestion ont été fortement réduits. L'art rupestre ? La philosophie ? On ne saura jamais vraiment. Ainsi en gagnant du temps les bonnes questions arrivent : pourquoi cette fonctionnalité ? Et pourquoi pas celle-ci ? Laquelle marche comme prévu ? etc. On passe doucement d'une approche projet, à une approche produit. Le but n'est pas de délivrer un ensemble de demandes, mais de travailler sur une dynamique entre votre produit et son utilisation. Autre effet de cette période : on se met à ne plus estimer par avance, on perçoit une cadence : tiens on délivre 4 "petites", et 2 "moyennes" par itération, en moyenne. À partir de cette cadence on se projette sur une capacité. On a une capacité de production relative à notre produit, et non plus une somme estimée de livrables pour un instant t. (D'où aussi la disparition des backlogs, qui eux aussi sont des puits à perte de temps dès qu'ils deviennent un peu chargés).  

La réflexion s'étant déplacée vers le produit, la valeur. L'estimation s'est réduite d'un côté à : Quelle est notre capacité de production ? Qu'est-ce qui serait le plus intéressant à délivrer ? Comment délivrer quelque chose d'exploitable au plus vite (donc, savoir découper en petits morceaux autonomes qui portent du sens) ? Et la véritable estimation a grandi : qu'est-ce qui a de la valeur pour nous, pour le produit ? Comment valider nos hypothèses de la meilleure façon ? À ce moment on priorise par valeur vis-à-vis du produit, et on découpe en petits morceaux nos premières priorités (et juste cela) pour avoir assez de souplesse, pour rendre possible une articulation qui nous permette de bénéficier aisément de cet investissement : valider nos hypothèses, et potentiellement gagner de la valeur. Avant de faire le pas suivant. On maximise l'apprentissage, on minimise le temps perdu. On garde une vision, une direction. On obtient les agendas en observant notre cadence, notre capacité. 

On pourrait résumer quatre phases : 

* Grand mensonge et perte de temps.
* Petits mensonges entre amis.
* On est plus efficace sans chiffre.
* De quoi a besoin mon produit ? 

Enfin quelques articles liés :

* [Beyond budgeting et #noestimates](/images/2017/05/Agile-CFO.pdf).
* [La malédiction du jour-homme](/2012/03/la-malediction-du-jour-homme/).
