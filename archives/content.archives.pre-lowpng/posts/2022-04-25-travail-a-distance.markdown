---
date: 2022-04-25
slug: travail-a-distance
tags: [management, rh, remote, distance]
title: "Travail à distance"
draft: false
---

Suite à la clôture des sites et entrepôts benext, je publie ici un document collectif réalisé en avril/mai 2020 (premier confinement), sur le travail à distance. 
Avec l'objectif de garder des traces.  

{{< figure src="/images/2022/04/travail-a-distance.png"   link="/pdf/livre-digital-travail-a-distance-benext.pdf" alt="image par Olga Bozalp" title="image par Olga Bozalp " class="left" width=300vw >}}

### PDF 

[LIVRE DIGITAL "travail à distance" de benext](/pdf/livre-digital-travail-a-distance-benext.pdf) 

### Introduction

Depuis le début de son existence BENEXT est une société qui cherche à créer une culture forte. Ses membres étant en grande partie
dispersés en Ile-de-France, nous avons toujours cherché à maintenir le lien au quotidien —via notre outil de conversation en ligne (Slack)— et
nous nous retrouvons très régulièrement lors de multiples événements en présentiel.

BENEXT n’héberge aucun service informatique, aucun outil “maison”.

Depuis longtemps nous sommes dématérialisés. La nouvelle situation nous pousse plus loin. Le télétravail devient la norme pour nous et pour
nos clients.

Ici nous ne parlons pas de l’état de l’art du télétravail, nous parlons de notre cheminement. Notre sujet est la nature du travail à distance et
surtout, en toile de fond, les transformations plus profondes à venir liées aux nouvelles relations que le travail à distance amène.
S’il n'y a pas d'interrogations sur ce que cette nouvelle forme de travail amène, nous n’apprendrons pas, nous n’avancerons pas. Evitons de
transposer le réel dans le virtuel trop simplement et de façon trop précipitée.


### Meetup

Il y eu aussi un meetup sur le sujet : 

{{< youtube id="MB9O0E26yxk" >}}





