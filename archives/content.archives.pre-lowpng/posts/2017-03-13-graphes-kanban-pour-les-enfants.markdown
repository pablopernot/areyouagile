---
date: 2017-03-13T00:00:00Z
slug: graphes-kanban-racontes-aux-enfants
tags: ['enfant','enfants','kanban','cfd','controlchart','cumulativeflowdiagram']
title: Les graphes Kanban racontés aux enfants
---

Aujourd'hui on m'a lancé le défi de vous expliquer les deux principaux
diagrammes Kanban comme si vous étiez des enfants. Qui ne s'est pas
perdu dans un diagramme de flux cumulé (*Cumulative Flow Diagram*) ou
une carte de contrôle (*Control Chart*) ? Enfin, moi je dis ça, j'ai
fait Lettres, Arts & Philosophie (et j'ai bossé sur les Monty Python),
alors ne comptez pas sur moi pour calculer un pourcentage ! Et pourtant
j'en ai besoin avec ces satanés diagrammes. Je me suis donc dit, essaye
de l'expliquer à un enfant, pour reprendre l'adage de Einstein "Si tu ne
sais pas l'expliquer à un enfant de 8 ans c'est que tu ne sais pas ce
que tu veux").

Il était une fois...

## Le terrible diagramme de flux cumulé

![C'est quoi cette bouteille de lait ?](/images/2017/03/bouteille-penchee.jpg)

![C'est quoi cette bouteille de lait ?](/images/2017/03/bouteille-penchee-2.jpg)

Écoute, tu as un robinet de demandes (dont on espère qu'elles ont de la
valeur), qui remplit un récipient, disons une bouteille. Mais une
bouteille penchée. Et donc elle se remplit (le robinet hein).

![C'est quoi cette bouteille de lait ?](/images/2017/03/bouteille-penchee-3.jpg)

![C'est quoi cette bouteille de lait ?](/images/2017/03/bouteille-penchee-4.jpg)

Et puis il y a une réaction chimique sur ces demandes contenues dans
cette bouteille. Peu à peu elles passent d'un état à un autre et du coup
elles changent de couleurs, elles se transforment. De bleues elles
deviennent vertes, mais la réaction chimique continue : de vertes elles
deviennent oranges, puis grises. Et le processus gagne la bouteille,
mais des demandes continuent de couler du robinet. C'est beau non ? Un
peu comme tes bouteilles de sables colorés.

![C'est quoi cette bouteille de lait ?](/images/2017/03/bouteille-penchee-5.jpg)

Ça pourrait presque être comme des roches sédimentaires. Des quoi ?
Laisse tomber tu verras cela au collège.

![C'est quoi cette bouteille de lait ?](/images/2017/03/bouteille-temps.jpg)

Finalement si on devait regarder cela sur une ligne de temps, euh, si
l'on devait regarder les différents états de la bouteille au fil du
temps on verrait cela (concentre toi un peu et regarde le dessin). On
pourrait compter les demandes qui ont coulées du robinet. On pourrait
les cumuler, les additionner si tu préfères. On en a d'abord eu 50, puis
60 (en tout), puis disons 80. Mais sur ces 80, à un moment 15 sont
devenues vertes. Puis on a eu jusqu'à 90 demandes, dont 20 étaient dans
l'état vert, et 10 sont passées de vert à orange. Puis de oranges elles
deviennent grises, et comme elles ne changent plus cela se cumule, cela
repose au fond de la bouteille dans un dernier état définitif.

![C'est quoi cette bouteille de lait ?](/images/2017/03/bouteille-penchee-5.jpg)

À la fin on pourrait imaginer que l'on a eu 120 demandes en tout qui ont
coulées, 10 qui sont encore dans un état vert, 10 qui sont encore dans
un état orange, et 60 dans un état gris. Si je compte bien, tu fais des
maths toi en cm2 ? Si je compte bien, il reste donc 40 demandes qui sont
encore bleues. C'est un peu ce que pourrait représenter cette dernière
bouteille.

Si je te raconte cela c'est pour prendre une image, les différentes
couleurs, les différentes étapes, c'est les différents moments d'une
activité. Cela pourrait être important pour toi de savoir combien tu en
as qui commencent, combien tu en as qui sont finis, combien tu en as qui
sont en train de se transformer. Et la représentation sous forme de
couleurs, et de lignes, cela pourrait t'aider à comprendre ce qui se
passe.

Regarde, imaginons que ton activité cela soit "manger les cookies de
papa et maman", et les différentes étapes de ton activité : les mettre
dans ton sac et les manger. Les éléments qui coulent du robinet sont les
cookies de maman et papa (ils sont indiqués en bleu). Quand tu les mets
dans ton sac ils deviennent verts, quand tu les manges ils deviennent
oranges, quand ils ont été mangés ils sont indiqués en gris. Mais donc
maman et papa ont fabriqué 120 cookies, dont 10 sont encore dans ton
sac, tu es en train d'en manger 10, et tu en avais déjà mangé 60. Dans
la vraie vie comme disent les adultes qui ont oublié de vraiment vivre,
on va utiliser des graphes (des sortes de dessins informatiques).

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods1.jpg)

Dans un fichier du genre excel (ceci est complètement irréaliste quel
parent infligerait à son enfant de huit ans un fichier excel...). Clique
sur l'image pour agrandir ou ouvre le fichier lié \[fichier
[excel](/images/2017/03/kanban-graphes-enfants.xlsx)\]. Regarde un
peu la liste des informations : maman et papa fabriquent entre 10 et 14
cookies par jour (tu as la ligne des jours à gauche, on observe
l'activité durant 20 jours), tu en mets entre 5 et 11 dans ton sac tous
les jours, tu en manges entre 9 et 4 par jour. Mais tu réussis à en
finir entre 5 et 7 par jour (je sais c'est bizarre, disons que tu as des
cookies constamment dans la bouche). Mais joli rendement. Oui je sais,
m'énerve pas, j'ai vu que les codes couleurs entre ma bouteille et mon
graphe n'ont rien à voir.

Tu as vu les colonnes : le plus au fond, à droite, les premiers états,
comme l'eau qui commence à couler dans notre bouteille, ou maman et papa
qui fabriquent les cookies. Tous les jours tu notes le nombre de
demandes qui coulent, le nombre de cookies fabriqués. 10 le premier
jour, 12 le second jour, etc. Puis un état plus récent, moins au fond,
le nombre de cookies dans ton sac, pareil, juste la somme par jour, 0 le
premier jour, ils sont en train d'être fabriqués. Etc. Sur la dernière
colonne concernant les cookies tu cumules, tu les additionnes : c'est
tous les cookies que tu as mangé.

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods2.jpg)

Tu as vu dans la colonne la plus à gauche la ligne de temps, les jours
qui défilent. C'est un joli graphe. Dans la vie les adultes peuvent s'en
servir pour voir ce qui se passe quand ils ne sont pas près de toi à
regarder si tu manges tes cookies ou non et à quel rythme.

### Blocage, pendant 5 jours tu ne peux plus avaler les cookies !!

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods3.jpg)

Imagine que tu arrêtes d'avaler les cookies ??? Tu les mâches sans fin
et ils s'accumulent car tu continues à les prendre et à les mettre dans
ta bouche du dixième au quinzième jour. Au quinzième jour tu te remets à
avaler et tu essayes de récupérer ce retard pris. Tu as vu sur le graphe
on voit la poche : ta bouche qui s'élargit pleine de cookies. Tu les
stocks dans tes joues, puis te remets à les avaler au quinzième jour. Tu
as le nombre de cookies *en train d'être mangés* qui gonfle (comme tes
joues), puis qui rétrécit à nouveau. À l'inverse le nombre de cookies
*finis, mangés* n'avance plus du dixième au quinzième jour. Et cela se
voit sur le graphe.

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods4.jpg)

### Tu n'en peux plus des cookies, tu arrêtes de les manger

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods5.jpg)

...et tu les caches dans ton sac. Imagine à partir du dixième jour tu
décides d'arrêter de manger et de les cacher dans ton sac. La colonne
*cookies en train d'être mangés* passe donc à zéro, et la colonne qui
cumule les *cookies mangés* stagne, on additionne des zéros. Mais le
quinzième jour le sac ne plus contenir de cookies supplémentaires, les
cookies commencent donc à s'accumuler chez papa et maman. Sur le graphe
c'est net : le sac grossit à partir du dixième jour, plus rien n'est
mangé : la courbe bleue est plate, et à partir du quinzième jour c'est
le sac qui ne peut plus rien avaler et qui devient plat, on voit alors
l'espace *papa et maman* qui grossit. Si papa et maman avaient regardé
le graphe ils auraient vu que tu ne finissais plus tes cookies et
auraient pu s'arrêter d'en fabriquer.

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods6.jpg)

On a déjà compris un truc important : **les poches d'accumulation ce
n'est pas terrible** et donc mise à part la dernière colonne qui est
l'accumulation des *cookies mangés* c'est plutôt meilleur signe d'avoir
une faible épaisseur, pourquoi ? (Je crois que comme avec Hansel et
Gretel j'ai perdu les enfants...)

## Deux indications de lecture du maléfique diagramme de flux cumulé

### Le temps de traverser des différents états, le "lead time"

C'est le temps que met un cookie qui vient d'être fabriqué par papa et
maman pour être mangés : il passe dans le sac, dans ta bouche, puis il
est *fini, mangé*.

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods-leadtime1.jpg)

Sur le graphe c'est facile je tire un trait horizontal du début de la
couleur *dans le sac* à la fin de la couleur *en train d'être mangés*
puis je regarde le nombre de jour qu'il a fallu et je me dis que le
temps de traversée est de trois jours. (N'oublie pas de cliquer sur
l'image si tu veux grossir, ou mange plus de cookies ça marche aussi).
C'est important, un cookie cela périme en 5 jours, là ils ont une durée
de vie de 3 jours, je ne suis donc pas inquiet pour toi.

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods-leadtime2.jpg)

Mais là c'est le scénario où tu as décidé d'arrêter d'avaler des cookies
pendant cinq jours ! Et là le temps de traversée est devenu de neuf
jours ! Normal tu as accumulé les cookies dans tes bajoues garnement. Et
là 9 jours tu pourrais avaler des cookies un peu périmés...

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods-leadtime3.jpg)

Et là ... et bien le temps de traversée est infini car je te rappelle
que tu as arrêté de manger des cookies, et que ton sac a ensuite
explosé. C'est visible non sur le graphe ? Ça n'avance plus, ça ne monte
plus, c'est littéralement "à plat".

### Le "travail en cours", le "work in progress"

Le nombre de cookies qui ne sont ni chez papa & maman ni finis, ceux qui
sont soient dans ton sac, soient dans ta bouche, en pleine activité
quoi. Facile tu traces un trait dans l'autre sens, verticalement du haut
de la ligne des cookies *dans le sac* et en bas des *cookies en train
d'être mangés* et tu comptes.

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods-wip1.jpg)

Ici au dixième jour nous avons un "travail en cours", un "work in
progress", le fameux WIP, de 19. Pas la peine de compter les cookies
encore chez papa et maman, ils ne sont pas encore en notre possession,
ni ceux que nous avons déjà mangés. Mais je me rends compte que le 19ème
jour j'ai un WIP plus petit de 12 cookies (qui sont soit dans mon sac,
soit dans ma bouche).

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods-wip2.jpg)

Ici, lors de l'événement dit "de la pause cookie", avant, le huitième
jour, j'ai un WIP de 13 cookies, mais au plus fort de la crise, le
quinzième jour j'ai un WIP de 51 cookies, dont 42 dans mes bajoues !

![C'est quoi cette bouteille de lait ?](/images/2017/03/ods-wip3.jpg)

Ici en plein désarroi j'ai un WIP de 62 mais comme surtout j'ai un temps
de traversée infini et bien je suis mal barré. **Car effectivement c'est
l'association du WIP, *travail en cours* et du temps de traversée, *lead
time*, qui peut nous donner des indications de cadence**.

## Mais qu'est ce que l'on recherche ?

### On veut des cookies frais

D'abord on veut des cookies frais. Si les cookies ont passé un long
temps dans le sac, ils ne vont pas être bons. Dans les autres contextes
c'est aussi souvent la même chose, si une ligne centrale est trop
épaisse sur le graphe, ça veut dire que les choses stagnent, et
généralement elles périment. Donc mise à part le cumul au fond de la
bouteille on voudrait des lignes pas trop épaisses concernant les
activités.

### On veut pouvoir changer le goût des cookies plus souvent

Autre avantage à avoir des lignes pas trop épaisses c'est que l'on peut
demander à maman et papa de changer la recette et on ne se retrouve pas
avec un stock de cookies au chocolat énorme alors que là on a envie de
vanille. Si il y en a un que l'on digère mal on veut pouvoir rapidement
changer la recette et ne pas se retrouver avec plein de cookies
indigestes sur les bras. Bref, là aussi c'est important d'avoir des
lignes du graphe assez fines car elles indiquent peu de stock.

**Peu de stock, peu d'inertie** ça me permet d'avoir des cookies frais,
à jour en quelque sorte, d'accueillir facilement le changement (de
recette) et les apprentissages (sur ta tolérance).

### La suite

Va ranger ta chambre et je te parlerai un de ces quatre de
**l'abominable carte de contrôle** !

### Sur Kanban

-   [Portfolio projets Kanban 1ère partie](/2016/01/portfolio-projets-kanban-partie-1/)
-   [Portfolio projets Kanban 2ème partie](/2016/02/portfolio-projets-kanban-partie-2/)
-   [Les graphes Kanban racontés aux enfants](/2017/03/graphes-kanban-racontes-aux-enfants/)
-   [Les graphes Kanban racontés aux enfants 2](/2017/03/graphes-kanban-racontes-aux-enfants-2/)
