---
date: 2012-09-15T00:00:00Z
slug: management-visuel-cognitif
tags: ['management','visuel']
title: Management visuel & cognitif
---

Une petite présentation sur le management visuel & cognitif. C'est simple, mais c'est tellement important et performant... J'espère que vous apprécierez mes dessins au touchpad dans le TGV.

<script async class="speakerdeck-embed" data-id="5054462c5281be0002021de6" data-ratio="1.4483734087694484" src="//speakerdeck.com/assets/embed.js"></script>
