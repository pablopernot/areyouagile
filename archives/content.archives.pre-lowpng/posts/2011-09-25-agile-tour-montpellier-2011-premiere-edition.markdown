---
date: 2011-09-25T00:00:00Z
slug: agile-tour-montpellier-2011-premiere-edition
tags: ['conference']
title: Agile Tour Montpellier 2011, première édition
---

Pour la première fois Montpellier organise un [Agile Tour](http://at2011.agiletour.org/fr/Montpellier.html). Le 20 octobre 2011. Enfin Montpellier ... un petit groupe d'organisateurs dont j'ai l'honneur de faire partie (sur le papier, car je ne suis vraiment pas assez présent ... mais ils me font malgré tout le plaisir de me compter parmi eux).

![AT MTP](/images/2011/09/agile_tour_MPL2011.jpg)

Le programme, les infos, les inscriptions, les sponsors (que l'on
remercie car ils nous permettent de mettre en oeuvre cette
manifestation) [Là](http://at2011.agiletour.org/fr/Montpellier.html).
L'[Agile Tour Montpellier](http://at2011.agiletour.org/fr/Montpellier.html) se
déroulera dans l'école Polytech le 20 octobre toute la journée. Et les
places seront limitées (**elles partent assez vite me dit-on,
donc...**). C'est gratuit, vous aurez à manger pour le corps et
l'esprit. A propos de ce dernier amenez le ouvert et léger.

[AT Montpellier](http://at2011.agiletour.org/fr/Montpellier.html)

Au plaisir de vous croiser dans les couloirs (haut lieu d'échanges
riches et amicaux) ou lors d'une conférence/atelier (je serai aussi à
l'[Agile Tour Marseille](http://at2011.agiletour.org/fr/at2011_marseille.html) le 13
octobre et l'[Agile Tour Toulouse](http://agiletoulouse.org/) du 19
octobre , et à [ParisWeb le 15 octobre](http://www.paris-web.fr/2011/ateliers/planification-ou-iteration-specialisation-ou-heterogeneite-transmission-orale-ou-ecrite-deux-jeux-po.php)
pour des ateliers agiles, enfin je me suis inscrit à [Agile Innovation 2011](http://agile-grenoble.org/2011/innovation) du 25 novembre de Grenoble).

Nos sponsors

![Sponsors](/images/2011/09/sponsorsat20111.png)

