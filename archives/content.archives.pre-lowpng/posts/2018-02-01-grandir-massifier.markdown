---
date: 2018-02-01T00:00:00Z
slug: grandir-ou-massifier
tags: [ masse, echelle, scaling, repetabilite, cadre, geste ]
title: Grandir ou massifier ?
---

Hier j’ai pu croiser [Thomas](http://www.cafe-craft.fr/hosts/thomas-wickham) qui tient un troquet digital, le [café craft](http://www.cafe-craft.fr/). [Pauline](https://www.le-lab-de-pauline.com/) a pu y présenter très récemment notre offre [Wake Up RH](https://www.wakeuprh.fr/) : dans cet [épisode / podcast](http://www.cafe-craft.fr/18). Thomas voulait parler “agilité à l’échelle”, comme beaucoup aujourd’hui. L’épisode sortira très bientôt, j’avais préparé ce petit texte pour entamer notre conversation avec Thomas. L’épisode le complètera.

## Pourquoi parle-t-on d’agilité à l’échelle ?

* Au delà d’approche pour les équipes (comme Scrum) on s’interroge sur l’intégralité de l’organisation. Soit sur un plan organisationnel (entreprise libérée, holacratie, etc.). Soit sur un plan opérationnel : c’est ce que les gens mettent sous le nom agilité à l’échelle : agilité à toute l’organisation.
* Mouvement renforcé par l’impression que quand cela passe à l’échelle cela devient “sérieux”, plus “acceptable”, que ces équipes agiles, parfois isolées. Un peu comme l’idée saugrenue qui persistent que le Lean est plus “sérieux” car il est “industriel”.
* Et donc l’idée sous-jacente que “ouf on va reprendre la main”. L’agilité, ou plutôt évoluer dans un monde complexe, plonge le management dans la perplexité. Il ne peut plus édicter clairement ce qu’il attend comme livrable car le monde est devenu incertain. Il doit décrire sa vision, et maintenir un cadre qui permette à chacun d’exprimer son potentiel pour y parvenir. Cela n’a rien à voir. Dans ce monde systémique, holistique, il ne peut plus jouer avec son fichier excel en ajoutant tant de jours homme ici, et en enlevant tant là, cela n’a plus de sens. Cela ne fonctionne plus. Les personnes ne sont pas interchangeables, les dynamiques sont uniques, les additions et les soustractions ne fonctionnent pas logiquement : on ajoute 2 personnes à une équipe elle va aller … plus vite ? moins vite ? Je ne sais plus. Ainsi en passant à l’échelle certains espèrent retrouver des principes de causes à effets plus clairs, plus prédictibles.
* On sait que c’est appliquant des contraintes que l’on obtient plus de prédictibilités. Par exemple les limites dans Kanban, par exemple les itérations dans Scrum. En renforçant ces contraintes à l’échelle de l’entreprise je soupçonne que certaines personnes espèrent tapisser un maillage qui leur rendent leur prédictibilité. Mais tapisser de contraintes étouffe l’implication et les résultats. L’agilité à l’échelle c’est comme son nom l’indique mettre à l’échelle, démultiplier, grandir, mais on ne change pas de monde. Il reste incertains. Il continue de demander une approche différente de nos approches traditionnelles. Des rôles différents.

Pour résumer :

* Besoin de s’interroger sur l’organisation au delà de l’équipe : organisationnellement et opérationnellement.
* Illusion du retour à l’échelle = prédictibilité, industrialisation, mais le monde reste le même : incertains, complexe.

## Quels sont alors les fondements de l’agilité à l’échelle ?

* Si on doit résumer les composants essentiels à mes yeux. La recherche est celle de la création de valeur. Quelle qu’elle soit : apprentissage, meilleurs résultats etc. On ne peut apprendre, juger, profiter, de cette valeur que si elle est menée jusqu’au bout, “finie” dans le jargon. Délivrée. Exploitée. Observée. Au plus tôt étant le mieux, pour sortir au plutôt de l’hypothèse. L’élément le plus réduit capable de proposer de la valeur “jusqu’au bout”, “finie”, c’est l’individu. Mais très rapidement il ne suffit plus. C’est dans la plus grande part des cas plutôt l’équipe. Une dynamique et un assemblage d’individus dont la pluridisciplinarité permet de délivrer des choses finies. Les contraintes imposées par des approches comme Scrum ou Kanban permettent à un groupe d’individus de travailler ensemble, de constituer une équipe, rôles, responsabilités, rituels. Des règles claires pour un objectif clair (la valeur désirée). Pour que cela fonctionne encore mieux, il est souhaitable que cet objectif clair porte un sens (et des valeurs), et que les règles laissent de l’espace pour permettre aux individus de s’engager et d’avoir les meilleurs résultats. C’est important pour la suite. L’individu ira plus vite que l’équipe (relativement) mais on préférera l’équipe car elle amène avec elle sa dose non négligeable d’intelligence collective, et sa capacité à finir des choses plus facilement (pluridisciplinarité). Ainsi donc les composants essentiels c’est l’équipe et le focus valeur et le cadre et les règles claires qui permettent aux individus de s’engager.
* Quand y a-t-il une mise à l’échelle ? Quand la création de valeur nécessite l’agrégation de plusieurs équipes. Mais en gardant bien en tête que plus il y a d’équipes plus c’est lent et compliqué pour le “fini”. Et que la valeur vient du “fini” (délivré, exploité, observé). L’agilité à l’échelle c’est donc surtout bien découper le périmètre pour permettre un maximum d’autonomie dans son “fini”, dans sa capacité à délivrer un ensemble autonome qui fait sens, pour le maximum d’équipes. Et quand cela n’est pas possible de tout de même poursuivre un maximum d’autonomie pour chacune des équipes avec des points de synchronisation fort pour a) consolider régulièrement un “fini” qui fasse sens avec plusieurs équipes b) apprendre des autres équipes, observer les émergences, les divergences, les convergences, etc. Il peut y avoir aussi exceptionnellement un rôle de synchronisation : par exemple un product manager qui synchronise les product owners en tant que méta-product owner. Un rôle de priorisation dans ce cas.

Pour résumer :

* Comprendre que c’est l’équipe le composant de base, la valeur la cible et la synchronisation le besoin.

## Vers quelles approches doit-on se tourner pour une mise à l’échelle ?

* Les erreurs d’une mise à l’échelle à mes yeux sont les mêmes qu’une approche classique autour d’une seule équipe. Dans une équipe on doit comprendre que les meilleurs résultats viennent de personnes engagées, et donc de personnes auto-organisées (Dan Mezick). On leur demande simplement de s’auto-organiser pour un certains but, un certains sens, et en respectant certaines contraintes, certaines règles. Ces règles doivent laisser assez d’espace pour avoir un vrai sentiment de contrôle de son outil de travail au sein de l’équipe. Le cadre est donc à placer, mais certainement pas le geste à définir : le geste appartient à l’équipe. Cet effort de synchronisation multi-équipes que j’évoquais fait aussi partie du cadre.
* Les deux “erreurs” (elles sont très volontaires à mes yeux) commisent par le plus connu de ces cadres “agile à l’échelle”, SAFe, est a) d’insuffler un état d’esprit contraire aux besoins de ces entreprises en leur faisant penser que “oui, le monde redevient prédictible, oui elles vont retrouver des certitudes, oui elles peuvent fonctionner en oubliant le tout, oui tout est compliqué (expertise) et non pas complexe (émergence)”. b) donner une recette sur les gestes à effectuer et non pas la clarification du cadre. C’est d’ailleurs assez révélateur que tout le monde retiennent de SAFe le “PI Planning” qui est un élément du cadre et de la synchronisation, et non pas la multitude de détails qui tuent l’engagement et les bons résultats mais rassurent le management. Je prends cela comme une bonne nouvelle – que les gens retiennent le PI Planning –.
* La mise à l’échelle n’est pas un renforcement de certains gestes. C’est un renforcement du cadre : notamment concernant la synchronisation inter-équipes, sur la priorisation inter-équipes, sur l’acceptation de certaines contraintes de qualité (par exemple tout le monde fait de la haute disponibilité – définition de fini). En voulant préciser le geste on étouffe l’implication, l’innovation, la création de valeur.

Pour résumer :

* Le cadre est important à définir, pas le geste.
* Illusions de SAFe : état d’esprit mensonger et précision qui étouffe.

## C’est donc grandir et pas massifier.

* Passer à l’échelle c’est donc grandir ce n’est pas se massifier. Comme une ruche, cela doit vivre de façon très interactive. C’est chaud, pas froid comme un schéma qui ne bouge pas. On n’ajoute pas des éléments pour ajouter des éléments dans cette mise à l’échelle. On préserve un focus valeur, on ajoute des éléments car ils amènent de la valeur. Se massifier pour réduire les coûts est une approche qui ne marche pas dans ce cadre, dans le contexte d’un monde en constante évolution, complexe (massifier peut fonctionner dans d’autres types de contexte). On rappellera l’idée clef qui veut que si vous suivez les coûts vous générez des coûts, si vous suivez la valeur vous générez de la valeur.
* Grandir ne veut pas dire répéter. On ne répète pas en tous cas le(s) geste(s) sinon on perd l’engagement des personnes, l’implication et ainsi un vrai levier pour avoir les meilleurs résultats, de l’innovation. Si on n’implique pas les personnes, non seulement on n’obtient pas les meilleurs résultats, ni même les bons, mais en plus la transformation échoue souvent, et l’organisation reprend sa forme antérieure.

## Une petite série sur mise à l’échelle (et auto-organisation).

* [Agile à l'échelle : c'est clair comme du cristal, 2013](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)
* [Agile à l'échelle : synchronisation, 2017](/2017/11/agilite-a-grande-echelle-synchronisation/)
* [Agile à l'échelle : pourquoi il faut se méfier de SAFe, 2017](/2017/11/pourquoi-faut-il-se-mefier-de-safe/)
* [Agile à l'échelle : libération de l'auto-organisation, 2017](/2017/12/agile-a-l-echelle-liberation-auto-organisation/) 
* [Agile à l'échelle : équipes et management, 2017](/2017/12/agile-a-l-echelle-equipes-et-management/)
* [Agile à l'échelle : grandir ou massifier](/2018/02/grandir-ou-massifier/)
* [Agile à l'échelle : Podcast "Café Craft" - "L'agilité à l'échelle"](http://www.cafe-craft.fr/20)

Et sinon l'événement que nous organisons en **avril 2018** autour de l'agile à l'échelle : 

* [VALUE DRIVEN SCALING](http://valuedrivenscaling.com)

