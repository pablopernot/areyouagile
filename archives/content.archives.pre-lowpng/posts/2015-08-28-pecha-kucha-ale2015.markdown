---
date: 2015-08-28T00:00:00Z
slug: pecha-kucha-ale2015
tags: ['conference','ale','organisation','pechakucha']
title: Pecha Kucha ale2015
---

Un [pecha kucha](https://fr.wikipedia.org/wiki/Pecha_Kucha) c'est 20
slides de 20 secondes, donc 6'40" pour exprimer quelque chose. C'est ce
que m'a proposé de réaliser le comité d'organisation de
[\#ALE15](http://www.alenetwork.eu/), à Sofia en Bulgarie. ALE c'est
juste l'une, si ce n'est ma conférence favorite. Le challenge ici était
naturellement de commencer à faire des conférences en anglais (j'ai
aussi organisé l'openspace avec Alexis Monville, toujours impeccable,
mais je vous en parlerai dans un autre article).

Pour revenir à l'anglais : mon accent est digne de celui de Jacques
Delors. Autant dire : il met en exergue mon origine français. Je le vaux
bien. Et bien depuis quelques années que je fréquent les milieux
européens anglophones je peux vous dire un truc : parlez l'anglais ainsi
c'est génial. On bénéficie d'une côté de popularité énorme. Les gens
trouvent tout simplement cet accent très séduisant. Plusieurs sont venus
me dire qu'ils doivent se concentrer et donc qu'ils écoutent mieux, et
comme ils trouvent l'accent très sympa, ils aiment ça. Bref, n'ayez
aucune gène avec votre accent (seuls les français s'en moquent donc on
s'en fout).

Vous trouverez les slides ici :
<http://www.areyouagile.com/slides/slum-ale2015/>

**Il vous faut taper sur 'S' pour faire apparaître les commentaires (en
anglais et français) sinon cela va vous paraître obscure.**

## Slide \#1

![Jacques Tati](/images/2015/08/ale15/tati.jpg)

**Français** Bonjour, je suis content d'être là avec vous. Pablo est un
prénom espagnol. Mais je suis bien français, 3/4 en fait, un 1 quart
hollandais, personne n’est parfait. Je vais vous parler des bidonvilles
et de l’agilité.

**English** Hello, I’m glad to be here. Pablo is a spanish surname, but
I’m french, really, 3/4 quarter french, 1 quarter dutch, nobody’s
perfect. I’m gonna talk but Slum and Agile.

## Slide \#2

![Bidonvilles](/images/2015/08/ale15/vignette2.jpg)

**Français** D’abord car les bidonvilles ont un contexte très proche de
celui de l’agilité : frugalité, économie de ressources, un lien chaos et
créativité fort, adaptation, etc. Ensuite car les solutions observées
pour améliorer et transformer ces bidonvilles sont très proches de nos
constats agiles.

**English** First because Slum are very linked to our agile context :
frugality, economy, chaos/creativity, adaptation Second because
learnings from "upgrading Slum" (improve life condition, transform
slum), are very similar and linked with what we say regarding Agile.

## Slide \#3

![Bidonville Ken Kibera](/images/2015/08/ale15/vignette3.jpg)

**Français** Les observations sur ce qui réussit à transformer ces
bidonvilles sont étonnament agiles : proximité, diversité, autonomie,
approche incrementale, etc La mise à grande échelle de celles-ci sont
aussi conformes à notre philosophie : pas de linéarit, pas de
répétabilité, pas de massification, ne pas diriger par les coûts, etc.
C’est très agréable de trouver un autre domaine qui apporte les mêmes
réponses, mais pas surprenant.

**English** What we observed about solutions for upgrade Slum are really
Agile : proximity, diversity, autonomy, incrementally, etc. Scaling
question is also related to the way we think Agile : no linearity, no
repetability, no mass approach, no cost over value, etc It’s really cool
find that another field bring the same answers, but not surprising.

## Slide \#4

![Bidonville Caracas ?](/images/2015/08/ale15/vignette4.jpg)

**Français** Mais l’exercice du pecha kucha est puissant, mais cruel, je
dois me focaliser sur quelques points. Lesquels ? Je vous propose
d’observer plutôt les variations, pour qu’un domaine enrichisse l’autre.
Je vais vous parler des causes, et de l’importance des physiques.

**English** Pecha kucha is powerful but cruel, I had to focus on few
points. Which ones ? I propose to look at variations, the other field
can enrich our. What are the learning from "slum upgrading" ? I will
talk about causes, and of the importance of physical aspects.

## Slide \#5

![Bidonville Caracas !](/images/2015/08/ale15/vignette5.jpg)

**Français** D’abord il n’y a pas de question de valeurs, de vision ou
de raison d'être (purpose), mais plutôt de causes, de bonnes causes.
Courage est une valeur. Sauver les éléphants d’Afrique, une cause.
Liberté est une valeur. Libérer votre pays de sa dictature, une cause.

**English** First there is no values, vision or purpose in slum
upgrading, but a cause, or few good causes. Courage is a value. Save
elephant of extintion in Africa, a cause. Freedom is a value. Free your
country from oppression, a cause.

## Slide \#6

![Cause](/images/2015/08/ale15/vignette6.jpg)

**Français** Les valeurs sont comme des ordres, coercitives. Et elles
sont statiques, c’est un mot figé qui n’a pas de mouvement. Un état. Les
causes sont des conteneurs qui indiquent une direction sans vous imposer
de façon d'être ni de faire. Elles induisent un mouvement, un
changement.

**English** Values are like order, coercitives. They are static, a word,
no dynamic, they are more like a state. Causes are more like contener
showing a direction and not telling you how to be and how to achieve it.
They implie a dynamic, a change.

## Slide \#7

![Cause](/images/2015/08/ale15/vignette7.jpg)

**Français** Les causes se différencient des raisons d'être (purpose)
car elles suggèrent d’aller à l’encontre de quelque chose. Elles
impliquent un changement.

**English** Causes are different from purpose because they implie to go
against something, they implie a change.

## Slide \#8

![Cause](/images/2015/08/ale15/vignette8.jpg)

**Français** Les causes se différencient de la vision car, du fait
d’aller à l’encontre de quelque chose, elles intégrent une émotion plus
forte. Martin Luther King n’a pas dit "j’ai eu une vision" mais "j’ai
fait un rêve". Les causes sont une vision chargée d'émotion.

**English** Causes are different from vision, because, due to they go
against something, they carry a stronger emotion. Marting Luther King
didn’t say "I had a vision" but "I had a dream". Causes are vision
loaded with emotion.

## Slide \#9

![Cause](/images/2015/08/ale15/vignette9.jpg)

**Français** En donnant de l'émotion, en proposant un conteneur, une
direction sans être directives, les causes sont très adaptées à nos
systèmes complexes émergents. L’autre point important qui est souligné
par les bidonvilles est l’importance des aspects physiques. Ce que nous
sommes nous, physiquement, et notre environnement.

**English** Giving emotion, proposing container, direction without
directives, causes are well adapted to our complex emerging systems. The
other underlined point by slum is the importance of physical aspects.
What we are, us, physically, and our environment.

## Slide \#10

![Physical aspects](/images/2015/08/ale15/vignette10.jpg)

**Français** Hier ([\#ALE15](http://www.alenetwork.eu/)) quelqu’un
citait Drucker : "notre culture avale notre stratégie tous les matins au
petit déjeuner". Je dirais : notre nature avale notre culture tous les
matins au petit déjeuner ! Nous oublions trop souvent ce que nous sommes
intrinsèquement. Les solutions qui fonctionnent sont pourtant liées à
notre physique. Vous connaissez aussi la loi de Conway, grosso modo :
nos produits ressemblent à la façon dont nous sommes organisés.
J’ajouterais : la façon dont nous fonctionnons devrait influencer la
façon de fabriquer nos organisations.

**English** Yesterday ([\#ALE15](http://www.alenetwork.eu/)) someone
quoted Drucker : "Culture eats strategy at breakfast". I would add : Our
nature eats our culture at breakfast ! We too often forget what we are
intrinsically. But solutions that worked are linked to our physical
abilities. You also know Conway’s law, I simplify it : The way
organizations are designed influence their products. I would add : the
way we are design, our nature, should influence our organizations.

## Slide \#11

![Physical aspects](/images/2015/08/ale15/vignette11.jpg)

**Français** Ce que nous sommes ?

-   La courbe de Allen : la communication s’effondre au delà de 15m
-   La taille des groupes (8 max)
-   Le sentiment d’appartenance à 7 ou 50

**English** What we are ?

-   Allen curve : no good communication further 15m
-   Group size for constructive communication is limited to 8.
-   We feel good in group at 7 or 50, etc

## Slide \#12

![Physical aspects](/images/2015/08/ale15/vignette12.jpg)

**Français**

-   La limite social du chiffre de Dunbar (150/220)
-   Le visuel plutôt que les mots (verbal overshadowing)
-   Le format "histoire (raconter une histoire)" améliore énormément
    (x22) votre mémorisation
-   Les hormones dont on nous a parlé hier
    ([\#ALE15](http://www.alenetwork.eu/))

**English**

-   Social limit with the dunbar number : 150/220
-   Visual versus words (the verbal overshadowing effect)
-   Storytelling helps people remind 22 times better any information
-   Hormones as discussed yesterday
    ([\#ALE15](http://www.alenetwork.eu/))

## Slide \#13

![Stigmergy](/images/2015/08/ale15/vignette13.jpg)

**Français** Edgar Morin, un philosophe français, a dit : notre nature
influence notre culture qui influence nos pratiques, qui influencent
notre nature, etc. Il y a une vraie boucle d’influence entre notre
nature et notre environnement. Les bidonvilles et la façon de les
améliorer m’ont beaucoup fait penser à la stigmergie, que je rapproche
désormais beaucoup de l’agilité.

**English** Edgar Morin, a french philosopher, wrote : our nature
impacts our culture, which impacts our practices, which impact our
nature, etc. There is a real feedback loop between our nature and the
environment around. Slum and the way they are improved, upgraded, makes
me think a lot to stigmergy and I link it to agility.

## Slide \#14

![Stigmergy](/images/2015/08/ale15/vignette14.jpg)

**Français** La stigmergie c’est l’idée que l’interaction avec
l’environnement produise de petites actions qui mènent à de grandes et
complexes réalisations. La stigmergie est un système auto-organisé
proposé par la nature. Un système sans planning sans contrôle, sans
supervision centralisée, sans arrangements compliqués.

Pensez aux termites, aux fourmis, à Wikipedia, à l’opensource.

**English** Stigmergy is the idea that interactions with environment
generates effects that build complex result. Stigmergy is a
self-organisation system proposed by nature. A system without planning,
without control, without central supervision, without complicated
arrangements.

Think termites, think ants, think wikipedia, think opensource

## Slide \#15

![Stigmergy](/images/2015/08/ale15/vignette15.jpg)

**Français** Regardez Kanban c’est typiquement de la stigmergie mise en
œuvre.

**English** Look at Kanban, it’s typically stigmergy in action.

## Slide \#16

![Conclusion](/images/2015/08/ale15/vignette16.jpg)

**Français** Ce que j’ai appris de mes lectures sur les bidonvilles, et
la façon de les améliorer, c’est l’importance des causes, ou d’une
cause, et de l’impact des aspects physiques.

**English** What I learned from my readings about slum and slum
upgrading it is how important are causes, and all the physical aspects.

## Slide \#17

![Conclusion](/images/2015/08/ale15/vignette17.jpg)

**Français** Nous disons toujours aux gens "cela dépend du contexte", et
nous ne devrions pas travailler sur autre chose : le cadre,
l’environnement.

**English** We always say to our customers : "it depends on the
context". And we should not focus and work on other thing : the
container, the physical aspects.

## Slide \#18

![Conclusion](/images/2015/08/ale15/vignette18.jpg)

**Français** Et laissez le reste se faire ! Laissez la dynamique se
débrouiller toute seule. Vous parlez d'émergence : laissez faire.

**English** And let the dynamics do it alone, by itself. You talk about
emergence : let it be.

## Slide \#19

![Conclusion](/images/2015/08/ale15/vignette19.jpg)

**Français** Pensez au forum ouvert : invitez, ayez un besoin de changer
(une cause) ; maintenez l’espace ! ou au moins essayez… :)

**English** Think open space technology : invite (with a cause, a need
to change) ; hold the space, or I least try… :)

## Slide \#20

![Conclusion](/images/2015/08/ale15/vignette20.jpg)

Merci ! Thank you !

[Olaf](http://trustartist.com/), que j'adore, a enregistré la session,
vous la trouvez là : [youtube, SLUM](https://www.youtube.com/watch?v=VQNQnHRo-b8)

Merci à mes "relecteurs" lors de la préparation (ils se reconnaîtrons).

Sur les bidonvilles : [Sur les bidonvilles](/2014/10/des-bidonvilles/)
