---
date: 2010-05-08T00:00:00Z
slug: conseils-aux-chefs-de-projet
tags: ['chefdeprojet','scrummaster']
title: Conseils aux chefs de projet
---

Ouah. Quelle [impudence](http://www.google.fr/search?hl=fr&q=define:impudence) !
donner des conseils à des chefs de projets. bon allez oui, osons. Juste
quelques réflexions sur la façon de traiter certains aspects de votre
projet. Ces réflexions viennent autant de mon expérience en la matière,
et aussi de ce que je suis (ma personnalité), elles ne vont donc pas
s'adapter à tous.

Première chose à savoir faire c'est gérer les priorités. Dans la liste
des actions à réaliser pour le projet vous devez savoir avec une
certaine précision, ou une certaine intuition, dans quelle ordre les
actions doivent s'agencer. Je ne vais pas donner d'exemple vous avez
tous en tête les problématiques d'agencement et de dépendances soient
techniques, soient organisationnelles, pour comprendre cela. En tant que
chef de projet votre capacité à bien ordonner, a bien"prioriser" les
tâches sera cruciale. Les tâches étant liées entre elles, les équipes
affectées aux tâches, la somme des erreurs (qui sont inéluctables) peut
s'avérer fatale à votre productivité. Il faut une certaine vision
(globale) sur le projet, une certaine expérience, une compréhension des
différents enjeux : un schéma mental du projet qui regroupe un peu tous
ses axes (techniques, fonctionnels, planning, relation client, dispo des
équipes, etc etc etc).Surtout ne pas avoir la "grille MS Project"
complètement incrustée en mémoire,  un projet est une chose qui évolue
constamment -contrairement à votre planning/découpage initial- et donc
cela ne marche pas ainsi. D'autant que  les bonnes priorités sont aussi
liées au rythme de votre client, à sa façon d'aborder le projet. En tous
cas :

1.  N'hésitez pas mais basez vos choix sur une réflexion logique : je
    déclenche cela parce ça ça et ça (=&gt; votre "schéma mental du
    projet").
2.  Tout le monde peut se tromper, sachez revenir en arrière si besoin,
    ou stopper une tâche en cours dès que les premiers "feedback"
    montrent que ce n'était pas le bon choix (ou parce qu'un autre
    critère est venu changer la donne).
3.  Soyez prêt à dire oui,non à tout instant. Surtout "non" en fait, car
    il faut éviter de démarrer des tâches qui seront mieux appréhendées
    plus tard dans le projet. Gardez sous le coude des tâches
    "buffer/tampon", pas complexes, incompressibles (qui devront être
    réalisées quoi qu'il arrive), qui n'ont pas besoin d'être réalisées
    par un profil particulier, et qui n'ont pas de dépendances, ceci
    afin de vous donner un peu le temps de la réflexion si besoin.

Ahh ça c'est dur. hein. Combien de fois vous êtes vous dit : "bon ok, si
je le fais cela va prendre 30mn, si je lui donne, hum... 4h". Donc vous
le faites... J'y décèle deux grosses erreurs : d'abord fini la gestion
des priorités évoquée plus haut puisque vous vous lancez dans une tâche
inattendue qui devient priorité 1 (pour de très mauvaises raisons) et
vous empêche de garder assez de recul et assez de temps pour continuer à
bien mener le projet.

Deuxièmement, vous faites un très mauvais pari pour l'avenir.  Ce choix
indique que vous n'avez pas confiance en vos équipiers.  Peut-être à
juste titre, mais le minimum est de leur donner leur chance une ou
plusieurs fois. Si effectivement vous ne pouvez pas avoir confiance en
certains de vos équipiers il faut le dire et prendre une décision à ce
sujet (j'y reviendrai). Mais il faut se baser sur des faits concrets et
responsabiliser les gens. Affecter les tâches. Dans de nombreux cas cela
va se révéler payant : des gens vont progresser, vous aurez confiance en
eux, vous pourrez déléguer de plus en plus souvent, ils en seront
contents (voire fiers).

Même cas de figure : je commence le tâche pour qu'elle parte sur "de
bonnes bases" puis je la délègue. Même problème. Sachez juger *a
posteriori :*ce jugement sera basé sur des faits ! (c'est quand même
indispensable non ? )

Je rebondis sur le paragraphe précédent. On juge les gens sur des faits.
 Donc A) pas de jugement *a priori*. Le boulot se déroule mal : trop de
retard, pas de qualité, etc. On le dit. On ne porte pas de jugement, on
dit les choses. Nous avons du retard. Pourquoi ? discutons. sachons
pourquoi. Cela n'est pas forcément lié à la personne naturellement :
elle n'a pas été formée, les specs ou exigences sont pourries, un
problème inattendue a surgit. etc etc. C'est vraiment lié à la personne
(elle n'est pas motivée, ne fait aucun effort, a des problèmes
personnels qui la rendent acariâtre, etc. ) : on lui dit. Je ne suis pas
satisfait pour telle et telle raison. J'attends de toi ça ça et ça.
Peux-tu faire un effort sur ces points. merci. Si les problèmes se
reproduisent régulièrement et sont vouées a ne pas évoluer je vous
recommande de sortir la personne du projet. Cela arrive rarement si à
intervalles réguliers on dit les choses. Dire les choses est la première
façon d'instaurer une certaine confiance ou a minima un certain contrat.
Quoi de plus injuste que de se faire sortir d'un projet sans avoir vu
venir le boulet ? Quoi de plus normal d'être prévenu lors que l'on est
pas satisfait de votre travail ? Quoi de plus grisant que de s'observer
progresser ? (je vous renvoie sur le même sujet à ce
[post](/2010/03/les-non-dits/))

Je vois trop de chef de projet qui ne disent pas les choses car ils ont
le sentiment que leur jugement est définitif et global. Si je dis à X
qu'il a mal fait cette tâche c'est comme si je lui disais qu'il est
mauvais. Rien de plus faux ! Répétez le autant qu'il le faudra à vos
collaborateurs, mais soyez convaincu vous même : oui il y a des bons,
oui il y a des mauvais, mais chacun peut changer de catégorie selon les
projets, avec le temps (dans les deux sens), selon les interlocuteurs,
selon le contexte etc. Ce n'est pas à vous d'avoir un jugement : c'est à
vous de leur laissez l'occasion de faire leur preuve. Je reviens au
point précédent : ne pas dire les choses quand elles se produisent mais
plus tard, trop tard, ne fait qu'accentuer ce sentiment et ce trouble
que le jugement est global. Si vous dites les choses ponctuellement en
liaison directe avec un évènement présent cela évite beaucoup de
quiproquo.
