---
date: 2011-04-12T00:00:00Z
slug: la-dette-technique-selon-uderzo-goscinny
tags: ['dette','technique']
title: La dette technique selon Uderzo & Goscinny
---

Hello, après avoir rapidement jetez un oeil du côté de la loi de
Parkinson, voyons aujourd'hui la notion de *dette technique*. Pour
l'historique de cette notion assez simple mais ô combien efficace :
[wikipedia](http://fr.wikipedia.org/wiki/Dette_technique). 

![Astérix ](/images/2011/04/asterix_helvete.png)

Grosso modo l'idée est que, à chaque fois que vous réalisez "à la vite" du
développement (généralement parce que quelqu'un exige une date de
livraison trop difficile à tenir et donc que la qualité disparaît, ou
parce que vos pratiques de développement ne sont pas assez pointues
-TDD, Unit Test, Pair programming, refactoring, etc-.) ce même code vous
demandera de payer des intérêts à terme. A chaque fois que vous
reviendrez sur ce code, une somme de travail supplémentaire due à sa
mauvais qualité sera -en plus-nécessaire.

![Inventor's Dillemma ](/images/2011/04/Inverntors-Dillemma-Burndown-630x489.jpg)

Comme une vraie dette, celle-ci peut s'accumuler jusqu'à rendre
complètement inerte une solution, un produit, etc. Rappelez vous bien
que votre code n'est pas un capital mais est un coût : les plusieurs
millions de lignes de code d'un code ne sont pas une richesse mais une
contrainte.

Comme le montre ce diagramme (de Agilitrix 2010, ;) ça tombe bien avec
le thème... ) si vous ne résorbez pas la dette technique, vous
obtiendrez un "dead core". Un produit foutu.

Je viens de passer quelques jours en Suisse en mission, j'en profite
afin de mieux exprimer cette idée de dette technique de faire appel aux
idées de Uderzo & Goscinny : à chaque que vous laissez filer votre bout
de pain, votre code, sans faire attention, vous accumulez du fromage
partout dans votre application qui va -peut à peut-  l'empêcher
d'avancer, jusqu'à un immobilisme certain (c'est l'application que vous
jeterez dans le lac). Il est toujours difficile de convaincre les
décideurs mais il est parfois nécessaire d'investir la résorption de la
dette technique. (merci encore à Frédéric pour son inspiration). Cliquez
sur l'image pour y accéder de façon lisible.

![Dette technique](/images/2011/04/dette_technique.png)
