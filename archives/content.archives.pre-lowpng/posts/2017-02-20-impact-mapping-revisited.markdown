---
date: 2017-02-20T00:00:00Z
slug: cartographie-strategie-impact-mapping-hors-sentiers-battus
tags: ['user','story','mapping','agile','cartographie','leanstartup','lean','startup','impact','impactmapping', 'peetic']
title: Cartographie de stratégie, l'impact mapping, hors des sentiers battus
---

Cet article est la suite de celui sur la [cartographie de stratégie](/2017/02/cartographie-strategie-impact-mapping/), il essaye
de présenter quelques façons alternatives de penser vos cartes mentales sur votre stratégie.

J'ai pu évoquer avec insistance au sujet des *story map*, des
[cartographies de plan d'action](/2017/02/cartographie-plan-action-revisitee/), leur **mise en
perspective visuelle efficace**, un **champ d'action autant auditif,
visuel, que kinesthésique**, un **séquencement soigneusement pensé pour
établir du relief dans la pensée ainsi que dans le déroulé**. Pour la
cartographie de stratégie, surtout connue sous la forme d'un *impact
mapping*, c'est pareil. Mêmes causes, mêmes effets. D'autant qu'une
cartographie de stratégie est une *mindmap*, une carte heuristique, une
carte mentale. C'est déjà en soit une approche enrichissante.

Un *impact mapping* c'est très orienté *produit*, cela vient du monde
*Lean Startup*. Pourtant vous vous dîtes à raison que cela serait
efficace d'utiliser l'approche de l'*impact mapping* pour une approche
autre que produit. Agile42 a proposé d'appeler ça une ... *strategy
map*, et je vous recommande la lecture de cet article : [Agile Strategy
Map explained](http://www.agile42.com/en/agile-transition/agile-strategy-map/explained/).

Le frein à utiliser un *impact mapping* classique c'est souvent l'étape
du **QUI**. On aimerait utiliser une cartographie de stratégie pour
notre transformation agile, digitale, etc. On aimerait utiliser une
cartographie de stratégie pour notre infrastructure de données
spatiales, ou notre travail de *datascientist*. Là cela peut-être
dommage de tordre l'*impact mapping*, autant se libérer de ses
contraintes.

**C'est plus la clarification d'une démarche que la mesure d'une
stratégie produit !** c'est pour cela que le **QUI** est malaisé. Mais
l'intérêt reste le même : **donner du relief à votre démarche pour en
mesurer rapidement l'avancée et ainsi votre stratégie**.

## La carte de stratégie de Agile42

![Agile Strategy Map (Agile42)](/images/2017/02/strategymap-2.png)

La carte de stratégie de Agile42 ([Agile Strategy Map explained](http://www.agile42.com/en/agile-transition/agile-strategy-map/explained/))
propose dans un premier temps de définir comme d'habitude un objectif
qu'ils appellent stratégique. Puis de penser à des **critères de succès
possible pour l'atteindre** (*Possible Success Factor*), et surtout des
**critères de succès critiques pour l'atteindre** (*Critical Success
Factor*). Il s'agit donc d'une pondération à deux niveaux (*must have*,
*should/could have*).

![Agile Strategy Map (Agile42)](/images/2017/02/strategymap-3.png)

Après ils ajoutent un niveau ou des niveaux qui contiennent des
**conditions nécessaires** pour appliquer les critères de succès. Et
naturellement ils priorisent pour faire émerger les chemins de valeur
afin de valider au plus tôt leurs hypothèses.

## Clarification d'une démarche : sentez vous libre

Personnellement j'ai tendance à évoquer : **un objectif stratégique**,
des **leviers** pour l'atteindre, des **conditions de succès**, et enfin
des façons de les **mettre en œuvre**. Sous forme de question comme
précédemment : *Quel est mon objectif stratégique ?*, puis *Quels
leviers dois-je activer pour l'atteindre ?*, *quelles conditions sont
nécessaires pour activer ces leviers ?* et enfin *Comment mettre en
œuvre ces conditions ?*

**C'est plus la clarification d'une démarche que la mesure d'une
stratégie produit**, je le répète.

### Mettre en œuvre une démarche agile ?

Cela pourrait être une démarche pour mettre en œuvre une approche agile
? Il va falloir pondérer : en fait il faut pondérer en cours d'atelier
pour éviter de creuser des pistes inutiles.

Cliquez sur l'image pour agrandir.

![Approche agile](/images/2017/02/approche-agile.png)

### Accompagner des datascientists ?

Autre exemple : pour les *datascientists* on a pu proposer :
*cible/mesure* puis *sources/données idéales* puis *mise en œuvre*. Ici
on a pondéré et deux chemins de valeurs à réaliser pour valider nos
hypothèses sont apparus.

Cliquez sur l'image pour agrandir.

![Gestion de doublons](/images/2017/02/doublons.png)

## La carte et le territoire

La suite c'est probablement d'évoquer le *Event Storming* du plein de
gouaille Alberto Brandolini. Avec la cartographie de plan d'action, et
la cartographie de stratégie c'est le troisième atelier que je pratique
le plus autour de la cartographie.

Mais n'oubliez pas "**la carte n'est pas le territoire**" (Alfred
Korzybski), **vous travaillez avec des hypothèses** et **l'objectif est
de valider les plus essentielles au plus tôt dans l'état d'esprit Lean
Startup**.

## Les articles de la série

1.  [Cartographie de plan d’action : le User Story Mapping](/2017/01/cartographie-plan-action/)
2.  [Cartographie de plan d’action(user story map) revisitée](/2017/02/cartographie-plan-action-revisitee/)
3.  [Cartographie de stratégie, l’impact mapping](/2017/02/cartographie-strategie-impact-mapping/)
4.  [Cartographie de stratégie, l’impact mapping, hors des sentiers battus](/2017/02/cartographie-strategie-impact-mapping-hors-sentiers-battus/)

