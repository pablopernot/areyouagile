---
date: 2013-05-02T00:00:00Z
slug: lart-de-la-retrospective
tags: ['retrospective']
title: L'art de la rétrospective
---

J'ai pu essayer lundi dernier avec [SmartView](http://www.smartview.fr)
un nouveau format, ou plutôt un nouveau thème de rétrospective. La
rétrospective est un élément essentiel de la pensée agile, elle est
l'outil de l'amélioration continue. Or la rétrospective est un moment
qui peut vite devenir usant, du fait d'un format répétitif, et de
l'implication que cet évènement requiert. Il faut donc très
régulièrement changer les formats des rétrospectives, même de façon
légère, afin d'éviter cette usure. Mais aussi dans le but de faire
émerger de nouvelles choses qui n'ont pu s'exprimer dans les précédents
formats, permettre de penser différemment.

Sinon un autre article sur les rétrospectives : [festival de rétrospectives](/2015/03/festival-de-retrospectives/). Et aussi celui-ci : [2 nouveaux formats de rétrospective](/2016/04/2-nouveaux-formats-de-retrospective/)

## Rétrospectives

Le livre de référence pour la rétrospective est [agile
retrospectives](/2011/12/quelques-bouquins-a-lire-pour-faire-de-lagile/)
de Esther Derby. Ce livre de recettes fourmille de bonnes idées, je vous
le recommande, qu'il s'agisse de rétro de projets, de départements,
d'organisations, de releases, etc.

Dans ma pratique je vais essayer d'alterner donc régulièrement les
formats, quelques exemples chez [Peetic](/2012/11/peetic/) : [le
classique](/pdf/retrospective1.pdf) (pdf), ou l'étoile de mer ci-dessous
(cliquez pour agrandir). Vous avez aussi le célèbre "speedboat" (mais
que j'utilise très peu).

![Rétrospective](/images/2012/11/retrospective2.png)

Ou sinon la timeline avec dot voting :

![Rétrospective](/images/2013/05/retro-timeline.jpg)


## Changer de cadre, pour penser différemment

Il est conseillé de changer de cadre pour essayer de penser
différemment. Je me suis donc interrogé récemment quelques nouveaux
cadres à proposer aux équipes (et que je pourrais tester chez
[SmartView](http://www.smartview.fr)).

### Ishikawa

![Ishikawa](/images/2013/05/ishikawa.png)

En me disant que la rétrospective était le lieu de la résolution des
problèmes, j'ai naturellement pensé à Ishikawa (que je manipule de temps
en temps quand on me demande certains types d'audits). Ishikawa est un
[diagramme de causes et effets](http://fr.wikipedia.org/wiki/Diagramme_de_causes_et_effets). En
adaptant un peu ce diagramme j'ai proposé une réflexion sur 5 thèmes (à
la manière de l'"étoile de mer" ou du format "classique").

Voici les 5 thèmes ci-dessous :

-   PERSONNE : relation, communication personnelle, bien-être, etc.
-   PROCESSUS : nos pratiques et façon de travailler, etc.
-   EQUIPEMENT : équipement, matériel, etc.
-   MANAGEMENT : relation, communication boite, vision, valeurs,
    objectifs, cohérence, etc.
-   ENVIRONNEMENT : (locaux, lieux, etc.

![Retro Smartview](/images/2013/05/retro-smartview.jpg)

Le résultat a été concluant, je vais donc l'essayer au sein des autres
équipes.

### Gishydo

![Gyshido](/images/2013/05/Yen-Symbol.gif)

Autre chose qui m'a bien fait rire, et m'a intéressé sur le web :
[Gyshido](http://www.gyshido.com) ("The Art of Getting Your Shit Done"
soit "l'art de réussir à sortir la merde dans laquelle on est empêtré"),
récupéré via un pourvoyeur d'idées basque : Bruno Bord @brunobord (cf le
lien de son blog dans le pied de page).

Au travers d'une approche philosophie zen, les amigos de
[Gyshido](http://www.gyshido.com) traitent malgré tout des sujets
importants. Pourquoi ne pas les utiliser en rétrospective ?

Voici les questions clefs d'une rétrospective
[Gyshido](http://www.gyshido.com) telles que je les imagine :

-   Quelles activités inutiles avons nous malgré tout réalisées ?
-   Qu'est ce qui a été embétant à réaliser ?
-   Qu'est ce qui s'est révélé pas clair, pas vrai ?
-   Quelles réunions ont été inutiles ?
-   Qui avez vous aidé ?
-   Quand vous êtes vous mal comporté ?

Questions tranchantes qui pourraient amener des réponses nouvelles,
attention cependant à manier avec délicatesse.

**Cet article a eu un complément en 2015** : [festival de
rétrospectives](/2015/03/festival-de-retrospectives/)

**Et en 2016** : [2 nouveaux formats de rétrospective](/2016/04/2-nouveaux-formats-de-retrospective/)
