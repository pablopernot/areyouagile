﻿---
date: 2017-07-07
slug: etre-agile
tags: ['agile','etre','temps','argent','responsabilisation','engagement','leanstartup']
title: Être agile ?
---

*Ce texte est la première partie d'une refonte de celui-ci :*
[modernité, agilité, engagé](/2015/01/modernite-agilite-engage/) *datant
de deux années. Je parle de cela à de nombreux auditoires. Mon discours
évolue nécessairement. J'ai voulu qu'il reflète plus ce que je dis
aujourd'hui. La deuxième partie à venir sera sur l'engagement.\[J'ajoute
que, oui c'est normal, un paragraphe est en double, et il y a des
répétitions, car c'est un texte que je* **raconte** *ainsi j'apprécie
répéter plusieurs fois la même phrase.*

## Du temps et de l'argent ou de l'implication et le bon produit ?

Les entreprises, les managers pensent qu'ils ont besoin de temps et
d'argent, de plus de temps, de plus d'argent.

Elles, ils, se trompent. Dans le monde **complexe**, changeant,
fluctuant dans lequel nous vivons ce n'est ni avoir plus de temps ou
plus d'argent qui fait la différence. Ce qui fait la différence pour les
entreprises, pour les managers, c'est une capacité à engager les
personnes avec lesquelles ils travaillent, car l'écart entre l'impact
produit par une personne non engagée ou par une personne engagée est
immense. Ce qui fait la différence c'est maximiser la valeur de ce que
nous produisons en, idéalement, minimisant les efforts pour le produire.

Oubliez le temps et l'argent. Ayez un *focus* sur l'engagement, la
responsabilisation, l'implication des personnes avec qui vous
travaillez, et assurez vous très régulièrement de produire les bonnes
choses. L'engouement actuel pour le terme Agile n'est rien d'autre que
la traduction de cela.

*Agile* c'est finalement le terme marketing pour *complexe*. Dire que
l'on veut être Agile c'est d'abord acter que le monde est complexe.
Complexe vient de "entrelacer". Les besoins évoluent, les technologies
évoluent, la communication est globale, instantanée. Impossible
désormais de nier que les choses, les projets, les produits vivront en
partie de façon imprévisible, que l'inattendu sera là, impossible
d'avoir des certitudes dans ce monde complexe. Être agile c'est savoir
évoluer dans ce monde complexe. Et, vous en conviendrez, *Agile* est un
terme plus facile à attraper que "complexe". Je doute que vous ayez
embauché un "coach complexe" ou que vous vous soyez lancé dans une
"transformation complexe".

Ainsi donc être agile c'est savoir évoluer, être performant (encore un
mot magique) dans ce monde complexe. Comment est-ce que cela se met en
œuvre ? La première des choses pour les managers c'est apprendre à
laisser les gens se responsabiliser, être autonome. Quand une personne
est responsabilisée, est autonome, elle devient beaucoup plus facilement
engagée. Et l'**engagement** multiplie considérablement l'**impact** de
ces personnes. Vous savez très bien vous-même que lorsque vous subissez
les choses sans pouvoir les contrôler, sans avoir envie, ni votre mot à
dire, votre performance est, comment dire, très médiocre. En tous cas
c'est ainsi pour moi. Et si au contraire vous êtes engagé en étant
responsabilisé, en ayant un pouvoir de décision, une autonomie, vous
devenez bien plus impliqué, bien plus performant.

Rappelez-vous ces histoires de champs de bataille où les généraux
campaient du haut de leurs collines à dicter à chacun de leurs soldats
ou de leurs escouades les mouvements et les actions qu'ils devaient
opérer. On imagine mal un tel dispositif aujourd'hui, dans une guerre
moderne. Si une troupe ou un commando n'a pas d'autonomie, elle sera bien
trop lente, bien trop inefficace, bien trop vulnérable. La
responsabiliser, lui donner de l'autonomie, c'est la rendre bien plus
efficace, mais ce n'est pas la laisser libre de faire ce qu'elle veut.
Cette troupe imaginaire a bien un objectif, elle doit détruire tel pont,
elle a bien des règles à respecter : elle ne peut pas aller à tel
endroit, car les forces ennemies sont présentes, elle ne doit pas
détruire tel autre ouvrage, etc. Ce n'est plus le geste que vous dictez
à vos équipes, c'est le cadre : l'objectif et les règles que vous
clarifiez. Libre à cette équipe, à ces personnes, de prendre cet espace,
c'est çà qui fait toute la différence en termes d'impact de performance
ou d'innovation comparée au suivi d'un processus pas à pas, fossilisant.

Être agile, évoluer dans ce monde complexe, c'est donc d'abord
responsabiliser, donner de l'autonomie, donner un pouvoir de décisions
sur tout un ensemble de sujets. Le management moderne c'est ainsi
d'abord un lâcher-prise, un laisser-faire, et une clarification du cadre
et des règles. Mais les gens ne seront autonomes, responsables, que si
ils ont le droit de se tromper. Sinon votre soi-disant
responsabilisation est un mensonge. Ce n'est que si vous avez le droit
de vous tromper que vous pouvez sereinement prendre tout le temps la
décision que vous pensez être la meilleure. Si vous n'avez pas le droit
de vous tromper, votre décision est brouillée par d'autres
considérations, vous ne choisissez plus la meilleure. Là encore une
différence fondamentale se fait. Pour que cette autorisation de se
tromper soit facile à donner, on va essayer de travailler sur des petits
morceaux. Si on se trompe sur un petit morceau, ce n'est pas grave, si on
se trompe sur un gros morceau c'est plus embêtant, implicitement si vous
travaillez sur de gros morceaux vous vous interdisez de vous tromper.
Petites itérations, peu de choses à la fois, il y a plusieurs stratégies
,mais on travaille sur de petites choses pour avoir l'autorisation
implicite de se tromper, on n'a pas la peine de se protéger.

Être agile, évoluer dans ce monde complexe, c'est ainsi
**responsabiliser, rendre autonome**, pour transformer l'impact que
peuvent avoir les gens en prenant cet espace. Autoriser les gens c'est
aussi les **autoriser à se tromper**. Cela implique une grosse part
d'**amélioration continue** le but n'étant pas de se tromper. Pour
autoriser facilement les gens à se tromper, que l'interdiction ne soit
pas implicite, on travaille par itération, ou sur peu de choses à la
fois.

Là il y a un autre paramètre important que les gens ne saisissent pas
forcément. Pour avoir un vrai regard, un vrai apprentissage, une vraie
observation sur ce que l'on a produit. Pour s'assurer de ne pas se
tromper et ainsi dire ça marche, ou ça ne marche pas. Pour avoir ce
regard, ce **feedback** dirait-on en anglais, et qu'il soit réel, il
faut observer des **choses finies**, des choses qui fassent sens par
elles-mêmes, des petits morceaux (on a dit que l'on travaillait sur des
petites choses pour autoriser l'erreur) autonomes qui fassent sens. Par
exemple, ne me dites pas que vous avez préparé tout votre modèle de
données pour les futurs rapports financiers. Au moment d'ajouter les
objets métiers, l'interface utilisateur, les aspects ergonomiques, les
choses vont changer. On croit que l'on a quelque chose de fini, mais
c'est une illusion. On fait donc perdurer le risque de se tromper. Ce
n'est bon pour personne. Quelque chose de fini aurait été l'un des
rapports ou encore plus simplement un chiffre de l'un des rapports, etc,
mais cela aurait mis en œuvre un bout d'interface, un bout d'algorithme,
d'objet métier, un bout de stockage de données, ainsi une approche
verticale. Et surtout **ce petit morceau autonome faisant sens produit
par lui-même de la valeur**. Il amène déjà par lui-même quelque chose
d'utile.

Être agile, évoluer dans ce monde complexe, c'est ainsi responsabiliser,
autonomiser, pour transformer l'impact que peuvent avoir les gens en
prenant cet espace. Autoriser les gens c'est aussi les autoriser à se
tromper. Cela implique une grosse part d'amélioration continue le but
n'étant pas de se tromper. Pour autoriser facilement les gens à se
tromper, que l'interdiction ne soit pas implicite, on travaille par
itération, ou sur peu de choses à la fois. Pour être sûr que l'on ne se
trompe pas, on porte un regard, un *feedback* sur les choses produites,
et pour pouvoir vraiment en juger, ces choses produites sont des petits
morceaux autonomes faisant sens.

Pour le management le changement de posture c'est le lâcher-prise, le
laisser-faire. Pour les gens du métier, du produit, c'est cette
capacité à ne pas arriver avec une solution globalisante, mais savoir
découper en petits morceaux autonomes qui font sens, porteurs de valeur,
et qui vont s'étoffer peu à peu.

C'est pour savoir produire ces petits morceaux autonomes faisant sens et
porteurs de valeur que toutes ces organisations se recomposent en
**équipes pluridiscplinaires** : pour avoir cette **autonomie**, cette
capacité à délivrer de façon très verticale. Entre autres raisons.

À cette réflexion sur le être agile, sur le monde complexe qui nous
entoure, il faut ajouter un paramètre : nous n'avons pas le temps de
faire tout ce que nous voudrions faire, nous en avons très très rarement
la capacité. Être agile, évoluer dans un monde complexe, c'est donc
aussi une capacité à **prioriser par valeur** (valeur d'apprentissage,
valeur concrète, etc.). Ainsi on arrive à **la promesse du être agile :
Si je suis capable de responsabiliser et d'autonomiser je multiplie
l'impact et la performance des personnes, et si je sais délivrer des
petits morceaux autonomes faisant sens, apportant de la valeur, et
priorisés par celle-ci, je sais lire, écouter, apprendre, rebondir sur
mon marché. J'économise en échouant rapidement. J'engrange des bénéfices
plus rapidement en n'attendant pas pour délivrer des ensembles trop gros
de choses.**

Pour évoluer dans ce monde complexe, pour être agile faites ce que vous
voulez tant que vous responsabilisez, vous autonomisez, ainsi vous
transformerez l'impact des gens. Faites ce que vous voulez tant que vous
avez un *focus* fort sur l'amélioration continue liée à votre
autorisation à l'échec, à votre compréhension que les bonnes pratiques
émergent dans ce monde changeant. Faites ce que vous voulez tant que
vous avez un vrai *feedback* un vrai regard sur des choses finies
faisant sens, pas sur le nombre de fonctionnalités que vous avez
déployé, pas sur le nombre d'heures que vous avez passé, mais sur le
nombre d'usagers en plus, sur le nombre de coups de téléphone à la
*hotline* liés à votre dernière livraison, etc. Des mesures souvent
externes et pas internes. Ainsi si vous êtes en mesure de prioriser ces
petits éléments autonomes faisant sens et porteurs de valeur vous saurez
lire, apprendre, rebondir sur votre marché à moindre coût et en
engrangeant rapidement bénéfices. C'est ça la promesse de l'agile.

Vous n'avez pas de problème de temps et d'argent. Vous avez un problème
d'impact de vos collaborateurs qui ne sont pas assez responsabilisés,
qui n'ont pas assez d'autonomie, et vous avez un problème pour maximiser
la valeur : vous n'êtes pas en capacité de réaction, d'apprentissage vis
à vis du marché.

Dans la série :

-   [être agile](/2017/07/etre-agile/)
-   [être engagé - partie 1](/2017/09/etre-engage-1/)
-   [être engagé - partie 2](/2017/10/etre-engage-2/)
