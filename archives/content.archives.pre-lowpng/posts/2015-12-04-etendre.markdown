---
date: 2015-12-04T00:00:00Z
slug: etendre-les-possibles
tags: ['micromanagement','espace','temps','valeur']
title: Etendre les possibles
---

Mais pourquoi donc chacun s'échine, dans les entreprises modernes, à
vouloir libérer les personnes, à vouloir impliquer, à supporter, aider,
plutôt que dominer, contrôler ou autre façon coercitive de mener ses
équipes ? La réponse est assez simple, le micro-management, le
management à la tâche, sans vision, sans implication, sans
responsabilisation porte peu de fruits, et ne convient qu'aux
entreprises médiocres. Médiocres ? Celles qui le sont, et celles qui
dominent leur marché par le biais d'un monopole ou d'une position
dominante, ou privilégiée, et qui n'ont pas nécessité à se repenser.
Leur effondrement sera soudain.

L'actualité est à une clarification -- pas forcément simple -- du sens
de l'entreprise, de sa direction, et du cadre qu'elle propose, de ses
règles claires établies. Dans ce schéma : une direction, une vision, un
sens, et un cadre qui défini les contours des possibles chacun est alors
à même d'occuper son espace, et d'occuper l'espace, de prendre l'espace.
A l'inverse d'une gestion, à la tâche, du micromanagement, sans
visibilité, qui ne laisse qu'un fil, qu'un espace restreint, sans
surprise, sans innovation, sans implication.

Encore dans l'["Homme mois mythique" de Fred Brooks](https://archive.org/details/mythicalmanmonth00fred), il est dit
qu'une personne peut donner, produire, offrir, délivrer, faire émerger, utiliser le verbe que vous chérissez, entre un rapport de 1 à 10. Donner x 1 ou donner x10. On peut oublier les budgets et les délais avec un tel spectre autant se concentrer sur ce qui permet aux gens d'être à x10 (et aussi de s'interroger sur une capacité à rester à x10 : s'autoriser des cycles, des baisses de régimes, des phases de repos, etc). J'y crois, comme je crois que l'enthousiasme fait une bonne partie des compétences. Sur cette motivation, cette implication, j'ai pu résumer mes pensées là : [modernité, agilité, engagé](http://areyouagile.com/2015/01/modernite-agilite-engage/). Je crois de plus en plus qu'il ne faut pas tant s'occuper des personnes que du cadre, de ne pas s'occuper de conduire l'énergie, que de la libérer et la laisser prendre la forme qu'elle veut.

Le problème du micromanagement c'est la voie étroite qu'il laisse à chacun.

![Micro Management](/images/2015/12/micromanagement.jpg)

En donnant un cadre on libère un espace dont chacun est à même de se saisir et de remplir. Laissez vous surprendre. Etendez les possibles.

![Cadre](/images/2015/12/cadre.jpg)
