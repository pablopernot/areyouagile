---
date: 2010-09-14T00:00:00Z
slug: richesse-des-modes-de-communication-x-rated
tags: ['management','visuel','xrated']
title: Richesse des modes de communication X-RATED
---

De l'avantage d'afficher au mur les choses : elles sont vues, elles sont
lues, elles sont manipulées. Certains osent même améliorer les
diagrammes de Scott Ambler...  découverte ce matin au mur d'un niveau
"very" hot de communication. J'ai apprécié, je vous le fait partager,
j'y vois une forme de team building : )

![XRated](/images/2010/09/richnesscommunicationmode.jpg)

Se référer à : [Richesse des modes de communications](/2010/09/richesse-des-modes-de-communication/)

