﻿---
date: 2012-03-24T00:00:00Z
slug: la-malediction-du-jour-homme
tags: ['estimation','fiboncacci','jour','homme','malediction']
title: La malédiction du jour-homme
---

Question récurrente parmi mes interventions : mais bon sang pourquoi ne
pas associer, rapprocher, remplacer les points *user story* par des
jours hommes ? Sous-entendu parfois clairement exprimé : c'est juste
"votre" mysticisme agile à la noix, mais cela n'a aucune incidence en
réalité, encore un gadget marketing ces points *user story*.

![Malédiction](/images/2012/03/jourhomme1-300x219.png)

Non. il est très important -à mes yeux- de ne pas faire de parallèle
entre les points \*user story\* et des jours hommes.

Pourquoi ?

La gestion des jours-hommes est une vieille pratique obsolète, mais
profondément ancrée dans notre culture qui nous renvoie directement au
projet au forfait et à son mensonge historique : il serait possible de
respecter une promesse à triple visage qui consiste en un engagement
simultané sur la date, le scope, le coût. (Pour l'agile c'est un
mensonge notoire). Sur le terrain j'observe que dès que l'on parle de
jours hommes cela implique *de facto* tout un ensemble d'autres
informations particulièrement nuisibles : une cristallisation autour de
cette hydre (scope,coût,date).

Imaginons 2 cas de figures concernant un manager bien intentionné qui
décide de compter en jours hommes :

## Cas de figure 1

Il observe les précédents sprints et en déduit une équivalence entre les
points user story et les jours hommes. Or lors du sprint suivant
l'équipe s'engage d'après ses calculs sur 40 jours hommes alors que si
il observe le temps de présence de l'équipe (sprint de 2 semaines, 6
personnes, soit \~60 jours), celle-ci pourrait s'engager sur bien plus.
Que va faire le manager bien intentionné. A minima il va s'en émouvoir.
Et comme c'est un manager son émotion a de l'influence, cela risque
d'entre être fini des prérogatives de l'équipe qui aura beaucoup de mal
a justifiée de ne pas s'engager "à la bonne hauteur". Et c'est en fini
de l'agilité. Fini. Basta. On est retombé dans un système de "command &
control" autant obsolète que néfaste.

## Cas de figure 2

Un manager bien intentionné demande à son équipe de chiffrer en jours
hommes directement plutôt que d'utiliser les points user story. Là, le
message est immédiat. Comment dire à son manager que l'on souhaite
s'engager sur 40 jours-hommes alors que l'équipe a la capacité d'en
réaliser 60 ? La sortie de route est rapide : soit l'équipe va gonfler
ses estimations pour faire coïncider la capacité de l'équipe et
engagement. Soit elle va effectivement s'engager sur 60 jours au
détriment de la qualité de réalisation. On retombe dans le mensonge de
l'engagement au forfait : sauvons les apparences ! en fait l'objectif de
l'équipe a implicitement changé : il ne s'agit plus de s'interroger sur
la véritable estimation des éléments, sur sa véritable capacité, mais de
réussir à faire coïncider deux valeurs (quel que soit le fond). On a
perdu le fond pour la forme. Finie l'agilité.

Dans les deux cas, l'introduction des jours-hommes a rendu les
estimations caduques. Dans les deux cas, on est revenu à une relation de
"command & control" néfaste au projet.

C'est aussi pour cette raison que je déconseille vivement l'utilisation
des "jours idéaux".

## Pourquoi les estimations de l'équipe diffèrent-elles de sa capacité de réalisation ?

Car il est très difficile d'estimer en jours hommes, voire impossible.
Il y a trop de critères, donc trop de complexité, donc trop peu de
prédictibilité pour que ces calculs soient bons. Dans la réalité les
gens qui réussissent à estimer en jours hommes ne se basent pas sur un
calcul, mais ils reproduisent et s'inspirent de leurs expériences
passées, c'est agile : il reproduise une cadence qu'ils ont observée
dans un contexte similaire. La difficulté étant justement de retrouver
un "contexte similaire", il y en a peu. Très peu. Très très peu. Par
contre des réalisations dans un projet : il y en a beaucoup et elles
sont présentes dans notre esprit, car récentes. L'estimation agile se
base donc beaucoup sur une comparaison des différents éléments du
projet. Cet élément est-il plus dur, complexe, long à faire que celui-ci
? La comparaison, et la triangulation (3 éléments) sont la base de
l'estimation agile. Si j'introduis la notion de "jour homme" la
comparaison est comme empoisonnée. On a fait entrer un élément parasite
qui rappelle de nombreuses autres contraintes. Le "jour homme" a une
capacité d'attraction fatale assez phénoménale. Prononcez le mot "jour"
ou "heure", et la personne s'enferme d'elle-même dans un piège dont elle
sort rarement gagnante.

## L'engagement est différent de l'estimation !

Je demande aux équipes agiles d'occulter les points user story lors de
leur engagement. Il faut qu'elles se basent sur leur ressenti : celui-ci
a de la valeur, car les équipes viennent de réaliser plusieurs itérations
à la durée strictement identique. Comme ci-dessus : on compare. Et on
compare ce qui est comparable : les itérations entre elles, ou les user
stories entre elles, mais pas une itération à une user story, et donc
l'engagement du sprint est une expérience différente que la simple somme
de points user story. (La vélocité peut servir d'indicateurs, mais
n'intervient pas dans l'engagement).

## À quoi peuvent servir les jours hommes ?

![Malédiction](/images/2012/03/jourhomme2-300x238.png)

Dans les deux cas évoqués plus haut (notamment le premier) vous pouvez
me répondre : ce manager bien intentionné peut aussi ne faire subir
aucune pression à l'équipe même si il décide de compter dans son coin en
jours-hommes et garder cette information pour lui ? Pour moi ce cas de
figure est extrêmement rare : pourquoi compter en jours-hommes ET garder
l'information pour soi ? A quoi cela servirait-il ? Et du moment qu'il
évoque les jours-hommes, il risque de rendre caducs les engagements et
les estimations. Donc autant ne PAS le faire.

En fait ce monsieur cherche a projeter une planification, un planning
release, savoir a priori quel sera le scope cible pour telle date afin
de lui permettre de faire des arbitrages. C'est la bonne raison (c'est
notamment le désir du product owner) : est-ce que nous sommes dans les
clous ? Est-ce que nous sommes dans le budget ? Est-ce que nous pouvons
envisager sortir ce scope à cette date ? Quel scope pouvons nous sortir
à cette date compte tenu de notre capacité ? L'agile dit : voici une
projection, cette projection est d'autant plus fiable qu'elle se base
sur la cadence que nous avons observé sur le terrain, mais cela demeure
une projection. Cette cadence est observée en user story ou en autre
chose (potentiellement simplement en user story). Mais surtout pas en
jours hommes pour les raisons évoquées plus haut.

## On ne calcule pas un budget avec la suite de Fibonacci, monsieur !

Je me suis entendu dire l'année dernière : "On ne calcule pas un budget
avec la suite de Fibonacci, monsieur !"

C'est vrai. C'était rigolo comme réplique, mais fondé. Vous voulez
savoir combien vous devez engager, ou combien vous avez engagé, c'est
normal. Rien ne vous empêche de compter VOUS en jours hommes. Mais il ne
s'agit pas d'estimation et d'engagement : ça, c'est le rôle de l'équipe
ou des équipes. Vous pouvez par exemple estimer que si on place 2
équipes de 6 personnes pendant 1 an sur un projet celui va "coûter"
\~2500 jours hommes (216\*12, 216 étant de mémoire le nb de jours ouvrés
moins les 5 semaines de vacances d'une année). On peut ainsi se projeter
en jours hommes ou faire des bilans, mais comme vous l'aurez compris on
comptabilise un engagement de moyens : combien de personnes, combien de
temps ? et pas un engagement forfaitaire : combien de personnes, combien
de temps ... et quel scope. Et la personne qui s'autorise à compter en
jours hommes pour "budgetiser" ne fait pas partie de l'équipe, sinon de
nouveau comme évoqué plus haut elle va rendre caduque les estimations.

## Le point de user story peut muter, le jour-homme est immuable.

Autre effet pervers : le point de user story peut évoluer au fil du
temps. L'équipe s'améliore, elle connaît de mieux en mieux son périmètre
et donc ses contraintes et ses facilités, son estimation va donc
naturellement évoluer. Hypothèses : ce qui était facile au début du
projet on sait désormais que c'est dur, car on a découvert les anguilles
qui se cachaient sous les roches, à l'inverse ce qui était dur au début
du projet est peut-être désormais grandement facilité (par exemple par
tout le code qui a été généré, par la connaissance que l'on a désormais
des problématiques, etc.). A quel moment les personnes qui souhaitent
faire un lien entre les points user story et les jours hommes
adaptent-elles leur taux de change ? Rarement ? c'est donc que le jour
-homme n'a pas la bonne valeur. Souvent ? Mais donc si la valeur du jour
-homme change constamment...

## Pour finir

Vous pouvez déterminer le nombre de lignes de code générées par
l'itération. En associant un taux de lignes de code par jour vous
devriez pouvoir en déduire la marge générée par le lot 1 de votre
contrat si il ne s'agit pas d'un appel d'offre publique et si vous ne
faites pas de offshore. Sinon vous devez multiplier le tout par 2,75.
Pour cette dernière recommandation, bon courage (et naturellement c'est
une plaisanterie... dire que je me sens obligé de préciser...)

Fuyez le jour-homme avant qu'il ne vous dévore.

Concernant les estimations elles-mêmes, je vous renvoie à mon [feedback sur Agile Open Sud 2012](http://www.areyouagile.com/2012/03/agile-open-sud-2012-cest-fait-aussi/).
