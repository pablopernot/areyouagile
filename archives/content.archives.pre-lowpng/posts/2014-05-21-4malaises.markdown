---
date: 2014-05-21T00:00:00Z
slug: 4-malaises-et-un-tres-bon-moment
tags: ['conference','sudweb']
title: 4 malaises et un très bon moment
---

Je suis hypnotisé par les discussions qui tournent autour de cette drôle
de session de clôture du vendredi 2014 à Sudweb. Ne vous laissez pas
embarqué par le titre (de cet article), pour lequel je n'ai pu résisté à
faire une petite allusion à un film bien connu, [SudWeb
2014](http://sudweb.fr/2014/) a été une grande réussite (je vous laisse
vous référer à tous les blogs qui en parlent, notamment
[là](http://www.js-attitude.fr/2014/05/17/sud-web-2014/)). Je ne reviens
pas là dessus, mais gardez bien cela en tête.

## Ma vision des évènements

Ma vision des évènements : l'orga Sudweb par l'entremise de Thomas me
propose de faire une session débat en commun avec [David
Bruant](https://twitter.com/DavidBruant). Session qui démarre par une
interruption. En gros il me demande d'interrompre une session de techos,
à moi ! Puis, avec David, de mener un débat autour d'un thème qui nous
est cher. Il s'agira d'épanouissement technique, nous voulions évoqué ce
qui conduisait à l'épanouissement technique. David aurait du débuter une
session sur une solution technique, mettant en œuvre une technicité
élevée, nous souhaitions interroger les gens non pas sur cette
technicité, mais sur ce qui va permettre, en amont, celle-ci. Donc on se
prépare -trop peu- avec David, et la session arrive. Et l'interruption
intervient, et ... de mon point de vue, elle marche vraiment très très
bien : tout le monde y a cru, un vrai malaise s'est instauré : hop
déséquilibre, hop ouverture, hop désordre, hop émotion, hop opportunité
de penser différemment. Signe révélateur : la personne la plus applaudie
de la journée (**tout orateur confondu**) a été celle qui a tenté de
m'arrêter en expliquant que le sujet de mon interruption était
fallacieux (et elle avait raison).

Au passage je n'ai pas été géné d'interrompre David, lui-même faisant
partie du jeu : il n'avait **pas** de session.

## La dérive

Vient ensuite la dérive, peut-être due justement à un trop grand
déséquilibre (c'est possible çà ? on était quand même en environnement
ultra-sécurisé) ? Et le débat part dans tous les sens. Peut-être ai-je
essayé trop rapidement de le cadrer en proposant un socle que j'apprécie
beaucoup et auquel je crois énormément ? Peut-être que beaucoup de
paroles attendaient de pouvoir s'exprimer ? Attention de bien comprendre
: je parle de "dérive" vis à vis de ce que j'avais imaginé. Cela ne veut
pas dire que le débat n'était pas intéressant. Il l'était mais il
partait dans tous les sens.

## Une erreur

J'ai aussi commis une erreur : j'ai personnifié l'épanouissement
technique en David Bruant. J'ai voulu discuté de comment devenir David
Bruant. A mes yeux peu importe David, mais il représente une personne
qui a su s'épanouir techniquement. Cela était une erreur car d'abord
cela mettait mal à l'aise David, ensuite car j'ai découvert que certains
n'entendaient pas le message ainsi : non pas savoir s'épanouir comme
David dans la technique, mais être "aussi célèbre" que David. Et ça,
devenir célèbre, c'était le dernier de nos soucis.

## Ce que nous voulions dire, en tous cas moi

Le sujet que j'espérais ainsi le plus développer était celui de
l'épanouissement technique. Les mots indiquent bien le sens :
"épanouissement technique", épanouissement d'abord, la technique venant
ensuite. Au lieu de pourchasser la technique mieux vaut -à mes yeux-
s'épanouir au préalable (euh et oui cela est mieux en s'étoffant : on
s'épanouit cela amène une progression globale et donc technique, on
s'épanouit à nouveau, etc, un cycle vertueux). Il est important de
comprendre -à mon avis- qu'il faut d'abord être pour faire, et non pas
faire pour être.

Pour s'épanouir je rebondissais, comme souvent en ce moment, sur les
enseignements de [Dan Mezick](/2013/10/deux-amis/), lui même les tirant
de plusieurs lectures dont le *Reality is broken* de Jane McGonigal.

Pour faire court sachez que l'industrie du jeu s'est beaucoup interrogée
sur ces notions d'engagement et d'épanouissement, qu'elle ramène à :

-   Un sentiment de contrôle (pas le contrôle de l'autre, mais le
    contrôle de son action, la maitrise de votre outil de travail si
    vous préférez).
-   Un sentiment de progrès (si vous faîtes 10 projets en simultanés peu
    importe qu'ils aillent bien, vous avancez tellement peu que cela
    sape votre engagement).
-   L'appartenance à une communauté
-   Travailler, jouer, agir pour quelque chose de plus grand que soi.

Et qu'elle rend possible avec un cadre qui propose :

-   Un objectif clair
-   Des règles claires
-   Du feedback constant
-   L'invitation (à participer, à jouer, à agir, etc.)

J'espérais, mais je ne l'ai pas vraiment eu, une discussion sur les
moyens, les idées, les retours de chacun pour promouvoir ce type de
cadre, et ce type d'effet, afin de viser un épanouissement personnel.
Certains ont [écrit](http://www.js-attitude.fr/2014/05/17/sud-web-2014/)
que cela fait bisounours (et que cela est surprenant compte tenu de ma
"bouteille"), mais plus j'accumule de l'expérience plus je réduis mes
réponses à des choses simples. Tout apparaissant de plus en plus
complexe, les réponses ne peuvent que prendrent de la hauteur en se
simplifiant (en se ramenant à leur essence). Donc, non seulement je ne
pense pas que cela soit vraiment bisounours, mais en plus je suis
convaincu du bien fondé de la synthèse de [Dan
Mezick](/2013/10/deux-amis/). Ceux qui étaient présent à la session de
[Kevin Goldsmith](http://www.kevingoldsmith.com/) le lendemain matin
(samedi), auront d'ailleurs entendu un message complètement identique de
sa part.

## De la non motivation

Plusieurs personnes ont argué que parmi leurs connaissances certains
n'étaient pas et ne seraient pas (jamais ?) motivés, que c'était dans
leur *nature*. Non, non et non (quitte à en irriter certains autres, je
préfère dire non quand je ne suis pas d'accord), ou alors 1% (moi c'est
là que je le place le 1%, et pas ailleurs
[Xavier](http://dascritch.net/post/2014/05/20/Un-Sud-Web-en-hauteur)).
L'énorme majorité des êtres humains recherche naturellement la
motivation, l'implication. Motivation, du latin, *motivi*, motif, en
quelque sorte : raison de faire, et donc raison de vivre à mes yeux.
Quand on ne peut pas, on ressent une grande frustration qui a un impact
fort sur nous. Donc oui des fois on n'a pas envie d'être motivé au
travail mais je pense que c'est parce que : a) on a des soucis par
ailleurs, et on n'a plus la force de s'engager au travail b) on ne croit
plus (à tord ou à raison) que l'engagement au travail soit possible
(mais si on n'y croyait et si cela était possible, on le rechercherait).

## De l'industrie

Plusieurs autres personnes ont aussi évoqué l'industrialisation probable
à venir de leur métiers (dans le web), faisant allusion au statut
d'ouvrier comparable à d'autres métiers. J'imagine que c'est parce que
nous sommes encore proche de l'ère industriel que les gens pensent
encore avec cette référence. A mon avis, l'ère industrielle que l'on a
connu est derrière nous et ne se reproduira plus, comme il n'y a eu
qu'un seul moyen-âge, qu'une seule renaissance, qu'une seule
pré-histoire, etc. Nous sommes à l'aube d'un grand changement de cycle,
et oui le système résiste autant qu'il le peut, mais nous ne reviendrons
pas en arrière.

## Et je suis super ravi

Je suis ravi car j'ai eu tous les genres de feedback possible. Des gens
qui sont venus chaleureusement me remercier, d'autres, par d'autres
moyens, on fait part de leur mécontentement. On a eu un débat
intéressant, enfin un débat... plutôt un échappatoire, ou un exhutoire,
comme si on avait crevé une poche. Beaucoup voulait s'emparer de la
parole, cela fait plaisir. C'est un peu la météorite qui explose en fin
de journée. Des résignements inexplicables, des idéalistes (on est tous
le bisounours d'un autre), et j'observe que cela s'agite encore à ce
sujet. Donc, que voulez vous, je suis ravi.

## 2 actions pour lundi

J'ai quand même noté deux actions "pour lundi" proposées par les
participants lors du débat : si vous souhaitez vous épanouir
techniquement : la veille, la lecture sont indispensables. Mais cela a
un prix et il faut savoir par avance que l'on ne peut pas tout savoir,
sinon on risque le vertige, la noyade. Enfin, à défaut de vivre dans un
environnement qui propose des règles claires (de conduite, de savoir
être, etc.), forgez les vôtres.

Merci à [Brice](http://pelmel.org/) & [Thanh](http://www.flou-sentimental.com/) de m'avoir transformé le temps
d'un week-end en Richard Gere.

![Richard ?](/images/2014/05/sudweb2014.jpg)
