﻿---
date: 2019-09-29
slug: modele-des-entreprises-complexes-partie-2
tags: [entreprise, agile]
title: Modèle des entreprises complexes, partie 2
--- 

Maintenant que nous avons un regard sur ces archétypes et leurs caractéristiques, projetons-nous sur la dynamique que nous pourrions accompagner. Dans la plupart des cas les gens s'interrogent quand leur entreprise est dysfonctionnelle. Nous avons évoqué deux types solutions pour une entreprise dysfonctionnelle : une revitalisation, lorsqu'il s'agit de ne pas changer sa nature (sa mission, son identité, ses convictions), mais de redevenir fonctionnelle ; Une reforme, un changement de nature, quand il s'agit de se repenser. 

Pour cela la pyramide de Dilts offre un bonne grille de lecture. Comme toujours son utilisation donne une orientation, elle ne sera pas tout le temps valable. On dira que vous êtes fonctionnel si votre mission (votre sens), votre identité, vos convictions, votre capacité (les moyens que vous vous donnez), vos comportements et enfin l'environnement que cela génère sont alignés. Dans le cas contraire vous avez une dissonance. Votre entreprise est probablement dysfonctionnelle à un ou plusieurs niveaux. Si il s'agit des niveaux plus opérationnels (capacité, comportements, environnement) vous pourriez "simplement" vous revitaliser. Si il s'agit des niveaux plus liés à votre essence (sens, identité, convictions) il se peut que cela soit l'indicateur d'un besoin de changement de nature.   

N'oubliez pas un changement de nature intervient logiquement dans le cycle de vie selon un parcours connu. Mais si il s'agit de changer de branches (en quelque sorte) nous avons besoin d'une force externe (crise, acteurs externes, etc.). 

![Pyramide](/images/2019/09/pyramide.png)

Voici quelques exemples. 

## Exemple beNext 

beNext ressemble beaucoup à une entreprise avec l'archétype adhoc de mon point de vue. Une culture entrepreneuriale qui s'adapte constamment en agrégeant des compétences. La réputation, et ainsi notre recherche d'excellence ou le *no bullshit* (deux de nos valeurs), sont très importants. Nous faisons cohabiter des fonctions plus classiques (RH, équipe commerciale), avec des équipes moins classiques créées dynamiquement par un assemblage de compétences (offre événementiel, offre school of *product ownership*, offre des robinsons (le fameux "cadrage" agile), offre *value driven entreprise*, etc.). Nous manageons par les compétences (encore recherche d'excellence), et la marque entreprise comme la marque personnelle sont clefs, nous voulons les développer, également pour nos clients (notre *leitmotiv* est *be your potential* interne comme externe). 

### Parcours

Je peux donc penser que beNext est une entreprise complexe avec un archétype de type adhoc. La structure est d'ailleurs dirigée par David et moi-même, pour former depuis quelque temps un *triumvirat* avec Dragos. Le reste comme l'indique la structure type de l'archétype adhoc est très à plat.

![beNext 1](/images/2019/09/benext-1.png)

Aujourd'hui je ne dirais pas que beNext est une entreprise dysfonctionnelle (au contraire). Nous atteignons 130 personnes, et nous nous dirigeons vers 250 dans les années à venir. Nous pourrions changer de nature. Cette expansion est une sorte d'attraction. Nous pourrions devenir un archétype produit. Des équipes par compétences, en mode lab, pourraient trouver de façon plus définitive un marché, et basculer sur des tactiques de l'archétype produit. C'est la compétence du produit qui deviendrait plus importante que celle de la personne en quelque sorte. Autre option, nous pourrions aussi pour permettre cette croissance faire disparaitre ce *triumvirat* managérial, en basculant sur un archétype holistique. Il s'agirait d'autonomiser totalement chaque groupe, pour une meilleure résilience.

### Problématique exemple : comment repenser son leadership pour aller plus loin ? 

Mais aujourd'hui beNext n'est pas dysfonctionnel et rien ne nous oblige à penser à se réformer. Nous pouvons malgré tout anticiper des aspects dysfonctionnels. 

Je vous avais parlé de notre problématique autour du leadership [ici (avec déjà la pyramide de Dilts)](/2019/04/pyramide-dilts-harmonisation-organisation/). Le *triumvirat* pourrait devenir un élément bloquant avec une taille plus grande. Avons-nous la capacité à avoir assez de leadership ? Si cela devient dysfonctionnel, souvent la réponse se trouve à l'étage au-dessus dans la pyramide qui nous sert de grille de lecture : c'est nos convictions qu'il faut travailler pour ne plus avoir de dissonance sur la capacité (pas assez de leadership, stratégie trop centralisée). La revitalisation passerait par les convictions : nous devrions peut-être repenser, aligner, notre façon d'imaginer le leadership et son émergence (en sachant que généralement pour une entreprise dans l'archétype adhoc la réputation est importante pour les enjeux individuels). 

![beNext 2](/images/2019/09/benext-2.png)

## Une grande banque 

Une grande banque, vous en connaissez tous. Nous pourrions dire qu'il s'agit de l'archétype de l'entreprise à l'échelle qui est très mécaniste. Une démultiplication, assez mécanique, de l'appareil de production, d'où leur attrait pour des socles comme SAFe. Il s'agit de mécaniser un appareil de production, pas d'être focus produit ou flux, ni encore moins spontanément créatif en se basant sur les compétences. Ce n'est pas un mal ni un bien. Il faut juste appréhender la conversation ainsi à nos yeux, et ne pas croire qu'une approche mécaniste, entreprise à l'échelle, va amener des réponses de type produit de marché, ou une capacité à évoluer en mode flux, par exemple. 

La taille des organisations qui se tournent vers l'entreprise à l'échelle permet aussi de dire que probablement elles vont se diviser en départements qui n'auront pas les mêmes archétypes. Vous connaissez la grosse structure avec l'archétype entreprise à l'échelle, et son département, son incubateur, avec son archétype adhoc. Attention il faudra comprendre et anticiper le remue-ménage quand deux cultures et archétypes si différents essayent de cohabiter. 

### Parcours

On peut imaginer un parcours de revitalisation qui consiste à distiller cet archétype de l'entreprise à l'échelle avec de nouvelles façons de travailler plus adaptées à ce monde complexe. On peut aussi imaginer une recherche de réforme de leur nature pour se replacer comme un acteur produit ou flux (archétypes produit, flux) majeur ("être le meilleur sur son produit" par exemple, avec une vraie logique de marché, des OKR). Voire une réforme complète qui propose de se réinventer (archétype adhoc). Cela change l'approche stratégique et tactique.    

![Grande banque](/images/2019/09/banque.png)

### Problématique exemple : agir en accord avec mes convictions ou me réformer véritablement ? 

Problème par excellence de ces grandes banques, elles réfutent leur nature, leur archétype entreprise à échelle, et son approche mécaniste. Dans ce monde complexe où tout le monde se dit agile elles veulent être dans le mouvement. Sur la lecture pyramide de Dilts, on pourrait dire qu'elles se donnent la capacité (en embauchant plein de "coachs agile"), qu'elles imposent de nouveaux comportements (rétrospective, management visuel, etc), mais ni leurs identités, ni leurs convictions, ni leurs sens n'ont changés. Cela provoque une dissonance forte. La dysfonctionnalité de ces acteurs vient de là. Le mieux seraient qu'ils actent de leur nature et n'essayent pas de rendre agile de bout en bout un archétype qui est plutôt mécaniste, cela marcherait probablement beaucoup mieux. Où si vraiment l'archétype de l'entreprise à l'échelle ne leur convient plus, que le risque est trop grand, ils devraient véritablement se réformer, pas uniquement sur la partie opérationnelle (capacité, comportements, environnement), mais bien au cœur de qui ils sont (sens, identité, convictions).   

![Grande banque](/images/2019/09/banque-2.png)

## Une maison du luxe 

Les grandes maisons du luxe sont le fruit initialement d'artisans de haut vol (archétype entrepreneurial), qui ont développé, dans un cycle de vie classique, une vraie gamme de produits (archétype produit). Aujourd'hui le succès est tel qu'elles sont poussées à basculer sur l'archétype de l'entreprise à l'échelle pour étendre leur domination (en quelque sorte). Mais certains y voit la perte d'un savoir-faire (ou la peur de la perte d'une identité artistique) qui faisait la différence, dans la qualité, et dans l'innovation, et tentent de relancer un archétype de type adhoc.   

### Parcours

D’archétype entrepreneurial (comme tous) pour aller vers l'archétype produit. À la croisée des chemins en ce moment elles s'interrogent pour profiter d'un effet de levier en basculant sur l'archétype entreprise à l'échelle, mais aussi sur redonner un éclat sans pareil avec un archétype adhoc. Certaines ont fait le choix de ne pas choisir et proposent des organisations différentes (des archétypes différents) selon les groupes : des produits plus "de masse", aux pièces uniques et rares. Mais c'est probablement difficile à vivre en interne. 

![Maison du luxe](/images/2019/09/luxe.png)

### Problématique exemple : Qui suis-je ? Une industrie, un artisan ou un innovateur ? 

Puis-je rester un archétype produit (logique de marché) ?  Dois-je me réinventer (archétype adhoc) en puisant dans mes compétences hors pair et garder une identité très forte ? Où dois-je rechercher un effet de levier, de masse (archétype entreprise à l'échelle) quitte à perdre mon excellence ? 

![Maison du luxe](/images/2019/09/luxe-2.png)

Voilà la fin de ces exemples. 
Bonnes réflexions pour la suite. 

## Slides, le diaporama

Plus de slides, de diapositives, mais moins d'explications ci-dessous. Si cela fait sens pour vous et pour nous, n'hésitez pas à nous joindre pour réaliser à nouveau ce meetup. 

[![Slides](/images/2019/09/slides.png)Les slides du meetup modèle des entreprises complexes](/pdf/meetup-modele-entreprises-complexes.pdf)

## Merci 

Un grand merci à [CFM](https://www.cfm.fr/) pour son accueil, son buffet, sa terrasse, ses bières. Merci à toutes les personnes présentes, pour les sourires et les conversations. 

![Le meetup](/images/2019/09/meetup2.jpg)
