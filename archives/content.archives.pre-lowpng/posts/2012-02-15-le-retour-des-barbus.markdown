---
date: 2012-02-15T00:00:00Z
slug: le-retour-des-barbus
tags: ['scrum','xp']
title: Le retour des barbus ?
---

Sous ce titre -que je sais provocateur- je voudrais parler de toutes les
revendications que j'entends autour de moi de la part de connaissances
que j'estime sur un retour aux "fondamentaux de l'agile", sous-entendu
aux pratiques d'ingénieries (pures et dures), sous-entendu aussi contre
cet agile mascarade dont ils font de **Scrum** le porte étendard.

Malgré tout le respect et l'amitié que j'ai pour eux, sur ce point, à
mon avis, ils se plantent.

Je comprends cependant leur désarroi.

## Agile (et Scrum),

c'est le buzzword. Et comme l'écrit [Comte-Sponville](http://comte-sponville.monsite-orange.fr/): "lorsque
la mode s'en mêle cela se paie ordinairement d'un certains nombres de
confusions". C'est ce qui nous arrive avec "agile". Les vingt pages du
livret du site scrum.org de Jeff Sutherland qui permettent de s'emparer
du sujet et de jouer aux apprentis sorciers.

Pour ces camarades aux profils ou aux désirs très techniques (dans le
sens très noble du terme, d'ailleurs je n'en connais pas d'autres sens)
une des façons de ramener ces moutons égarés à la raison est de monter
le prix du "ticket d'entrée" de l'agile. En rappelant tout le savoir
faire technique nécessaire au bon déroulement d'un projet agile
(**extreme programming**, ou **craftsmanship**). D'où aussi la *fatwa*
contre **Scrum** qui permet de dire que l'on fait de l'agile sans même
faire de code. Qui permet à chacun de se lancer (souvent n'importe
comment). Imaginez **Kanban**...

Si je pense qu'ils se plantent c'est que j'estime que leur réponse est
trop réductrice, et qu'ils se trompent de cible. Sinon ce qu'ils disent
n'est pas faux, et qui plus est très intéressant. Ils ont la bonne
question, mais la mauvaise réponse, ou une réponse très incomplète.

Leur réponse est trop réductrice : en voulant ramener les fondements de
l'agile à un approche du logiciel pour le développeur ils se
restreignent à un mouvement agile parmis d'autres, ni le premier, ni le
dernier, ni le fondement, ni le but ultime. L'agile ne se réduit pas à
un seul mouvement. C'est une philosophie, ou \*des mouvements\* de
pensées constitués par plusieurs branches, plusieurs écoles avec des
croisements, des recoupements, des bifurcations, etc. Je comprends en
tous cas le soin qu'ils ont à vouloir anoblir le développeur c'est un
rôle rarement estimé à sa juste valeur (la faute à qui : c'est un autre
débat).

Ils se trompent de cible en visant Scrum. Si Scrum marche autant là où
extreme Programming a échoué ils devraient s'interroger. Pour ma part
j'envisage dans un projet informatique que les deux soient plus que
complémentaires, qu'ils soient indissociables. Quand les XPistes
expliquent que XP a déjà tout (sous entendu toutes les bonnes
pratiques), je m'interroge à nouveau : pourquoi donc un échec quelques
années auparavant (échec dans le sens où l'on a pas connu le même succès
que Scrum) ? Probablement car ce "tout" est mal exprimé pour la majorité
des auditeurs (le terme "rétrospective", l'expression "définition de
done" c'est scrum, même si cela existe d'une façon différente dans XP).
Le contenu de XP est aussi à mon avis mal équilibré (Extrême...).Donc
ils se trompent de cible à mon avis. Ils contournent le problème au lieu
de le traiter. Le traiter c'est mieux communiquer, mieux appréhender ou
palier aux difficultés engendrées par le "ticket d'entrée" de scrum.
Mieux expliciter les relations fortes entre ces deux approches et les
nombreux points de convergences, les reformuler si besoin, etc.

## Pas de fausse querelle

J'adhère complètement à leur analyse et je soutien leur action : le
ticket d'entrée de Scrum est une faiblesse (mais j'ajoute que c'est
aussi une force). Oui ils ont raison de mettre en avant tous les
bienfaits et la nécessité dans le cadre informatique des pratiques
Extreme programming ou Craftsmanship. Oui l'agile dans bien des cas a
été dénaturé (mais pas dans de nombreux autres cas).

Mais je dirais plutôt : nous souffrons du succès de l'agile. Tant mieux,
mais nous devons redoubler d'effort. Oui tout le monde craint le "retour
de baton" d'une trop grande dénaturation et donc incompréhension de
l'agile. Mais qui sommes nous pour nous ériger en gardien du temple ? Je
pense agile comme une philosophie ou un état d'esprit. Il n'appartient
donc en aucun cas à un petit nombre.

Pour finir, écrire (comme j'ai pu le lire) "l'agile est mort" est pour
moi juste un aveu d'échec ou d'impuissance. La critique est trop facile.
Soyez plus constructif. Soyez agile quoi.

Je vous remercie de la lecture de ces quelques lignes (*I Thank You* une
reprise de **Sam & Dave** par les barbus sur leur meilleur album,
**Degüello**)

[ZZTop, I Thank You](http://www.youtube.com/watch?v=tkeLAqIjp-E)
