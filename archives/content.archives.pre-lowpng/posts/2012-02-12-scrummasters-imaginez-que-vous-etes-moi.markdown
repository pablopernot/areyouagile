---
date: 2012-02-12T00:00:00Z
slug: scrummasters-imaginez-que-vous-etes-moi
tags: ['scrummaster']
title: Scrummasters, imaginez que vous êtes moi
---

Devenir scrummaster c'est souvent changer de point de vue. La neutralité
et la transparence sont un prérequis. Pour citer [André
Comte-Sponville](http://comte-sponville.monsite-orange.fr/) : "là où
croît la complexité croissent aussi les exigences de clarté et de
distinction" (que je pourrais détourner ainsi : dans le monde complexe
de l'agile la transparence et la séparation des rôles et des
responsabilités garantissent son bon fonctionnement).

![Alice](/images/2012/02/Alice_Through_the_Looking_Glass-238x300.jpg)


Car contrairement à ce que beaucoup de gens imaginent : éduquer les gens
à l'agilité c'est évoluer dans un monde complexe. Scrummaster c'est
s'assurer que les contraintes libératrices nécessaires à l'émergence des
pratiques agiles sont présentes et adaptées. C'est ordonner (ordre et
rigueur dans l'agile) le conteneur qui va permettre au système et à ses
agents (à l'organisation et à ses membres) de "co-évoluer" (pour
reprendre le terme de [Dave Snowden, vidéo de ALE
2011](http://www.youtube.com/watch?v=nTZKVlP2un8)).

Souvent je suis confronté à des futur scrummasters très impliqués ( dans
un sens très opérationnel) dans les projets,. Souvent car ces personnes
connaissent parfaitement leur organisation, leurs projets et qu'elles
ont un avis -souvent bon- sur beaucoup de choses. Tout cela est positif
mais  finalement peut beaucoup nuir à leur rôle, toutes ces compétences
et connaissances seront peut-être trop envahissantes.

Pour mieux appréhender ce nouveau rôle de scrummaster je peux leur
conseiller ce petit jeu : essayez pendant quelques minutes d'être moi.
Vous êtes moi (je m'amuse mais ce n'est pas un exercice pour amplifier
encore mon égo démesuré). C'est à dire quelqu'un qui va être coach,
scrummaster, facilitateur, dans une société qu'il ne connait pas
(imaginez que vous ne connaissiez ni la structure, ni son passé, ni les
gens, etc.. je sais c'est dur, le conditionnement est un ennemi
implacable).

Donc, vous êtes moi débarquant chez vous :

En quoi suis-je légitime pour savoir quel est le besoin du product owner
? (mon rôle est d'accompagner le product owner à mieux/bien exprimer son
besoin).

En quoi suis-je légitime pour m'engager à la place de l'équipe sur le
scope qu'elle va délivrer ? (mon rôle est de m'assurer que l'équipe
s'est engagé en toute connaissance avec honnêteté, au plus juste).

![Alice](/images/2012/02/alice1.4-239x300.jpg)

En quoi suis-je légitime pour avoir un avis sur les estimations de
l'équipe ? (mon rôle est de m'assurer que l'équipe fournit les
estimations qu'elle juge réalistes, potentiellement de m'assurer que
tous les membres de l'équipe ont pu s'exprimer -et encore là je touche
une limite-)

En quoi suis-je légitime pour arbitrer certains choix durant l'itération
? (mon rôle est de m'assurer que les rôles et responsabilités de chacun
des acteurs sont bien respectés, ou dans le cas contraire que ces choix
ont été connus, analysés, validés par les acteurs du projet en toutes
connaissances de cause).

En quoi suis-je légitime pour affecter des tâches à certains membres de
l'équipe ? (je n'ai aucun rôle à ce sujet, généralement je leur suggére
de décrire les tâches nécessaires à la réussite du projet, de
l'itération, de l'objectif car cet effort de "design" est reconnu comme
une aide depuis les [règles de la méthode de
Descartes](http://fr.wikipedia.org/wiki/Discours_de_la_m%C3%A9thode))

etcetera etcetera

Cette distanciation aide à mieux appréhender le rôle de scrummaster.
Elle est nécessaire pour véritablement libérer les équipes. Et le
plaisir que vous aurez à observer cette libération est plus bien
agréable que le plaisir plus simple et plus immédiat d'un passage à
l'acte en lieu et place de l'équipe.
