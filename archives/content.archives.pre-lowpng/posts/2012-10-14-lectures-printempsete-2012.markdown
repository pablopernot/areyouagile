---
date: 2012-10-14T00:00:00Z
slug: lectures-printempsete-2012
tags: ['livre','lecture']
title: Lectures printemps/été 2012
---

5 bouquins lus récemment (mais qui datent presque tous de 2009...) sur
lesquels je souhaite revenir car ils me semblent digne d'intérêt. Lus,
démarrés au printemps et j'en achève certains là (pas mal de romans à
côté pour me vider la tête , je vous recommande le
[Déchronologue](http://www.cafardcosmique.com/Le-Dechronologue-de-Stephane),
de Beauverger, que m'avait recommandé à son tour Antoine V. Fable sur
les pirates des caraïbes confrontés à des évènements étranges et
futuristes).


## [Freedom Inc (Liberté et compagnie) (en/fr), 2009](http://freedomincbook.com/)

![Freedom Inc](/images/2012/10/Freedom-Inc.jpg)

**Brian M. Carney and Isaac Getz**

A rapprocher du suivant (Tribal Leadership), la description du nouveau
management, ou management moderne. La force de ces nouvelles approches
(auto-organisation, organisation organique -et pas industrielle-,
motivation des personnes, etc.) autour d'histoires vécues et d'exemples
assez intéressants. Pour en citer un ou deux :

-   Une organisation américaine (donc pas de "période d'essai") propose
    à ses fraîches recrues une très belle prime (disons 5000 euros) pour
    qu'elles ... quittent la compagnie. Si elles estiment que la
    compagnie en vaut une autre, autant prendre la prime et aller à
    côté. Si ce n'est pas le cas et qu'elles sont attachées aux valeurs
    de la compagnie, elles sont prêtes à sacrifier la prime. Pour la
    compagnie cet engagement est très signifique. Le coût est faible
    face aux gains.
-   Une autre compagnie transforme ses entretiens d'embauches -face à
    face- en entretien d'équipe à la cantine. Le candidat doit être
    accepté par le groupe, l'idée étant que s'insérer dans le groupe est
    important, et que l'on ne trompe pas 10 personnes. Mais je ne sais
    plus, les deux livres ayant le même fond, si cet exemple ne vient
    pas de Tribal Leadership.

Pour nous français, ce livre est aussi une bonne surprise car il cite
pas mal d'initiatives françaises ou de talentueux patrons français. (et
vous pouvez le trouver en français : "liberté & compagnie")


## [Tribal Leadership (en), 2009)](http://www.triballeadership.net/)

![Tribal Leadership](/images/2012/10/tribal-leadership.jpg)

**Dave Logan, John King, Halee Fischer-Wright**

Comme évoqué ci-dessus Tribal Leadership parle du même sujet :
management moderne, nouvelles pratiques et approches managériales (je
dirais), que Freedom Inc. Sans encore peut-être assez de recul je dirais
que Freedom Inc est plus riche, mais Tribal Leadership a la force de
proposer un système, une matrice.

Donc l'idée se dégage mieux de Freedom Inc mais se fixe dans la matrice
de Tribal Leadership.

En gros le livre propose un découpage entre 5 types de comportement/de
tribus, d'organisations. Vous vous situez dans l'un des niveaux (il
décrit surtout votre état d'esprit et votre relation aux autres, donc
votre implication). Vous pouvez passer d'un niveaux à l'autre (monter
c'est progresser, mais on peut aussi descendre), sans pouvoir sauter de
niveau. C'est intéressant car ces archétypes sont assez parlants ("life
sucks", "I'm great"...). Cela ne restreint pas à cela (il évoque les
triades -dynamique de groupe, "3 c'est une foule"- au lieu de dyades
-dynamique de binome-) mais c'est principalement l'idée.


## [Kanban pour l'IT (fr), 2012](http://www.morisseauconsulting.com/index.php/blog/)

![Kanban pour l'IT](/images/2012/10/Kanban_pour_l_IT-211x300.jpg)

**Laurent Morisseau**

Autre domaine, le livre de Laurent sur Kanban, qui vient étoffer nos
livres en français, et deviendra probablement comme le livre de Claude
sur Scrum un standard. Efficace et complet il va s'avérer très utile.
J'y retrouve les forces et les faiblesses de Kanban (à mes yeux).

De ma petite fenêtre je continue d'utiliser Kanban quand :

-   l'organisation n'est pas assez mature, ou trop en conflit, pour
    utiliser scrum.
-   l'organisation est très mature et veut s'extraire des contingences
    de deadline que possède scrum (itération).
-   de grosses contraintes matérielles : dispersion des groupes et des
    personnes.
-   dans quasiment tous les cas pour avoir une visibilité macro sur le
    portefeuille des projets, ou autre vision transverse nécessaire.

Je continue de ne pas comprendre pourquoi toutes ces sociétés qui
veulent s'accapparer le label agile mais qui ne le sont pas ne se
précipitent pas sur Kanban qui leur correspond bien mieux. Imaginez les
centres de services kanban/extreme programming. Ca ça a de la gueule.

Je continue de ne pas être sensible à toutes les formules mathématiques
employées par Kanban. C'est sûrement intéressant mais ce n'est pas à mes
yeux de la gestion de produits ou de projets, ni du management, qui pour
moi doit être humain (dans le sens empathie/non nécessairement
prédictible/complexe, émotionnel, etc.). Kanban est un modèle, une
modélisation de l'activité. Scrum une approche de dynamique de groupe.
L'agile un état d'esprit. Et le débat repart.

Je suis toujours estomaqué par la puissance de Kanban, et du management
visuel en règle générale.


## [Brain rules (en), 2009](http://brainrules.net/)

![brain rules](/images/2012/10/book_brain_rules.jpg)


**John Medina**

Et justement comment marche-t-il ce management visuel. Pourquoi notre
cerveau y est-il si sensible (en tous cas le mien, mais aussi le votre
si j'en crois ce livre). Comment fonctionne-t-il, à quoi est-il sensible
ce cerveau ? C'est le sujet de cet ouvrage très pédagogique et très
vulgarisé. Très sympa à lire. Si vous voulez avoir un peu plus mal au
crâne il faut lire "L'erreur de Descartes" (de Damasio) dont "Brain
rules" tire une partie de ses sujets. Mais là c'est du scientifique dur
et dur, du coup je ne le cite pas, mais j'en ai tiré une idée majeure
(sans savoir comment m'en servir) : votre raisonnement ne peut se
concevoir sans émotion. Si vous raisonner sans émotion vous raisonner
nécessairement à court terme. Un bon raisonnement dans le cerveau humain
véhicule nécessairement sa part d'émotion (ou provient de sa part
d'émotion).

Cela remet un peu de sel dans la discussion évoquée plus haut entre les
modélisation et les formules mathématiques de Kanban, et l'approche
*sociale* de Scrum.


## [Unfolding the napkin (en), 2009](http://www.danroam.com/the-back-of-the-napkin/)

![unfolding the napkin](/images/2012/10/unfoldingthenapkin-238x300.jpg)


**Dan Roam**

Enfin, le dernier mais pas le moindre, ce magnifique ouvrage de Dan Roam
sur l'utilisation de dessins en lieu et place de texte, powerpoint et
autre trucs froids. C'est juste magnifique. Depuis je dessine des
bonhommes chez mes clients, et cela ... marche. A lire, absolument.

## ALE 2012, session openspace *lectures*

Cela me permet de rebondir sur une session à laquelle j'ai participé
lors de l'openspace à ALE 2012. Nos listes de lectures et nos
recommandations. Je vais me borner à vous afficher la liste. Et juste
vous décrire l'intervention finale de \#\#\# qui, manifestement
enthousiaste, nous a expliqué que tous ces f\#\#\#!!! nous faisaient
croire que l'écriture était réservée à une pseudo motherf\*\*\*\* élite
et que c'était du b\*\*\*s\*\*\*. Qu'il suffisait de lire ce livre
: \**Joseph Williams Style : ten lessons to clarity and grace*\* et puis
le tour était joué, on pouvait écrire comme n'importe qui. Les deux
premiers chapitres suffisent à ses dires. Moi qui possède un style si
particulier... j'ai envie de répondre : cela ne risque-t-il pas de faire
écrire tout le monde pareil ? (ce que je constate malheureusement).

Nos retours, dans l'ordre du tableau blanc :

**Joseph Williams Style : ten lessons to clarity and grace**

**Jospeh O'conner : NLP, achieving results**

**Dave Logan : Tribal Leadership**

**Linda Rising : Fearless change**

**Gori, Le Coz : L'**[empire des coachs](/2012/04/lecture-lempire-des-coachs-de-gori-le-coz-2006/)

**Medina : Brain rules**

**Sharon Bowman : Training from the back of the room**

**Dan Heath : Switch**

**Susan Caine : Quiet**

**Henry Collins : Tacit & explicit knowledge**

**David ...K ? : Thinking for V and slow ?**

![ALE 2012 readings](/images/2012/10/readings_ale2012.png)

### Lectures, les liens depuis 2010

-   [Lectures automne/hiver 2014](/2014/03/lectures-automnehiver-2014/)
-   [Fiche de lecture 2013 : entre le cristal & la fumée (Henri
    Atlan)](/2013/12/auto-organisation-et-storytelling/)
-   [Lectures automne 2013](/2013/10/quelques-lectures-recentes-2013/)
-   [Lectures automne/hiver 2013](/2013/04/lectures-automnehiver-2013/)
-   [Lectures printemps/été 2012](/2012/10/lectures-printempsete-2012/)
-   [L'empire des coachs (Gori &
    LeCoz)](/2012/04/lecture-lempire-des-coachs-de-gori-le-coz-2006/)
-   [Quelques bouquins à lire pour faire de
    l'agile (2011)](/2011/12/quelques-bouquins-a-lire-pour-faire-de-lagile/)
-   [Fiche de lecture 2010 : Karmic
    Management](/2010/08/fiche-de-lecture-karmic-management-roachmcnallygordon/)
-   [Fiche de lecture 2010 : Scrum (de Claude
    Aubry)](/2010/08/fiche-de-lecture-scrum-guide-pratique-claude-aubry/)

