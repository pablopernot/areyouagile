---
date: 2020-11-26
slug: grooming-cette-erreur
tags: [agile, scrum, sm, grooming, affinage]
title: "Le grooming cette erreur"
--- 

Laissez-moi un peu de temps pour m'expliquer. Mais le sujet est revenu trop souvent dans nos intervisions cette semaine. Je vais d'abord pousser le bouchon : le "grooming" (l'affinage) c'est une erreur. Comme la "définition de prêt". Ce qu'ils sont devenus c'est des cache-misère. Maintenant je vais adoucir mon propos et vous expliquez l'éclairage que j'aimerais partager le plus possible. 

Le *grooming* est né pour palier à la situation : en plein *sprint planning* le *product owner* est venu avec une *user story* et patatras : mal découpée, ou trop grosse, ou des dépendances terribles qui se découvrent, etc. Et donc c'est le chaos. En dehors du fait que le chaos est souvent source de créativité, je peux comprendre que c'est fatigant et si c'est créatif, ce n'est pas forcément productif. Bref, le *grooming* est né pour anticiper ces moments de chaos et fluidifier les *sprint planning*. À raison. 

## Problème    

Les *grooming* sont devenus obèses. On en fait deux ou trois par itération (de deux semaines). Ils durent deux heures. Et ça, c'est un vrai souci. Et c'est aussi le signal que vous n'abordez pas les sujets avec une approche agile, pour moi c'est un problème, mais cela sont mes convictions. Pourquoi un problème ? Pourquoi pas agile ?   

### Une conversation pas forcément partagée

Dans les *grooming* beaucoup de personnes ont pris l'habitude d'avoir les conversations en sous-groupe, pas avec tout le monde, finalement il s'agit d'anticiper pour éviter le chaos du *sprint planning*, à deux ou trois on répond aux questions. Mais quand le *sprint planning* arrive tout est réglé. Le *sprint planning* devient une chambre d'enregistrement creuse. Et la véritable grande conversation de groupe de l'itération a disparu. **On a perdu les interactions si importantes**. 

### Trop de conversation pas assez d'action

Scénario inverse tout le monde est bien là, et on parle pendant des heures. En un sens : on réfléchit trop avant d'agir. Et une grande partie des réponses, comme toujours, viendront dans l'action. On anticipe trop. **La durée de vos conversations devrait être relative à la durée de vos itérations et proportionnelle à votre capacité de réalisation**. Si vous avez tant de conversations, c'est que vous anticipez trop, et que vous essayez de trop répondre par avance. Justement ce qu'essaye d'éviter une approche agile.  

### La bonne conversation au bon moment : le *sprint planning*

Finalement vous devriez juste traiter ce qui va arriver bientôt, et vous ne devriez pas y répondre définitivement en sous-groupe, mais avoir assez d'information pour que cela soit traité sereinement dans le *sprint planning* qui reprendrait tout son sens. Le *sprint planning* est le début de l'itération. Il a du sens aussi parce que la revue et la rétrospective ont eu lieu avant, et *normalement*, elles ont apporté de nouvelles informations au *product owner* et à l'équipe.  

### Étape par étape

On m'interroge (lors de ces intervisions) : *mais pour les grosses dépendances, les gros sujets ?* 
Comme toujours les gros sujets sont traités étape par étape par priorité d'importance, de valeur. Si vous avez besoin d'une sorte de gros défrichage initial c'est peut-être justement l'occasion de ces *quarterly planning* (ou *PI planning*), d'avoir une approche disons trimestrielle ou au démarrage durant deux ou trois jours vous partagez et traitez ces "grandes" questions.  

## Les signaux à observer 

* Vos *sprint planning* durent plusieurs heures (2 à 3h disons ) et c'est là que les débats ont lieu. C'est un bon signal. 
* Les conversations du *grooming* ne sont que des anticipations pour rendre la conversation possible. C'est un bon signal. 

## Le mal fondamental avec un *grooming* obèse

Les *grooming* trop fréquents, trop longs, sont là pour justifier les dépendances (comme le tableau du *pi planning*) au lieu de les régler. Les dépendances doivent être limitées. Les *grooming* obèses les cautionnent. Le mauvais *grooming* sert à entrenir ces dépendances. Le *grooming* grossit, car elles existent et on ne cherche pas à les résoudre. C'est exactement ce qu'il faut éviter. 

## Mon expérience de Scrum 

[**Petit rappel sur scrum**](https://pablopernot.fr/pdf/guiderapide.pdf)

Je vous encourage à interroger les personnes avec qui j'ai pu travailler dans ce cadre. 

## Conclusion

Hier matin je me suis demandé, confronté à la question récurrente du *grooming* et puis confronté aussi soudainement à cette hérésie sur le *scrummaster* dans le *scrum guide* (prochain article), je me suis demandé : suis-je condamné à répéter jusqu'à la fin de mon temps les mêmes choses ? Et puis, je me suis demandé : est-ce un problème ? Peut-être pas. Je suis prêt à essayer d'expliquer mon point de vue *ad vitam aeternam* c'est peut-être mon rôle, cela manque peut-être d'humilité.    

