---
date: 2021-05-01
slug: roadmaps-dependances 
tags: [planning, release, roadmap, roadmaps, dependances, delai, attente]
title: "Roadmaps & dépendances"
--- 


À l'occasion d'un meetup [school of product](https://schoolofpo.com) (mes acolytes), j'ai parlé des *roadmaps*, des dépendances et des temps d'attente. La *roadmap* ou plutôt le [plan de livraison](/2020/01/plan-de-livraison/) est un élément clef. Il ne s'agit pas d'une session "basics" (les basiques, les bases, sous-entendu pour débutant), il s'agit d'un session "fondation", de fondamental, soit indispensable. 

## Les slides (cliquez sur l'image): 

[![Slides roadmaps et dépendances](/images/2021/05/roadmaps-dependances.png)](/pdf/2021-roadmaps-dependances.pdf)



## Enregistrement de la session 

{{< youtube id="qkSWp36USB4" >}}

### Complètement

* Le petit complètement suite aux conversations post-session : [Le grooming cette erreur](/2020/11/grooming-cette-erreur/).    



