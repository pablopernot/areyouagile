---
date: 2020-11-09
slug: une-possible-histoire-de-l'agilite
tags: [agile, coach, sm, po, ux, pm, data, histoire]
title: "Une possible histoire de l'agilité, COACH, SM, PO, UX, PM, DATA"
--- 

En face d'un groupe de UX (*user experience*) lors d'un de nos fréquents événements chez benext. J'essaye tant bien que mal de donner de la perspective à ce rôle, je me lance dans une "possible histoire de l'agilité". 

J'écris cela avec le sourire : 1999, **XP** (*extreme programming*) voit le jour. Quelques bouquins de **Kent Beck** fleurissent sur les tables. Certains se lancent. 2003, le choc brutal de la crise range beaucoup de cela dans les armoires, et le côté extrême de la communication finit de le mettre dans l'ombre. 2005 ou 2006 un petit fascicule de **Henrik Kniberg** décrit des parties de poker un peu drôles au milieu des développeurs. Il suscite un intérêt. En 2008 une nouvelle crise économique surgit. C'en est fini des CMMi et consorts, gros trucs lourdingues *has been*. Les US (en avance sur nous) se lancent à bras le corps sur cette approche qui leur ressemble (**SCRUM**) (puis retour de **XP** sous **software craftsmanship** et **Kanban**). 

En France cela frémit sévèrement de 2010 à 2013. 2013 C'est l'année où tous les grands groupes se lancent. Et où les questions d'agile à l'échelle émergent chez nous. 

Et les UX ? Puisque c'est ma conversation du moment, à quel moment le terme se propage-t-il ? 2015 me dit-on. Son employabilité explose en 2018. Et pourquoi cette bataille larvée avec les PO (*product owner*) ? Ma lecture ? La voici. Elle vaut ce qu'elle vaut. Elle est plutôt pessimiste sur le court terme. Sur le long terme, je reste optimiste.  

De 2010 à 2015 agile, c'est SCRUM. SCRUM c'est d'abord le coach et le scrummaster dans l'imaginaire des organisations. Mais les années passant et le refus de vraiment s'y mettre de la majorité des entreprises, de vraiment s'approprier la culture et l'approche, va les pousser à changer d'interlocuteurs. Le scrummaster et plus encore le coach sont souvent trop gênants dans le sens où ils impactent directement le coeur du système : son organisation, ses jeux de pouvoir, la décision. On vide alors souvent de sens ces rôles en en faisant des potiches ou des faire-valoir dans de nombreux cas (pas partout rassurez-vous). Des facilitateurs, mais certainement pas des coachs agiles : les coachs peuvent être des facilitateurs. En ajoutant le mot agile vous ajoutez **des convictions, une façon de voir le monde, une école, une approche** plus opérationnels qu'une école de coaching (ce n'est pas le débat). 

Personne n'est dupe, pas l'entreprise elle-même naturellement, qui va essayer de s'approprier l'agilité (dont elle sent pertinemment qu'elle en a besoin) avec un rôle moins au coeur de l'entreprise, celui de *product owner*. Moins au coeur ? Oui. Le produit elle le fabrique, elle le diffuse. Il ne régit pas sa culture ni sa prise de décision. Et puis les coachs agiles et les scrummasters ont la fâcheuse manie de ne pas faire mais d'être et d'accompagner. Le *product owner* lui il fait. Temporalité très différente, système de causes à effets beaucoup plus classique.  

La nouvelle agilité ce n'est plus le *coach agile* ou le *scrummaster*, c'est le *product owner*. Et puis celui-là on peut le mêler à toutes nos sauces. Il s'occupe du produit. Mais là aussi chassez le naturel il revient au galop. Dans la plupart des structures, le *product owner* devient le responsable **du développement** du produit (aïe). Et comme souvent le produit est un projet. Il devient le responsable du développement du projet. Vous voyez où je veux en venir ? 

2015-2018 c'est l'âge d'or de l'employabilité du *product owner* et *product manager*. Mais les gens sont bons. Ils se débattent pour ne pas être broyés par l'entreprise. Ils défendent bec et ongle le *product owner* celui qui n'est pas un chef, mais qui a les yeux rivés sur son produit, tactique, stratégie, facilitation autour de lui et de ses utilisateurs et de ses *fabricateurs*. Le produit n'est pas le coeur de l'entreprise (oui cela créé une tension de lire cela), mais les *product owner* sont encore trop centraux avec leur **revendication sur la valeur**. L'entreprise doit repousser plus loin ces velléités. Remettre de l'ordre. 

2019-2020, c'est là que le succès de l'employabilité des UX explose (*user experience*, avant on les appelait les ergonomes si cela peut vous aider). Ils sont plus à la périphérie. Ils ne décident pas vraiment, ils sont comme les *product owner* avec moins de revendication, ils savent lire les utilisateurs. Aujourd'hui d'ailleurs on les mets en concurrence avec les *product owner*, le terme *product design* commence à se répandre. Quand j'interroge les gens sur la différence, la seule réponse sensée qui émerge c'est : une compréhension des mécanismes cognitifs des utilisateurs et donc mieux intégrer ceux-ci. Pour le reste c'est ce que faisaient les *product owner*, les *scrummasters* et les *coachs agiles* avant. Et d'autres avant eux. *Design  Thinking*, *Lean startup* et leurs ancêtres. Maintenant y'a même *Lean UX*. Kamoulox[^1].

[^1]: Allusion à une émission parodique qui donne ici à Kamoulox le sens : "c'est vraiment n'importe quoi".  

Comme ils sont plus à la périphérie, le reste de l'entreprise n'a pas besoin de changer, de se remettre en question. Ainsi on les plébiscite. Bizarrement depuis leurs succès les *product owner* ont retrouvé une certaine liberté, et avec, un certain sens. 

Les UX sont très bien. C'est à ceux chez benext que je m'adresse quand j'aborde cette possible histoire de l'agilité. Avec de la perspective, c'est surtout le mouvement d'expulser vers l'extérieur ce qui viendrait bouleverser les habitudes de l'organisation qui me frappe au long de ces années. 

Et demain alors ? Naturellement les prédictions n'engagent que les crédules. La data qui nous permettra d'enlever toutes décisions humaines et donc gênantes ?  










