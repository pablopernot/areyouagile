---
date: 2016-03-08T00:00:00Z
slug: were-gonna-groove
tags: ['agilefluency','raidagile','fluency']
title: We're gonna groove
---

**Pablo** : Claudio on a un souci.

**Claudio** : Oui Pablo, après celui des certifications, un deuxième
fléau guette le mouvement agile : le fléau des évaluations. On veut
connaître son niveau d'agilité, savoir si on a une plus grosse agilité
que son voisin ou son concurrent.

**Pablo** : Ah mais il m'arrive d' "auditer" l'agilité d'entreprises.

**Claudio** : Moi aussi ces dernières années, j'ai pratiqué de
nombreuses évaluations ou audits "agiles", j'ai réfléchi à la façon de
les mener, notamment avec Alexandre Boutin, j'ai discuté avec des gens
que cherchaient à définir un modèle d'évaluation agile. J'ai lu de
nombreuses propositions de modèles, le dernier en date étant celui du
[Cigref](http://www.cigref.fr/rapport-cigref-agilite-dans-lentreprise-modele-de-maturite),
inutilisable de mon point de vue. J'en suis arrivé à la conclusion
suivante : ce qui compte, ce n'est pas tant de savoir où on est, mais la
route à suivre. L'agilité est un voyage. Attention, il me paraît
légitime de s'évaluer dans le but de progresser. Mais rester dans les
schémas classiques de l'évaluation et les appliquer à l'agilité, c'est
là que se situe le danger.

**Pablo** : Oui naturellement pas aisé d' "auditer" en quelque sorte
quelque chose qui par définition d’une part émerge et est contextuel et
d'autre part se base autant sur des indicateurs du cerveau gauche que
droit : vulnérabilité, émotion, ressenti, valeurs, culture, etc. J'aime
bien le ["scorecard" de
Spotify](https://labs.spotify.com/2014/09/16/squad-health-check-model/),
ou le [tableau présenté par Olaf Lewitz et Michael
Sahota](http://areyouagile.com/2015/05/quest-ce-quune-transformation-agile-reussie/)
autour des niveaux de "Reinventing Organisations", je me sers de ces
deux éléments.

**Claudio** : C'est avec cette idée que je me suis intéressé au modèle
[Agile Fluency](http://martinfowler.com/articles/agileFluency.html), dès
qu'il est sorti. J'ai essayé de me l'approprier. J'ai aussi tenté de le
partager avec d'autres. Il n'est pas si facile à appréhender, et cela
n'a pas bien marché, les gens n'ont pas adhéré. Sauf avec toi, Pablo.
Nous avons présenté le modèle dès le premier Raid Agile et depuis le
dernier, nous avons carrément décidé d’y consacrer un atelier, basé sur
[celui présenté chez Spotify et
Crisp](http://blog.crisp.se/2015/12/15/peterantman/fluent-at-agile-visualizing-your-way-of-working).

**Pablo** : Je dois t'avouer que depuis que tu m'as présenté [Agile
Fluency](http://martinfowler.com/articles/agileFluency.html) je
l'utilise fréquemment, pour des évaluations ou des audits, ou pour
démarrer mes cycles d'"[Open Adoption](/2015/11/darefest-2015/)" (cycles que je
fais désormais de plus en plus de façon mensuel ou bimestriel, et hors
des contrées informatiques, mais c'est un autre sujet).

**Claudio** : Oui, c'est un atelier qui fait réfléchir à là où on est
(mais sans niveau d'agilité) et surtout à là ou on veut aller (mais sans
marche forcée à utiliser plein de nouvelles pratiques agiles). Nous
allons le "customizer" un peu plus pour le prochain Raid, celui d'avril.

**Pablo** : Oui j'aime bien l'idée d'espaces d'expression au lieu des
niveaux. Un peu comme une carte avec différents paysages, à savoir si
vous voulez bâtir un royaume, ou simplement vous sentir bien dans votre
région. Il n'y a pas de meilleure réponse.

**Claudio** : C'est un atelier qui parle de musique (il y est donc
question de Led Zeppelin). Il faut lui donner un nom à cet atelier. Je
te propose **la route du groove agile**. Groove parce que musique,
groove parce que c'est une sensation, une dynamique qui correspond bien
à une équipe Scrum épanouie. Ou tout simplement "groove". Qu'en dis-tu ?
Les Raiders repartiront avec leur feuille de route vers le groove agile.

![Claude & Pablo](/images/2016/03/claude-pablo.jpg)

**Pablo** : Ah difficile de résister à notre passion commune pour le
dirigeable. Tant qu'a y être reprenons un de ses titres (même si c'est
une reprise) : "[We're gonna
groove](https://www.youtube.com/watch?v=479SdH4UwuE)", et ne customizons
pas, remasterisons. J'aime bien l'idée de "groove" elle se rapproche de
l'idée de "performing" ce dernier niveau de dynamique d'équipe, quand
tout semble s'emboiter naturellement, quand il y a une intuition
partagée, une résonance limbique dirait Daniel Goleman. Et le groove
c'est pareil, c'est là, tu dois remuer, bouger, c'est intuitif. Et tout
le groupe se met à remuer.

**Claude** : Le groove, c’est quand on arrive à faire les choses
ensemble sans s’en rendre compte, cela devient de la routine, et qu’on
peut prendre des initiatives pour aller plus loin. Improviser tout en
restant dans la dynamique d’équipe. Pour le rugby on dirait, pour
reprendre Pierre Villepreux, l’intelligence situationnelle.

**Pablo** : Y’a un magazine très connu sur Led Zep qui s’appelle Tight
But Loose, avant il était papier, maintenant il est
[web](http://www.tightbutloose.co.uk/). C’est comme cela que l’on
appelait la façon de faire des concerts du Zeppelin : Tight But Loose :
serré mais lâche (relaché) à la fois. On est carré, mais dedans on est
libre. Bref je faisais de l’agilité en 1997 en faisant cela : [Zepablo’s
Led Zeppelin](http://pablo.pernot.free.fr/) :) (quiz : trouvez ma photo
avec Page & Plant).

**Claudio** : Allez, puisqu’on est dans les confidences, j’étais déjà
agile il y a bien plus longtemps encore, en escaladant le grillage pour
assister au [concert de Led Zeppelin au Parc des Expos de
Nancy](http://www.aubryconseil.com/post/2007/07/28/270-pas-de-bottleneck-au-concert-de-robert-plant-ex-led-zeppelin)
:)

**Pablo** : J'ai aussi beaucoup apprécié les interrogations soulevées
par l'atelier, la dernière fois que je l'ai proposé j'ai simplement posé
les feuilles que nous avons adaptées sur la table et j'ai demandé aux
gens de fabriquer quelque chose avec. Toutes les questions qui ont
émergé m'ont semblé bonnes.

**Claudio** : “Agile Fluency” n’est pas si simple à appréhender. Avec
cet atelier, qui s’appelle donc “we’re gonna groove”, le cadre du jeu
favorise cette émergence, qui est renforcée par la métaphore musicale.

**Pablo** : Comme je te le disais dans le cadre d'un transformation
agile avec des équipes qui pratiquent déjà c'est un formidable point de
départ pour des cycles d'Open Adoption. On obtient un paysage
intéressant, une carte, un point de départ passionnant pour
expérimenter. Mais la carte n'est pas le territoire, ne l'oublions pas,
les discussions vont émerger là : les pratiques c'est la carte, le
territoire c'est les valeurs, la culture, la pensée agile, et comment
elles se traduisent au quotidien.

**Claudio** : J’ai essayé de présenter cette articulation (notamment
Agile Fluency, Open Adoption) dans le chapitre 22 de la dernière édition
de mon livre, “Transformer les organisations”, que tu m’as d’ailleurs
aidé à structurer. L’atelier We’re gonna groove rend cette approche
accessible et actionnable.

**Pablo** : Au raid, on profite aussi de l'atelier "[Scierie à
pratiques](/2016/02/la-scierie-a-pratiques-evolue/)"
pour instancier la liste des pratiques. La liste fournie initialement
par l'atelier est très orienté "spotify", donc très devops. C'est
intéressant mais pas forcément adapté à tous. On peut aussi plonger dans
la liste que tu proposes dans ton livre, elle est pléthorique.

Bon eh bien écoute je suis ravi de savoir que nous allons avancer sur ce
thème ensemble. [Rendez vous du 19 au 22 avril au Raid
Agile](http://raidagile.fr).

Led Zeppelin We’re gonna groove lors de l’extraordinaire concert au
Royal Albert Hall 1970.

{{< youtube id="479SdH4UwuE" >}}


(article co-signé [Claude Aubry](http://www.aubryconseil.com/) & Pablo
Pernot)
