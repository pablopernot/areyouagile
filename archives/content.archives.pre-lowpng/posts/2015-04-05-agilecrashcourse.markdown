---
date: 2015-04-05T00:00:00Z
slug: age-of-agile
tags: ['jeu','videogame','game']
title: Age of agile
---

Je croise Anthony au badminton, depuis quelques années, quand j'ai le
temps d'y passer. Je savais que Anthony travaillait dans les jeux vidéos
d'entreprise, les serious games. On avait pu échanger sur le petit monde
montpellierain où voilà 15 ans nous avons failli nous croiser (Yéti &
Albert).

![We play agile](/images/2015/04/ageofagile-steppe-400.jpg)

C'est lui qui est revenu vers moi il y a quelques mois. La lecture de la
[horde agile](/2014/01/mini-livre-la-horde-agile/) lui avait donné
quelques idées. Comme moi il trouve le contexte préhistorique propre à
l'imagination et à la projection. De là à penser faire un jeu ensemble
le pas a été rapide à franchir. Il m'a présenté Mathieu, le codeur du
trio, spécialiste *es* animation & jeux.

## Pour les formateurs et les entreprises

![We play agile](/images/2015/04/ageofagile-mammouth-400.jpg)

L'idée est de faire d'abord un jeu assez simple (1h de jeu) sur les
principaux principes agiles, sa culture, et les bases de la méthode
Scrum, au travers de la survie d'une horde préhistorique. Une heure en
ligne (tablette, browser, etc.). Qui permettront à chacun de s'y
frotter, et notamment les formateurs de valider les acquis d'une
formation en clôturant celle-ci par *age of agile*.

On pense naturellement à la suite si nous réussissons notre premier
défi, un MOACESG\[1\] (**massive online agile culture enabler serious
game**), un jeu multijoueur d'entreprise pour diffuser la culture agile,
suivra.

## *Early adopters* et investisseurs

![We play agile](/images/2015/04/ageofagile-snow-400.jpg)

Bon pour l'instant, on démarre, on prépare (c'est en cours) un *lean
canvas*, un scénario, l'articulation du jeu, les scénarios
d'utilisation, etc. On va placer ces informations sur
[weplayagile.com](http://weplayagile.com).

Bien évidemment nous cherchons des *early adopters*, certains se sont
manifestés via Twitter, je les remercie. Mais naturellement nous
recherchons surtout des **investisseurs** (une petite somme pour se
mettre le [pied à l'étrier](https://www.youtube.com/watch?v=A3vlj8mUiPA)).

## Signes d'intérêts

Quoiqu'il en soit tous les signes d'intérêts, *feedback*, etc. sont les
bienvenus. 

\[1\] je n'ai pas encore déposé l'acronyme, je vous le laisse.
