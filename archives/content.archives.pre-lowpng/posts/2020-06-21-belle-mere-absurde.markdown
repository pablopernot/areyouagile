﻿---
date: 2020-06-21
slug: comment-se-sortir-des-pieges-d-une-belle-mere-et-autres-absurdites
tags: [absurde, situation, non-sens, rire, decalage, meta]
title: Comment se sortir des pièges d'une belle-mère et autres absurdités 
---

Durant le confinement on a eu peur pour beaucoup de couples. Mais nous avons été aveugles. C'est lors du déconfinement, au dégel, que les vrais dangers émergeaient. En témoigne ce coup de téléphone : "j'ai retrouvé ma belle-mère, elle me fait tourner en bourrique, comment faire pour m'en sortir ???". 

Mon premier conseil : la déstabiliser. Sans violence, sans agressivité, sans bienveillance ni empathie. Juste la déstabiliser. Pour cela : créer une distance inattendue avec ce qui aurait pu se produire "normalement". 

## Le rire, agent du décalage

Il se trouve qu'à la fin des années 80 et aux débuts des années 90 j'ai pu écrire un mémoire sur le rire, le non-sens et l'absurde chez les comiques cinématographiques, dans le burlesque (que vous trouverez dans la page [à propos](/a-propos/)). 

Chacune des théories du rire s'appuie sur une idée de décalage : 

* Historiquement les Grecs anciens, Hobbes, Bain, voient dans le rire un décalage entre l'avantage et la faiblesse : quelqu'un chute sur une peau de banane. Cela est appelé les "théories de la dégradation" (les coups de pied aux fesses du slapstick américain : Chaplin, Keaton, Mack Sennett, etc.).  
* Puis viennent les théories du contraste (Schopenhauer, Kant) : le rire viendrait du contraste entre l'attendu et ce qui survient. Généralement entre quelque chose de très digne qui se retrouve ridicule. La grande-duchesse qui glisse sur la peau de banane encore, ou encore, le bourgeois qui se fait botter les fesses par le *tramp*, le vagabond.  
* Enfin les théories plus modernes (ceci n'est pas une liste exhaustive) de Freud ou Bergson. Je m'attache particulièrement à celle de Freud qui explique que le rire naît d'une énergie prévue pour quelque chose et qui finalement ne se résoud pas : ne sachant quoi en faire elle devient du rire. Je vois cette personne marcher puis soudainement glisser sur une peau de banane. L'énergie que j'avais mis de côté pour suivre son mouvement normal (pensez à la théorie des [neurones miroirs](https://fr.wikipedia.org/wiki/Neurone_miroir)), je ne peux plus la dépenser normalement elle se redirige en rire. 

Partout un écart, un décalage, une énergie qui ne sait plus où aller, et qui doit **s'utiliser à autre chose**, d'où la déstabilisation.    

## De l'utilisation de l'absurde et du non-sens

{{< figure src="/images/2020/06/marxbrothers.jpg" title="A day at the race, the Marx Brothers" class="center" >}}


Pour créer ce décalage, l'absurde est un moyen efficace. On va appeler absurde[^dea] toute chose qui entre en conflit avec le sens, c'est-à-dire où le sens est tronqué, berné, manquant, voire absent. On parlera d'absurde idiot : défaut de raisonnement, ou sauvage : en conflit avec le raisonnement. Le non-sens est une forme dérivée de l'absurde : on parle soit du non-sens qui possède un sens propre mais qui n'a pas de sens dans le contexte, on dira non-sens hors contexte, soit du non-sens qui fait semblant d'avoir du sens, mais qui n'a véritablement pas de sens (on ne parlera pas du non-sens qui n'a pas de sens et qui ne cherche pas à faire croire qu'il en a un). 

Quelques exemples en provenance des comiques cinématographiques, puis nous nous attaquerons à notre belle-mère, et enfin à l'organisation.  

* Absurde "idiot" : "Hardy, en retard à son mariage, réçoit un coup de téléphone de son futur beau-père. Conscient de sa faute il demande a Laurel de répondre qu'il est parti depuis dix minutes. Laurel s'exécute et annonce sans embarras, avec une logique d'acier ou une bêtise phénoménale (on ne sait vraiment) : "Mr Hardy est à côté de moi, il m'a dit de vous dire que nous venions de partir depuis dix minutes."

* Absurde "sauvage" : Groucho est le patron d'un hôtel dans lequel Harpo et Chico viennent d'entrer. Alors que tous trois allaient se serrer les mains, ils se lancent dans une sorte de farandole... un début de rixe éclate, mais il tourne a la congratulation réciproque, lorsqu'un coup de klaxon soudain de Harpo met tout le monde en fuite. Ce dernier en profite pour manger les boutons du veston d'un groom, accrocher sa jambe au bras du groom, et il commence, au moment de s'inscrire sur le registre, à jouer aux fléchettes avec les stylos. Enfin, il se tourne vers le courrier qu'il déchire méthodiquement avant de se mettre a manger et boire encre, tampon, fleurs, téléphone, etc.

* Non-sens hors contexte : Les Nuls dans "la cité de la peur", une poursuite effrénée dans les rues de Cannes. Soudain une scène anodine dans une épicerie de Vera Cruz. La poursuite dans Cannes reprend. Ce moment à Vera Cruz a du sens en tant que tel, mais il n'en a aucun dans le film.  

* Non-sens qui simule le sens, mais qui n'en a pas, un *trompe sens* : C'est le plus dur à décrire. Dans *A day at the race* des Marx Brothers, Harpo est un faux malade. Groucho lui prend le pouls. Il prononce la phrase : "Ou bien cet homme est mort, ou bien ma montre est arrêtée". C'est du non-sens. On sait que Harpo n'est pas mort. On imagine un lien entre le pouls et la montre. La phrase pourrait presque faire sens, elle nous piège, on tombe dans le vide.  

### Comment utiliser cela avec les pièges de votre belle-mère

Imaginons que vous mettiez les chaussures de votre enfant. Les enfants grandissent, les pieds sont serrés. Vous parlez de la taille de leurs chaussures. La belle-mère acariâtre vous reprend : "on parle de pointure quand il s'agit de taille de chaussures, très cher". Quelques réponses que je vous propose : 

* Absurde idiot : "Est-ce que l'on ne dit pas taille pour le pied gauche et pointure pour le pied droit ?". 
* Absurde sauvage : Prendre les chaussures et les jeter au fond du jardin avec un grand rire. Crier : "le premier qui rattrape la pointure". Allez chercher la chaussure. Reprendre une vie normale.  
* Non-sens hors contexte : "Oui je sais j'ai dit cela la dernière fois à mon coiffeur". 
* Non-sens trompe sens : "J'espère que la taille de l'autre pied est de la même pointure que celui-ci".  

Naturellement **tout est dans la tonalité employée, l'expression corporelle**. Il est très important de vouloir **en rire**, de ne rien prendre au sérieux. La belle-mère ne sera pas dupe que vous êtes en train de jouer, pour ne pas rediriger cette énergie en décalage (dont nous allons reparler) vers de l'agressivité il faut proposer un terrain de jeu ludique. Donc à faire avec gaieté.  

## Les bienfaits du décalage

"Sans déviation de la norme, le progrès n'est pas possible" disait Frank Zappa (expert absurde). Ce décalage, pour les comiques cinématographiques il sert à déclencher le rire. Pour nous ? Il détourne le propos (de la belle-mère) et propose une énergie qui ne sait pas se satisfaire. J'ai noté dans les échanges personnels que j'évoque qu'elle se traduisait par un moteur pour la réflexion. **Cette énergie sert à s'interroger**. La surprise pour à s'interroger sur qu'est-ce qui est en train de se passer ? **Qu'est-ce qui *se joue* ?** Elle place les acteurs en position méta : qu'est-ce que je veux ? Qu'est-ce que je fais ? Pourquoi je le fais ? Personne n'est dupe dans le jeu avec cette belle-mère, cela lève le voile, l'illusion disparaît. Nous pouvons jouer, mais personne ne peut dire qu'il ne le sait pas, on ne peut plus faire semblant. Des questions plus vraies, plus réelles, sont posées.  

## L'absurde et le non-sens, agents du changement dans le système

Naturellement ce jeu de l'absurde, du non-sens, du rire, je vous suggère de le jouer en organisation (pas en bande organisée, mais dans les entreprises, dans vos milieux professionnels). Et cela pour les mêmes raisons. Cette énergie nouvelle qui apparaît dans le système et qui n'est pas présente habituellement, que devient-elle ? Pareil, elle révèle les jeux de dupes ou les situations. Elle ramène aux vraies questions. Elle fait disparaître les illusions et les faux-semblants. 

Quelques exemples : 

* "Je veux que nous livrions ce produit au plus vite !". Réponse *trompe sens* : "D'accord nous le mettons en service dès cet après-midi !", alors que tout le monde sait qu'il n'est pas prêt. Ce décalage, cette perspective, amène à s'interroger et probablement à générer le bon dialogue : "au plus vite" c'est dès que cette fonctionnalité est réalisée ? Dès que quoi ? 
* "Comment transformer au plus vite l'intégralité de la structure en entreprise agile ?". "Pourquoi ne pas utiliser le pouvoir de la coercition ?" (*absurde idiot* : par définition une organisation agile tend vers l'auto-organisation, l'autonomie. La coercition est le sens opposé). Cela soulève la question : veut-on faire semblant ? Veut-on simuler un comportement ? Probablement pas, donc que veut-on réellement ?  
* "Est-ce que l'on peut estimer la charge liée à ce travail, le délai ?". Faisons de l'*absurde sauvage*. Dans certains cas j'ai déjà proposé à quelques personnes d'utiliser une grande bobonne transparente (à bonbons) remplie de papiers. À cette question la personne tire un papier au hasard de la bobonne et annonce la réponse "3 jours !". Ceci est pour mettre en évidence l'absurdité des estimations, on pousse la logique jusqu'au bout[^estimations]. Si personne ne veut de vrai dialogue, nous jouons le jeu.
* "Cette équipe est nulle". "Oui moi aussi je n'ai jamais su faire les boeufs bourguignons (*non-sens hors contexte*)." Cette réplique hors contexte a le mérite en créant probablement perplexité chez votre interlocuteur de lui indiquer que votre phrase en tant que telle n'a pas plus de sens que la sienne. Que veut-il dire par "nulle" ? Abordons la vraie question, le vrai sujet, en tous cas ne restons pas en surface.  

Encore une fois : **tout est dans la tonalité employée, l'expression corporelle**. Il est très important de vouloir **en rire**, de ne rien prendre au sérieux. Personne ne sera pas dupe que vous êtes en train de jouer, pour ne pas rediriger cette énergie en décalage vers de l'agressivité il faut proposer un terrain de jeu ludique. Donc à faire avec gaieté. En tous cas à manipuler avec beaucoup de précautions. Le plus important : vous faîtes cela en voulant le bien de l'organisation, et non pas la moquer véritablement. 

Pour répondre du tac au tac il faut de l'entrainement, pour avoir la bonne attitude il faut de l'entrainement, pour avoir de l'entrainement il faut essayer régulièrement.  

Merci aux Marx Brothers, à WC Fields, aux Monty Python, à Chaplin, Keaton, et tous les autres. 


[^dea]: [première partie DEA](/pdf/02-nonsens-absurde-premierepartie.pdf) page 11
[^estimations]:[Agile CFO : noestimates, beyond budgeting](/2017/05/agile-cfo-noestimates-beyond-budgeting/)