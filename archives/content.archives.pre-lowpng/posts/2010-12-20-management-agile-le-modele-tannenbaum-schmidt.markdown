---
date: 2010-12-20T00:00:00Z
slug: management-agile-le-modele-tannenbaum-schmidt
tags: ['equipe','agile']
title: 'Management agile : le modèle Tannenbaum & Schmidt'
---

En étoffant un de mes supports de cours je suis tombé par hasard (il
faut le dire) sur le "Modèle de délégation et de développement d'équipe"
de **Tannenbaum & Schmidt** chez [Business
Balls](http://www.businessballs.com/tannenbaum.htm) (nom évocateur au
demeurant).

![Tannen](/images/2010/12/tannen-300x143.jpg)

On fait souvent allusion lorsque l'on traite des questions d'équipes et
de management avec l'agile du modèle de
[Tuckman](http://en.wikipedia.org/wiki/Tuckman's_stages_of_group_development)
concernant l'évolution de l'équipe. En quelques mots trop courts :

**forming** : l'équipe se forme, elle se découvre

**storming** : l'équipe se structure avec douleur, compétition,
rivalité, etc.

**norming** : l'équipe s'est calibrée, elle sait comment travailler
ensemble pour atteindre un objectif commun. (Ce stade n'est pas
forcément atteint un jour...)

**performing** : La capacité de l'équipe est supérieure à la somme des
capacités des individus qui la constitue. L'équipe excelle. (Celui-là
non plus...)

Oui c'est vrai, on le vit tous les jours, et c'est très intéressant et
cela concerne le développement de l'équipe en elle-même. Ce  [modèle de
délégation et de développement d'équipe de Tannenbaum &
Schmidt](http://www.businessballs.com/tannenbaum.htm) que je découvre
aujourd'hui complète cette approche du développement d'une équipe agile
notamment dans sa relation -extrêmement importante-avec le management.
Qu'est ce que ce modèle nous dit ? Comme l'indique l'image en haut à
gauche : que plus l'équipe à d'autonomie et de liberté, et plus le
manager laisse l'équipe s'émanciper, plus celle-ci peut donner des
résultats remarquables (à évoquer aussi peut-être dans sa relation avec
le *product owner*).

**Tannenbaum & Schmidt** décrivent 7 stades (je vous invite à vous
reporter à l'article en lien
[ici](http://www.businessballs.com/tannenbaum.htm) pour avoir plus de
détails) :

**1.** Le manager décide et annonce la décision

**2.** Le manager décide et "vend" la décision au groupe

**3.** Le manager présente la décision avec des idées générales et
invite à la réflexion

**4.** Le manager suggère une décision et invite à la réflexion

**5.** Le manager présente la situation, écoute les suggestions et
décide

**6.** Le manager présente la situation, défini les contraintes et
demande à l'équipe de décider

**7.** Le manager permet à l'équipe d'identifier le
problème, développer les options, décider de la solution. Le manager est
informé des contraintes par l'équipe.

En alliant les deux on a vraiment une visibilité sur la cible des
équipes agiles. Je me permets un petit diagramme et vous souhaite bonne
continuation.

![Modèle](/images/2010/12/tanneubaum_schmidt_tuckman.png)

