﻿---
date: 2020-01-30
slug: plan-de-livraison
tags: [plan, livraison, release, planning]
title: Le plan de livraison
--- 

Il y a quelques jours j'ai réalisé une nouvelle rétrospective auprès de "benexters" chez leurs clients. C'est l'occasion d'avoir un regard neuf avec du recul (et de l'expérience) pour les appuyer dans leurs activités. Plusieurs sujets ressortent. Et notamment quelque chose qui me fait leur dire rapidement : pas de plan de livraison (*release plan*) ? Et bien non, pas de plan de livraison. C'est dommage et fréquent. Cet outil est hautement efficace pour communiquer, harmoniser les points de vues, faire apparaître les contradictions ou les besoins, faciliter les prises de décisions, etc. Bref c'est un élément central d'un accompagnement produit ou projet. Et il est trop souvent oublié. 

C'est simple, et très efficace et quelques petits détails me paraissent cruciaux. 

Je vous propose quelques exemples parlants (mais toujours imparfaits, "c'est la vraie vie") que je commenterais un peu. Le reste devrait couler de source. (Le contenu est flouté pour des raisons évidentes). 

### Exemple 1 

![Release Plan 1](/images/2020/01/release-plan-1.png)

Un plan sur 3 mois. Cela pourrait-être 6 mois voir 1 an ou 2 années (mais au fil de l'éloignement, les blocs deviendraient de plus en plus gros, car de plus en plus approximatifs). 

Un indicateur qui nous dit où nous nous situons. *Vous êtes ici*. Ce qui avant, à gauche, est fait, fini, idéalement en production. On observe une cadence validée, confirmée (sauf si vous dîtes que c'est fini pour découvrir que ce ne l'est pas, d'où l'intérêt aussi de la mise en production). Et, à droite, on se projette. Des estimations, idéalement basées sur ce qui se trouve à gauche (on reproduit une cadence).    

À droite ce qui est estimé ? a) les estimations sont toujours fausses donc b) le plan est constamment remis à jour (une partie à **gauche** qui montre **la cadence réelle**, à **droite** **la projection** : des fois c'est cohérent des fois c'est n'importe quoi pour faire plaisir à quelqu'un).

Dans les **cartouches**, le contenu de chaque itération. Là le titre est flouté, mais c'est quelque chose de court et de parlant pour tous les acteurs. Les cartouches représentent la capacité et elles sont de fait très importantes. Cela n'a rien à voir avec un *gantt*.  

Les triangles rouges ? Ici des évènements, dans ce contexte : Noël, le jour de l'an et le jour de l'an chinois, qui impactent les équipes. Cela devrait impacter le contenu des itérations, pas le cas ici, on doit se voiler la face. En tous cas il est important de montrer des moments clefs sur le plan de livraison. 

### Exemple 2

![Release Plan 2](/images/2020/01/release-plan-2.png)

Ici les cartouches grises (temps passé), celles de couleurs (projection). 

On a aussi catégorisé les histoires utilisateurs (*user stories*) ou fonctionnalités (*features*) par couleurs. Imaginez ce qui vous convient. Ici c'était le pari hippique d'une certaine couleur, le pari sportif d'une autre, enfin la relation client d'une autre.   

Encore les marqueurs au-dessus : date butoir pour la certification légale, date de la coupe d'Europe de football, mais on indique aussi avec l'étoile jaune à partir de quand on peut sortir le produit, quitte à ne pas faire les dernières cartouches. 

### Exemple 3

![Release Plan 3](/images/2020/01/release-plan-3.png)

Dans cet exemple je veux montrer l'inclusion d'une itération sur le *code legacy*, sur un autre sujet, soudainement, au milieu. Cela décale les cartouches ou le contenu des cartouches. La capacité ne change pas. D'où l'importance des cartouches, et non pas d'une approche en mille-feuille de type *gantt*. 

La verticalité pour la lecture me parait importante. 


### Exemple 4

![Release Plan 4](/images/2020/01/release-plan-4.png)

Cet exemple est factice. Il me permet de vous montrer comment vous pouvez agréger plusieurs plans de livraison en un pour voir les dépendances. 

### Exemple 5

![Release Plan 5](/images/2020/01/release-plan-5.png)

C'est une équipe qui travaillait sur cinq versions différentes de son produit en parallèle. Mais cela aurait pu aussi être cinq équipes qui travaillent en parallèle pour rejoindre l'exemple au-dessus.  

Avez-vous noté que cela s'étale sur deux ou trois ans ? 

### Exemple 6

![Release Plan 6](/images/2020/01/release-plan-6.png)

Un dernier exemple : projection à 4 mois, pas encore de passé, dommage ; des codes couleur. 

### Une suite logique (et royale)

Les plans de livraison sont les derniers éléments d'un parcours classique. Un parcours classique ? Toutes les options ne sont pas indispensable, mais par exemple : un *lean canvas* pour le besoin, puis un [*impact mapping*](/2017/02/cartographie-strategie-impact-mapping/) pour la stratégie, puis un [*user story mapping*](/2017/01/cartographie-plan-action/) pour la tactique de réalisation, et peut-être un [*extreme quotation*](/2017/09/estimation-visuelle/) pour les "estimations".  

### Qui fait et quand ? 

Le responsable du produit, du projet, en est responsable (il peut déléguer, je suppose, il se débrouille). 

La mise à jour est régulière : à chaque itération. Cela ne veut pas dire de gros bouleversements, juste une mise à jour si besoin. 

### Conclusion

On le partage à tous les acteurs. Les plans de livraison ainsi réalisés peuvent stresser les décideurs qui comprennent très bien (ils ne sont jamais idiots) que si ils ajoutent quelque chose le reste se décale. Mais c'est une vision honnête de l'état des lieux. Ils perçoivent la variabilité de leurs demandes si elle existe. Tout le monde perçoit la cadence, car elle apparaît aux yeux de tous. Le plan de livraison est donc un élément hautement politique dans le bon sens du terme, qui concerne le citoyen de la cité, les acteurs ou les parties prenantes du produit, du projet. 

### Complément suite à la publication

J'entends : "on se synchronise avec 17 équipes (...) même si c'est utile au bout d'une semaine cela devient erroné". Attention, mon expérience me fait penser qu'au delà de 5 ou 6 équipes la synchronisation sera difficile, très difficile. La synchronisation c'est garder une simultanéité. Vous pouvez faire un tableau pour plus d'équipes, mais là oui je suis d'accord c'est très éphémère, il faut s'interroger pour savoir si le jeu en vaut la chandelle. Et ce n'est plus un plan de livraison qui est bien maintenu constamment et donc toujours à jour. Vous pouvez agréger 5 ou 6 plans de livraison maximum pour que cela reste un élément vivant de décision et de suivi. Alors pas de "grands projets" a plus de 5 ou 6 équipes ? Si naturellement, mais vous devez travailler votre découpage organisationnel : des groupes de 5 à 6 équipes maximum, qui eux-mêmes s'agrègent par grappe de 5 ou 6 groupes maximum, etc. Les gros tableaux qui agrégent tout sont bien beaux mais inutiles, car pas compréhensible, trop dense, non maintenable, etc. Le plan de livraison c'est un outil de suivi et de décision. C'est de cela dont nous avons besoin.      