---
date: 2022-06-17
slug: livres-sur-le-produit
tags: [produit, product, management, lecture, livre]
title: "Lectures sur le product management"
draft: false
---

Je suis en ce moment assez focalisé sur le produit responsable, et je prépare quelques réflexions sur le sujet. Entre temps les personnes de la newsletter produit de la [School of product](https://schoolofpo.com), et notamment la puissante [Marion Lecerf](https://www.linkedin.com/in/marionlecerf/), m'ont demandé une petite liste de lecture sur le produit. 

Vous pouvez la lire (en cliquant sur le lien ci-dessous)

{{< lowimage src="/images/2022/06/newsletter-low.png" title="Newsletter spéciale livres" >}}
[Newsletter spéciale livres](https://communication.octo.com/nl20-sp%C3%A9ciale-livres-%C3%A0-emmener-en-week-ends-prolong%C3%A9s?hs_preview=LHNfHJQH-73499781264)


Le lien vers la newsletter complète : [Newsletter School of product #20 spéciale livres](https://communication.octo.com/nl20-sp%C3%A9ciale-livres-%C3%A0-emmener-en-week-ends-prolong%C3%A9s?hs_preview=LHNfHJQH-73499781264) 


{{< figure src="/images/2022/06/frechin.jpg" alt="le design des choses de Frechin" class="left" width="200">}}
Quant au livre de **Frechin, Le design des choses**,il est très intéressant, notamment je pense, car nous sommes depuis quelque temps au moment où la branche historique du design, celle des architectes, des artistes, des ingénieurs rejoint celle des entrepreneurs sauvages du monde des startups. Nous sommes au moment où le sens historique de design -> dessein, intention, sens, revient (pour de bonnes raisons, on touche à la fin d'une époque, voire d'un monde). Toute la partie qui met cette réflexion en exergue au travers d'une histoire du design est intéressante. La deuxième partie me paraît moins percutante d'une part, car la critique justifiée de la boulimie d'itérations modernes est exagérée et donc perd de sa substance, et d'autre part, car Frechin ne possède pas la partie "entrepreneur sauvage", ça lui manque, ce n'est pas sa nature, il ne comprend pas une partie de ce qu'il évoque sur le numérique. 

Mon conseil mélangez : From zero to one de Peter Thiel avec Le design des choses de Jean-Louis Frechin. 


