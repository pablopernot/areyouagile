---
date: 2013-03-25T00:00:00Z
slug: le-jeu-scrum-des-balles-et-des-points
tags: ['atelier','workshop','seriousgame','cjd']
title: Le jeu scrum des balles et des points
---

...plus connu sous le nom de "Scrum ball point game" de [Boris Gogler](http://borisgloger.com/2008/03/15/the-scrum-ball-point-game/). 
Ce jeu innovant propose un bel apprentissage du vécu d'une équipe scrum.
Je l'utilise depuis quelques temps assez régulièrement (en formation ou
lors d'un [projet dont vous êtes le héros agile](/heros/)). Et j'ai pu vendredi dernier en
participant à une rencontre du [Centre des Jeunes Dirigeants (CJD)](http://www.jeunesdirigeants.fr/) le proposer à 300 personnes...

Toute une histoire qui me donne l'occasion de revenir sur les leçons de
ce jeu. *Merci donc à Boris Gogler pour ce jeu. Merci aussi à lui de ne
pas sombrer dans ce minable mercantilisme des innovations games où
maintenant pour dessiner un bateau il faudrait soi-disant une
certification*.

## Objectif du jeu

Faire transiter le plus de balles dans un système dans un temps imparti.
Le système c'est votre équipe : donc il faut faire passer le plus de
balles au sein de l'équipe. Le temps imparti avoisine habituellement
2mn. A chaque tentative l'équipe annonce son estimation : le nombre de
balles qu'elle pense pouvoir faire transiter dans le système (le groupe
!) durant les 2 mn. A la fin de chaque tentative on s'octroie un petit
moment (1mn, 1mn30 selon les cas) pour s'interroger : comment pouvons
nous faire mieux ? comment pouvons-nous nous améliorer ? On réalise
généralement 5 itérations qui contiennent donc : estimation,
réalisation, introspection.

## Contraintes

Oui, c'est la vie, il y a des contraintes.

-   Vous êtes une équipe.
-   Entre chaque équipier les balles prennent l'air. (C'est à dire que
    l'on ne peut pas se passer la balle de main en main sans qu'à un
    moment la balle soit seule dans l'air).
-   Pas de passe à votre voisin direct. (Pas de passe à votre voisin le
    plus proche c'est simple non ?).
-   Le point de départ est le point d'arrivée (La personne qui plonge la
    main dans le sac pour attraper une balle est aussi celle qui place
    les balles qui ont parcouru le système dans un autre sac).
-   Tous les membres de l'équipe doivent toucher la balle une seule fois
    sauf le point de départ qui est aussi le point d'arrivée. (Et une
    seule seulement ! ce qui ne veut pas dire une seule à la fois...).
-   Une balle qui tombe, touche le sol, ou qui ne respecte pas ces
    règles est perdue.
-   Itération = 2mn, introspection = 1mn30 & estimation = 30s (voir
    moins, c'est plutôt un *buffer*).
-   5 itérations

## Quelques consignes aux facilitateurs

**Attention la suite va dévoiler quelques aspects du jeu qu'un futur
joueur ne devrait pas connaître.**

Vous pouvez vous servir de ces [consignes](/pdf/consignes-1-1.pdf) que
j'ai réalisé pour les facilitateurs qui m'accompagnent lors de gros
évènement (voir plus bas...).

-   Essayez d'être entre 12 (min) et 20 joueurs (max) par équipe, et
    d'avoir entre 40 et 80 balles par équipe (des balles différentes
    sont un plus : tennis, ping pong, molles, grosses, etc.)
-   Surtout ne commencez pas en disant aux joueurs : "mettez vous en
    rond" car vous induisez déjà ainsi très fortement un système.
    Proposez leur plutôt : "merci de vous rassembler par ici".
-   A partir de la troisième ou quatrième itérations introduisez de
    l'imprévu (demandez en tant que client une série de balles
    spécifiques, voir les consignes en pdf ci-deçu).

## Les questions du debrief

À la fin du jeu naturellement on debrief.

-   Que s'est-il passé ?
-   Quelle a été votre meilleure itération ?
-   L'amélioration est venue de quoi? On a travaillé plus dur ? plus
    vite ?
-   L'auto-organisation de l'équipe a-t-elle bien fonctionnée ?
-   D'où est venule "leadership" ? avec quel style ?
-   Application chez vous ?

Autres thèmes : Vélocité naturelle, inspection & adaptation
(amélioration continue), théorie des contraintes, sécuriser l'échec,
éliminer le gaspillage, "pull & push", protection de l'itération, héros
?, équipe & leadership, planification & estimation, etc.

## Les apprentissages et les observations

Que peut-on retenir de ce jeu ? Plein de choses, et chaque partie a ses
propres conclusions, mais voici les principaux points qui émergent
régulièrement :

-   On parle de système : on observera une **performance dépendante du
    système et non "des individualités"**, ou d'une volonté d'aller plus
    vite, plus fort. C'est en changeant le système que la performance
    change. On peut aller plus vite ou plus fort, on peut devenir expert
    d'un système mais celui-ci a des limites assez rapidement.

![CJD](/images/2013/03/cjd-03.png)

-   Ainsi le **"héros" n'a qu'un impact limité**. C'est plutôt au
    maillon faible qu'il faut faire attention. En rapport avec la
    [théorie des contraintes de
    Goldratt](http://en.wikipedia.org/wiki/Theory_of_Constraints) :
    cherchez à d'abord régler le plus gros problème du système, pas tous
    à la fois car les autres problèmes se remodèleront suite à la
    résolution du principal.
-   Les **estimations initiales sont souvent catastrophiques** (dans un
    sens comme dans l'autre : trop bas, trop haut). Surtout la première
    (qui me permet de replacer ma citation fétiche : "aucun plan ne
    survit au premier contact avec l'ennemi" --Von Moltke). Mais peu à
    peu la vélocité, la performance liée au système mis en place est
    rendue visible. Comme les équipes réussissent à améliorer ce système
    on observe ainsi facilement des scores en 5 itérations allant de 0 à
    la première itération à 60 à la cinquième. Bel écart qu'il faudra
    noter. Quand on observera une performance qui se stabilisera on
    pourra planifier (normalement cela oscille autour de 3 itérations si
    le système ne change pas trop).
-   Ces améliorations viennent de la pratique et d'une **phase
    d'introspection** vouée à l'amélioration. Cette amélioration est
    possible car on opère par cycle court : 2mn, 5 fois. Et que
    l'erreur, nécessaire à l'expérimentation elle même nécessaire à
    l'amélioration, est d'autant plus acceptée que le **cycle est
    court** car ainsi l'**échec n'est pas un drame**.
-   On observera comment cette introspection et ces axes d'amélioration
    sont discutés au sein de l'équipe (leadership naturel, accord
    partagé de type ["decider" de Jim
    McCarthy](http://www.mccarthyshow.com/online/), etc.) : c'est
    généralement une bonne mise en pratique de l'**auto-organisation**
    (vraiment pas si dur) et d'où émerge une **intelligence
    collective**.
-   Si les équipes sont de grandes tailles (autour de 15 ou 20 max) on
    découvre que c'est plus difficile de communiquer et de s'améliorer.
    Il faut donc **privilégier les équipes de petites tailles**.
-   Vous observerez malheureusement la **résistance des gens à vouloir
    changer un système** ("on pourrait se rapprocher..."), vous
    observerez la crainte à transgresser des règles qui n'existent pas
    ("on peut lancer plusieurs balles dans la système à la fois ???"), à
    penser différemment ("j'ai le droit d'accrocher ce sac à mon cou
    ?"), etc. C'est la **mémoire du muscle**...
-   Vous observerez pourquoi le management aime tant Scrum : cette
    capacité à se mettre à fond, en sprint, dans la réalisation, et
    **vous interrogerez vos équipes sur un rythme soutenable**. C'est
    généralement rarement le cas spontanément.
-   On critique parfois ce jeu en disant : "c'est le bazard, ce n'est
    pas efficace". Habituellement c'est une illusion de bazard : ça
    crie, ça bouge, bref **ça vit**. on voit les systèmes s'améliorer,
    les performances augmentées. Mais bon, vous pouvez entendre cette
    critique.
-   On critique parfois ce jeu en disant : "Mais c'est du Taylorisme".
    Avec les grosses équipes la majorité des membres de l'équipe sont
    assez passifs, c'est vrai. Sinon c'est une cacophonie, ou sinon la
    mise en oeuvre est trop improbable (tout le monde ferait tout à la
    fois). Mais il faut rappeler aux gens que personne ne leur impose
    cette passivité, qu'à tout moment **ils peuvent prendre
    l'initiative**, c'est fondamental.
-   Chassez, interrogez les scores incohérents, aux fluctuations trop
    inattendues. Amusez vous à comparer estimations et résultats...

![CJD](/images/2013/03/cjd-05.png)

-   On pourrait même faire une lecture [cynefin](http://cognitive-edge.com/) de ce jeu : première
    itération, on découvre la dure réalité des balles dans le système.
    On est dans le réactif, imprédictible, il faut là du "command &
    control" ! Quelqu'un prend la main et déclame : "on forme un rond
    !". L'orage de la première itération passé on fera appel aux
    "experts" : c'est un système prédictible et compliqué qui apparait :
    cette forme sera efficace, ou celle-ci. Attention de ne pas vous
    faire manger par les experts qui ne s'accorderont peut-être jamais,
    ou ne penseront pas "out of the box". Enfin quand on introduit de
    l'imprévu ou si votre système a atteint sa limite c'est le moment
    des pratiques émergentes (complexe, imprédictible) : si on essayait
    cela ? etc etc.

## Antithèse

L'antithèse de ce jeu aurait été de se lancer dans 10 ou 15 mn de
réalisation (5 itérations de 2 minutes regroupées en 1, avec ou sans la
phase d'introspection) basé sur un plan définitif et figé pour la mise
en oeuvre et mené par un chef suprême. Le premier tour aurait été
peut-être satisfaisant (si on a pas perdu trop de temps à préparer le
plan...). Mais rapidement on aurait :

-   Soit eu une stagnation de la performance du au système, et le
    système étant figé, c'était la fin, une fossilisation.
-   Soit une dégradation du à la schlérose du système, car tous les
    acteurs du système étant passifs car soumis, nul doute qu'au moindre
    grain de sable personne n'aurait eu le désir ni même le droit
    d'utiliser son intelligence pour y pallier...
-   Encore eut-il fallu que la plan soit bon ce qui est loin d'être sûr
    (encore une fois : "aucun plan ne survit au premier contact avec
    l'ennemi" --Von Moltke)

Vous voyez la métaphore ?

![CJD](/images/2013/03/cjd_ballpointgame1.png)

## 300 (pas le film), les jeunes dirigeants !

Donc vendredi dernier (22 mars 2013). J'ai eu le plaisir de faire un
"scrum ball point game" avec \~300 participants, et donc 15 équipes de
20 personnes et 500 balles. Oui cela a été extérieurement le bazard,
mais uniquement extérieurement car les équipes n'ont cessées de
s'améliorer. (Naturellement dans l'action je n'ai fait aucune bonne
photo...). Merci à tous les facilitateurs, à Jérôme et au [Centre des
Jeunes Dirigeants (CJD)](http://www.jeunesdirigeants.fr/) d'avoir permis
cela à l'occasion de leur rencontre autour du dirigeant agile (et qui
fera l'objet très rapidement d'un autre billet de blog, car observer 300
dirigeants parler agilité cela vaut le déplacement).

![CJD](/images/2013/03/cjd_ballpointgame2.png)

les scores ont été jusqu'à 50 (ils ont ramassés des balles par terre
d'autres équipes j'imagine). Le CJD avait affreté un charter de 500
balles aux couleurs de son logo.

![CJD](/images/2013/03/cjd_ballpointgame3.png)

Et là ça debrief.

![CJD](/images/2013/03/cjd_ballpointgame4.png)

## Quelques caricatures

Le CJD a aussi eu la très bonne idée de faire venir [Loïc
Schvartz](http://preenbulles.bedee.free.fr/index.php?option=com_content&view=article&id=60),
un dessinateur rennais qui collabore régulièrement avec Charlie hebdo,
Marianne et Ouest France, pour croquer les conférences et les
ateliers... Voici les résultats de ce premier "scrum ball point game"...

Les trois dernières sont des évocations du petit speech que j'ai réalisé
derrière en m'appuyant sur ces
[slides](https://speakerdeck.com/pablopernot/agilite-pour-les-entrepreneurs).

**Cliquez dessus pour une meilleure résolution.**

![CJD](/images/2013/03/cjd-02.png)

![CJD](/images/2013/03/cjd-03.png)

![CJD](/images/2013/03/cjd-04.png)

![CJD](/images/2013/03/cjd-05.png)

![CJD](/images/2013/03/cjd-06.png)

![CJD](/images/2013/03/cjd-07.png)
