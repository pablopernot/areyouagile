---
date: 2009-11-06T00:00:00Z
slug: check-cmmi-et-projet-scrum
tags: ['scrum','cmmi']
title: Check CMMi et projet scrum
---

Je viens de passer un "check CMMi" avec pour sujet un projet Scrum dont
je suis le *ScrumMaster*. On lit beaucoup de choses sur les liaisons ou
équivalences en CMMi et Scrum, par exemple dans une annexe du bouquin de
Ken Schwaber ("Agile Project Management"). Notamment sur les pratiques
couvertes par Scrum concernant CMMI. Je reste assez dubitatif sur cette
volonté de rapprocher ces deux méthodes car je perçois pour l'instant
que ce n'est fait que pour rassurer certains clients, ou pour bâtir un
argumentaire commercial. Actuellement -dans la société pour laquelle je
travaille encore durant deux mois- c'est assez caricatural : les
scrumers moquent les cmmis et vice-verça. Je joue moi aussi beaucoup à
cela. C'est de la détente, hein. Mais je suppose qu'une vraie analyse
des pratiques CMMi (telles qu'elles sont mises en oeuvre ici) via le
prisme de Scrum apporterait une vraie (contre)analyse et permettrait de
faire évoluer le modèle. Et là aussi vice-verça.

Bref en tous cas je viens de passer un "check" : on vérifie
l'application des pratiques CMMi sur vos projets au sein de l'"agence".
Parmi les points forts sont remontés le projet Scrum, notamment sur la
pratique PMC (en gros gestion de projet) : visibilité et implication des
équipes (le radiateur, les stand-up meeting, etc.), mais aussi sur la
pratique VER (vérification) : test d'intégration et d'acceptation par le
client à chaque itération. Bref : L'auditeur CMMi est  probablement
moins borné que moi et a observé avec intérêt les pratiques Scrum !
