---
date: 2011-03-18T00:00:00Z
slug: lagile-pourrait-il-redonner-a-la-regie-ses-lettres-de-noblesse
tags: ['contrat','agile']
title: L'agile pourrait-il redonner à la régie ses lettres de noblesse ?
---

Cher client, cher acheteur,

Si je t'écris cette lettre c'est pour te souhaiter de réussir tes
projets.

Naturellement il ne t'aura pas échappé que réussir un projet "au
forfait" est une entreprise incertaine, malgré le contrat qui te lie à
ton prestataire. Je me doute que si tu es mature tu as déjà compris que
les projets importants, les projets stratégiques, tu ne vas pas les
confier à une société externe. Ce "forfait" qui va partir avec tes
demandes et revenir quelques temps plus tard avec un résultat plus
qu'incertain ? Un "forfait" dont tu ne connais pas les intervenants,
peut-être même que tu ne connais ni leurs visages, ni leurs noms, à ces
gens qui vont travailler sur ton projet. Même si il y a de nombreux
projets au forfait qui fonctionnent bien, les projets importants,
stratégiques, tu les soignes, tu ne les confies pas à des inconnus. Tu
souhaites être proche de leur réalisation.

Le forfait est historiquement [un crime commis avec audace...](http://fr.wiktionary.org/wiki/forfait)

Bref tu as la sage intention de réaliser tes projets stratégiques à la
maison, prêt de toi, avec des gens que tu connais, en un mot : en régie.

La régie est historiquement une [administration de bien](http://fr.wiktionary.org/wiki/r%C3%A9gie) (comprendre de valeurs
?)

Mais la régie t'inquiète. Tu associes cela à une sorte de mission sans
fin, sans but, à un bataillon de prestataires qui viennent épauler ton
"middle-management" (Est-ce la régie qui l'a rendue apathique ?, ou lui
en étant apathique qui a déclenchée la régie ? Est-il réellement
apathique, ou le contraints-tu à ne pas pouvoir être dynamique ? Cela
pourrait être une autre question). Et parfois tu n'as pas tort.

L'agile pourrait-il redonner à la régie ses lettres de noblesse ?

L'agile va te permettre en axant son action sur la production de valeur
à intervalles réguliers et rythmés de te donner rapidement de la
visibilité, des certitudes, des acquis. L'agile va te proposer une
cadence claire qui te donnera une réelle prédictibilité sur la suite de
ta réalisation. Si le projet va dans le mur ? Eh bien tu vas t'en rendre
compte rapidement et économiser les longs mois inutiles qui auraient
servis à masquer cet état de fait.

-   Oui mais avec la régie, me dis-tu, le projet ne finira jamais.
-   Au contraire, en produisant ce qui a le plus de valeur au début, tu
    vas pouvoir arrêter celui-ci plus tôt.
-   Alors tu me dis que tu ne sais pas ce que cela va te coûter ! (si tu
    n'as pas de budget défini)
-   Et tu as raison ! Mais le contrat agile devrait te permettre d'avoir
    le mieux possible pour le budget investi, le meilleur rapport
    valeur/prix.
-   Avec le forfait j'ai des certitudes !
-   Hum... est-ce vraiment le cas ?

*PS : autant sur la forme que sur le fond Yassine me balance que je
suis à côté de la plaque sur ce coup.! gasp, il va me falloir
m'interroger encore.*
