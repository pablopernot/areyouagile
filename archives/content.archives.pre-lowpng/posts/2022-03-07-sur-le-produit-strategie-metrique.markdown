---
date: 2022-04-04
slug: sur-le-produit-strategie
tags: [produit, product, management, strategie, vision, resultat, objectif]
title: "Sur le produit : la stratégie"
draft: false
---

Notre conversation [sur le produit](/2022/02/sur-le-produit/) continue. 
"Il manque pour moi deux choses essentielles", me glisse mon interlocutrice. "En fait, c’est pour moi la frontière entre product « ownering » et product management :
- La stratégie produit : pour moi la référence c’est John Cutler avec par exemple le [framework the North Star](https://amplitude.com/north-star/about-the-north-star-framework), t’as peut-être d’autres références ?
- Le marketing et l’analytics, les indicateurs clés". 

Bien, comment démarrer ? Ce ne sont pas des questions faciles que tu poses. 

### Sur la stratégie

Souvent, très souvent, très très très souvent, le premier pas, mais il est éminemment important, c'est de demander au *product owner*, ou au *product manager*, ou au *head of product*, qu'est-ce qui te fait dire dans les trois ou six mois au sujet de leur produit que ce qu'ils mènent sera un succès ? Que cela aura eu de l'impact ? Ou encore, d'une façon *solution focus* : nous sommes dans six mois et ils ont le sourire concernant l'impact de leur produit, pourquoi ? Qu'est-ce qui provoque ce sourire ? 

Cette approche, cette question, qui semble anodine, est pourtant renversante au sens propre. Quel est le point d'arrivée désiré et non plus le point de départ. On évite ainsi une foultitude de solutions préconstruites basées sur leurs habitudes, ou leurs croyances, ou sur la politique interne (et qui donc poursuivent un autre objectif), et on ramène le sujet à l'essentiel, l'impact. 

Impact dans le sens étymologique de rupture : qui a provoqué un changement.   

Cette question pose souvent beaucoup de problèmes aux personnes qui doivent y répondre, notamment par manque d'habitude, ou par crainte. 

D'une part la réponse n'est valide que si la réponse est circonscrite à un critère de succès, et ne prend pas la forme d'une liste à la Prévert. Car quand on donne une liste sans fin de critères de succès (pour une période de trois mois à six mois) c'est qu'on ne sait pas décrire le succès attendu. Imagine aussi 10 critères de succès ? Comment répondre à des questions clefs dans ton cheminement ? Impossible ou trop long si tant de critères sont en jeu. Et si une liste doit malgré tout être fournie il faut impérativement la prioriser, et ainsi vous ne prenez en compte que le premier point pour les mois à venir (les autres suivront).  

D'autre part on attend de véritables critères de succès concrets (et donc mesurables) concernant les produits, et non pas : "nous aurons délivré en temps et en heure" qui ne parle absolument pas de l'impact de leur produit, ou encore "nous aurons délivré les 20 nouvelles fonctionnalités" qui là encore ne dit rien de l'impact de leur produit, sauf qu'il a 20 fonctionnalités en plus et rien ne dit qu'elles soient utiles. **La mesure doit être externe au domaine de maitrise** des *product owner* et *product manager*, elle doit être piochée dans un domaine qu'ils ne maitrisent pas et pour lequel ils souhaitent avoir un impact. Par exemple : "nous augmentons de 3% le taux de transformation d'achat", ou "les jeunes de moins de 16 ans se mettent à utiliser notre produit", ou encore : "grâce à la nouvelle version l'usager gagne 1h de manipulations par semaine", etc. 

Je reviens sur la question de "concret" : la cible doit être concrète même si c'est illusoire. Bizarre ? Il s'agit de pouvoir articuler des conversations, prendre des décisions. On ne parle pas "d'avoir un meilleur taux de transformation", mais "d'avoir un taux de transformation qui augmente de 10%". De 10% ou de 30% ? vous voyez déjà selon les chiffres que les conversations seront différentes. Si c'est 10% ou 30%, ce n'est pas le même écart, et donc pas la même stratégie. Ainsi plutôt que "les jeunes de moins de 16 ans se mettent à utiliser notre produit", "au moins 1000 jeunes de moins de 16 ans se mettent à utiliser notre produit dans les 3 mois" aurait été mieux. 

La stratégie porte un but avec du sens. Avoir une nouvelle fonctionnalité n'a pas de sens. On souhaite une nouvelle fonctionnalité dans le but de ... Une stratégie sera une façon mettre en musique, de faire une projection hypothétique pour aboutir à ce but. 

On va réduire cette demande de succès à un seul élément pour faciliter systématiquement la réalisation. Les organisations peuvent espérer plein de critères succès, mais il en faut un seul pour répondre aux questions, prendre des décisions, faire des choix, et ... tracer une stratégie. C'est la *North Star* dont tu parles par exemple. Sinon si tu gardes plus d'un élément tu mets en péril ton action : il n'y a pas de gouvernance sans choix. 

Nous sommes en train de parler d'une période de trois mois à six mois. Ne te méprends pas, au-delà on commence à parler de vision. Ainsi une vision sur quelques années pourrait se décliner en objectifs de trois à six mois, et qui eux-mêmes individuellement vont se décliner sous-objectifs. La stratégie c'est comment atteindre cet objectif et cette déclinaison en sous-objectifs. 

Car la stratégie est la façon de mettre en perspective les différentes articulations possibles, ou les différentes hypothèses pour essayer d'aboutir à un résultat.

La stratégie qui n'est ni une vision, ni une ambition[^mazery], mais une façon de mettre en perspective les différentes articulations pour essayer d'aboutir à un résultat. 

[^mazery]: À signaler un [article intéressant de Mazery](https://www.media.thiga.co/ce-quest-et-nest-pas-une-strategie-produit) duquel je reprends ce terme "ambition". Je pense qu'il résume assez bien les incompréhensions autour du terme stratégie.

Ce résultat étant lui-même une façon de rendre pragmatique une ambition, une vision.  

On se retrouve donc potentiellement avec une vision, qui se traduit par un objectif, et une stratégie basée sur des hypothèses, des chemins, pour aboutir au résultat. 

C'est un peu tout le cheminement opérer par des outils comme le *lean canvas* (par exemple, pour la vision, ou la résolution d'irritants), l'*impact mapping* pour les hypothèses et le cheminement de la stratégie, le *user story mapping* pour le découpage tactique de ce que la stratégie aura fait émerger. Les OKR[^kr] dont tout le monde parle en ce moment sont une déclinaison de ces principes : une arborescence d'objectifs qui se rattachent à un objectif primaire. Tous sont chiffrés pour permettre l'articulation des conversations (sur des éléments mesurables), l'accélération des décisions, et de rapidement savoir s'ils sont atteints ou pas.  

[^kr]: Sur les OKR ces livres blancs : [Piloter le produit par l'impact](https://publication.octo.com/fr/piloter-produit-impact-okr)

Encore une fois l'important dans le monde mouvant qui nous entoure est d'économiser (et cette économie entre aussi désormais dans une approche éco-responsable), de ne pas aller trop loin avant de valider ou d'apprendre. Ainsi la stratégie a pour but, pas uniquement d'avancer, d'indiquer un chemin potentiel, mais aussi de valider au plus vite (et régulièrement) nos hypothèses sur celui-ci. 

Depuis le début je ne fais pas trop de différence entre *product owner* et *product manager*, mais si tu veux, ce n'est pas une règle, et pas forcément ma philosophie, mais une sorte de convention fait dire que les *product owner* s'occupent de la tactique (comment on fait tout cela sur le terrain, la mise en oeuvre des sous-objectifs, des hypothèses), et les *product managers* de la stratégie (par quels chemins on imagine passer, l'ensemble de l'arbre, et ils nourrissent le pourquoi). 

Ainsi une stratégie est la mise en musique d'un désir (ambition, vision, irritant, etc.) de façon pragmatique : pragmatique étant qui sache rendre concrète la validité d'une mise en oeuvre, la validité de nos hypothèses. Qui propose une certaine façon de valider, de réaliser, un objectif. 

Bien souvent l'idée d'avoir une équipe auto-organisée qui va avancer vers un objectif clair, et un seul, concret comme vu plus haut, serait salvateur, et un pas en avant de géant pour tant d'organisations ! Mais trop souvent la politique a rendu ces objectifs illisibles : on n'est pas fichu de les prioriser, on n'est pas fichu de les rendre concrets, il y a trop de dépendances, etc. C'est là que nous avons besoin de la force des dirigeants : qu'ils sachent réduire à l'essentiel vos objectifs, qu'ils sachent les rendre concrets, qu'ils sachent prioriser la réduction des dépendances, qu'ils sachent ne pas se faire emporter par la politique interne, qu'ils sachent ne pas se faire séduire par d'autres objectifs cachés, etc. Ils ne réduiront pas toutes les dépendances, mais le plus vous ils en réduiraient assez pour faire du bien à leurs produits, les décisions seraient accélérées avec des objectifs priorisés et concrets, véritablement liés à leurs produits. Et le reste ? Et les autres contraintes ? Les autres objectifs ? Elles et ils suivront. Et surtout, elles et ils suivront beaucoup plus vite que si vous ils les prennent toutes et tous en comptes immédiatement !


Donc une stratégie : un objectif clair, concret, qui peut se décliner en sous-objectifs tout aussi clairs et concrets (mesurables) à la façon d'un *impact mapping* ou d'une approche OKR par exemple. La stratégie est le cheminement, la priorisation dans les différentes branches de ce cheminement, et une façon de valider ou invalider rapidement les hypothèses (d'où les objectifs intermédiaires mesurables). Le tout sur une période relativement limitée, car au-delà de six mois les hypothèses deviennent vraiment peu solides et ne méritent pas que l'on passe trop de temps à les envisager.   


Je te parle des mesures la prochaine fois.  

