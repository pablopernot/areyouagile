---
date: 2016-05-20T00:00:00Z
slug: il-etait-une-fois-scrum-3
tags: ['scrum','review','revue','retrospective','scrummaster','productowner','daily']
title: Il était une fois scrum 3
---

Dernier épisode de cette petite histoire de scrum, variation sur un
thème bien connu.

Notre itération avance. Nous arrivons le vendredi de la deuxième semaine. L'après-midi c'est la **revue** que l'on appelle aussi la **démo**, mais on ne fait pas une **démo**, on fait la **revue** du travail réalisé, c'est différent, donc je préfère **revue**. Non pardon : pas du travail réalisé, du travail fini. Fini : testé, validé, qui respecte la définition de fini et qui a été validé par le *product owner*.

À 14h et pendant disons 45 minutes on va donc faire la **revue** (que
l'on peut préparer un peu avant, à la fin de la matinée, si on est bon
élève, quelqu'un de bien, pas un vaurien, un *bad boy*). Durant celle-ci
on montre le travail fini. Et seulement celui-ci. Pas question de
montrer quelque chose en cours. Ce qui est en cours ne sera peut-être
jamais fini, ou différemment. Et pour avoir de bons débats, de bons
arbitrages, une vraie visibilité, un vrai *feedback* (rigolo le
*feedback*, on n'a pas de terme français vraiment adapté, c'est
symptomatique. J'aime bien le lien avec l'apprentissage) : bref, on
montre ce que l'on a et PAS ce que l'on a PAS. Mais pas d'intermédiaire.
On nous prend toujours pour des [cowboys
hippies](/2014/11/les-cowboys-hippies/) mais sur ce point nous sommes
binaires. La lecture du projet/produit est ainsi éminemment facilitée.
Et comme les éléments finis peuvent potentiellement être mis en
production on sait exactement ce que l'on a et ce que l'on a pas. Pas de
flou artistique sur 80% d'une fonctionnalité en cours : cela ne veut
rien dire.

Ainsi que se passe-t-il durant les 30/45mn de la revue ? Disons qu'il y
a une ouverture de 5mn : *"voilà l'état des lieux, voilà sur quoi nous
nous sommes projetés pour cette itération, voila l'état de nos
plate-formes et de nos tests automatisés. Naturellement à tout moment
vous pouvez vous connecter à la plate-forme d'intégration continue pour
jouer avec notre produit"*. Puis on fait la revue des choses finies, le
mieux c'est que cela soit le *product owner* qui le fasse (surtout les
premières itérations a minima, qu'il sente bien qu'il appartient à
l'équipe, et que quand il valide il pense à sa revue...). Donc la revue
surtout au début est portée par le *product owner* (après une autre
personne peut le faire, quand le pli est pris). Naturellement aucun
slide, on veut des choses finies, on montre des choses finies, sur du
réel. Si on ne peut pas montrer, on ne peut pas faire. Soyez inventifs.

Si dans la **définition de fini** il y a marqué **haute disponibilité**
comptez sur moi pour débrancher le serveur en direct avec tout le monde,
normalement la revue peut continuer sur un autre serveur puisque nous
disons que nous faisons de la haute disponibilité. Ou si il y a marqué
dans la **définition de fini** : apps mobile et site web, comptez sur
moi pour arrêter la revue sur le site et demander à la voir sur
l'application mobile. Je sais ces scénarios sont durs pour l'équipe,
mais c'est tellement mieux de se trouver en défaut pendant une revue que
en production. Et on se retrouve tellement plus zen quand on a poussé le
niveau de qualité jusqu'au bout. Et on gagne tellement en vitesse quand
cette qualité offre une base solide. Ainsi pendant \~20 minutes le
*product owner* fait la revue des réalisations finies (qu'il a validé au
fil de l'eau lors de l'itération), et demande du *feedback*. C'est le
lieu du *feedback* la revue. Que les gens parlent maintenant ou se
taisent à jamais (pour le meilleur et pour le pire...). Ils ne pourront
ne devraient pas dire qu'ils ne savaient pas. Les 5/10 dernières minutes
de la revue s'ouvre sur la suite : on propose un plan de release (qui
est remis à jour toutes les itérations) et qui projette la réalisation
estimée sur un calendrier. On a observé une cadence, on la reproduit, et
cela nous dit où nous devrions être à telle ou telle date. C'est très
efficace, et cela marche relativement bien : pas d'estimation mais la
reproduction d'une cadence observée, cela marche relativement bien aussi
car la lecture est claire : ce que l'on a on l'a, et ce que l'on a pas,
on ne l'a pas. Et comme tous les éléments sont découpés par avoir du
sens de façon assez autonome, la lecture de l'état des lieux du
projet/produit est efficace. À cela je rajoute qu'il ne devrait pas y
avoir de bug, et que nous appliquons la définition de fini qui prend en
charge les éléments clefs du projet/produit (charge ? performance ?
etc.) donc pas de surprise.

Et si vraiment on découvrait un bug lors de la revue ? Ca arrive,
rarement normalement, mais ça arrive. Il vient se placer en tête du
backlog, de l'expression du besoin, et on le traite donc en priorité 1.
Enfin, c'est ma philosophie, après le *product owner* peut décider
différemment tant qu'il assume.

Autre sujet, la revue est l'occasion pour tous les gens concernés
notamment les sponsors d'être au courant. Une demi-heure toutes les deux
semaines. Si malgré tout personne ne vient, proposez d'arrêter le
projet/produit. Il va s'ensuivre un dialogue de ce genre avec le sponsor
: "Nous avons arrêté le projet", "Quoi ? Pourquoi ?", "Personne ne vient
aux revues, c'est qu'il n'est pas important, autant se concentrer sur
les choses importantes", "Mais si il est très important !", "Attendez je
ne comprends pas soit il est très important et les gens viennent une
demi-heure toutes les deux semaines, soit il ne l'est pas et autant
l'arrêter". Naturellement c'est plus facile à porter comme dialogue pour
un externe comme moi, mais croyez moi, ça marche.

45 mn, 30mn, les 45mn sont une boite de temps hein. Tous les rituels de
scrum sont des boites de temps : on peut faire plus court, mais on ne
dépasse pas (la durée de l'itération, le sprint planning, etc.). On
apprend à fonctionner de façon optimale dans ce cadre, dans cette boite.
Et chaque boite se déroule au même moment, avec le même objectif, les
mêmes éléments en entrée, et les mêmes éléments en sortie. D'où l'idée
de rituels. Pas la peine de sacrifier de chèvre.

Là une sorte de mur Scrum et Kanban qui parait bordélique mais qui
marchait assez bien (cliquez pour agrandir).

![Sorte de Scrum & Kanban](/images/2016/05/sorte-scrum-kanban-small.jpg)

Petit moment de relaxation après la revue, puis c'est le tour de la
rétrospective.

La rétrospective est un sanctuaire, elle est réservée à l'équipe (les
développeurs -- dans le sens réalisateurs -- du produit --dans son
ensemble, pas que le code--, le *product owner*, le *scrummaster*). Pour
s'améliorer il faut se parler franchement, pour parler franchement au
sein de l'équipe, il faut savoir que les débats vont rester entre nous,
et qu'aucune personne extérieure ou manager n'est présente. Cependant si
l'équipe le demande, tout reste permis.

La **rétrospective** c'est le lieu donc de l'amélioration continue, pour
cela il faut s'interroger. Généralement pour deux semaines d'itération,
la rétrospective dure 2h. Les premières 20mn sont consacrées à se
rappeler ce qui s'est passé durant les deux semaines écoulées (une ligne
de temps avec les faits par exemple), puis l'heure suivante sert à
s'interroger, faire émerger les questions, les succès, les échecs, les
frustrations, les plaisirs, etc. On prend 5/10mn pour choisir ce qui
nous interpelle le plus : soit le point douloureux que l'on veut
résoudre, soit le point positif que l'on veut propager. Puis on passe la
dernière demi-heure à trouver comment le résoudre, ou le propager, de
façon factuelle, mesurable, réaliste durant les deux prochaines
semaines. Les embûches de la **rétrospective** : a) vouloir changer le
monde en deux semaines, se lancer dans cinq améliorations dans les dix
prochains jours, irréaliste et frustrant, mieux vaut se focaliser sur un
seul point ; b) ne jamais changer de format, toujours les mêmes
conversations et points de vue reviennent, irritant et frustrant, c'est
le boulot du *scrummaster* de manipuler différents formats qui feront
voir les choses sous des angles nouveaux régulièrement ; c) trop se
focaliser sur les aspects négatifs à améliorer : se focaliser sur les
points positifs et les propager c'est tout aussi efficace, voire plus !
; d) Se donner un plan d'action simple et réaliste qui puisse être porté
par l'équipe, et pas un plan compliqué, non mesurable, qui est lié à des
gens externes, c'est frustrant et inutile.

Voilà, lundi ça recommence : sprint planning première partie, 2h30, on
se projette sur un ensemble d'éléments à réaliser, l'après-midi l'équipe
découpe en tâches, on fait le *burndown* et le *burnup*, que l'on suit
dans la semaine, tous les jours on a notre *daily standup*, le *product
owner* valide au fil de l'eau les *user stories*, une ou deux réunions
d'affinage (*backlog grooming*) permettent d'anticiper le prochain
*sprint planning*, notre corpus de tests automatisés s'enrichit,
deuxième vendredi revue et rétrospective. Puis le lundi après sprint
planning, et ça repart. Un Scrum bien appliqué dégage une forte
impression de cadence et de rigueur, presque comme le Taylorisme. Sauf
que les équipes sont maîtres de leur destin. Ca change tout. J'ai
l'habitude de m'amuser à dire que si vous débranchez des équipes scrum
vous avez des poules sans tête : où est mon daily ? Sur quoi on
travaille pour les deux semaines à venir ? pour renforcer l'idée de
cadence. Cette cadence on l'aime ou pas, il faut des cycles de relâche.
Ne soyez pas surpris si tous les six mois, comme une équipe de sport,
votre équipe Scrum a besoin de repos. Laissez là faire. N'oubliez jamais
le rythme soutenable. Et que l'on ne va pas plus vite en allant plus
vite mais en allant différemment.

Cette rigueur est présente car l'outillage Scrum est compact, dans la
tête.

Quand plusieurs équipes cohabitent ? Et bien je suggère de ne pas les
synchroniser au niveau calendaire. C'est mon vécu, et je n'ai pas vu
l'inverse fonctionner aussi bien. Pourquoi ? Je fais primer la présence
des sponsors et des utilisateurs finaux lors des revues avant toute
chose : l'apprentissage, le feedback. Impossible d'avoir ces personnes
qui suivraient 10 équipes, soit 10 fois 45mn dans une même journée. Trop
lourd, indigeste. A ceux qui défendent l'idée qu'il suffit de faire 2
heures pour l'ensemble des équipes : j'y trouve un manque de respect et
manque de responsabilisation des équipes, c'est mon expérience, mon
point de vue. Donc je m'oriente plutôt vers des alternances de semaines,
de jours, pour permettre à toutes les revues d'avoir une vraie
existence, avec de vrais sponsors, client finaux (potentiellement), et
un vrai feedback/apprentissage.

Articuler plusieurs équipes ? On utilise généralement des plan de
release consolidés et/ou un [kanban
portfolio](/2016/01/portfolio-projets-kanban-partie-1/).

Articuler une organisation autour d'un produit ou projet en scrum ? de
l'[holacratie](/2016/04/hellocratie-holocratie-holacratie-honolulucratie/)
? votre propre saveur d'entreprise libérée, au choix. Mais n'oubliez pas
les [bases de ce monde complexe](/2015/01/modernite-agilite-engage/).

Une transformation agile ?

-   [Qu'est ce qu'une transformation agile réussie
    ?](/2015/05/quest-ce-quune-transformation-agile-reussie/)
-   [Chemin d'une transformation
    agile](/2015/06/chemin-dune-transformation-agile/)
-   [Paradoxes des transformations
    agiles](/2015/09/paradoxes-des-transformations-agiles/)

Des rétrospectives ?

-   [Festival de rétrospectives](/2015/03/festival-de-retrospectives/)
-   [L'art de la rétrospective](/2013/05/lart-de-la-retrospective/)
-   [Deux formats de
    rétrospectives](/2016/04/2-nouveaux-formats-de-retrospective/)
-   [Rétrospectives osées]()

Les trois épisodes de la série et le guide de survie :

-   [Il était une fois scrum \#1](/2016/05/il-etait-une-fois-scrum-1/)
-   [Il était une fois scrum \#2](/2016/05/il-etait-une-fois-scrum-2/)
-   [Il était une fois scrum \#3](/2016/05/il-etait-une-fois-scrum-3/)
-   [Guide de survie à l'agilité](/pdf/guiderapide.pdf) (glossaire,
    descriptions courtes des rituels, bibliographie, etc.).

Pour finir, Scrum, on s'en fout. Kanban on s'en fout. XP on s'en fout
(mais ça tout le monde le dit malheureusement). Avant toutes choses
pensez aux fondations : l'intention et l'attitude de ceux qui ont le
pouvoir dans l'entreprise et les moyens physiques de vos équipes et
produits: co-localisation ou véritable espace virtuel, type de produit,
de marché, de déploiement, etc. Après Scrum, Kanban, XP sont des
outillages aboutis, aiguisés, qu'il est très bien d'utiliser pour faire
la différence.

