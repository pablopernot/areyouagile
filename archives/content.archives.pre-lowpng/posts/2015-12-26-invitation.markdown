---
date: 2015-12-26T00:00:00Z
slug: invitation
tags: ['oaa,','openagileadoption,','openadoption,','invitation,','openspaceagility']
title: Invitation
---

Je dis souvent que le meilleur moyen de responsabiliser, d'impliquer
c'est un objectif clair, des règles claires, du feedback et de
l'invitation, un sentiment de contrôle, de progrès, d'appartenance et
d'agir pour quelque chose qui nous dépasse (relire : [modernité,agilité, engagé](/2015/01/modernite-agilite-engage/)).

Comment rendre opérationnel ces préconisations : clarifier ? Expliquer ?
La grand majorité du temps pour un leader, pour un manager, pour un
mentor, se passe à expliquer, à expliciter, à questionner. Comme en
thérapie, avant de vouloir régler un problème, ou d'aller plus loin,
d'atteindre un objectif, il faut le clarifier et le partager. La
transparence, le management visuel, le dialogue sont au cœur des
pratiques que l'on attend. D'abord communiquer, aligner, clarifier avant
de gouverner.

Mais l'**invitation** demeure probablement la notion la plus difficile à
appréhender dans nos organisations. Elle est pourtant l'une des plus
puissantes. Comment inviter? Plusieurs applications concrètes. A
l'instar de [Jim et Michele
McCarthy](http://www.mccarthyshow.com/aboutus/), pourquoi ne pas inviter
les équipes à se constituer selon les projets de l'organisation ? Le
premier réflexe est d'imaginer une bande de copains aussitôt se
constituer en oubliant les attentes projets. Or l'invitation n'est pas :
« faites une bande de copains » mais: « essayez de constituer les
équipes au mieux pour réussir ces projets ». Dès lors, comme tout
système vivant normalement constitué, chacun comprenant et intégrant la
responsabilisation qui va de paire avec la liberté octroyée, chacun va
faire au mieux en essayant de trouver la meilleure alchimie entre une
équipe de copains et les compétences requises pour la réussite des
projets. L'implication liée à l'invitation permettant probablement le
meilleur alliage entre amitié et compétence. Un projet fait fuir tout le
monde? L'invitation rend cela visible: personne ne veut faire ce projet,
et probablement pour de bonnes raisons, il est donc évident, apparent,
clair, qu'il va falloir des mesures particulières pour le traiter. On
l'apprend ainsi clairement, et assez tôt.

Autre exemple: les réunions, ces fameuses réunions où tout le monde se
sent obligé de respecter l'invitation (quel paradoxe). Imaginez que
l'invitation devienne réelle? Plusieurs scénarios: sur les douze
personnes que le grand chef a invité, aucune ne vient. La première
hypothèse est qu'il est possible que la réunion soit inutile et
franchement toutes ces personnes ont bien fait de l'esquiver. Autre
hypothèse, cette réunion était bel et bien importante, mais personne
n'en a compris la teneur, la gravité ou l'importance. Le grand chef
réalise à cet instant que sa formulation et son message n'étaient pas
les bons, qu'il doit revoir sa copie pour clairement expliciter
l'intérêt et le sens de cette réunion. Autre hypothèse: tout le monde a
compris que la réunion était importante, mais des choses importantes,
tout le monde en a plein. Et personne n'a su si celle-ci était plus
importante que les autres choses sur lesquelles il travaille. Le grand
chef vient de mesurer qu'au delà d'une bonne communication, il doit
gouverner, principalement arbitrer, prioriser; à lui d'indiquer quoi
stopper, quoi arrêter pour se pencher sur cette nouvelle réunion (il
doit donc lui aussi savoir ce qui se passe en ce moment dans son
organisation).

Autre scénario: seules quatre personnes viennent sur les douze invitées.
Il s'agit probablement des personnes qui ont saisi l'importance de la
réunion, et ont comprises la priorisation à mettre sur cette réunion. En
un mot: il s'agit des bonnes personnes, celles qui ont compris et qui
ont le temps de traiter.

Autre scénario, le malheureux scénario traditionnel: tout le monde est
venu, car tout le monde sait qu'une invitation du grand chef ne se
refuse pas. Lors de la réunion tout le monde hoche de la tête (coup-ci
coup-ça sur son mobile ou son portable). À la fin le grand chef est
satisfait. Mais il ne saura pas dire: qui a vraiment compris le message?
Et parmi eux, qui a vraiment compris l'importance, la priorisation du
sujet, et qui peut prioriser ce sujet ? Il est aveugle. **Sans
invitation tout n'est qu'illusion**.

Un dernier exemple d'invitation: les [Open
Adoption](/2015/11/darefest-2015/). Il s'agit d'une technologie de
conduite du changement. Elle se base sur l'invitation: chacun est invité
à participer, selon le thème chacun est invité à proposer des
initiatives, invité à mettre en œuvre ces initiatives (ou celles
d'autres personnes) dans les mois à venir (jusqu'à la nouvelle journée
d'[Open Adoption](/2015/11/darefest-2015/)). Cette invitation a
plusieurs vertus: elle laisse la liberté d'essayer et de revenir en
arrière, c'est crucial en [conduite du
changement](/2015/09/paradoxes-des-transformations-agiles/), un
changement imposé et définitif est celui qui peut générer la plus grande
résistance. Elle laisse la liberté d'une réelle adaptation au contexte:
on fait ce qu'il est possible de faire selon le contexte (volonté,
désir, environnement, histoire, etc.) et en respectant le thème. Cette
invitation rend caduque les autres chemins plus obscurs : voici un
espace d'expression publique c'est celui-ci qui doit être utilisé.

Autre vertu: elle rend flagrant l'intégration ou non de chacun dans le
mouvement de l'organisation. Celui qui ne vient jamais, qui n'a jamais
de proposition, qui ne met jamais en œuvre quoi que soit, et sur un laps
de temps indicatif. Celui-là s'interroge sur son intégration dans
l'organisation, et sur sa raison d'être.

**Inviter pose des questions essentielles: qu'est ce que je voudrais?
Qu'est ce que je désire ? Suis-je bien dans ce cadre, ce lieu ? Et il
est essentiel de se les poser**.

*Ceci est un extrait des "organisations vivantes" que je me suis fixé de
clôturer début 2016 (avec vos encouragements svp ), et cette partie est
très inspirée par les conversations que j'ai pu avoir avec Dan Mezick en
2014 et qui ont pu macérer depuis dans mon esprit*.
