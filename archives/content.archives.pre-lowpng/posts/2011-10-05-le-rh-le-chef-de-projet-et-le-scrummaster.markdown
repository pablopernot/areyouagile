---
date: 2011-10-05T00:00:00Z
slug: le-rh-le-chef-de-projet-et-le-scrummaster
tags: ['scrummaster','rh']
title: Le RH, le chef de projet et le scrummaster
---

Je reviens sur ce sujet très récurrent de la transition vers Scrum/XP
qu'est le bouleversement des rôles et des responsabilités. Au détour de
l'une de mes récentes missions deux chefs de projet ont annoncés qu'ils
donneraient prochainement leurs démissions si l'on poursuivait dans
cette voie (Scrum/XP). Il faut dire que pour lutter contre les habitudes
j'ai tendance à vouloir "[déséquilibrer la routine](/2011/07/desequilibre-et-conduite-du-changement/)".

![Detritus](/images/2011/10/detritus2.gif)

En effet le "chef de projet" est mis à mal par l'organisation agile. A
des gens a qui on n'a pas cessé de demander d'être (un peu) bon sur 3
domaines à la fois : en management, sur les aspects fonctionnels, et sur
les aspects techniques, on demande soudain de choisir entre l'un des 3
domaines (management, soit le scrummaster, aspect fonctionnel soit le
product owner, aspect technique soit l'un des membres de l'équipe).
Drame.

Il faut comprendre que le fait d'être "chef de projet" est souvent un
cheminement de longue haleine. Beaucoup de chefs de projet sont des gens
avec pas mal d'ancienneté qu'il a fallu récompenser. Et délivrer le
statut de chef de projet est aussi une façon de mieux rémunérer
certaines personnes. Une abhérration qui fait que certaines personnes
qui raffolent de la technique sont devenus bon gré mal gré chef de
projet pour enfin pouvoir franchir des échelons (c-a-d augmenter son
salaire). Le mieux aurait été d'avoir un cursus technique digne de ce
nom (j'y reviens).

Petite parenthèse : c'est normal de vouloir être récompensé, c'est
normal de vouloir gagner plus d'argent.

Et donc le drame agile c'est que l'on va dire à ces personnes : soit tu
actes définitivement que tu souhaites manager et tu deviens scrummaster
(mais fini le code, les estimations, etc.), soit tu valides que la
technique est ton dada et tu réintègres l'équipe. Aïe. Là tout le monde
râle. L'ex chef de projet qui a l'impression de "rentrer dans le rang"
(et comment va-t-il négocier son salaire, ses augmentations, son rang !
etc.) et l'équipe : pourquoi lui serait-il plus payé que nous ? Le
troisième cas de figure : le "chef de projet" devient product owner est
plus rare car le rôle de product owner est souvent attribué (bonnes ou
mauvaises raisons) a des gens hiérarchiquement plus gradé que le "chef
de projet".

Si le "chef de projet" souhaitait s'orienter vers le fonctionnel et
qu'il devient product owner, les choses se déroulent généralement bien.

Si le "chef de projet" souhaitait s'orienter vers le management et qu'il
devient scrummaster, les choses se déroulent généralement bien.

Dans tous les autres cas des tensions et des difficultés apparaissent.

Pour palier à ces difficultés l'une des principales solutions que je
préconise : acter de la réelle existence d'une carrière technique senior
bien rémunérée au sein de la société. Cette voie manque cruellement dans
les entreprises françaises. On doit pouvoir faire partie d'une équipe
tout en se prévalant d'un échelon de carrière élévé et technique, et
donc bien rémunéré. Les gens intéressés par la technique doivent pouvoir
faire une vraie carrière dans cette branche. Ne pas hésiter au niveau RH
(et donc concernant la rémunération) de mettre en avant des profils
senior, expert, technical lead, etc. qui permette un avancement "de
profil" (avec salaire en adéquation) et pas nécessairement un avancement
hiérarchique.

Car quoi qu'il en soit nos hiérarchies sont assez plâtes et si vous
souhaitez progresser encore après avoir été "chef de projet" quelles
sont les solutions qui s'offrent à vous ? Disons pour les SSII :
Directeur de projets, puis directeur d'agences, etc. Ou pour les
éditeurs : Directeur technique, puis.. Beaucoup de candidats, peu de
postes. Pas de secret là dessus soit vous êtes la bonne personne, soit
il faudra changer de boite pour gravir les échelons plus vite, ou gravir
les échelons tout simplement.  Agile ne parle pas de cela, car il n'a
rien de plus à promettre à ce sujet (si ce n'est un véritable
épanouissement au sein d'une équipe, ou d'un groupe de personnes, ce
n'est pas rien finalement).

Les RH m'ont confirmés que effectivement un chef de projet pouvait
démissionner et se pourvoir aux prud'hommes pour gagner, car le fait de
changer ainsi sa "feuille de route" était une raison valable de rupture
de contrat.

Rassurez vous aucun des chefs de projet n'a démissionné et ils
apprécient les nouvelles conditions de travail.
