---
date: 2021-06-23
slug: panorama-agilite 
tags: [panorama, mouvement, approche, agile]
title: "Panorama des approches agiles"
--- 

En écho à ce vieil article de 2015, [mouvements agiles](/2015/08/mouvement-agile/), je remets au goût du jour cette petite présentation, car je m'en sers en ce moment auprès de plusieurs groupes. Naturellement dès qu'on présente l'histoire de l'agilité les milices de la bonne pensée agile se révoltent, je propose de les laisser se révolter et de vous donner ma lecture. 

	Ne dites pas les faits ! Dites la vérité ! – Maya Angelou

## Petit panorama visuel

![Panorama des approches agiles](/images/2021/06/panorama.png)

## Le glossaire 


### *Lean*

Pour les méthodologistes et les industriels, oncle d'Agile quand le monde était encore compliqué (et pas complexe, où commençait à
l'être). On aurait pu ajouter **Lean* Software*, qui est le pendant IT du *Lean*. Attention avec l'appellation *Lean* : à lire [les cowboys
hippies](/2014/11/les-cowboys-hippies/).

Le *Lean* est proche de agile, mais il en diffère dans le sens où l'adaptation sert une standardisation, alors que dans agile l'adaptation est reine. 

### Agile

Historiquement pour l'IT et les DSI, car cela apparaît avec le **code**. Le terme apparaît quand une presque vingtaine de beatniks américains en ont marre de voir leur industrie aller dans le mur et veulent changer leurs façons de travailler. Mais comme ils ont du mal à s'entendre, ils se mettent d'accord sur un terme simple "agile", et sur un manifeste. Cette frugalité est une chance.  

Trois grandes approches qui **ne s'excluent pas, mais se complètent** !


* **eXtreme Programming** (XP) Des pratiques d'ingénieries (technique, code, etc.) et l'émancipation sociale du développeur. Il apparaît à la fin des années 90, et est vraiment le mouvement phare au moment de la création de Agile. Mais la mauvaise communication, ou la non-communication, ou la posture *extreme* ou le mouvement politique associé, ou la difficulté, ou <mettez une autre raison> en fait un élément fragile qui ne réussit pas à se populariser tant que cela. La crise économique de 2003 le met en berne. 

* **Scrum**, Faire des projets complexes en environnement complexe. Il apparaît au milieu des années 90. Il gagne en popularité au milieu des années 2000, et explose littéralement en 2008 avec la crise économique. Il est conspué par les gens XP qui sont jaloux de ce succès et l'attribue à la facilité de prise en main de l'outil. Cette facilité est réelle c'est un avantage comme un inconvénient. Beaucoup des critiques sont justifiées, mais elles s'adressent à ce que l'on a fait de Scrum, et pas à Scrum lui-même. Enfin c'était vrai jusqu'à que [les deux vétérans qui ont créé Scrum dérapent par appât du gain](/2021/01/difficile-fin-de-carriere-pour-scrum/). À lire le [guide de survie à l'agilité et à Scrum](/pdf/guiderapide.pdf). 
  
* **Kanban** Amélioration continue des flux de création de valeur (comme la maintenance, un cycle de vente, l'organisation d'évènements, des activités de RH, etc.). Proche et en même distinct du Kanban de *Lean*, il apparaît dans cette nouvelle forme au milieu des années 2000 porté par David Anderson, personnage intelligent, mais imprévisible et à la communication erratique. Il porte le même risque que Scrum : ça prise en main est facile et ainsi il peut aussi facilement être corrompu. Un livre sur [Kanban](/2019/06/kanban-le-livre/) !


### Software Craftmanship

Les mecs et les nanas de XP (*eXtreme Programming*) sont vénères (énervés) par les projecteurs sur Scrum. Et ainsi à la fin des années 2000 ils se décident : on refait une beauté à *eXtreme Programming* et on rappelle qu'il faut du temps et de la sueur, à l'inverse d'un Scrum ou d'un Kanban : le ticket d'entrée est cher, mais il est plus dur à corrompre. On rappelle que c'est un mouvement agile : l'artisanat l'emporte sur l'industrialisation, l'idée de compagnons du code se développe.

### Devops

C'est *Extreme Programming* dont on pousse les pratiques jusqu'au déploiement continu (en plus de l'intégration continue) en intégrant d'autres populations comme les infras. Et ainsi d'une part avec plus de regards, de feedback, on enrichit le produit, et avec un flux régulier de l'idée à la mise en production : on améliore le feedback des clients, on améliore la qualité (régularité + flux = qualité).  

### Lean Startup

Agile pour la gestion "produit", pour le *product management*, pour les métiers, les idées, les startups. Aurait du s'appeler *Agile Startup*, mais *Lean* fait plus vendre (À lire [les cowboys hippies](/2014/11/les-cowboys-hippies/)). 

### Design Thinking

Agile avec la mise en exergue de l'expérience utilisateur (UX): pour les designers, les créas, etc. Mais aussi les métiers, les gens du
produit, très redondant ou complémentaire avec *Lean Startup*. On parle de *discovery* (mais que fait donc mon utilisateur ?), et d'*ideation* (comment pourrais-je penser les choses différemment). C'est une approche des années 80 récupérées dans tous ces mouvements. 

### Management 3.0

Un ensemble de pratiques faciles et efficaces à prendre en main pour commencer à faire basculer son entreprise vers la modernité. La plus importante à mes yeux : le tableau de délégation (*delegation board*). Car où se situent les décisions en dit beaucoup sur votre entreprise. 


### Entreprise Libérée 

Je ne le fais pas apparaître dans le diagramme, une posture Agile pour les entrepreneurs et les managers, vision de
l'entreprise nouvelle.

### Holocratie - Sociocratie

Au-delà des approches orientées flux ou produit, une façon de penser son organisation de façon organique et à l'échelle. Sociocratie, Holacratie : des méthodes pour équiper l'auto-organisation et le management Agile dans les entreprises libérées.
[Petite série sur l'Holacratie](https://www.infoq.com/fr/holacratie/). Pour résumer : le mouvement historique initial dans les années soixante-dix, la sociocratie, une adaptation en marque déposée pour faire du fric à la mode américaine : l'holacratie.  

### Spotify/Nexus/SAFe/Less

Des manuels pour mieux appréhender la mise à l'échelle de l'agilité. S'en inspirer sans reproduire !!! SAFe a été imaginé pour être une réussite marketing peu importe le fond, il a diablement réussi. C'est aujourd'hui le cache-misère des entreprises qui veulent se dire agile sans l'être. Note : on s'en fout d'être agile, on veut être "performant", mais SAFe vous détourne des deux. 

* [Pourquoi faut-il se méfier de SAFe](/2017/11/pourquoi-faut-il-se-mefier-de-safe/)
* [Sur l'agile à l'échelle](/2019/03/dojo-agile-a-l-echelle/)

### Openspace Agility

Un outillage basé sur le forum ouvert pour générer de l'engagement (et c'est clef) pour votre conduite du changement. 

* [OpenSpace Agility](/2014/04/histoires-dopen-agile-adoption/)

### Focus valeur

Les organisations désormais commencent à s'organiser autour de la valeur qu'elles souhaitent créer (*impact teams*) ou comment appréhender la valeur.

* **Beyond Budgeting/Noestimates** Une approche qui met en évidence les pertes liées aux planifications basées sur les estimations (temps et coûts), et qui propose une alternative en faisant au fil de l'eau (par priorité et en mesurant les impacts). [Une série de slides sur #noestimates et beyondbudgeting](/images/2017/05/Agile-CFO.pdf). 

* **OKR** À manier avec précautions, car il devient le nouveau grâal des entreprises en mal de macaron agile. Permet d'articuler sa stratégie et sa tactique autour d'objectifs concrets pour améliorer l'analyse, la réalisation, la compréhension. Le mieux est de lire ce livre blanc sur le sujet de Laurence Wolff et Tiphanie Vinet [**Piloter le produit par l'impact, grâce aux OKR**](https://clic.benextcompany.com/livre-blanc-produit-piloter-produit-par-impact-avec-okr)


## N'oubliez pas

La sauce de base est la même. Il s'agit de saveurs développées pour adresser des populations souvent différentes qui sont intéressées par
des portes d'entrée différentes. Des cartes joker pour faciliter la conduite du changement aussi. Au final, c'est la même approche, la même
philosophie, le même changement de paradigme. D'autant que dans une compagnie il **ne** me paraîtrait **pas** absurde que **tous** ces
mouvements soient présents, au contraire. Même dans les petites compagnies très rapidement plusieurs de ces mouvements sont nécessaires.


