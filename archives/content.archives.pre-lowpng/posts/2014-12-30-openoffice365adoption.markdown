---
date: 2014-12-30T00:00:00Z
slug: open-office365-adoption
tags: ['oaa','openagileadoption','office365','adoption','openspaceagility']
title: Open Office365 Adoption
---

**Open Office365 Adoption**, qu'est ce que c'est ? C'est d'abord la
synergie de deux compétences fortes au sein de
[Smartview](http://www.smartview.fr) : sa branche microsoft et sa
branche agilité/conduite du changement. C'est aussi l'aboutissement de
deux constats croisés : réussir le déploiement d'une telle solution
n'est pas aisé, réussir les transformations organisationnelles
d'entreprises ce n'est pas aisé. CQFD : Réussir une transformation
autour d'une solution comme Office365 n'est pas aisé. **Open Office365
Adoption** c'est donc la convergence de nos constats, de nos expériences
autour de nos métiers pour mieux répondre aux attentes de nos clients.

Nous avons pu faire une première présentation officielle de cette
approche ce 12 décembre 2014, à Genève, à l'invitation de [Yoan
Topenot](http://ytopenot.wordpress.com/). Pour moi c'était une première,
une sorte de safari ethnologique dans la tribu des MVP Microsoft que je
ne côtoyais jusqu'à présent qu'au travers de mon associé [Gilles
Pommier](http://mvp.microsoft.com/fr-fr/MVP/Gilles%20Pommier-5000494)
(cela fera peut-être l'objet d'un futur article). Je ne vais pas vous
parler de cette matinée, [Renaud Comte en a fait un feedback
ici](http://blogs.developpeur.org/themit/archive/2014/12/12/compte-rendu-sharepoint-o365-des-pratiques-pour-une-meilleure-productivit.aspx),
et je vous en propose les *slides* plus bas. Je vais plutôt vous
présenter notre approche, et notre offre.

## Nos constats

Adopter **Office365** ce n'est pas aisé. Gilles observe que trop
fréquemment les gens n'y accordent pas assez de **temps**. Syndrome
classique : c'est important mais on n'a pas de temps à y consacrer.
Solution simple : ne le faites pas ! Ah si c'est important !! Ah mais
alors je ne comprends plus : c'est important mais vous n'avez pas de
temps à y consacrer. On revient au point de départ : si c'est important,
il faut du temps.

Ce manque de focus, pourtant crucial, est accentué par tout un tas de
**croyances** associées à Office365, qui a plein de qualités (et des
défauts), mais pas nécessairement celles auxquels vous pensez. Et
notamment il est indispensable d'y consacrer du temps, j'insiste, ce
n'est pas magique. Cela me fait penser à souvent la réponse que
j'obtiens quand on m'interroge sur SAP et l'agilité, sur le côté
intégré, global, tout prêt, tout en un de SAP : "cela ne se prête pas
donc pas l'agilité monsieur". Mon avis est tout le contraire.
Généralement je réponds à cette question par une autre question : si
tout est intégré, tout en un, global, tout prêt, vous n'avez strictement
aucun consultant SAP qui vous accompagne sinon je ne comprends plus. Je
vous laisse imaginer la réponse.

Autre point : la **gouvernance** est très souvent manquante. Là encore
comme d'habitude on souhaite un outil, un "comment", avant de
s'interroger sur le "pourquoi", la valeur, la vision. Et sans valeur
désirée, sans vision, il est normal que l'on évacue la gouvernance.

Dernier point que me rappelle Gilles, mon associé, comme toujours pour
les aspects pointus, gagner un temps précieux, éviter les écueils connus
et récurrents, il faut des **experts**. S'entourer du bon expert au bon
moment (pas trop tard) va vous coûter plus cher à l'instant T mais vous
fera gagner beaucoup plus sur le moyen terme.

A ces principales problématiques d'adoption autour de Office365 viennent
s'ajouter les classiques difficultés liées à la conduite du **changement
organisationnel** en entreprise. Le constat n'est pas folichon, depuis
30 ans les chiffres n'évoluent pas, *grosso modo*, 70% des changements
organisationnels échouent. J'ai bien dit 70%.

## Notre approche

Ainsi se mêle dans ces transformations des problématiques d'émergences
(c'est le domaine du complexe), à des problématiques d'expertises (c'est
le domaine du compliqué). Complexe et compliqué les réponses sont très
différentes : des experts d'un côté, du temps, de l'émergence et de
l'interaction de l'autre (pour creuser cela : [cynefin et son lego
game](/2013/04/cynefin-et-son-lego-game/)). Il faut donc deux pendants à
ces transformations : une expertise technique et métiers pour gagner du
temps, de l'argent, de la connaissance, de la précision ; une approche
émergente pour s'adapter au mieux au contexte : cible, vision,
adaptation aux personnes et ressources, réponses aux problématiques,
etc.

Concernant la partie émergente nous proposons d'utiliser les dernières
approches de conduite du changement pour réussir son adoption en
entreprise d'une solution comme Office365, que nous "équipons" des bons
experts. Les dernières approches de conduite du changement ? Pour
l'avoir utilisé et avoir pu être au première loge en recevant **Dan
Mezick** en 2013, je suis un fier supporter de **Open Agile Adoption**.
C'est pourquoi nous appelons notre approche **Open Office365 Adoption**.
Nous appartenons au groupe **Prime OS** lancé par Dan Mezick, et nous
réalisons régulièrement des Open Agile Adoption. C'est une technologie
que j'apprécie beaucoup et qui demeure récente et encore malheureusement
méconnue, pour en savoir plus : [histoires d'open agile
adoption](/2014/04/histoires-dopen-agile-adoption/). A celle-ci
j'associe des ingrédients du très proche
[Changeboxing](http://www.slideshare.net/claudee/le-changeboxing-une-approche-innovatrice-de-gestion-du-changement-inspire-du-lean-version-longue-avec-rfrences)
de [Claude Emond](http://www.claudeemond.com/).

Globalement il faut s'assurer de la vision, et du soutien du management,
sans cela le changement sera très difficile. Il faut s'assurer du désir
de changement et d'une capacité d'auto-organisation comme le dit [Claude
Emond](http://www.claudeemond.com/). C'est une phase d'alignement
indispensable (qui peut prendre la forme d'entretiens, de séminaires
et/ou formations). Si celle-ci est concluante on peut définir les
groupes cibles ou fonctionner purement par invitation (comme dans Open
Agile Adoption).

Pour bien comprendre Open Agile Adoption, je vous renvoie à : [histoires
d'open agile adoption](/2014/04/histoires-dopen-agile-adoption/). Mais
je reviens sur plusieurs points clefs :

-   Encore une fois il faut le support des **grands chefs** (et donc
    leur présence aux moments clefs du cycle : *openspace* ou journée de
    *changeboxing* ). Ils vont définir les contraintes et la vision, et
    garantir de leur soutien. Rien de pire que de se dédire là dessus,
    la confiance perdue est difficilement récupérable.
-   Le principe de l'**invitation**, et donc de la non contrainte, est
    indispensable : pour y voir clair sur l'état des lieux, pour
    permettre l'implication des acteurs, pour favoriser les bénéfices de
    l'adaptation au contexte, etc.
-   L'approche par **cycle** et la remise possible en cause des actions
    d'un cycle à l'autre est essentielle pour atténuer la résistance
    naturelle au changement, ou ne pas se fixer trop tôt sur une
    solution inadéquate.
-   La transformation est portée et adaptée par les acteurs de celle-ci.
    Les porteurs des *brainstorming* qui ont lieu durant la journée
    d'*openspace* sont souvent *invités* à devenir les champions du
    sujet durant le cycle. Mais il n'y a aucune obligation à ce sujet.
-   On va distiller dans les cadres proposés par Open Agile Adoption un
    ensemble de bonnes pratiques technologiques ou fonctionnelles
    portées par nos experts (voir plus bas) qui permettront aux
    *brainstorming* des *openspace* de partir sur des bases saines, sans
    jamais être prescriptif. Ceci est tiré par exemple du
    [Changeboxing](http://www.slideshare.net/claudee/le-changeboxing-une-approche-innovatrice-de-gestion-du-changement-inspire-du-lean-version-longue-avec-rfrences)
    de Claude Emond et se marie très bien avec l'Open Agile Adoption de
    Dan Mezick (ici la [changeboxing
    cheatsheet](https://speakerdeck.com/claudeemond/cheat-sheet-changeboxing-process)).
-   On consolide les résultats et propositions de chaque début de cycle
    : chacun sait qu'ils peuvent les mettre en œuvre dans l'intervalle,
    et qu'un nouveau cycle se déclenchera (dans les 2 mois, dans les 3
    mois, la durée est à définir selon le contexte). Souvent on suit
    l'avancée à l'aide d'un *Kanban Portfolio*.
-   Accompagnement, expertise et coaching, amplification du
    *storytelling* par le management, vont permettre le changement et
    son ancrage dans l'entreprise. Ces actions se déroulent entre les
    cycles selon un rythme qui dépend du contexte.

## Notre offre

Notre offre, l'offre de [Smartview](http://www.smartview.fr), et je suis
très content car c'est la première fois que l'on marie aussi bien nos
compétences diverses et variées, c'est donc de vous proposer trois
profils qui couvrent tous ces aspects pour réussir vos adoptions de
Office365.

-   [Gilles
    Pommier](http://mvp.microsoft.com/fr-fr/MVP/Gilles%20Pommier-5000494),
    en tant qu'expert technique des solutions Microsoft et notamment MVP
    Office365, porte les actions d'alignement, de clarification,
    d'accompagnement dans le cycle, et dans les journées d'Open Agile
    Adoption.
-   **Mylène Dumon**, en tant qu'experte des aspects fonctionnels autour
    des solutions Microsoft, porte aussi les actions d'alignement, de
    clarification, d'accompagnement dans le cycle, et dans les journées
    d'Open Agile Adoption.
-   Et moi même, **Pablo Pernot**, je porte les démarches de conduite du
    changement et de conduite de projet.

Plus bas vous trouverez les slides de la présentation du 12 décembre à
Genève, ainsi qu'un petit film pour proposer notre session au travers le
monde, âmes sensibles s'abstenir.

La [vidéo pour les âmes non sensibles](https://drive.google.com/file/d/0B3ZHNHN0HR5mVnRacV9UbG1keTA/view?usp=sharing).

La présentation :

<script async class="speakerdeck-embed" data-id="057c0180633a0132afd532fb620966c7" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>

