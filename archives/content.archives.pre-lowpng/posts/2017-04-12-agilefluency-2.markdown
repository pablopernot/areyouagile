---
date: 2017-04-12T00:00:00Z
slug: conversation-agile-fluency
tags: ['agile','fluency','conversation','openadoption','adoption','holacratie','sociocratie','openspaceagility']
title: Conversation autour d'Agile Fluency
---

Suite à mon [précédent billet sur Agile Fluency](/2017/04/perspective-agile-fluency/) on m'interroge :

*Bonsoir Pablo*

*Je viens de lire votre article "PERSPECTIVE PLUTÔT QUE FRAMEWORK :
AGILE FLUENCY", j'aimerais savoir où vous placeriez l'* [Open Agile
Adoption](/tag/openadoption/) *dans cette classification ? à la
quatrième étoile avec la Holacracie et consort ?*

*Si je fais le parallèle avec l'entreprise où je travaille :*

**1ère étoile** : *Scrum est maitrisé, appliqué à la lettre comme nous
l'a enseigné* [Claude Aubry](http://www.aubryconseil.com) *pendant deux
ans. Nous avons migré vers du Kanban depuis 2 ans.*

**2ème étoile** : *Nous faisons de l'intégration continue, la qualité
est au rendez-vous et avec le sourire.*

**3ème étoile** : *Relation accentuée avec les commerciaux pour
connaitre le besoin, le feedback et les opportunités. Nous pouvons
encore nous améliorer coté Lean-Startup*.

**4ème étoile** : *Nous n'avons plus de hiérarchie depuis 3 ans et nous
utilisons l'Open Agile Adoption. Nous avons fait le 8ème forum ouvert
fin mars.*

*Est-ce que mon parallèle est bon pour la 4ème étoile ?*

## D'abord merci

D'abord merci pour ce feedback, qui avant de répondre, me parait sain et
joyeux, donc signe avant coureur de bonne santé ! Et puis une
organisation qui a déjà huit [Open Agile
Adoption](/tag/openadoption.html) à son actif cela ne court pas les rues
! Félicitations.

## Ma lecture de vos propos

### 1ère étoile ?

Scrum ? Kanban ? Ils ne sont que des moyens. Des cadres, très affutés,
très performants quand on peut où quand on sait les manier, les
appliquer, mais ils demeurent des moyens. Pour cette première étoile il
faudrait s'assurer lorsque que produisez quelque chose c'est toujours
fait sous l'angle de la valeur générée : quelle valeur cela amène-t-il ?
Par exemples : Est-ce que cette fonctionnalité ou cette initiative va
nous faire gagner des parts de marché ? Est-ce que nous allons gagner du
temps en réalisant cela ? Quels bénéfices tirons-nous de cette activité
? Est-ce que cela marche ? Marche ? Est-ce que cela nous apporte quelque
chose ? En temps ? En part de marché ? En satisfaction ?

Vous pourriez très bien faire un Scrum de haute volée mais comme les
Shadoks ne réaliser que des choses inutiles. Donc pour le niveau 1 il
faudrait répondre avant toute chose à ce **focus valeur**. On dit que
\~50% des équipes agiles atteignent ce niveau.

C'est des fois difficile de définir ce qui se cache sous le terme valeur
: gagner du temps, gagner des parts de marchés, des utilisateurs, etc.
Mais cela peut aussi être une valeur d'apprentissage. On en sait plus
sur...

### 2ème étoile ?

*La qualité et le sourire*. Très bien. *Intégration continue*. Très
bien. La question est la suivante : Pouvez-vous délivrer quand cela fait
sens pour le marché ? Quand les clients attendent soudainement quelque
chose, quand un événement se produit (nouveau concurrent, rupture
technologique, nouveau marché, etc.) Êtes-vous en mesure de délivrer la
valeur que vous avez produit ? Une valeur qui répond potentiellement à
cet événement ? Quand on vous dit : là il faut sortir cela, vous ne
répondez pas : nous pourrons le sortir dans trois mois, non vous êtes
prêt à le sortir ? Si oui, c'est cela le niveau deux. Donc oui cela
passe par **une maîtrise technique qui rend invisible la technique**.
Ici on sait **délivrer de la valeur quand cela a de la valeur**. On dit
que \~30% des équipes agiles atteignent ce niveau.

### 3ème étoile ?

*Une relation accentuée avec les commerciaux*. C'est très bien.
Idéalement au delà : avec les utilisateurs finaux, ceux qui sont
impactés par votre activité, votre produit, ceux pour qui vous avez
pensé ce produit, cette activité. Donc une capacité à avoir un vrai
dialogue avec les acteurs de votre domaine. Avoir une vraie connaissance
des acteurs de ce domaine. Avoir beaucoup de mesures : ceci est utilisé,
ceci non, ceci oui mais uniquement à ce moment pour telle raison, etc.
J'ai sorti deux variations de la fonctionnalité, c'est la seconde la
plus utilisée, pourquoi ? Pour telle raison car nous avons interrogé nos
utilisateurs. Et une capacité à ne pas aller trop loin, à ne pas trop
investir avant d'obtenir ces mesures et l'apprentissage lié à votre
action, livraison, etc. Une capacité à arrêter les pistes qui s'avèrent
décevantes. Donc oui un niveau très lié à *Lean Startup* et *Design
Thinking*. Ici on sait **optimiser la valeur**. On dit que \~10% des
équipes agiles atteignent ce niveau.

### 4ème étoile ?

Souvent le besoin de générer des éléments autonomes faisant sens
porteurs de valeur a nécessité d'avoir des équipes autonomes et
pluridisciplinaires. On a souvent cassé les silos pour recomposer les
équipes. Cela a été accentué quand on a voulu optimiser la valeur. On a
constitué des équipes autour d'un objectif porteur de sens : par exemple
*non pas nous sommes l'équipe de mise en production, mais nous sommes
l'équipe qui gère toute la relation client*. Autonomie,
pluridisciplinarité, sens, cadre : cela a fait germer des modèles comme
l'[holacratie, la sociocratie](/tag/holacratie.html), les *feature
teams* à la Spotify, etc.

Que votre modèle soit à plat ou pas ce n'est pas ma question, même si le
fait qu'il soit *assez plat* induit qu'il n'y a pas de besoin de
validation pyramidale, que l'**autorisation** est implicite, et donc
l'**autonomie**. Est-ce que votre modèle donne de l'autonomie et est
porteur d'un sens qui permet d'avoir un focus valeur ? Est-ce que chaque
question est donnée avec une perspective sur l'ensemble de
l'organisation ? Voilà les questions pour ce niveau quatre. Ici on sait
*optimiser le système* pour répondre encore mieux aux niveaux
sous-jacents. On dit que \~3% des équipes agiles atteignent ce niveau.

## Dans plusieurs lieux

Est-ce que je peux être dans plusieurs lieux ? Sans pour autant avoir le
sentiment d'avoir achevé réellement un niveau ? Oui c'est tout à fait
possible. C'est cependant dommage de ne pas avoir été assez loin dans la
complétion d'un niveau car vous n'en tirez du coup peut-être pas
réellement avantage.

Durant le [raid agile](http://raidagile.fr) avec [Claude Aubry](http://www.aubryconseil.com), nous réalisons un [atelier autour
de Agile Fluency (inspiré par Agile42)](http://blog.crisp.se/2015/12/15/peterantman/fluent-at-agile-visualizing-your-way-of-working)
que nous avons un peu adapté et rebaptisé [we're gonna groove, nous en parlons là](/2016/03/were-gonna-groove/). Nous balayons les pratiques
des participants ou abordés durant le raid et nous essayons de les
classifier par niveau mais aussi par acquisition (à acquérir, acquise,
en cours d'acquisition, non désirée). Cela permet de visualiser (dans un
Kanban) une cartographie des pratiques et de mettre en évidence les
investissements que l'on souhaite réaliser et où.

Et moi j'ai deux ou trois étoiles (je ne sais plus) au ski, et je me
régale, pas la peine d'aller chercher le chamois. Rappelez vous de
l'idée qu'il vous faut choisir une bonne destination, mais pas
nécessairement la plus "haute" : [Agile Fluency](/tag/fluency.html), une
perspective.

## Et Open Agile Adoption ? aka OpenSpace Agility


Et [Open Agile Adoption](/tag/openadoption.html) dans tout cela ? Agile
Open Adoption, que Dan Mezick appelle depuis [OpenSpace
Agility](http://openspaceagility.com/), n'est pas un niveau, ni une
cible, mais une façon de conduire le changement vers tel ou tel lieu.
Dan Mezick reviendra nous parler de OpenSpace Agility en France cet
automne, je vous en dis plus dès que possible.

