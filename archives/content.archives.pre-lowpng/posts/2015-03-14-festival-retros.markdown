---
date: 2015-03-14T00:00:00Z
slug: festival-de-retrospectives
tags: ['retrospective','scrum','raidagile']
title: Festival de rétrospectives
---

![Quiz sur le Mont Brion (Raid Agile)](/images/2015/03/festival-retrospectives.jpg)

Nous avons profité du [raid agile \#2](http://raidagile.fr/raid2.html)
pour pratiquer 6 formats de rétrospectives. Les voici. Mais auparavant
quelques points : l'idée d'une rétrospective est de s'améliorer :
amélioration continue, émergence de nouvelles approches, etc. Les
rétrospectives sont des moments d'analyse, d'introspection et de
réflexion sur un plan d'action. On peut décider d'essayer de résoudre un
point de blocage, comme de propager une bonne pratique. Il est souvent
nécessaire de renouveler, désaxer les points de vue pour réfléchir
différemment. C'est pourquoi il est toujours intéressant de prendre un
référentiel différent pour penser différemment. A l'occasion de ce raid
nous avons pu manipuler 6 formats : 2 célèbres (l'*étoile de mer*, le
*speedboat*), 2 axes particuliers (Ishikawa, Gyshido) auquel j'ajouterai
une 3ème piste (les niveaux logiques de Dilts), et deux imaginés par
[Clodio](http://www.aubryconseil.com/) spécialement pour ces raids en
Cévennes (la rétro-châtaigne, la rétro-glandouille). Voici donc les 7
formats de rétrospectives :

## L'étoile de mer

Pour l'étoile de mer vous divisez un espace en cinq parties :
**commencer à**, **arrêter de**, **plus de**, **moins de**, **continuer
à**. Vous laissez les participants de la rétrospective placer leurs
post-its selon ces thèmes. Vous définissez sur quoi creuser à l'aide
d'un *dot voting*. Simple, efficace.

-   COMMENCER A
-   ARRETER DE
-   PLUS DE
-   MOINS DE
-   CONTINUER A

Voici une maquette de l'étoile de mer :

![Maquette étoile de mer "Peetic"](/images/2012/11/retrospective2.png)

Et lors d'une "flash-rétrospective" (3 équipes 16 participants 5
minutes) durant un jeu multi-équipe de [Claudio](http://www.aubryconseil.com/).

![Etoile de mer - Raid Agile](/images/2015/03/etoiledemer.jpg)

## Le *speedboat*

Pour le *speedboat*, que j'avais longtemps laissé de côté car -- je ne
sais pour quelle raison -- j'oubliais l'île, île que Richard H. m'a
remis en tête. Merci. Car l'île donne tout son sens à ce format. Quatre
espaces, dont trois à remplir : l'**île** c'est la **destination**, la
**cible** ; le **bateau** c'est l'équipe ; le **vent**, ce qui pousse le
bateau vers la destination et enfin les **ancres**, ce qui freine le
bateau. Vous laissez les participants de la rétrospective placer leurs
post-its dans ces espaces. Ils peuvent aussi dessiner des choses : des
bouteilles à la mer, des requins, des personnes sur des canots de
sauvetage à côté du bateau, une personne qui fait du ski-nautique, etc.
Vous définissez sur quoi creuser à l'aide d'un *dot voting*. Simple,
efficace.

-   ILE : destination, cible
-   BATEAU : vous, l'équipe (qui participe et comment ?)
-   VENT : ce qui vous aide, vous pousse vers la destination
-   ANCRES : ce qui vous freine, vous ralentit

Et en voici un en fin de [raid](http://raidagile.fr).

![Speedboat - Raid Agile](/images/2015/03/speedboat.jpg)

Pour tout dire je me sers désormais de ce format pour faire des
"audits", "rétrospectives", "retour d'expérience", etc. avec le template
ci dessous :

![Speedboat](/images/2015/03/speedboat2.jpg)

## Ishikawa

En cherchant dans mon passé, je me suis dit que l'on pouvait utiliser
les thèmes d'un diagramme *Ishikawa* (vous savez les arêtes de poisson)
pour aborder une rétrospective différemment, notamment des questions
souvent clefs mais souvent évacuées sur le matériel, et l'environnement,
les espaces de travail, etc. Dans cette rétrospective cinq espaces à
remplir :

-   PERSONNE : relation, communication personnelle, bien-être, etc.
-   PROCESSUS : nos pratiques et façon de travailler, etc.
-   EQUIPEMENT : équipement, matériel, etc.
-   MANAGEMENT : relation, communication boite, vision, valeurs,
    objectifs, cohérence, etc.
-   ENVIRONNEMENT : locaux, lieux, etc.

Ensuite pareil : vous définissez sur quoi creuser à l'aide d'un *dot
voting*.

Diagramme d'Ishikawa :

![Ishikawa](/images/2013/05/ishikawa.png)

Lors d'une "flash-rétrospective" (3 équipes 16 participants 5 minutes)
durant un jeu multi-équipe du [raid](http://raidagile.fr).

![Ishikawa - Raid Agile](/images/2015/03/ishikawa.jpg)

Lors d'une rétrospective [SmartView](http://www.smartview.fr).

![Ishikawa - Smartview](/images/2013/05/retro-smartview.jpg)

## Gyshido

Beaucoup plus desaxé, j'ai aimé l'esprit *on arrête les conneries* du
*Get Your Shit Done* qui nous dit (voir
[gyshido.com](http://gyshido.com/)) : *Focus sans répit*, *Répète
jusqu'à la perfection*, *Pas de baratin*, *Pas de réunion*, *Sois dans
l'action, ne sois pas un boulet*, *Ne sois pas un connard*.

On va donc définir à nouveaux 5 espaces :

-   Les choses chiantes
-   Les choses inutiles
-   Les choses brouillonnes
-   Qui et comment avez vous aidé ? Quelles actions avez vous menées ?
    (*j'ai aidé*)
-   Qui et comment avez vous perturbé ? (*j'ai foiré*)

Lors d'une "flash-rétrospective" (3 équipes 16 participants 5 minutes)
durant un jeu multi-équipe du [raid](http://raidagile.fr).

![Gyshido - Raid Agile](/images/2015/03/gyshido.jpg)

## Niveaux logiques de Dilts

J'ai failli l'utiliser lors du raid agile pour l'essayer une première
fois, mais le contexte ne s'y prétait pas. Pourtant je suis sûr qu'une
rétrospective basée sur les niveaux logiques de Dilts se révélerait
intéressante. 6 espaces ici, au format d'une pyramide (gardez en tête
que généralement les problèmes d'un niveau se résolvent avec le niveau
supérieur !) :

-   Appartenance \[ avec qui, vision\]
-   Identité \[qui, mission\]
-   Valeurs -croyances \[pourquoi, motivation\]
-   Compétences - capacités \[comment, stratégies, savoirs\]
-   Comportements \[quoi, action, réaction\]
-   Environnement \[quand, où, contraintes\]

![Niveaux logiques de Dilts](/images/2015/03/niveaux-logiques-dilts.jpg)

## La rétro-châtaigne

La [rétro-châtaigne](http://www.aubryconseil.com/post/La-retrochataigne)
est un format proposé par [clodio](http://www.aubryconseil.com/) (Claude
Aubry, Scrum & rock'n roll) spécialement créé pour le
[raid](http://raidagile.fr). Ici quatre espaces (ils sont bien décrits
là : la
[rétro-châtaigne](http://www.aubryconseil.com/post/La-retrochataigne)) :

-   AÏE : on se pique : qu'est ce qui fait mal
-   OH : on découvre la châtaigne, les bonnes surprises
-   GRRR : cette pellicule chiante qu'il reste à enlever : la
    frustration
-   MIAM : enfin on peut déguster ce que l'on aime.

![Rétro-châtaigne : raid agile](/images/2015/03/retrochataigne.jpg)

## La rétro-glandouille

Enfin la rétro-glandouille. C'est [Claudio](http://www.aubryconseil.com/)
qui a introduit ce nouveau format dans le dernier
[raid](http://raidagile.fr). Il s'agit de découvrir sur 3 niveaux de
lectures : personnel, petit groupe, équipe complète des espaces et des
possibilités de glandouille, de proscratination. *Grosso modo*, il faut
exprimer quelles opportunités (rétrospective positive) vous avez détecté
pour glandouiller plus, et ainsi être plus performant le reste du temps
(*sustainable pace*, rythme soutenable). Pour en savoir plus, lire
l'article de [claudio](http://www.aubryconseil.com/) sur la
[rétroglandouille](http://www.aubryconseil.com/post/La-retroglandouille).

Lors du [raid agile \#2](http://raidagile.fr/raid2.html) on a pu, grâce
à cette rétrospective, déclencher une pétanque, et une pause dans les
activités de l'après-midi.

-   Comment trouver un moyen de glandouiller sur le plan individuel dans
    ma journée de [raid](http://raidagile.fr).
-   Comment trouver un moyen de glandouiller avec mon groupe dans notre
    journée de [raid](http://raidagile.fr).
-   Comment trouver un moyen de glandouiller avec l'équipe (tout le
    monde) dans notre journée de [raid](http://raidagile.fr).

![Rétro-glandouille: raid agile](/images/2015/03/retroglandouille.jpg)

Naturellement on espère vous faire participer à toutes ces
rétrospectives lors du [Raid Agile \#3](http://raidagile.fr/) (juin
2015).

**Cet article est un complément à celui-ci**, de 2013, [l'art de la
rétrospective](/2013/05/lart-de-la-retrospective/).

**Et de celui-ci** : [2 nouveaux formats de
rétrospective](/2016/04/2-nouveaux-formats-de-retrospective/)

