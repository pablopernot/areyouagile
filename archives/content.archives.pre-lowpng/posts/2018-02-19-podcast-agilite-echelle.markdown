---
date: 2018-02-19T00:00:00Z
slug: agilite-echelle-podcast
tags: [ masse, echelle, scaling, repetabilite, cadre, geste ]
title: Podcast du café craft // agilité à l'échelle
---

Voici le podcast enregistré par Thomas dans son [café craft](http://www.cafe-craft.fr) au début du mois. Je le remercie pour cet échange. N'hésitez pas à me faire du feedback.   

[Agile à l'échelle : Podcast "Café Craft" - "L'agilité à l'échelle"](http://www.cafe-craft.fr/20)

Merci Thomas. 

![Termitières](/images/2018/02/termitieres.jpg)

## Une petite série sur mise à l’échelle (et auto-organisation).

* [Agile à l'échelle : c'est clair comme du cristal, 2013](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)
* [Agile à l'échelle : synchronisation, 2017](/2017/11/agilite-a-grande-echelle-synchronisation/)
* [Agile à l'échelle : pourquoi il faut se méfier de SAFe, 2017](/2017/11/pourquoi-faut-il-se-mefier-de-safe/)
* [Agile à l'échelle : libération de l'auto-organisation, 2017](/2017/12/agile-a-l-echelle-liberation-auto-organisation/) 
* [Agile à l'échelle : équipes et management, 2017](/2017/12/agile-a-l-echelle-equipes-et-management/)
* [Agile à l'échelle : grandir ou massifier](/2018/02/grandir-ou-massifier/)
* [Agile à l'échelle : Podcast "Café Craft" - "L'agilité à l'échelle"](http://www.cafe-craft.fr/20)

Et sinon l'événement que nous organisons en autour de l'agile à l'échelle avec [Dragos Dreptate](http://andwhatif.fr): 

* [VALUE DRIVEN SCALING](http://valuedrivenscaling.com)



