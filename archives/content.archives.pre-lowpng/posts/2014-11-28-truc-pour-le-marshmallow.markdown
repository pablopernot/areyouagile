---
date: 2014-11-28T00:00:00Z
slug: truc-pour-le-marshmallow-challenge
tags: ['atelier','workshop','seriousgame','marshmallow','challenge']
title: Truc pour le marshmallow challenge
---

Petit truc pour le [marshmallow challenge](http://www.marshmallowchallenge.com) que j'utilise depuis
plus d'un an et dont [Anthony](http://acassaigne.info/) a qui j'ai pu le
montrer me dit qu'il faut écrire une broutille dessus (et qu'il l'utilise avec succès et plaisir).

A ne pas lire si vous ne connaissez pas le jeu.

![Sudweb 2012](/images/2014/11/marsh-sudweb.jpg)

Pour avant en savoir plus sur le marshmallow challenge :

 - [marshmallow challenge](http://www.marshmallowchallenge.com)
 - [2011 : Les enfants et le marshmallow challenge](/2011/04/les-enfants-et-le-marshmallow-challenge/)
 - [2012 : Sudweb / Marshmallow Challenge](https://vimeo.com/53396148)

L'un des points clefs du marshmallow challenge c'est -- malheureusement
-- la capacité des équipes à ne pas avoir de véritable indicateur sur
leur création de valeur. Pour mettre cela en exergue j'ai pris
l'habitude de leur demander toutes les deux minutes durant l'atelier
l'état des lieux du projet, avec seulement trois réponses autorisées :
ça va bien (smiley souriant), nous ne savons pas comment va le projet
(point d'interrogation), ça va mal (smiley qui fait la gueule). C'est
moi en tant que grand chef supposé du projet leur demande régulièrement
(toutes les 2mn) de me fournir une et une seule réponse (mais elle peut
naturellement varier durant les 18mn du challenge).

Je prend un bout de tableau blanc et voici ce que cela peut donner
(naturellement aucun participant n'y prête attention) :

![Suivi marshmallow](/images/2014/11/marsh.png)

J'indique aussi sur le diagramme à quel moment l'équipe place vraiment
le marshmallow sur une structure, sans aide (sans tenir par les mains).

Très souvent comme sur la ligne deux on me dit très longtemps que tout
va bien. Ici les trois quart du temps. Pour finalement lamentablement
échouer. J'interroge donc l'équipe : comment ont-ils pu me dire durant
les trois-quarts du projet que tout allait bien ? Nous sommes aux
antipodes d'une méthode sensée protéger de l'échec ou optimiser la
valeur.

Deux raisons sont souvent invoquées : la première est que l'on dit que
tout va bien pour que le grand chef nous laisse tranquille, mais on ne
sait pas vraiment. Comme cela va bien, il nous laisse tranquille.
Malheureusement on observe bien souvent cela "dans la vraie vie". C'est
aller au devant de mésaventures et déceptions : optez pour la
transparence, soyez responsables. C'est un peu ce qui arrive à la ligne
3, l'équipe reste bien longtemps dans un mode "on ne sait pas". Dans la
"vraie vie" cette incertitude (si elle est transparente) sera intenable
et va déclencher des actions.

La seconde, plus terrible, est que l'on confond temps passé à travailler
et création de valeur, sous-entendu : "oui tout va bien, nous
travaillons". Mais nous savons que vous travaillez ! Ce n'est pas une
question à se poser, dans un environnement correct tout le monde à envie
d'avancer, de travailler. Mais ici la vraie question, la question
essentielle est : "comment va le projet : c-a-d où en est la création de
valeur", et le cumul des heures de travail n'est en rien un indicateur
de création de valeur. Vous pourriez travailler très peu et avoir une
idée de génie, comme travailler beaucoup pour tout jeter (comme bien
souvent dans le marshmallow challenge).

Le véritable indicateur de l'état du projet c'est d'avoir à intervalles
réguliers un marshmallow qui tient tout seul.
