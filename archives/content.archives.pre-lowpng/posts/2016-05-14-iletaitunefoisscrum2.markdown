---
date: 2016-05-14T00:00:00Z
slug: il-etait-une-fois-scrum-2
tags: ['scrum','review','revue','retrospective','scrummaster','productowner','daily','dod','definitiondefini', 'backlog', 'userstory', 'userstorymapping', 'test', 'burndown', 'burnup', 'planderelease']
title: Il était une fois scrum 2
---

Voici la suite de cette petite histoire de Scrum, qui je le rappelle est
sans ambition : une façon de raconter Scrum, une variation sur un thème.

Nous avons démarré l'itération lors de notre précédente histoire ([Il
était une fois Scrum \#1](/2016/05/il-etait-une-fois-scrum-1/)). Bref
nous sommes dans la gadoue, au milieu du
[maul](http://www.francerugby.fr/les-regles-du-rugby-maul/) (car c'est
bien d'un [maul](http://www.francerugby.fr/les-regles-du-rugby-maul/)
dont voulaient parler les japonais qui observaient un match de rugby et
faisaient une métaphore avec leur dynamique d'équipe, et pas d'une mếlée
(scrum)).

Durant cette itération le product owner valide au fil de l'eau quand
c'est fini. Fini cela veut dire quoi ? **Fini** c'est défini par la
[définition de
fini](/2016/04/nul-nest-cense-ignorer-la-definition-de-fini/) (elle
porte bien son nom).

En tous cas le product owner valide qu'il a bien obtenu ce qu'il avait
demandé, il ne test pas si cela marche, il valide que c'est bien ce
qu'il avait demandé. Rien ne l'empêche de faire tous les tests qu'il
veut mais l'équipe avec ses différentes compétences a tout testé avant
de proposer ces éléments au product owner. Tester cela veut dire que
tout marche comme prévu, et que cela respecte la *définition de fini*.
Tout roule, cela pourrait être mis en production aux yeux de l'équipe
(ce n'est pas une obligation de mettre en production, c'est
potentiellement une opportunité). Mais comme c'est le product owner qui
porte le quoi et le pourquoi et qui est à l'origine de la demande c'est
lui qui porte la validation finale : *oui c'est bien ce que j'ai
demandé*.

Durant ces jours de travaux on va se rendre compte que l'on a oublié des
tâches : on va les ajouter au mur de post-its (ou en retirer des
obsolètes). Les courbes (*burndown*...) vont donc refléter ces
changements. Pour les burndowns je demande à ce que l'on découpe en
tâches, et idéalement ces tâches sont petites. L'intérêt n'est pas dans
vraiment dans l'estimation, mais dans la clarification et dans la
conceptualisation du travail à réaliser. Ensuite avec les *burndowns* on
sait où sur ce chemin conceptualisé on se trouve. Il est primordial de
toujours montrer l'état des lieux avec transparence, sans pipoter (de
toutes les manières, cela se verrait). On n'incriminera pas l'équipe :
que les choses changent c'est normal. Qu'il y ait des bonnes ou de
mauvaises surprises c'est normal, mais ce que l'on veut c'est savoir :
pour aider, pour arbitrer.

D'ailleurs pour les plus butés qui continuent à consolider les chiffres,
si vous consolidez les tâches des *user stories* vous observerez que
bizarrement les éléments jugés les plus conséquents sont souvent ceux
avec le moins de tâches car c'est normal on les maîtrise mal ! Et à
l'inverse les éléments jugés les plus simples sont ceux souvent avec le
plus de tâches et c'est aussi normal car on sait les découper sans souci
! La découpe en tâches que je recommande à pour objectif non pas de de
mesurer mais de penser le travail à faire.

Au fait, pas de *business value* sur les *user stories*, c'est
intenable. Vous devez scinder une user story vous divisez par deux votre
*business value* ou pas ? C'est le genre de questions qui font que je
n'ai jamais réussi à maintenir une *business value* unitaire par *user
story*. Un vrai casse tête chinois, et ce n'est pas franchement utile.
Donc pas de *business value* sur les *user stories*. Vous pouvez
représenter la *business value* a un niveau plus macro : epic/épopée,
fonctionnalités/feature. La valeur aux yeux de product owner, cela sera
sa priorisation.

Tous les jours un point au sein de l'équipe un point a lieu (le
scrummaster comme le product owner n'ont pas besoin d'y participer, mais
le scrummaster en tant que garant de la méthode doit s'assurer de son
bon déroulement, et même potentiellement entendre les obstacles qui
gènent l'équipe, bref, c'est paradoxal il est là sans être là). Donc
tous les jours au sein de l'équipe ce point a lieu : au même endroit, à
la même heure, maximum 15mn (debout pour s'en rappeler), pour partager
l'état des lieux entre membres de l'équipe. C'est le **daily standup**.

![Yellow Story](/images/2016/05/yellowstory.jpg)

Une pratique que j'aime bien c'est de demander à l'équipe un point rouge
ou vert représentatif de leur confiance sur chaque élément tous les
jours. Il est vert (on est confiant) ou rouge (on est pas confiant).
Cela donne une tendance que tout le monde apprécie. Orange interdit.
Normalement rien de rouge le premier jour (ou alors l'équipe n'a pas
vraiment choisi le scope de l'itération...).

Après l'équipe est autonome et auto-organisée ; d'ailleurs je demande à
ne pas écrire de noms sur les tâches. Les tâches appartiennent à
l'équipe, pas a des personnes. Je ne veux pas entendre un Laurent
(exemple) me dire "Moi j'ai fini, mais Julien est un boulet et il rame".
Primo, lors du sprint planning il s'agissait d'une projection d'équipe,
donc je ne comprends pas cette distinction entre Laurent et Julien, et
donc ils doivent se débrouiller. Deuxio, il faut dans un premier temps
que l'équipe apprennent à se gérer elle même donc généralement je
réponds : que pense Julien de cela quand tu lui as dit ? Ne vous
inquiétez pas l'auto-organisation de l'équipe est quelque chose qui
marche très très souvent très bien. Comme tous les systèmes vivants
l'être humain sait très bien s'auto-organiser. Naturellement on observe
les étapes classiques d'une équipe quand elle se constitue ("forming,
storming, norming, performing", voir
[Tuckman](https://en.wikipedia.org/wiki/Tuckman's_stages_of_group_development)),
et il faut 3 (voire 4 itérations) de calibrage, mais dans la grande
majorité des cas cela roule. Jusqu'à ce que vous décidiez de changer
l'équipe, et le calibrage doit reprendre. Si le problème persiste au
délà de trois ou quatre itérations, consultez.

Ce calibrage de 3,4 itérations est aussi très perceptible concernant la
relation et la communication entre le product owner et l'équipe,
concernant la qualité des engagements des estimations des projections,
etc. Et donc là aussi si vous changez l'équipe, il faudra se calibrer à
nouveau durant 3,4 itérations pour la planification (par exemple).

Donc généralement le product owner passe la moitié du temps qu'il
octroit au projet/produit a accompagné l'équipe dans sa réalisation : il
lui donne du feedback, des informations, valide. Et l'autre moitié de
son temps à anticiper la suite : il va rencontrer les autres product
owner, product managers, les sponsors, les utilisateurs finaux, les gens
qui influencent ou qui seront influencés par le produit/projet. Avec
toutes ces informations il précise son backlog, son expression du
besoin, le change, le réoriente.

Les codeurs codent, testent, documentent, discutent, épaulent le product
owner pour ses user stories, osent des choses hors de leurs périmètres
habituels. Les designers *designent*, codent, testent, documentent,
discutent, épaulent le product owner pour ses user stories,etc. Les
business analyst épaulent particulièrement le product owner, testent,
documentent, discutent, etc. etc etc. Mais tout ça c'est l'équipe, ils
sont auto-organisés. Ils veulent s'amuser à inverser les rôles, c'est
leur tambouille, c'est comme ils veulent.

Si le besoin nécessite une attention particulière : un sujet compliqué
mal maîtrisé, des adhérences avec d'autres entités par forcément très
limpides, etc. On va faire des réunions d'affinage, du [backlog
grooming](http://www.theobserverself.com/pratiques/corvee-backlog-grooming/).
Elles permettent d'anticiper les soucis, de préparer l'expression du
besoin pour ne pas avoir des sprint planning en souffrance, elles
permettent d'enrichir le propos du product owner.

-   En savoir plus sur les niveaux de constitution d'une équipe par
    [Tuckman](https://en.wikipedia.org/wiki/Tuckman's_stages_of_group_development)
-   Le [backlog
    grooming](http://www.theobserverself.com/pratiques/corvee-backlog-grooming/)
-   Une [vidéo](https://www.youtube.com/watch?v=0ZjdrYLKOdM) sur un
    "mur" scrum de deux semaines
-   Le [scrummaster que vous avez toujours rêvé
    d'être](https://www.youtube.com/watch?v=P6v-I9VvTq4), avec Jeff
    Sutherland dans le rôle du DG.
-   Pourquoi j'aime que les [user stories restent métiers et pas
    techniques](/2013/03/pourquoi-une-user-story-technique-est-un-aveu-dechec/),
    et la [partie
    2](/2013/03/pourquoi-une-user-story-technique-est-un-aveu-dechec-2/)
    de l'article.

Et la semaine avance, les tâches (sur les post-its) avancent, des user
stories, éléments fonctionnels, sont validés par le product owner.

Naturellement toute la validation se déroule sur la plate-forme
d'intégration : un serveur propre, sans anomalie (si il y a une anomalie
la user story n'est pas validée et remise à l'équipe).

Donc oui : il est juste impossible de valider une user story avec un
bug, une anomalie. Soit l'anomalie est mineure, cosmétique, et il est
rapide de la corriger, donc pas de raison de la valider avec cette
anomalie. Soit l'anomalie est importante et donc pas de raison de la
valider avec cette anomalie. Oui les agilistes sont intransigeants sur
ce point (ou devraient l'être). Nous ne voulons pas de bug, aucun. Ce
n'est pas que nous ne faisons pas de bug, nous en faisons autant voire
plus que les autres (car nous aimons essayer), mais nous n'acceptons pas
de valider comme fini quelque chose avec des bugs, nous n'acceptons de
sortir quelque chose avec des bugs. Ce n'est pas une religion ! Nous ne
sommes pas les chevaliers blancs du bug ! C'est tout simplement que tous
ces bugs coûtent plus cher et plus de temps. Nous ne voulons pas de bugs
car nous voulons gagner du temps et de l'argent. Tout le monde a déjà
vécu, a observé, que un bug qui se corrige dans l'instant va coûter deux
minutes, deux heures ou deux jours, et qu'un bug qui se corrige plus
tard, va coûter le double, le triple, le quintuple... Et le 8ème bug
c'est un nouveau bug où il est lié au 4ème ? ... bref une vraie gadoue.

Ensuite comme nous travaillons itérativement impossible de demander au
product owner de valider toute la première itération la première fois,
puis la première et la seconde la deuxième fois, puis la première, la
seconde, la troisième... cela deviendrait fou.

Impossible aussi d'imaginer valider et tester durant une itération+1 le
résultat de l'itération, avec une autre équipe, tout en commençant une
nouvelle itération. Cela ne marche pas, c'est une fuite en avant
suicidaire. Dès que l'on détecte une anomalie, et cela sera le cas sans
validation ni test au préalable, comment la traiter ? Interrompre
l'équipe qui a commencé l'itération suivante ? Impossible on se prend
systématiquement les pieds dans le tapis. Une seule pratique : quand
c'est fini, c'est fini, un seul rythme, une lecture clarifiée de l'état
des lieux.

Il est donc aussi obligatoire que ce qui est validé reste en bon état
constamment. Pour cela nous faisons tourner régulièrement (très
régulièrement : plusieurs fois par jours), des tests automatisés à tous
les niveaux : unitaire, technique, métiers, d'intégration, de
performances, d'interface, etc. Cela demande donc des serveurs (oui
Agile coûte plus cher en infrastructure, mais compte tenu de la qualité
et de son optimisation de la valeur sont coût global est meilleur
n'oubliez pas, je n'y crois pas : je l'observe). La vraie difficulté
dans les tests automatisés (ils sont faits par l'équipe naturellement !)
c'est d'abord d'apprendre à les réaliser, à en intégrer l'importance,
mais surtout les jeux de données adéquats, simili prod idéalement (sinon
comment vraiment développer ?). Jeux de données que l'on va déployer,
modifier, supprimer, re-déployer, constamment... Une vrai réflexion doit
avoir lieu sur le ratio effort/valeur, mais voilà la direction.

Les trois épisodes de la série et le guide de survie :

-   [Il était une fois scrum \#1](/2016/05/il-etait-une-fois-scrum-1/)
-   [Il était une fois scrum \#2](/2016/05/il-etait-une-fois-scrum-2/)
-   [Il était une fois scrum \#3](/2016/05/il-etait-une-fois-scrum-3/)
-   [Guide de survie à
    l'agilité](/pdf/guiderapide.pdf) (glossaire,
    descriptions courtes des rituels, bibliographie, etc.).

## Un burndown

A cette époque (2010), je comptais encore les points par tâche (en quart
de journée : 1=0,25, soit normalement 4 max, car une tâche ne devrait
pas excéder un jour, mais j'acceptais facilement 8, jusqu'à deux jours).
Je ne compte plus les points par tâche, cela n'a pas d'intérêt, je
compte juste le nombre de tâches pour les burndowns, pour voir si on
avance sur le chemin prévu, où si il faut agir, prendre une décision,
etc. Je demande simplement à ce que les tâches restent petites "moins de
deux jours".

Cliquez sur le burndown pour agrandir. Attention sans *burnup* nous
pourrions nous faire piéger : disons que nous avons dix tâches par *user
story*. Elles avancent toutes sauf une par *user story*. La ligne serait
presque parfaite sauf qu'à la fin nous n'aurions...rien. Or, pas de
valeur dans les tâches (qui font parties du *comment*). La valeur réside
dans les *user stories* (c'est pourquoi aussi j'aime que les [user
stories restent métiers et pas
techniques](/2013/03/pourquoi-une-user-story-technique-est-un-aveu-dechec/),
et la [partie
2](/2013/03/pourquoi-une-user-story-technique-est-un-aveu-dechec-2/) de
l'article). Un *burnup* de validation des user stories m'aurait montré
que l'on ne valide rien au fil de l'eau.

![Burndown 2](/images/2016/05/burndown-2.jpg)

## Un autre plan de release

![Plan de release 2](/images/2016/05/plan-de-release-2.jpg)

## Un bout de "radiateur d'information"

C'est comme cela que l'on appelle les murs "chauds" agile : il diffuse
de l'information. Ici on chambre le coach en l'associant avec le serpent
KA (et on lui dit aussi qu'on va le transformer en sac si besoin),
c'était bien moi. Cela date de 2011 ? On y voit un plan de release, une
liste d'actions post-rétro, une définition de fini, un burndown, un
burnup ? (Cliquez pour agrandir)

![KA](/images/2016/05/ka-small.jpg)

## Management visuel : petit musée des horreurs

Savoir être explicite ! (c'est de l'ironie)

![Etre explicite](/images/2016/05/horror-1-small.jpg)

Voilà CA c'est explicite ! (là aussi c'est ironique)

![Etre explicite](/images/2016/05/explicite-1-small.jpg)

Sarcasme (après l'ironie)

![Sarcasme](/images/2016/05/sarcasm.jpg)

Mur mou, méfiez vous du scotch de peinture : les premiers jours il ne
tient pas, les suivants il arrache le mur. Mais comme je le dis souvent
: un projet/produit raté plusieurs milliers d'euros, un mur à faire ou à
refaire : deux à cinq mille euros.

![Mur mou](/images/2016/05/mur-mou.jpg)

Des champignons sur le mur ? Ok pour avoir des murs un peu en vrac : si
ils sont trop propres personnes n'osent y toucher et deviennent
fossiles, mais tout de même un peu de respect, si vos murs sont sexy ils
intéresseront. Essayez de faire de beaux cadres.

![Champignons](/images/2016/05/champignons-small.jpg)
