---
date: 2009-09-05T00:00:00Z
slug: la-contractualisation-des-projets-scrum-avec-les-ssii
tags: ['contrat','ssii','esn']
title: La contractualisation des projets Scrum avec les SSII
---

En abordant ce sujet je me dis qu'il va falloir du temps pour voir
émerger une vraie réponse et que probablement plusieurs, voire de
nombreux billets seront nécessaires. Actuellement toutes les SSII
s'interrogent sur comment surfer sur la "vague agile" mais se heurtent a
un problème crucial : la contractualisation. Au passage, même si il
consacre un [chapitre](http://www.amazon.fr/Agile-Project-Management-Scrum-Schwaber/dp/073561993X),
Ken Schwaber évacue rapidement la question sans vraiment y apporter de
réponse.

Historiquement toute la relation projet entre un client et une SSII se
base sur un engagement de résultat -le forfait-  (je ne parle pas ici
des régies, c'est autre chose). L'avant-vente est un moment éminément
stratégique où l'on fixe définitivement le périmètre de cet engagement,
notamment financier. De cela découle toutes une séries d'aberrations,
par exemples :

-   La SSI remet bien 100% de son engagement mais le client et elle se
    sont mal compris et seul 30% des fonctionnalités seront vraiment
    utilisées...
-   Le client voudrait signer avec vous car vous êtes le seul à avoir
    vraiment compris son besoin ! mais pas de chance du coup vous êtes
    30% plus cher que les concurrents et les achats ont éliminés votre
    candidature. Peu importe si par la suite le produit acheté ne
    correspondra pas, ou si le client devra faire face à une multitudes
    d'avenants...
-   Votre client demande une interface de gestion des utilisateurs, vous
    imaginez 3 ou 4 gabarits de gestion et les fonctionnalités
    associées. Erreur, il sous-entendait simplement l'injection d'un
    script SQL dans la base... Du coup vous êtes bien trop cher
    monsieur. Vous étiez pourtant le bon interlocuteur, mais la
    rédaction du cahier des charges a troublé le jeu...

Des fois, et heureusement beaucoup plus souvent qu'on ne l'imagine, si
la phase d'avant vente n'a pas été fatale tout le monde a assez de
maturité pour réorganiser les développements, les fonctionnalités et les
objectifs au sein de cette chape de plomb représentée par cet engagement
de résultat qu'est le forfait.

Je ne vais pas revenir dessus mais c'est clairement en partie pour
palier à ces problèmes que les méthodes Agiles rencontrent un tel
succès. La transparence et l'implication étant les maitres mots on évite
de nombreux écueils, on se focalise sur le ROI, etc etc. Très bien. mais
comment facturer cela ? Comment expliquer à votre client : à la fin de
notre collaboration vous ***devriez*** avoir ceci et cela, au lieu du
beaucoup plus rassurant : à la fin de notre collaboration vous
***aurez***ceci et cela. Même si, et j'en suis convaincu, ce que vous
***devriez*** avoir aura bien plus de valeur que ce que vous
***aurez***, difficile de faire avaler la pilule ! Surtout avec certains
préjugées autour des SSII, et la réalité qui veut qu'il s'agit
d'entreprises comme les autres qui vivent de leurs bénéfices.

J'évacue pour l'instant ici la question de la responsabilité (autre
complication de la contractualisation Scrum qui veut que la
responsabilité soit portée par l'équipe et par le *product owner*-le
client-). Allez expliquer à votre client que le projet a échoué à cause
du *product owner*, c'est à dire lui-même... J'évacue aussi pour
l'instant les questions de contractualisation des appels d'offres
publiques...

Bref comment contractualiser un projet Scrum (a équivalence de la
contractualisation d'un projet classique forfait) entre une SSII et un
prospect ?

Une méthode émerge actuellement. Ce n'est qu'une étape et elle ne répond
que partiellement au problème. Elle semble fédérer cependant un peu tous
les acteurs. J'en trouve un écho dans cet
[article](http://www.journaldunet.com/developpeur/temoignage/temoignage/258101/les-methodes-agiles-donnent-du-rythme-au-developpement-via-les-livraisons-successives-a-courte-echeance/)
(merci Christophe).  Il s'agit de réaliser un ***Sprint 0***, ou une
étude initiale qui va permettre :

-   définir le périmètre fonctionnel clairement : sans quiproquo
-   définir la solution proposée technique/architecture
-   organiser un calendrier des *sprints* et la liste des
    fonctionnalités associées à chacun
-   de faire disparaître toutes les zones opaques

L'objectif de ce "sprint 0" est d'obtenir une évaluation suffisamment
précise qu'elle puisse donner assez de garanties sur le *devriez avoir
en fin de projet*. Etude qui devrait d'ailleurs définir ce que l'on
appelle "fin du projet"... Bref, malgré cette étude, et connaissant le
lourd historique commercial qui pré-existe, vous avez toujours un gros
risque d'entendre dire : "bon puisque nous avons maintenant assez de
visibilité nous pouvons contractualisé un engagement de résultat de type
forfait ? non ? ". Là, a vous de ré-expliquer toute la pertinence
d'utiliser les méthodes agiles et d'organiser une facturation par cycle
de 2 ou 3 *sprints*.

Enfin on peut estimer *grosso-modo* que ce ***sprint 0*** nécessite une
charge de 5 à 10j (certains vont s'arracher les cheveux que je puisse
donner ainsi, ex-nihilo, dans le vide, des chiffres...). La réalisation
de ce ***sprint 0*** sous-entend naturellement aussi que vous avez déjà
gagné une première étape commerciale.

Rien n'est simple avec Scrum, sauf Scrum lui même.
