﻿---
date: 2019-10-12
slug: satane-vuca-colloque-psf
tags: [entreprise, coaching, supervision, vuca, colloque, psf]
title: Satané VUCA et colloque PSF
--- 

Le réseau a ses raisons que la raison ignore. [Laurent](https://twitter.com/bangalaurent) a pu récemment me mettre en contact avec deux superviseuses du réseau PSF pour me substituer à lui lors de leur conférence annuelle. C'est avec beaucoup de curiosité que j'ai entrepris cette démarche. La curiosité semblait partagée de l'autre côté. L'idée étant de présenter lors de deux sessions comment on peut superviser un groupe de coachs agiles avec ma double posture : coach organisationnel, et directeur général chez beNext.    

C'était un colloque de superviseurs. Le [colloque PSF](https://www.linkedin.com/company/profesional-supervisors-federation/). Sans surprise j'ai croisé des personnes passionnantes, impliquées, les pieds ancrés dans la réalité, et certaines personnes atterrantes, aux antipodes de ce qu'elles prônent.

## Superviser des coachs agiles ? 

Chez beNext nous réalisons toutes les deux semaines une journée qui comprend de l'intervision. Cette journée nous permet de traiter moult sujets, tensions, célébrations, de synchroniser tout le monde sur l'actualité. Toutes ces conversations peuvent découler sur des séances d'échanges et d'intervision. J'ai fait part de l'importance d'être légitime pour être écouté dans ce cénacle. J'ai fait part de l'importance de la confrontation (à laquelle je crois beaucoup, une confrontation saine). J'ai fait part de la qualité des coachs agiles a être *crossover*, pluridisciplinaires. C'est leur force, mais aussi leur faiblesse, car il y a beaucoup d'apprentis sorciers, comme dans tout le coaching, pas si bien encadré (les fédérations de coaching professionnelles sont loin d'avoir gagné mon estime). J'ai fait part de l'importance d'être vulnérable, de montrer ses lacunes et échecs, de montrer ces essais, réussis comme infructueux. 

Chez beNext nous proposons aussi des *triades*, des points individuels en tête à tête avec une tierce personne qui vient en invitation de la personne sujet, pour intégrer un regard différent, des conversations plus "méta". 

Chez beNext nous proposons aussi des *coach corner* : c'est un espace ouvert a qui veut pour un coaching comme il le souhaite (pro, perso, etc.). 

Merci à [Élisabeth Georges](https://www.linkedin.com/in/elisabeth-georges-05bb317) et [Éveline Forlot](https://www.linkedin.com/in/eveline-forlot-369a1618/) de m'avoir accompagné dans cette réflexion. Élisabeth m'a donné une clef sur le côté *crossover* en me disant que l'on répondait à tous les niveaux logiques. Elle m'a aussi éclairé quand je lui parlais d'accompagner des organisations, de nous "centrer sur l’ensemble du système supervisé, dans une vision globale, plutôt que sur un problème ponctuel" et de la difficulté pour certains de savoir qui est le client. Pour résumer, probablement à la serpe, le client est le payeur. Ainsi en coaching organisationnel, et plus globalement en coaching agile le client est l'organisation. Ce qu'elle appelle le coaching systémique. Pour le coaching humaniste (il s'agit de ses mots), en coaching individuel, le client est l'individu.  

À part cette conversation autour de la supervision de coachs agiles, je retiens deux sujets que j'avais pu déjà évoquer de-ci de-là. 

## Des décisions plus fréquentes

Dans ce monde VUCA (je ne me fais pas à cet acronyme, *volatile, incertain, complexe et ambiguë*) les décisions sont plus nombreuses et plus fréquentes. On avance pas à pas, il faut décider plus souvent. Et observer, inspecter, apprendre. Décider plus souvent c'est aussi s'autoriser à décider plus facilement (plus d'autonomie). 

## Dire cela fait 15 ans ou se poser les bonnes questions 

On me dit beaucoup récemment : "cela fait 15 ans..." ou "cela fait 25 ans...". Je me suis amusé à dire lors d'un séminaire : "vous connaissez tous cette citation du président de Kodak : *cela fait 25 ans...* (pause) à moins qu'il ne s'agisse de celui de Thomas Cook ?". Tout le monde a compris. 

On vit dans un monde compliqué où imposer des habitudes ou un savoir que l'on pense acquis ("cela fait 25 ans") peut se révéler un piège, car il ne l'est plus acquis, justement. C'est un monde changeant, nous venons de nous dire, cela semble alors ironique d'en appeler à ses habitudes. Pour autant seul le temps semble juge d'une bonne émergence (l'effet Lindy du "skin in the game" de Nassim Taleb). C'est quand le temps est passé que l'émergence a validé sa qualité. Entre valider avec le temps (et probablement étoffer) quelque chose d'émergent, et rompre avec ses habitudes bâties (et peut-être étoffées aussi) avec le temps la lecture n'est pas simple pour tout le monde. Pourtant pas de répétition à l'identique dans l'émergence.   

Puisqu'il est question de temps, que savons-nous à l'instant t ? Que pourrions-nous savoir ? 

1° qui est-on ? Que veut-on ? Nous pouvons le savoir. 

2° est-ce que nos actions portent leurs fruits et quels fruits ?  Nous pouvons le savoir.

Les deux extrêmes de la pyramide de Dilts me fait remarquer Élisabeth Georges. Tout le travail est de la traverser. Si on la traverse bien sans dissonance, on obtient un environnement adéquat.

![PSF](/images/2019/10/psf.jpg)
