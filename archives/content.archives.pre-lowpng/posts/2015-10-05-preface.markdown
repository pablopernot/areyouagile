---
date: 2015-10-05T00:00:00Z
slug: preface-scrum-4eme-edition
tags: ['livre','scrum']
title: Préface Scrum 4ème édition
---

Dans deux jours sort la nouvelle édition de
"[Scrum](http://www.amazon.fr/Scrum-guide-pratique-m%C3%A9thode-populaire/dp/2100738747/)"
de [Claude Aubry](http://www.aubryconseil.com/). Édition pour laquelle
Claude m'a fait l'honneur de me demander une préface. Probablement trop
de photos compromettantes réalisées lors des "[raid
agile](http://raidagile.fr)". Comme l'avant-propos le confirme, Scrum et
l'Agile plus globalement ne cessent d'évoluer. Et comme je le dis dans
la préface, Claude est un explorateur. Ce n'est pas donc une nouvelle
édition pour une nouvelle édition, mais bien le journal de la mutation
constante de nos pratiques.

Voici la préface, j'espère qu'elle vous invitera à acheter le
[livre](http://www.amazon.fr/Scrum-guide-pratique-m%C3%A9thode-populaire/dp/2100738747/)
:

## Préface "Scrum", de Claude Aubry, 4ème édition

*Scrum* est un drôle de truc. Une petite chose devenue énorme. Quelques
paragraphes qui révolutionnent nos méthodes de travail, voire nos vies.
Quelques paragraphes… Initialement, *Scrum* est une méthode pour
réaliser des projets dans des environnements complexes, incertains.
Cette petite idée a fait son chemin : elle a détrôné l’aristocratie
méthodologique en place (ouste les PMI, CMMi, RUP, etc.). Les géants aux
pieds d’argile se sont effondrés. Comme si on avait ouvert une fenêtre
dans une pièce fermée depuis bien trop longtemps. Aidé en cela par
l’époque, cette époque moderne et incertaine à la concurrence exacerbée
où toutes les énergies comptent. J’en connais qui se marrent bien : ils
ont soulevé avec *Scrum* le voile sur tous les petits mensonges de nos
organisations.

C’est drôle aussi, le petit poucet est devenu l’ogre. Scrum est comme un
trou noir qui avale tout ce qui passe à côté de lui. Le livre de Claude
ne parle pas que de *Scrum*, mais de tout ce que *Scrum* symbolise et a
embarqué avec lui dans sa proposition d’une organisation ou d’un projet
agile. Et c’est beaucoup plus que les quelques paragraphes initiaux.
Sans complexe *Scrum* a emprunté là où était la valeur. Dans notre bon
sens, dans les autres méthodes agiles, et autour encore quand cela en
valait la peine. *Extreme programming*, c’est comme l’oncle rustre et
frustré. Il ronchonne dans son coin, *Scrum* ne serait qu’un vendu qui
aurait oublié certaines de ses valeurs. *Extreme programming* est
ombrageux et exclusif. Mais il oublie que lui et *Scrum* sont de la même
famille, et que leurs discours sont proches. *Scrum* est juste un beau
parleur, et cela génère des jalousies. De fait, Scrum rayonne
aujourd’hui. J’en connais qui se marrent bien quand ils découvrent que
*Scrum* est devenu la méthode de référence.

C’est drôle, car les gens se trompent sur *Scrum*. Ils le pensent
simple, souple et facile. Bien compris et mis en œuvre il se révèle
complexe, rigoureux et ardu. C’est tout le paradoxe de nos méthodes :
elles sont comme nos organisations ou projets, complexes. Il faut les
adapter, se focaliser sur la valeur, s’améliorer, essayer, apprendre par
l’échec. Ce paradoxe est partout chez *Scrum*. Comme évoqué, la petite
méthode est devenue grande, David, Goliath. L’approche alternative est
devenue la référence. On se jette sur elle car on a bien compris qu’elle
était la mieux armée pour lutter dans le monde actuel. Mais sa légèreté
est trompeuse, car c’est bien une lame tranchante, un outil aiguisé pour
appliquer les principes et valeurs de l’agile. Beaucoup de gens se
trompent de sens et l’attrapent par la lame !

Le succès de cette méthode pourrait aussi la rendre obèse et caduque. En
cela le livre de Claude que vous tenez entre les mains est une sorte
d’antidote, une base saine. On commence d’ailleurs à payer le retour de
flamme de l’agilité : de trop nombreuses incompréhensions, récupérations
font la promotion d’un agile complètement dévoyé. C’est exactement ce
qui est arrivé au Lean des années 1990. Donc pour faire sérieux, pour ne
pas dire « agile » qui fait peur au système en place (mais oui, car il
décrète la fin du système précédent), on parle beaucoup de Lean (Un
autre grand oncle, mais bien plus vieux). Ce n’est pas une bonne raison.
C’est encore une fuite. D’autant plus que le Lean que l’on évoque est
archaïque. On évoque le Lean du non-gaspillage : on oublie complètement
celui du respect des personnes et de l’amélioration continue. Le Lean de
la manufacture ou de l’industrie n’est pas fait pour nos organisations
ni nos projets modernes, ne l’oubliez pas. La période n’est plus au
Lean, standardisation, amélioration continue sur l’élimination des
gaspillages par implication et respect des personnes. La période est à
l’adaptation, à la NON standardisation, à la NON linéarité, à la NON
répétabilité, induites par la complexité de notre temps. C’est bien cela
aussi ce paradoxe : probablement aucun des lecteurs du livre de Claude
n’appliquera la même saveur de Scrum. Et pourtant parle-t-on bien de la
même chose ? Oui, mais tout est contexte et intention. Il faudra essayer
en puriste avant de changer, mais impossible d’essayer sans s’adapter.
Ah ce joli paradoxe ! Une chose est sûre, je me marre quand je vois
l’ancien système qui remue, se secoue encore un peu, tente de résister.
Mais on ne résiste pas à l’irrésistible progression du temps et du
contexte. Vive *Scrum* (et son oncle ronchon *Extreme Programming*, ou
son cousin ambitieux, *Kanban*) !

D’ailleurs Claude est aussi un drôle de gars. Il fallait bien cela pour
porter cette synthèse de *Scrum* (et au delà) depuis 5 années à travers
ce livre. Vous l’avez compris ce livre ne se résume pas au *Scrum*
officiel, mais bien à sa pratique vivante qui absorbe, essaye, rejette,
intègre, les bonnes idées, les bonnes pratiques des dix, vingt dernière
années. Vous pensez Claude doux, simple et scolaire ? Il est taquin,
curieux et rigoureux (mais c’est vrai qu’il est doux et sage), il est
aussi complexe que l’approche qu’il prône (et aussi sain). On aborde
Claude comme *Scrum*, avec facilité, c’est une maison accueillante. En
creusant on y découvre des choses inattendues. Une soif d’apprentissage
(qu’il diffuse : c’est pour cela qu’il est un si bon pédagogue je
pense), et une vraie curiosité : il veille. En fait c’est cela, c’est un
veilleur. Il veille à la cohérence de cette pensée qui ne cesse de se
développer ; pas comme un gardien d’un temps passé, mais comme le
porteur d’une nouvelle perspective, car il prospecte sur ces nouvelles
approches, ces nouveaux flux d’idées, de façon intarissable (une 4ème
édition, ceci explique cela, Claude est un prospecteur, un pionnier).
Moi je me marre bien avec Claude, quand je le vois vitupérer contre un
discours sans nouvelles idées, ou quand — d’un petit geste de la main
très révélateur — il laisse les pisses-froids maugréer dans leur coin.

Avant de vous laisser avancer avec Claude, et de vous souhaiter une
bonne lecture, je voudrais rappeler un point qui me parait essentiel
d’avoir en tête lors de cette découverte du mouvement Agile. Ce point
c’est la zone d’inconfort. Si vous vous lancez dans ces pratiques avec
facilité c’est que vous vous plantez probablement. L’agile propose un
changement de paradigme assez radical par rapport à notre manière de
percevoir l’entreprise, sa création de valeur et sa dynamique de groupe.
Nous sommes aux antipodes des habitudes françaises des cinquante
dernières années : son cartésianisme, sa hiérarchie, et (contrepartie de
sa sophistication que j’aime) son culte de la perfection.

Prenez ce livre comme si il y avait un sticker « Positionnement
dangereux » dessus. Et mettez vous en danger pour bien comprendre les
tenants et les aboutissants de l’Agilité. Sans danger, sans inconfort,
c’est probablement que vous maquillez vos anciennes habitudes sous
couvert de nouveaux habits. Et Claude aura échoué.

Essayez de provoquer le « OH » des familles américaines qui, dans les
années cinquante, ont vu sur leurs TV débouler Elvis avec son
déhanchement et son magnifique « that’s all right mama ». Si vous ne
ressentez pas le toupet qu’il faut pour penser et être agile aujourd’hui
c’est que : a) nous sommes en 2030 et vous tenez entre les mains une
très vieille édition du livre de Claude, b) vous faîtes partie des rares
personnes pour qui l’agile est innée c) alertez-vous et mettez-vous dans
l’inconfort, prenez plus de risques.

Quand vous essayez une pratique de ce livre, faites la réellement, pour
voir. Pour en connaître les limites et les véritables enseignements.
Presque jusqu’à l’absurde, qui pourrait se révéler plus sain qu’il n’y
parait. Soyez « jusqu’au-boutiste » pour savoir, pas dogmatique pour
fossiliser. Je ne parle pas de s’enfermer dans quelque chose d’extrême,
mais d’essayer vraiment, puis de placer son curseur à bon escient, en
connaissance. Beaucoup voit dans Agile du bon sens, c’est en grande
partie vrai. Mais en grande partie seulement, un tiers de son approche
n’est ni intuitive, ni apparentée au bon sens. Souvent on oublie
celle-là, et la cohérence générale en pâtit. Pour découvrir ce paysage
secret, il faut s’y balader.

Aujourd’hui les mots « agile », « lean », « lean startup », « design
thinking », « entreprise libérée » sont lancés. Chacun de ces mots est
un emballage qui correspond le mieux à la population à laquelle il
s’adresse (Agile pour les informaticiens, Lean pour les méthodologistes,
Lean Startup pour le métiers, Design Thinking pour les agences et
créatifs, entreprise libérée pour les entrepreneurs). Mais derrière
c’est le même mouvement de fond : cette transformation profonde,
révolutionnaire, sur la façon de percevoir et de penser nos
organisations et nos relations. Est-ce nous qui l’espérons tant que nous
en faisons une réalité, ou cette transformation est-elle réellement
inéluctable (ce que j’espère) ?

Comment allez vous juger que cela marche d’ailleurs ? Scrum porte-t-il
ses fruits ? Ne lisez pas votre implémentation en référence aux mesures
de l’ancien système. Essayez de savoir ce qui se raconte dans les repas
d’amis et de familles le soir concernant votre organisation, votre
utilisation de scrum, le meilleur indicateur se trouve là.

Mince je suis en train de vous dire que vous imaginez Scrum simple,
qu’il ne l’est pas, que sans inconfort, prise de risque, point de salut,
Vous êtes découragés ? Bien ! Maintenant tout ne peut qu’aller mieux.

PS/ Claude et moi avons un autre centre d’intérêt commun : la musique,
le rock’n roll, et s’il fallait se focaliser sur un groupe : Led
Zeppelin. Ça doit compter pour chambouler les idées, d’aimer ces quatre
gars qui débarquaient comme une horde sauvage sur la scène et assenaient
le riff inégalé de Whole Lotta Love. Pour avoir donc une expérience
améliorée de la lecture de ce livre, je vous recommande donc de
l’accompagner avec un disque de Led Zeppelin dans les oreilles et une
Chartreuse (ou une mirabelle) sur les lèvres.

## Feedback ?

Merci à Jean-Luc Blanc des éditions [Dunod](http://www.dunod.com/) pour
ce petit mot : "C’est une très belle préface, vivante et chaleureuse,
différente des textes convenus que nous recevons trop souvent."
