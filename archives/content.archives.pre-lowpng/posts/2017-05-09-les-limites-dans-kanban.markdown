---
date: 2017-05-09T00:00:00Z
slug: les-limites-dans-kanban
tags: ['kanban','limite','wip','leadtime']
title: Les limites dans Kanban
---

À la suite de mes deux articles sur les graphes Kanban expliqués aux
enfants, on me demande si je voudrais préciser un peu des choses sur
l'utilisation des limites dans Kanban. Impossible de faire un article
sur les limites expliquées aux enfants, ceux qui en ont savent. Je vais
donc simplement essayer d'expliquer ce que je sais et ce que je
comprends des limites dans Kanban. Le spécialiste étant bien évidemment

[Laurent](http://www.morisseauconsulting.com/), son blog et son livre à
ce sujet.

## À quoi cela sert de mettre une limite ? Et comment savoir quelle limiter poser ?

![Les limites de Kanban](/images/2017/05/kanban-limites1.png)

Pour expliciter cet article je vais prendre comme exemple un Kanban tel
que présenté lors du [petit déjeuner Wake Up RH](http://wakeuprh.fr/kanban-rh.html) que je simplifie (pas de
responsable de colonne, pas de critères de sortie). Kanban RH donc,
autour d'une équipe RH. **Vous pouvez cliquer sur les images pour les
agrandir**.

Pour expliciter cet article je vais aussi utiliser quand je le pourrais
une analogie assez classique entre un flux Kanban et un cours d'eau. Le
but est de vous fournir une métaphore.

Bref, quand on démarre un Kanban c'est d'abord souvent du management
visuel qui dévoile le flux de votre activité, de votre création de
valeur. Puis on ajoute une limite pour contraindre le flux. C'est une
action décisive qui a un but : limiter le stock, accélérer la livraison,
ne pas surcharger une étape, etc. Une limite n'est jamais gratuite ni
aléatoire. Quelle limite poser ? La limite est le moyen. **Quel but
poursuivez vous ?** Quelques exemples ci-dessous.

### \#1 Commencer à finir, arrêter de commencer ?

![Les limites de Kanban](/images/2017/05/kanban-limites-stopstarting.png)

Vous êtes dans le cas assez typique où l'une de vos étapes s'engorgent:
on adore commencer des choses, mais on aime rarement les finir. On perd
le **focus**, on se disperse. Dans ce cas on peut placer une limite. Le
but de celle-ci est de limiter le travail en cours sur l'étape
incriminée pour **s'obliger à finir des choses avant d'en démarrer
d'autres**.

#### Métaphore du cours d'eau ?

*Votre activité, votre création de valeur est comme un long cours qui
s'étale grassement à travers la plaine, jusqu'à ne plus couler, qui
stagne. Si vous limitez la taille du cours d'eau, si vous creusez un
fossé assez serré dans lequel l'eau doit passer, le débit s'accélère,
l'eau avance. Il y en a moins en surface, mais elle va beaucoup plus
vite, et au total le volume d'eau qui a traversé est le même voir plus
beaucoup plus gros. Enfin comme toujours la première goutte d'eau à
arriver à l'autre bout de la plaine arrive bien plus vite si le fossée
est étroit. Mais l'eau peut-être violente car elle est contrainte,
attention aux secousses liées à la limite.*

![Les limites de Kanban](/images/2017/05/kanban-limites-stopstarting-2.png)

#### Kanban RH ?

Dans l'exemple des images jointes, on a plein d'idées et de demandes
concernant la marque employeur. On cogite, on étudie, mais on démarre
trop peu. Il faudrait finir d'avoir des choses à préparer, et commencer
à en réaliser certaines. On imagine que placer une limite sur la
préparation : désengorger, obliger à démarrer des choses ou à ne pas en
préparer d'autres avant de traiter l'un des quatre éléments déclarés
dans la colonne "étude/préparation". Dans ce scénario une autre option
aurait été de mieux faire l'action de sélection : en injectant à bon
escient dans le flux. Ou peut-être d'anticiper le goulot de la
préparation en plaçant une limite sur la sélection. À vous d'essayer.

### \#2 Appliquer une stratégie sur votre capacité à faire ?

![Les limites de Kanban](/images/2017/05/kanban-limites-strategie.png)

Vous êtes dans le cas où votre capacité n'est pas répartie selon votre
stratégie, vos besoins. Par exemple vous avez trois domaines d'activités
et tout le monde s'investit dans le domaine \#1. Or il faudrait un
investissement identique dans les trois domaines. Vous limitez
l'activité des différents domaines pour vous assurer qu'elle soit mieux
répartie entre les différents domaines. (Vous pourriez cumuler les deux
premiers scénarios : tout le monde s'investit uniquement sur le domaine
\#1, démarre plein de choses, ne finit rien). La limite va mieux
répartir et pousser à finir.

#### Métaphore du cours d'eau ?

*Le cours d'eau qui s'écoule dans la plaine était fait pour irriguer
initialement trois de vos champs. Mais l'eau est oisive, elle a choisie
le chemin le plus facile et elle ne s'écoule que vers un seul des trois
champs. Si vous contraignez ce chemin facile en limitant le débit
possible, l'eau n'a d'autres choix que d'emprunter les passages vers les
autres champs.*

![Les limites de Kanban](/images/2017/05/kanban-limites-strategie-2.png)

#### Kanban RH ?

Dans l'exemple des images jointes, toute l'équipe se rue sur le
juridique (mais pourquoi *diantre* ?) et sur le recrutement, laissant la
marque employeur et la formation en friche. On pourrait essayer
d'équilibrer, si cela a du sens pour nous, en répartissant des limites
qui contraignent les activités sur les autres domaines (non mesdames et
messieurs, si vos équipes sont dans un environnement sain elles
n'arrêtent pas de travailler, ou ne travaille pas moins, si le travail
fait sens on aime s'y plonger). *Il ne s'agit que d'exemples : ne
respectez pas ces chiffres ou ce Kanban, faîtes la démarche complète
chez vous*.

### \#3 Rétablir un déséquilibre des forces en présence ?

![Les limites de Kanban](/images/2017/05/kanban-limites-equilibre.png){.image
.left width="400"}](/images/2017/05/kanban-limites-equilibre.png)

Là aussi c'est une question de déséquilibre dans la répartition des
forces en présence, mais non pas une répartition selon les domaines,
mais selon les étapes. Vous avez disons dix personnes qui pensent,
cogitent, écrivent les activités à réaliser, et deux personnes qui les
réalisent. Les idées, les cogitations, les écrits fusent, mais
s'accumulent et s'entassent, car les deux personnes qui réalisent ne
font pas le poids face aux dix qui les nourrissent. C'est dommage car
rapidement les idées, les cogitations, périclitent, périment. C'est un
investissement perdu. On place ici une limite en amont de l'étape de
réalisation, sur l'étape de cogitation, pour éviter un amoncellement
inutile, une péremption de celles-ci, pour là encore ne pas travailler
pour rien, ou pour peu, et orienter cette énergie à un endroit plus
pertinent.

#### Métaphore du cours d'eau ?

*Inutile d'arroser trop vos champs avec le cours d'eau : ils ne savent
pas ingurgiter plus d'eau que leur capacité et cette eau est gaspillée.
Autant limiter celle-ci (comme un barrage) pour l'utiliser ailleurs.*

![Les limites de Kanban](/images/2017/05/kanban-limites-equilibre-2.png)

#### Kanban RH ?

Caroline, Marie-Claire, Bénédicte, Simon, André, et Martha sont des
consultants embauchés pour remettre à flot la marque employeur. Ils
accompagnement Johanna et Natasha sur cette mission. Vincent et Patrick
réalisent toutes ces demandes, quand ils peuvent, car Patrick s'occupent
aussi du support clients. De fait les demandes, idées, s'accumulent dans
la case "étude/préparation", et aussi dans la case "réalisation". On a
poussé Vincent et Patrick a démarré des choses, mais pour eux impossible
de les finir, pas assez de bras. (C'est encore plus visible si on
utilise [les flux tirés comme dans cet article](/2016/02/portfolio-projets-kanban-partie-2/)). Notre option :
placer un flux tiré (sous-colonnes *en cours* et *fini*) pour rendre
visible cette surcharge entre deux forces en présence déséquilibrées, et
une limite sur la partie en amont "étude/préparation" pour ne pas trop
en faire, une limite sur la partie "réalisation" pour s'assurer que
l'équipe reste focus. Ainsi plusieurs effets : plus la peine d'autant de
consultants... ou les consultants se mettent à faire, à réaliser aussi,
ou ils se partagent sur une autre activité. Vous auriez peut-être pu ne
pas mettre de limite en faisant grossir votre équipe de réalisation.
Tout cela reste des hypothèses.

### \#4 N'oubliez pas les limites basses

![Les limites de Kanban](/images/2017/05/kanban-limites-basses.png)

Pour garantir la cadence du flux on peut le contraindre, mais il ne faut
pas oublier de l'alimenter aussi. Vous pouvez avoir des limites basses.
Mais vous avez peut-être besoin d'irriguer vos champs *a minima* tous
les jours. Si plus personne n'a d'idée, ne cogite, soudainement les deux
personnes qui réalisaient (dans notre exemple précédent) se trouvent à
cours de matière. Vous devez peut-être vous assurer d'avancer sur le
juridique, ou dans un *portfolio kanban* de projets vouloir *a minima*
un ou deux projets d'innovation pure, etc. Il faut parfois penser à
assurer un débit minimum.

![Les limites de Kanban](/images/2017/05/kanban-limites-basses-2.png)

Ici pour s'assurer que un minimum de candidats soient vus en entretien,
on place une limite basse sur le "sourcing" qui doit se faire sur quatre
éléments minimum.

### \#5 S'assurer que tout le flux est limité ?

![Les limites de Kanban](/images/2017/05/kanban-limites-expedites.png)

Si vous avez essayé de placer une ligne "urgence" (qu'on appelle souvent
"expedites" en kanban) pour mettre en évidence les urgences à traiter,
il se peut que ceci vous soit arrivé... (voir image à gauche). C'est
humain peut-être. Tout est devenu urgence, vidant de son sens cette
appellation. Du coup pour régler cela vous voudriez placer des limites
sur *toutes* les colonnes, ou même encore plus limiter le nombre
d'éléments en en autorisant moins d'un par colonne.

![Les limites de Kanban](/images/2017/05/kanban-limites-expedites-2.png)

Dans ce cas c'est très contraignant mais vous pouvez placer une limite
en début de ligne.

Ces cinq exemples sont des exemples comme leur nom l'indique. Ils sont
fréquents, mais ils sont loin de limiter les cas de figures.

## Quand est-ce que je propose à une équipe de mettre des limites ?

La première des choses est de rendre visuel l'activité, la création de
valeur. Au début il n'y a donc sûrement pas de limite. Sauf si vous avez
déjà des limites dans votre flux, mais alors vous faîtes déjà du Kanban,
ce n'est pas le début. Pourtant certains d'entres vous diront que sans
limite ce n'est pas encore un kanban mais "simplement" du management
visuel. Ils ont peut-être raison, et le management visuel c'est **très**
bien. Je vous suggère de d'abord coller à la réalité, de laisser passer
quelques temps pour observer la représentation de votre flux et en tirer
des enseignements, puis ensuite (quelques semaines ou mois plus tard ?)
commencer à placer des limites. Ces limites doivent avoir un sens, un
but, une raison d'être. Je suggère presque de l'écrire quelque part :
"Le but de cette limite est de ...".

## Ma limite est-elle la bonne (en capacité octroyée ?)

Je suis incapable de vous dire d'appliquer telle ou telle formule pour
définir la "bonne" limite. À mes yeux c'est le fruit de l'observation de
votre flux qui vous fait déduire la bonne limite à appliquer. Elle
devrait être rationnelle : en fonction du nombre de personnes qui
travaillent dans la "colonne", sur l'activité. En fonction des
compétences et de la pluridisciplinarité de ces personnes. Mais aussi du
type d'éléments dans votre flux de création de valeur. Enfin et surtout
du sens que poursuit cette limite. **C'est donc une information à
définir en fonction du contexte qui va émerger assez naturellement et
qui peut se modifier, se calibrer**.
