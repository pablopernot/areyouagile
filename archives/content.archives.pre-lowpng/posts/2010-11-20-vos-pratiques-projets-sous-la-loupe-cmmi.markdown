---
date: 2010-11-20T00:00:00Z
slug: vos-pratiques-projets-sous-la-loupe-cmmi
tags: ['cmmi','agile']
title: Vos pratiques projets sous la loupe CMMi
---

Je voudrais prendre le temps de rappeler l'existence de cette fiche
excel (trouvée un jour par hasard sur le web) en provenance directe de
**Richard Basque**, et dont je me suis servi maintes fois dans le cadre
d'audits (en citant ma source !). Car cette fiche mentionne :
*Permission de réutiliser mais avec la référence obligatoire: " © 2006
Alcyonix (www.alcyonix.com)"*. Voilà, c'est chose faîte une nouvelle
fois. Merci **Alcyonix** (et au passage je ne sais pas si c'est encore
d'actualité : "salut Eric ! salut Christian !").

![CMMi](/images/2010/11/cmmi-levels-300x239.jpg)

En 9 mots : CMMi est un très intéressant ensemble de pratiques projets
(c'est un résumé très succinct que je fais là, lisez plutôt
[ceci](http://en.wikipedia.org/wiki/Capability_Maturity_Model_Integration)).
L'une de ses forces est de laisser chacun implémenter ces pratiques
comme il le souhaite. Concernant certaines de ses faiblesses je vous
laisse consulter certains de mes précédents billets. Pour ma part, et je
l'ai déjà évoqué, j'utilise surtout mes connaissances et mon expérience
CMMi dans le cadre d'audits (d'où aussi cette *checklist*). Je
recommande à tous les chef de projets ou scrummaster ou autres de se
plonger un peu dans les différentes questions proposées ici et qui -je
l'espère- nourriront votre réflexion concernant vos projets
informatiques. Merci encore Al' !

Le lien vers la fiche excel :
[template-cmmi-v13-standard-piis](/images/2010/11/template-cmmi-v13-standard-piis.xls)
