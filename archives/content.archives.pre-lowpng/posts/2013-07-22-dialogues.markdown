---
date: 2013-07-22T00:00:00Z
slug: dialogues-poeme-et-situation
tags: ['storytelling','conversation','dialogue','motivation']
title: Dialogue, poème et situation
---

C'est l'été, qu'est ce que j'ai dans ma besace ? Quelques scènes et
dialogues vécus par moi-même ou d'autres, un poème qui m'a été adressé
(si si). J'espère que cela vous donnera le sourire, comme ce fut le cas
pour moi.

## La motivation

Formation "agile" (quelle qu'elle soit), c'est le début de la formation,
une introduction sur l'histoire de l'agilité. On passe en revue les
différents
[principes](http://agilemanifesto.org/iso/fr/principles.html). Et
toujours, systématiquement, quelqu'un s'esclaffe, ou montre
ostensiblement un sourire en coin.

- Qu'est ce qui vous fait rire ? (mais je sais très bien)

- *Réalisez les projets avec des personnes motivées*. Pfffff. C'te bande
de bisounours.

- Ah oui, vous voulez dire : forcément avec des gens motivés tous les
projets réussissent !

- Ben oui, c'est tellement évident pas la peine d'en rajouter.

(à mon tour d'arborer un grand, très grand, sourire en coin)

- Je suis entièrement d'accord d'autant que ça facilite notre objectif.
Oublions tout le reste, toutes les formations, l'agilité, ne gardons
qu'un seul objectif : débrouillons nous pour que les gens soient motivés
et tous nos projets aboutirons. D'accord ?

- Errrrr... oui.

## Génération agile

Celle là je la tiens de Claude Emond (@claudeemond), une de mes
dealers d'idées en ce moment, j'espère qu'il ne m'en voudra pas de la
raconter (je le préviens de ce pas). Claude donne des sessions pour des
élèves. Les élèves sont passionnés mais dubitatifs :

- Avec les managers que nous connaissons et avons il sera impossible
d'appliquer les idées que tu défends. Elles nous plaisent mais dans la
vraie vie...

Ce à quoi Claude répond :

- D'accord mais quand ils seront morts, que certains d'entres vous
seront les nouveaux managers, sachez vous rappelez de ces idées et de ce
moment !

J'adore.

## Le prince de l'agile

Un poème, oui, j'ai reçu un poème d'un client, d'un copain, que je
n'avais pas croisé de longue date. Naturellement il doit être très très
proche du burnout :)

Voici donc ce qu'il m'écrit à la fin du printemps :

*Sinon, Prince de l'Agile, ca sonne bien, non ? Pour te motiver à
répondre, je t'ai fait une belle chanson, tiens:*


>*Le sourire prompt et le laptop dell*

>*Je m'en vais courir la gente ssi*

>*Moult consultants n'ont pas ma fière allure*

>*Je suis Pablo Pernot le prince de l'agile*

>*Le prince de l'agile*

>*Pour aller gagner glâner quelques story points*

>*Je risque le burndown, en plein milieu d'un sprint*

>*Mais je suis plutôt scrum donc ma force est tranquille*

>*Mon coeur est serein et mon outil agile*

>*Et mon outil agile...*

>*Car je suis le prince de l'agile*

>*Le priiiince de l'agile*

>*Sauf si t'es Spiral ou Waterfail*

>*Tu ne peux que m'aimer toujours*

Il vous sera consenti le droit à m'appeler le prince de l'agile. ahah.
Je risque de le trainer celui-là.

## Dialogue suite à un ball point game

Pour finir un dialogue suite à un [ball point
game](/2013/03/le-jeu-scrum-des-balles-et-des-points/) qui -sans que le
sache- aurait pu tourner au drame. La personne croise son patron dans
les couloirs suite à une journée de formation...je lui laisse la parole:

...

A peine arrivé, j’ai eu le plaisir de me faire alpaguer par mon boss,
l’air mauvais :

- Tu m’as foutu un beau bordel avec ta formation !

- ?

- Jeudi après midi…

- ?

- Les balles !

Là, je commence à le voir venir…

- Mes avant-ventes vous ont vu jouer à la baballe, ça a fait le tour de
l’immeuble !

- Ha…je vois…

- Ils m’ont dit *tu comprends, Thierry, nous on en a jusqu’à par-dessus
la tête, et eux, ils jouent à la baballe !!!*

- OK, mais bon, en même temps, c’est une formation, et dans ce contexte,
je t’assure, ça fait sens : ça mets notamment les doigts sur le fait que
c’est avec…

- Oui bon, je sais, je sais, mais là, ils veulent se mettre en grève…

- Plait-il ?

- Ben oui, il manque plein de trucs cons dans l’appli pour les démos,
des rapports consolidés, notamment, alors ils en ont marre de se faire
éclater en démo et ils veulent plus y aller.

- Ben….y’a qu’à nous demander de les faire, non ? on est là pour ça, non
?

- Oui mais vous avez des trucs à faire, on a 3 ans de retard sur la
nouvelle offre...

- Ben oui mais en même temps, on parle de 10 rapports, on les connait,
pas d’analyse à faire, on les mets dans le flux et elle sont prises en
charges quand on a un trou, ça le fait…

- Ha ? ha ben OK… mais le dit pas au chef produit, ça va le mettre en
vrac...

Là, j’ai failli lui parler du national monument de Washington ([5
pourquoi]()), mais vu son état, alors je me suis abstenu…

...

Voilà. Attention donc quand vous organisez vos ateliers agiles...
