---
date: 2016-04-18T00:00:00Z
slug: les-estimations-ne-sont-pas-uniquement-liees-a-la-complexite-bordel
tags: ['estimation','effort','scrum','fibonacci']
title: Les estimations ne sont pas uniquement liées à la complexité, bordel
---

C'est dimanche, et je sors d'un repas de (belle)-famille. J'ai donc
besoin d'exterioriser une pression intérieure. Merci de m'assister dans
cette tâche. j'entends trop souvent dire que les estimations sont liées
à la complexité. C'est trop limitatif.

Dans notre monde mouvant il faut agir au plus juste. Enfin surtout dans
nos entreprises où un constant rapport entre l'effort et la valeur est
de mise (et si on peut juger de cette valeur au plus vite, on saura
mesurer au mieux notre effort).

Et donc c'est bien l'effort que l'on estime. Si tant est que l'on
continue à estimer, si tant est que cela soit nécessaire : si on a
découpé en petits morceaux qui font sens, et que l'on priorise par
valeur, pourquoi estimer ? Autant réaliser et apprendre (et faire table
rase des estimations).

C'est donc bien l'effort que l'on estime.

Et oui l'effort se divise en **a) complexité**, c'est à dire en **jus de
cerveau**, il faut cogiter avant de sortir cette moelle substantifique.
Par exemple trouver l’algorithme de calcul qui convient.

En **b) besogne** : ici pas de jus de cerveau, mais de l'**huile de
coude**. C'est un **labeur**, répétitif probablement, vaste peut-être.
Par exemple : peindre tous les volets du grand hotel Budapest, changer
le nom des classes dans tout votre code.

En **c)** ce que l'on désire souvent peu : l'**obscurité**,
l'**incertitude**, l'**opacité**. On avance dans les brumes. Par exemple
: trouver un restaurant dans une ville inconnue sans connexion internet
ni carte ni plan, où se brancher à un système tiers sans assez
d'informations sur lui.

Le plus de complexité, le plus de besogne, le plus d'obscurité et plus
vous haussez l'estimation de l'effort. Mais n'oubliez pas si vous
découpez votre activité en petits morceaux qui font sens (ce qui est
déjà une forme d'estimation ceci dit) et dont la valeur peut ainsi être
mesurée, et que vous savez prioriser, et qu'il faudra le faire, inutile
d'estimer, autant démarrer. Cela arrive plus souvent qu'on ne le pense.

Bordel.

Je me sens mieux. Et je fais plaisir à des proches qui me demandent des
articles courts.

Aussi en complément un vieil article sur [la malédiction du jour homme](/2012/03/la-malediction-du-jour-homme/).

