---
date: 2014-09-13T00:00:00Z
slug: histoires-que-je-raconte-en-ce-moment
tags: ['histoire','storytelling']
title: Histoires que je raconte en ce moment
---

Voici une petit série d'histoires que je raconte en ce moment pour
illustrer mes propos (accompagnement, formation, conférence, etc.).
J'aime beaucoup les histoires. C'est un format, un motif, dont le
cerveau est friand. Un excellent moyen de conservation, de projection,
de changement. Je vous donne les sources de ces histoires mais je vous
raconte ma version (ce que mon cerveau en a gardé), ne vous attendez pas
à trouver exactement les mêmes.

## Sur l'implication liée à la *gamification*

Alors que je vous parle, je reçois le bulletin scolaire de mon fils
aîné, Mathieu. Mathieu a de bons résultats scolaires mais le carnet
indique que si il était plus impliqué il aurait de **très bons résultats
scolaires**. Je cherche Mathieu dans la maison :

-   Mathieu ? Mathieu ?
-   (de l'autre pièce) Oui papa ?
-   Peux-tu venir que l'on parle de ton bulletin scolaire ?
-   Attend, là je ne peux pas, je suis en train de construire la
    quatrième tour de mon château fort (*Minecraft*...) et elle n'a pas
    exactement les mêmes décorations que les autres, je ne peux pas la
    laisser ainsi, tu comprends il manque la bonne proportion de fer,
    ...
-   (soupir)(silence) ...Oui, c'est exactement de cela que je souhaite
    te parler.

En d'autres termes Mathieu est très impliqué dans son jeu, et ses
résultats sont très bons. Qu'est ce qui manque à l'école pour déclencher
ce même engagement ? Qu'est-ce qui manque à l'entreprise pour avoir ce
même type d'implication ?

Pour en savoir plus sur cette *gamification* :
<http://areyouagile.com/2013/10/deux-amis/>

## Sur la [sérendipité](http://fr.wikipedia.org/wiki/S%C3%A9rendipit%C3%A9)

*C'est à dire notre capacité à trouver des solutions à des problèmes par
des moyens inattendus ou détournés, une découverte accidentelle.
L'essence de beaucoup de brainstorming ou de jeux*.

Un gros consortium canadien est responsable des lignes électriques qui
parcourent les forêts du pays. Problème : le poids de la neige menace de
faire s'effondrer ces dîtes lignes. Comment résoudre ce problème ? Ils
*brainstorment*, sans hésiter à délirer, on ne sait pas ce qui en
sortira, mais bon. Voyons.

-   Soudain une idée incongrue surgit :
-   Et si on s'appuyait sur les ours ? Les ours ? oui en les faisant
    secouer les poteaux ?
-   (vraiment cette réunion c'est n'importe quoi).
-   Ok et comment on amène les ours sur les poteaux ?
-   En plaçant des pots de miel en haut des poteaux, sur les lignes ?
    Attirés ils vont venir secouer les poteaux.
-   Ok, et comment on place les pots de miel sur les poteaux ? On ne
    peut pas placer de milliers de pots de miel ?
-   Hummm, et si nous faisions couler du miel sur les lignes en passant
    avec un hélicoptère ?
-   ...
-   Bingo : l'organisation a réglé son problème.... en faisant passer un
    hélicoptère au dessus des lignes, les pales de celui-ci font tomber
    la neige.

(Je tiens cette histoire de **Valérie Wattelle** qui la tient de je ne
sais où, je dois lui demander).

## Sur les options réelles, le *décider le plus tard possible* du Lean

C'est l'histoire d'une nouvelle université, toute belle, toute neuve.
Alors que tous les plans sont achevés, que tous les immeubles sont
construits, le doyen fait stopper les travaux. Comment ? mais c'est
impossible la rentrée c'est maintenant ! Peu importe, répond-t-il, nous
finirons les chemins qui séparent les bâtiments l'été prochain. Chez les
bâtisseurs c'est la consternation. Mais l'année scolaire passe. Et quand
l'été revient le doyen indique les marques laissées au sol par les
étudiants dans leurs pérégrinations à travers l'université et il glisse
aux chefs des travaux : voilà c'est ici que nous ferons finalement les
chemins reliant les différents bâtiments. Inutile de préciser qu'aucun
des chemins tracés par les pas des étudiants ne correspondait au plan
initialement prévu.

(Cette histoire vient de *The spirit of leadership* de **Harrison Owen**,
<http://www.amazon.com/The-Spirit-Leadership-Liberating-Leader/dp/1576750566>)

Illustrée ces temps-ci sur Twitter par cette photo :

![Pelouse](/images/2014/09/pelouse.png)

## Encore l'implication

Un dernier détour par l'école de mon fils aîné. Réunion
parent/professeur, je suis en tête à tête avec la professeur principale,
elle se plaint de la qualité générale de la classe, de son manque de
sérieux et du niveau de son travail. Puis nous passons à la récente
sortie pédagogique sur les fouilles archéologiques (plusieurs jours sur
site). Elle se délecte de la réussite de cet événement, et dans les
minutes qui suivent m'indique que la classe a fourni un excellent
travail et dossier. Surpris je l'interroge donc sur l'écart entre le
travail quotidien et ce travail là. Elle reste plusieurs secondes
silencieuse comme si un piège s'était refermé sur elle puis me glisse,
un peu désarçonnée : mais on ne peut pas faire des choses nouvelles
intéressantes comme cela tout le temps. Je suis resté silencieux en
pensant que c'était bien de l'éducation et de la vie de nos enfants dont
on parlait alors.

Là aussi j'opère un rapprochement évident avec nos organisations : si
vous faîtes des choses qui intéressent les personnes avec lesquelles
vous travaillez, si vous réussissez à les impliquer, ce qui en résultera
n'aura rien à voir, non ? donc.

## Management cognitif

Denis est nouvellement nommé dans cette boite de manufacture. Il doit
améliorer les processus. Avec son œil neuf il voit tout de suite qu'il y
a un nombre anormalement grand de gants divers et variés aux mains des
employés. Il mène l'enquête. 264 ! 264 paires de gants différentes, ou
pire, les mêmes achetées à des prix différents à différents
fournisseurs. C'est n'importe quoi ! Il prend sa plus belle plume, un
fichier *excel*, dresse le rapport chiffré de son enquête et envoi cela
au *board*. Celui-ci répond : merci, très bien, cette information est
très importante, au prochain budget nous changeons cela.

... au prochain budget, c'est à dire dans 6 mois... aux yeux de Denis
c'est ridicule. Il invite les membres du *board* à passer à son bureau.
Quand ils entrent ils découvrent 264 paires de gants sur le bureau, au
centre de la pièce, sur chaque paire une petite étiquette qui stipule
son prix, et donc plusieurs fois la même paire à des prix différents.
"ce n'est plus possible", dit l'un, "il faut changer cela immédiatement"
dit l'autre, "j'appelle les fournisseurs" dit le troisième.

Ce qui était acceptable sous la forme d'un tableau *excel* ne l'est plus
quand on est confronté physiquement à lui.

(Cette histoire vient de *Switch*, des frères **Heath**, <http://heathbrothers.com/books/switch/>)
