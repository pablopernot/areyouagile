﻿---
date: 2019-06-14
slug: kanban-le-livre-les-illustrations
tags: [kanban, livre, illustration]
title: Kanban, le livre, les illustrations
--- 


La semaine dernière j'avais le plaisir de sortir avec [Laurent Morisseau](http://www.morisseauconsulting.com/) **Kanban**, *L'approche en flux pour l'entreprise agile*. J'ai pu ajouter pas mal d'illustrations au travail initial de Laurent, et dans les parties que j'ai souhaité ajouter. Seul regret ces illustrations ne sont pas en couleurs. Je vous les propose ici dans leur format original, en couleurs, avec l'aval de Dunod.  

Il s'agit ici uniquement des nouvelles illustrations, beaucoup d'autres sont présentes dans le livre. (Le livre est une refonte de "Kanban pour l'IT").  

*N'hésitez pas à vous en servir dans vos présentations, mais merci de citer la source.*  

### Partie 1 : Découvrir Kanban

Pas forcément dans l'ordre, cliquez sur les images pour agrandir. 

{{< thumbnail 
"/images/2019/06/partie1/01-kanban.png"
"/images/2019/06/partie1/thumbnail-01-kanban.png"
"Kanban" >}}

{{< thumbnail 
"/images/2019/06/partie1/02-fiche-exemple.png"
"/images/2019/06/partie1/thumbnail-02-fiche-exemple.png"
"Kanban fiche exemple" >}}

{{< thumbnail 
"/images/2019/06/partie1/02-kanban.png"
"/images/2019/06/partie1/thumbnail-02-kanban.png"
"Kanban" >}}

{{< thumbnail 
"/images/2019/06/partie1/03-kanban.png"
"/images/2019/06/partie1/thumbnail-03-kanban.png"
"Kanban" >}}

{{< thumbnail 
"/images/2019/06/partie1/04-kanban-wip.png"
"/images/2019/06/partie1/thumbnail-04-kanban-wip.png"
"Kanban WIP" >}}

{{< thumbnail 
"/images/2019/06/partie1/05-kanban-time.png"
"/images/2019/06/partie1/thumbnail-05-kanban-time.png"
"Kanban Lead Time" >}}

{{< thumbnail 
"/images/2019/06/partie1/06-kanban-flux.png"
"/images/2019/06/partie1/thumbnail-06-kanban-flux.png"
"Kanban flux" >}}

{{< thumbnail 
"/images/2019/06/partie1/07-kanban-flux-2.png"
"/images/2019/06/partie1/thumbnail-07-kanban-flux-2.png"
"Kanban flux 2" >}}

{{< thumbnail 
"/images/2019/06/partie1/08-kanban-limite.png"
"/images/2019/06/partie1/thumbnail-08-kanban-limite.png"
"Kanban Limite" >}}

{{< thumbnail 
"/images/2019/06/partie1/09-kanban-urgences-2.png"
"/images/2019/06/partie1/thumbnail-09-kanban-urgences-2.png"
"Kanban Urgence" >}}

{{< thumbnail 
"/images/2019/06/partie1/afaire-encours-fini.png"
"/images/2019/06/partie1/thumbnail-afaire-encours-fini.png"
"Kanban à faire - en cours - fini" >}}

{{< br >}}

### Partie 2 : S'organiser avec Kanban

{{< thumbnail "/images/2019/06/partie2/011-kanban-pull-1.jpg" 
"/images/2019/06/partie2/thumbnail-011-kanban-pull-1.jpg" 
"Kanban Pull" >}}

{{< thumbnail "/images/2019/06/partie2/012-kanban-pull-2.jpg" 
"/images/2019/06/partie2/thumbnail-012-kanban-pull-2.jpg" 
"Kanban Pull 2" >}}

{{< thumbnail "/images/2019/06/partie2/013-kanban-pull-3.jpg" 
"/images/2019/06/partie2/thumbnail-013-kanban-pull-3.jpg" 
"Kanban Pull 3" >}}

{{< thumbnail "/images/2019/06/partie2/kanban-limites-basses.png" 
"/images/2019/06/partie2/thumbnail-kanban-limites-basses.png" 
"Kanban Limite Basse" >}}

{{< thumbnail "/images/2019/06/partie2/kanban-limites-basses-2.png" "/images/2019/06/partie2/thumbnail-kanban-limites-basses-2.png" 
"Kanban Limite Basse 2" >}}

{{< thumbnail "/images/2019/06/partie2/kanban-limites-equilibre.png" "/images/2019/06/partie2/thumbnail-kanban-limites-equilibre.png" 
"Kanban Limites équilibre" >}}

{{< thumbnail "/images/2019/06/partie2/kanban-limites-equilibre-2.png" "/images/2019/06/partie2/thumbnail-kanban-limites-equilibre-2.png" 
"Kanban Limites équilibre 2" >}}

{{< thumbnail "/images/2019/06/partie2/kanban-limites-expedites.png" "/images/2019/06/partie2/thumbnail-kanban-limites-expedites.png" 
"Kanban Expedites" >}}

{{< thumbnail "/images/2019/06/partie2/kanban-limites-expedites-2.png" "/images/2019/06/partie2/thumbnail-kanban-limites-expedites-2.png" 
"Kanban Expedites 2" >}}

{{< thumbnail 
"/images/2019/06/partie2/kanban-limites-stopstarting.png" 
"/images/2019/06/partie2/thumbnail-kanban-limites-stopstarting.png" 
"Kanban Stop Start" >}}


{{< thumbnail 
"/images/2019/06/partie2/kanban-limites-stopstarting-2.png" 
"/images/2019/06/partie2/thumbnail-kanban-limites-stopstarting-2.png" 
"Kanban Stop Start 2" >}}

{{< thumbnail 
"/images/2019/06/partie2/kanban-limites-strategie.png" 
"/images/2019/06/partie2/thumbnail-kanban-limites-strategie.png" 
"Kanban Stratégie" >}}

{{< thumbnail 
"/images/2019/06/partie2/kanban-limites-strategie-2.png" 
"/images/2019/06/partie2/thumbnail-kanban-limites-strategie-2.png" 
"Kanban Stratégie 2" >}}

{{< br >}}

### Partie 4 : Accompagner Kanban dans l'organisation

{{< thumbnail 
"/images/2019/06/partie4/a-acquerir.png" 
"/images/2019/06/partie4/thumbnail-a-acquerir.png" 
"Agile Fluency and Kanban" >}}

{{< thumbnail 
"/images/2019/06/partie4/famille-1.png" 
"/images/2019/06/partie4/thumbnail-famille-1.png" 
"Exercice coaching : la famille" >}}

{{< thumbnail 
"/images/2019/06/partie4/famille-2.png" 
"/images/2019/06/partie4/thumbnail-famille-2.png" 
"Exercice coaching : la famille - 2" >}}

{{< thumbnail 
"/images/2019/06/partie4/startup-1.png" 
"/images/2019/06/partie4/thumbnail-startup-1.png" 
"Exercice coaching : the startup" >}}

{{< thumbnail 
"/images/2019/06/partie4/startup-2.png" 
"/images/2019/06/partie4/thumbnail-startup-2.png" 
"Exercice coaching : the startup - 2" >}}

{{< thumbnail 
"/images/2019/06/partie4/verybigcompany-1.png" 
"/images/2019/06/partie4/thumbnail-verybigcompany-1.png" 
"Exercice coaching : the very big company" >}}


{{< thumbnail 
"/images/2019/06/partie4/verybigcompany-2.png" 
"/images/2019/06/partie4/thumbnail-verybigcompany-2.png" 
"Exercice coaching : the very big company - 2" >}}

{{< br >}}

### Annexes : Kanban pour les enfants

{{< thumbnail 
"/images/2019/06/partie5/billes-mur-2.jpg" 
"/images/2019/06/partie5/thumbnail-billes-mur-2.jpg" 
"Pour les enfants : la partie de billes" >}}

{{< thumbnail 
"/images/2019/06/partie5/billes-mur-3.jpg" 
"/images/2019/06/partie5/thumbnail-billes-mur-3.jpg" 
"Pour les enfants : la partie de billes - 2" >}}

{{< thumbnail 
"/images/2019/06/partie5/billes-mur-4.jpg" 
"/images/2019/06/partie5/thumbnail-billes-mur-4.jpg" 
"Pour les enfants : la partie de billes - 3" >}}

{{< thumbnail 
"/images/2019/06/partie5/cc-1-1.jpg" 
"/images/2019/06/partie5/thumbnail-cc-1-1.jpg" 
"Pour les enfants : la partie de billes - control chart" >}}

{{< thumbnail 
"/images/2019/06/partie5/cc-2-1.jpg" 
"/images/2019/06/partie5/thumbnail-cc-2-1.jpg" 
"Pour les enfants : la partie de billes - control chart 2" >}}

{{< thumbnail 
"/images/2019/06/partie5/cc-2.jpg" 
"/images/2019/06/partie5/thumbnail-cc-2.jpg" 
"Pour les enfants : la partie de billes - control chart 2 détails" >}}

{{< thumbnail 
"/images/2019/06/partie5/edmond-1.png" 
"/images/2019/06/partie5/thumbnail-edmond-1.png" 
"Pour les enfants : les dinosaures - edmond 1" >}}

{{< thumbnail 
"/images/2019/06/partie5/edmond-2.png" 
"/images/2019/06/partie5/thumbnail-edmond-2.png" 
"Pour les enfants : les dinosaures - edmond 2" >}}

{{< thumbnail 
"/images/2019/06/partie5/edmond-2-2.png" 
"/images/2019/06/partie5/thumbnail-edmond-2-2.png" 
"Pour les enfants : les dinosaures - edmond 2.2" >}}

{{< thumbnail 
"/images/2019/06/partie5/edmond-3.png" 
"/images/2019/06/partie5/thumbnail-edmond-3.png" 
"Pour les enfants : les dinosaures - edmond 3" >}}

{{< thumbnail 
"/images/2019/06/partie5/edmond-4.png" 
"/images/2019/06/partie5/thumbnail-edmond-4.png" 
"Pour les enfants : les dinosaures - edmond 4" >}}

{{< thumbnail 
"/images/2019/06/partie5/edmond-5.png" 
"/images/2019/06/partie5/thumbnail-edmond-5.png" 
"Pour les enfants : les dinosaures - edmond 5" >}}

{{< thumbnail 
"/images/2019/06/partie5/ods1.jpg" 
"/images/2019/06/partie5/thumbnail-ods1.jpg" 
"Pour les enfants : les cookies - cfd 1" >}}


{{< thumbnail 
"/images/2019/06/partie5/ods2.jpg" 
"/images/2019/06/partie5/thumbnail-ods2.jpg" 
"Pour les enfants : les cookies - cfd 2" >}}

{{< thumbnail 
"/images/2019/06/partie5/ods4.jpg" 
"/images/2019/06/partie5/thumbnail-ods4.jpg" 
"Pour les enfants : les cookies - cfd 3" >}}

{{< thumbnail 
"/images/2019/06/partie5/ods5.jpg" 
"/images/2019/06/partie5/thumbnail-ods5.jpg" 
"Pour les enfants : les cookies - cfd 4" >}}

{{< thumbnail 
"/images/2019/06/partie5/ods6.jpg" 
"/images/2019/06/partie5/thumbnail-ods6.jpg" 
"Pour les enfants : les cookies - cfd 5" >}}

{{< thumbnail 
"/images/2019/06/partie5/ods-leadtime1.jpg" 
"/images/2019/06/partie5/thumbnail-ods-leadtime1.jpg" 
"Pour les enfants : les cookies - cfd - lead time" >}}

{{< thumbnail 
"/images/2019/06/partie5/ods-leadtime2.jpg" 
"/images/2019/06/partie5/thumbnail-ods-leadtime2.jpg" 
"Pour les enfants : les cookies - cfd - lead time 2" >}}

{{< thumbnail 
"/images/2019/06/partie5/ods-wip1.jpg" 
"/images/2019/06/partie5/thumbnail-ods-wip1.jpg" 
"Pour les enfants : les cookies - cfd - WIP" >}}

{{< thumbnail 
"/images/2019/06/partie5/ods-wip2.jpg" 
"/images/2019/06/partie5/thumbnail-ods-wip2.jpg" 
"Pour les enfants : les cookies - cfd - WIP 2" >}}

{{< thumbnail 
"/images/2019/06/partie5/ods-wip3.jpg" 
"/images/2019/06/partie5/thumbnail-ods-wip3.jpg" 
"Pour les enfants : les cookies - cfd - WIP 3" >}}

{{< br >}}

### Références

![](/images/kanban-dunod.jpg)

	Kanban
	L'approche en flux pour l'entreprise agile
	Laurent Morisseau & Pablo Pernot   
	Hors collection - juin 2019  
	272 pages ; 25 x 17,5 cm ; broché  
	ISBN 978-2-10-078105-8  
	EAN 9782100781058

### Où le trouver

Dans vos librairies, et sinon "en ligne" :   

* [Chez l'éditeur Dunod](https://www.dunod.com/sciences-techniques/kanban-approche-en-flux-pour-entreprise-agile)
* [Chez Hachette](https://www.hachette.fr/livre/kanban-lapproche-en-flux-pour-lentreprise-agile-9782100781058)
* [Chez Amazon](https://www.amazon.fr/Kanban-pour-lIT-am%C3%A9liorer-d%C3%A9veloppement/dp/2100781057/ref=asap_bc?ie=UTF8)
* [Chez les libraires](https://www.leslibraires.fr/livre/14475993-kanban-l-approche-en-flux-pour-l-entreprise-a--morisseau-pernot-dunod)
* [À la Fnac](https://livre.fnac.com/a12674262/Laurent-Morisseau-Kanban-L-approche-en-flux-pour-l-entreprise-agile)
