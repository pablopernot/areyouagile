﻿---
date: 2020-03-20
title: "Les organisations vivantes"
url: /organisations-vivantes/chap2/
subtitle: "Penser organique"
---

# Chapitre 2 : Penser organique

> « The bigger we got, the colder we became »
> -- Steven Adler, Guns ’n roses

Nous avons ainsi rapidement dressé une sorte de ligne de temps : le
Fordisme, le Toyotisme, et donc le Lean, puis l’Agile. Chacun répondant
aux contraintes et attentes de son époque. Quelles sont les
caractéristiques de l’époque qui s’ouvre à nous ?

Indéniablement la complexité des dernières années fait partie de ses
caractéristiques. Cette complexité est d’ailleurs en expansion, propagée
par l’accélération des découvertes, et surtout des moyens de
communication, et la mondialisation qu’elle a engendrée disent certains.

Ces moyens de communication sont probablement la deuxième
caractéristique de nos temps : instantanéité, village global, immatériel
: les relations et les richesses changent.

Troisième point et non des moindres : les anciennes richesses
périclitent : non pas uniquement, car elles s’amoindrissent, mais parce
qu’elles disparaissent. Nous les avons usées en même temps que notre
planète. Sans sombrer dans la démagogie il paraît essentiel de
comprendre qu’il va falloir optimiser, économiser, nos ressources, de
tous types. Nous entrons dans une ère de la frugalité.

**Complexité, immatérialité, économies de ressources** : c’est le cadre
de nos nouvelles organisations.

Je défends une vieille idée assez commune : nos organisations ne doivent
pas être pensées comme un assemblage rationnel cartésien, mais comme un
tout organique, mouvant. Ce n’est ni original ni nouveau. C’est
l’approche organique et complexe défendue par beaucoup depuis des
années. Cependant, si l’idée n’est pas nouvelle, la situation l’est
devenue. Nous y sommes : ce moment où le concept est devenu réalité,
nous avons besoin de (re)penser nos organisations selon cette idée.
J’espère vous expliquer ce qu’est une organisation vivante et comment
transformer nos organisations pour aller vers ces nouvelles
organisations.

## Une organisation organique ?

Mais d’abord qu’est-ce qu’une organisation ?

L’organisation désigne l’action, mais aussi le résultat de l’action de
celui qui délimite, structure, agence, répartit, articule[^7].

Elle traduit, au niveau des moyens, l’expression d’une volonté par :

-   La manière dont les différents éléments d’un ensemble complexe, d’une société, d’un être vivant sont structurés et articulés ; ou 

-   Une hiérarchie et/ou un agencement concret des organes ou moyens requis.

## Structure

### Un schéma de pensée non cartésien

Ainsi, c’est trop aisé de ne penser nos organisations que comme des
structures géométriques simples, claires, sous-entendu rectilignes,
carrées. C’est une facilité de l’esprit, un déni de la réalité.
Naturellement pour le management en place, la direction, la hiérarchie,
les décideurs, du moins ceux qui s’obstinent dans cette vision simpliste
c’est du confort. On décrit nos organisations comme le dessin à droite,
bien ordonné, personne n’est dupe, c’est un déni de réalité.

![Sapin](/media/sapin.jpg)

Sapin - Image twitter, @organizedthings

Le sapin bien rangé dans la case de droite est tout simplement mort,
donc inerte, il ne produit plus rien. Levez la tête dans les étoiles,
voilà une organisation à grande échelle ! Là aussi inutile d’y chercher
un quelconque organigramme ou diagramme qui puisse correspondre à l’une
de nos organisations. Ce qui est bien rangé, à droite dans le dessin,
c’est de la science-fiction.

![Espace](/media/espace.jpg)

Espace - Image twitter, @organizedthings

Ne cherchez pas la vie, l’évolution, dans des éléments statiques,
inertes. Même chez nous, les humains, toute vie et de facto toute
organisation ne s’embarrassent pas de schémas confortables basés sur des
tracés rectilignes. Dans la case de droite, cet enfant semble bien
malheureux.

![Enfant](/media/enfant.jpg)

Enfant - Image twitter, @organizedthings

![fun.jpg](/media/fun.jpg)

Piscine - Image twitter, @organizedthings

Á aborder nos organisations comme des machines, c’est la folie qui nous
guette.

Il ne faudrait pas aborder nos organisations avec une structure
classique, organigramme à l’appui, mais avec un schéma de pensée. Mais
si donc ce schéma ne s’appuie pas sur nos dérives cartésiennes, sur quoi
peut-il bien se baser ?

Les motifs que nous prenons en référence ne sont manifestement pas les
bons. Mais où trouver les bons schémas ? Simplement en ouvrant les yeux
et en observant ce qui fonctionne autour de nous, dans la nature, comme
nous venons de le faire sur certaines des images précédentes (l’espace,
le sapin, par exemple).

Si la nature, l’organique, parce qu’elle répond au monde complexe
actuel, doit de nouveau nous servir d’exemple, où chercher ? La nature
elle-même nous apprend qu’il n’y a pas de solution, du moins de bonne
solution, elle nous apprend l’adaptation. Et, elle propose des choses
très différentes, aux antipodes l’une de l’autre.

Penchons-nous sur deux extrêmes pour évoquer des métaphores
d’organisations vivantes : d’un côté le cristal, probablement l’un des
éléments les plus organisés (pour l’esprit cartésien qui nous habite),
et de l’autre la termitière qui recèle quelques surprises.

### Le cristal

Nous évoquons le cristal comme structure naturelle organisée, car sa
caractéristique est d’être périodique, c’est-à-dire qu’il se bâtit sur
la même combinaison qu’il répète, périodiquement[^8]. On retrouve là une
vieille ambition de nos organisations : reproduire le même schéma,
presque à l’infini, pour grandir, grossir, et faciliter sa gestion
(reproduction des mêmes gestes).

De fait, le cristal est utilisé comme un conducteur efficace d’énergie
,car la périodicité de sa structure provoque une économie substantielle.
En rencontrant les mêmes schémas, on économise un coût énergétique de
communication qui évite une adaptation constante. La nature, chez le
cristal, nous enseigne qu’en répétant des structures identiques on
économise ou on optimise le coût en énergie de communication.

Au-delà de cette périodicité qui le rend si cartésien, le cristal
demeure un élément complexe, non prédictible dans sa forme.

> « La cristallisation est le passage d’un état désordonné liquide,
gazeux ou solide à un état ordonné solide, contrôlé par des lois
complexes. La fabrication d’un cristal se déroule sous le contrôle de
différents facteurs tels que la température, la pression, le temps
d’évaporation ». -- Geowiki[^9]

#### ![cristal-sur-eau.jpg](/media/cristal-sur-eau.jpg)

#### Le cristal est fainéant

> « Pour comprendre mieux la raison de la forme d’un cristal, il ne faut
pas ignorer que la nature est fainéante et que lorsqu’on lui laisse le
choix et le temps, elle choisit toujours les solutions qui lui coûtent
le moins d’énergie. Ainsi, la forme d’un cristal témoigne des conditions
physiques qui prévalaient lors de sa croissance, car c’est la forme qui
a coûté le moins d’énergie ». -- Geowiki

Ainsi si le cristal répète une structure à l’infini, cette structure est
définie par son contexte. Ce n’est pas une forme prédéterminée. La
fainéantise est une aubaine, là aussi, en termes de coût énergétique. La
nature, chez le cristal, nous enseigne que la structure prendra la forme
la plus économique en terme énergétique. La frugalité est clef.

#### Stabilité du cristal

>« Après cette ébauche de raisonnement, il faut savoir que les choses ne
sont pas toujours aussi simples. Une face cristalline est aussi une
discontinuité, une surface où les atomes ne sont pas liés chimiquement
comme les atomes du cœur du minéral. Cela revient en quelque sorte à
casser des liaisons (l’approximation est un peu brutale, mais correcte).
Or dans le milieu de croissance, des espèces solubles sont capables de
s’absorber à la surface en se liant faiblement avec les atomes de la
surface. On peut stabiliser des plans qui coûtent normalement très
chers, et en déstabiliser d’autres qui sont normalement faciles à payer.
Ainsi on modifie la forme finale du cristal cette fois-ci sans modifier
les conditions physiques. Ces phénomènes de stabilisation peuvent ainsi
induire des morphologies cristallines qui ne devraient pas être
habituellement observées. Quand des espèces chimiques s’absorbent en
surface et donc stabilisent les plans présentés, ces surfaces sont moins
accessibles pour continuer la croissance suivant ces plans. Ainsi ce
sont les plans les moins stables, ceux qui ont peu, voire pas, de
molécule en surface, qui croissent le plus vite.Comme ces plans sont
instables, ils finiront par ne plus être représentés lors de la
croissance, privilégiant ainsi la croissance lente des plans les plus
stables ». -- Geowiki[^10]

Si nous continuons notre analogie, on sera surpris par la facilité à
stabiliser certains pans de l’organisation, et à l’inverse en
déstabiliser certains –pourtant pensés comme stables– de façon
inattendue. La mise à l’échelle est plus aisée avec les éléments qui
n’ont pas encore été stabilisés dans l’organisation. La dynamique de la
mise à l’échelle se ralentit nécessairement quand toutes les parties
seront stabilisées. Faudrait-il donc presque encourager par moment la
déstabilisation ?[^11] La nature, chez le cristal, nous enseigne que ce
qui est stabilisé demande un coût énergétique plus fort pour être
changé, et donc que ce sont les parties non stables qui évolueront le
plus vite, et le plus probablement.

![snowflakes.jpg](/media/snowflakes.jpg)

#### La troncature du cristal

La troncature est une des sources de modification du cristal.

> « Une troncature c’est le remplacement d’un sommet ou d’une arête d’un
cristal par une face ». -- Geowiki[^12]

> « Une des causes modifiant la forme initiale des cristaux est la
troncature ». -- Geowiki[^13]

Est-ce une métaphore qui nous indiquerait que c’est en changeant la
tête, le sommet, que l’organisation prendra une nouvelle forme ? Faut-il
voir là l’idée que c’est en redonnant un pouvoir d’auto-organisation à
chaque groupe, en supprimant la tête, qu’on lui permet de se
s’organiser, d’une façon complexe, de se cristalliser au mieux avec le
reste des parties de la compagnie ?

#### Gestion des dépendances : les macles ?

> « Selon la position des cristaux on distingue des macles par accolement
et des macles par pénétration (ou interpénétration - plus ou moins
complète). On parle de macles simples lorsque deux cristaux sont
associés, et de macles multiples lorsque plus de deux sous-individus
composent la macle, on en arrive parfois, par multiplication des
sous-individus impliqués, à des macles cycliques, alors que lorsque la
formation de macle se répète à l’intérieur d’un groupe, on obtient des
macles polysynthétiques (répétées) ».
—- Geowiki[^14]

![troncature.jpg](/media/troncature.jpg)

Si nous nous amusons à pousser l’analogie loin, les macles seraient ces
amas d’équipes associées...Le vocabulaire utilisé par la science des
cristaux rappelle furieusement les situations vécues.

#### Les enseignements du cristal

En reproduisant le même schéma, on réduit le coût énergétique de
communication. Mais ce schéma est déterminé par le contexte, et
notamment par son coût : c’est le schéma qui coûte le moins cher en
énergie qui est plébiscitée par la nature. Évolutions et améliorations de
la structure du cristal sont plus simples dans les parties non achevées,
encore désordonnées.

L’analogie avec nos organisations paraît claire et réaliste.

### La termitière

De l’autre côté du cristal cartésien, de notre imaginaire sur les formes
de la nature, observons la termitière. Pourquoi à l’autre bout ? Car si
le cristal pouvait paraître organisé naturellement (toutes ces belles
structures, ces approches fractales), la termitière semble complètement
empirique. Et elle l’est : sa construction débute ...au hasard. Mais
comme la nature fait bien les choses, les termitières sont les plus
grandes constructions qui ne soient pas humaines. Le cristal est un
matériau, la termitière est une construction. Les mettre sur la même
échelle est peut-être osé.

Voyez la termitière comme une construction très intéressante. L’une des
plus intéressantes si on l’observe avec la réflexion de J.Scott Turner
en tête :

> « Je souhaite creuser une idée : que les édifices construits par les
animaux sont en fait des parties externalisées de leur physiologie ». «
Une termitière est comme un organisme vivant, dynamique et constamment
maintenu »[^15].

![termitiere.jpg](/media/termitiere.jpg)

Dans quel but les termites fabriquent-elles ces extensions
physiologiques que sont leurs termitières ? Toujours pour des questions
énergétiques, pour économiser de l’énergie, stocker de l’énergie,
déporter un effort énergétique.

Est-ce que l’on peut faire une analogie avec nos organisations ?
Probablement. Quelles seraient les énergies évoquées ? D’abord bien
évidemment la valeur créée par l’organisation, premier flux d’énergie.
Mais aussi l’optimisation des énergies entrantes : celles de ses
collaborateurs (implication, engagement), celles de la matière première
qu’elle utilise (électricité, etc.). Une énergie entrante, une valeur
sortante. On ne parle que de flux, de transformation.

#### C’est l’occasion qui fait le larron

Tout comme le cristal était fainéant, la termitière naît d’une
opportunité. C’est l’occasion qui fait le larron. Les termites font des
tas, et quand une opportunité se présente pour bâtir la termitière (des
tas qui peuvent être assemblés), c’est la termitière qui est bâtie. La
nature, là aussi avec la termitière, nous enseigne que la forme et la
structure ont pour but une optimisation énergétique (ici il est question
de température).

[^7]: [Wikipedia sur organisation](http://fr.wikipedia.org/wiki/Organisation)
[^8]: Conversations avec mon frère, chercheur, spécialiste du cristal
[^9]: [Comment se forment les cristaux](http://www.geowiki.fr/index.php?title=Comment_se_forment_les_cristaux)
[^10]: [Comment se forment les cristaux](http://www.geowiki.fr/index.php?title=Comment_se_forment_les_cristaux)
[^11]: Dans les commentaires Nicolas Delahaye indique que pour plus approfondir cette approche : *Nudge marketing: Comment changer efficacement les comportements* de Eric Singler et *Makestorming: Le guide du corporate hacking* de Marie-Noéline Viguié, Stéphanie Bacquere.
[^12]: [Troncatures](http://www.geowiki.fr/index.php?title=Troncatures)
[^13]: [Troncatures](http://www.geowiki.fr/index.php?title=Troncatures)
[^14]: [Les macles](http://www.geowiki.fr/index.php?title=Les\_macles)
[^15]: J.Scott Turner, The extended organism, "I actually wish to explore an idea : that the edifices constructed by animals are properly external organs of physiology."
