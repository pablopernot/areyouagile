---
title: "Les organisations vivantes"
url: /organisations-vivantes/chap7/
subtitle: "Conclusion"
---

# Chapitre 7 : Conclusion

> Ils ne savaient pas que c’était impossible, alors ils l’ont fait – Mark Twain

DYNAMIQUE

eXTENSION (IMMATÉRIEL)


### Révisions

   Date  |   Objet 
   ------|-------- 
  2014-2016  | Premiers travaux, premiers écrits 
  2016-10-21 | Initialisation du document du gdrive 
  2016-10-31 | Ajout bibliographie, webographie, remerciements, relecture rapide : je dois finir les derniers chapitres 
  2016-11-01 | Intégration de certains commentaires en note de bas de page 
  2017-08-16 | Prise en compte de commentaires 
  2017-11-11 | Reprise du texte, retour markdown / web blog. 
  2017-11-12 | Début de nettoyage d'hiver 
 





<!--
stigmergie habitude

Trouver une représentation visuelle de ces organisations.


# Bibliographie

The Machine that changed the world, James P. Womack, Daniel T. Jones,
and Daniel Roos.

The Mythical Man-Month : Essays on Software Engineering, Fred Brooks
([*https://archive.org/details/mythicalmanmonth00fred*](https://archive.org/details/mythicalmanmonth00fred))

The extended organism, J Scott Turner

Social : Why Our Brains Are Wired to Connect, Matthew D. Lieberman

Brain rules, John Medina

“Entre le cristal et la fumée. Essai sur l'organisation du vivant”,
Henri Atlan

Chip & Dan Heath, “Switch : How to Change Things When Change Is Hard”

“Thinking, fast and slow”, Daniel Kahneman

“To the desert and back : The Story of One of the Most Dramatic Business
Transformations on Record”, Philip H. Mirvis

“Petit traité de manipulation à l’usage des gens honnêtes”,
Robert-Vincent Joule

Webographie
===========

[*http://fr.wikipedia.org/wiki/Organisation*](http://fr.wikipedia.org/wiki/Organisation)

Les images de @organizedthings (twitter)

[*http://www.geowiki.fr/index.php?title=Comment\_se\_forment\_les\_cristaux*](http://www.geowiki.fr/index.php?title=Comment_se_forment_les_cristaux)

[*http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf*](http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf)

[*http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/*](http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/)

« Blah blah blah », Dan Roam,
http://changethis.com/manifesto/show/88.01.BlahBlahBlah

Harrison Owen, Dancing with Shiva
([*https://www.youtube.com/watch?v=APD7oQ3xrSA*](https://www.youtube.com/watch?v=APD7oQ3xrSA))

OpenSpace Agillity :
[*http://openspaceagility.com/*](http://openspaceagility.com/)

Musique pendant l’écriture
==========================

Tony Joe White, “Swamp Fox”, The definitive collection 1968-1973

King Crimson, XXI century Schiso Man

Noir désir : 666667 Club

Joe Mascis “several shades of why”

Remerciements 
==============

Aux lecteurs, correcteurs, conseillers, et autres qui ont laissé des
commentaires constructifs :

(par ordre alphabétique des prénoms) Merci à eux.

Benjamin Rechou

Frank Taillandier

Nicolas Delahaye

Paul-Georges Crismer

Dominique Suripad

Nils Lesieur

Denis Jallet

Nicolas Delahaye

Steeve Evers


This work is licensed under a [Creative Commons
Attribution-NonCommercial-NoDerivatives 4.0 International
License](http://creativecommons.org/licenses/by-nc-nd/4.0/).

[^1]: Je préfère mentor (« Guide attentif et sage, conseiller
    expérimenté » dit le Larousse) à coach (ambivalent et prétentieux à
    mes yeux, je ne me sens pas de te changer, ni la compétence, ni le
    droit, ni l’envie).

[^2]: The Machine that changed the world, James P. Womack, Daniel T.
    Jones, and Da-

    niel Roos.

[^3]: J’insiste volontairement sur l’aspect purement économique. Ce
    n’est pas nécessairement ma tasse de thé mais j’évolue moi-même en
    environnement concurrentiel et je sais que c’est là le nerf de la
    guerre pour les entreprises qui font appel à moi. La bonne nouvelle
    est que pour faire réussir son entreprise aujourd’hui il faut
    engager et responsabiliser les personnes comme a pu le faire Toyota.

[^4]: https://hbr.org/2014/11/a-primer-on-measuring-employee-engagement

[^5]: The Mythical Man-Month : Essays on Software Engineering, Fred
    Brooks

[^6]: [*http://www.areyouagile.com/2016/07/faciliter-ou-batir/*](http://www.areyouagile.com/2016/07/faciliter-ou-batir/)

[^7]: http://fr.wikipedia.org/wiki/Organisation

[^8]: Conversations avec mon frère, chercheur, spécialiste du cristal

[^9]: [*http://www.geowiki.fr/index.php?title=Comment\_se\_forment\_les\_cristaux*](http://www.geowiki.fr/index.php?title=Comment_se_forment_les_cristaux)

[^10]: http://www.geowiki.fr/index.php?title=Comment\_se\_forment\_les\_cristaux

[^11]: Dans les commentaires Nicolas Delahaye indique que pour plus
    approfondir cette approche : *Nudge marketing: Comment changer
    efficacement les comportements* de Eric Singler et *Makestorming: Le
    guide du corporate hacking* de Marie-Noéline Viguié, Stéphanie
    Bacquere

[^12]: http://www.geowiki.fr/index.php?title=Troncatures

[^13]: http://www.geowiki.fr/index.php?title=Troncatures

[^14]: http://www.geowiki.fr/index.php?title=Les\_macles

[^15]: J.Scott Turner, The extended organism, *I actually wish to
    explore an idea : that the*

    *edifices constructed by animals are properly external organs of
    physiology.*

[^16]: Étonnamment ces macles ressemblent beaucoup aux guildes proposés
    par une

    organisation comme Spotify.

[^17]: “Et pourquoi pas parler des villes du moyen-Age ? Un exemple plus
    proche de chez "nous" (càd dont nous pouvons expérimenter encore les
    traces) et associé à moins d'interprétations aujourd'hui (pauvreté,
    insalubrité) me paraîtrait plus concret.” m’écrit Paul-Georges
    Crismer. Certainement c’est une bonne idée. Il se trouve que je suis
    tombé sur un document qui a résonné en moi et qui parlait des
    bidonvilles. Les bidonvilles sont cependant plus liés à
    l’augmentation sans fin de notre population et à la fin proche de
    nos ressources, ils me semblent bien adaptés au propos.

[^18]: http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf

[^19]: http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf

[^20]: Fred Brooks, https://archive.org/details/mythicalmanmonth00fred

[^21]: Voir le ’Plaisir d’appartenance à un groupe
    ([*http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/*](http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/))
    suivant concernant les écrits de Christopher Allen et Robin Dunbar

[^22]: « Social : Why Our Brains Are Wired to Connect », Matthew D.
    Lieberman

[^23]: ([*http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/*](http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/)

[^24]: “En lisant ces paragraphes j'ai eu un choc, parce que j'aime la
    cohérence. Pourquoi écrivez-vous ce livre alors? Pourquoi ces
    digressions sur les cristaux et les termitières ? En effet, une
    image c'est 300 à 3000 mots. Quelle valeur supplémentaire ont donc
    les mots pour que vous en veniez à écrire un livre ? Bref, j'aime
    les discours mesurés. Selon moi, chaque médium a son utilité.
    D'ailleurs, comment pourrais-je vous communiquer ce que je vous
    écris avec un dessin sans que vous interprétiez ? Le dessin aussi
    peut introduire des ambiguïtés. Bref, je que j'aimerais trouver dans
    ce que vous écrivez à propos des mots ce sont les circonstances dans
    lesquelles un dessin vous semble plus approprié.” S’insurge
    Paul-Georges Crismer en commentant ces mots. Je suis bien d’accord.
    Lorsque je ne serai plus incohérent ou paradoxal je serai
    certainement mort. Les mots ont la force de rester, de m’aider à
    organiser ma pensée. Si je pousse dans l’autre sens c’est que dans
    notre culture la culture de la puissance de l’écrit est considérable
    et qu’il faut savoir la mettre en perspective.

[^25]: « Blah blah blah », Dan Roam,
    http://changethis.com/manifesto/show/88.01.BlahBlahBlah

[^26]: Brain rules, John Medina

[^27]: Brain rules, John Medina

[^28]: Harrison Owen, Dancing with Shiva
    (https://www.youtube.com/watch?v=APD7oQ3xrSA)

[^29]: “Entre le cristal et la fumée. Essai sur l'organisation du
    vivant”, Henri Atlan

[^30]: “Social”, Liebermann

[^31]: Jeff Patton, User Story Mapping. //TODO aller chercher la source
    originale

[^32]: Chip & Dan Heath, “Switch : How to Change Things When Change Is
    Hard”

[^33]: “Thinking, fast and slow”, Daniel Kahneman

[^34]: “Petit traité de manipulation à l’usage des gens honnêtes”,
    Robert-Vincent Joule

[^35]: “To the desert and back : The Story of One of the Most Dramatic
    Business Transformations on Record”, Philip H. Mirvis

[^36]: OpenSpace Agillity : http://openspaceagility.com/
-->