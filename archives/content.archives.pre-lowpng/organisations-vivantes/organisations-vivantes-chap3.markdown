﻿---
date: 2020-03-20
title: "Les organisations vivantes"
url: /organisations-vivantes/chap3/
subtitle: "Dynamique des formes"
---

# Chapitre 3 : Dynamique des formes

> « The stable state of a living organism is to be dead »
> -- Schrödinger

De cette irruption dans le monde de la nature on va garder deux éléments
en tête : d’une part les formes prise par la nature sont souvent
dépendantes du contexte et au delà un motif macroscopique varient
grandement. D’autre part concernant les structures tout est question
d’énergie, d’optimisation énergétique.

Concernant la question des formes et les apprentissages pour celles de
nos organisations, nous ferons un détour par la théorie des catastrophes
de René Thom, disons surtout qu’elle servira d’alibi pour sortir du
cartésien, et penser en verbe (dynamique, énergie encore).

Les aspects d’optimisation énergétique me paraissent essentiels. Ils
sont partout dans nos organisations : optimisation du flux de
communication, optimisation du flux de création de valeur, optimisation
de l’implication (énergie) des personnes, etc...Enfin, et non des
moindres, optimisation des ressources énergétiques pures, celles de la
planète.

## La théorie des catastrophes

J’avance en terrain miné. La théorie des catastrophes est sujette à de
nombreuses critiques ; Je ne suis pas du tout (du tout) mathématicien.
Je souhaite juste profiter de cette théorie, et surtout des images
qu’elle véhiculent pour sortir de la formule et en appeler au verbe.
Plus clairement : peu importe de comprendre vraiment comment appliquer
cette théorie qui ne s’applique d’ailleurs pas vraiment (d’où le feu
nourri de critiques). Ce qui m’importe c’est de commencer à penser
différemment les formes de nos organisations : en matière qui se saisit
des opportunités, une matière non pas carrée, mais opportuniste en
suivant des principes et non des règles trop précises. Elle est souvent
différente tout en étant commune.Elle ne se pense pas en résolution
d’équation ou de calcul, mais en énergie et ainsi en dynamique. Les
verbes de la théorie des catastrophes appellent à comprendre son
organisation non pas comme un assemblage de calculs, mais comme un magma
soumis à des dynamiques.

### Théorie des changements de forme

Il faut plutôt comprendre la théorie des catastrophes comme la théorie
des changements de forme. Une catastrophe, pour René Thom, c’est un
événement qui surgit, un changement de situation : attractions, forces
en présence qui déclenchent un changement de forme. René Thom distingue
sept types de catastrophes élémentaires et des modèles associés. Une
critique forte contre cette théorie est justement ce qui m’y plaît : son
ouverture aux sciences molles, on ne va pas trouver simplement des
formules (lesquelles je suis bien incapable de comprendre), mais surtout
des verbes pour représenter ces forces et changements en action.

Au lieu de catastrophes disons singularités : quand il se passe quelque
chose de singulier la situation change. Selon le nombre de paramètres et
de variables, tant que nous restons dans un domaine élémentaire, Thom
indique sept singularités et les décrit, nous les verrons plus bas.
Zeeman, un adepte de Thom, propose de nombreuses applications de la
théorie des catastrophes : aux révoltes dans les prisons, au krach
boursier, au comportement du chien en colère ou apeuré, etc. Donc
naturellement sociologie et psychologie se sont emparées de la théorie
des catastrophes, des changements de forme, et nous essayerons de
l’utiliser pour penser les mouvements et changements de forme d’une
organisation. On lit cependant souvent que l’application quantitative de
la théorie des catastrophes aux sciences humaines est un exercice
dangereux, je crois qu’il faut juste faire attention entre l’intuition,
l’inspiration et jouer aux apprentis sorciers.

Je vais y rechercher un ensemble de formes et de mouvements qui nous
permettent de mieux percevoir l’organisation, et ainsi proposer un
outillage alternatif à la vision hiérarchique, plate et mécanique que
l’on nous offre habituellement. Je vais y rechercher des verbes et des
mots, des métaphores que l’on peut intégrer beaucoup plus facilement
comme motifs organisationnels (vous verrez ci-dessous le « pli » ou « la
fronce »). Je vais y rechercher une impression, une esquisse de tableau
japonais, un côté contemplatif dans les grandes lignes des formes des
organisations. Je ne tiens pas à retomber dans le travers d’une règle
qui dit que si telle singularité se produit alors on obtiendra
nécessairement tel type d’organisation, juste une compréhension
d’ensemble des dynamiques en jeu. Pour citer Thom : « Cela va à
l’encontre de la philosophie dominant actuellement, qui fait de
l’analyse d’un système en ses ultimes constituants la démarche première
à accomplir pour en révéler la nature. Il faut rejeter comme illusoire
cette conception primitive et quasi cannibalistique de la connaissance,
qui veut que connaître une chose exige préalablement qu’on la réduise en
pièces ». Nous sommes dans l’avènement d’une approche holistique de
l’organisation. Il faut voir le tout. D’où une approche par silhouette,
par dynamiques. C’est cela que Thom met en exergue il y a 40 ans.

#### Les catastrophes élémentaires et les organisations

![catastrophes.jpg](/media/catastrophes.jpg)

Dans le tableau ci-dessus, vous voyez les sept catastrophes élémentaires.
Chaque catastrophe, singularité, est déclenchée par un certain nombre
de paramètres et provoque certaines variables, formes. On dit qu’il y a
sept catastrophes élémentaires, élémentaires, car on considère peu de
paramètres en entrée (de un à deux). Vous observerez aussi -- dans le
tableau toujours -- que l’on peut adjoindre à chacune des catastrophes
une interprétation spatiale et temporelle (par le biais d’un verbe).
Thom cherche délibérément — en accord avec les présocratiques — à
exprimer ses idées au travers de mots et d’images de la vie.

> « Nos modèles, écrit Thom, attribuent toute morphogenèse à un
conflit, à une lutte entre deux ou plusieurs attracteurs ; nous
retrouvons ainsi les idées (vieilles de 2 500 ans !) des premiers
présocratiques, Anaximandre et Héraclite. On a taxé ces penseurs de
confusionnisme primitif, parce qu’ils utilisaient des vocables d’origine
humaine ou sociale comme le conflit, l’injustice … pour expliquer les
apparences du monde physique. Bien à tort selon nous, car ils avaient eu
cette intuition profondément juste : les situations dynamiques régissant
l’évolution des phénomènes naturels sont fondamentalement les mêmes que
celles qui régissent l’évolution de l’homme et des sociétés, ainsi
l’usage des vocables anthropomorphiques en Physique est foncièrement
justifié ».

#### Catastrophes élémentaires par ordre de complexité croissante

**Le pli**

![pli.jpg](/media/pli.jpg)

La première des catastrophes est le « pli ». 
Imaginez un drap que vous pliez. Il y a un « bout », une « fin ». On
y associe le verbe « finir », ou « commencer », au pli, cela finit ou
commence. Il y a un seul paramètre, on est d’un côté ou de l’autre du
pli, donc un seul résultat, une seule variable de sortie. Si j’essaye de
prendre une image : encore une fois, c’est cela qui me plaît dans
l’approche de Thom, sa volonté de revenir aux présocratiques qui
ramenaient leurs analyses mathématiques à des images que l’on peut
naturellement saisir. Ainsi donc comme image on observe la communication
au sein d’une équipe. Cette équipe grossit, grossit, grossit : c’est le
paramètre, on ajoute 1 membre à l’équipe régulièrement. Soudain on
atteint un point, la catastrophe, la singularité, et la communication
change complètement : c’est le pli. On est passé de l’autre côté. Dans
les cas simples de bord, de bout, l’organisation opère un pli, mais un
pli net. Il se produit une singularité cela déclenche un résultat (le
pli).

**La fronce**

![fronce.jpg](/media/fronce.jpg)

La fronce c’est le pli du tissu dans votre rideau de douche. Mais ce pli
est une courbe. On imagine une bille qui passe d’un côté ou de l’autre
d’une petite colline. Tout dépend des paramètres qui propulsent la bille
: elle passera vite ou montera pour redescendre sans passer le Rubicon.
C’est avec la catastrophe de la fronce que Zeeman applique la théorie au
chien : entre peur et agressivité : quels paramètres et selon quelles
forces le chien va passer de la peur (la fuite) à l’agressivité
(l’attaque). On entre dans l’aspect mathématique de la théorie, et je
suis incapable de le maîtriser. Mais je peux m’interroger sur les forces
et les attractions qui me font évoluer sur la fronce et basculer d’un
côté ou de l’autre. Par exemple : la fronce à deux paramètres en entrée,
on pourrait dire encore la taille d’une équipe, et disons sa
co-localisation. C’est-à-dire on a plus ou moins de personnes dans une
équipe, et ils sont plus ou moins co-localisés. Avec ces différents
paramètres et leur intensité, on décrira une fronce plus ou moins abrupte
(une variable en sortie) qui décrira le fonctionnement de la
communication au sein de l’équipe. Va-t-elle doucement s’effriter,
va-t-elle subitement se briser, à quel moment doit-on engendrer une
nouvelle équipe.

Les mots clefs sont capturer, casser : on franchit le sommet de la
fronce, en passant de l’autre côté : on engendre, on devient, on unit.
Je parle d’équipe et de communication, vous pouvez envisager l’expansion
des filiales au travers d’un continent, les différents départements
d’une solution, la vie d’un produit d’entreprise, etc.

**La queue d’aronde (d’hirondelle)**

![aronde.jpg](/media/aronde.jpg)

Penser à la queue d’une hirondelle : au
croisement, il y a une sorte de superposition, que l’on peut percevoir
comme un déchirement ou une — au contraire— une couture. Il y a trois
paramètres en entrée et une variable en sortie (j’écris cela pour ceux
qui y comprennent quelque chose). On évoque donc des problématiques de
superposition, de recouvrement, de tension, de déchirement au sein de
l’entreprise. Deux gammes produit qui se recouvrent, doit-on les
fusionner, les coudre ? A quel moment ce produit, cette gamme, se
déchire en deux (idem pour les équipes, encore). Je ne vais pas creuser
plus et je vous laisse trouver les paramètres qui vont régir ce
changement de forme, avec la queue d’aronde il doit y en avoir trois.

**Le papillon**

![papillon.jpg](/media/papillon.jpg)

Je vais désormais me limiter aux verbes
associés, les formes deviennent compliquées et je n’ai pas encore assez
intégré le modèle associé... Ce qui est intéressant dans le papillon
(quatre paramètres ! et toujours une variable en sortie) c’est l’idée
d’une surface cachée, recouverte, une poche, qui s’écaille, s’exfolie,
c’est-à-dire s’effeuille par lamelles. Donc elle se vide ou se remplit.
Encore une métaphore, mais aussi donc une théorie permettant
d’appréhender les mouvements de l’organisation.

Les trois dernières catastrophes élémentaires sont un peu plus complexes
,car elles proposent deux variables en sortie.

**La vague, le poil, le champignon**

![ombilic.jpg](/media/ombilic.jpg)
![ombilic2.jpg](/media/ombilic2.jpg)
![ombilic3.jpg](/media/ombilic3.jpg)

Pareil, pour l’instant j’ai l’impression qu’il devient vain pour moi de
comprendre les aspects mathématiques et le calcul de ces différentes
formes. Je m’intéresse uniquement aux verbes qui peuvent me donner une
façon d’appréhender cette théorie et de la projeter sur mon monde.

-   La vague(ombilic hyperbolique) : Lisez briser, s’effondrer, recouvrir pour la vague. Pensez au rachat de filiale, au recouvrement de secteur, mais aussi là l’effondrement soudain d’un vague ayant atteint sa crête, attrapez la notion de voûte.
-   Le poil (ombilic elliptique) : Piquer, pénétrer, boucher.
-   Le champignon (ombilic parabolique) : Briser, éjecter, lancer, percer, couper, lier, ouvrir, fermer.

#### Que faire de tout cela

Pensez votre organisation comme un tout, régie par des événements qui
déclenchent des formes et des dynamiques. Si vous êtes plus
mathématicien que moi vous pouvez utiliser des formules, mais je pense
que les tensions suffisent à comprendre que l’on approche, peut-être,
d’une singularité, et donc d’un changement de forme.

Gardez à l’esprit que les formes les plus adaptées à votre organisation
sont probablement les plus économes en énergie. Pour cela, laissez la
forme naturelle émerger. Employé ainsi, le mot naturel sous-entend la
plus simple, la plus opportune, celle qui demande le moins d’énergie, et
propose la meilleure optimisation du contexte.

En ce sens le hasard qui préfigure une termitière souligne l’intérêt de
laisser le temps à l’émergence. Rien contre la répétition de schéma, qui
seront bon pour propager à moindre coût votre énergie (communication,
savoir-faire, etc.), mais sachez placer le curseur au bon endroit : ce
sont les morceaux les moins figés, achevés de votre organisation qui
seront les plus simples à faire évoluer.

Pensez aux macles des cristaux pour penser des courants transverses dans
l’organisation (étonnamment ces macles ressemblent beaucoup aux guildes proposées par une organisation comme Spotify). 
