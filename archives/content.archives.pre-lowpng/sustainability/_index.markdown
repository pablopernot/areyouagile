---
type: section
layout: sustainability
title: Viabilité
url: /viabilite/
sidebar: true
---


# Ici

J'observe mes premiers pas tardifs vers demain. 

L'objectif : travailler sur des produits écoresponsables.

Mini mini mini périmètre : mon blog.  

## Concernant le blog 

### C'était déjà le cas : 

- Pas de pisteur, tracker
- Pas de cookies
- Site statique (depuis longtemps grâce à Frank Taillandier, Pelican puis Hugo)

### Et maintenant : 

- Plus de favicon
- Plus de logo sur le site 
- Plus de tags

### Sur les images

- De 214mo à 34mo (248mo en tout mais 34mo en visualisation directe, 214mo d'images dispos si besoin d'une meilleure qualité). 

## À faire 

- Curation des contenus que je juge devenus inutiles 
- Politique et réduction des tailles des images 
- Lien vers des vidéos : ne pas charger 
- Gérer le sous-dossier ["héros"](/heros/), gérer le sous-dossier ["pablo.pernot.free.fr"](/pablo.pernot.free.fr/)

## Sources

- https://solar.lowtechmagazine.com/fr/2018/09/how-to-build-a-lowtech-website.html
- https://solar.lowtechmagazine.com/
- https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/


## Consolidation des informations