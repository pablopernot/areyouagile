---
title: Matériel
url: /materiel/
alchemy: 2
---

# Vidéos 

* 2021, [This is not scrum or agile (ceci n'est pas du scrum / agile)](https://www.youtube.com/watch?v=cXK0T-8R73g)
* 2021, [Ceci n'est pas du scrum, keynote agile tour Montpellier](https://www.youtube.com/watch?v=VV9pY0Yk4Rs)
* 2021, [From basics to foundation / Frug'Agile Armenia](https://www.youtube.com/watch?v=FAIc7q3TqmQ)
* 2021, [Roadmaps & dépendances](https://www.youtube.com/watch?v=qkSWp36USB4)
* 2021, [Agilité, regard critique sur les dernières années. Frug'agile.](https://www.youtube.com/watch?v=ykqERA7a9yk)
* 2020, [Storytelling, mémoriser et transmettre](https://www.youtube.com/watch?v=uOlNwFaqBXA) 
* 2020, [Meetup Entreprises complexes, partie 2](https://www.youtube.com/watch?v=Iq0byH0lR-w)
* 2020, [Meetup Entreprises complexes](https://www.youtube.com/watch?v=e9c4hu_lg-Y)
* 2020, [Cynefin, décider dans la complexité](https://www.youtube.com/watch?v=nhLkuhTjTAM)
* 2020, [Conversation autour du clean coaching](https://www.youtube.com/watch?v=RY_4iopXzB4)
* 2020, [Mind maps : Impact map & Agile strategy map](https://www.youtube.com/watch?v=X1DlbPInh1s)
* 2020, [Travail à distance, équipes distribuées](https://vimeo.com/405493699)
* 2017, [Keynote Agile Grenoble, être agile et sur l'engagement](https://www.youtube.com/watch?v=pjH0x21pYA0)
* 2016, [Open Adoption, NCrafts 2016](https://vimeo.com/167722770)
* 2015, [Lean Topologies, Flowcon](https://www.youtube.com/watch?v=a9KrjWv_Rt8)
* 2015, [Pecha Kucha about Slums, ALE2015](https://www.youtube.com/watch?v=VQNQnHRo-b8)
* 2014, [Open Agile Adoption - Pablo Pernot et Oana Juncu](https://vimeo.com/104987420)
* 2013, [Keynote Agile Tour Toulouse, La horde agile, partie 2](https://vimeo.com/78405990)
* 2013, [Keynote Agile Tour Toulouse, La horde agile, partie 1](https://vimeo.com/78404401)
* 2013, [Keynote Agile Tour Montpellier, La horde agile](https://www.youtube.com/watch?v=a0cI-E603bI)
* 2012, [Agile Tour Montpellier, La stratégie du product owner avec Alexis Beuve](https://www.youtube.com/watch?v=g7mYDe3ZIcM)
* 2012, [Sudweb 2012, Marshmallow challenge](https://vimeo.com/53396148)
* 2011, [Sudweb 2011, Anatomie d'une mission agile](https://vimeo.com/53349009)


# Podcast

* 2019, [Révolutionner une entreprise, Da Scritch, Carré, Petit, Utile](https://cpu.dascritch.net/post/2019/03/21/Pablo-Pernot%2C-agent-provocateur-et-coach-agiliste-pour-BeNext)
* 2019, [La transformation commence par celles des managers, Thomas Wickham, Café Craft](https://www.cafe-craft.fr/34) 
* 2019, [La transformation commence par celles des managers, Questions du public](https://www.cafe-craft.fr/34-bonus)
* 2018, [Agilité à l'échelle, Café Craft, Thomas Wickham ](https://www.cafe-craft.fr/20)


# Slides 

* 2021, [Ceci n'est pas du scrum / agile](/2021/12/ceci-n-est-pas-du-scrum/)
* 2021, [Roadmaps & dépendances](http://pablopernot.fr/pdf/2021-roadmaps-dependances.pdf)
* 2017, [Perspective plutôt que framework : Agile Fluency, Agile France 2017](https://www.slideshare.net/pablopernot/perspective-plutt-que-framework-agile-fluency)
* 2016, [Open Adoption, Dare to invite, Ncrafts](https://speakerdeck.com/pablopernot/open-adoption-dare-to-invite)
* 2015, [Lean Topology, Flowcon 2015](https://www.slideshare.net/pablopernot/lean-topology-lean-kanban-fr-15)
* 2015, [Open Adoption, Darefest, Antwerpen](https://www.slideshare.net/pablopernot/open-adoption-darefest-2015) 
* 2014, [Transformation RH, agile & RH, avec Jas Chong](https://speakerdeck.com/pablopernot/transformation-rh)
* 2014, [Les organisations vivantes](https://speakerdeck.com/pablopernot/les-organisations-vivantes)
* 2014, [Open Agile Adoption avec Oana Juncu](https://speakerdeck.com/pablopernot/open-agile-adoption-scrumday-2014)
* 2013, [Storytelling Battle - Pablo Pernot & Oana Juncu](/pdf/storytellingbattle.pdf)
* 2012, [Agilité : Comment ne pas devenir un shadok](https://www.slideshare.net/pablopernot/agilit-comment-ne-pas-devenir-un-shadok)
* 2012, [Stratégie du product owner, avec Alexis Beuve](https://speakerdeck.com/pablopernot/strategie-du-product-owner)
* 2012, [Management visuel & cognitif](https://speakerdeck.com/pablopernot/management-visuel-and-cognitif)
* 2011, [Agile & CMMi, potion magique ou grand fossé ?](https://www.slideshare.net/pablopernot/agile-cmmi-potion-magique-ou-grand-foss)
* 2011, [Anatomie d'une mission agile](https://www.slideshare.net/pablopernot/anatomie-dune-mission-agile)

# Textes (au delà d'un article) et ouvrage 


### 2022 - Avril 

* [L'énigme RH](/pdf/enigme-rh.pdf) - Comment passer de bonnes soirées avec mes ami(e)s même en étant RH - 96 questions pour changer notre vie de RH

### 2021 - février - Agilité, regard critique sur les dernières années pour bien débuter les suivantes

* [Agilité, regard critique sur les dernières années pour bien débuter les suivantes](/pdf/2021-regard-agilite-20-ans.pdf) 

### 2019 - Kanban, l’approche en flux pour l’entreprise agile


{{< figure src=/images/kanban.png width=100px class=inline >}}

* Co-auteur avec Laurent Morisseau de [Kanban, l’approche en flux pour l’entreprise agile chez Dunod. EAN 9782100781058, juin 2019](https://www.dunod.com/sciences-techniques/kanban-approche-en-flux-pour-entreprise-agile).

{{< br >}}

### 2014-2017 - Texte inachevé sur les organisations vivantes 

- [Les organisations vivantes - texte inachevé, 2014-2017](/organisations-vivantes/)   

### 2014-2017 - Guide de survie à l'agilité et à Scrum 

* [Le guide de survie à l'agilité et à Scrum](/pdf/guiderapide.pdf). 

### 2013 La horde agile  

* [La horde agile, le mini livre, 2013](/pdf/lahordeagile.pdf), pdf, 42 pages, \~4mo
* [la horde agile, le mini livre (epub), 2013](/epub/la-horde-agile.epub),
    epub, 42 pages, \~3.5mo
* [Article associé](/2014/01/mini-livre-la-horde-agile/) avec les informations complémentaires


# Les ateliers 

### La scierie à pratiques, 2013-2016 

Avec Stéphane Langlois.  

* [La scierie à pratique évolue](/2016/02/la-scierie-à-pratiques-évolue/), 2016 
* [La scierie à pratiques le PDF](/pdf/scierie-a-pratiques.pdf), un jeu pour échanger sur nos pratiques, 2013.
* [Agile ADN, scierie à pratique première version](/2013/05/adn-agile-feedback/), 2013

### Le projet dont vous êtes le héros agile, 2012-2013 

Le projet dont vous êtes le héros agile avec [Bruno](http://jehaisleprintemps.net/) et [Antoine](https://blog.crafting-labs.fr/).

* [Le projet dont vous êtes le héros agile](/heros/)

### Peetic, 2012-2015

Un scénario qui permet d'outiller mes formations, on y retrouve pas mal d'éléments d'un projet/produit agile.
[Claude](http://www.aubryconseil.com/) m'a fait le plaisir d'en intégrer des morceaux dans son livre ("Scrum") et nous l'utilisons dans le [raid
agile](http://www.raidagile.fr).

* 2012 : [Episode 1](/2012/11/peetic/)
* 2012 : [Conversations](/2012/12/peetic-conversations/)
* 2014 : Un article de [l'idée au plan de livraison](/2014/06/de-lidee-au-plan-de-livraison/)
* 2015 : [Un peu de croquettes](/2015/05/peetic-2015-un-peu-de-croquettes/)


# Les vieux supports de formation 

### Vieux supports de formation, 2012-2013-2014. 

* [Support de formation "agile" généraliste sur 3 jours](/pdf/pratiques_de_lagilite_2_1_15.pdf), 12.5mo, pdf, 2012. 
* [Initiation agilité 1 journée](/pdf/initiation-agilite-0-6.pdf), 6.5mo, pdf, 2013.
* [Agilité pour les entrepreneurs, donnez l'initiative à vos équipes](/pdf/entrepreneurs.pdf), 7.2mo, pdf, 2014. 
* [Product Owner](/pdf/product-owner-2014-1-0.pdf), 8.5mo, pdf, 2014.
* [Kanban](/pdf/kanban-0-8.pdf), 12mo, pdf, 2014.
* [Lean Software Development](/pdf/LSD-08.pdf), 8.5mo, pdf, 2014.

### Modules sur une demi-journée

* [Vision & Sens](/pdf/vision-sens-1-1.pdf), 1.9m, pdf, 2013. 
* [Expression du besoin](/pdf/expression-du-besoin-1-1.pdf), 956ko, pdf, 2013.  
* [Introduction à Kanban](/pdf/kanban-1-3.pdf) , 2.2m, pdf, 2013.
* [Optimiser la valeur](/pdf/optimiser-1-1.pdf) (Value Stream Mapping), 683ko, pdf, 2013. 
* [Zoom Scrum](/pdf/zoom-scrum-1-1.pdf), 800ko, pdf, 2013.
* [Responsable produit](/pdf/responsable-produit-1-1.pdf), 2.8m, pdf, 2013. 
* Un [Scrumshot](/2012/11/scrumshot/) qui me permet de faire un panorama sympa sur Scrum, 2012. 

