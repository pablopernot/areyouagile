---
title: "Conférences"
url: /conferences/
sidebar: true
---

![ALE2015](/images/ale2015.png)

## Contenus pour interventions

### Agilité, engagé, modernité

**Durée** : 20 à 45 mn selon les besoins (session). 

**Public** : Tout public. 

**En une phrase** : Démystifier le terme agilité, comprendre les leviers de
l'engagement et l'implication. 

**Liens** : 

 * [Être agile](/2017/07/etre-agile/)
 * [Être engagé - partie 1](/2017/09/etre-engage-1/)
 * [Être engagé - partie 2](/2017/10/etre-engage-2/).
 * [Vidéo keynote Agile Grenoble 2017](https://www.youtube.com/watch?time_continue=1953&v=pjH0x21pYA0).

### La horde agile

**Durée** : ~ 45 mn (session). 

**Public** : Tout public. 

**En une phrase** : Tout ce que nous avons à apprendre de notre ancienne horde.

**Liens** : 

 * [La horde agile](/2014/01/mini-livre-la-horde-agile/)
 * [Vidéo La horde agile - keynote Montpellier 2013](https://www.youtube.com/watch?v=a0cI-E603bI)
 * [La horde agile - keynote Toulouse 2013 - 1](https://vimeo.com/79713655)
 * [La horde agile - keynote Toulouse 2013 - 2](https://vimeo.com/80577433)


### Agile Fluency

**Durée** : ~ 45 mn (session avec petit atelier). 

**Public** : Monde "agile".

**En une phrase** : Une grille de lecture qui permet d'appréhender son cheminement, sa "transformation" agile.

**Liens** : 

  * [Session et mini atelier](/2017/06/agile-fluency-session-atelier/)
  * [Slides we're gonna groove](https://www.slideshare.net/pablopernot/perspective-plutt-que-framework-agile-fluency)
  * [Conversation autour d'Agile Fluency](/2017/04/conversation-agile-fluency/)
  * [Perspective plutôt que framework, Agile Fluency](/2017/04/perspective-agile-fluency/)


### Gothamocratie, holacratie et sociocratie

**Durée** : 2h30-3h (atelier). 

**Public** : Tout public.

**En une phrase** : De nouvelles formes d'organisations : sociocratie, holacratie. En vous plongeant dans Gotham City nous vous apprenons à les utiliser, et si vous savez gérer Gotham vous saurez gérer votre organisation. 

**Liens** :

 * [Le cadre de l'holacratie](/2016/12/cadre-de-holacratie/)
 * [L'holacratie, pourquoi on en parle](https://www.infoq.com/fr/articles/holacratie-pourquoi-on-en-parle)
 * [Holacratie : Comment démarrer lundi](https://www.infoq.com/fr/articles/holacratie-comment-demarrer-lundi)
 * [Gothamocratie, explicit lyrics](https://www.infoq.com/fr/articles/gothamocratie-explicit-lyrics)
 * [Chausse-trappes et Chimères de l'Holacratie](https://www.infoq.com/fr/articles/chausse-trappes-et-chimeres-holacratie)
 * [Spirit déploie l'auto-organisation : retour d'expérience](https://www.infoq.com/fr/articles/spirit-auto-organisation)
 * [Atelier Gothamocratie](https://drive.google.com/drive/folders/0B779TCU1Z5goQ3RXZEdDWTdqUUU)

### Openspace Agility : dare to invite (a.k.a Open Adoption)

**Durée** : ~45mn (atelier ou session)

**Public** : Tout public.

**En une phrase** : Les paradoxes de la conduite du changement, et comment le conduire par l'invitation et le forum ouvert. 

**Liens** :

 * [Open Adoption - Dare to invite](https://www.slideshare.net/pablopernot/open-adoption-dare-to-invite)
 * [Open Adoption - Darefest 2015](https://fr.slideshare.net/pablopernot/open-adoption-darefest-2015)

### Pecha Kucha ! (20 slides de 20 secondes soit 6'40")

 * [À quoi ça sert un product owner ?](/2016/11/a-quoi-ca-sert-un-product-owner/)
 * [Bidonvilles et organisations](/2015/08/pecha-kucha-ale2015/) 


## Projets 

- [Raid agile](http://raidagile.fr). Des formations différentes dans les Cévennes sur l'agilité. 
- [School Of PO](http://schoolofpo.com). Une communauté autour du produit et des product owners. 
- [School of PO, la conf'](http://laconf.schoolofpo.com). La conférence de la communauté des PO. 
- [Wake Up RH](http://wakeuprh.fr). Repenser les RH. 

## Conférences récentes

Conférences durant lesquelles j'ai été orateur ou organisateur.

### 2018

-   Organisateur de "[La conf, school of po](https://laconf.schoolofpo.com/)", Paris

### 2017

-   [Printemps Agile, Caen](http://www.club-agile-caen.fr/printemps-agile/printemps-agile-2017), "Gothamocratie, Holacratie, Sociocratie" avec Géraldine Legris.
-   Agile France, Paris, "Agile Fluency, we're gonna groove"
-   [Agile Grenoble](http://agile-grenoble.org), Keynote "Agilité, engagé, modernité"
-   [Agile Grenoble](http://agile-grenoble.org), "Gothamocratie, Holacratie, Sociocratie" avec Dragos
    Dreptate.

### 2016

-   Lean Kanban France 2016, "Kanban" avec Laurent Morisseau.
-   Organisateur de ALE2016, Paris.
-   Agile France, Paris, "Gothamocratie, Holacratie, Sociocratie" avec Dragos Dreptate et Géraldine Legris.
-   [NCrafts, Paris, "Open Adoption, dare to invite"](http://videos.ncrafts.io/video/167722770)
-   Facilitateur de l'openspace du Scrum Gathering Prague 2016 avec Olaf
    Lewitz.

### 2015

-   [ALE2015, Sofia, "Les organisations vivantes"](https://www.youtube.com/watch?v=VQNQnHRo-b8)
-   [Lean Kanban France, "Lean Topologie"](https://www.youtube.com/watch?v=a9KrjWv_Rt8)
-   Agile France, Paris.
-   Scrumday, "Satisfaction en 24h, les marabouts agiles", avec
    Christophe Addinquy
-   Darefest, Anvers, "OpenSpace Agility, dare to invite"

### 2014

-   Agile tour Paris, Montpellier, "Voyage contextuel : agilité
    organisationnelle et dynamique individuelle" avec Boris Quinchon
-   Agile tour Toulouse, "Les organisations vivantes"
-   Agile France, Paris, "Les organisations vivantes"
-   [Scrumday, "Open Agile Adoption" avec Oana Juncu](https://www.youtube.com/watch?v=Rx8d36WYdGc)
-   [SudWeb Toulouse](https://vimeo.com/103403865)

### 2013

-   Keynote "La horde agile" : Agile tour [Montpellier](https://www.youtube.com/watch?v=a0cI-E603bI&t), [Toulouse](https://www.youtube.com/watch?v=d9efofwYKzI),[Clermont-Ferrand](https://www.youtube.com/watch?v=IyPJzpOJcRc)
    Marseille
-   Agile France, Paris
-   [Agile Tour Tunisia (Tunis)](https://www.youtube.com/watch?v=NrMaKPToTJM)
-   Scrumday, "storytelling" avec Oana Juncu
-   Rencontres du réseau (CJD), "qu'est-ce qu'un patron agile ?"
-   IUT Agile, Aix en provence
-   [Agile Open Sud](/2013/01/annonce-agile-open-sud-2013/) 

### 2012

-   Agile Tour, Montpellier, [Paris](/2012/11/agile-tour-paris-2012/), Marseille
-   [SudWeb Toulouse](https://sudweb.fr/2012/talk/le-projet-web-dont-vous-etes-le-heros-agile/)
-   ParisWeb
-   [Mixit, Lyon](https://www.youtube.com/watch?v=1efayxnn_Gs), ou [encore](/2012/04/mix-it-sudweb/)
-   [Agile Open Sud](/2012/03/agile-open-sud-2012-cest-fait-aussi/)

### 2011

-   Agile Tour, [Toulouse](/2011/10/agile-cmmi-potion-magique-ou-grand-fosse/), [Montpellier](/2011/11/lagile-des-causes-perdues/), Toulouse
-   [SudWeb - Nîmes, anatomie d'une misson agile](https://sudweb.fr/2011/post/Anatomie-d-une-mission-agile.html) et [encore](/2011/11/anatomie-dune-mission-agile/)
-   [ParisWeb - Paris](https://www.paris-web.fr/2011/ateliers/planification-ou-iteration-specialisation-ou-heterogeneite-transmission-orale-ou-ecrite-deux-jeux-po.php)
