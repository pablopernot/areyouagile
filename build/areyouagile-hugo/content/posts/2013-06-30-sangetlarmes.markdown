---
date: 2013-06-30T00:00:00Z
slug: du-sang-et-des-larmes
tags: ['larme','sang','churchill','rigueur','discipline']
title: Du sang et des larmes
---

*Je n'ai rien d'autre à offrir que du sang, de la peine, des larmes et
de la sueur.* --Winston Churchill, 13 mai 1940, Chambres des communes.
Messieurs les managers, les chefs, les dirigeants, les entrepreneurs,
les patrons, vous qui êtes "aux responsabilités", je ne trouverai pas
meilleure formule.

En effet, en me basant sur cette formule très très connue, je promets
souvent aux dirigeants qui me sollicitent "des larmes et de la sueur". A
ceux qui ne connaissent ni larmes ni sueurs j'indique qu'il y a un
problème. A la question c'est quoi un dirigeant agile ? c'est quoi un
manager agile ? Nous pourrions répondre : c'est la fin d'une croyance
qui veut que le dirigeant, manager, soit servi par ses équipes. il doit
servir ses équipes. La fin de la croyance que le management est une
science dure, c'est une science molle.

## En quoi cela se matérialise-t-il ?

Vous devriez arrêter de croire que la gestion de vos équipes peut se
réduire à la gestion d'une feuille excel, à un tableau de nombres.
Comptabiliser, additionner, soustraire, ajouter des nombres est
illusoire. On ne remplace pas deux par deux, on ne remplace pas un nom
par un autre nom, une activité de 100 divisée en 4 ne donne pas 4x25.
Toute ceci c'est se mentir. D'ailleurs **personne n'est dupe**. Mais
c'est tellement confortable, tellement simple.

Arrêtons cette illusion, ce mensonge.

Votre matière est humaine. Elle est donc complexe. Elle ne peut se
réduire à des algorithmes. Elle est en constante évolution avec son
environnement. Elle ne sera **jamais** stable. Votre rôle est d'être à
son service, et non pas l'inverse. Votre objectif est probablement de
lui assurer la meilleure *performance*, *productivité*, pour cela, comme
un numéro de *tourneur d'assiettes*, vous ne cesserez de vous occuper
d'eux, d'où le sang et les larmes.

On ne peut penser envisager changer 10 français par 30 personnes en
offshore. Ces nombres ne s'additionnent pas (je ne peux donc pas vous
dire pourquoi cela ne fonctionne pas). On ne peut pas transformer un
expert payé 2000 par deux juniors payés 500 (je ne dis pas que l'un est
mieux que l'autre, je dis que cela est différent). Ces chiffres ne sont
pas de vrais chiffres. Les indicateurs que vous devriez manipuler sont
immatériels, malheureusement pour nous tous, cela demande des efforts.

Un dirigeant agile, un manager agile, n'a pas vraiment de zone de
confort (d'où le sang et les larmes).

## Pistes

Est-ce que 25% de votre activité dédiée à la conversation avec vos
équipes vous paraît trop ? pourtant cela me paraît indispensable au
maintient de ce *liant*.

Si vous estimez ne pas être utile, réellement, admettez le et trouver
une autre façon de contribuer à l'émancipation de votre activité. Ce
n'est pas forcément facile à admettre mais cela sera très gratifiant.

Dans votre boulot de tous les jours, allez sur le terrain (le *gemba* du
Lean), c'est eux qui savent répondre à de nombreuses questions. On
notera aujourd'hui comme l'a justement souligné Marc Halevy lors d'un
récent congrès auquel j'ai pu participer : *l'ouvrier est là depuis et
pour plus longtemps que son petit chef, lui même est là depuis et pour
plus longtemps que son chef, lui même est là depuis et pour plus
longtemps que son patron, lui même étant là depuis et pour plus
longtemps que son actionnaire.* Pour la pérénnité de l'entreprise les
décisions prisent par la base sont souvent les bonnes si tant est que la
base souhaite l'émancipation de l'entreprise.

La *base*, le faiseur, l'opérateur, doit donc souhaiter l'émancipation
de l'entreprise. Oui cela aussi c'est votre job.

Et ce job est passionnant : constamment assurer le bien-être du système,
donc les larmes et la sueur au début, la joie, le plaisir ensuite.
