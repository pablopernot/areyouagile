---
date: 2013-10-11T00:00:00Z
slug: la-horde-agile-les-slides
tags: ['livre','lecture','horde']
title: La horde agile - les slides
---

Vous trouverez ci-joint les slides de **la horde agile**, session
présentée récemment en *keynote* à l'Agile Tour Marseille, à l'Agile
Tour Toulouse, et très bientôt à l'Agile Tour Montpellier. Les slides
n'ont que peu d'intérêt en eux mêmes si ce n'est pour la liste de
références en fin. Pour cela les voici.

Pour préparer cette **horde agile** je me suis mis à travailler sur un
texte (20-40 pages). Dès la fin de ce mois d'octobre je l'achèverai et
le publierai.

<script async class="speakerdeck-embed" data-id="14dded1014680131df7d6e4d3bcbfccc" data-ratio="1.44837340876945" src="//speakerdeck.com/assets/embed.js" width=400></script>

Merci à tous.

## Mini livre associé

Je me permets de reprendre ce petit post pour vous proposer le texte que
j'évoquais plus haut :

-   [La horde agile, le mini livre](https://gitlab.com/pablopernot/peetic),
    pdf, 42 pages, ~ 4mo

