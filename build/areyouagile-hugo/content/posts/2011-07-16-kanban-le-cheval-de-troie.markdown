---
date: 2011-07-16T00:00:00Z
slug: kanban-le-cheval-de-troie
tags: ['kanban']
title: Kanban, le cheval de Troie
---

En ce moment beaucoup de choses sont dîtes sur **Kanban**, et cette
approche agile semble avoir le vent en poupe. J'en suis ravi et je ne
suis pas surpris : de nombreuses choses imposées par **Scrum (et XP)**
qui freinent les entreprises dans leur adoption ne sont pas présentes
dans Kanban, par exemple : la redistribution des rôles, ou la
recomposition des équipes. La principale force de Kanban est de
permettre une modélisation du flux sur l'existant, puis de le faire
évoluer. Je ne sais plus de qui sont ces mots (Mike Cohn peut-être) mais
quand Scrum opère une révolution, Kanban propose une évolution. On
comprendra alors aisément que de nombreuses entitées soient plus tentées
par **Kanban** que par **Scrum**. Et donc Kanban pourrait probablement
bien plus que Scrum dans les entreprises être le cheval de Troie des
méthodes agiles.

Mais

Car il y a -à mon avis- un "mais".

Il est beaucoup plus aisé de ne pas être agile en faisant du Kanban,
qu'en faisant du Scrum.

Il sera donc plus beaucoup plus aisé à certaines entreprises de
s'emparer faussement du concept agile au travers de Kanban qu'au travers
de Scrum.

Dans de nombreux cas une révolution est bien plus préférable qu'une
évolution.

En ce moment où le succès des méthodes agiles parait à son comble, et
donc où le risque de perversion (pour citer Alistair Cockburn) est très
fort, Kanban pourrait finalement plutôt se transformer en cheval de
Troie des méthodes agiles à leur insu...

On pourrait me répondre : "l'important est que cela marche ou que cela
soit agile ?", naturellement que cela marche. Après que veut dire
"marcher", "réussir", "résultat" (ouh les restes d'une discussion dans
une voiture hier). Si on s'accorde dessus, je suppose que je
compléterai en disant que cela marchera cependant mieux si c'est agile.

Je n'ai naturellement pas à ce jour l'expérience de par exemple [Laurent
Morisseau](http://www.laurentmorisseau.com/)  sur Kanban (et je vous
recommande donc sa lecture). Mais, -à titre personnel- j'y vois de
grosses qualités : centraliser/modéliser et prioriser en un seul point
toute l'activité. Comme le backlog de Scrum, si ce n'est que le backlog
de Scrum ne contient pas tout, ou très rarement. Avec Kanban tous les
arbitrages se font dans un même lieu, sans échappatoire. D'autre part
comme on travaille au niveau, disons de la "story", on a une souplesse
(dans le changement) plus grand que dans l'itération.

Mais je retiens aussi des lacunes : dans mon expérience (et je prends
donc là aussi une précaution), les aspects collaboratifs d'équipes y
sont trop effacés (je sais on peut y remédier : mais comme évoquer plus
haut, ce n'est pas une obligation...), la présence d'une deadline (des
itérations) s'estompe, les interactions sont moins nécessaires.
Globalement si je me sens plus à l'aise avec Scrum c'est qu'il propose
une forte connotation humaine, de rapport humain, de dynamique, alors
que Kanban est clairement plutôt un plaisir intellectuel de modélisation
(d'où aussi sa séduction).

J'ai évoqué un peu ce sujet hier à ce panier repas agile auquel j'ai eu
le plaisir de participer, et finalement je crois que c'est Thierry (Cros
@thierrycros) qui a eu le mot de la fin et j'espère ne pas trahir sa
pensée : *oui Kanban mais ne pas démarrer avec, l'appréhender quand
l'équipe est déjà mature*.
