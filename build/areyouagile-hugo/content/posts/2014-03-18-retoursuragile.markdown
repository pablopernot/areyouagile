---
date: 2014-03-18T00:00:00Z
slug: les-temps-complexes-revisitons-agile
tags: ['emergence','complexe','complexite']
title: Les temps complexes, revisitons Agile
---

Je vous propose de revisiter la notion d'*agile*. Plus les mois passent
moins je parle du manifeste ou des pratiques (éléments pourtant
fondateurs historiquement), non pas qu'ils perdent beaucoup de valeur,
mais plutôt qu'ils restent assez difficiles à saisir lors d'un premier
abord. Et puis -- disons-le aussi -- ma pratique évolue, ma pensée
change, vers une approche bien plus liée finalement au management, à la
conduite du changement, à l'organisation des personnes.

Alors comment expliquer l'agile aux gens, voilà ma façon de faire :
j'invoque les **temps complexes** comme l'étape suivante des temps
modernes, de Chaplin, Charlot, et sa bataille contre le taylorisme, ou
encore plus récemment, ceux de Sartre et de sa revue d'après-guerre.
Nous sommes au delà de cette modernité, et le **mot agile**, c'est le
**mot caoutchouc**, le mot qui se glisse partout, le mot facile, pour
représenter en fait **la complexité** (en ce sens j'adhère complètement
à l'analyse sur le mot "agile" de Dominique Dupagne dans sa *Revanche du
rameur*).

## Les temps complexes

![Les temps complexes](/images/2014/03/temps-complexes-3.png)


## Dialogue

-- Nous sommes entré dans les temps complexes : tout va très vite, trop
vite, tout est **entrelacé**, tout change, tout le temps, etc

-- La seule façon de répondre à cette **complexité** c'est de
**responsabiliser**, d'engager les gens. Comme un général qui ne peut
plus tout diriger comme il y a 200 ans, et qui doit se reposer sur
l'auto-organisation de ses troupes, celles-ci restent fidèles à sa
stratégie, à sa vision mais mettent en oeuvre leurs tactiques.

-- Mais si vous responsabilisez sans laisser le **droit à l'erreur**,
vous mentez, vous ne responsabilisez pas vraiment.

-- La responsabilisation est aussi possible si celle-ci ne met pas
l'organisation dans un état trop dangereux, sinon qui osera quoi que ce
soit ?

-- Donc oui à la **responsabilisation** mais uniquement si l'erreur est
possible, mais elle ne doit pas être "mortelle". Pour cela il faut
avancer par petits pas, de **façon itérative**. Et juger étapes par
étapes de nos progrès. Une petite fièvre ça soigne, une grosse, ça tue !

-- Mais pour vraiment juger il faut des **choses finies**. Peu importe
l'itératif si les choses ne sont pas finies (c'est alors de
l'incrémental...). Interrogez vous très fort sur cette notion de fini
(le mieux est de mettre sur le marché ! pensez au produit viable
minimum).

-- Si vous réussissez à délivrer des choses finies, étape par étape, en
étoffant, alors autant **prioriser par valeur**. Car a) on a jamais le
temps de tout faire, donc autant faire uniquement les choses de valeur,
b) cela permet de se donner les plus grandes garanties et les plus
grands enseignements, et enfin c) d'avoir un sacré **time to market**
(c'est à dire pouvoir mettre sur le marché de façon très réactive).

-- *Time to Market* très utile en ces temps de grande complexité, car
tout change si vite.

-- Naturellement ce cycle n'est apprenant (c'est nécessaire dans cette
complexité ambiante) que si l'on s'interroge constamment sur **comment
s'améliorer**.

Voilà, du sens et de la cohérence j'espère. L'étape suivante c'est de
rendre le général cité plus haut multiple, de transformer la vision de
un en la vision de plusieurs, d'un groupe, car l'intelligence collective
n'est pas qu'une formule facile.
