﻿---
date: 2018-09-06
slug: la-rupture-manageriale
tags: [syndrome, peters, management, rupture, manageriale]
title: "La rupture managériale"
--- 

L'histoire est simple, mais elle se répète trop souvent. 


Pierre est un bon organisateur, un bon leader, un bon expert, il mène le groupe avec lequel il travaille. Il devient en quelque sorte leur "manager", leur "leader". Puis l'implicite devient explicite : il devient officiellement "manager". Ses capacités d'analyse, d'organisation, son savoir-faire, son implication, sa capacité de travail en font un bon manager, d'autant qu'il est très sympathique et humain. Au milieu de son équipe, il excelle. Le succès est au rendez-vous et Pierre franchit les échelons. Jusqu'à ce qu'il arrive à la tête du département, de l'organisation. Et là c'est l'échec, incompréhensible. Pourtant Pierre n'a pas changé.     

Pierre vient de toucher le principal syndrome de Peters du management. 

Pierre est toujours cet homme sympathique et humain, plein de qualités. Mais il est devenu son pire ennemi. 

À la tête du département, il y a trop de décisions pour que ses capacités d'analyse suffisent. Pourtant Pierre a toujours réussi avec ses qualités organisationnelles à organiser cela. Il ne va pas en démordre, il va vouloir toujours décider de tout, en tous cas de trop. Trop de décisions, son échec est accentué par l’isolement lié à la position. Son savoir-faire devient un fardeau, car il se sent capable de mieux comprendre, de mieux savoir, il se pense en devoir d'absorber le sujet, de s'en mêler, et personne n'ose l'envoyer bouler. Il sait organiser. Mais encore, il veut tout organiser, n'est-ce pas tout cela qu'on lui demande depuis le début ? 

C'est un moment difficile. Soit Pierre ne devrait pas accepter ce job. Soit il va devoir comprendre que cela demande une posture qui s'équilibre très différemment. Ne plus faire, ne plus contrôler, déléguer, ne plus décider (au jour le jour), mais donner la direction, inspirer, rappeler le cadre, et communiquer, communiquer, communiquer, partager, partager, communiquer, partager, inspirer, incarner, communiquer, partager, inspirer, cadrer, déléguer. Rien à voir avec ce qu'on lui demande depuis le début de son cheminement.

Les managers ne sont pas assez sensibilisés à cette rupture.

Je vois plein de gens admirables ne pas s'en sortir. 

Un premier pas pour s'analyser en tant que "manager" pourrait être d'utiliser les [quatre lieux du *host leadership*](/2018/01/hote-ecosysteme/). Faites d'abord la liste des activités que vous pratiquez en tant que manager. Si vous ne répondez que : "je manage" c'est que franchement vous ne vous posez pas assez de questions. *A minima* reprenez votre dernière semaine et listez vos activités. Ensuite, positionnez-les dans ce diagramme. Donc a) vous listez vos activités de "manager" b) vous les placez dans ce cadrant c) vous observez densité et équilibre. 

![Host Leadership](/images/2018/09/host-leadership.jpg) 

Souvent il y a un déséquilibre, un déficit, entre d'un côté "le balcon" et "la cuisine", que les managers apprécient et qui sont généralement assez étoffés en activités. *A contrario* "sur la scène" ou "avec les participants" sont trop souvent laissés en creux, manquent de densité. Les managers oublient trop facilement de porter, d'incarner, de communiquer, de partager, de déléguer. C'est normal c'est ce qui demande le plus d'énergie et de prise de risque.    

Tant pis pour eux, mais surtout dommage pour nous. Souvent la différence est là. 
