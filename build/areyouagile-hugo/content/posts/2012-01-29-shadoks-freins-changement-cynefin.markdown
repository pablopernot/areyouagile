---
date: 2012-01-29T00:00:00Z
slug: shadoks-freins-changement-cynefin
tags: ['complexe','complexite','emergence','cynefin','shadoks','shadok']
title: Shadoks, freins, changement, cynefin
---

Ce mois-ci j'ai pu donner deux fois une petite présentation (\~40mn) sur
globalement le thème de la conduite du changement agile dans les
organisations. Mon souhait dans cette présentation est la difficulté sur
le terrain de concilier le ticket d'entrée de l'agile -très bas
finalement (une lecture de 20 pages par exemple le scrum guide de
scrum.org)-  avec la complexité sous-jacente qui existe bel et bien. Cet
écart est la source de bien des déceptions et déconfîtures. Et
aujourd'hui où l'effet de mode bat son plein, où il faut mieux faire de
l'agile -on ne sait jamais- que de ne pas en faire, les désillusions
seront nombreuses.

![Shadok](/images/2012/01/d_04-300x215.jpg)

Voici un petit retour sur le contenu, puis le contenant et le feedback.

## Le contenu

J'ai essayé de découper mes slides (ils sont plus bas) en trois parties
: la première met en avant le socle "cynefin" et "cynefin dynamics". Ce
framework a pour qualité de bien mettre en évidence les différences
entre le domaine ordonné (compliqué ou simple), et le domaine complexe.
Je vous encourage à commencer par voir [cette
vidéo](http://www.youtube.com/watch?v=N7oz366X0-8) ou à en lire
plus [ici](http://www.cognitive-edge.com/articlesbydavesnowden.php) et [là](http://www.youtube.com/watch?v=N7oz366X0-8).
Mais pour simplifier à l'extrême c'est dans le domaine complexe que
l'émergence des pratiques "agiles" va se produire, et c'est en les
faisant basculer dans le domaine ordonné que l'on va vraiment pouvoir
les répandre au sein de l'organisation. La dynamique de ces pratiques
qui passent de "nouvelles" à "émergentes" ou "bonnes" ou "meilleures"
est aussi la source de freins. Quand j'essaye de fixer une bonne
pratique je suis confronté à des freins. C'est la deuxième partie de
cette présentation. L'idée que je souhaite soutenir est que le freins ne
sont pas un problème en tant que tel. Ils sont la manifestation que l'on
est en train de travailler sur nos pratiques, ils sont nécessaires, ils
sont la preuves de nos progrès. Si on souhaite citer la [théorie des
contraintes de
Goldratt](http://en.wikipedia.org/wiki/Theory_of_constraints), on  peut
aussi dire (toujours très simplifié) que les freins sont la plus grande
source d'amélioration. Mais si les freins sont trop nombreux, trop
récurrents, trop gros nous ne franchirons pas cette étape
d'amélioration. Elle sera insurmontable. C'est la troisième partie de ma
présentation : si on ne souhaite pas être submergé par les freins
(taille, nombre) il faut une bonne conduite du changement. Pour cela je
me réfère à un acronyme que je trouve très efficace : ADAPT.

*Awareness*, sensibilisation, si les gens ne savent pas ce qu'est
l'agile, difficile de réussir.

*Desire*, envie, maintenant qu'ils savent ce qu'est l'agile, en ont-ils
envie ? (avaient-ils envie de quelque chose qui n'était pas l'agile ?).

Enfin *Ability*, capacité, en fonction de la nature de votre
organisation, du contexte mais aussi donc de la sensibilisation à
l'agile de votre organisation et de son envie, comment adapter votre
parcours agile.  

(Je ne traite pas "*Promotion & Transfer*" les deux derniers qui sont là
pour "fixer" le changement). Donc, différente capacité, différents
parcours, différentes méthodes agiles, différentes possibilités, il y a
de nombreuses voies, il faut en choisir une qui soit bien adapté à sa
capacité sinon on va générer trop de freins. Il n'y a pas de meilleure
capacité.

Pour résumer et en inversant la chronologie de la présentation : si vous
vous donnez les moyens d'une bonne conduite du changement, vous éviterez
d'avoir trop de freins, ou de trop gros freins. Mais des freins sont
nécessaires car ils sont la preuve que le changement est en cours, ils
sont aussi la preuve que l'on fait émerger de nouvelles pratiques en
adéquation avec ce qu'est votre organisation. Pour bien comprendre ces
dynamiques il me parait très intéressant de se plonger dans le framework
cynefin.

Les slides chez Speakerdeck :

<script async class="speakerdeck-embed" data-id="4f9cf343b3f2f3002200d9c0" data-ratio="1.44837340876945" src="//speakerdeck.com/assets/embed.js"></script>

## Le contenant et le feedback

![esprit agile](/images/2012/01/20120126_espritagile3-225x300.jpg)

J'ai pu donc donner cette présentation dans deux lieux jusqu'à
maintenant. Lors d'un [petit déjeuner dans les salles de formation de
Smartview](http://www.smartview.fr/petits-dejeuners-1er-semestre-2012/).
J'ai aussi pu donner cette présentation grâce aux efforts de Karine &
Eric (@viaxoft) et de l'association [Esprit
Agile](http://www.esprit-agile.com/) à Marseille. Merci à eux ! (A
Marseille nous avons fait 40mn de présentation et 1h30 de
discussions...).

La réception a été bonne, mais pas mal chahutée (j'ai appris beaucoup)
 :     - "c'était trop théorique, pas assez concret".

Oui mais je crois qu'il faut parfois asseoir une analyse théorique pour
mieux appréhender le concret. Cette remarque est recevable
naturellement. J'ai cependant la prétention de penser qu'il faut poser
une graine sur ces aspects théoriques considérés complexes et laisser
murir. Mais oui c'est assez théorique, et surtout \*trop\* théorique
pour les gens venus chercher des clefs concernant l'agile, des novices
ou débutant (ceux qui sont venus chercher des réponses "meilleures
pratiques" à une *checklist* de freins : justement nous sommes loin
d'être dans ce cas de figure, ce n'est pas ainsi qu'il faut
l'appréhender). J'aurais du préciser dans le programme que ce n'était
pas pour des novices. Les agilistes (ou les gens endurcis à la conduite
du changement) ont su raccrocher mon discours à l'expérience, une partie
des autres non.

- "cela s'applique à tout, pas seulement à l'agile".

Oui c'est vrai. Mais si l'on considère (comme moi) que les freins sont
le résultats des collisions, des frottements explicités par cynefin
dynamics et que l'on estime que l'agile a une forte adhérence avec le
domaine complexe et les pratiques émergentes, alors même si ce discours
s'applique à beaucoup de chose, il est particulièrement important pour
l'agile.

Une présentation à méditer pour moi. Il est clair que je n'ai pas encore
assez réussi à clarifier le discours que je souhaite voir passer. Mais
d'un autre côté les réactions provoquées me laissent penser qu'il y a
vraiment une base de travail très intéressante.

![Shadok](/images/2012/01/20120126_espritagile1.jpg)

Merci à Jacques Rouxel & Claude Pieplu pour ces inestimables
[Shadoks](www.amazon.fr/les+shadocks%20) ! Merci aux gens présents et
aux riches discussions qu'ils ont provoqués.

 
