---
date: 2012-11-12T00:00:00Z
slug: peetic
tags: ['peetic']
title: Peetic
---

**Peetic**, est un petit scénario que j'utilise lors de mes formations
agiles (concernant Scrum).

Il n'est pas ambitieux, la route est longue avant qu'il ne soit complet.
Mais c'est une base de travail. J'ai d'ailleurs posé les sources sur
[Gitlab](https://gitlab.com/pablopernot/peetic) si certains veulent
participer.

*Les personnages et les situations de ce récit étant purement fictifs,
toute ressemblance avec des personnes ou des situations existantes ou
ayant existé ne saurait être que fortuite.*

Il s'agit ici d'imaginer un site de rencontres pour animaux et plus si
affinité.

Leur vision est : faisons un site de rencontre pour animaux car les gens
dépensent beaucoup d'argent pour leurs petites bestioles. Nous pourrons
ainsi vendre de l'accompagnement, des produits et des publicités. (c'est
là leur, hein, ils peuvent progresser !).

## L'équipe

[Equipe](/pdf/equipe.pdf) (pdf)

### Story Mapping

Une première réunion autour d'un story mapping (cliquez pour agrandir) :

Cliquez ici : [Features](/images/2012/11/features.png) pour
l'image en grand.

![Story Mapping](/images/2012/11/features.png)

Puis on demande naturellement au product owner de prioriser ! (rouge =
1, bleu = 2, vert 3). A la fin n'hésitez pas à enlever TOUTES les cartes
vertes ou bleues pour ramener les participants à la réalité : "ça on
oublie". Au passage le petit morceau gris indique des paramètres pour le
DONE.

![DotVoting](/images/2012/11/features_dot_voting.png)

**Nouveau \[15/3/2013\]**

Un "prune the tree" :

![Prune the tree](/images/2013/03/prunethetree.png)

Un "impact mapping" proposé par [Claude Aubry](http://www.aubryconseil.com/) :

![Impact Mapping](/images/2013/03/impactmapping.png)

## Backlog

Un (tout) début de backlog émergeant de cette séance de *story mapping*.
Vous noterez l'utilisation du Gherkin dans les tests d'acceptance.

[backlog peetic](/images/2012/11/backlog_peetic.ods) (ods)

## Et les personas ayant servis !

[personas 1](/pdf/personas1.pdf) (pdf)

[personas 2](/pdf/personas2.pdf) (pdf)

## La notion de Done/Fini

[dod](/pdf/dod.pdf) (pdf)

## Enfin le plan de release (en deux parties)


![Plan de release partie 1](/images/2012/11/planning_release_part1.png)

![Plan de release partie 2](/images/2012/11/planning_release_part2.png)

## Retrospectives

Quelques retours de rétrospectives

[Retrospective](/pdf/retrospective1.pdf) (pdf)

Une Rétrospective en "étoile de mer" :

![rétrospective](/images/2012/11/retrospective2.png)

