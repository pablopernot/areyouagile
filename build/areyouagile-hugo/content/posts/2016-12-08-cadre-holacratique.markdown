---
date: 2016-12-08T00:00:00Z
slug: cadre-de-holacratie
tags: ['holacratie','sociocratie','entrepriseliberee']
title: Le cadre de l'holacratie
---

Cet article a pour objectif de donner une petite perspective sur
l'holacratie, la
[gothamocratie](https://drive.google.com/open?id=0B779TCU1Z5goQ3RXZEdDWTdqUUU),
la [sociocratie](http://sociocracy30.org/), ou tout simplement sur une
façon actuelle qui me séduit particulièrement pour accompagner les
organisations.

J'avais déjà pu écrire que je pensais plutôt du bien de cette approche
[ici](/2016/04/hellocratie-holocratie-holacratie-honolulucratie/)
([Hellocratie, Holocratie, Holacratie,
Honolulucratie](/2016/04/hellocratie-holocratie-holacratie-honolulucratie/)).
Elle n'est pas nouvelle, mais elle synthétise bien, dans la lecture que
j'en fais, les éléments auxquels je crois dans le but de faire grandir
son organisation. Je pense qu'en complément de mes articles, ceux de
[Géraldine](http://www.theobserverself.com/tag/holacratie/) donnent une
dimension opérationnelle, je vous recommande donc d'aller lire le blog
de cette coach. L'objectif de mon article est de permettre de démarrer
en holacratie, en sociocratie, etc. en s'assurant d'avoir saisi quelques
fondamentaux.

-   En savoir déjà plus sur
    l'[holacratie](https://fr.wikipedia.org/wiki/Holacratie) ou la
    [sociocratie](http://sociocracy30.org/) ; Les articles de
    [Géraldine](http://www.theobserverself.com/tag/holacratie/), mon
    précédent article : [Hellocratie, Holocratie, Holacratie,
    Honolulucratie](/2016/04/hellocratie-holocratie-holacratie-honolulucratie/).

## Évolution continue et distribution du pouvoir

Ces mouvements, notamment la sociocratie et plus encore l'holacratie,
pourraient être réduits (sans être diminués) à deux concepts clefs :

-   L'importance à saisir que nous ne sommes pas dans une transformation
    mais dans une constante évolution, **une évolution continue**.
    L'organisation ne s'arrête jamais de bouger, il faut un système qui
    sache accueillir ce constant changement.
-   Que tout l'intérêt est la mise en œuvre d'une **distribution du
    pouvoir** qui passe par une autonomisation des entités créées
    (cercles, etc.). C'est là que se trouve le levier recherché.

Si votre implémentation de ces démarches n'intègre pas ces deux
composants, je pense qu'elle est vaine car je crois à ces deux idées
(évolution continue et distribution du pouvoir), ou qu'elle est non
représentative de ce que vous pourriez appeler holacratie ou
sociocratie. Ainsi donc comme toujours, **peu importe le nom que cela
porte**, c'est bien les principes sous-jacents qui sont fondamentaux.

## Cadre et Autorisation

![From sociocracy30.org](/images/2016/12/top-and-general-circle.png)

Il me parait donc clef de comprendre le principe de cercle englobant et
de sous cercles concernant la déportation, la distribution du pouvoir.
Qu'un cercle englobé est un sous ensemble qui s'attribue un domaine de
compétences et d'intervention déporté par le cercle supérieur. Ceci
n'est possible que si le cadre est clarifié. On le clarifie en énumérant
la raison d'être, le sens, du cercle (le "driver" dit la sociocratie).
On le clarifie en définissant ses redevabilités (ses devoirs) en rapport
avec sa raison d'être et en accord avec le cercle qui l'englobe, celui
qui a déporté son autorité chez lui. On clarifie le cadre en énumérant
le domaine, les zones d'intervention de ce cercle.

Le cercle et ses participants se sentent autorisés à prendre cet espace
car le cadre est clarifié. Le cercle englobant se sent sécurisé à
déporter son autorité car il sera régulièrement tenu au courant de
l'état du cercle par des métriques (des indicateurs) et des checklists
en provenance de celui-ci. Ainsi que la garantie que la raison d'être et
les redevabilités (devoirs) sont bien suivies. Il y a un "ambassadeur"
en quelque sorte, fonction tenue par le premier lien qui personnifie et
incarne cette déportation du pouvoir mais qui est surtout messager du
sens car il va faire partie de ce "sous" cercle. Les cercles ne sont pas
isolés ils sont reliés par ces liens.

Ainsi donc les principes d'une évolution continue et d'une distribution
du pouvoir sont rendus possible par la **clarification d'un cadre** et
la délivrance d'une **autorisation** *de facto* car validée constamment
par le respect d'une raison d'être, de redevabilités, de la définition
d'un domaine d'intervention et la remontée constante d'informations :
métriques et checklist, liens.

Les images sont piquées à sociocracy30.org que j'apprécie
particulièrement.

## Deux types d'entités : opérationnelles et par affinité

Ce qui est recherché par cette distribution du pouvoir c'est
l'**autonomie**. Car c'est l'autonomie qui va permettre une réponse
adéquate à ce monde changeant complexe actuel. Dans cet ensemble de
cercles dans lesquels sont déportés les autorisations, le pouvoir, on
observe rapidement deux familles : les cercles opérationnels et les
cercles par affinité.

Dans les organisations, particulièrement dans ce monde agile, on va
avoir une approche verticale : groupes pluridisciplinaires qui peuvent
proposer chacun une **réponse auto-suffisante**, qui sont capable de
**délivrer des éléments autonomes faisant sens**. En sociocratie et en
holacratie on a donc souvent des cercles axés sur un périmètre
opérationnel : je délivre quoi, pourquoi. Mais je suis autonome (donc
pluridisciplinaire) pour le délivrer. J'appelle cela des **cercles
opérationnels**.

De l'autre côté on a des cercles qui garantissent un service transverse
à l'organisation : par exemple un cercle RH, un cercle qualité logiciel,
etc. J'appelle cela des **cercles par affinité**. Leur rôle est de
**diffuser** ou de **centraliser**, de **capitaliser**. Leur autonomie
leur sert à pouvoir décider sur leur périmètre, ils doivent donc être
légitimes (exemple: les principaux représentants RH dans un cercle RH,
mais aussi peut-être des experts d'autres champs pour compléter les
points de vues et avoir de l'autonomie).

## Capacité de communication

![From sociocracy30.org](/images/2016/12/double-linked-hierarchy.png)

Je n'ai pas encore assez de recul pour vous dire exactement les
variations que ces deux types d'entités vont engendrer. Mais elles
existent sans doute, j'en perçois les effets dans mes missions. La
première difficulté que j'observe c'est la communication entre ces
cercles de différents types : en déportant le pouvoir dans ces groupes,
ces cercles, en les protégeant, on les a aussi potentiellement isolés,
surtout entre les cercles opérationnels et par affinité, pas de la même
"famille".

Je vous ai dit que les cercles n'étaient pas isolés mais reliés par les
liens. Normalement on a donc un pouvoir qui est distribué avec des
remontés et des descentes d'informations : premier lien, deuxième lien,
métriques, checklist. L'holacratie comme la sociocratie sont équipés
pour la remontée et la descente d'information. C'est plus les
informations transverses qui ont plus de mal à transiter.

Dans mes observations du terrain les cercles remplissent bien leurs
rôles ces remontées et descentes d'informations. Les informations
transverses se délivrent aisément par une communication libre, orale
bien souvent, entre les cercles. Mais des fois la question est posée, et
la communication à plat entre les cercles, est plus problématiques qu'il
n'y parait.

![give take](/images/2016/12/give-take.jpg)

Dans ce cas j'ai utilisé récemment une adaptation proposée par
[Dragos](http://andwhatif.fr) d'un atelier du nom de "give and take". Je
créé une simple matrice avec tous les cercles en abscisse et en ordonnée
(ou tous les cercles d'un domaine), et j'utilise la jointure pour
exprimer un besoin, une attente. **La question de l'enfermement de la
communication inter cercles non englobés/englobant est systématiquement
remontée dans mes contextes**. J'en griffonne un exemple ici.

Cette communication peut aussi être résolue quand des personnes font
partie de plusieurs cercles (en plus des personnes qui sont premiers
liens et deuxièmes liens). Mais attention à ne pas saturer les
personnes. Deux cercles me semble raisonnable par personne.

Enfin ces soucis de communication qui pourraient apparaître sont
peut-être le signe d'un manque d'autonomie ou de clarté dans la raison
d'être, le domaine, les redevabilités du cercle.

## Émergence et invitation

Comme très souvent dans la conduite du changement il faut **privilégier
l'émergence et l'invitation**.

On opte pour l'émergence de la forme adéquate et ne pas planifier la
forme adéquate. Il est donc raisonnable de démarrer soit par former vos
cercles selon votre réalité, ce que vous êtes, puis ils évolueront au
fil de l'eau puisque justement cette évolution continue est au coeur de
l'holacratie ou de la sociocratie. (Ainsi l'émergence ne s'arrête pas
non plus...) Soit en créant des premiers cercles qui fassent sens : en
accord avec le sens de l'organisation (qui n'était peut-être pas à ce
jour organisée en fonction). On peut démarrer en formant des cercles qui
représentent la réalité, ou vous pouvez démarrer par la création d'un
premier cercle pluridisciplinaire qui instaure une nouvelle organisation
autour du sens. Par exemple : nous avons besoin de traiter ce besoin du
marché : on crée un premier cercle autour. Il est souvent immédiatement
dépositaire d'un premier cercle englobant porté par le management. Les
choses pourront changer mais la réalité et le sens c'est souvent cela au
début.

**Faire émerger c'est donc suivre une vision, un sens, pas un plan**.

Et de fait toutes les conversations sont possibles car tout le monde
connaît cette réalité, ce sens, ou le découvre ou perçoit mieux les
gaps, les tensions, entre les différentes visions. Émerger est assez
simple : on demande le sens premier puis on le décline selon les besoins
dans des "sous"-cercles englobés.

**Sens, émergence, rien ne devrait être forcé. Cela demeure une
invitation**.

Être prescriptif c'est freiner l'appropriation, on devrait pouvoir
inviter les gens. On n'amène pas une organisation où elle ne veut pas
aller, on n'amène pas les gens où ils ne veulent pas aller. Je fais
attention de ne pas attribuer de rôle dès le démarrage trop rapidement :
je les laisse émerger des tensions, des besoins, du sens. Quand la
routine est prise, on peut traiter les rôles plus rapidement (par
exemple un nouveau rôle apparaît que l'on déploie dans tous les cercles
rapidement). Si on arrive avec une grille de rôles toute prête on va
enfermer les personnes dedans. Les rôles comme les cercles évoluent dès
que cela fait sens.

## Organisationnel ou opérationnel ?

On croise souvent scrum et holacratie, on les compare, on les mélange,
mais c'est une erreur. L'un répond à un **besoin opérationnel**, l'autre
souhaite répondre à l'organisation de l'organisation, à un **besoin
organisationnel**. Je suggère de ne pas mélanger les niveaux de lecture.
Ne commencez pas à me dire : on a besoin d'un scrummaster et d'un
product owner dans ce cercle, en tous cas pas de prime abord.
L'holacratie et la sociocratie proposent des cercles pour répondre aux
besoin de l'organisation. Une méthode comme scrum propose des rôles qui
sont endossés avec des pratiques pour réaliser des choses (en
l’occurrence souvent des produits en environnement complexe, et le
complexe est partout). Il se peut donc que certains rôles qui émergent
dans les cercles soient ceux d'une méthode comme scrum, mais si vous
liez les deux trop fortement dans votre discours vous rendez la lecture
de l'organisation compliquée, et vous fermez des portes à l'invitation
et à l'émergence.

**Gardez le bon niveau de lecture : celui de l'organisation de
l'organisation**.

## Ne pas suivre à la lettre

Pour finir il ne faut naturellement, et comme toujours, ne pas suivre à
la lettre les *framework*, méthodes, etc. Il faut en suivre les
principes, ce que j'ai essayé de mettre en avant ici. Les écarts les
plus flagrants dans ma pratique sont :

-   L'émergence des rôles au fil de l'eau (des tensions).
-   d'utiliser le *advice process* au lieu de la prise de décision par
    consentement.
-   Ceux proposés naturellement par
    [Géraldine](http://www.theobserverself.com/tag/holacratie/)
    d'intégrer des considérations de gouvernance ou de stratégie dans
    n'importe quelle réunion de triage même si certaines réunions sont
    dédiées à ces questions...
-   ...et de ne pas s'arc-bouter sur un formalisme trop sévère dans la
    tenue des réunions même si clarification et réaction doivent être
    sainement possible.

## Faut-il écouter les critiques

Oui toujours.

Bien qu'elles soient souvent fondées sur ce que j'estime être de
mauvaises implémentations de ces principes qui vont au delà de
l'holacratie et de la sociocratie. Une holacratie, une sociocratie, sans
émergence, sans invitation, sans distribution du pouvoir, sans autonomie
des cercles peut vite se transformer en machine à broyer assez inhumaine
basée uniquement sur le dictât de règles sans sens.

Un dernier mot :

-   Est-ce qu'il y a du management ? Naturellement. Des managers ?
    beaucoup moins.
-   Est-ce qu'il y a une hiérarchie ? Naturellement. Mais une hiérarchie
    du sens, plus que du pouvoir. Ce n'est pas le pouvoir qui monte et
    redescend, mais plus le sens.

À bientôt.

