---
date: 2013-12-01T00:00:00Z
slug: bienveillance
tags: ['bienveillance']
title: Bienveillance
---

![Emile Chartier Alain](/images/2013/12/Emile-Chartier-Alain.jpg)

*"Qu'il est difficile d'être content de quelqu'un !". Cette sévère
parole de La Bruyère doit déjà nous rendre prudents. Car le bon sens
veut que chacun s'adapte aux conditions réelles de la vie en société, et
il n'est point juste de condamner l'homme moyen ; c'est folie de
misanthrope. Donc, sans chercher les causes, je me garde de considérer
mes semblables comme si j'étais un spectateur qui a payé sa place et qui
veut qu'on lui plaise. Mais au contraire, repassant en moi-même
l'ordinaire de cette difficile existence, je mets d'avance tout au pire
; je suppose que l'interlocuteur a un mauvais estomac ou la migraine, ou
bien des soucis d'argent, ou des querelles domestiques. Ciel douteux, me
dis-je, ciel de mars, gris et bleu mêlé, éclairs de soleil et bise aigre
; j'ai ma fourrure et mon parapluie.*

-- Alain, propos sur le bonheur
