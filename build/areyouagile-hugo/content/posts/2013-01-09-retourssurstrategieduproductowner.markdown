---
date: 2013-01-09T00:00:00Z
slug: retours-sur-la-strategie-du-product-owner
tags: ['po','productowner','strategie','tactique']
title: Retours sur la stratégie du product owner
---

J'ai pu avec plaisir donner 2 fois la conférence Stratégie du Product
Owner avec Alexis Beuve des [Editions Praxeo](http://praxeo-fr.blogspot.fr/). Je vous en ai pas mal parlé en
fin d'année [ici](/2012/11/agile-tour-paris-2012/), 
[là](/2012/09/le-product-owner-contre-attaque/) ou [encore
ici](/2012/06/introduction-a-la-strategie-et-la-tactique-du-product-owner/).
Nous avons donc pu donner 2 fois cette conférence : à l'**Agile Tour
Paris 2012**, à l'**Agile Tour Montpellier 2012**.

![Stratégie du product owner](/images/2013/01/po4_fabriceaimetti.jpg)


Nous avons eu de bons retours, systématiquement, et nous en sommes
ravis. Nous espérons donner une suite à cette conférence, notamment
autour d'une différenciation des rôles de *product owner* et *product
manager*.

Nous avons été filmés dans les deux cas, je ne sais pas quand la session
parisienne sera dispo, mais voici la version Montpellieraine (merci à la
[Web TV UM2](http://www.webtv.univ-montp2.fr/?s=agile)).

Paris c'était la première -toujours difficile de se juger lors d'une
première (et j'ai toujours besoin de 5-10mn de chauffe...grr)- et dans
le grand amphi. 60 personnes ? 80 personnes ? sympa en tous cas (j'ai
préféré notre rendu à Paris qu'à Montpellier, et Alexis a préféré le
rendu de Montpellier à Paris. Bref). Montpellier c'était la toute fin de
journée, il restait juste les durs à cuirs (Claude, Antoine, Anthony,
Stéphane, etc. ), donc une petite quinzaine de personnes (mon seul
regret). Mais c'était bien amplifié et j'ai pu passer "Black Dog" lors
de l'évocation de la stratégie Led Zeppelin (bon anniversaire Jimmy
Page, 69 ans aujourd'hui -enfin le 9 janvier 2013- !).

Les [slides](http://speakerdeck.com/u/pablopernot) sont disponibles sur
mon *speackerdeck* (que je recommande désormais largement plutôt que
slideshare)

<div class="videoWrapper">
<iframe src="http://player.vimeo.com/video/56083361" width="500" height="375" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>    

Merci à [Antoine Vernois](http://blog.crafting-labs.fr/) (sur Paris) et [Fabrice Aimetti](http://agilarium.blogspot.fr/) (sur Montpellier) pour les photos !
     
![Dr Jekyll & Mr Hide](/images/2013/01/po1_antoinevernois.jpg)

![Dr Jekyll & Mr Hide](/images/2013/01/po2_fabriceaimetti.jpg)

![Dr Jekyll & Mr Hide](/images/2013/01/po3_fabriceaimetti.jpg)

![Dr Jekyll & Mr Hide](/images/2013/01/po4_fabriceaimetti.jpg)

Vous trouverez aussi chez [Web TV UM2](http://www.webtv.univ-montp2.fr/?s=agile) les autres sessions de
l'Agile Tour Montpellier 2012 ET 2011.

Enfin, pour finir, une proposition de Edouard pour compléter la session: [La bataille rangée](http://www.youtube.com/watch?v=p-mKKLBvego).
