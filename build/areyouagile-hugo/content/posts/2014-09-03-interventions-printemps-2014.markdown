---
date: 2014-09-03T00:00:00Z
slug: conferences-printemps-2014
tags: ['conference']
title: Conférences printemps 2014
---

Quelques liens sur mes conférences lors du printemps 2014, dans l'ordre
chronologique, le [ScrumDay 2014](http://www.scrumday.fr), [Agile France
2014](http://2014.conference-agile.fr/agile-france-2014-en-mots-et-en-images.html)
et [Sudweb 2014](http://sudweb.fr/2014/). Disons que c'était ma période
pull bleu et slides verts. J'espère redonner certaines sessions,
notamment "les organisations vivantes" (merci
[Fabrice](https://twitter.com/fabriceaimetti) pour le nouveau titre) cet
automne. C'est un peu la suite de [la horde
agile](/2014/01/mini-livre-la-horde-agile/) pour moi, et je me lancer
aussi dans un nouveau petit mini livre.

![Sudweb 2014](/images/2014/09/sudweb.png)

## ScrumDay 2014 : Open Agile Adoption

J'ai pu réaliser une session avec [Oana](https://twitter.com/ojuncu) sur
Open Agile Adoption, suite à notre extraordinaire rencontre avec Dan
Mezick.

Les slides "[open agile adoption](https://speakerdeck.com/pablopernot/open-agile-adoption-scrumday-2014)".

<iframe src="//player.vimeo.com/video/104987420" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/104987420">Open Agile Adoption - Un modèle pour la transformation agile - Pablo Pernot et Oana Juncu</a> from <a href="http://vimeo.com/smartview">SmartView</a> on <a href="https://vimeo.com">Vimeo</a>.</p> 

## Agile France 2014 : Les organisations vivantes

Le titre a changé, le contenu commence à changer, et j'espère redonner
ces sessions à l'automne. Voici les slides de Agile France 2014 :

<script async class="speakerdeck-embed" data-id="ce1eff00c5410131946106305ec17342" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>

## SudWeb 2014 : Épanouissement technique ?

Enfin une intervention rock'n roll à SudWeb 2014, dont j'ai déjà pas mal
parlé : [4 malaises et un très bon
moment](/2014/05/4-malaises-et-un-tres-bon-moment/).

<iframe src="//player.vimeo.com/video/103403865" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/103403865">D'où vient l'épanouissement technique ? David Bruant et Pablo Pernot</a> from <a href="http://vimeo.com/sudweb">Sud Web</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

