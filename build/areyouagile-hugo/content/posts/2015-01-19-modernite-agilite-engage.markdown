---
date: 2015-01-19T00:00:00Z
slug: modernite-agilite-engage
tags: ['complexite','complexe','modernite','engage','agilite']
title: Modernité, Agilité, Engagé
---

Durant ce trajet de TGV du dimanche 18 janvier de l'an de grâce 2015,
j'ai pris le soin de commenter ma bibliographie pour la consolidation
d'un travail pour un client. J'ai profité de cet effort pour l'adjoindre
à mon [guide de survie suite à la formation](/pdf/guiderapide.pdf). Et
du coup j'y ai aussi ajouté le point de départ de ce que j'espère être
un nouveau mini-livre sur mes réflexions (une suite à [la horde
agile](/2014/01/mini-livre-la-horde-agile/)). Ce point de départ c'est
trois tableaux : **modernité**, **agilité**, **engagé**. Pour ceux qui
me suivent, et je les en remercie chaleureusement, ce n'est pas
forcément très nouveau, pour les autres qui pourraient s'y intéresser,
et je les remercie aussi, c'est une bonne synthèse de mes réflexions
actuelles sur notre environnement.

J'espère bien finir avant le printemps ces "organisations vivantes",
cette suite à [la horde agile](/2014/01/mini-livre-la-horde-agile/),
mais pour l'instant le temps m'a manqué, ainsi que l'aspect
cannibalesque de mes réflexions : je pense avoir arrêter celles-ci mais
de nouvelles idées émergent et grignotent les précédentes. Et surtout je
cherche une cohérence peut-être vaine.

## Trois tableaux

J’aimerais vous décrire trois tableaux, trois points de départ décrivant
le monde dans lequel j’évolue.

Le premier tableau traite des deux approches méthodologiques et
organisationnelles qui ont façonnées les derniers siècles : celle de
Ford et celle de Toyota. Le but de ce premier tableau est de surtout
mettre en évidence que chaque époque à sa réponse propre, et qu’aucune
réponse n’est la bonne isolée de son contexte.

Le deuxième tableau traite de l’agilité, ce terme qui devient galvaudé,
mais qui représente encore beaucoup pour moi. L’agilité qui en soit est
l’aboutissement et la synthèse de notre compréhension de notre monde
nouveau, complexe.

Le troisième et dernier tableau traite de l’engagement des personnes,
point clefs qui fait la synthèse des deux premiers tableaux.

### Modernité

Au regard du dernier siècle et concernant l’organisation de nos
entreprises on pourrait distinguer deux modes de pensée assez
différents.Á chaque époque (début du XXème siècle, fin du XXème siècle)
des réponses adéquates mais très différentes car ne répondant pas aux
mêmes problématiques ont été proposées. Ces différentes réponses sont
assez bien mises en relief par la comparaison du Fordisme et Toyotisme.

Au début du XXème siècle, quand John Ford annonce « les gens peuvent
choisir n’importe quelle couleur pour la Ford T, du moment que c’est
noir », il a raison, personne ne songe encore à tous les progrès et les
variations qui suivront. Ford est en train de révolutionner nos
habitudes avec la voiture qui devient à partir de ce moment le meilleur
ami de l’homme. Mais à ce moment personne ne s’interroge sur la couleur
ou la forme de sa voiture, la question c’est d’en avoir une ou pas, tout
simplement. D’autres traits caractérisent cette époque et cette façon
d’aborder le travail : la main-d’œuvre est très bon marché, en
surnombre, pas formée, Ford va donc baser sa force sur le quantitatif et
non le qualitatif ; c’est l’heure de gloire du taylorisme. Ce taylorisme
se justifie très bien compte tenu du contexte qui le voit naître.

Chez Ford on observe aussi que la dernière étape avant la sortie de la
fameuse Ford T, c’est l’assurance qualité, le test, la recette en
quelque sorte. Et on tri : cette voiture sort, celle-ci non. Rien de
bien choquant pour l’époque, la matière première ne manque pas.

Quand une voiture sort elle s’accompagne d’un guide concernant les 150
anomalies les plus probables ou fréquentes. Là aussi, rien de choquant,
en ce début de siècle il suffit de se plonger dans ce manuel, de s’armer
d’un bon marteau, et le tour est joué. La réparation de la voiture n’est
pas un casse-tête chinois, et demeure accessible au commun des mortels.
Il est donc préférable pour Ford et le consommateur de laisser ces
anomalies à la charge de ses clients, le coût de revient est meilleur
pour tous.

Quarante, cinquante ans après, chez Toyota les choses ont bien changées.
C’est un pays détruit par la guerre que l’on découvre. La matière
première est devenue un élément critique. On ne peut pas gaspiller ou
stocker exagérément. Les machines outils sont aussi une denrée rare, il
va falloir les exploiter au maximum de leurs possibilités. En un
certains sens, tout comme les hommes, qui, à la demande du gouvernement
pour redresser le pays, sont embauchés à vie par Toyota. La technologie
a aussi changée, et elle continue de changer. Un marteau ne suffit plus
pour réparer sa voiture. Enfin les attentes du public ne sont plus les
mêmes. La firme japonaise est donc aux antipodes de la situation de
Ford. Autre époque, autres mœurs.

La réponse de Toyota est révolutionnaire elle aussi : pour tirer le
meilleur de ses employées, qui vont l’accompagner toutes leurs vies, la
firme comprend que le meilleur moyen est de les respecter et les
responsabiliser au maximum. C’est d’ailleurs en les responsabilisant
qu’elle tire le meilleur parti de ses machines-outils. L’homme de
terrain démontrant une capacité indéniable à adapter ces machines et à
avoir des idées pour les exploiter au mieux. La réflexion pour
l’amélioration continue devient une pratique courante chez eux. Cette
pratique ne se limite pas en interne, les fournisseurs sont encouragés à
trouver des moyens d’améliorer leurs produits ou leurs capacité de
production : ils ne répercuteront cette amélioration que deux ans après
celle-ci.

Pour palier au problème du stock, Toyota découvre que le meilleur coût
provient de deux choses : a) la qualité intrinsèque : dès qu’une
anomalie est détectée on stoppe la chaîne, l’impact et la correction de
cette anomalie seront moindres, et le stock touché minime ; b) une
gestion par flux tendu régulée par une participation active des
employés, c’est eux qui tirent le flux (ils vont chercher et déclencher
leurs actions qui s’adaptent ainsi parfaitement à leurs rythmes et à
leur compétences, d’où aussi une forte responsabilisation), et ce n’est
pas un flux poussé (là les employés subissent le rythme).

Il ne s’agit pas de réponses idéologiques en corrélation avec chaque
époque. Nous parlons d’industrie, de compagnies, dont la raison d’être
est de générer des bénéfices. Il s’agissait des meilleures réponses en
terme de performance d’entreprise.

Aujourd’hui beaucoup de nos entreprises évoluent encore sous le modèle
du Fordisme, mais c’est bien, même si il devient lui aussi peu à peu
obsolète, le modèle de Toyota qui devrait nous inspirer : qualité
intrinsèque (pas parce que cela fait, mais parce que cela coûte moins
cher et génère plus de revenu), responsabilisation (pas parce que cela
fait bien, mais parce que cela génère plus de valeur, de revenus).

J’insiste volontairement sur l’aspect purement économique. Ce n’est pas
nécessairement ma tasse de thé mais j’évolue moi-même en environnement
concurrentiel et je sais que c’est là le nerf de la guerre pour les
entreprises qui font appel à moi. La bonne nouvelle c’est que pour faire
réussir son entreprise aujourd’hui il faut engager et responsabiliser
les personnes comme a pu le faire Toyota.

### Agilité

Cette réponse aux temps modernes de Toyota est ce que l’on appelle le «
Lean ». L’héritier du « Lean »est le mouvement « Agile » :
l’intensification de la communication, le renouvellement de la
concurrence, des marchés et des technologies nous ont fait basculer dans
les temps complexes. Pour comprendre l’Agile au regard du Lean
aujourd’hui il faudra retenir l’adaptation et non pas la standardisation
industrielle.

![Les temps complexes](/images/2015/01/temps-complexes-3.png)

Nous sommes entré dans les temps complexes : tout va très vite, trop
vite, tout est entrelacé, tout change, tout le temps, etc. La meilleure
façon de répondre à cette complexité c’est de responsabiliser, d’engager
les gens. Comme un général qui ne peut plus tout diriger comme il y a
200 ans, et qui doit se reposer sur l’auto-organisation de ses troupes,
celles-ci restent fidèles à sa stratégie, à sa vision mais mettent en
œuvre leurs tactiques.

Cette responsabilisation (et ainsi la libération de cette capacité de
performance) n’est réelle que si on laisse le droit à l’erreur, sinon
c’est juste un faux-semblant et les effet escomptés ne sont pas au
rendez-vous.

Mais la responsabilisation est aussi possible si celle-ci ne met pas
l’organisation en danger (ou le projet, etc.), sinon qui osera quoi que
ce soit ?

Il faut donc soutenir la responsabilisation mais uniquement si l’erreur
est possible, mais elle ne doit pas être « mortelle ». Pour cela il faut
avancer par petits pas, de façon itérative. Et juger étapes par étapes
de nos progrès. Une petite fièvre ça soigne, une grosse, ça tue !

Mais pour vraiment juger il faut des choses finies. Peu importe
l’itératif si les choses ne sont pas finies (c’est alors de
l’incrémental...). Interrogez vous très fort sur cette notion de fini
(le mieux est de mettre sur le marché ! pensez au produit viable
minimum). On aura un vrai feedback, de vraies réponses, qu’avec des
choses finies.

Si vous réussissez à délivrer des choses finies, étape par étape, en
étoffant, alors autant prioriser par valeur. Car a) on a jamais le temps
de tout faire, donc autant faire uniquement les choses de valeur, b)
cela permet de se donner les plus grandes garanties et les plus grands
enseignements, et enfin c) d’avoir un sacré « time to market »(c’est à
dire pouvoir mettre sur le marché de façon très réactive).

Ce « Time to Market »très utile en ces temps de grande complexité, car
tout change si vite.

Naturellement ce cycle n’est apprenant (c’est nécessaire dans cette
complexité ambiante) que si l’on s’interroge constamment sur comment
s’améliorer. Voilà en quelques mots ce qui porte à mes yeux la notion
d’agile aujourd’hui, une réponse adaptée aux temps complexes :
responsabilisation, feedback, apprentissage, time to market. La
complexité nécessite de l’émergence (feedback, apprentissage et ainsi
time to market), qui nécessite de l’interaction (responsabilisation,
amélioration continue, intelligence collective).

### Engagé

Toyota, le Lean, puis l’Agile : on évoque constamment responsabilisation
et l’engagement. Mais il est vain de dire « soit motivé ! », on connaît
l’absurdité d’une telle démarche.

Les récentes études de Gallup indiquent que seulement 13% des employés
dans le monde ne se sentent pas impliqués, ou n’ont pas envie de l’être.
Voilà qui ouvre le débat sur la performance de nos organisations.

Dans le *Mythical Man Month* l’auteur (Fred Brooks) exprime l’idée
qu’une personne (en l’occurrence dans son cadre, un développeur
informatique) à un ratio de productivité de un à dix selon le contexte.
Sa capacité fluctuerait ainsi de un à dix selon le contexte. Mes
observations -sur moi même et les autres- tendent à confirmer cette
information. Et je pense que la grand part de cet écart viens de
l’implication de la personne. Observez vous vous même à réaliser une
tâche sur laquelle vous ne vous sentez pas impliqué, et une sur laquelle
vous vous sentez impliqué, que vous vous êtes approprié.

On ne peut pas décrété la motivation, ni l’implication, ni
l’appropriation. Observons donc les domaines où elles sont florissantes,
un de ceux-ci est bien connu et a donné lieu à des études sur cette
implication : les jeux vidéos, et plutôt les joueurs de vidéos.

On observe que l’implication dans un jeu vidéo est souvent bien
supérieure à celle dans la vie d’entreprise. Tous ces joueurs de jeux en
ligne qui payent pour échouer, recommencer, apprendre, refaire, fournir
une documentation de qualité, ne pourrait-on pas avoir le même
engagement en entreprise ? L’industrie du jeu vidéo est florissante,
comprenez qu’elle implique d’énormes sommes d’argent. Avant donc pour
nous même d’en tirer des enseignements pour l’organisation, elle s’est
interrogée sur elle-même car son succès est liée à l’implication qu’elle
va engendrer. Les observations qui ont découlé de ces recherches sont
une mine pour nous.

Mais alors que j’évoque ce sujet, je reçois le bulletin scolaire de mon
fils aîné, Mathieu. Mathieu a de bons résultats scolaires mais le carnet
indique que si Mathieu était impliqué il aurait de très bons résultats
scolaires. Je cherche Mathieu :

-   Mathieu ? Mathieu ?
-   (de l’autre pièce) Oui papa ?
-   Peux-tu venir que l’on parle de ton bulletin scolaire.
-   Attend, là je ne peux pas, je suis en train de construire la
    quatrième tour de mon château fort 4 et elle n’a pas exactement les
    mêmes décorations que les autres, ni la même constitution en fer, je
    ne peux pas la laisser ainsi.
-   Oui c’est exactement de cela que je souhaite te parler...

En d’autres termes Mathieu est très impliqué dans son jeu, et ses
résultats sont très bons. Qu’est ce qui manque à l’école vous récolter
ce même engagement ? Qu’est-ce qui manque à l’entreprise pour avoir ce
même type d’implication ? C’est le travail de synthèse opéré par Dan
Mezick.

Un dernier détour par l’école de mon fils aîné. Réunion
parent/professeur, je suis en tête à tête avec la professeur principale,
elle se plaint de la qualité générale de la classe, de son manque de
sérieux et du niveau de son travail. Puis nous passons à la récente
sortie pédagogique sur les fouilles archéologiques (plusieurs jours sur
site). Elle se délecte de la réussite de cet événement, et dans les
minutes qui suivent m’indique que la classe a fourni un excellent
travail et dossier. Surpris je l’interroge donc sur l’écart entre le
travail quotidien et ce travail là. Elle reste plusieurs secondes
silencieuse comme si un piège s’était refermé sur elle puis me glisse,
un peu désarçonnée : mais on ne peut pas faire des choses nouvelles
intéressantes comme cela tout le temps. Je suis resté silencieux en
pensant que c’était bien de l’éducation et de la vie de nos enfants dont
on parlait alors.

Opérons là aussi un rapprochement évident avec nos entreprises : si vous
faîtes des choses qui intéressent les personnes avec lesquelles vous
travaillez, si vous réussissez à les impliquer, ce qui en résultera
n’aura rien à voir.

Ainsi que nous enseigne ces études sur l’implication dans les jeux
vidéos ? Des choses simples, évidentes, mais trop souvent oubliées, ou
délaissées. Pour qu’un jeu (ou une entreprise) génère de l’engagement il
doit proposer :

-   Un **objectif clair** : a-t-on des objectifs clairs dans nos
    organisations ?
-   Des **règles claires** : a-t-on des règles claires dans nos
    organisations ? Un cadre bien défini ?
-   Du **feedback** : a-t-on régulièrement du feedback ? Sait-on où nous
    en sommes ? Quel est le résultat de nos dernières actions ?
-   Le jeu est une **invitation**, nul n’est obligé d’y jouer : cette
    notion d’invitation existe-elle dans nos organisations ?

En proposant ce cadre le jeu (ou l’organisation) va générer plusieurs
sentiments qui sont la base de cette implication et de cet
épanouissement.

-   Un **sentiment de contrôle**, dans le sens maîtrise, je peux agir et
    je sais comment agir.
-   Un **sentiment de progrès**, j’avance et j’observe que j’avance.
-   Un **sentiment d’appartenance à une communauté**.
-   Le **sentiment de travailler pour quelque chose qui me dépasse**,
    quelque chose de plus grand que soi, on se projette dans une
    réalisation qui nous dépasse.

Voilà des points simples mais cruciaux sur l’implication.

