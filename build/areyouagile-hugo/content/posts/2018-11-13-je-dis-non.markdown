﻿---
date: 2018-11-13
slug: je-dis-non
tags: [non, management, situation]
title: "Je dis non"
--- 

Enfin j'essaye de bien dire non. 

Chez beNext je ne suis pas seulement coach. Je suis directeur général, investi de la mission d'être le garde-fou ou le bouffon du roi, celui de David, actionnaire unique et fondateur. Je me fais aider par Dragos, l'observateur à l’œil perçant et à la parole sage. Notre trio fonctionne à merveille (jusqu'à maintenant, merci de toucher du bois). Il se nourrit d'une tension positive. Mais alors que la société s’épanouit, je deviens de plus en plus ce gardien du temple, ça m'use.   

Nous essayons de définir au maximum un cadre (au travers de notre approche [sociocratique/holacratique](/2017/12/petite-serie-sur-holacratie-et-sociocratie/) que l'on appelle gothamocratie), mais aussi en parlant constamment aux personnes. En tous cas j'essaye. Et je laisse faire les gens ce qu'ils veulent dans ce cadre (en tous cas, j'essaye *bis repetita*), avec pour posture, pour rôle, de les rattraper, de les bloquer, de leur dire non, s'ils en sortent, s'ils font quelque chose qui ne convient pas à nos valeurs, ni au sens de la société. Surtout j'essaye qu'ils n'attendent pas mon aval, mais plutôt de les rattraper au vol si besoin.

J'essaye, ce n'est pas simple de faire passer ce message, car plus je dis "non" plus je freine des initiatives alors que justement le but est de les provoquer. Pourtant plus je dis "non" plus ils devraient comprendre qu'ils peuvent essayer, c'est paradoxal et difficile à manipuler, je ne réussis pas bien. S'il n'y a jamais de "non" c'est que l'on n’essaye rien (ou que l'on est d'accord avec tout et qu'il n'y a pas de sens). Mais je dois mal dire "non". Posture compliquée que j'essaye de revoir en permanence. Et plus la société grandit plus je dis "non", car il y a de plus en plus de façon de sortir du cadre, d'oublier le sens, de piétiner les valeurs. De passer d'un cercle vertueux à un cercle vicieux. Et plus je dis non, plus je veille, moins j'expérimente.

Je dis "non" quand on bafoue ou qu'on oublie nos valeurs (pas de recherche d'excellence, du *bullshit*, pas assez d'humilité, trop sérieux (pas assez de *fun*)) ou notre sens (*be your potential*, épanouir le potentiel des benexters et de nos clients, ou plus récemment, *hack your future*, une façon plus rebelle de dire : prenons en main notre futur, notre potentiel). 
 
Je dis "non" si souvent... trop souvent peut-être en ce moment. 

Non à cette personne pour qu'elle anime un meetup chez beNext. Non désolé elle n'est pas assez dans la recherche d'excellence, elle ne s'interroge pas assez, et ne véhicule pas assez l'envie d'aller plus loin. Cela ressemble plus à simplement une opération de communication et ne diffuse pas un réel apprentissage. Donc non. Je ne dirais pas non tout le temps, mais il faut d'abord que VOUS vous posiez des questions. Et tous les copains de cette personne m'en veulent. 

Non à cette personne qui postule chez nous. Non, car elle n'est pas encore prête, elle ne sait pas encore ce qu'elle veut. Cela ne rendrait service à personne. Ou NON à celle-ci encore, car elle ne poursuit pas cette recherche d'excellence, cette introspection, cette volonté d'essayer et d'apprendre, peut-être même qu'elle est suffisante, hautaine. Donc NON. Ou NON à celle-ci, car vraiment elle n'a pas les bonnes valeurs. Et tous les copains de cette personne m'en veulent. 

Je dis "non" tout le monde n'est pas égal. Je dis "non" tout le monde ne doit pas décider sur tous les sujets. Car naturellement si tout le monde décide il y a une complaisance et une baisse flagrante du niveau des décisions. Je dis "non" tout le monde n'est pas payé pareil, naturellement que "non" tous les contextes ne sont pas les mêmes, ni les histoires. Je dis "non" on ne divulgue pas les salaires, car tout le monde n'est pas assez mature pour comprendre et gérer intelligemment cette information, ou d'autres informations[^salaire]. Je dis "non" tout le monde n'a pas le même pouvoir de décision. Pour les mêmes raisons. Et surtout mon avis peut changer. Je l'exprime que cela peut changer, j'exprime à voix haute mes interrogations et mes réflexions, mon cheminement. Mais tout le groupe me suspecte. Je peux changer d'avis, mais aujourd'hui c'est mon avis, et il me paraît conforme avec le bon épanouissement de la société.      

[^salaire]: c'est un non facile à hacker ils ne s'en privent pas. 

Je dis "non" à telle ou telle proposition commerciale, car elle n'entre en rien dans l'épanouissement du "benexter" ou du potentiel de notre client. Il n'y aura ni plaisir ni impact. Je dis "non" quand il faut se contraindre à un formalisme bureaucratique aussi obsolète que stupide, car justement nous luttons tous les jours pour un monde meilleur, en recherche d'excellence et pour le #nobullshit. Je dis "non" pour faire un dossier de compétence abscons pour un groupe achat qui a perdu contact avec la réalité. Et toute l'équipe commerciale me regarde avec un regard noir. 

"Pablo est dur" ou "Pablo fait peur" fleurissent sur le slack ou sur nos CR de Gothamocratie ([holacratie/sociocratie](/2017/12/petite-serie-sur-holacratie-et-sociocratie/)). Je n'en suis pas fier, ce n'est pas agréable, mais c'est aussi possible de l'écrire au vu et au su de tous, ça me rassure. 

Dire non, c'est refuser la facilité. C'est la condition éreintante pour prendre *par ailleurs* un plaisir fou à vivre des choses passionnantes. 

