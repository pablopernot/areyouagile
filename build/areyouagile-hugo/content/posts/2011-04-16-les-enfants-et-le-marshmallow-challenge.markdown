---
date: 2011-04-16T00:00:00Z
slug: les-enfants-et-le-marshmallow-challenge
tags: ['seriousgame','workshop','atelier','jeu']
title: Les enfants et le marshmallow challenge
---

J'ai été initié il y a peu de temps par
[Pierre](http://managingagile.blogspot.com/) au [Marshmallow
Challenge](http://marshmallowchallenge.com/). Ce challenge très ludique
permet de mettre en évidence l'intérêt du travail par itération (et
optimisation), l'intérêt des équipes hétérogènes et complémentaires. Je
vous recommande de vous plonger dans la
[documentation](http://marshmallowchallenge.com/Welcome.html) et
d'essayer au moins une fois, c'est simple et rapide. Joué plus 70 000
fois, le plus grand rassemblement a réuni 800 personnes, ce n'est pas
initialement un atelier agile et pourtant il rempli fort bien ce rôle.
Le défi est -à partir de 20 spaghetti, d'1 mètre de scotch, d'1 mètre de
corde et d'un marshmallow-de monter la tour la plus haute, le
marshmallow devant figurer (intègre) à son sommet. Pour cela 18 minutes.

![Marshmallow](/images/2011/04/mc_adultes_7.png)

En analysant les résultats, les auteurs ont découverts que si les
étudiants ou les avocats échouent généralement du fait de leur trop
grande planification, les enfants d'écoles primaires sont des
"performeurs" ! Ok ok ok. L'idée est plaisante, mais qu'un groupe
d'enfants d'école primaire soit plus performant qu'un groupe adulte à
fabriquer un tour... L'idée est belle, mais je me disais qu'elle était
un peu "arrangée".

J'ai donc profité cet après-midi de l'anniversaire des 9 ans de mon fils
dans la [Garrigue](http://fr.wikipedia.org/wiki/Garrigue) du sud de la
France pour challenger le challenge. C'était un environnement un peu
hostile : pas de table sauf une, des groupes de plus de 4 personnes. Les
tours n'ont donc pas été extraordinaires, mais peu importe le résultat a
été au delà de mes espérances. Naturellement les enfants ont battu à
plat de coutures les adultes. Les deux groupes d'enfants présentant une
tour qui tenait à la fin, à l'inverse du groupe d'adultes.

Comme annoncé, mais c'est toujours savoureux de le vivre, les enfants
n'ont pas cherché "midi à quatorze heures". Ils ont fonctionné en
groupes soudés, par tâtonnement et optimisation. Et ce dès le début du
chrono.

![Marshmallow](/images/2011/04/mc_enfants_3.png)

![Marshmallow](/images/2011/04/mc_adultes_6.png)

Les adultes quand à eux, ont commencé tard, avec des idées biscornues.
(Pour d'évidentes raisons, dont le divorce encouru, vous comprendrez que
je cache les visages).

![Marshmallow](/images/2011/04/mc_adultes_1.png)

Les enfants sont naturellement arrivés au bout de l'objectif. Les
adultes ? ... ils ont d'abord décidé de repartir à zéro, voir avec 3
personnes travaillant sur 3 tours différentes (si si ! jamais les
enfants n'auraient pensé à cela...). Puis finalement de se précipiter
vers une solution de désespoir quand le compte à rebours est arrivé à
3mn. Solution désespérée puisqu'il s'agissait de monter un tour très
haute, pour finalement placer beaucoup trop tard le marshmallow au bout
d'une pâte, et voir la tour s'effondrer -grand classique du marshmallow
challenge-. A noter que les adultes se sont plaints du matériel (et pas
les enfants...).

![Marshmallow](/images/2011/04/mc_enfants_2.png)

![Marshmallow](/images/2011/04/mc_adultes_3.png)

L'effet "TADA ! ... ... Ooooohhhhhh.... "

![Marshmallow  ](/images/2011/04/mc_adultes_5.png)

And the winners are :

![Marshmallow](/images/2011/04/mc_winners.png)

