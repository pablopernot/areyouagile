---
date: 2018-12-08
slug: management-hier-aujourdh'ui
tags: [management, travail]
title: Management d'hier et d'aujourd'hui
--- 

On a découvert qu'en réduisant l'activité des gens a seulement quelques actes on augmentait plus facilement leur savoir-faire et ainsi leur productivité. Par exemple que les développeurs développent et se focalisent là-dessus, et que les gens de l'infrastructure se focalisent sur l'infrastructure, leur dextérité augmentera et ainsi la productivité. 

On a découvert qu'en spécialisant et en divisant le travail on limitait le temps de pause. Quand, par exemple, les gens du test ne font que du test, ils ne perdent pas de temps à s'occuper du reste.  

On a découvert qu'en divisant le travail, en spécialisant les personnes, d'un côté ceux qui pensent, de l'autre ceux qui font, on pourra remplacer ceux qui font par des machines, et on va renforcer le savoir en rassemblant ceux qui pensent.   

On a découvert que si on utilisait l'intérêt et l'égoïsme comme moteur d'échange entre les personnes on augmentait la productivité. Car l'échange enrichit. Surtout donc si il est provoqué par l'égoïsme et l'intérêt.  

On a découvert tout cela, mais on l'a découvert au XVIIIème siècle. 

Il s'agit des analyses et propositions de Adam Smith ([Adam Smith 1723-1790](https://fr.wikipedia.org/wiki/Adam_Smith)). Elles seront reprises dès la fin du XVIIIème siècle, reprises et poussées par [Frederick Winslow Taylor](https://fr.wikipedia.org/wiki/Frederick_Winslow_Taylor) au début du XXème siècle chez Ford. 

La notion de travail apparaît d'ailleurs au XVIIIème siècle, travail dans le sens je paye le temps de quelqu'un, avant il s'agissait de payer quelqu'un contre quelque chose de palpable (lire [Bullshit Jobs - David Graeber](http://www.editionslesliensquiliberent.fr/livre-Bullshit_Jobs-546-1-1-0-1.html)). La notion de travail mélange alors l'idée de création de valeur, l'idée que le travail est le sens de la vie (pas que l'on travaille pour un sens mais qu'il est le sens), et l'idée que le travail devient un indicateur social, un système de distribution des revenus, des droits et des protections.  

Et le management, la notion de management, et donc de manager, apparaissent ainsi au plein de cœur de ces idées, vers la fin du XIXème siècle. 

Et naturellement aujourd'hui tout ceci est devenu complètement anachronique, complètement faux.

Mais comme le management est né là-dedans c'est le point de départ pour beaucoup. 

Toute l'idée c'est de savoir s'en détacher, car cette approche du management est obsolète. 

Aujourd'hui c'est complètement obsolète de mesurer le temps passé au travail et pas l'impact généré, la valeur créée. Je me fous du nombre de fonctionnalités que vous avez développées, du nombre d'heures que vous avez passé, ma question c'est quel impact avez-vous eu ? 

Le *slack time*, le temps passé hors de la production elle-même, les pauses, les distractions, les sujets hors contextes sont des sources de richesses pour penser différemment, pour penser tout simplement.

Avoir une approche "bout à bout" est essentielle pour délivrer et apprendre vite.

Avoir une approche dans son ensemble donne aussi du sens, fait percevoir l'objectif, nourrit en *feedback*. 

La plus grande maladie du siècle est peut-être le *bore out*, cet ennui mortel qui se répand dans les entreprises qui n'ont pas de sens, dans lesquelles on travaille pour le travail, où on travaille parce que l'on travaille, et pas pour avoir un impact qui a du sens. 

Avoir un travail qui a du sens est ce que désire peut-être le plus les personnes qui vous accompagnent. 

	"Don't pick a job. Pick a boss. 
	Your first boss is the biggest factor in your career success. 
	A boss who doesn't trust you won't give you opportunities to grow" 
	-- William Raduchel. 

Qui a un *boss* qui le fait grandir ? 

Est-ce que vous faites grandir les gens qui travaillent pour vous, avec vous ? 

Il y a des gens qui critiquent cette phrase (sur internet). Ils expliquent qu'il ne faut pas s'enticher du meilleur *boss*, car celui-ci sera destiné à évoluer et à partir, ou qu'il est le meilleur dans son domaine, mais que ce domaine n'est pas forcément le vôtre. Mais ils ont mal lu. Il n'est pas écrit le meilleur. Il n'est pas écrit pour faire comme lui. Il est écrit celui qui vous fait confiance et qui vous donne des opportunités pour grandir.  

Je repose donc la question : qui a un boss qui lui fait confiance et qui lui donne des opportunités de grandir ? Avez-vous des exemples ? 

Je repose donc la question : est-ce que vous faites confiance à vos équipes ? Est-ce que vous leur donnez l'opportunité de grandir ? Avez-vous des exemples ?  

Ou est-ce que vous pensez, comme au XVIIIème siècle, qu'il vaut mieux que certains réfléchissent pour d'autres ? Qu'il vaut mieux limiter au maximum la richesse de ce que pourrait réaliser une personne pour la spécialiser sur une tâche répétitive ? Que l'égoïsme est un bon levier ? Limiter le temps de la réflexion (pas de pause, pas d'espace pour laisser l'esprit cheminer) pour ne laisser place qu'à la production, qu'aux gestes ?  

Le management moderne c'est permettre à ses équipes de réfléchir, de décider. 


