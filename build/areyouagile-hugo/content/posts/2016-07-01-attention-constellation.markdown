---
date: 2016-07-01T00:00:00Z
slug: attention-et-constellation
tags: ['constellation','management','systemique','ecoute','observation','coaching']
title: Attention et constellation
---

Et si vous arrêtiez d'écouter pour être attentif ? Attentif à ce qui se
passe autour. La prédominance de l'écoute dans le discours des coachs,
consultants, gurus et autres est exagérée. En réalité cela pourrait
autant m'amuser que m'user. Est-ce qu'il s'agirait parfois plus d'une
posture que d'une réalité ? Note à soi-même : nonobstant c'est assez
reposant comme posture. L'observation de réalité par des chercheurs
indique que finalement très peu de choses passent par les mots, par
l'écoute des mots, par le sens des mots ?

## Arrêtez d'écoutez, observez

Une étude connue de [Albert Mehrabian](https://fr.wikipedia.org/wiki/Albert_Mehrabian) a montré que
seulement 7% de la communication passait par les mots que l'on écoute.
Que la communication transiterait bien plus par le ton de la voix (38%)
et l'attitude corporelle, l'impression visuelle (55%). **Mais** cette
étude a été remise en question, par son auteur lui-même, cela ne
concernerait *que* les communications relatives aux sentiments et à
l'état d'esprit (quand on parle de ses sentiments, de son état
d'esprit). Note pour les coachs : il faut donc se rappeler **de moins
écouter** quand les gens parlent de leurs sentiments de leurs émotions,
de leurs états d'esprit, et **plus observer**.

Je m'amuse, naturellement qu'il faut porter une grande attention à
l'autre, oui naturellement l'écoute inclus le ton. Mais bien souvent
cette attention trouvera matière non pas dans les mots mais toute la
[chorégraphie](https://fr.wikipedia.org/wiki/Communication_non_verbale)
qui les entoure. Faire attention à l'autre c'est plus l'observer que
l'écouter peut-être. À la maxime : **arrêtez de parler, agissez**,
j'aimerais compléter avec : **arrêtez d'écouter, observez**.

Faîtes un jeu : prenez 10 minutes lors de votre prochaine réunion à ne
pas entendre les mots, à ne pas écouter. Mais juste à observer les
mimiques, les postures, les tons, les gestes, les attitudes. Où et
comment se tient la personne de pouvoir dans la salle ? Et vous ? Je me
rappelle de [Dan](http://newtechusa.net/dan-mezick/) (Mezick) qui me
disait qu'en tant que coach on devait se mettre dos à la porte d'entrée,
vulnérable, et pas de l'autre côté de la table, avec le dos protégé, en
mode "fort Alamo". Bref, notez les corrélations entre attitude et
contenu, notez les récurrences avec le temps. Les mots sont ambigus.
Rien qu'en lisant ce petit texte vous embarquez votre propre mythologie,
qui n'est pas la mienne, nous divergeons déjà. Cette divergence peut
nous mener dans des lieux intéressants, mais nous divergeons.

Le ton, la chorégraphie qui accompagnent les mots c'est déjà autant de
feedback sur lesquels se reposer pour moins diverger, à condition d'y
être attentif. La prévalence de la conversation sur la spécification
écrite vient de là. Mais c'est bien une conversation pas un monologue.
La conversation est une boucle de feedback, de reformulation (d'où
l'importance de la reformulation pour les "managers", les "leaders" et
autres animaux du bestiaire moderne).

## Constellations, coaching systémique

Tout cela me ramène aussi à une session de [Andrea
Provaglio](http://andreaprovaglio.com/) sur les constellations
systémiques et le coaching associé. Là pas de mots, mais des postures,
des attitudes, des endroits où l'on se tient, tourné, de dos, de face,
penché, assis, on peut se voir, ne pas se voir : des constellations
systémiques. Alors attention quand on cherche "constellations
systémiques" on tombe rapidement sur des choses liées à des thérapies
familiales, et là, kryptonite, je ne connais pas, et cela à l'air lourd.
Moi je vous parle de ce qui semble être une extension de ces méthodes
dans le domaine du **coaching systémique** donc de dynamique
d'organisation. C'était le sujet de la session de Andrea, pas autre
chose.

![La session de Andrea](/images/2016/07/constellation.jpg)

Ainsi dans cette session pas de parole, mais des postures, des
positions. Andrea choisit un sujet, dans les deux sens : une personne et
un thème qu'elle souhaite porter. Cette personne raconte son histoire et
Andrea en extrait cinq ou six concepts : autant des acteurs de
l'histoire (l'expert, le directeur général, le RH, etc.) que des objets
ou concepts (argent, augmentation, performance, fierté). Puis il écrit
chacun de ces concepts sur une feuille A4, et il demande ensuite à la
personne de placer au sol ces feuilles A4 selon le sens et la dynamique
de ces concepts dans "la vraie vie". Afin de les rendre encore plus
palpables il demande aux participants de la session de venir incarner
ces concepts en se plaçant sur les feuilles. Dès lors on perçoit
beaucoup plus les éloignements, les rapprochements, le champs de vision
de chacun. La scène est devenue concrète, palpable, incarnée. Au sujet,
à la personne qui le porte, d'observer et de déplacer les personnes en
fonction de ce qu'elle recherche : on rapproche des personnes/concepts,
on les éloigne, on s'intercale, on rend visible, on agenouille, on
redresse. C'est très visuel, la scène, **la question est incarnée**, et
provoque une petite révélation.

![La session de Andrea](/images/2015/11/andrea-constellations.jpg)

Imaginez quelqu'un qui fasse "goulot" qui se retrouve au milieu d'un
groupe à gauche, d'un groupe à droite. Imaginez trop d'interfaces, le
groupe à gauche ne voit pas le groupe à droite caché par les personnes
au milieu. Imaginez deux personnes côte à côte qui sont de dos l'une de
l'autre. Imaginez "eux ils sont là dans leur coin". Imaginez "tout le
monde se tourne vers lui, mais il ne voit que la moitié des personnes".
Etc.

Dès lors des clefs sont apparentes. Pas de parole, on a rendu
**physiquement intelligible** une problématique, sans tomber dans
l'**équivoque des mots**. Moins parler, moins écouter, plus observer,
moins parler mais rendre palpable.

Ps: vous noterez sur le *paperboard* de Andrea, ci-joint (cliquez dessus
pour agrandir), "sensations yes, feelings no" : on n'embarque pas de
mots qui peuvent rendre les choses ambiguës.

![Constellation](/images/2016/07/constellation2.jpg)
