---
date: 2012-12-18T00:00:00Z
slug: peetic-conversations
tags: ['peetic','storytelling','conversation','dialogue']
title: Peetic, conversations
---

**Quelques anecdotes avec l'équipe Peetic et autres tranches de vie.**

Quelques dialogues et tranches de vie imaginées autour d'équipe agile,
notamment celle de [Peetic](/peetic.html). Comme c'est reposant de faire
les questions et les réponses, tout seul...

Voici *le dog allemand sans queue, Le diable se cache dans les
détails,Comment on a réussi à monter une méga architecture au début du
projet qui a répondu à tous les besoins, Le samedi patate, C'est des
adultes, Sociocratie, frustrocratie, Ok*

## L'histoire du Dog allemand sans queue

- Ah je me rappelle les premières itérations on estimait en point (de la
suite de Fibonacci). Mais bon finalement on s'en servait peu, notre
engagement on le ressentait (les tripes, toujours les tripes).
Aujourd'hui on continue d'estimer en chien ou chat (Ah oui, un dog
allemand c'est plus long à réaliser qu'un caniche même sans la queue !
ben oui on est chez [Peetic](/peetic.html)) mais plus de point : le
management détournait ces estimations et cela nous bloquait malgré tout
(j'te parle même pas du jour homme...)

- Ah ah tu te rappelles la tête de Pierre quand on lui a dit qu'on
faisait deux labradors et un siamois ! Au début cela aurait été
impossible ! Et aussi le moment où il comparait la vélocité des
équipes...pffffff portnawak.

- Bah aujourd'hui on a compris que l'important était ailleurs : Pierre a
sa projection sur planning, son budget, son scope. Ses rapports sont au
poil... ah ah ah !  Et nous c'est discussions et comparaisons, ça c'est
sympa et efficace.

## Le diable se cache dans les détails

- Alors Sarah, contente ?! Scrummastrice ? Scrummasseuse ? Ouarf. Non je
blague je ne suis qu'un vieux phallocrate.

- Ouais je ne te le fais pas dire.

- Mais comment est-ce que tu t'es révélé dans ce rôle de scrummaster ?

- Bah tu sais je m'entends très bien avec Lionel depuis longtemps et
donc quand il a fallu apaiser toutes ces tensions et que nous ne
retrouvions sans scrummaster après le départ de Denis, et bien je me
suis dit : why not ?

- Ah oui Denis... dommage non ? Au début je pensais sincèrement que le
chef de projet était la bonne personne pour devenir scrummaster. Et au
pire je me disais que changer les règles des ressources humaines, les
titres, et tout le tralala cela aurait été trop long. Donc basta Denis
=chef de projet = scrummaster. En plus Denis voulait, il imaginait qu'il
gardait « son » pouvoir. Mais bon on a compris que le « pouvoir » du
scrummaster était autre. Pas celui que Denis s'imaginait.

- Oui le pauvre a été assez malheureux. Personne n'y a trouvé son
compte. Ni nous, ni lui, ni l'organisation. Satanée manie de ne pas
oser. Mais si il avait voulu réintégrer l'équipe ou devenir Product
Owner, au lieu d'être Scrummaster comment aurais-tu fait ? Je veux dire
pour son salaire ? Pour son plan de carrière ?

- Au sein de l'équipe ? franchement je ne sais pas. Mais j'aurais du
vraiment me poser la question. Et Product Owner tu pouvais oublier,
Sylviane n'aurait jamais voulu, un chef de projet responsable du
produit, crime de lèse majesté !

- Mais il aurait été bon !

- Je sais, mais que veux-tu...

- Tu aurais du essayer.

- Euh c'est mon entretien ou le tien ?

- Ok ok (sourire) écoute moi on est vite arrivé à une décision avec les
autres. J'étais le nouveau scrummaster. Et comme je te le disais mes
bonnes relations avec Lionel ont permis d'appaiser la situation et tout
va à nouveau dans le bon sens.

- Coool

- Tu sais, le diable se cache dans les détails. T'as vu Clara. Un
changement d'équipe et de cadre et c'est une autre personne.

- Ah oui je suis bluffé. Et dis moi donc tu as arrêté tout code et toute
estimation, tu réussis à être neutre.

- Oui, finalement c'est vrai job à temps plein, et passionnant. Rappelle
toi on se disait tous que scrummaster c'était le truc chiant sur un coin
de table, le porteur d'eau. Tu sais j'étais super fier de réussir
certaines choses. Autant dire je faisais ma diva et roulait des
mécaniques parfois. Mais c'était vraiment mérité. Et bien tu ne vas pas
le croire, mais je ressens encore de plaisir et de fierté à faire
réussir … les autres.

- Ah ah ! Bel épanouissement.

## Comment on a réussi à monter une méga architecture au début du projet qui a répondu à tous les besoins

- Ca a marché ça ?

- Non, jamais.

## Le samedi patate

- J'en ai marre de l'aaaaggiiillllle. Attend on nous prend pour des
robots ! Le lundi planning, le vendredi review et retro, et le samedi
patate ! Ca s'arrête un jour ?

- Mais qu'est qui te frustre comme ça ? La répétition ? Le produit
n'évolue pas c'est toujours la même chose ?

- Non c'est vrai le produit évolue constamment. Et du coup si il y a 3
mois on était sur une boutique ecommerce assez classique, on arrive
aujourd'hui à traiter des sujets assez innovants. C'est cool ça.

- Bon alors ? Le stress ?

- Déjà plus. J'ai besoin de lever la tête de temps en temps. De
respirer, de souffler.

- Tu ne le fais pas ?

- Non.

- Mais qui t'en empêche ?

- Ah facile à dire, tu ne crois pas que Sylviane va accepter cela.

- Tu as essayé ? Et puis souffler cela veut dire quoi ?

- Souffler ? Avoir un rythme moins soutenu, prendre plus le temps
d'essayer de nouvelles choses, la plate-forme d'intégration continue
aurait besoin d'un peu d'huile.

- Propose à Sylviane. Tout cela n'est que bénéfique au projet. Et puis
te voir te cramer cela ne rendra service à personne. Tu es d'accord tout
cela est bénéfique pour le projet ?

- Ben oui, c'est évident.

- Bon ben tu as ta conscience pour toi, le plaisir du devoir bien
accompli. Eh eh.

- Et si elle n'est toujours pas d'accord ?

- Et bien fais le quand même. Non ?

- Et si on me blame ?

- Combien de temps pour trouver un nouveau job dans le coin ?

## C'est des adultes

- On est coincé, on a pas eu d'unanimité au sein de l'équipe sur la
solution à prendre.

- Ah. Et donc.

- Et bien on est coincé.

- Et votre engagement ?

- On ne pourra pas le tenir, sauf si on choisi arbitrairement sans
unanimité une solution.

- Pourquoi vous coincez sur cette question d'unanimité ?

- Ben c'est l'auto-organisation agile.

- Ah bon ? Tu lis « unanimité » dans « auto-organisation » ?

- Ok mais si on prend la solution de Laurent, Sandra va faire la gueule
et vice verça.

- Ca arrive. Je m'inquiète pas c'est des adultes, ils se sont tous
trompés au moins une fois.

## Sociocratie, frustrocratie.

- R\^ôôôôh dit donc, t'as vu l'équipe de Jean-René à côté.

- Oui ils en avait marre de lui, ils ont pris la sociocratie comme alibi
pour le déboulonner !

- Sociocratie ? Frustrocratie oui.

- Ah ah Jean-René, tu sais c'est le beau parleur. Ca excite les
jalousies. Certains n'aiment pas qu'une tête dépasse. Toute tête qui
dépasse estaccusée de manipulation, surtout si c'est le scrummaster !
Ils ont décidé de faire un vote pour élire le nouveau scrummaster, mais
en fait tout ce qu'il voulait c'est faire un exemple.

- Mais ce n'est pas ça la sociocratie?!\# !

- Je sais mais bon, tu sais...

- Et alors ?

- Et bien ils ont élu David.

- Et ?

- Et bien tu sais David lui il parle peu, et en octets.

- Et ?

- Et le code de Jean-René est aussi monolithique que sa parole est
fluide.

- Et ?

- La communication a complètement changé au sein du projet.

- Et ?

- Le projet est arrêté.

- Ah ?

- Il a été dans le mur.

- Aïe.

- Plus rien ne fonctionnait.

- Oh.

## Ok

- Tu veux mon point de vue sur la question ?

- Ok.

- Je serai efficace et pertinent pour l'organisation si j'agis comme
développeur senior au sein des équipes. Je peux débloquer de nombreuses
situations, apporter de nombreuses solutions, accompagner de nombreuses
personnes.

- Ok.

- Et je serai heureux ainsi.

- Ok.

- Naturellement, à 45 ans, être un développeur sénior en France c'est
comme avouer une perversion mentale. Pas de cela ici. Il faudrait un
vrai parcours RH pour les aspects techniques, une vraie sensibilisation
et reconnaissance de cette branche, et une salaire en fonction.

- Ok.
