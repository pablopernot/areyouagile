---
date: 2010-12-14T00:00:00Z
slug: lignorance-est-une-benediction
tags: ['coaching']
title: L'ignorance est une bénédiction
---

Effet de bord très intéressant : en mission depuis septembre chez une
(très) belle enseigne : celle-ci édite des logiciels dans un domaine
assez spécifique et ma foi fort compliqué. Il découle de cela qu'il
m'est très difficile de m'immerger dans le métiers. Pour ainsi dire je
suis souvent paumé au milieu de la terminologie complexe et absconse du
domaine en question.

Là où les choses se révèlent intéressantes c'est que cette défaillance
apparaît comme une véritable bénédiction : je ne vois que ce que je dois
voir : les forces en présence, les pressions, les oublis manifestes, les
problèmes, les obstacles, etc. Cette ignorance renforce ma neutralité et
me donne une meilleure acuité.

Et la neutralité du scrummaster ou du coach agile, c'est toute sa force.
La neutralité va avec transparence.

Plus encore, quand je commence à intégrer le métiers je tombe parfois
dans le piège (très humain) de m'immiscer dans la réflexion et dans la
discussion. Je sors de mon rôle, happé par ma (meilleure) connaissance
du sujet. C'est d'ailleurs un piège récurrent : quand on intervient au
sein d'une équipe déjà formée de longue date et que l'un de ses membres
devient le scrummaster, il lui est difficile d'oublier son ancien
positionnement, son ancien rôle.

Voilà pour ce court billet.
