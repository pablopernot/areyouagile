---
date: 2016-10-17T00:00:00Z
slug: organisations-vivantes-votre-aide
tags: ['organisations','organisation','management','tribu','horde']
title: 'Organisations vivantes : votre aide'
---

Suite à une discussion tonitrutante avec des copains à une conférence
récemment, nous avons décidé avec Bruno\_de creuser l'idée d'une
conférence, d'un propos, sur ce que nous aimerions que soit la culture
de nos entreprises, et notamment de ses dirigeants car ils en sont le
plus gros levier. [Bruno](https://brunoboucard.com/) a commencé à
m'envoyer des liens sur des conférences et des conversations. En les
voyant j'ai encore pensé à mon serpent de mer : [Les organisations vivantes](/organisations-vivantes/).
Un texte, une consolidation d'une pensée que je pourchasse sans assez
d'effort depuis trois années. C'est autant une consolidation de mes
anciens propos ou d'idées jetées ici que certaines nouvelles choses. En
relançant cette réflexion philosophique Bruno a rendu involontairement
mais pour mon plus grand bien, ce stock, cette incapacité à finir,
insupportable. C'est pourquoi je me suis dit que je devais en appeler à
vous.

Je vais partager le brouillon de ce texte, vous allez me bottez les
fesses, me donnez votre *feedback*, ou me laissez errer dans le désert.
Mais j'aurais fait un pas de plus en ouvrant ce texte en devenir. Je
vais garder mes idées, je vais vous laisser commenter, ceux qui le
désirent. N'hésitez pas à me vexer, je n'hésiterai pas à vous dire que
je ne suis pas d'accord. Cela reste mon texte, mes idées (ou la
consolidation d'idées dans lesquelles je me retrouve, j'essaye de
toujours donner mes sources). Tout compte : les idées et les fautes, la
forme et le fond, le signifiant et le signifié, vous et moi. Je me
réserve le droit d'enlever tous les commentaires qui seraient des
insultes ou petits mots d'amour du même genre.

J'ai besoin de vos encouragements.

Je vais expliciter mon propos et donner un état de l'avancée de mes (nos
?) travaux dans une page dédiée dont vous voyez le lien dans la colonne
à gauche.

## Le texte mis à jour au fil de l'eau

Voici le lien vers le texte au format html ([Les organisations vivantes](/organisations-vivantes/)). Je vais
continuer à écrire là au fil de l'eau. Il se peut que par bloc j'ajoute ou
corrige soudainement un ensemble de choses : c'est juste que je
travaillerais en off dans le train, meilleur bureau de France de par le
ronronnement et l'isolement qu'il procure quand on ne sombre de fatigue
dans les aventures de Hannah, Reddington ou d'un autre Pablo.

Mettez votre nom ou mail dans le commentaire si vous souhaitez
apparaître dans les contributeurs ou que je puisse vous répondre plus
personnellement. 

Je viens juste de passer le texte de Latex à GoogleDoc, je vais donc
nettoyer un peu tout cette semaine et surtout la semaine prochaine, mais
n'hésitez pas à intervenir d'ici là. Pour l'instant c'est pour les plus
courageux, je vais passer quelques heures à nettoyer, arranger. Vous
noterez les chapitres ou sous-chapitres vides. Je dois faire une grosse
relecture de nettoyage rapidement.

[Les organisations vivantes](/organisations-vivantes/)

## Est-ce que je me fixe une deadline ?

Je n'ose plus, je me suis trop vu échouer à son sujet concernant [Les
organisations vivantes](/organisations-vivantes/).
J'avance à pas de géant grâce à David, Dragos, Lucie, Pauline, Rémy,
Nicolas, Quentin, Daniel, Florent, Justine, Julien, etc etc et les
autres benexters (je ne peux pas vous citer tous, mais j'ai envie) chez
[beNext](http://benextcompany.com) et cela me prend aussi beaucoup de
temps : [School Of PO](http://schoolofpo.com), [Agile
CFO](http://agilecfo.fr), *Wake Up Rh* à venir... Mes futures
conférences : celle avec [Bruno](https://brunoboucard.com/) donc, une
avec Laurent Morisseau pour [Lean Kanban
FR](http://2016.leankanban.fr/sessions/right-brain-left-brain-kanban/),
et un duo de musique à organiser en off avec Alberto (pour LKFR aussi),
[Gothamocratie](https://drive.google.com/open?id=0B779TCU1Z5goQ3RXZEdDWTdqUUU)
à continuer à répandre. Sans compter ma vie complètement fragmentée
entre Paris et Sommières (entre Nîmes et Montpellier), trouver un
appartement en urgence ce mois (ça y est c'est fait !)... Je ne fais
donc plus de promesse, à personne.
