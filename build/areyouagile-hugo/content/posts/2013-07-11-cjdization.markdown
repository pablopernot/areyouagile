---
date: 2013-07-11T00:00:00Z
slug: cjdization
tags: ['cjd','centre','jeunes','dirigeants']
title: CJDization
---

Saviez vous que le mandat (2 ans) des deux co-présidents du plus vieux
syndicats d'entrepreneurs, de patrons, de France, le CJD ([Centre des
jeunes dirigeants](http://www.jeunesdirigeants.fr)) est basé sur la
notion d'agilité ? Intéressant non ? reprenons, automne 2012, [Agile
Tour Paris](/2012/11/agile-tour-paris-2012/), Le "projet dont vous êtes
le héros agile", une personne entre...en retard. Tout le monde est
chauffé à blanc par l'atelier, on demande au retardaire de payer un
tribu : des pompes ! Il s'exécute immédiatement avec le sourire aux
lèvres ! première rencontre avec un JD, le vice-président du mouvement,
Jérôme Lefèvre.

Viendra donc dans la foulée ce petit film en souvenir de cette
intervention :

<iframe src="http://player.vimeo.com/video/64552449" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

## Formateur CJD

C'est le début d'une belle rencontre. Jérôme me propose de devenir le
formateur officiel (celui qui figure au catalogue 2013/2014 pour
l'agilité : "agilité, donnez l'initiative à vos équipes"). Pour cela je
passe à mon tour une série d'épreuves (c'est de bonne guerre), et me
voici validé. Comme je suis moi-même "entrepreneur" ([petite boite de
conseil de 9 personnes](http://www.smartview.fr)), on adhère vite (mais
je n'adhère pas officiellement au CJD, je n'ai pas le droit, comme je
leur fourni une prestation de formation).

Pour les JD qui liraient, voici un *pitch* de la formation qui est
désormais dans votre catalogue :

<iframe src="https://docs.google.com/a/smartview.fr/file/d/0B3ZHNHN0HR5mb0c4dkpyajJPUW8/preview" width="500" height="281"></iframe>    

## Congrès et rencontres

Grâce à cette rencontre j'ai pu participer à plusieurs évènements : une
soirée mensuelle du côté de Béziers (30 personnes), une Rencontre du
réseau (300 personnes) pour laquelle j'ai animé un [ball point game et
donné une petite
conférence](/2013/03/le-jeu-scrum-des-balles-et-des-points/), et tout
récemment un congrès dans le magnifique palais des Papes à Avignon pour
lequel j'ai aussi animé des ateliers (150 personnes). Bref j'ai appris à
croiser ces JD (jeunes dirigeants). Ils ne sont pas du tout comme vous
imaginez les "patrons" (vieux, bedonnants, contraignants, moralisants,
etc.). C'est une bande de jeunes personnes dynamiques et intéressantes.
Majoritairement ils ont des structures assez petites, des TPE. C'est
donc souvent des gens très actifs dans la production de valeur, ou très
proche de leurs équipes. D'ailleurs maintenant je fais gaffe, les
ateliers ne se déroulent jamais vraiment comme d'habitude.

## Et donc l'agilité ?

![Caricature](/images/2013/07/cjd-2.jpg)

Et donc l'agilité est leur mot d'ordre pour les deux années en cours.
Agilité ? la "notre" ? Oui et non, et oui. Oui, ils sont tout à fait
alignés avec le mouvement de pensée qui nous représente dans les pensées
et dans les actes : lors des rencontres évoquées, un véritable
*openspace* agile s'est déroulé devant moi. Non, car ils abordent le
sujet de façon très vaste, pour englober toute leur diversité, et donc,
comme "chez nous" le mot agilité perd vite son sens. Il est tellement
facile à attraper et manipuler. Cela peut arranger tellement de
discours, se glisser dans tellement de conversation.

Mais oui à nouveau, car leur approche "ouverte" le permet directement de
s'ouvrir à des débats bien plus larges que les misérables questions sur
telle ou telle méthode, sur les guéguerres kanban/scrum, etc. On est
directement dans la sociologie, dans l'ethonologie presque, notamment
chez les intervenants de ces différentes conférences.

## Des conférences passionnantes de prospectivistes

![André-Yves Portnoff](/images/2013/07/cjd-4.jpg)

J'aurais noté deux conférenciers particulièrement intéressants lors de
ces différents évènements. D'autant que je me retrouve beaucoup dans
leurs propos. Leur premier **André-Yves Portnoff** est un petit homme
passionnant, passionné, avec une pointe d'ironie et assez de velours
dans la voix pour faire passer tous ses messages sans heurts. il prône
comme nous pouvons beaucoup le faire dans l'agilité, le patron,
l'entrepreneur humain, responsabilisant, soutenant, aidant ses équipes.
Le "servant-leadership" par excellence. Il honnie le vautour, le
dépeceur, le "courtermiste". j'ai pu l'entendre deux fois, deux fois il
abonde dans un discours proche de celui que je soutiens, en mieux, en
plus réfléchis, en plus avancé.

[Un interview de André-Yves Portnoff pour le
CJD](http://www.jeune-dirigeant.fr/011-533-1-Andre-Yves-Portnoff-Misons-sur-l-innovation-et-les-PME.html).

Un autre intervenant dont le discours a fait mouche, **Marc Halévy**. Le
point clef du discours de Marc Halévy (son site web : [Maran
Group](http://www.noetique.eu/)), c'est **la grande bifurcation**. Nous
sommes dans un grande rupture (technologique, écologique, économique,
etc.). Or on observe ce genre de grande rupture, de grande bifurcation
tous les 500 ans (l'invasion des cités grecques, la fin de l'empire
romain, la mutation féodale, la Renaissance, etc.). Et nous arrivons
dans ce moment de grande rupture, de grande bifurcation, notre "crise"
n'est que la résistance de l'ancien monde à ce passage, vive la crise.
D'une économie de masses nous sommes en train de passer à une économie
de niches, d'une économie de volumes vers une économie de marges, d'une
économie capitalistique vers une économie humanistique, d'une économie
du standard vers une économie du génie, d'une économie de productivité
vers une économie de créativité, d'une économie de taille vers une
économie de l'agilité, d'une économie de pillage vers une économie de
frugalité, d'une économie de prix vers une économie de la valeur. C'est
passionnant, et complètement dans la culture agile.

![Bifurcation](/images/2013/07/cjd-3.jpg)

En savoir plus ? je vais lire "un univers complexe" (Oxus - 2011) et
surtout son nouvel ouvrage qui doit sortir chez Dangles en octobre 2013
: "Prospective 2015-2025 - L'après-modernité" .

Bref, je suis en voie de CJDization.

