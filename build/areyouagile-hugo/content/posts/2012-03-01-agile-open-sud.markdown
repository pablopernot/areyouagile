---
date: 2012-03-01T00:00:00Z
slug: agile-open-sud
tags: ['conference','agileopensud']
title: Agile Open Sud
---

Ce que j'attends de **Agile Open Sud (Conférence ouverte sur l'agile
dans le sud)** (16-17 mars, Banuyls sur Mer)

![AOSUD](/images/2012/03/hoteldeselmes-300x199.jpg)

## D'abord

Rencontrer des copains, des amis, des connaissances, de nouvelles têtes
dans un lieux qui a l'air fort sympathique : l'hôtel des elmes (oui oui
c'est çà sur la photo). Cela sera j'espère un bon moment d'échange et de
détente. Accentué par la présence d'un ou deux matchs de rugby qui nous
permettront de nous libérer de certaines toxines et par la célébration
de la Saint Patrick (le 17 mars) qui nous permettra de récupérer ces
toxines perdues (et de souffler dans les whistles).

## Ensuite

J'espère que nos discussions, brainstorming, etc (car il s'agit d'un
openspace, donc difficile d'anticiper le programme en détails par
avance, mais pour ceux qui ne connaissent pas ce format je vous assure
que cela densifie le contenu) seront constructifs. En cela j'aimerais
beaucoup arriver à un résultat. C'est à dire ? Que l'on s'engage. "Voilà
ce que nous pensons de tel et tel sujet". Naturellement les réponses ne
seront pas monolithiques, elles exprimeront notre diversité.
Naturellement nous pouvons nous tromper sur de nombreux sujets. Mais
j'aimerais (je ne le vois pas assez dans les manifestations autour de
moi) un engagement clair sur les idées qui seront exprimées durant ces
heures. Voilà ce que nous avons dit à ce moment sur ces sujets. Sans
s'embarrasser des "qu'en dira-t-on". Je vais proposer cela aux autres
participants et essayer de trouver un format/support pour capitaliser
ces informations.

Je serai ravi de vous y croiser. Il reste des places disponibles. Cela
coûte 164,5 euros. Le prix comprends les repas du vendredi soir au
samedi midi et la chambre pour la nuit. (Il reste moins de dix places
-sur trente max quota de l'hôtel-).

**Pour s'inscrire : prendre sa carte bleue, fermer les yeux, cliquer**
[ici](https://ticketlib.com/agileopensud)  

Quelques articles sur le même sujet de
[Antoine](http://blog.crafting-labs.fr/?post/2012/01/17/Agile-Open-Sud-1617-mars-2012), de
[Thierry](http://etreagile.thierrycros.net/home/?post/2012/02/17/Agile-Open-Sud-%3A-16-et-17-mars-2012-%C3%A0-Banyuls-sur-Mer),
de [Claude](http://www.aubryconseil.com/post/Agile-Open-Sud-a-Banyuls),
[Fabrice](http://agilarium.blogspot.com/2012/01/agile-open-sud-les-16-et-17-mars-2012.html).

Le tag sur tweeter est \#aosud.
