---
date: 2012-07-29T00:00:00Z
slug: les-gens-sont-frustres
tags: ['frustration']
title: Les gens sont frustrés
---

Lors des soirées de discussions agiles non formalisées, ou même lors de
séances plus formalisées, les gens sont souvent frustrés de ne pas
repartir avec des réponses claires.

Dernièrement encore, lors du bilan d'une séance réussie on interroge
plusieurs personnes. Sans surprise l'une d'elle se dit déçue de ne pas
avoir eu de réponses claires et de ne pas être sûre de a) repartir avec
plus de questions que de réponses, b) l'intérêt dès lors de tel
évènement.

## Réponse n'est pas solution

Malheureusement vous ne pourrez pas avoir de réponses claires à vos
problématiques car le contexte de vos questions est un élément trop
important pour que nous puissions y répondre de façon définitive.

Malheureusement vous ne pourrez pas avoir de réponses claires à vos
problématiques car il n'y a pas de réponses définitives mais un parcours
à mettre en oeuvre, à initier.

Malheureusement vous ne pourrez pas avoir de réponses claires à vos
problématiques car celles-ci n'existent pas encore, elles doivent
émerger de votre contexte (lire
[cynefin](http://en.wikipedia.org/wiki/Cynefin) !)

## Pas de certitudes

Nous ne travaillons pas dans un univers de [sciences
dures](http://fr.wikipedia.org/wiki/Sciences_dures) mais bien
[molles](http://fr.wikipedia.org/wiki/Sciences_molles). La complexité
des éléments qui se mélangent dans nos projets nous interdit d'avoir des
certitudes. Mener un projet dans nos domaines est subtil, c'est délicat.

## Pas de réponse, de l'observation

Ne venez donc pas chercher de réponses précises. Venez chercher des
façons de faire, des façons pour aborder une réponse, une expérience,
etc .  (C'est ça que je vends, que nous vendons -consultants ou coachs
selon votre propre bestiaire-).

Et prenez le temps de l'observation. On cherche trop rapidement des
réponses. Lors d'une transition à l'agilité par exemple, il faudra
laisser filer quelques mois avant d'y voir clair. Ou plutôt quelques
itérations. Il faut compter autant la fréquence du feedback que le temps
passé.

On ne peut commencer à imaginer des réponses que lorsque l'on est
immergé dans le contexte. Et réponse n'est toujours pas solution.

## Ma frustration

Enfin un dernier mot sur **ma** frustration. Quand vous posez une
question, admettez la réponse que l'on vous donne. Inutile de poser une
question si vous jugez non recevable par avance certaines réponses.

Rappelez vous que nos réponses n'ont pas forcément de sens dans vos
contextes, c'est aussi pour cela qu'elles peuvent paraître parfois
déconnectées avec votre réalité.
