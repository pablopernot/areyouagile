﻿---
date: 2018-11-04
slug: livrer-pour-apprendre-et-experimenter-ou-livrer-pour-finir
tags: [livrer, apprendre, experimentation, hypothese]
title: Livrer pour apprendre et expérimenter ou livrer pour finir ? 
--- 

Dans une conversation sur Linkedin une phrase se voulant "choc" est délivrée : "Si t'as l'impression de ne pas avoir fait de ton mieux, ne livre pas."

À mes yeux en l'état, cette phrase ne veut rien dire. Tout va dépendre du contexte et des sous-entendus. Pour souligner cela j'y réponds : "Si tu ne te sens pas mal à l'aise avec ce que tu livres, c'est que tu livres trop tard" dixit le patron de Linkedin, je ne sais plus quand je ne sais plus où. J'ajoute "ça serait dommage de ne pas livrer et de rater un apprentissage. Le mieux est l'ennemi du bien, etc. Faire de son mieux sonne trop perfection et la perfection est une mauvaise conseillère ces jours-ci". Cette fameuse phrase citée approximativement ne dira rien non plus si elle n'a pas plus de contexte. 

S'ensuit donc une longue conversation pas forcément utile, car justement chacun patauge dans son contexte. 

Mais le soir même vers 21h je suis interpellé sur notre groupe de communication interne (slack) par une collègue sur ce dialogue. 

*Elle :* 

*Tu le penses vraiment (on évoque : "Si tu ne te sens pas mal à l'aise avec ce que tu livres, c'est que tu livres trop tard") ? Est-ce que ce n’est pas un risque au contraire de finir par s’habituer à la médiocrité ?*

Moi : 

Rien à voir avec la médiocrité.  
Le gars (de Linkedin) il test ses hypothèses. 85% de ce qu'il a livré a été retiré. (85% des fonctionnalités de Linkedin on dit-on été retiré[^1])  
Et il ne parle surtout pas de qualité. Il parle du périmètre du produit.  
La qualité n'est pas remise en question.  

[^1]: Je ne sais plus non plus d'où je tire ce chiffre, mais mon intuition me dit que ma source était assez fiable. 

*Elle :*

*Hmmm je suis fatiguée j’ai peut être mal compris... mais moi quand je lis ça j’ai l’impression que ça justifie les trois quarts des produits pourris "sorry on a fait au mieux, on n’avait pas le temps de se pencher sur ce qui crée de la valeur au risque de perdre l’utilisateur".*

Moi : 

C'est dur pour toi, je le comprends. Car tu le lis sous le terme "perfection".  
Tant pis (pour la conversation Linkedin) je laisse les gens croire ce qu'ils veulent.

*Elle :* 

*Oui je sais pas, mais j’ai l’impression quotidienne d’entendre TOUTES mes équipes me dire "euh désolé, mais on n’avait pas le temps de faire propre, l’expérience utilisateur n’est pas la cible, mais on se rattrape au prochain sprint", 6 mois plus tard on est pas au rendez-vous et on cumule les dettes.*


Moi : 

La conversation que tu décris ne parle pas du fond.  
Qu'est-ce que l'on veut ?  
Quelle valeur ?  
Surtout pas de la qualité ou pas de l'expérience utilisateur ou du code. Quelle vraie valeur produit ?  
Après les questions et la compréhension de ces choses se lisent bien mieux. À mes yeux.  
Rien que dire "6 mois plus tard" c'est que l'on ne répond pas à la vraie question.  


*Elle :* 

*Et moi franchement je suis épuisée parce que je bosse comme une malade et même pas 5% de mon taff est finalement bien mis en prod, j’en suis à un stade où je livre des parcours ciblés ET les dégradés tout le temps “qu’on ne sait pas faire” alors que si on voulait bien faire on pourrait.*

Moi : 

Faut livrer beaucoup plus en production et mesurer. Et apprendre.  
Mais souvent la politique prime sur la valeur.

*Elle :*

*Enfin je ne sais pas je suis un peu dark en ce moment je vois tout sous un spectre où les équipes sont sans passion et livrent / délivrent des trucs degeu ! Y a zéro valeur.*

Moi : 

Tu ne sais pas.  
Vous ne livrez pas.      
Je provoque, mais c'est l'idée.  
Si tu me livres des trucs pourris en production    
Et que tu mesures vis-à-vis de la vraie valeur recherchée.  
Et bien crois-moi tu apprendras vite.  

*Elle :*

*Non je ne livre pas, mais je ne comprends pas comment, pourquoi on ne livre pas de la valeur à l’utilisateur
franchement ? J’ai l’impression d’être un moins bon "designer" qu’il y a un an et ça me coûte.*

Moi : 

Un "designer" c'est comme un codeur. C'est les sciences molles qui feront souvent -- pas toujours -- la différence.

*Elle :* 

*Je préférerais qu’on livre plus tard, mais bien, quitte à effectivement livrer des choses en préproduction, tester apprendre, mais qu’en front on soit parfait lorsqu’on livrera sauf que pour tout le monde livrer c’est en front[^front]. Et ça ça me coûte beaucoup après on nous rappelle “pour éteindre le feu”*. 

[^front]: en "front", devant, l'interface utilisateur, la partie visible. Au contraire du "back", à l'arrière, ce qui travaille en fond, de façon non visible.   

Moi : 

Je préférerais que vous livriez beaucoup moins.  
Mais bien.  
Et que vous appreniez et mesuriez.  
Donc j'aimerais que vous livriez le plus tôt possible.  
Donc la plus petite chose de valeur.   
Rien à voir avec bien ou mal de bonne ou mauvaise qualité.  
Ce n'est jamais le débat.  

Et tu écris : "en front on soit parfait", *wrong*.  
Ce n'est jamais parfait avant d'avoir été mis en production.

*Elle :* 

*Oui, mais ça c’est aussi mon problème je livre la cible (ça fait beaucoup trop) après il faudrait qu’ont lotissent avec tout le monde. Sauf que ce n’est jamais suivi jusqu’au bout! “Ils oublient entre temps les parcours cibles”*

Moi : 

Non pas lotir.  
Étoffer. Étoffer. Étoffer.  

*Elle :*

*Ce n’est jamais parfait moi j’ai du mal avec le pas parfait ahah*

Moi : 

Drogue-toi :)  
Ce que tu vas penser parfait ne le sera pas pour un autre. On fait comment alors ?  
Il n'y a pas de parfait partagé.  

*Elle :*

*Enfin je n’en sais rien, je sais bien que le problème doit aussi venir de moi, mais là je sais plus comment œuvrer pour que l’on aille vers le pas toujours “OK ça passe”, mais vers le “les mecs là on donne tout”.*

Moi :

On donne tout sonne "à fond".   
Tu n'as pas de qualité en allant à fond.  
Le problème n'est pas dans le rythme, mais dans le périmètre.  

*Elle :*

*Pourquoi pas de qualité si on prend le temps, mais avec une vision plus long terme ? Là on patch, on patch, on patch et on se retrouve avec trop de patch qui rendent l’évolution impossible.*

Moi : 

Faire à son rythme pour de la qualité.  
Mais plus tu sors des choses complètes plus tu risques de te planter.  
Car trop d'hypothèses. Pas assez d'apprentissages.  

*Elle :* 

*Mais avant j’ai fait des tests utilisateurs, j’ai confirmé mes intuitions et je les ai fait évoluer !*

Moi :

Pourquoi pas prendre son temps.  
Mais travailler sur des petits bouts.  
Et observer apprendre en production.      
Ça bouge et ça reste du travail en chambre.      
Et si tout reste OK plus tu attends moins cela le sera.   
Donc, sortir des petits morceaux rapidement.  
Rapidement c'est différent de "aller vite".   
Plus des petits bouts.  

*Elle :* 

*Je ne sais pas, je n’en sais plus rien... je n’arrive pas à me contenter de ce qu’on livre ! Livrer un petit bout de parcours d’inscription ce n’est pas possible.*  

Moi : 

Ça ne doit pas se limiter qu'au boulot cette bataille ?  
Bataille pour la perfection.

*Elle :* 

*Une vie sans recherche constante d’élévation vaut le coût? Je n’en suis pas sûre.  
Enfin OK pour les autres, chacun deal avec soi-même, mais pas pour moi* 

Moi : 

Recherche constante d'élévation n'a rien à voir avec la perfection.  
Celui qui recherche constamment à s'élever sait par définition qu'il n'y a pas de perfection.  

*Elle :* 

*Hmm je ne sais pas la perfection pour moi c’est d’être capable de sublimer tout et d’être au plus proche du vrai.    
S’élever c’est vouloir être au plus proche du vrai pour ne pas subir.*

Moi :

Et ça change ça bouge ce vrai ?  
Ça évolue ?  
Et quand tu fais cette recherche d'élévation. Tu fais un gros morceau annuel ? Ou c'est de constants petits ajustements qui s'enrichissent les uns des autres ?  

*Elle :* 

*En tout cas je sais où je vais.  
Ou du moins où je veux aller.*

Moi : 

Oui un sens, une direction, mais pas les détails.  
Un pourquoi une direction.  
Mais pas les détails.  
Pour les produits c'est pareil.  
Ou tu as les détails de ta réflexion du moment.  

*Elle :* 

*Mais si un moment on ne met pas les mains dans les détails ça reste de l’"idéation"*

Moi : 

Et la suivante s'enrichira de la précédente. Et alors tu penseras à ces détails.  
Tu ne prépares pas les détails de ton introspection dans 3 mois...  

*Elle :*

*Non bien sûr c’est le recul qui donne du sens à ce que l’on a vécu et donc aux détails.  
Mais y aussi les moyens que l’on se donne pour y arriver et ça passe dans les détails sinon on va vers le phare et on risque de déchirer sa coque.   
Bref je suis pas être vive ce soir, mais juste que je suis fatiguée “des OK, on ne sait pas faire, donc on se contente de ça » "*

Moi: 

Oui les détails, mais pas de ce qui vient plus tard.


Pour conclure. 

Il y a plusieurs débats de fond. Je note ces quatre-là :   

* Délivrer pour expérimenter et apprendre plutôt que délivrer pour finir. 
* La qualité vient de la régularité.  
* On ne va pas plus vite, on délivre des plus petits morceaux, pour lesquels on essayera de densifier la valeur suite aux apprentissages. 
* L'introspection chez chacun est essentielle. 

