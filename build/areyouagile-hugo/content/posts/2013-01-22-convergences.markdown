---
date: 2013-01-22T00:00:00Z
slug: le-mystere-picasso
tags: ['convergences']
title: Convergenc.es
---

Convergenc.es
=============

date

:   2013-01-09 11:30

tags

:   oana

![La banane](/theme/images/2013/01/banane.jpg){.image .left width="150"}

[Convergenc.es](http://convergenc.es) est le fruit d'une nouvelle
collaboration entre [SmartView](http://www.smartview.fr) (Pablo &
Jérôme) et Oana Juncu, qui vient de lancer
[Co-Emerge](http://leanovation.org). En tous cas c'est son premier
*lineup* (on fait comme les groupes de rock).

Comme décrit [ici](http://convergenc.es/pages/proposition.html)
l'objectif de [Convergenc.es](http://convergenc.es) est de regrouper une
petite (humaine, raisonnable, agréable) communauté d'agilistes pour
proposer des offres complémentaires et cohérentes. De nos jours où le
mot agile de par son succès à tendance à s'étioler il est aussi très
important de se mettre d'accord sur sa signification. Notre regroupement
propose une convergence vers l'idée que nous nous faisons de notre
métier et des
[principes](http://convergenc.es/pages/pourquoi-ensemble.html) qui nous
accompagnent.

Nous souhaitons profiter de cette dynamique de groupe pour offrir des
[formations en
binôme](http://convergenc.es/pages/formations-en-binome.html) ou des
[sessions communes](http://convergenc.es/pages/evenements.html).

Nous estimons que la mise en commun d'une expertise, le partage
d'expérience, une direction commune, valent plus que la concurrence qui
pourrait nous opposer. Sans aucun doute la richesse de cette
collaboration compensera largement une illusoire rétention
d'information.

Au plaisir de vous en parler de vive voix. La [liste de nos
formations](http://convergenc.es/pages/calendrier-des-formations.html) à
venir. (si ça ce n'est pas la promo je ne sais pas ce que c'est).

*ci-dessus : illustration du* [management par la
banane](http://convergenc.es/pages/pourquoi-ensemble.html) *plutôt que
par la carotte par Andy Warhol*

*ci-dessous : une magnifique cover du Velvet Underground (jamais trop ma
tasse de thé mais celle-ci quel plaisir, et l'interprétation des Black
Crowes est extra).*

<iframe width="560" height="315" src="http://www.youtube.com/embed/lf9-BCix4io" frameborder="0" allowfullscreen></iframe>

