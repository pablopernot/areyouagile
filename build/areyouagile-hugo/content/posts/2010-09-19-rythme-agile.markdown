---
date: 2010-09-19T00:00:00Z
slug: rythme-agile
tags: ['cadence','rythme']
title: Rythme agile
---

Dans ce petit post je voudrais mettre en évidence un élément fondamental
de la démarche agile : la notion de rythme. Une itération c'est comme
une portée en musique : elle possède sa vélocité (4/4 par exemple : soit
4 noires dans une portée, ou 8 croches c'est pareil !, enfin vous voyez
clairement l'analogie\*). D'ailleurs si Ken Schwaber compare souvent
Scrum & échec : règle simple, stratégie et jeu complexe ;  on peut faire
de même avec la musique : 7 notes : des possibilités sans fin.

Bref il s'agit d'avoir une cadence. Au sein de celle-ci deux éléments
importants : une certaine anticipation, une livraison effective. Je
traduis là deux concepts mis en évidence par Mary & Tom Poppendieck et
propre à l'agilité : "délivrer aussi vite que possible", et "décider
aussi tard que possible". Il faut savoir jongler pour éviter le trop tôt
ou le trop tard (dans vos livrables, dans votre expression du besoin,
etc.). Si vous n'êtes pas dans le rythme vous dégradez la qualité ou
vous fournissez trop d'effort pour peu de résultat (ce que j'appellerai
sur-qualité). Toute la gamme d’artéfacts proposée par l'agilité est là
pour éviter cela : sprint, timeboxes, dailyscrum, user stories, etc.

Dans le schéma ci-dessous (cliquez 2 fois dessus pour le voir en
grand...) j'essaye de mettre en évidence de façon macro les effets
négatifs dans le cycle du projet. En rouge ce qui génère le plus de
problèmes, en vert ce qui propose le plus de valeur, d'efficacité. Toute
la difficulté résidant à viser les zones vertes sans tomber dans les
zones rouges adjacentes. C'est souvent mal perçu mais je tiens à le
préciser : l'expérience y joue un facteur clef, un novice pouvant jouer
sur toute la longueur de la barre...

![Rythme](/images/2010/09/rythme_agile.png)

\* en fait je vois tellement d'analogies entre musique et scrum (ou plus
généralement l'agilité) que je vais y dédier un post...

\*\* je vous recommande encore, toujours, l'excellent "[Lean Software Developpement, an agile toolkit](http://www.poppendieck.com/ld.htm)"
