---
date: 2017-11-02T00:00:00Z
slug: agilite-a-grande-echelle-synchronisation
tags: ['agilite','echelle','scaling','synchronisation','storytelling']
title: 'Agilité à grande échelle : synchronisation'
---

Depuis plusieurs années l'agilité -- ou une certaine agilité -- a gagné l'entreprise à grande échelle. Son succès l'a poussé à s'interroger sur comment répondre au delà de l'équipe. 

## D'abord l'équipe

Car l'équipe est le composant essentiel d'une organisation agile. C'est l'ensemble des équipes qui composent l'organisation. On peut les regrouper par niveaux intermédiaires (produit, programme, etc.). Quand on parle d'agile à l'échelle c'est pour évoquer cette dynamique multi-équipes. On a recherché dans une équipe agile : autonomie et implication sur un périmètre. On a donc revu nos périmètres de façon "verticale", avec des équipes pluridisciplinaires. L'intérêt étant non seulement d'impliquer en contrôlant son outil de travail de bout en bout, une réelle autonomie, mais aussi de délivrer des choses finies (toujours de bout en bout), et ainsi de bénéficier d'un engagement et d'un retour rapide sur investissement. Tout cela a inversé la structure classique des années précédentes en cassant les silos "horizontaux".  

## Puis une synchronisation inter-équipes

La synchronisation entre les silos sans rien de fini et sans autonomie était un vrai casse-tête (cela l'est toujours dans de nombreux endroits), et un système renforçant les animosités inter-silos. En inversant cette matrice le dialogue a été relancé (en plus de l'engagement et du retour rapide sur investissement). Mais avec les programmes ou les grands projets à plusieurs équipes, une synchronisation redevient nécessaire, on a simplement changé l'axe de la matrice. Au lieu de devoir agréger des morceaux de différents silos pour voir si le tout se met à fonctionner (avec la prise de risque que l'on connaît et le manque de réactivité -- il faut tout pour assembler --), on doit désormais agréger les morceaux des différentes entités pour voir si ils font sens ensemble et si ils continuent de fonctionner ensemble. On doit le faire **en amont pour garder du sens de la valeur** (est-ce que mon tout fait sens, a de la valeur), et on doit le faire **en aval pour s'assurer que ce qui est fini est bien fini** dans le tout. Enfin **on doit le faire régulièrement** pour avoir un sentiment de progrès (et donc renforcer l'engagement) et toujours bénéficier d'un retour rapide sur investissement.    

Dans des équipes agiles cela se déroule donc généralement à deux moments : lors du planning, lors de la revue (ce que certains appellent la démo). Et cela se consolide dans des portfolios projets/produits. 

### Du planning (quarterly planning, pi planning, etc.)

Généralement sur un cycle on déclenche ce que certains appellent désormais "quarterly planning", ou d'autres "pi planning"(mon prochain article : "pourquoi se méfier de Safe" sans point d'interrogation), etc. Le cycle est généralement de deux mois ou trois mois. L'idée est de synchroniser les équipes qui travaillent sur un même "macro-scope". Synchroniser des équipes au démarrage d'un cycle ? C'est à dire : 
 
 * les co-localiser. Équipes de 7-8 maximum et maximum de 6-7 équipes, par expérience (des gens vont bien au delà). On découpe par groupe de cinquante personnes si il faut aller au delà, et cela durant une ou deux journées.
 * s'assurer d'une vision commune (et pourquoi pas la présenter au travers d'un [impact mapping](/2017/02/cartographie-strategie-impact-mapping/))
 * éviter le maximum de dépendances pour garder un sentiment de contrôle et d'autonomie
 * valider l'harmonisation des plan d'actions, pour cela j'essaye d'utiliser du management visuel au travers de [user story mapping](/2017/01/cartographie-plan-action/).

 Il est indispensable que **le storytelling l'emporte sur de l'imbrication sans sens. Chaque histoire s'inscrit dans la grande histoire**. 

### Le début d'un cycle c'est aussi l'occasion de renforcer la culture agile 

Idéalement cette synchronisation ne se déroule pas que sur le produit mais aussi sur l'intention et l'attitude, sur la culture du groupe dans son ensemble. 

#### État des lieux de la culture 

On peut s'interroger sur le respect de la culture agile ou des valeurs que l'on souhaite porter au début de chaque cycle. Comme par exemple avec le [healthcheck de Spotify](https://labs.spotify.com/2014/09/16/squad-health-check-model/) 

![Healthcheck](/images/2017/11/healthcheck.jpg)

ou sur une araignée. 

![Spider](/images/2017/11/spider.jpg)

#### Proposition d'expérimentations

Renforcer la culture agile c'est aussi renforcer l'idée de pouvoir expérimenter, l'idée de l'amélioration continue au travers d'expérimentations. Qu'allons nous expérimenter durant ce cycle ? Où faut-il avancer ? L'expérimentation est au cœur d'une conduite du changement réussie. Ainsi donc **la liste des expérimentations du cycle est une bonne chose à définir**.  

### Du produit (démos, revues)

À la fin de ce cycle, on s'assure que ce que l'on a jugé fini au fil de l'eau est bien fini et que le regard que l'on porte sur la grande histoire fait toujours sens. Car il est -- très -- souhaitable que l'on ait pu valider au fur et à mesure que l'on véritablement quelque chose de fini dans la globalité (problématique du "commit" en environnement informatique sur un "tronc" ou sur une "branche" par exemple ; problématique en informatique des plate-formes de validation, de pré-production qui intègrent au fil de l'eau les ajouts finis de chaque équipe).   

Cette cohérence globale est réalisée durant les revues (aussi appelées les "démos"). Toutes les équipes se réunissent et montrent leur histoire au milieu de la **grande histoire qui se dessine au travers des petites histoires**.  

Ma préférence va pour que cette synchronisation lors des revues n'existent qu'en fin de cycle (mais pas trop tard !). J'apprécie que les équipes portent leur produit de façon visible, et donc de façon indépendante durant le cycle (revue dédiée à chaque équipe). Cet effet de masse des revues multi-équipes tend à effacer l'identité de l'équipe. D'autre part elle allonge considérablement la durée des revues et ainsi des personnes clefs s'y montrent moins fréquemment qu'elles ne le devraient. 

## Portfolio projets

Cette agrégation multi-équipes se lit au travers d'un portfolio projets / produits : idéalement sous la forme d'un [kanban dédié à cela](/2016/01/portfolio-projets-kanban-partie-1/). Cette agrégation se lit aussi au travers d'un plan de release consolidé (comme si vous agrégiez 4 ou 5 de [ce type de plan de release](/images/2016/05/release-plan-large.jpg))


## Une petite série sur mise à l’échelle (et auto-organisation).

* [Agile à l'échelle : c'est clair comme du cristal, 2013](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)
* [Agile à l'échelle : synchronisation, 2017](/2017/11/agilite-a-grande-echelle-synchronisation/)
* [Agile à l'échelle : pourquoi il faut se méfier de SAFe, 2017](/2017/11/pourquoi-faut-il-se-mefier-de-safe/)
* [Agile à l'échelle : libération de l'auto-organisation, 2017](/2017/12/agile-a-l-echelle-liberation-auto-organisation/) 
* [Agile à l'échelle : équipes et management, 2017](/2017/12/agile-a-l-echelle-equipes-et-management/)
* [Agile à l'échelle : grandir ou massifier](/2018/02/grandir-ou-massifier/)
* [Agile à l'échelle : Podcast "Café Craft" - "L'agilité à l'échelle"](http://www.cafe-craft.fr/20)

Et sinon l'événement que nous organisons en autour de l'agile à l'échelle avec [Dragos Dreptate](http://andwhatif.fr): 

* [VALUE DRIVEN SCALING](http://valuedrivenscaling.com)





 