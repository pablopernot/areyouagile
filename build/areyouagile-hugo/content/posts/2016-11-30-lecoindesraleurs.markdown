---
date: 2016-11-30T00:00:00Z
slug: le-coin-des-raleurs
tags: ['enfant','mathieu','stage']
title: Le coin des râleurs
---

Voici les quelques mots que j'ai arraché à mon fils concernant sa
semaine à Paris avec moi (pour son stage de 3ème). Après sa
[soeur](/2014/12/sarah-journal-de-bord-jours-1-et-2/)...

Pourquoi le coin des râleurs ? Car quand je présente Mathieu, avec sa
mine renfrognée, je dis bêtement (je ne devrais pas le faire) "mon ado
râleur", j'ai été surpris d'entendre bien souvent : "ah ben il va bien
s'intégrer dans la communauté des coachs agiles".

NdP == note du père.

## Journée \#1

![Mathieu](/images/2016/12/mathieu1b.jpg)

Où : Train et [beNext](http://benextcompany.com) - 12 rue Vivienne

Nous sommes partis avec mon père à 7h30 et nous sommes arrivés à la gare
de Nîmes, à 8h15, pour 3h de train vers Paris. J'étais impatient de
cette semaine. À Paris on passe à l'appartement, au 7ème étage ! Mon
père m'a fait marcher plus de 10000 pas. Il m'amène dans son entreprise,
[beNext](http://benextcompany.com). Je ne pensais pas du tout que
c'était dans un immeuble d'habitations. Chez
[beNext](http://benextcompany.com) ils vont me faire faire un jeu où je
dois trouver qui fait quoi sans poser de questions, juste en écoutant.
J'ai découvert plein de métiers d'une startup : HR Manager qui consiste
à voir le personnel de l'entreprise ; l'Office Manager qui s'occupe des
finances (en fait en discutant avec mon père et
[Dragos](http://andwhatif.fr) cela concerne tout l'administratif); le
coach qui propose des idées pour travailler plus facilement ; le CEO,
c'est le patron.

Dans les locaux de [beNext](http://benextcompany.com).

## Journée \#2

![Lean Kanban FR avec Laurent](/images/2016/12/mathieu2b.jpg)

Où : [Lean Kanban France](http://leankanban.fr/) - 13ème arrondissement

Je suis mon père pour découvrir son métier : coach, conseiller
d'entreprises. Il m'amène dans une conférence, j'ai vu mon père en
action ! Je retiens qu'il joue un peu trop. Il rigole plus qu'il ne
travaille. Il a présenté comment faire un tableau pour s'organiser
lorsque l'on travaille. Après le repas j'ai été voir une conférence sur
la facilitation graphique. La facilitation graphique est un métiers où
l'on utilise une grand feuille pour dessiner ce qu'une personne dit,
pour résumer une conférence par exemple.

Ici à préparer l'atelier avec
[Laurent](http://www.morisseauconsulting.com/).

## Journée \#3

![Lean Kanban FR](/images/2016/12/mathieu2a.jpg)

Où : [Lean Kanban France](http://leankanban.fr/) puis tour Eiffel -
Opéra à pieds

On part pour le deuxième jour de conférence. J'écoute une conférence en
anglais mais je n'ai pas compris. J'ai vu qu'il faisait une frise
chronologique. Avec des équipes de cinq ou six personnes qui devaient
décrire ce que l'on faisait avant et après le moment donné par le
responsable (NdP: de l'atelier ?, atelier *Event Storming* de [Alberto
Brandolini](http://ziobrando.blogspot.fr/)). Le midi nous sommes allés
dans un restaurant du sushis puis nous sommes revenus pour écouter
d'autres conférences. Le soir j'ai visité Paris, nous sommes allés voir
la tour Eiffel, elle était illuminée, on aurait cru qu'elle était en or.
Je suis également allé sur la place Vendôme et la rue de la Paix.
C'était une belle journée.

Ici avec la mascotte de LKFR

## Sur les personnes

![avec Dragos](/images/2016/12/mathieu3b.jpg)

J'ai rencontré plein de personnes cette semaine, il y avait David le CEO
de [beNext](http://benextcompany.com), il est sympathique, drôle. Il y a
Justine l'*Office Manager* elle est gentille, il y a Florian le
développeur web il est sympa. En fait tout le monde est sympathique.
Quentin le *biz dev* il est drôle et fait un peu l'idiot, *biz dev* il y
a aussi Daniel il peut être sérieux comme l'inverse. Amandine est
responsable des communications, elle est gentille également. Il y a
Pauline la HR manager qui est très travailleuse, Maja HR aussi qui aime
faire des gâteaux, Marie aussi chargée RH, [Dragos](http://andwhatif.fr)
le coach, un des meilleurs amis de mon père.

Ici avec [Dragos](http://andwhatif.fr).

## Journée \#4

![Gaz B Gaz H](/images/2016/12/mathieu4b.jpg)

Où : Matinée à la Défense, après-midi à Pantin.

Aujourd'hui nous sommes allés à la Défense, on a pris deux métros
pendant 30min chacun, puis mon père m'a amené dans un immeuble de 31
étages ! Je suis allé au 26ème étage, j'en ai profité pour faire ce
rapport de stage. J'ai assisté à deux réunions qui portaient toutes deux
sur le fonctionnement de l'entreprise, qui est repartie en "clans". Les
gens votaient sur des points à améliorer et les tensions à changer dans
l'entreprise. On pose des questions sur les points sur des tensions, on
évalue les points à améliorer (NdP : réunion de triage / holacratie /
état de santé des équipes).

L'après-midi nous sommes allés dans un atelier chez \*\*\*\* pour
émettre des hypothèses pour savoir comment ils vont changer le gaz (NdP:
du gaz B au gaz H, *Event Storming* et *User Story Mapping*).

Quelques mots clefs : frise chronologique, projet, hypothèse, squelette.

À gauche atelier frise

## Journée \#5

![Mathieu et le train](/images/2016/12/mathieu1a.jpg)

Où : Matinée à la Défense, train

Le matin on a fait une réunion pour briefer une entreprise et après on
est rentré en train, chez nous, dans le sud.