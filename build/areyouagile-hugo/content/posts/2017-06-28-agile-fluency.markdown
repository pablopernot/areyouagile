---
date: 2017-06-28T00:00:00Z
slug: agile-fluency-session-atelier
tags: ['fluency','agilefluency']
title: 'Agile Fluency : session et mini atelier'
---

Ci-joint les slides de ma session sur Agile Fluency à Agile France. Dans
l'idée c'est 20/25 mn de présentation sur Agile Fluency (je ne pense pas
que plus soit nécessaire), et un petit atelier d'appropriation qui se
base une idée de Crisp et les pratiques de Spotify.

Merci encore à [Claude](http://www.aubryconseil.com/) qui il y a trois
ans m'a mis le nez dans ce petit texte (mais qui devient grand) sur une
façon de percevoir un cheminement agile.

Cela demande des commentaires et d'entendre la session et j'espère
pouvoir la rejouer ailleurs.

## La liste des liens

-   <https://martinfowler.com/articles/agileFluency.html>
-   <https://www.youtube.com/watch?v=OlPP3kQmpV8>
-   <http://www.agilefluency.org/>
-   <https://www.areyouagile.com/2017/04/perspective-agile-fluency/>
-   <https://www.areyouagile.com/2017/04/conversation-agile-fluency/>
-   <https://www.areyouagile.com/2016/03/were-gonna-groove/>
-   <https://www.areyouagile.com/2015/06/chemin-dune-transformation-agile/>
-   <https://blog.crisp.se/2015/12/15/peterantman/fluent-at-agile-visualizing-your-way-of-working>
-   <http://openspaceagility.com/>
-   <https://www.areyouagile.com/2014/04/histoires-dopen-agile-adoption/>
-   <https://www.areyouagile.com/2015/11/darefest-2015/>
-   <https://www.youtube.com/watch?v=uc2pdN9HvZQ>

## Les slides

<iframe src="//www.slideshare.net/slideshow/embed_code/key/3YYBP8FqRzE6pz" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/pablopernot/perspective-plutt-que-framework-agile-fluency" title="Perspective plutôt que framework : Agile Fluency" target="_blank">Perspective plutôt que framework : Agile Fluency</a> </strong> from <strong><a target="_blank" href="https://www.slideshare.net/pablopernot">Pablo Pernot</a></strong> </div>
