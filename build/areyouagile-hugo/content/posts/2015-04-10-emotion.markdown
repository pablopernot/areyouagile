---
date: 2015-04-10T00:00:00Z
slug: emotion-energie-et-conduite-du-changement
tags: ['emotion','energie']
title: Émotion, énergie et conduite du changement
---

Et si la conduite du changement, comme la clef de nos organisations,
était une question d'**énergie**, autant sur le plan structurel que le
sur le plan personnel ? Une question de **déplacement d'énergie**, de
flux.

Les termites construisent un substitut de leur corps (une extension
physiologique pour paraphraser [J.Scott
Turner](https://en.wikipedia.org/wiki/J._Scott_Turner) - *The extended
organism*) pour économiser ou stocker l'énergie, le cristal est
périodique : il répète le même motif pour économiser de l'énergie, les
corps et les organisations ont des facultés de résilience, ou
d'homéostasie, de régulation d'énergie ; l'afflux d'une certaine énergie
provoque des "catastrophes" pour reprendre le langage de [René
Thom](https://fr.wikipedia.org/wiki/Ren%C3%A9_Thom) (théorie des
catastrophes) : rupture, déchirement, courbe, renversement, c'est encore
des flux d'énergie. Mais rappelez vous de vos grands changements
personnels, de vos ruptures, l'avant, l'après, de vos grandes joies ou
révélations, épiphanies, elles semblent identiques : c'est un afflux
d'énergie ou un soudain vide, qui fait bifurquer vos positionnements.

## Émotion

Dans ce monde qui se rêvait cartésien pour le confort de l'esprit -- *je
pense donc je suis, ouf* -- l'irruption de l'émotion est un ennui. Elle
fait figure d'irrationnel. Mais pourquoi la nier ? Depuis 30 années elle
revient sur le devant de la scène ([psychologie
évolutionniste](http://fr.wikipedia.org/wiki/Psychologie_%C3%A9volutionniste),
*Erreur de Descartes* par [Antonio
Damasio](https://en.wikipedia.org/wiki/Descartes%27_Error), etc.).
Aucune de nos décisions importantes n'est prise sans émotion. Dans ce
monde complexe il faut rallier raisonnement cartésiens, et instinct ou
intuition, le fruit de notre vie et de celles de nos ancêtres. Il faut
allier compliqué et complexe, prévisible et émergent.

D'ailleurs personne n'en ignore la réalité quand il s'agit de faire des
grands choix de vie : choisir un compagnon/compagne, choisir sa voie,
etc.

Si donc cet aspect émotionnel, intuitif, instinctif, prend une si grande
part dans notre vie, nul doute qu'il est clef dans notre façon de
changer, d'appréhender une nouvelle façon de faire, de penser. L'image
utilisée par les [frères Heath](http://heathbrothers.com/books/switch/)
dans leur livre *Switch* est bien trouvée (et le titre évocateur) :
c'est celle du cornac, le conducteur d'éléphants indien, et de son
éléphant. Le cornac est la face cartésienne, raisonnante, du duo, mais
elle ne peut aller nulle part sans la l'adhésion de la partie plus
sauvage, instinctive, émotionnelle qu'est l'éléphant. Pour amener
l'éléphant, il faut le séduire, lui montrer des passages évidents,
rendre aussi visible les voies sans issue. Mais c'est peut-être aussi
l'instinct de l'éléphant qui lui évitera le piège du tigre.

Pour reprendre cette image, ignorer que sans l'accord de l'éléphant nous
n'irons nulle part c'est échouer d'avance. Dans toutes conduites du
changement il faut ainsi autant jouer sur la raison que sur les
émotions.

Annoncez jouer sur les émotions, vous susciterez l'effroi. Pour
certains, c'est de la manipulation dans ce qu'elle a de plus pervers.
Mais en disant cela vous confirmez l'importance de l'émotion dans nos
choix, et notre faiblesse devant sa gestion. [Frank
Taillandier](http://frank.taillandier.me/) me rappelle la *stratégie du
choc* de [Naomi Klein](https://en.wikipedia.org/wiki/Naomi_Klein) ; je
pense à la lecture de *thinking fast and slow* de [Daniel
Kahneman](https://en.wikipedia.org/wiki/Daniel_Kahneman), ou du *traité
de manipulation à l'usage des honnêtes gens* de
[J.L.Beauvois](http://traitedemanipulation.com/), qui me dépriment sur
notre asservissement aux biais cognitifs.

Je ne peux pas vous dire autre chose, il me semble que comme chaque
découverte, on peut en faire bon comme mauvais usage. Mais je ne pense
pas qu'une véritable conduite du changement personnelle ou
organisationnelle se déroulera sans émotion.

Rappelez vous vos changements de croyances, d’idéaux, de principes, ne
sont-ils pas produit avec un afflux émotionnel ? Comme une énergie
rendue disponible pour une autre voie, une catastrophe, évènement,
singularité, pour reprendre [René
Thom](https://fr.wikipedia.org/wiki/Ren%C3%A9_Thom). Comme le rire, qui
se produit, par exemple dans les sketchs absurdes des [Monty
Python](https://en.wikipedia.org/wiki/Monty_Python), quand on a
emmagasiné de l'énergie (en nous projetant, on retrouve ici encore le
*storytelling* et les neurones miroir -- lire *Social* de
[Libierman](http://www.nytimes.com/2013/11/03/books/review/social-by-matthew-d-lieberman.html)
-- ) et qu'elle débouche sur une absurdité, elle se déverse par le rire,
seul issue pour cet afflux d'énergie piégée.

Pensez aussi comme me le rappelle [Oana
Juncu](http://oanasagile.blogspot.fr), aux ruptures soudaines avec
certains de vos proches, une barrière a été franchie, un comportement
jugé hors des frontières du possible. On assèche immédiatement toutes
communications. Il n'y aura plus aucune énergie qui ira dans ce sens. Ce
changement est très rapide, très violent, très soudain, et souvent
définitif. Et pourtant on parle d'un ami proche. La rupture, le
changement, la catastrophe (Thom encore) est à la hauteur de l'énergie
présente, celle associée à un ami proche.

En écho du discours de *Switch* des frères Heath, la raison indique la
voie, mais c'est le flux d'énergie émotionnel qui va nous pousser à
avancer dans ce sens. Comme dans un jeu d'irrigation, le changement de
paradigme est comme une rivière détournée.

Il faut donc savoir, à l'instar du rire, permettre cet afflux d'émotion
et lui donner l'occasion de s'orienter différemment. Là aussi on pourra
y voir le visage d'une manipulation je ne peux rien vous dire contre
cela. Comme toujours cela dépend de l'intention que l'on met dans nos
actes. Chaque chose peut être utilisée à bon ou mauvais escient.
J'espère les utiliser à bon escient.

On pourrait prendre deux exemples : la transformation de Unilever durant
les années 90 racontée dans *To the desert and back* par [Philip
Mirvis](http://eu.wiley.com/WileyCDA/WileyTitle/productCd-0787970638.html),
et les *Open Agile Adoption* de [Dan
Mezick](http://newtechusa.net/open-agile-adoption/).

## Des évènements, singularités, à charge émotionnelle

Quand Mirvis nous conte l'histoire de la transformation d'Unilever, il
analyse *a posteriori* que celle-ci a été rendue possible par des
moments clefs qui ont permis des bascules significatives. Chacune de ces
bascules ayant bien souvent été cristallisées par des évènements hors du
commun (dans le désert, dans les Ardennes, etc.) avec des mises en
scènes artistiques, et qui apparaissaient symboliques *a posteriori*
même si elles n'étaient pas nécessairement voulues au départ.

Cristallisées ou plutôt rendues possibles par ces évènements qui ont mis
en disponibilité cette émotion propice à générer des déclics, des
épiphanies, des bascules dans les modes de pensée. Par "épiphanie" je
veux dire des sortes de révélations, de réveils, de réalisations, sans
tomber dans le mystique.

Bien souvent ces évènements ont été amplifiés par une approche
artistique, de véritables mises en scène. C'est bien le sens du mot
sensibiliser. Cela permet de mieux comprendre qu'il se déroule quelque
chose. De créer un écart, ou une aspiration pour détourner un terme du
monde de l'art.

On peut être pour ou contre, selon le **degré d'orchestration, de
préméditation, et surtout l'intention**, que l'on rapprochera ou non, de
celui de manipulation. La meilleure façon de se prémunir c'est
d'annoncer ce que l'on cherche à faire, pourquoi on le fait ainsi et de
laisser les personnes libres de participer. Comme toujours c'est
l'**intention** qui compte.

## Un espace sécurisé, bienveillant

C'est cette liberté de participer que l'on retrouve dans les *Open Agile
Adoption* proposés par Dan Mezick. Sur invitation, on va poser un cadre
propice, un espace sécurisé. Et c'est cet espace sécurisé qui pourra
permettre l'émergence de l'émotion. Elle-même étant l'énergie qui
transforme.

Quand [Olaf Lewitz](http://trustartist.com/) parle protection, de
bienveillance et de vulnérabilité il décrit typiquement ce type de cadre
: un espace protégé propice à l'émotion. Un moment sensible. Pour avoir
vécu un certains nombres d'*open agile adoption* (j'ai pu croiser et
tisser des liens d'amitié avec Dan Mezick au tout début de sa
proposition), il est surprenant d'observer comme cette émotion ne
demande qu'à émerger.

Beaucoup plus modeste, mais hors du temps, [raid
agile](http://raidagile.fr) se veut se type d'espace à charge
émotionnelle.

## Conduire le changement

Essayez d'observer et d'être attentif aux moyens d'ouvrir ces flux
d'énergie émotionnelle pour provoquer le changement.

Des lieux particuliers (raid agile dans les Cévennes, les Ardennes, le
désert, etc.), des positionnements différents, des immersions, des
abandons, etc. Des postures particulières, des discours particuliers.
Par "particuliers" j'entend "inhabituels", "déstabilisants", "protégés",
"intimes", etc.

