---
date: 2017-03-21T00:00:00Z
slug: graphes-kanban-racontes-aux-enfants-2
tags: ['enfant','enfants','kanban','cfd','controlchart','cumulativeflowdiagram']
title: Les graphes Kanban racontés aux enfants 2
---

C'est bon tu as rangé ta chambre ? Tu veux une autre histoire de graphe
? D'accord, allez va pour celle de l'**abominable carte de contrôle**.
Plus simple, plus court, tu vas vite aller te coucher. Je te l'avais
promis je te la raconte. C'est bien pour te faire plaisir pfff mais
celle là elle ne va t'emballer. Bon tu sais, ton jeu de billes, la
variation que tu appelles du "mur". Tu dois lancer les billes le plus
proche du mur mais elles ne doivent pas toucher le mur.

## Règle du jeu de billes dit du "mur"

![Range ton graphe !](/images/2017/03/billes-mur.jpg)

Tiens sur ton sac de billes y'a marqué : *Pour cette règle il faut se
placer à 3 mètres d’un mur ou d’un trait tracé au sol à la craie. Chaque
joueur va devoir lancer sa bille en direction du mur mais attention il
ne doit pas le toucher sinon c’est perdu. Le joueur qui place sa bille
le plus près du mur a gagné.*

## Tu y jours toi au "mur" ?

![Range ton graphe !](/images/2017/03/billes-mur-2.jpg)

![Range ton graphe !](/images/2017/03/billes-mur-3.jpg)

Tu y joues toi au "mur" avec tes billes à la récréation ? Oui. Bon, ça
va m'aider. Bon donc imagine plusieurs billes prêt du mur.

Et puis hop ! Comme par magie on le regarde dans un miroir. Le trait du
mur se trouve en bas. Mais c'est toujours la même chose, comme dans ton
jeu, **plus les billes sont loin du mur, moins bien c'est**. C'est pour
que tu comprennes l'impression visuelle que tu dois rechercher en
regardant ce graphe. Combien de billes ? Sont-elles près du mur ou loin
? Dernière chose : imagine que tu jettes une bille par jour et à chaque
fois tu te décales de quelques centimètres vers la droite. Tu peux ainsi
mesurer tes progrès, ou pas, jour après jour.

## Les questions que tu vas te poser

![Range ton graphe !](/images/2017/03/billes-mur-4.jpg)

Les questions que tu vas naturellement te poser : est-ce que je
progresse ? C'est à dire est-ce que mes billes se rapprochent de plus en
plus du mur ? Est-ce que je suis constant ? Est-ce que j'ai des
résultats différents selon les types de billes ? Est-ce que je peux en
déduire ma compétence moyenne ? Est-ce que je touche ma bille quoi.

## Avec les gens farfelus qui travaillent avec moi

![Range ton graphe !](/images/2017/04/cc-1.jpg)

Avec les gens farfelus qui travaillent avec moi voilà l'allure que cela
prend. Tu peux cliquer sur l'image. Et si tu veux épater tonton Julien
tu peux même utiliser le [fichier excel](/images/2017/04/kanban-graphes-enfants-2.xlsx). Je ne sais
pas comment font les autres. Mais tu fais simplement défiler sur la
ligne du bas, celle du "mur" dans mon exemple, les jours qui passent. Et
quand tu lances une bille, tu notes à combien de centimètres elle est
arrivée. Ou ici le nombre de jours qu'il a fallu à une activité pour
être complètement réalisée depuis son démarrage jusqu'à la fin. Le temps
qu'il a fallu depuis ce que tu estimes être le début (*au moment où tu
as l'idée ? Au moment où tu démarres vraiment ?* ) jusqu'à ce que tu
estimes être la fin (*au moment où les autres bénéficient de ton idée ?
Au moment où ton idée est prête ?* Intéressantes questions non ? ).

![Range ton graphe !](/images/2017/04/cc-1-1.jpg)

Tu as vu, j'ai aussi estimé dans mon exemple que trois éléments
pouvaient être lancés, ou finis, le même jour, et je me suis amusé à
dire que l'une était une bille de verre, l'autre de terre, et des
boulards, mais ils n'arrivent pas tous les jours. Bon on voit qu'il y a
moins de boulards que de billes, et que c'est plus dur : si je reprends
l'image du mur tes boulards sont plus loins, dans mes exemples en
organisations cela veut dire que l'on a mis généralement plus de temps à
faire un boulard qu'une bille en verre ou en terre. Mais tu vois aussi
que de façon assez générale les billes de verres mettent plus de temps
que les billes de terre (plus de temps à réaliser toutes les activités
du début à la fin). On voit les durées moyennes : les billes c'est entre
cinq et quinze jours. On voit les accidents : Qu'est ce qui s'est passé
avec les éléments du troisième et du quinzième jours ? Et ce boulard
perdu le septième jour ? C'est important à analyser. Il ne s'est rien
passé ces jours là précisément, mais ces éléments ont mis un temps
anormal à être réalisés et cela est visible à ce moment (dans le graphe,
avant sur le Kanban lui même). Pourquoi ?

Pareil il faudra savoir pourquoi cette bille de terre a été si rapide
(si proche du mur dans mon exemple) le onzième jour. Était-ce vraiment
une bille ? Est-ce que tu t'es rapproché involontairement ?

Si tu tiens ce genre de tableau pour tes lancés de billes, tu vas
peut-être te rendre compte que de temps en temps quand Johanna t'observe
tes lancés ratent complètement. Johanna te fait de l'effet. Il faudrait
s'améliorer car c'est justement pour le sourire de Johanna que tu fais
cela. Pour une organisation si elle sait limiter ces écarts elle saura
mieux prédire ce qu'elle est capable de faire. Là elle se dirait qu'en
moyenne une activité est réalisée de début à fini en huit à dix jours,
avec une durée minimum de cinq jours et maximum de quinze jours. Le
reste étant des accidents sur lesquels investiguer.

## Ça voudrait dire quoi progresser ?

![Range ton graphe !](/images/2017/04/cc-2.jpg)

Ça voudrait dire **éviter autant que possible les écarts inattendus et
incompréhensibles**. Cela serait bien que même en présence de Johanna tu
saches que tu vas faire une bonne bille, sans te laisser influencer.

Ça voudrait dire réduire le spectre des possibles : plutôt que d'avoir
un écart possible entre cinq et quinze jours : plutôt l'avoir entre sept
et douze jours. Pour toi c'est encore plus savoir par avance ta
compétence de lancé de billes, savoir avec plus de certitude que tu vas
pouvoir certainement battre encore Geoffrey ou William. Dans ces deux
premières améliorations : **tu te donnes plus de certitudes sur ta
capacité, moins de volatilité**.

![Range ton graphe !](/images/2017/04/cc-2-1.jpg)

Enfin naturellement c'est améliorer ta performance : c'est à dire faire
des temps plus courts, rapprocher les billes du mur.

L'idée c'est que si ton graphe a des points dans tous les sens c'est
comme un tireur à la fête foraine qui aurait troué partout sa cible :
autant de la chance que de hasard. Toutes les flèches ou les balles d'un
bon tireur seraient près du centre, regroupées. Pareil pour toi, si ton
graphe est erratique, c'est à dire avec des points un peu partout sans
cohérence, c'est que tu n'es pas vraiment discipliné dans ta maitrise de
la bille. Après la cantine, avec le sourire de Johanna, où quand Quentin
te taquine, ou encore bien concentré face à Antoine : est-ce que tu es a
peu près sûr du résultat ou cela peut-il aller dans tous les sens ?

Pour toi le graphe montre si cela va dans tous les sens ou pas, si tu
vas savoir avec assez de certitudes épater Johanna ou battre Geoffrey et
William, et si au fil du temps tu deviens plus compétent. Pour une
organisation c'est pareil : est-ce qu'elle est disciplinée ou erratique
? Est-ce qu'elle sait avec assez de certitudes le temps que va prendre
chacune de ses activités ou sa volatilité est trop grande ? Enfin si
elle s'améliore. Comment cette amélioration arrive ? En s'interrogeant
collectivement sur comment s'améliorer (au travers de *rétrospectives*
par exemple). Avec l'intégration de **limites** qui peuvent introduire
une stratégie plus claire, et qui peuvent améliorer le flux en poussant
à *arrêter de démarrer des choses, pour commencer à en finir*. Avec une
gestion en flux tiré plutôt que poussé. Tout cela est évoqué dans les
liens Kanban plus bas.

### Le rythme autour de ces graphes

Que cela soit le *Cumulative Flow Diagram* précédemment évoqué (lien
ci-dessous) ou le *Control Chart* **le mieux c'est de ne pas réfléchir**
et de commencer à stocker les chiffres au plus tôt. Comme des fleurs qui
bourgeonnent les diagrammes prendront du sens avec le temps. Comme une
pâte qui gonfle : laisser reposer un temps en stockant les chiffres.
C'est au bout d'un ou plusieurs mois que vos graphes prennent du sens.

Ça te plait que je te dise de ne pas réfléchir. Mais écoute, et je ne
vais pas te le répéter de si tôt, souvent on réfléchit bien trop avant
d'agir. Allez file jouer avant que je n'y réfléchisse.

### Sur Kanban

-   [Portfolio projets Kanban 1ère partie](/2016/01/portfolio-projets-kanban-partie-1/)
-   [Portfolio projets Kanban 2ème partie](/2016/02/portfolio-projets-kanban-partie-2/)
-   [Les graphes Kanban racontés aux enfants](/2017/03/graphes-kanban-racontes-aux-enfants/)
-   [Les graphes Kanban racontés aux enfants 2](/2017/03/graphes-kanban-racontes-aux-enfants-2/)

