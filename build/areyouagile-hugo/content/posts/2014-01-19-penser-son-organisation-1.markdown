---
date: 2014-01-19T00:00:00Z
slug: penser-son-organisation-introduction
tags: ['organisation','structure']
title: Penser son organisation - introduction
---

Voici un petit article pour commencer à en dire plus sur ma perception
d'une organisation à grande échelle selon l'expression consacrée du
moment. Aujourd'hui les questions clefs autour de l'agilité sont de deux
ordres (à mon humble avis) : a) la durabilité et l'intégrité de
l'agilité, b) la mise à l'échelle. J'avais déjà commencé à évoquer ce
point dans cet article : [agile à grande échelle c'est clair comme du
cristal](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/),
et j'y reviendrai plus bas.

## Partie 1 : introduction à ma réflexion

L'agilité est bien plus une façon d'être, une culture, que de simples
pratiques, elle n'est donc pas si simple à acquérir. Et aujourd'hui il
faut être "agile" comme les
[Shadoks](http://fr.wikipedia.org/wiki/Les_Shadoks) devaient pomper,
c'est l'effet de mode avec tous ses travers. On observe donc de
nombreuses déceptions. Pour cette raison la première des questions
autour de l'agilité est comment bien l'adopter et réussir de façon
durable ses transitions. A cette question il me semble que **Dan
Mezick** répond de façon très adéquate et je vous engage à lire son
*culture game* et ses informations sur l'**open agile adoption**. J'ai
pu l'évoquer dans cet [article](/2013/10/deux-amis/) (avec *moults*
liens).

Donc la première question est d'ordre temporel : durabilité, pérennité
de l'approche agile. Cela ne sera *pas* le sujet de cette petite série
d'articles, car, comme je viens de vous le dire, il me semble que Dan
Mezick y répond très bien.

Deuxième ordre de questions : cela marche et cela grossit. Comment gérer
? Bien souvent on part avec une ou deux équipes et le succès aidant, il
faut appréhender un passage plus global. Les vraies questions arrivent :
faut-il basculer l'intégralité de la structure dans une culture et une
organisation agile, et si oui comment ?

Ainsi la deuxième question est d'ordre spatial : comment étendre
l'approche et la culture agile à une grande organisation. Cela sera le
sujet d'une petite série d'articles auxquels je songe.

### Théorie et expérience

Pour répondre à cette problématique, deux axes : la théorie et
l'expérience. La théorie me semble toujours indispensable, notamment
quand on défriche des sujets nouveaux. L'expérience se nourrit
d'ailleurs de la théorie. Même si les intuitions peuvent être
fulgurantes, l'expérience me semble aussi importante. Le mot est à
concevoir sous deux sens : expérience *vécue*, et expérience *à
réaliser, à essayer*, celle de l'empirisme.

Côté théorie j'essayerais de vous donner toutes les sources qui ont pu
nourrir ma pensée, peut-être foireuse au demeurant sur le sujet...
Concernant l'expérience sur des questions de grandes échelles, je dirais
que j'ai pu suivre à un certains moment (suite à une *transition agile*)
une quinzaine -- voire plus -- d'équipes dîtes agiles, au sein d'une
même organisation. Je n'ai pu *que* suivre, car à un certains moment on
ne peut pas accompagner plus d'un certains nombres d'équipes de façon
sérieuse. Étant dans ce cadre le référent agile j'avais cependant une
vue assez générale, et intéressante. Une quinzaine d'équipes, et tous
les gens qui gravitaient autour, soit cent cinquante personnes. Le bon
chiffre en quelque sorte pour se projeter encore plus loin à mes yeux
(j'expliciterai ce point plus tard mais les lecteurs de ce [mini livre
sur la horde agile](/2014/01/mini-livre-la-horde-agile/) ont déjà la
réponse).

Pour l'empirisme, je vous encourage à essayer.

### Deux écoles

Aujourd'hui en réponse à cette problématique on a deux écoles qui
s'affrontent.

Ceux, dont **je ne fais pas partie**, qui pensent qu'à une certaine
échelle les anciens modes de pensée reprennent leurs droits. C'est à
dire que les aspects agiles sont consentis au niveau des équipes, des
actions opérationnelles *terrain*, en groupes resserrés, mais qu'au
niveau organisationnel, à grande échelle, l'agile n'a pas de valeur
intrinsèque. Donc il calque les modèles *matriciels*, rationalistes,
cartésiens, sur leurs organisations, au pire avec une approche silos par
compétence, et pas une approche verticale métiers.

En fait c'est juste que c'est très rassurant pour le management
classique d'opérer ainsi. Inutile de se remettre en question, les
anciennes habitudes vont se remettre à fonctionner (sur fond de
hiérarchie conservée, de blâme, d'illusion de contrôle et d'effort).
C'est donc très vendeur, c'est là dessus que se base par exemple -- à
mon avis -- le fameux *SAFe Scaled Agile Framework* (pas de lien, c'est
inutile, ça ne marche pas). Malheureusement je ne l'ai jamais vu
fonctionner, par contre je l'ai vu échouer. Quand je dis que cela ne
fonctionne pas ce n'est pas -- malheureusement -- que les entreprises
s'effondrent et disparaissent. Non elles sont justes non performantes,
lentes, moribondes, voire dévorées de l'intérieur par des luttes
intestines. Elles disparaîtront, n'en doutez pas, mais lentement, lors
d'une lente agonie. C'est tout le problème des organisations
monopolistiques : elles peuvent longtemps errer tant que leur monopole
tient, quand il s'effondre, elles s'effondrent avec. C'est aussi tout le
problème des organisations trop riches, sans souci financier, rien ne
les pousse à être performantes et donc à faire les bons choix (à ceux
qui disent : "mais elles sont riches elles font donc les bons choix", je
réponds : "non je ne crois pas"). Quand la manne disparaît, il leur faut
vite s'adapter, elles n'en ont pas l'habitude.

D'autres, dont **je fais partie**, pensent qu'il n'y a pas de rupture
entre la gestion d'une équipe et/ou d'une grande organisation puisque
nous basons notre fonctionnement sur une culture ; celle-ci peut ainsi
s'appliquer à tous les niveaux : les raisonnements, les comportements
qui en découlent sont les mêmes. Ce que développe l'organisation en
local, elle doit le développer de façon globale. Pensez à une approche
**organique** au lieu d'une approche **mécanisiste**, mode de pensée,
culture, plutôt que harmonisation ou rationalisation.

![Où est la créativité ?](/images/2014/01/bacasable.jpg)

### Premières pistes

#### Cristal

Pour décrire cette façon de pensée **organique** de l'organisation je
m'appuierai sur image que j'ai trouvé intéressante : celle du
[cristal](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/).
Je dois encore valider cette intuition. Car le cristal n'est finalement
pas forcément le bon matériau. La termitière ?

#### Complexité, désordre, apprentissage

Nécessité du désordre pour générer de nouveaux motifs d'apprentissage,
de la valeur, de la créativité, de la vie : pas des entreprises fossiles
qui ne respirent plus. Ces motifs prennent la forme d'histoire
(*storytelling*). Pour une première ébauche sur ces réflexions ce
[texte](/2013/12/auto-organisation-et-storytelling/), ou le chapitre
"**storytelling et auto-organisation**" de ce [mini livre sur la horde
agile](/2014/01/mini-livre-la-horde-agile/). Il est nécessaire d'avoir
du désordre pour avancer, créer, générer. Le désordre est toutes façons
présent dans notre monde complexe. Laisser l'auto-organisation naturelle
dont nous faisons preuve pour appréhender cette nouveauté. Motifs par le
biais de *storytelling* pour l'apprentissage (le stockage). Je décris
ainsi des systèmes ouverts, apprenants.

![L'univers](/images/2014/01/universe.jpg)

Observez les images, pensez vous sérieusement qu'un univers bien rangé
est riche ? Qu'un bac à sable propre est amusant et créatif ? (j'ai
piqué ces images au compte twitter :

[@organizedthings](https://twitter.com/organizedthings))

#### Théorie des catastrophes de Renée Thom

Autre intuition au fil des lectures, la **théorie des catastrophes de
René Thom** : celle-ci ne proposerait pas pour nos organisations des
schémas prédéfinis, mais des modèles. Ainsi on pourrait penser une
topologie des entreprises et comprendre leurs mouvements et formes,
autrement dit, une piste pour appréhender la discontinuité de formes des
organisations à l'aide d'un modèle connu. Pensez le mot *catastrophe*
comme évènement. Il se déroule un évènement qui génère un changement,
positif ou négatif. J'enchaîne ce préambule rapidement par un première
tentative d'appropriation de cette théorie dans le cadre d'organisation
d'entreprises (agile ou pas, peu importe). C'est ce qui m'intéresse le
plus, aujourd'hui, dimanche 19 janvier 2014.

#### Double lien, systèmes ouverts en thermodynamique

Beaucoup de mes copains savent que je hais le terme de Sociocratie, pas
ce qu'il cache. Il y a du bon et du moins bon, c'est un habillage, une
synthèse de pas mal de choses déjà vues par ailleurs. Mais comme toute
synthèse elle cristallise une nouvelle histoire, et est un intéressante
comme boite à outils faisant sens. Parmi ces outils, un se révèle très
inspiré par la thermodynamique des systèmes ouverts (de nombreux
penseurs l'évoquent pour parler organisation) : le double lien. En
thermodynamique un système est ouvert quand il y a échange de matière
(d'information !) et d'énergie entre le système et le milieu extérieur,
comme le font les êtres vivants par exemple, comme l'approche organique
évoquée plus haut. Ce double lien de la Sociocratie ne fait pas autre
chose que dire cela et il est peut-être plus simple à prendre en main
que l'idée de systèmes ouverts en thermodynamique. J'y reviendrai donc
*(spéciale dédicace Grég.L)*.

Voilà, une première perspective que je vais essayer de creuser. Ces
intuitions et réflexions se révéleront peut-être fausses. Peut-être
qu'un autre sujet s'emparera de moi entre temps, mais bon cela fait
plusieurs mois que cela trotte dans ma tête et j'y trouve un lien avec
ma horde agile, une continuité, donc *a priori*, je vais m'y tenir.

#### Série sur les organisations

0 - prémisses : [Agile à grande échelle : c’est clair comme du
cristal](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)

1 - penser son organisation :
[introduction](/2014/01/penser-son-organisation-introduction/)

2 - penser son organisation : [théorie des
catastrophes](/2014/01/penser-son-organisation-theorie-des-catastrophes/)

3 - penser son organisation : [sur le
terrain](/2014/02/penser-son-organisation-sur-le-terrain/)
