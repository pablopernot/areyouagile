---
date: 2009-10-25T00:00:00Z
slug: django-python-and-the-meaning-of-scrum
tags: ['monty','python','scrum']
title: Django Python and the meaning of Scrum
---

J'ai pu assister ce vendredi à une conférence organisée par le réseau
[Particul.es](http://particul.es) concernant Scrum, Python & Django.
Ayant pas mal touché à Django y'a quelques années (4 ou 5) ; ayant été
un grand fan de Python (le language et aussi les Monty puisque j'avais
réalisé mon master sur le gang anglais, si si...), enfin étant
aujourd'hui un défenseur ardent des méthodes agiles et plus
particulièrement de Scrum j'ai immédiatement sauté sur l'occasion.

![Scrumpy](/images/2009/confparticules.jpg)

La conférence était organisée sur une alternance des "prez" 30mn sur un
domaine, 30 mn sur l'autre durant 4h, saupoudrez de 30mn de pause. 
[Claude Aubry](http://www.aubryconseil.com/) menait les affaires
concernant Scrum, et [David Larlet](http://www.biologeek.com/) prenait
en main les choses Python. Comme je vous le disais venant des deux
mondes je me suis senti "dans le bain", mais je m'interroge sur les
personnes n'étant venu que pour un seul domaine. D'un côté cette
alternance donnait du rythme mais d'un autre elle a peut-être empêché de
creuser assez un sujet, ou d'interroger assez précisément un intervenant
: pas assez de temps libre "off" avec les intervenants ! ,  une
déception je n'ai pas eu le temps de parler rock'n roll avec Claude
Aubry entre autres.

Pour résumer, c'était très agréable de replonger dans le bain
django/python, et cela semble être comme le vélo : on n'oublie pas. Mais
je reste encore dubitatif sur la percé du langage dans les entreprises
ne serait-ce qu'en raison de la rareté des compétences locales.  Quand à
Scrum rien de nouveau de mon côté, mais ce fut un plaisir de croiser et
d'entendre cette présentation par Claude Aubry (il faut que je m'achète
un chrono de cuisine pour mes timeboxes). J'ajoute : très beaux slides
de David Larlet : simple et évocateur (des planches*creative commons* de
flickr), une bonne idée que je vais reprendre (mais n'est-il pas
developpeur web ? )

Enfin, première rencontre avec les gens de Particul.es. Sympa ! A ce
sujet, ces deux conférences en une sont le prélude de
formations/coaching autour de ces deux domaines (Python/Django & Scrum).
Je vous encourage à aller sur le site pour en savoir plus
(<http://particul.es>).

\[ajout\] Réflexion dans le bain cet après-midi : le vrai point commun
entre Scrum & Python c'est le fait qu'ils sont tous les deux
**compacts**. On s'approprie facilement et aisément les règles,
librairies, de ces deux outils. inutile d'aller fouiller dans une
documentation pour consulter le point x-44-b (comme Java par exemple qui
se noie sous la documentation, ou CMMi...). L'outillage est là, présent,
facile à appréhender, à l'esprit : c'est productif.
