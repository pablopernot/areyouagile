---
date: 2013-08-04T00:00:00Z
slug: scrum-2013-le-canevas
tags: ['scrum','productowner','scrummaster','sprint','planning','iteration','daily','review','demo','retrospective', 'dod', 'definitiondefini']
title: Scrum 2013, le canevas
---

Dans le TGV, 3 heures devant moi avec la clim', j'enfourne [Roxy &
Elsewhere de Frank Zappa](http://www.youtube.com/watch?v=xEZwa1Funh0)
dans les oreilles et je me lance dans une lecture du [Scrum Guide
nouveau](http://www.scrumguides.org/) (celui qui vient de sortir en
juillet 2013).

Avant de démarrer il me parait clair que si **Ken Schwaber & Jeff
Sutherland** sont les créateurs de **Scrum**, ils ne maitrisent plus
vraiment la créature. Le nom leur appartient, la consolidation du
"toolkit" sous cette appelation aussi, les concepts et les idées
derrière ont été libérées au délà de leurs espérances et hors de leur
réelle emprise.

Je n'ai pas lu ce guide depuis 4 ans, je ne cherche pas les
"différences" entre les versions mais les variations avec mon approche.

## Piliers Scrum

Très vite on observe à la lecture de l'opuscule que Scrum demeure un
socle, un canevas, un outillage, et pas une pensée, une culture, etc. On
observera que cette volonté de rendre les choses très opérationnelles
est présente partout, dès la description des piliers de Scrum, ses
valeurs en quelque sorte, **transparence, inspection, adaptation** : la
*Transparence* "pour les gens responsables" (*to those responsible*).
Schwaber & Sutherland décident ainsi de la limiter à ces personnes.
C'est probablement une volonté de rendre l'outil très aiguisé, peut-être
à tord, peut-être pas. On pourrait ne pas limiter la transparence...
*Inspection* : des éléments de scrums et de l'objectif du sprint.
Pareil, ils décident volontairement de cibler. Enfin, *Adaptation*, un
inspecteur observe (*an inspector determines...*). Encore une précision
-pas forcément utile-, nous tous pourrions penser à amélioration
continue non ? Je comprends que Schwaber & Sutherland souhaitent une
outil très défini, une boite à outil éprouvée. Très utile surtout pour
les prises en main (oui la transparence est essentiellement pour
l'engagement des stakeholders, parties prenantes, d'où la précision dans
le guide). Mais ce ciblage va peut-être vite se révéler une limitation
dès que la culture agile se répandra. Donc une lame à double tranchant.
Bref précisions oui, restrictions non.

## Rôles

J'aime la définition du Product Owner et de l'équipe, même si le mot
"développement" est très associé au code, ce qui n'est pas du tout
nécessairement le cas. Mais si les personnes font cette amalgame ce
n'est pas le cas du guide Scrum.

J'aurais ajouté : le Product Owner doit être fier, l'équipe heureuse, et
le scrummaster absolument neutre. J'aurais apprécié d'ailleurs comme ils
l'ont fait avec le Product Owner (*sole accountable*) certaines
précisions sur le scrummaster (trop de dérives observées) : pas de
production, pas d'estimation, neutralité oblige, par exemple. Mon
expérience m'incite à penser qu'il vaut mieux généralement ne pas avoir
de Scrummaster que d'en avoir un qui produit et qui estime.

## Taille de l'équipe Scrum

Là c'est un point sur lequel je ne rejoins pas Schwaber & Sutherland.
Ceux-ci proposent des équipes de développement minimale de 3 et maximale
de 9 auxquelles ils ajoutent le Product Owner et le Scrummaster. Dans
mon expérience j'observe une sorte de déliquessence dès que la taille de
l'équipe plus le Product Owner et le Scrummaster dépasse 7 (5+PO+SM).
Donc je suis loin des 11 (7+PO+SM) évoqué par les auteurs. Mon
observation terrain va d'ailleurs dans le sens d'autres observateurs qui
estime que 7 est un très bon chiffre pour une équipe (voir ces
informations sur le le [plaisir d'appartenance à un
groupe](/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/),
ce qui n'équivaut pas à performance officiellement, mais dans mon
panthéon le plaisir est souvent associé avec performance).

Ma pratique de 3 à 5 équipiers + PO + SM. Le PO et le SM peuvent
partager leur temps sur 2 projets en parallèle. A partir de 3 on
commence à dégrader réellement leur performance (au PO et au SM), mais
si la valeur induite est supérieure à cette dégradation pourquoi pas. Je
suggère aussi au Product Owner de réserver la moitié du temps qu'il
octroie au projet/produit aux travaux d'avance de phase (bichonner le
backlog, synchronisation avec les parties prenantes et autres PO, etc.)
et l'autre moitié à l'itération en cours (accompagnement de l'équipe).

## Sprint

Wouah ils continuent de proposer 1 mois en durée (max). 1 mois, c'est
énorme. 2 semaines est devenu un standard depuis 1 ou 2 an. Je ne suis
généralement jamais convaincu par les explications qui me disent qu'il
faut 1 mois (au lieu de 3 semaines par exemple) parce qu'il faut du
temps pour réaliser certaines choses. Ma réponse est que bien souvent il
suffit simplement de s'engager sur moins. Mais cela n'est possible que
si on utilise du jus de cerveau pour découper nos éléments. Cela demande
un effort. C'est cela que certains refusent.

Tiens on garde le mot "sprint", on ne pousse pas à l'utilisation de
"iteration".

Ma pratique courante : 2 semaines. 1 ou 3 semaines exceptionnellement
selon les contextes.

## Sprint planning

Schwaber & Sutherland évoque régulièrement un *sprint goal*, une sorte
de vision du sprint à l'instar de la vision du produit ou du projet. Par
exemple, le sprint du paiement en ligne pour [Peetic](/2012/11/peetic/).
Bon je n'ai jamais véritablement réussi : la priorisation du backlog est
déjà assez complexe (valeur, capacité production équipe, optimisation de
la productivité de l'équipe, etc.). Ajouter en plus une volonté d'avoir
un ensemble cohérent d'éléments qui tendent vers un objectif cohérent
(au niveau du sprint !) et fini n'est pas constant, pas forcément très
aisée, donc je ne le requiert pas.

Tiens on ne parle plus d'engagement, juste d'éléments sélectionnés pour
le sprint... J'aime la notion d'engagement car elle induit une
responsabilisation. Et la responsabilisation à mes yeux est une façon de
nourrir la motivation de l'équipe. Je n'aime pas l'engagement si il
devient un élément cohercitif à l'encontre de l'équipe. Tout engagement
peut être remis en question pour de bonnes raisons.

ma pratique : 2h à 2h30 de sprint planning 1 ère partie, 1h30 sprint
planning deuxième partie. La deuxième partie l'équipe est souvent
laissée à elle-même (auto-organisée comme toujours).

## Daily Scrum

Comme je vous le disais le *sprint goal* est notion que j'utilise peu.
L'objectif du sprint à mes yeux est de produire le maximum de valeur
pour le projet ou le produit sur lequel nous travaillons. Schwaber &
Sutherland promeuvent à nouveau une approche très opérationnelle en
changeant un peu les questions du daily scrum : ils y adjoignent "Qu'est
ce que tu as fait hier..." + "...concernant l'avancée de l'itération".
Ok pareil, a) cela me semble une évidence est-ce nécessaire de le
rajouter ? (toutes ces précisions sont un peu infantilisantes) b)
sait-on jamais la valeur viendra peut-être de quelque chose de
complètement externe au sprint et à l'équipe, pourquoi s'en priver ?

De plus le daily scrum me parait moins un moment de mesure comme ils le
sous-entendent (*inspect...progress toward... completing sprint*), qu'un
moment de partage de la culture et de la connaissance du projet/produit.
Sinon ma pratique est la même.

\[Roxy & elsewhere touche à sa fin, je bascule vers un live du [Grateful
Dead](http://www.youtube.com/watch?v=84QkKF-LF6s)\]

## Sprint review

La description est ok sauf que... dans ma pratique je limite le *sprint
review* à 30mn... (2h à 4h proposées par Schwaber & Sutherland).
Pourquoi ?

La revue de sprint est le moment de l'ouverture de la transparence vers
les parties prenantes, et le lieu du feedback. Je dis souvent comme dans
un mariage, "parlez maintenant ou taisez vous à jamais". Généralement il
est ardu mais essentiel de faire venir les bons les vrais parties
prenantes. Or leur proposer de participer à 2 heures de réunion toutes
les 2 semaines, ou 4 heures toutes les mois... est illusoire, et pas
forcément utile. Par contre il est **essentiel** qu'ils soient présent
aux revues.

Ces personnes clefs ne peuvent pas simultanément dirent que le projet
est important et qu'en parallèle ils n'ont pas 30mn toutes les deux
semaines à lui consacrer (si ils le font quand même interrogez les
gentiment sur cette contradiction). Si le projet n'est pas important (=
pas de présence aux revues), dans ce cas là proposez leur de l'arrêter,
on ne fait que des choses avec de la valeur dans le monde agile. 30mn
toutes les deux semaines est une valeur intéressante : 4-5 jours ouvrés
par an vous suivez l'intégralité d'un projet ou d'un produit sur un an
(intéressant pour les équipes qui gravitent autour). Car je n'ai pas
encore évoqué le cas des départements ou des produits et projt
multi-équipes : imaginez une entreprise avec disons 10-15 équipes en
parallèle, deux heures toutes les deux semaines, quinze heures par
semaines ??? Les parties prenantes sont souvent assez peu nombreuses,
cela devient impossible pour elles. Par contre 30mn\*8/semaine = 4heures
cela redevient jouable, et encore, il va leur falloir choisir mais cela
redevient envisageable. Pareil pour les membres des autres équipes qui
souhaiteraient suivre les revues des autres, ou les membres du
département support, etc.

Donc ma pratique : des revues de 30mn max, avec 5mn d'introduction, 20mn
de présentation des choses achevées, 5mn d'ouverture sur la suite (le
plan de release). Si il se révèle que la revue déclenche un somme de
feedback inattendue, ne pas hésiter à provoquer une réunion dédiée.

## Sprint retrospective

Au passage je suis surpris (j'avais oublié ?) que Schwaber et Sutherland
accordent moins de temps à la rétrospective qu'à la revue de sprint.
Ouch. Je désapprouve (mais ils s'en foutent et ils ont bien raison). Ma
pratique pousse à des rétrospectives qui oscillent autour de deux heures
toutes les deux semaines. Une autre précision sur laquelle je tique :
planifier les amélioration\**s*\* (*plan for implementing
improvements*). Il me parait essentiel, même si l'on détecte plusieurs
améliorations (ce qui est souvent le cas) de se limiter à la mise en
oeuvre d'une seule résolution (et de s'y tenir dur comme fer). En effet,
résoudre la difficulté estimée comme principale par l'équipe toutes les
deux semaines est déjà un bel accomplissement. Se lancer sur la
résolution de plusieurs améliorations simultanément à plutôt pour
résultat de diluer l'objectif et de moins s'y tenir... Cette variation
est peut-être due au fait que ces messieurs envisagent des itérations de
un mois, allez savoir.

Ma pratique : 2 heures, 1 seule amélioration ciblée et
mesurable/atteignable, la principale aux yeux de l'équipe.

## Backlog

Ok ok. Mais dans mon expérience difficile de donner une business value
chiffrée à chaque élément du backlog (c'est possible pour les
fonctionnalités, dur pour les *user stories*), avec les découpages et
autres manipulations cela devient en plus rapidement un casse-tête
chinois, donc je ne le suggère pas.

Autre différentiateur avec ma pratique, en cas de produit ou projet qui
sont conduits par plusieurs équipes je propose plusieurs backlogs, ce
qui n'est pas le cas de Schwaber & Sutherland qui suggèrent un seul
backlog partagé. Naturellement dans ma pratique la vision, le projet est
partagé mais chaque équipe a son backlog. Plus simple, plus lisible,
plus efficace, cela oblige à une clarification des objectifs et des
dépendances.

## Definition de fini

Schwaber & Sutherland accordent, peu clairement, la responsabilité de la
définition de fini à l'équipe de développement. C'est sûr ce n'est pas
simple, c'est l'une des choses les plus durs de Scrum. Pour moi la
définition de "done", de fini, appartient à tous... même au délà de
l'équipe Scrum elle-même. Je n'arrange et simplifie pas les choses. Je
le répète le "done" est l'une des choses les plus ardues. C'est une
notion qui doit convenir à l'équipe (sans cela elle apprend à
l'outrepasser), mais qui doit aussi convenir à l'organisation (sans cela
comment intégrer plusieurs équipes qui travaillent sur un ensemble
commun ? ). C'est quelque chose qui adresse autant les tâches (éléments
sous-jacents des *user stories*), que les *user stories* elles-mêmes,
voir des ensembles de user stories, ou même le sprint, et le projet dans
son intégralité. Après cela, que dire si ce n'est répéter : c'est un
élément complexe à manipuler. Donc ma seule recommandation c'est :
sensibiliser le plus de monde possible sur cette difficulté et cette
importance, ouvrez la discution avec l'ensemble de l'équipe dessus,
communiquez régulièrement dessus auprès des parties prenantes.

## Un canevas inclusif

Même si j'ai pris le temps de noter mes "différences" ou certaines
réflexions, je suis toujours bluffé par la qualité de ce petit ouvrage.
20 pages pour résumer un formidable outil -très mature-, d'une façon
simple et claire. Et surtout ouverte : le manuel semble dire : prenez
moi, utilisez moi, je ne suis pas limitif, je suis **inclusif**. On est
loin d'autres méthodes ou canevas beaucoup plus **exclusifs** (XP pour
ne pas le citer... ). Je pense que cette ouverture, que l'on retrouve
chez Kanban, est une chose très bénéfique de Scrum. Naturellement c'est
aussi pour cela que l'on se chope plein d'apprentis sorciers et de
dérives, mais cela part d'une bonne idée : venez l'auberge Scrum est
ouverte. Prenez.

## Feedback

### 9 août 2013 : Anthony Cassaigne

*Je peux dire que je partage assez largement ton point de vue. Notamment
qu'il est difficile d'avoir un objectif (cohérent) dans un sprint. Je
n'ai toujours pas trouvé comment faire. Pourtant dans Scrum et XP depuis
les tranchées Henrik Kniberg dit que ce n'est pas négligeable car çà
peut éviter de se perdre.*

*Pour la définition du fini, j'insiste sur la définition d'un Done pour
chaque élément. C'est à dire se donner une définition du done pour la
US, se donner une définition du done pour le sprint, se donner une
définition du done pour la release. Et pour l'équipe, s'il y a
divergence de point de vue entre les développeurs qu'ils se définissent
le done pour une tâche.*

*Quel est ton point de vu sur la notion US prête ? Je trouve çà
séduisant car çà officialise et formalise la possibilité qu'on les
développeurs de refuser une US pour le sprint à venir. Mais je n'ai pas
testé.*

Anthony, merci pour ton retour, j'ai vu faire une définition de fini
pour chaque type d'élément cela m'a parue une usine à gaz, une
complication de plus. Je n'ai donc jamais essayé et ne compte pas le
faire sauf si les acteurs le demandent. Cela induit trop de complication
et on perd l'idée du "tout" qui me parait indispensable. Je risque au
pire d'entendre "ah moi mon truc il est fini, à mon niveau". Or c'est
fini ou ce n'est pas fini. Dire "cela doit fonctionner avec 200000
utilisateurs simultanés" s'adresse autant à la user story qu'à certaines
tâches. les gens sont intelligents et savent quand cela impacte la user
story ou la tâche, ou les deux, etc. Si la définition de fini est trop
grosse (plus de 15 éléments... à 10 je sens déjà une limite) cela me
chagrine aussi. On devrait "l'avoir en tête", compacte. Cette limitation
est aussi une forme de priorisation.

### 10 août 2013 : Grégory Salvan

*J'ai beaucoup apprécié ton article sur le scrum guide 2013. C'est un
travail très inspirant, je pense qu'on devrait tous le faire. Ma petite
expérience étant différente j'ai quelques remarques.*

*"\[Ton\] expérience \[t\]’incite à penser qu’il vaut mieux généralement
ne pas avoir de Scrummaster que d’en avoir un qui produit et qui
estime."*

*Dans la mienne les product owners n'étaient pas capable de définir
correctement leur produit sans qu'on les aide car ils ne comprennent pas
correctement le métier de développeur (souvent un pb. d'indépendance des
story).*

*Ce que j'ai remarqué c'est que sans SM "quelqu'un qui produit" se
charge d'aider le PO et "par hasard" c'est toujours la même personne.*

*De même en tant que dev. j'aurai du mal à accepter un SM qui ne produit
pas ou qui est neutre. Au contraire j'attends de lui qu'il soit impliqué
dans sa relation avec le PO et dans le travail de production. S'il est
trop loin des problèmes techniques il y a un effet "boite noire" avec le
PO qui ne comprend pas pourquoi une story prend "autant de temps à être
réalisée" (par ex.)*

*Par contre pour moi il doit être impartial et ça demande un certain
recul lorsqu'on produit.*

*Un autre point que j'ai remarqué, lorsqu'il n'y a pas de SM, c'est que
les problèmes sont réglés plus tardivement (parfois après la rétro) car
personne dans l'équipe ne veut décider pour les autres ou interrompre sa
prod. pour s'occuper d'autre chose.*

*J'aurai tendance à dire qu'un SM qui n'est pas développeur ça produit
un effet silo.*

*Est-ce que tu as rencontré des problèmes similaire et qu'as/aurais tu
fait dans ces cas là ?*

*++ pour la taille de l'équipe et aussi pour l'objectif de sprint qui me
fait penser aux phases d'un projet classique.*

*Dans l'idéal je remplacerai l'objectif de sprint par une révision de/
réflexion sur la vision de produit. (quitte même à en avoir 2: une pour
le produit actuel et une pour le produit que l'on souhaite faire dans
l'idée du behaviourisme: état initial / état final)*

*Qu'en penses tu ?*

Merci Grégory (navré mais pas dispo pour la sortie en canöé). Je ne te
rejoins pas sur la position du Scrummaster ou du PO. Un PO laisse le
"comment" à l'équipe. Donc il n'a ncéssairement pas à connaitre les
arcanes de la technique. Cela peut lui servir à optimiser
l'ordonnancement de son backlog, mais pour cela il lui suffit d'écouter
l'équipe et de lui faire confiance. Il doit aborder les questions sous
l'angle métiers, libérant ainsi, laissant entièrement à la
responsabilité, toutes les questions de réalisations à l'équipe. Cela ne
devrait donc pas être un frein, cela peut même se révéler un avantage :
ces PO ne sont pas tentés par le "Comment", ils se restreignent au
"Quoi" et au "Pourquoi".

Autre chose : je ne dis pas qu'il ne devrait pas y avoir de scrummaster.
C'est **toujours** mieux avec un scrummaster. Dans les cas où cela se
révèle impossible : je dis que pour bien comprendre l'état d'esprit
sous-jacent, la culture demandée, c'est mieux de ne pas avoir de
scrummaster, que de trahir le rôle en le faisant sortir de sa
neutralité.

Que quelqu'un aide le PO, je ne sais pas, ils sont auto-organisés, et
ils font ce qu'ils pensent être bien. Si ce n'est pas le bon PO, il faut
oser le dire, cela ne remet pas en question les qualités de la personne,
mais la prise de ce rôle à ce moment, dans ce contexte.

Si le scrummaster n'est pas neutre, il ne peut pas faciliter, il ne peut
pas communiquer, il ne peut pas aider à faire comprendre Scrum. Etre
neutre n'interdit pas d'aider les gens, en faisant tout ce qui est en
son pouvoir hors des attributions du PO ou de l'équipe. Les PO que je
croise comprennent rapidement pourquoi une "story met autant de temps à
être réalisé" car ils participent activement au sprint planning, lieu de
toutes les discussions à ce sujet. D'habitude c'est les organisations
qui ont du mal avec un vrai Scrummaster, il faut réussir à leur vendre
quelqu'un qui n'aura aucune productivité **directe** mais sera la source
d'une **grosse productivité** pour l'ensemble.

En gardant des Scrummasters qui codent, qui estiment, on reste juste
dans l'ancien monde, on ne franchit pas le cap. Au travers de ce qui
peut sembler un détail, une différence d'implémentation énorme se
dissimule.

Oui la réflexion sur le produit doit être constante. A l'image du
backlog, un élément vivant en permanente mutation.

Merci pour ton feedback.

### 14 août: feedback de Claude Aubry (pablotages !)

Voici l'article de Claude sur son blog :
<http://www.aubryconseil.com/post/Scrum-Guide-2013-et-pablotages>

Et oui, de mon côté il n'est pas obligatoire au Product Owner d'être
présent au "daily scrum". Que l'on ne se méprenne pas : si le Product
Owner pouvait écouter tous les "daily standup", c'est le mieux. Mais
c'est bien une réunion au sein de l'*équipe de dev* dans l'équipe scrum.
Même le scrummaster n'est pas obligatoire (mais il doit s'assurer que le
daily scrum se déroule correctement). C'est le moment du partage de la
connaissance au sein de l'équipe (dans l'équipe auto-organisée), pas le
moment où l'on règle les grands problèmes, ni celui où on prend de
grandes décisions (de petites ok : "je vais faire la revue de ta tâche",
"je peux t'aider sur", etc.).

Il n'est pas interdit que cela soit le moment de grandes actions ou de
grandes décisions. Pourquoi s'en priver ? Mais alors on sort du "daily
scrum" : par exemple on claque dans ses mains et on annonce : "ok daily
scrum fini, réunion spéciale sur ce sujet". Car il faut absolument
préserver les 15mn (max, pas mini) du "daily scrum" pour garantir sa
pérénnité. C'est une réunion essentielle.

### 21 août: feedback de Pierre Neis


Bien ta revue, tu mets en avant les points qui ont ete mis en avant
en.... 2011... voir 2008?

Les changements majeurs dans le guide sont intervenus en 2011. Dans la
nouvelle version les subtibilites sont plutot rhetoriques
(<http://static.squarespace.com/static/51e3f87ce4b0031a73dac256/t/51f7de63e4b0eda27e6ea4ef/1375198819807/Scrum%20Guide%202013%20(Changes).pdf>)
:

-   transparence des artifacts --&gt; en clair, si l' equipe seule
    connait les artifacts ou si les documents sont planques sur un
    serveur, c'est FLOP
-   Sprint Planning repasse a un evenement: depuis 2011, le planning
    etait separe en 2 par le QUOI et le COMMENT. Le resultat du Planning
    est le meme depuis 2011: Sprint Goal (l' equipe annonce ce qu' elle
    cherche a atteindre, un plan definissant comment elle compte s' y
    prendre, un Sprint Backlog. Egalement depuis 2011, la Scrum Team ne
    fait plus de commitment mais un forecasting, une estimation.
-   Affinage du PBL au lieu de grooming sont des choix de terminologie
    lies a la langue anglaise qui peut etre sujet a interpretation. La
    notion de ready est renforcee: une equipe peut accepter qu'un item
    "ready" --&gt; lie a INVEST. Ceci reduit le cote "push".
-   les evenements de Scrum font partie integrante du Sprint et ceux-ci
    sont limites dans le temps. Cela signifie que tous les evenements
    en-dehors de ceux-ci sont des distractions, mais egalement cela
    previent les reactions negatives venant du nombre croissant de
    reunions. Ceci est un point d'attention apporte aux Scrum Masters.
-   Le Daily Scrum etait depuis 2011 l'Inspect/Adapt des developpeurs.
    2013 renforce le cote de planification en revisitant les 3 questions
    pour les lier avec l'objectif de Sprint. Dans mes equipes cela
    apporte une grande aide dans la detection de bugs. Lors du Daily,
    les developeurs font le triage et peuvent soit les integrer au
    Sprint ou les pousser dans la queue de preparation du Sprint suivant
    si c'est une dette technique.
-   le concept de valeur a la Revue de Sprint: ce point anodun est
    important car il renforce la position des developpeurs face au
    metier. La revue, a ne pas confondre avec la demo qui est une petite
    partie de la revue, a pour objectif de trouver un consensus entre la
    Scrum Team est le metier pour le prochain Sprint dans le but
    d'optimiser les resultats (business value vs dette technique).

Voila, c'est pas complique ;b)

Ah oui, j'oubliais. Petit rappel des durées des reunions:

-   quand c'est indique 4 semaines max, cela ne veut pas dire que moins
    n'est pas possible. Au contraire, c'est recommende.
-   duree max d'un Sprint: 4 semaines (2 c'est optimal, mais si tes
    clients ne sont pas dispo?)
-   duree max d'un Sprint Planning pour un Sprint de 4 semaines: 4
    heures (inversement proportionel)
-   duree max d'un Daily: 15 minutes
-   duree max d'un Sprint Review pour un Sprint de 4 semaines: 4 heures
    (inversement proportionel)
-   duree max d'une Retrospective pour un Sprint de 4 semaines: 3 heures
    (inversement proportionel)
-   grooming, oups l' afinage: 10% du temps de Sprint max.

Pour la taille des equipes 5+/- 2 personnes, en fait c'est depuis 2004
que cela fonctionne de cette maniere. Il s'agit en fait de dynamique de
groupe: au dela de 5 personnes la productivitie des equipes
proportionnellement stagne voir decroit (% h/dev).

Bon ben, j'en ai fini. Desole pour les accents, mon clavier US n'est pas
complet :b((

D'un point de vue personnel, les remarques que je pourrai apporter sur
le Guide sont les suivantes:

-   la cosmetique rhetorique est definitivement orientee vers le
    management pour reduire l'effet "push" cad command&control
-   j'aurai aime voir la notion de Sprint en tant que "pull system",
    flux tire pour toi ;b)
-   depuis 2011, j'attends la partie Release Planning qui est un point
    de blocage dans les organisations a Scrum multiples (la mienne)
-   une clarification sur les SoS
-   la notion de Product Line, la Gouvernance et la Gestion de
    Portefeuille liees au Release Planning
-   la notion de Level of Done (LOD), apparentee a XP, definissant le
    niveau de qualite delivree par les developpeurs.

@+ Pierre

Pierre, merci pour ton feedback. (pas le courage de replacer les
accents). Comme je le disais, peu importe à mes yeux la comparaison
entre les différentes versions et l'approche historico-chronologique. Ce
qui m'intéresse plus c'est les écarts avec ma pratique.
