---
date: 2011-01-11T00:00:00Z
slug: mammuth-et-les-5-pourquoi
tags: ['why','amelioration','retrospective','root','cause']
title: Mammuth et les 5 pourquoi
---

En voyant
[Mammuth](http://www.allocine.fr/film/fichefilm_gen_cfilm=146642.html),
film pittoresque qui voit Depardieu en motard débonnaire partir à la
recherche de ses justificatifs de travail pour toucher sa retraite à
taux plein, je suis tombé sur une séquence de mise en oeuvre des **5
pourquoi**. Du "**Root Cause Analysis**" entre Depardieu & Siné !

![Mammuth](/images/2011/01/mammuth-photo-promo-1-2010-depardieu-300x199.jpg)

Je rebondis sur l'occasion pour évoquer cette méthode que j'encourage
chacun à pratiquer régulièrement.

Tout le monde évoque toujours (ou presque toujours) le même exemple en
citant cette méthode : celle du **Washington Monument** (et encore, le
monument en question change souvent !) : L'obstacle n'est pas toujours
celui que l'on croit : en se demandant 5 fois "pourquoi ?" (ou 4, ou 6,
mais 5 est généralement significatif) on obtient souvent une réponse
bien plus pertinente que la réponse envisagée de prime abord.

Donc ce fameux exemple c'est celui du  « *Washington Monument* » qui
s'érode.  La firme responsable du ciment ne réussit pas à en trouver la
cause (*root cause analysis*). Et fait donc appel aux *5 pourquoi*

![Washington Monument](/images/2011/01/Washington_Monument_Dusk_Jan_2006-225x300.jpg)

-   Pourquoi le bâtiment se désagrège-t-­il ?

Parce que l'on y applique trop de produits chimiques

-   Pourquoi applique-­t-­on trop de produits chimiques ?

Pour nettoyer les crottes de pigeons !

-   Pourquoi y a-­t-­il autant de pigeons ?

Car ils mangent les insectes sur le bâtiment !

-   Pourquoi y­ a-­t-­il autant d'insectes ?

A cause de la lumière !

Solution : Réduire les horaires d'éclairage du Monument...

J'en reviens à mon "Mammuth" même si ici la séance de *Root Cause
Analysis* finie en eau de boudin... :

Siné : Pourquoi on ne t'a pas déclaré à la caisse de retraite agricole ?

Depardieu : Ben parce que je ne suis pas agriculteur.

Siné : Et pourquoi tu n'es pas agriculteur ?

Depardieu : Ben parce que je n'ai pas le diplôme.

Siné : Pourquoi tu n'as pas le diplôme ?

Depardieu : Parce que je n'ai pas le bac.

Siné : Pourquoi tu n'as pas le bac ?

Depardieu : Ben, parce que l'école me faisait chier.

Siné : Et pourquoi elle te faisait chier l'école ?

Depardieu : Parce que les profs m'emmerdaient.

Siné : Et pourquoi ils t'emmerdaient les profs ?

Depardieu : Ben parce que je n'y comprenais rien !

Siné : Et pourquoi que t'y comprenais rien ?

Depardieu : Ben parce que... parce que ...

Siné : T'as qu'un mot à dire (...) je vais t'aider ... parce que tu es
con.

![Siné Depardieu](/images/2011/01/sin_depardieu-300x158.png)

Bon ok cet exemple fini en queue de poisson,  et finalement je me
demande si je n'aurais pas du comme tout le monde me limiter au
Washington Monument...

Pourquoi trouve-t-on si peu d'exemples concrets (comme celui du
"Washington Monument"). Car souvent la réponse est très liée au
contexte, et donc n'a pas vraiment de sens en tant qu'exemple. Je vous
encourage cependant à pratiquer cette petite méthode simple et qui se
révèle très souvent (à mes yeux) très efficace. Naturellement on lui
oppose aussi pas mal d'objections ([voir criticism
ici](http://en.wikipedia.org/wiki/5_Whys)) mais j'en reste très amateur.
Ca ne demande pas beaucoup d'effort et cela rapporte souvent gros.
