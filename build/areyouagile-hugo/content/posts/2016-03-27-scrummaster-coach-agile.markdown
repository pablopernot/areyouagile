---
date: 2016-03-27T00:00:00Z
slug: du-scrummaster-au-coach-agile
tags: ['scrummaster','coaching','coachagile','coach']
title: Du scrummaster au coach agile
---

Suite d'un tweet que j'ai émis ce week-end : "finalement un coach agile
c'est un scrummaster qui ne prend plus de décision". Voici un essai
brouillon de complément de réponse.

Scrummaster ? Coach agile ? Normalement, idéalement, à mes yeux ces deux
expressions désignent la même chose, le même rôle, la même
**intention**, la même **attitude** (vous savez que j'aime ces deux
mots). C'est le même boulot. Je suis d'accord pour convenir que le
*scrummaster* désigne quelqu'un de moins expérimenté que le *coach
agile*. Mais si l'expérience est souvent un critère de qualité, elle ne
l'est pas toujours. Ainsi d'accord pour commencer à dire que le
*scrummaster* a généralement un périmètre plus réduit que le *coach
agile*. Par exemple le scrummaster est dédié à une équipe, et le coach
agile à une organisation, ou du moins quelque chose qui dépasse une ou
deux équipes, un département, un domaine, etc. D'où le serpent qui
change de peau, pour simplement représenter la mue. Mue qui n'ouvre pas
forcément sur quelque chose de différent contrairement à la chrysalide.

Si j'essaye de répondre à cette question c'est que je lis des articles
sur ces métiers (comme celui récent de
[Claude](http://www.aubryconseil.com/post/Une-apres-midi-revelatrice-de-ScrumMaster),
ou celui plus ancien de
[Géraldine](http://www.theobserverself.com/revelations/mauvais-scrum-master/)),
et que l'on m'interroge assez souvent : comment devient-on coach agile ?
J'avais l'habitude de répondre : il suffit de faire un blog et de mettre
"coach agile" dessus, ou un CV et de changer les termes.

## Un vrai problème de terminologie

Problème : aucun terme n'est bon. Que cela soit scrummaster ou coach
agile. Traduction anglaise défectueuse ? Souci de sémantique ou de
sémiotique ? Les deux : *Le sémiotique (le signe) doit être RECONNU; le
sémantique (le discours) doit être COMPRIS (É. Benveniste, Probl. de
ling. gén., II, Sémiologie de la lang., 1974)*.

*Coach* : celui qui conduit ou qui porte ? *Master*, celui qui dirige ou
qui maîtrise ? Le flou règne. Et chacun s'empare de ce flou pour y
mettre ce qu'il y veut. D'autant que des proximités avec d'autres termes
augmentent la confusion : promiscuité entre scrumMASTER et CHEF de
projet, voisinage entre le coach agile, organisationnel et le coach
individuel (le vrai coach).

Pour rester dans la linguistique je suggérais aussi un temps de suivre
le conseil de Henrik Kniberg (je ne sais plus où je l'ai entendu ou lu)
de préférer *coach* à *scrummaster* car *a minima* coach sous-entendait
du support.

Je reste cependant sans réponse sur le sujet du titre, du terme à
employer. Pour ma part je préfère évoquer du "mentoring agile", ou du
management organisationnel (le mot agile est lui aussi indéterminé...).
J'ai un peu évolué mais je suis toujours en phase avec cet article : [Je
ne suis définitivement pas coach
agile](/2015/03/je-ne-suis-definitivement-pas-coach-agile/).

## Cheminement de scrummaster à coach agile

À la lecture des articles évoqués plus haut ou des conversations que je
peux avoir, qu'est ce donc que je souhaite mettre en avant dans ce
cheminement de scrummaster à coach agile ? Est-ce qu'en gagnant de
l'expérience et en s'ouvrant à des espaces plus grands que l'équipe un
cheminement est généralement opéré ?

Il n'y aucun jugement de valeur entre scrummaster et coach, les deux
fonctions sont nobles et se suffisent à elles-même.

Les deux rôles devraient se draper de neutralité et clarifier le cadre,
les règles, tenir l'espace, rendre visible ce qui devrait être vu (chez
[beNext](http://benextcompany.com) j'ai appris à préférer le
"NoBullshit", dire la vérité, une de nos valeurs, à la transparence, qui
peut être vécu comme une mise en danger de l'intimité), propager et
soutenir le *feedback*, l'apprentissage, faciliter. Mais on leur demande
de venir avec un savoir-faire. Le danger étant dans la croyance à la
répétabilité et à la linéarité de l'application de ce savoir-faire
([différence forte entre l'Agile et le Lean à mes
yeux](/2014/11/les-cowboys-hippies/)). On vient surtout avec une
culture, des valeurs, une pensée, une approche, et pas une recette de
cuisine. Souvent c'est un parcours croisé : on part avec une façon de
faire forte, une recette de cuisine bien décrite, une liste à appliquer
sans approfondir ce qui se cache derrière. On est flou sur le sens, on
est strict sur la forme. Plus on avance, plus on développe le sens,
moins la forme n'a d'importance.

À l'image de [Shu-Ha-Ri](https://fr.wikipedia.org/wiki/Shuhari) du
scrummaster au coach agile, on pratique d'abord le geste, puis on
comprend les variations dans les motifs, puis on a intégré le sens.

Quelques questions que je me poserais pour s'étalonner dans ce
cheminement :

-   Le cheminement et la direction possèdent-ils plus de valeur que la
    façon de faire ?

Un coach agile va travailler sur le cadre et ne pas prendre de décision
pour l'équipe ou les personnes. Il va clarifier le cadre, les règles,
l'objectif, le feedback, l'apprentissage, etc. Les premiers pas d'un
scrummaster s'appliquent à adresser la méthode, à suivre les guides.

D'abord on communique, puis en évoluant on ouvre les canaux de
communication, on ne fait pas la communication, étape suivante on
indique que les canaux de communication dysfonctionnent et on permet aux
personnes de s'emparer du problème, puis on met les gens en position de
mettre en évidence leur sens et leur cheminement, et de découvrir que
leurs canaux de communication ne fonctionnent peut-être pas bien. Au
début on est un scrummaster puis on devient un coach agile.

-   Est-ce que tu prends des décisions pour l'équipe qui relève de la
    valeur à créer (et pas du respect et du maintien du cadre) ?

De fait le coach agile ne prend pas de décision qui sont liés à la
valeur de son client, de son interlocuteur, il essaye de clarifier le
cadre... *bis repetita*. La neutralité est une attitude essentielle à la
bonne réalisation de ces rôles scrummaster/coach agile. Chaque décision
prise à la place de l'équipe génère une dette d'accompagnement qui
éloigne celle-ci d'un certains épanouissement et donc de la performance
(être bien, être bon). Souvent les scrummasters sont enclins à prendre
plus de décisions que les coachs agiles, ou malheureusement on attend
cela de eux, et ils s'y sentent contraint.

Ne pas prendre de décision ne veut pas dire que l'on est faible ni que
l'on n'a pas de conviction. Nos forces et nos convictions se situent à
un autre niveau : nous croyons à notre analyse du monde, à notre
approche de pensée, à notre philosophie (mais comme tout le monde nous
pouvons aussi douter).

Prendre des décisions sur le sens et la valeur de son client c'est
sacrément gonflé aussi, voire prétentieux. Mais des fois être gonflé
c'est bien. Enfin je ne sais plus. Pour lui ouvrir les yeux sûrement.
Mais cela ne va pas l'aider à changer, cela va même freiner son
changement. D'où le "mentoring agile" : on lui ouvre les yeux (ça aussi
c'est prétentieux) en agissant d'une certaine façon pour soi. À lui de
décider si cela l'intéresse ou pas. Cette position est possible quand on
adresse des niveaux d'organisations au delà de l'équipe, au delà d'un
aspect opérationnel finalement réduit, c'est donc difficile pour le
scrummaster.

-   Est-ce que tu te projettes au delà des équipes, à un niveau plus
    large, avec cette compréhension du "voir le tout", du systémique ?

Comme évoqué plus haut c'est peut-être le périmètre qui différencie le
plus le coach agile du scrummaster. Généralement on demande au coach
d'aborder un espace plus grand, d'intégrer la notion de "voir le tout",
d'avoir une approche
[systémique](https://fr.wikipedia.org/wiki/Syst%C3%A9mique). Le
scrummaster est généralement focalisé sur une équipe.

-   Es-tu intransigeant ou empli de doutes ?

La réponse n'est pas si simple. Forcément en avançant on devient humble
car on connaît l'étendue des possibilités qui s'ouvrent devant nous.
Plus on observe de motifs, plus on sait qu'ils sont nombreux. Mais notre
intuition se forge et se clarifie. Nos doutes et nos intransigeances ne
sont pas les mêmes entre le scrummaster et le coach agile.

## Tarif et présence

Pour revenir à l'une des différences évoquées : l'expérience. Elle
entraîne habituellement une différence de tarif. Le tarif joue un rôle
sur le temps de présence. Et le temps de présence induit des
comportements plus ou moins bons.

Un coach agile a un tarif journalier plus élevé que le scrummaster. Mais
il ne coûte pas nécessairement plus en globalité car il devrait être
moins présent. On espère du coach agile qu'il ne va pas agir. On espère
la même chose du scrummaster mais c'est plus dur car généralement il est
là tout le temps, et il est très difficile d'expliquer aux organisations
que pour permettre à tout le monde d'être plus performant (être bien,
être bon), le scrummaster ne doit jamais sortir de sa neutralité pour
faciliter au mieux, et ainsi ne rien produire du tout. C'est ma
philosophie en tous cas. Il peut et devrait épauler, appuyer, supporter,
mais pas produire. Difficile, car souvent la pression est forte, de
l'organisation, de son employeur. Donc avoir un scrummaster à 100% c'est
aussi le risque de pervertir son rôle. Et pourtant un scrummaster
embauché et présent à 100% c'est aussi la preuve d'une implication dans
la démarche de la part de l'organisation. Je vous laisse vous
dépatouiller avec ça, moi je ne m'en sors pas.

Dans mon attitude de coach agile, mais je préfère "mentoring agile", ou
"management organisationnel" vous savez, j'explique à mes interlocuteurs
que je serai là de deux jours à six jours par mois (exceptionnellement
huit). En dessous de deux jours ma présence est trop diffuse pour
apporter de la valeur. Au delà de six jours on commence à se dire que je
vais faire le boulot à la place des personnes, et on oublie complètement
la transmission et l'appropriation côté organisation qui font partie
intégrante d'un succès pérenne. Une soutenance en avant vente la semaine
dernière, on m'a demandé ce que les coachs agiles prenaient comme
responsabilité sur le projet déclaré, j'ai répondu "aucune". Ce qui ne
nuit en rien à notre implication. (post-scriptum : soutenance gagnée).

## 4 paradoxes du changement

Lors de ma dernière présentation sur [Open
Adoption](http://fr.slideshare.net/pablopernot/open-adoption-darefest-2015)
(j'en fais une bientôt chez [NCrafts](http://ncrafts.io/) sur ce sujet,
rendez vous le 12/13 mai !), j'évoquais les 4 paradoxes du changement
agile (que j'[évoquais déjà ici](/2015/09/paradoxes-des-transformations-agiles/)), voyons comment le
scrummaster et le coach agile s'y positionnent :

### "Shu-Ha-Ri Combo"

**L’émergence s’appuie sur un apprentissage orthodoxe (et pas sur nos
habitudes ou nos désirs)**. C'est le début d'un apprentissage agile : on
s'essaye à une pratique pour s'y confronter, apprendre, découvrir notre
adéquation avec elle. Avant de décider par avance comment s'y adapter
(ce qui est une erreur fréquente). Le scrummaster tout comme le coach
agile (mais c'est les mêmes !) sont normalement à l'aise avec cela car
il s'agit justement d'appliquer une façon de faire pour en percevoir le
sens.

Difficile cependant parfois pour le scrummaster qui vient de
l'organisation, et qui a une forte mémoire du muscle (le retour aux
habitudes de l'organisation, une empreinte forte de celle-ci de ce
qu'elle a été). Plus facile souvent pour le coach agile qui est payé
plus cher : on l'écoute donc plus, et qui est souvent externe : on
l'écoute donc plus.

### Pas d'état stable

"L'état stable d'un organisme vivant c'est la mort" disait SchrÖdinger.
Comme les organismes, nos organisations ne sont pas stables. C'est aussi
l'un des messages utiles de l'Holacratie : comme un organisme, le
changement dans nos organisations est et sera continu.

**On doit donc s’adapter à un contexte qui n’attend pas pour évoluer
(donc on s’adapte constamment)**. Ce message est souvent porté par les
coachs agiles, généralement munis de plus d'expérience et avec une
assise plus stable pour être écouté sur ce message parfois perturbant
(encore une fois pas de linéarité, ni de répétabilité, même souvent au
sein de la même organisation).

On demande aux gens de s'adapter au contexte mais celui-ci est
changeant. Il faut généralement l'assise d'un coach agile pour garder la
cohérence d'une pensée au delà du contexte. Plus difficile pour le
scrummaster.

### Suspendre ses croyances

![Ka](/images/2016/03/ka.jpg)

Une équipe que je croisais c'était amusée à me charrier en affichant un
Ka sur le mur "ayez confiance". Ce qui voulait dire : "essayez" ou
était-ce moi qui jouait à l'apprenti sorcier ?

**Avancer dans une direction en suspendant nos croyances (essayer,
oser)**. Le changement de paradigme lié à la pensée Agile demande une
vraie expérimentation. Essayer sans idées préconçues. Cela vient soit
d'un coach agile qui saura rappeler la philosophie, le sens, de la
pensée Agile pour guider les expérimentations, soit d'un scrummaster
innocent qui va essayer avec toute sa bonne foi d'appliquer ce qu'il a
appris. Cette deuxième option est naturellement plus risquée mais elle
peut porter ses fruits. Le moment le plus dangereux vient du scrummaster
qui mue vers le coach agile, à la frontière, il est assez sûre de lui
pour faire prendre d'énormes risques à son organisation, il ne doute pas
encore assez pour mesurer les risques qu'il prend.

### Le changement est facilité si on peut revenir en arrière

Pour changer personne ne doit y être obligé. L'invitation est le moteur
du changement. L'invitation est renforcée si on peut revenir en arrière.
Le changement est donc un ensemble d'expérimentations dans une direction
(qui permet d'essayer en suspendant ses croyances, d'essayer pour
intégrer ce nouveau paradigme), avec retour arrière possible. Idéalement
ces expérimentations sont le fruits des propositions des personnes
amenées à changer. On est vraiment ici dans le cœur du coaching agile à
mes yeux. Bien au delà de l'application d'une méthode comme Scrum. Ce
n'est malheureusement pas quelque chose que l'on attend généralement
d'un scrummaster. À tord, comme je vous le disais finalement c'est le
même rôle à mes yeux. La rétrospective et son amélioration continue est
généralement le premier outil qui ouvre ce genre de pratiques,
d'attitude.

## Scrummaster ou coach agile ?

Tout cela pour revenir au serpent. Cela se mord la queue. Idéalement
scrummaster et coach agile, pas de différence, juste une simple mue liée
à l'expérience, au cheminement dans son parcours. Idéalement il faudrait
trouver un mot identique.

La chose à éviter c'est de ne pas être réduit à un simple affichage ("on
est agile"), au simple *decorum*.

![Snake Bag](/images/2016/03/snake-bag.jpg)

<hr/>

## Feedback

### Dragos

Tiens [Dragos](https://twitter.com/ddreptate)
([beNext](http://benextcompany.com) powa !) me glisse : *Feedback
bienveillant : trop long le dernier article, scinder en deux, voir
trois. Très intéressant, dommage de perdre certains seulement à cause de
sa taille.*

Je réponds : je ne me trouve pas trop long, mais très brouillon. Je
laisse les gens prendre ce qu'ils veulent dedans. Le fait qu'il soit
brouillon c'est juste le fait que ce n'est pas encore clair dans ma
tête.

### Julien

[Julien](https://twitter.com/jkaroubi) me fait une réponse tonitruante,
à laquelle je ne sais pas trop quoi répondre.

"Merci pour cet article Pablo.

Ma ptite contribution sur ce sujet :

Un coach agile est souvent utile pour commencer l'agilité ou la faire
grandir (quantité et qualité).

Quand on doit accompagner un chef de produit, un manager, un architecte
logiciel... avoir fait son job ça aide.

Comprendre les difficultés de leurs responsabilités, de leurs
engagements. Et ainsi leurs permettrent d'avoir les nouveaux
automatismes et de faire corps avec le reste du groupe c'est essentiel
pour dépasser l'agilité d'équipe.

Passer de scrum master a formateur Product Owner ou coach Product Owner,
sans avoir fait le job de Product Owner me semble difficile.

Comme coacher sans avoir été coaché.

De plus il existe des formations sur le coaching et le coaching agile en
particulier.

On peut faire ce métier avec professionnalisme et sans se faire passer
pour ce que l'on est pas encore 😉.

Et l'agilité ne se limite ni a scrum ni à l'accompagnement de
développement informatique.

En entreprise on parle de RH, marketing, Business, relation client,
support ...

De même les interactions entre 6 personnes sédentaires et un service
c'est un accompagnement différents avec le sujet des silos par exemple.

Le coach agile est à mon sens quelqu'un qui sait à minima faire le job
du scrum master (gestion de produit via l'équipe) mais pas mal d'autres
choses en plus.

De plus il n'y a pas que des sujets d'équipes dans une entreprise.

Et le coaching individuel prend aussi tout son sens quand on travaille
avec un manager sur la posture par exemple. On ne parle plus de produit
ou de rituel mais bien de relation et d'interactions.

Donc le coach agile à mon sens navigue de la prestation individuelle,
d'équipes via le coaching de scrum master, à l'organisation.

J'espère que cela précise plus que cela n'embrouille mais le sujet en
tant que coach agile m'interpelle particulièrement.

Mais j'ai un avis biaisé par ce que j'ai vécu donc pas très objectif.

Thks﻿"
