﻿---
date: 2018-07-17
slug: est-ce-que-vous-signalez-ce-que-vous-desirez
tags: [signal, emergence, transformation, dirigeant, manager]
title: "Dirigeants, est-ce que vous signalez ce que vous désirez ?"
---

Pour les dirigeants transformer leurs organisations c'est d'abord signaler clairement qu'ils le désirent, qu'ils le veulent, et agir en conséquence.

Car comment imaginer que l'émergence ne prendra pas la forme de nos désirs[^atlan] ?  

Trois questions pour vous. 

Est-ce que vous le désirez ? 

Est-ce que vous signalez ce que vous désirez ?

Comment ?

Signaler ? Quel signal ?   

Le signal c'est une forme en action.   

Le signal indique que c'est possible. Le signal indique que l'on essaye, que vous essayez. Le signal montre qu'on le désire, que vous le désirez. Les autres le sentent, le voient, le touchent, le signal est clair. Sans cette clarté du signal des dirigeants, les phases de mutation, de changement, qui transitent nécessairement par une phase chaotique risquent de s'enliser. 

Par exemple, le signal démontre que cette organisation veut vraiment changer, elle le signale en organisant des cycles de transformations, qu'elle essaye de mettre en œuvre les engagements définis à ces moments, que les réponses apportées sont réellement en fonction de ce désir, de ce sens. Les gens qui échouent ou qui se trompent ne sont pas blâmés, mais on cherche à savoir pourquoi cela a échoué pour réessayer avec plus de chances. Le signal que tous les décideurs même si ils s'expriment différemment proposent la même direction, pensent au même lieu, ont le même désir. 

Ce signal bien visible à tous qui dit que c'est possible, qu'on le désire. 

Et le fond alors ? Le fond viendra. Si le signal est bien le témoin de cette volonté. Si cette volonté de changement est clairement signalée, partagée, le reste suivra. L'émergence, l'intelligence collective, seront à l’œuvre. Parfois tout est mis en place pour le changement : tous les rituels, tous les outils, toutes les façons de faire. Mais j'ai observé que souvent le vrai changement vient d'ailleurs, d'une idée au coin d'une conversation, dans un lieu inattendu, dans un moment non crucial, non préparé pour. Il a pu être là, car tout le reste est présent. Car tout le reste signale qu'on est en mouvement. Ce moment de bascule peut s'épanouir, car tout autour concourt à montrer l'on veut accompagner ce changement, que cela est désiré.  

Si on croit à l'émergence comme je le fais, on attache plus d'importance au signal qu'à la façon de faire, et on sait que le fond viendra.

À nouveau. Ces questions sont importantes à se poser. 

Est-ce que vous le désirez ? 

Est-ce que vous signalez ce que vous désirez ?

Comment ?



[^atlan]: Cela me rappelle cette citation de Henri Atlan : ["cette auto-organisation dévoile notre volonté inconsciente (et ainsi le futur, vers quoi nous tendons), puisque nous essayons de créer de l’organisé en partant du chaos selon nos désirs"](/2013/12/auto-organisation-et-storytelling/).   





