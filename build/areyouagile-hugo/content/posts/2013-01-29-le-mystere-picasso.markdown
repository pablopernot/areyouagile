---
date: 2013-01-29T00:00:00Z
slug: le-mystere-picasso
tags: ['leanstartup']
title: Le mystère Picasso
---

Comment ai-je pu oublier cela ? Comment ai-je pu passer à côté de cette
démonstration ? J'ai vu, revu, au cinéma, ce film. Je l'ai analysé,
décortiqué, durant mes études universitaires. Et j'ai oublié.

![Le mystère Picasso](/images/2013/01/mystere-picasso.jpg)

Et puis Jérôme me l'a remis dans la figure. Vous savez comme De Funès
avec son nez : paf, paaaf ! Mais bien sûr bon sang. Car cet extrait
(grâcieusement accéléré par Jérôme, tout le film est une merveille que
je vous recommande) est une très bonne illustration de la conception
émergente chère à l'agile (ou si vous préférez à toute réalisation de
projet/produit dans un monde complexe, imprédictible, moderne).

Picasso va peindre sur une vitre, et Clouzot va filmer depuis l'autre
côté. Il y a des ellipses pour rythmer le film (mais je le rappelle dans
l'extrait ci-dessous Jérôme a encore accéléré le rythme). On découvre
donc, devant nos yeux ébahis, tout le processus créatif du grand
peintre.

<iframe src="http://player.vimeo.com/video/58469144" width="500" height="375" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

Cet extrait met en évidence le suivi d'une "vision" d'un objectif, et
aussi la découverte de valeurs au fil de l'eau. Picasso sait ce qu'il
veut faire (un taureau et un toreador), mais il va découvrir de nouveaux
gisements et innover au fil de l'eau, sans perdre de vue le sens de son
projet. C'est la leçon du [Marshmallow
Challenge](/2011/04/les-enfants-et-le-marshmallow-challenge/) :
prototypage, amélioration, nouvelles idées, prototypage,etc.

Cet extrait fait écho à l'[article de Jeff Patton](http://www.agileproductdesign.com/blog/dont_know_what_i_want.html)
sur la développement itératif ou incrémental (avec sa très bonne
utilisation de la Joconde). Et Jérôme me glisse que cette utilisation de
l'extrait du [film de Clouzot](http://www.amazon.fr/Le-Myst%C3%A8re-Picasso-Pablo/dp/B00005QSZ7)
provient de cette [présentation de Derek Sivers](http://www.youtube.com/watch?v=HhxcFGuKOys).

Merci Jérôme en tous cas de l'avoir rappeler à mon bon souvenir.

