---
date: 2014-03-02T00:00:00Z
slug: lectures-automnehiver-2014
tags: ['livre','lecture']
title: Lectures automne/hiver 2014
---

Voici un petit lot de lectures (4 ouvrages) que j'ai pu réaliser durant
l'automne & l'hiver 2013/2014 (j'adore ce petit côté catalogue des 3
suisses que les personnes de mon âge auront reconnu). Au passage vous
trouverez en fin d'article un rappel de tous mes liens "lectures".


## Reality is broken, Jane McGonigal, 2011

![Reality is broken](/images/2014/03/reality-is-broken.jpg)

Clef de voute -- il me semble -- des inspirations de [Dan
Mezick](/2013/10/deux-amis/), c'est donc un livre que je me suis
empressé d'acheter. A la question : qui passe en temps fou (\~20/30h)
par semaine à payer pour échouer ? Qui passe un temps fou (\~20/30h) par
semaine à s'impliquer avec un niveau d'exigence rarement atteint en
entreprise, tout en payant ? Qui maintient une documentation précise, à
jour, pour faire monter en compétence ses 25 millions de camarades en
compétence ? La réponse est toujours la même : les joueurs (notamment de
jeux vidéos). Implication, niveau d'exigence, etc, le "bon" monde est de
l'autre côté de l'écran (c'est le nouvel Alice de l'autre côté du
miroir). En s'y intéressant on va savoir comment améliorer le notre
monde. L'idée centrale de Jane McGonigal est de réapprendre notre monde
en s'inspirant des mondes virtuels des jeux vidéos. Et c'est franchement
excitant. Toute la raison et l'importance de la *gamification* déboule
et s'explicite aux travers de ces pages. Si parfois c'est un peu long
(mais c'est car on sent que Jane McGonigal est elle-même très immergée
dans les jeux) c'est très instructif.

Vous devriez lire ce livre si vous voulez changer d'axe de lecture, de
pensée, et si vous avez un enfant qui s'investit complètement dans les
jeux vidéos, avec un (très) haut niveau d'exigence, et que son carnet
scolaire dit : "mathieu est doué mais il ne s'investit pas assez".


## The spirit of leadership, Harrison Owen, 1999

![The spirit of leadership](/images/2014/03/leadership.jpg)

Les histoires simples et les mots sans prétention de Harrison Owen
désarment. Mais n'est ce pas la définition même du virtuose que de
laisser penser que c'est simple ? Plein d'idées saines et inspirantes.
J'y ai retrouvé une petite histoire que je m'amusais à raconter (pour
l'avoir entendu dans une conf') : un recteur d'université laisse se
dérouler les travaux de la nouvelle université. Tout est fait mais il
reste encore à aménager les chemins reliant les différents bâtiments,
soudainement le recteur suspend mystérieusement les travaux jusqu'à
l'année suivante. L'université ouvre sans ces chemins, mais alors que
l'année s'achève les chemins ont été tracé naturellement, de la façon la
plus optimale, en complète harmonie avec les attentes des étudiants, par
eux mêmes, mais pas du tout comme imaginé initialement, évitant ainsi de
nombreux problèmes. N'est-ce pas la meilleure introduction à une
adoption agile réussie ? Laissez les gens tracer eux mêmes les chemins
au sein de votre entreprise ?

Sur le management, le leadership, la conduite du changement : une
approche sensible, essentielle. Vous devriez lire ce livre si vous
voulez aborder le management comme une vraie relation humaine, ce qu'il
doit être.


## Food of the gods, Terence McKenna, 1993

![Food of the gods](/images/2014/03/food-of-the-gods.jpg)

Après avoir lu "Reality is Broken" (cf ci-dessus), je me suis remis à la
PS3, pour voir, j'ai fini *Last of us*, et je vais commencer *Dragon
Crown*... Est-ce que je vais essayer les champignons hallucinogènes
suite à la lecture de "Food of the gods" de Terence McKenna ?
Certainement.

Ce livre est disponible en pdf ici : [Food of the
gods](https://archive.org/details/TerenceMckenna-FoodOfTheGods.pdf).

Ce livre est l'histoire des drogues. Les plus anciennes, celles qui ont
fondé notre civilisation : les champignons hallucinogènes (sinon comment
expliquer le développement si soudain du cerveau ? l'avantage
considérable des effets comme l'acuité visuelle, les perceptions
exacerbées, ou même l'arrivée du langage avec la logorrhée de ces
stupéfiants...), et les plus nocives, les plus récentes : alcool, café,
etc.

Actuellement beaucoup s'interrogent : nous sommes nous perdus en chemin
? Comment retrouver la route initiale, celle de mère nature ? Terence
McKenna propose une lecture très moderne, très alternative et
passionnante de l'histoire de l'humanité au travers du prisme des
drogues. Les drogues sont le coeur du livre, mais ne percevez pas la
drogue nocive de l'ère moderne, percevez la drogue qui ouvre les portes
de Jim Morrison, celles des Shamanes, celles qui accompagnent l'humanité
depuis des millénaires. Au travers de ce symbole une culture (neuve)
(ré)-émerge.

Vous devriez lire ce livre si vous pensez vous aussi que nous nous
sommes perdus en chemin. Et le suivant pour formaliser ce grand
changement à venir si on se récupère en route.


## Prospective 2015-2025, Marc Halévy, 2013

![Prospective 2015-2025](/images/2014/03/prospective.jpg)

Les livres de Marc Halévy sont bourrés d'idées, celui là ne déroge pas à
la règle. Je vous le recommanderai chaudement si il n'y avait pas un
*mais*... De nombreuses idées ou postures sont bonnes, mais le style
dérange par son côté volontairement léger (certains m'ont fait le même
reproche sur la [horde agile](/2014/01/mini-livre-la-horde-agile/) ceci
dit...). Certaines affirmations manquent de subtilité : on balance tout,
bébé et l'eau du bain, on accuse tout le monde, tout le monde est dans
l'erreur, cela en devient *trop*. Bref Marc Halévy nous dit que le monde
est complexe mais ses propositions, solutions et démonstrations ne le
sont pas, c'est dommage.

Il y a parfois des positions politiques indéfendables à mes yeux (ou qui
démontrent une méconnaissance de certains sujets pourtant essentiels
(disons en tous cas que certaines positions sont complètement contraires
à certains de mes principes)).

Enfin, dès que l'on plonge dans des lectures connexes on perçoit les
inspirations et lectures de Marc Halévy, mais lui-même ne les cite pas
assez. C'est dommage, cela manque de transparence, il n'aurait rien à y
perdre au contraire.

Pour conclure, un livre bourré d'idées très intéressantes, que
malheureusement la forme et certaines maladresses à mes yeux viennent
entâcher. Le sujet du livre est sur la fin de notre époque et l'entrée
dans une nouvelle ère, indispensable à la survie de notre espèce. La
crise ? vive la crise, c'est le début de la bifurcation.

## Lectures, les liens depuis 2010

-   [Lectures automne/hiver 2014](/2014/03/lectures-automnehiver-2014/)
-   [Fiche de lecture 2013 : entre le cristal & la fumée (Henri
    Atlan)](/2013/12/auto-organisation-et-storytelling/)
-   [Lectures automne 2013](/2013/10/quelques-lectures-recentes-2013/)
-   [Lectures automne/hiver 2013](/2013/04/lectures-automnehiver-2013/)
-   [Lectures printemps/été 2012](/2012/10/lectures-printempsete-2012/)
-   [L'empire des coachs (Gori &
    LeCoz)](/2012/04/lecture-lempire-des-coachs-de-gori-le-coz-2006/)
-   [Quelques bouquins à lire pour faire de
    l'agile (2011)](/2011/12/quelques-bouquins-a-lire-pour-faire-de-lagile/)
-   [Fiche de lecture 2010 : Karmic
    Management](/2010/08/fiche-de-lecture-karmic-management-roachmcnallygordon/)
-   [Fiche de lecture 2010 : Scrum (de Claude
    Aubry)](/2010/08/fiche-de-lecture-scrum-guide-pratique-claude-aubry/)

