---
date: 2016-07-10T00:00:00Z
slug: faciliter-ou-batir
tags: ['developpeur','facilitateur','scrummaster','coach']
title: Faciliter ou bâtir
---

J'observe depuis quelques mois, un peu partout où je me tourne des
développeurs, des bâtisseurs, des gens du code, qui se tournent vers la
facilitation, qui souhaitent devenir *scrummaster*. Cela m'interpelle.
Est-ce une façon de fuir leur propre métier ? Est-ce une réelle vocation
?

## Être facilitateur ce n'est pas une solution de facilité

J'imagine que pour certains c'est une façon de jouer à l'art de la
fuite. Ne se sentant pas à l'aise, pas bien dans leur rôle de codeur, de
développeur, ils lorgnent vers le poste de *scrummaster*, de
facilitateur. Si on regarde bien autour des codeurs c'est une des voies
alternatives qui s'offrent à eux. Comme avant certains devenaient chefs
de projet, la voie la plus aisée, du moins la plus proche, qui leur
permet de changer **complètement** de métier c'est aussi aujourd'hui
celle de *scrummaster*, de facilitateur.

J'imagine que ces personnes désirent changer de métier pour de bonnes
raisons, en tous cas parce qu'elles ne s'y sentent pas bien, dans leur
travail. Si ce n'est pas le cas je les mets en garde : comme avec les
chefs de projet auparavant (...même si le rôle de facilitateur n'a que
peu de chose avoir le rôle de chef de projet), elles risquent le
désenchantement. Et nous aussi malheureusement.

D'abord être facilitateur, *scrummaster*, ce n'est pas plus simple, ni
plus dur, qu'être développeur, qu'être codeur. À ceux qui pensent qu'il
ne s'agit que d'organiser les réunions je prédis un avenir identique à
ceux qui pensent que coder c'est *pisser de la ligne* : pas folichon.
Tout dépend de ce que vous projetez dans votre métier, mais le faire
bien, cela demande toujours une véritable implication. Si donc ce n'est
pas une vocation, une envie, vous ne trouverez pas le repos, et
malheureusement vous risquez de ne pas rendre service aux autres.

Le codeur imagine qu'il est facile de porter une réunion, quand le
facilitateur pense qu'il est facile de se mettre un casque sur les
oreilles et de tranquillement passer la journée devant son code (et
probablement ses pages facebook ou gitlab). Les deux se trompent
probablement.

## Faciliter ce n'est pas reposant

Fin du mois juin, un atelier d'une journée avec un groupe. Nos relations
sont déjà bien établies, et nous approchons de la fin des sessions de
l'année. Je décide de proposer une élection sans candidat pour que
certains s'emparent du rôle de facilitateur (comme toutes élections sans
candidat, nul n'est contraint si il est élu) et prennent ma place pour
la suite. Une personne est nommée, je vais l'accompagner ce jour, elle
me déclare dans l'après-midi : "On est ravi d'être élu, on déchante
quand il faut faciliter".

La neutralité n'est pas reposante. Les interactions constantes ne sont
pas reposantes. C'est passionnant, mais il faut aimer cela. Les sciences
molles sont usantes car il est difficile de quantifier leur résultat, ce
n'est pas une compilation de code qui marche ou pas. Et ce n'est pas
parce que ce n'est pas quantifiable que l'on ne sentira pas un sentiment
d'échec ou une frustration. La neutralité n'est pas sans goût
heureusement.

## Faciliter c'est différent

[Sciences molles, sciences dures](https://fr.wikipedia.org/wiki/Sciences_dures), vieux débat, et
terminologie pas forcément aisée. Certains disent sciences exactes, et
sciences... ? Certains parlent de sciences douces. Qu'importe, mais en
basculant de développeur, codeur, à facilitateur, coach, on change de
monde et de posture. Ce n'est pas plus simple ou plus dur, c'est très
différent. Là aussi il faut savoir s'interroger sur sa nature, préférer
le complexe, l'empirique, au compliqué, l'expertise. Même si au final je
pense que toutes les natures peuvent trouver leur place dans le
coaching, chacun avec son style propre, interrogez vous sur ce point.

## Une promotion ?

J'espère que vous ne voyez pas cela comme une promotion. Changer pour
gagner plus ? D'abord ce n'est pas prouvé que cela vous apporte plus (et
de moins en moins en France où l'on a enfin compris que l'on devait se
payer les grands techniciens), et surtout cela indiquerait que vous
témoignez peu d'intérêt à votre travail puisque votre choix est d'abord
mené par autre chose que par la nature intrinsèque de votre métier.

Facilitateur ce n'est pas non plus un échelon dans la hiérarchie.
D'autant plus que aujourd'hui les facilitateurs, les coachs, ont
tendance à aplatir cette hiérarchie dans leurs démarches (entreprise
libérée, agilité, holacratie, sociocratie, gothamocratie, etc.).

## L'important est de trouver du sens

La première libération commence par soi-même. L'important est de trouver
du sens dans ce que l'on fait. Il faut se respecter, se projeter :
[rechercher un sentiment de contrôle, de progrès, de travailler pour
quelque chose qui nous dépasse, profiter de l'appartenance à une
communauté](/2015/01/modernite-agilite-engage/).

Comme chacun des professeurs de mes enfants, j'espère qu'il est à sa
place par vocation, par envie, par sens, et non pas qu'il a été attiré
par les sirènes d'un emploi à vie dans la fonction publique qu'il a cru
facile.

### L’allégorie du tailleur de pierre

Trois tailleurs de pierre façonnent, quasiment côte à côte, une pierre.

![Tailleur de pierre, Amiens](/images/2016/07/tailleur_pierre_amiens-2637690.jpg)

Le premier tailleur de pierre, assis sur sa chaise, travaille presque
mécaniquement sa pierre et quand on lui demande ce qu’il est en train de
faire, c’est l’air un peu ahuri qu’il répond qu’il taille une pierre.
Non loin de lui, un second tailleur de pierre effectue le même travail,
avec les mêmes outils et la même technique, mais de façon un peu plus
méthodique. Quand on lui demande ce qu’il est en train de faire, il
explique posément qu’il taille une pierre pour construire un mur.
Quelques mètres plus loin, un troisième tailleur de pierre travaille
consciencieusement sa matière première avec un respect quasi religieux.
Il a exactement les mêmes outils et la même technique que les deux
autres tailleurs de pierre mais, ce qui le rend différent, c’est la
délicatesse avec laquelle il taille sa pierre comme s’il s’agissait d’un
diamant. Et quand on lui demande ce qu’il est en train de faire, il
répond dans un large sourire : « je suis en train de construire une
cathédrale ».

Cette allégorie est très connue, mais je repris la formulation de
[Emeline Pasquier](https://contributionlibre.com/2011/12/11/lallegorie-du-tailleur-de-pierre/).

## On peut changer son propre métier

À ceux qui pensent que le sens de leur action est dans la facilitation,
le *scrummastering*, le coaching, je souhaite un avenir radieux. À ceux
qui se demandent si on peut faire les deux (coder d'un côté, faciliter
de l'autre), je réponds la même chose : foncez si c'est cela que vous
aimez, si c'est là où vous trouvez du sens.

À ceux qui fuient une douleur, une frustration dans leur métier,
j'aurais tendance à leur dire réfléchir à ce qu'ils veulent, et
peut-être de penser à travailler différemment d'abord avant de fuir vers
un autre métier. Si c'est pour trouver du sens, elles peuvent
probablement le trouver dans leur métier. Ils peuvent reconsidérer
l'environnement dans lequel ils travaillent (il n'y a pas de pénurie de
travail dans le métier du logiciel, et j'ose imaginer qu'il n'y a pas de
pénurie de travail pour les bâtisseurs de cathédrales quels qu'ils
soient), ils peuvent y trouver du sens. Comment ?

Comment faire quand on est codeur ? développeur ? artisan logiciel me
glissent des copains ? Dans le monde "agile" on devient *craftsman*,
compagnon du code, on bâtit des cathédrales.
[Craftsmanship](https://fr.wikipedia.org/wiki/Software_craftsmanship),
*Craftsman* cela ne veut pas dire s'enfermer dans sa tour d'ivoire cela
veut dire à mes yeux faciliter du point de vue du fabricant, du
constructeur. Donner du sens à chaque geste.

Pour reprendre en main votre métier donnez du sens à vos gestes.
