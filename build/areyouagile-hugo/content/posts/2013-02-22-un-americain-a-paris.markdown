---
date: 2013-02-22T00:00:00Z
slug: un-americain-a-paris
tags: ['leadership','gift']
title: Un américain à Paris
---

La création de [Convergenc.es](http://convergenc.es) s'est faîte assez
naturellement. Oana et moi nous croisions dans les évènements agiles et
nous avions bien compris que nos points de vue étaient similaires.
Autant en profiter pour créer quelque chose qui nous dépasse, et mettre
de côté ce qui pourrait nous opposer.

![Christopher Avery](/images/2013/02/christopher-avery-2013.png)

Et voici un premier *milestone*, **Christopher Avery** nous fait
l'honneur d'accepter de donner une formation sur la **responsabilisation
et l'engagement**, le *leadership gift*, sous nos bannières, le **26
avril 2013, à Paris**. Autant dire, nous sommes fiers, nous avons la
banane.

[L'article décrivant la formation sur Convergenc.es](http://convergenc.es/pages/christopher-avery-26-avril-paris.html)

[L'annonce sur Convergenc.es](http://convergenc.es/pages/christopher-avery.html)

## Pourquoi Christopher Avery ?

Les propos de Christopher Avery sont particulièrement cités ces derniers
temps. Son discours fait mouche. J'ai pu le découvrir lors de ALE 2013,
avec une session de Sergeï Dmitriev et Jurgen de Smet. Ils ont
participés à la formation de Christopher Avery et en sont revenus
manifestement enchantés, du coup ils ont rejoués une courte session
(45mn) pour mettre en valeur le message de Christopher.

C'était l'une des meilleures sessions de ALE 2013. Alexis (Monville), à
mes côtés lors de cette session, m'a dit la même chose (nous avions même
évoqué reprendre cette session à notre compte, cela ne s'est pas fait à
ce jour).

Mais j'ai tellement aimé le concept développé par Christopher que j'en
ai fait une épreuve dans le [héros agile](http://www.jabberwocky.fr/),
celle du Devin. Et j'introduis ce petit exercice lors de mes formations
désormais. J'ai donc hâte d'apprendre *à la source*.

Oana a pu participer à la session de Christopher en Autriche l'année
dernière.

## Qui devrait s'intéresser à cet évènement ?

Coachs, leaders, managers, toutes personnes pour qui les questions de
responsabilisation et d'engagement sont importantes. Attention,
certains, trop rapides dans leurs jugements ou nourris d'*a priori* sur
les américains, s'imaginent qu'il s'agit d'une approche très coercitive.
Il n'en est rien.

Et il n'est nullement nécessaire de connaitre l'agilité pour apprécier
son discours. Comme le *marshmallow challenge*, ce n'est pas une
formation agile, mais l'agile se reconnait grandement dedans.

## Comment nous aider ?

C'est le premier évènement d'importance pour [Convergenc.es](http://convergenc.es) (pas le dernier, on prépare notre
deuxième salve). Comme tout premier évènement on ouvre une porte sur
l'inconnu. Vous souhaitez nous aider, voici plusieurs façons de le faire
:

-   s'inscrire.
-   naturellement faire passer l'information à toute personne
    susceptible d'être intéressée.
-   nous donner du feedback : [par
    ici](http://convergenc.es/pages/nous-contacter.html). Le prix, la
    salle, le sujet, la date, le mode de paiement, tout nous intéresse.
-   nous encourager.
