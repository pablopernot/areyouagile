---
date: 2011-07-05T00:00:00Z
slug: desequilibre-et-conduite-du-changement
tags: ['change','transformation','organisation']
title: Déséquilibre et conduite du changement
---

Quand **Brian Marick** a achevé sa keynote à Madrid (XP2011) il a conclu
par "gardez l'agile étrange" (keep agile weird). Il entendait par là
dire que l'agile était une forme de résistance et qu'il fallait donc lui
garder cette étrangeté vis à vis du monde extérieur, indicateur de sa
bonne santé. Cette phrase a eu un autre écho en moi que j'ai essayé
d'exprimer à **Brian Marick** dans les couloirs juste après son
intervention. En fait cette étrangeté me faisait penser à quelque chose
que je défend et aime depuis toujours : l'**absurde**, et le
**déséquilibre** qu'il engendre. J'ai essayé de lui expliquer que sa
phrase me faisait penser aux **Monty Python**, c'est absurde, c'est
étrange, c'est déséquilibré, le déséquilibre produit une force qui
permet le changement. Vo‍ilà mon mojo. Bon, il a vaguement acquiescé
pour pouvoir rapidement s'éclipser vers le buffet.

![Marx Brothers](/images/2011/07/a_day_at_the_race.jpg)


Il n'empêche je reviens aujourd'hui vers ce concept car l'une de mes
lectures : [dynamique des groupes restreints de Anzieu & Martin (1968)](http://www.amazon.fr/dynamique-groupes-restreints-Didier-Anzieu/dp/2130558879/ref=sr_1_1?s=books&ie=UTF8&qid=1309876004&sr=1-1)
, me donne matière à réflexion.

Si vous déployez l'agile au sein d'une entité vous savez que la conduite
du changement, la réussite du changement, est un facteur clef. Nulle
pérénnité pour votre action agile si le changement n'est pas global. Le
vrai challenge est donc de provoquer un changement durable.Une des
techniques est ainsi de déséquilibrer les habitudes en place. Ce
déséquilibre produit une force dont on se sert pour provoquer le
changement. Il faut que le déséquilibre soit assez fort pour éviter de
revenir dans le positionnement initial. Il ne faut pas que le
déséquilibre soit trop violent, au risque d'être rejeté (et vous avec).

C'est **K. Levin** qui a réalisé des études au milieu du XXème siècle à
ce sujet nous disent **Anzieu & Martin** dans *Dynamique des groupes
restreints*. Les recherches de Levin expliquent que le changement
(social). Levin évoque une marge de voisinage : c'est à dire un espace
dans lequelles des forces modifient le groupe, la société, mais sans
provoquer un changement : une marge de fluctuation. Pour introduire le
changement il faut introduire une force opposée supérieur à la
résistance initiale (dont le but est de ramener l'équilibre au niveau
antérieur), qui fasse franchir cette marge de voisinage. **Levin**
explique : *En décristallisant peu à peu les habitudes par des méthodes
de discussion non directives, jusqu'au point de rupture, de choc, ou une
recristallisation différente peut s'opérer*.

En 1943 **Levin** mène une expérience pour essayer de convaincre
plusieurs groupes de personnes à changer leurs habitudes alimentaires (à
manger des abats). Il utilise plusieurs moyens pour essayer d'opérer le
changement. Premier enseignement : la méthode qui fonctionne le mieux
est celle où l'implication des acteurs est la plus élévée (celle où des
discussions entre les acteurs sont nécessaires) et où les acteurs sont
libres de leur décision. les acteurs doivent se convaincre eux-mêmes ou
sont convaincus par des pairs. Deuxième enseignement : la prise de
décision en groupe amène plus à l'action qu'une prise de décision
individuelle. Il faut donc changer des groupes et pas des personnes.

Je reviens sur les étapes évoquées par **Levin** sur la conduite du
changement : décristallisation, changement, cristallisation. Sans
surprise la phase de déséquilibre apporte avec elle une tension. Deux
forces entre en conflit : la résistance au changement et le
déséquilibre. Comprenez donc que souvent cette tension est nécessaire,
mais qu'il ne faut pas la faire rompre.

Petit aparthé et de façon très grossière : **Bergson, Kant, Freud** ou
autre estiment que le rire provient d'un déséquilibre dans nos forces
internes.

![WC Fields](/images/2011/07/wcfields4.jpg)

On s'attend à quelque chose et soudain autre chose se produit : un homme
digne marche, mais il glisse sur une peau de banane. Dans ce cas plus la
variation entre la dignité et le ridicule de la chute est grande, plus
la force nouvellement disponible le sera, le rire provient de ce surplus
de force, d'énergie. Autre exemple : on s'attend à quelque chose et
surprise autre chose arrive : cet écart génère une énergie qui peut se
déverser dans le rire (selon les situations). C'est souvent le cas de
l'absurde comique (**Monty Python, Marx Brothers, WC Fields** ou autre,
etc.), on s'attend à quelque chose et quelque chose de très différent
arrive (un ministère des démarches stupides par exemple : la distance
entre l'idée d'un ministère "normal" et l'idée d'un dédié aux démarches
stupides : voilà un écart, un gap, un espace). De cet espace néé une
énergie qui se dissipe en rire.

Dans mon cas j'essaye d'utiliser cette théorie ou conception dans mon
travail, je dois générer ce déséquilibre, cet écart entre une réalité,
et celle vers laquelle on doit tendre. La mise en évidence de l'absurde
des situations m'aide : il faut rendre visible ce gap aux yeux de tous
car comme exprimé plus haut : ce n'est pas moi qui vais convaincre, mais
les acteurs qui vont se convaincre (ou pas). Enfin si en plus je peux
utiliser ce déséquilibre pour rendre les choses comiques cela me permet
de faire tomber cette tension lié à la conduite du changement évoqué
plus haut. J'ai bu du petit lait lors de cette discussion avec l'un de
mes clients : l'un de ses projets n'avançait pas du tout. Je viens aux
nouvelles :

-  Le burndown indique que le projet fait du surplace depuis un bon
moment, que se passe-t-il ?

- Nous avons de nombreuses autres priorités, et ce projet est la
dernière de nos priorités.

- C'est tout à fait recevable, le mieux ne serait-il pas de suspendre
ce projet le temps que les choses prioritaires soient achevées ?

- Ah non, il est impensable de ne pas travailler sur ce projet !

Cela m'amène à une deuxième discussion lancée par [Oana J](http://oanasagile.blogspot.com/). Faut-il des coach agiles "héros",
"bulldozer" (on m'a dit ça un jour...), dans un mode très consultant, ou
des coach agiles "fée" (faerie) ou "djinn" (si fée porte atteinte à
votre masculinité), sous entendu qui murmurent à l'oreille des gens et
laissent bourgeonner leurs idées pour qu'elles réapparaissent comme par
magie (d'où les fées). On se rêve tous "djinn,fée", mais dans la réalité
on est souvent "héros" : le temps, les délais, et aussi souvent notre
nature propre nous y contraignent.

Pour résumer : un déséquilibre pour apporter le changement. Attention
ce déséquilibre va apporter son lot de tensions, elles peuvent être
amoindries par un effet comique (je dis cela sans rire). C'est un groupe
que vous allez changer, pas des individus. Le changement aura lieu si le
groupe se convainc lui même ou est convaincu par ses pairs. A vous
d'amener cette réflexion, là encore la mise à nu de l'absurde d'une
situation, de l'écart amené par le changement, est un moyen de
sensibiliser les acteurs, de générer, d'initier cette force vers le
changement.

Soit votre montre est arrêtée soit cet article est fini\*.

\* adaptation d'une phrase de **Groucho Marx** dans **A Day At The
Race**, je m'essaye au non-sens...
