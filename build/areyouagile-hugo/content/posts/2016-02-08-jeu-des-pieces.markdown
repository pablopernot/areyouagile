---
date: 2016-02-08T00:00:00Z
tags: ['atelier','workshop','piece','seriousgame']
title: Jeu des pièces
---

Mon actualité m'a fait rajeunir un peu ma documentation sur ce petit jeu
agile, le jeu des pièces, ou le "coin toss". Simple mais très efficace,
prenez un gros quart d'heure.

Et comme le veut cet ancien proverbe chinois : *Dis-moi et j'oublierai.
Montre-moi et je me souviendrai. Implique-moi et je comprendrai.* --
Proverbe chinois

L'objectif du jeu va être de démontrer quelques bénéfices fondamentaux
de l'agile : **un focus sur la valeur en maximisant celle-ci (maximiser
la valeur, minimiser l'effort), et délivrant celle-ci au plus tôt ("time
to market") tout en intégrant une capacité au changement**.

## Règles

4 participants, chacun représente les phases d'un projet classique
(c'est à dire à l'anciennce...) : disons par exemple : définition,
spécification, réalisation, recette. Une personne chronomètre : le temps
de la première phase, et le temps total. Un tas de pièces de monnaie (un
peu plus d'un euro en petites pièces de 1 cts à 20 cts). "Retourner une
pièce" est la métaphore de la réalisation d'une activité autour d'une
fonctionnalité (sa valeur est celle de la pièce). Les pièces, après
avoir été retournées, passent dans toutes les phases du projet jusqu'à
la fin de celui-ci.

## Premier tour : le projet classique

Sur le projet classique l'équipe va mettre autour de 1mn, 1mn30 pour
faire passer toutes les pièces, et va avoisiner disons 1 euros et 15 cts
(au hasard). Sur un projet classique les pièces vont d'abord toutes être
retournées par phase avant d'être passées à la phase suivante (on fini
l'intégralité de la définition, avant de passer aux spécifications).
C'est la règle du projet classique (même si certains pensent que c'est
une caricature, on le rencontre encore bien souvent).

Affichez sur le tableau : le temps de la première personne, le temps
global, la somme rapportée.

**Observations**

Certaines personnes ne travaillent pas pendant que d'autres travaillent.
Dans la vraie vie on vous dira qu'elles travaillent sur "autre chose".
Problème : dans la vraie vie il peut rapidement y avoir un effet goulot
Problème : on ne peut pas basculer au delà de deux activités sans perdre
en productivité, donc le "travailler sur autre chose" a très vite des
limites.

## Deuxième tour : le projet agile

Bascule agile : tout le monde travaille en même temps (dès que l'on a
retourné une pièce on peut la passer à la phase suivante) pour avoir des
choses finies (en bout de chaine au plus vite) car on veut de la valeur,
et la valeur (et l'apprentissage) n'est qu'avec des choses finies.
Interrogez les gens par avance : "Est-ce que cela sera plus court ? " Si
certains disent oui, appuyez "Vraiment plus court ? ... Bon on verra !".
En fait cela sera 4x plus court (à mettre en perspective avec la perte
de productivité au délà de 2 activités) avec le même nombre de pièces,
et la même somme.

On va donc réussir à faire passer toutes les pièces en quatre fois moins
de temps (et pour la même valeur), disons 1,15 euros en 34 secondes au
lieu de 1mn30.

**Observations**

On notera le sentiment de fatigue lié à cette véritable interaction. Il
faut y prendre garde en agile ("sustainable pace").

On demande toujours d'"aller plus vite". Or il est souvent vain d'aller
plus vite, d'accélérer les cadences ou d'augmenter la somme de travail.
Pour aller plus vite : il faut changer le système, penser différemment.
C'est ce que nous venons de faire.

On passe de 1mn30 à 34 secondes, mais surtout la première livraison avec
de la valeur arrive vers 14 secondes (en tous cas bien plus tôt, dès le
début, si ce n'est finalement pas la valeur que l'on souhaitait, ou pas
la valeur espérée, on peut s'arrêter à moindre frais ou changer son
fusil d'épaule).

## Troisième tour : petit passage chez Kanban

Si on note un goulot lié au nouveau rythme (un participant plus fatigué
? plus maladroit ? moins habile ? pas en forme aujourd'hui ? peu importe
c'est le symbole que les phases ne se ressemblent pas et demandent
différentes attentions), bref si on note des goulots et des empilements
par endroit on pourra ajouter un élément de Kanban (une des approches
agiles) : on place un post-it ou un morceau de feuille entre chaque
phase : c'est le point de passage, le ticket d'échange, on peut placer
une ou deux pièces retournées dessus que si la feuille est vide, sinon
on doit donc attendre que la phase suivante soit dans le rythme : cela
régule le flux au "juste à temps".

Je recommence : chaque participant ne peux passer les pièces retournées
à son voisin, la phase suivante, que si celui-ci a libéré l'espace sur
la feuille en s'occupant des pièces précédentes.

**Observations**

Avec cette régulation du "juste à temps", pas de stock, moins de pièce
dans le flux, si besoin on peut aisément changer son fusil d'épaule. Et
on ne va pas moins vite.

Vous pouvez expliquer la variations entre les différentes étapes par le
fait que les forces en présence dans la vraie vie ne sont pas les mêmes.

## Quatrième tour : le vrai projet agile

En fait on n'a jamais le temps dans la vraie vie de tout faire. On doit
se focaliser sur la valeur. On doit prioriser celle-ci. On va se fixer
20 secondes max, et essayer d'avoir le maximum de valeur en 20 secondes.
La personne qui initie le flux peut donc choisir les pièces. Elle n'a
pas le droit de les classer, d'anticiper, mais c'est inutile, comme dans
la vraie vie elle sait où se trouve les grosses poches de valeurs. Enfin
à dix secondes, on enlève toutes les pièces et les remplace par d'autres
: "le marché vient de changer !". Les nouvelles pièces introduisent de
nouvelles grosses pièces, comme dans la vraie vie : de nouvelles poches
de valeur sont apparues.

**Observations**

En 20 secondes on réussit à avoir 97cts (par exemple), en regard du
point de départ de 1m30 et 1,15cts. Dès la 14ème seconde on obtient de
la valeur, tout en ayant absorbé un changement du marché.

Vous pouvez annoncer 25 secondes, et finalement réduire à 20 secondes
pour encore plus sentir la réalité...

## Est-ce que c'est du Fordisme ???

Le côté rigoureux et répétitif peut rapidement être assimilé au
Fordisme, au Taylorisme. Quand une dynamique Scrum est en place
l'impression peut être la même ! En tous cas en apparence, car dans le
fond les personnes impliquées décident de leur façon de travailler,
elles sont autonomes et responsabilisés, c'est fondamental.

## Ajout : La devinette de la quatrième colonne

\[Edition 26 janvier 2017\]

![Pièces devinette](/images/2017/01/pieces-devinette.jpg)

Depuis quelques mois j'ajoute une information : la quatrième colonne. Je
me garde de la place pour une quatrième colonne que j'ajoute à un moment
sous forme de devinette : et si il y a avait une quatrième colonne ?
Qu'est ce qui serait intéressant de mesurer ? Faîtes le quand vous le
sentez mais généralement après le deuxième tour (les pièces deux par
deux) cela fait sens.

La réponse c'est le temps entre le début et la première mise en
production, la première livraison de valeur en production comme dit
[Alexandre](https://twitter.com/alexandrethib). Souvent entre le premier
passage et le second la différence est frappante. Cela permet de parler
*Time to market*, coût du délai, etc.

Un petit gribouillage rapide (cliquez dessus).

## D'autres références du jeu

-   <http://blog.crisp.se/2008/09/08/mattiasskarin/1220882915232>
-   <http://www.dhavalpanchal.com/coin-toss-exercise/>
-   <http://tastycupcakes.org/2013/05/the-penny-game/>

