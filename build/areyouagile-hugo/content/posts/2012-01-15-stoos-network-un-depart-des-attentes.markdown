---
date: 2012-01-15T00:00:00Z
slug: stoos-network-un-depart-des-attentes
tags: ['conference']
title: Stoos network, un départ, des attentes
---

Début janvier 2012, un groupe de personnes s'est réuni en Suisse, à
Stoos, dans le but de renouveler la façon de diriger (*management*) de
nos compagnies et organisations, faisant le constat que l'état des lieux
n'était pas reluisant. Voici le communiqué en français qu'il nous
délivre (traduction commune de plusieurs personnes), puis mes
commentaires sous ce texte.

Au passage et avant ce texte, nous avons traduit celui-ci par le biais
d'un google doc, je rappelle que c'est vraiment un outil très utile pour
une manipulation simultanée en commun. J'ai cependant découvert que
depuis le début de la traduction celle-ci n'avait pas cessée de changer,
de se transformer, d'évoluer. Et au bout d'un moment, pas forcément en
mieux. Une sorte d'entropie naturelle... les microchangements on fait
perdre du sens au texte (à mes yeux). Je prends donc le parti de vous
proposer le texte de la révision du 9 janvier à 22h30 (à l'heure où
j'écris ces lignes, il y a eu 8 changements depuis...).

## Le réseau Stoos

*Aujourd’hui, si on réfléchi à la manière dont sont gérées les
organisations, on constate que la situation n’est vraiment pas
brillante. On y observe trop souvent des raisonnements simplistes
appliqués de façon mécanique, les entreprises étant bien plus
intéressées par le prix de leurs actions que par le bonheur de leurs
clients, et des employés - ceux qui possèdent la véritable connaissance
du métier - dont les préconisations sont généralement ignorées par leurs
chefs. L’actuelle crise économique n’est que le résultat et le reflet de
cet état des lieux : les inégalités s’accroissent, les faillites se
multiplient, et partout la désillusion se répand.*

*Il doit y avoir une meilleure façon de faire.*

*En janvier 2012, nous - un groupe de 21 personnes aux profils variés
dont des cadres supérieurs, des managers, des business strategists, des
universitaires, et des acteurs du développement agile et lean, en
provenance de 4 continents - nous sommes réunis à Stoos, en Suisse. Nous
pensons avoir trouvé des pistes, une base commune pour obtenir cette
meilleure façon de faire. Par exemple, nous croyons que les
organisations devraient se transformer en un réseau apprenant composé de
personnes amenées à créer de la valeur et que le rôle des leaders
devrait être de soutenir cet organisme vivant plutôt que de le gérer
comme une machine.Plus important, nous nous engageons à continuer notre
travail, notre réflexion, ici et en ligne. Un défi de cet importance
nécessite beaucoup de coeur et d’idées. Nous aimerions entendre votre
voix et connaître votre expérience. Aidez cette réflexion à avancer en
nous rejoignant sur le groupe
[Linkedin](http://www.linkedin.com/groups/Stoos-Network-4243114) et sur
twitter avec le hashtag \#stoos.*

*Entamons notre mutation avant qu’il ne soit trop tard.*

\[ [Le lien vers le document mutant et
officiel](https://docs.google.com/document/d/1-1DN1eL7KLbRmRskcSmesRwiNMhMWg-Vtq6aBjSYwAI/edit?pli=1)\]

C'est donc le départ d'une initiative passionnante et probablement
salutaire. En tant que membre fondateur d'une petite société de conseil
(9 membres à ce jour) : [smartview](http://www.smartview.fr), j'essaye
depuis le début de l'aventure d'appliquer l'idée générale des valeurs
défendues par le [stoos network](http://www.stoosnetwork.org/), car
elles sont aussi les miennes. J'y arrive, mais pas toujours. Je
progresse, comme tout le monde. Ca aussi, comme d'habitude, c'est
passionnant : devoir confronter ses idées à la réalité.

Les idées lancées par Stoos ne me paraissent pas si originales ou
novatrices que cela (je suis gonflé, je dois juste ne pas avoir assez de
recul ou de neurones). Mais peu importe, là n'est pas l'important. Pour
une fois elles sont "officialisées" et un mouvement et une réflexion de
plus grande ampleur ont démarrés. Je vous encourage à les rejoindre.

Seul bémol, le groupe à l'origine du Stoos network semble attendre suite
à la dynamique qu'il a généré au sein du "réseau" (de nombreux inscrits
au groupe linkedin, des tweets en pagaille, etc.) qu'il produise
lui-même des résultats. En accord en cela avec la grande idée que c'est
ce réseau organique qui a le vrai pouvoir et qui peut générer de la
valeur. Mais cette attente de création *ex-nihilo* (à partir de rien)
parait quelque peu artificielle. Nous manquons d'appui concret, et nous
aurions envie que justement ces personnes présentes à Stoos nous
montrent comment avancer. On pourrait croire que nous nous plaçons
toujours dans l'ancien modèle attendant "du haut" les bonnes
indications. Mais pas du tout, l'impression donnée est plutôt : nous
avons réfléchis, maintenant les gueux, agissez... J'attends aussi de ces
personnes qu'ils agissent comme des mentors, par l'exemple. Ce n'est
qu'un bémol, et je suppose qu'il sera vite balayé par des actions
concrêtes à venir. Si je peux y contribuer je le ferai, si vous le
pouvez, n'hésitez pas.

Le  [stoos network](http://www.stoosnetwork.org/) s'étoffe au fil des
jours. Consultez les idées déjà émises, participez.

 
