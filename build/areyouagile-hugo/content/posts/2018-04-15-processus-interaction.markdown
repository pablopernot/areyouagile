---
date: 2018-04-22
slug: processus-interaction-cadre-sens
tags: [processus, cadre, interaction, sens]
title: "Retour à la langue : processus, interaction, cadre et sens"
---


Assemblons quelques briques qui mènent à cet article. D'abord grâce à [France Culture](https://www.franceculture.fr/emissions/les-chemins-de-la-philosophie/parlez-moi-lacan-14-place-au-maitre), je viens de me replonger dans Lacan. Dire que je comprends est un pas que je ne franchirai pas. Mais au travers de ces petits podcasts qui me permettent de me reposer la question d'une lecture soutenue ou non de Lacan, j'ai été frappé par ce que j'aime : l'importance du langage. Et par l'idée selon laquelle une analyse ne pouvait pas être menée aussi bien en anglais, qu'en français[^lacan]... Le langage utilisé --- la langue --- ne permettant pas le même type d'appropriation, d'expression, d'exposition, etc. 

[^lacan]: Je vous laisse plonger dans le podcast si vous voulez à ce sujet : [France Culture](https://www.franceculture.fr/emissions/les-chemins-de-la-philosophie/parlez-moi-lacan-14-place-au-maitre), ou entrer dans le sujet par [cet article trouvé sur le web](https://www.cairn.info/revue-essaim-2012-2-page-75.htm). 

Ensuite dans mon quotidien je vois fleurir des références au BDD, *Behaviour Driven Development*. J'ai pu m'en servir pour essayer de "normaliser" les interactions entre les *product owner*, *product managers* et les/leurs équipes il y a quelques années. Je n'y reviendrai pas. Aujourd'hui **tous** les exemples de BDD que je vois fleurir me hérissent le poil. Ils m'apparaissent comme un processus qui fait semblant d'être une interaction. Aversion accentuée par un regain chez moi de cette définition des *user stories* défendue par son inventeur même [Ron Jeffries](https://ronjeffries.com/)[^jeffries]; définition qui veut qu'une *user story*, une histoire utilisateur, se réalise par une conversation autour d'une histoire et vraiment, mais vraiment, très très peu d'écrit. Selon lui (et je le suis) si votre *user story* dépasse trois ou quatre lignes vous êtes hors sujet. La conversation et les interactions, elles, peuvent contenir toute la richesse que vous souhaitez. L'effort n'est pas d'écrire, mais de décrire au travers d'une conversation.     

[^jeffries]: Il vient de pondre au passage un très bel article sur agile : https://ronjeffries.com/articles/018-01ff/agile-riff/

C'est toute l'idée de cette évolution dans un monde complexe, l'idée d'une constante amélioration, d'une constante introspection, qui se nourrissent de constants apprentissages (basé par exemple sur le feedback). C'est pourquoi ce monde actuel se développe sur des interactions entre individus plutôt que l'application de processus. C'est l'une des clefs, probablement la plus importante, du mouvement agile. 

Et ainsi je m'interroge. Serait-ce que nous devons nous réapproprier ces mots, ce langage, pour mieux le comprendre ? Processus, interaction ? Se les réapproprier, les clarifier expressément pour arrêter les faux-semblants et les dérives ? Est-ce possible ? Essayons. 

### Processus 

    Enchaînement ordonné de faits ou de phénomènes, répondant à un certain schéma et aboutissant à quelque chose : Le processus d'une crise.  
    
    Suite continue d'opérations, d'actions constituant la manière de faire, de fabriquer quelque chose : Les processus de fabrication doivent être revus.  
    
    Manière que quelqu'un, un groupe, a de se comporter en vue d'un résultat particulier répondant à un schéma précis : Selon le processus habituel, il s'arrangera pour se faire excuser.

Dixit [Le Larousse](http://www.larousse.fr/dictionnaires/francais/processus/64066)

Voici ce que retient mon cerveau en mélangeant ces phrases du dictionnaire : **"enchaînement ordonné d'actions selon un schéma précis"**. 

### Interaction

    Réaction réciproque de deux phénomènes l'un sur l'autre.

    Action réciproque qu'exercent l'un sur l'autre deux ou plusieurs systèmes physiques.

Dixit [Le Larousse](http://www.larousse.fr/dictionnaires/francais/interaction/43595?q=interaction#43518)

J'en retiens : **"actions ou réactions entre plusieurs systèmes"**. 

C'est facile, l'un est déterminé, le processus, l'autre ne l'est pas, l'interaction. 

Dans un monde mouvant la prédétermination est le mauvais choix. Elle n'implique pas l'adaptation. 

D'où plutôt l'orientation vers une stratégie d'actions et de réactions non déterminées mais *contrôlées*. 

Contrôlées ?  Oui, une chaine sans fin d'actions et de réactions que l'on observerait impuissant n'aurait pas d'intérêt. Pas d'intérêt dans la grande grande majorité des contextes où les acteurs de l'agilité évoluent. **On a besoin de laisser les actions et réactions aboutir sans quelles soient prédéterminées, mais on a aussi besoin de prédictibilité. Ceci est opéré par un cadre qui introduit de la prédictibilité en définissant un périmètre**.

Entre en jeu le "cadre". 

### Cadre

    Limites d'un espace ; l'espace ainsi cerné : Une maison avec son cadre de verdure.

    Entourage, milieu, contexte : Habiter dans un cadre agréable.

    Ce qui borne, limite l'action de quelqu'un, de quelque chose ; ce qui circonscrit un sujet : Sortir du cadre de ses fonctions.

Dixit [Le Larousse](http://www.larousse.fr/dictionnaires/francais/cadre/12054?q=cadre#11899)

**Le cadre est une limite** (bonjour Kanban). Une limite sert à augmenter la prédictibilité car justement elle limite les possibilités, les options, des actions et réactions possibles. 

### Frontières et mutations

Alors voilà les questions que je me pose. 

#### A quel moment l'interaction risque de devenir un processus ? 

Si elle devient trop prédéterminée. Si les actions/réactions sont prédéterminées elles deviennent des processus et ne sont plus valables dans un monde mouvant, complexe. Deux façons de perdre la dynamique de ces actions/réactions : 

  * Le cadre est trop contraint, la combinaison actions/réactions devient trop pauvre, prédéterminée.   
  * On fige l'action/réaction car on souhaite la répéter. C'est devenu un processus.      
  * On n'a pas la liberté de changer, d'adapter le processus. 
  
C'est ce que je reproche --- par exemple --- dans mes observations a beaucoup de d'exemples de BDD[^bdd] (*Behavior Driven Development*). On a tellement outillé l'idée qu'elle en a perdu son espace, et il n'y a plus assez de possibilité de d'action et de réaction. La forme a pris le dessus sur le fond. On s'applique bien à respecter l'outil, la forme (*Given When Then*) on en oubli le sens, le fond. Le signifiant oblitère le signifié. Et du coup il n'y a plus d'intelligence : d'apprentissage, de feedback, de réelles interactions.  

[^bdd]: Je prends le bdd en exemple mais il aurait pu s'agir de bien autres choses, tant d'interactions sont transformées trop rapidement en processus.

Entre en jeu le "sens" : 

### Sens 

    Raison d'être, valeur, finalité de quelque chose, ce qui le justifie et l'explique : Donner un sens à son existence.

mais aussi 

    Direction dans laquelle se fait un mouvement : Traverser la France dans le sens nord-sud.

Dixit [Le Larousse](http://www.larousse.fr/dictionnaires/francais/sens/72087) et toujours [Le Larousse](http://www.larousse.fr/dictionnaires/francais/sens/72089)   

C'est donc je dirais **"une direction vers une finalité"**.  

#### Comment peut-on transformer un processus en interaction ? 

Comment leur faire perdre leur prédétermination ? On peut "détendre" le processus, insérer de l'espace pour produire ou diversifier les interactions. Pour reprendre les mots : quand le schéma *n'est plus précis*, quand l'enchainement *n'est plus ordonné*. 
Donc tout n'est pas décrit, des zones de liberté, de décisions sont présentes. Idéalement seul le point d'entrée (pourquoi) et le point de sortie (résultat espéré) sont décrits.

Pour qu'un processus devienne une interaction il faut pouvoir le transgresser, le tordre, le changer, pour cela il faut de l'espace. Pour avoir de l'espace il faut connaissance d'une limite, d'un cadre. Point d'entrée, point de sortie. Durée. Par exemple.  

## Le temps

La limite temps est souvent importante car depuis toujours le temps c'est de l'espace[^espace]. Si la finalité est lointaine : il y a beaucoup d'espace, si la finalité est proche ("le mois prochain") l'espace est resserré. Mais trop d'espace c'est aussi trop de temps entre l'action et la réaction et donc peu d'interaction. On privilégie un temps court entre l'action et la réaction pour augmenter les interactions. 

[^espace]: Du livre de J. Guilaine : ‘Le seconde naissance de l’homme’ (O. Jacob) : "Au paléolithique archaïque, aux alentours de 1,9 million d’années (homo ergaster et/ou homo erectus), l’analyse de la documentation fournie par plusieurs sites africains montre une gestion des matières premières fondée sur un certain rapport à l’espace (et donc au temps). A Oldowaï, les matériaux bruts nécessaires à la taille ont été apportés de sources distantes de 3 km. Des gîtes plus lointains, entre 9 et 13 km, on n’a ramené que des outils finis, après avoir laissé sur place blocs et déchets. Ces indices, parmi les plus anciens observés, donnent une première idée de l’espace prospecté et, de ce fait, du temps mis à le parcourir. L’histoire des temps paléolithiques, dans leur extrême durée, est précisément caractérisée par une maîtrise de l’espace toujours plus élargie, par des déplacements sans cesse portés vers des frontières plus lointaines. Ces pérégrinations impliquent donc une maîtrise minimale du temps". -- Tiré de l'article de blog : http://www.philipmaulion.com/2016/02/la-ou-pense-homo-sapiens.html

Du coup il me devient plus facile d'énoncer certaines choses : 

## Apprentissage 

Deux types d'apprentissage ou d'appropriation de cette façon agile de fonctionner, d'être: 

### Par imitation des gestes 

C'est à dire en imitant des processus ("enchaînement ordonné d'actions selon un schéma précis"). C'est ce que l'on appelle le ["SHU"](/2015/09/paradoxes-des-transformations-agiles/). **Puis savoir s'en détacher** [(HA & RI)]](/2015/09/paradoxes-des-transformations-agiles/). 

#### Les difficultés 

D'abord C'est compliqué de dire que l'on applique à la lettre mais ...temporairement, pour ensuite s'en détacher. On ne sait pas communiquer sur le côté temporaire de l'application à la lettre.

Ensuite parfois on applique sans savoir pourquoi cela a été pensé, et ainsi jamais il ne vient à l'idée de s'en détacher car on ne sait pas pourquoi il est fait (ce processus). C'est le fameux *Cargo Cult* : quand non seulement on répète un processus ("enchaînement ordonné d'actions selon un schéma précis"), sans plus savoir pourquoi il a été fait ainsi.  

Enfin des fois c'est plus facile de dire que l'on imite, que l'on applique, mais pas vraiment car "on est différent". Ainsi on se détache immédiatement sans même avoir perçu le moindre enseignement, le moindre apprentissage. On se détache sans savoir de quoi, ni pourquoi. C'est une illusion totale. 

### Par émergence : sans processus mais en posant un cadre. 

C'est à dire en posant un cadre et du sens (des limites et une direction). Avoir très régulièrement des observations pour apprendre et faire émerger. 

#### Les difficultés

D'abord ce n'est pas si simple de décrire un cadre et du sens. Il faut sans cesse répéter, clarifier, trouver des façons variées de les énoncer pour correspondre aux personnes. De plus ce cadre et ce sens sont mouvants. Le monde n'étant pas figé c'est normal. Cela ajoute à la difficulté de description.  

Ensuite on se fait très vite rattraper par notre volonté de répéter les choses, de transformer ces interactions en processus, en règles, en protocoles. C'est plus facile, c'est plus reposant. Un incessant réglage. Un incessant ballet entre processus et interaction.

Enfin on imagine perdre un temps fou. Inutile d'apprendre à nouveau à faire du feu par émergence quand d'autres "savent". C'est des fois vrai, des fois non. Parfois le contexte est le même, parfois il semble le même mais est très différent. Parfois en imitant on n'intègre pas vraiment le sens. Parfois c'est dommage de ne pas gagner ce temps. 

## Pour conclure

Je reviens au langage. Processus : **"enchaînement ordonné d'actions selon un schéma précis"**. Interactions : **"actions ou réactions entre plusieurs systèmes"**. Processus est très linaire, unidirectionnel. L'interaction est un échange, bi-directionnel. Je vais chercher les verbes adéquats. 

Je donne du sens (c'est un échange entre systèmes), je donne un cadre (c'est un échange entre systèmes). Suivre un processus. C'est linaire. Appliquer le BDD. C'est linéaire. Essayer le BDD (réactions possibles). S'inspirer du BDD (échange entre systèmes).  

Le langage n'est pas innocent. On le savait depuis "faire de l'agile" ou "être agile". Maintenant observez et essayez de trouver processus, interactions, cadre et sens dans votre écosystème. Idéalement transformez les processus en interactions et pas l'inverse. 

p.s. (Cet article est aussi un écho à mes réflexions sur le questionnement : [entrainement aux questionnements](/2018/04/entrainement-questionnements/)
