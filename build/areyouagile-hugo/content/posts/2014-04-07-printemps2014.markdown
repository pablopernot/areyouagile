---
date: 2014-04-07T00:00:00Z
slug: printemps-2014-on-sy-croise
tags: ['conference']
title: Printemps 2014, on s'y croise ?
---

Le printemps s'annonce en avance, et je déguste les rayons du soleil en
filant en TGV vers le sud, avec de belles chansons dans les oreilles. Je
vous **invite** à me croiser dans les différents évènements que je
décris plus bas, pour avoir une conversation. Ca me fait plaisir de
croiser de nouvelles têtes, de nouvelles idées, de nouvelles façon
d'être. Dans l'ordre chronologique :

## Scrumday 2014

Le [Scrumday](http://www.scrumday.fr/), c'est demain ! enfin jeudi &
vendredi cette semaine, le 10 & 11 avril. J'y participe en tant
qu'orateur, avec Oana, sur le thème des [sessions que nous avions
organisé](/2013/10/deux-amis/) avec Dan Mezick. J'appelle cela la
**conduite du changement agile**. Comment amener le changement de façon
agile, une sorte de **méta-agilité** comme l'avait si bien dit un
participant lors de la présentation de cette session à Agile Tunisie
2013. Evidemment cela passe par l'*invitation*, le *storytelling*, le
*forum ouvert* et des coachs pas trop présents !

Pour ce Scrumday on s'est aussi donné rendez-vous avec
[Claude](http://www.aubryconseil.com/) pour creuser nos supports
[Peetic](/2012/11/peetic/) lors de l'OpenSpace.

## Boulegan de l'Ostal

Bon là, l'agilité, c'est se plonger dans une grande [rencontre hippie
dans les Cévennes](http://www.festival-boulegan.com/) à jouer de
l'irlandais, du *old time*, et autres musiques que l'on ne trouve jamais
sur les ondes. J'**invite** quiconque qui souhaite m'y rejoindre le 19
avril. Pareil, l'idée est simplement de converser (et de jouer de la
musique, j'y amène mon banjo) ; on pourrait dire que c'est notre petit
**panier repas agile 2014**. Si il pleut c'est le Vietnam, donc
abandonnez sauf si vous voulez vraiment plonger au coeur de la tradition
; au soleil, vous déambulerez entre les groupes de musique, les danseurs
et les luthiers.

## Sudweb 2014

Fin mai j'irai marauder dans les sessions de
[Sudweb](http://sudweb.fr/2014/), essayer de grapiller quelques paroles
riches et surtout revoir les copains chers, et avec un peu de chance :
enfin lancer l'organisation avec
[Nathalie](https://twitter.com/nrosenberg) de notre *conférence sur
l'émotion*. A Sudweb tout est possible, vous êtes prévenu. C'est le 16 &
17 mai.

## Agile France 2014

Je ne sais pas encore si ma session est retenue, confirmée, mais
j'espère présenter dans le magnifique lieu de [Agile
France](http://2014.conference-agile.fr/), une première session sur
"penser son organisation". Je vois beaucoup de mes camarades s'orienter
vers le coaching, c'est à dire une relation privilégiée avec une
personne. Je n'ai pas cette prétention, je trouve cela trop risqué. Je
me positionne plutôt sur du management organisationnel, et comme vous
avez pu le lire, de plus en plus, je pense que l'[organisation physique
et spatiale](/2014/02/penser-son-organisation-sur-le-terrain/) joue un
rôle sans commune mesure avec l'idée que l'on s'en fait généralement.
J'espère donc bien développer, lors de cette Agile France, le 22 & 23
mai, ces [idées sur
l'organisation](/2014/01/penser-son-organisation-introduction/) :
thermodynamique, théorie de Thom, sociocratie, organisation physique &
spatiale, cristal, termitière, etc.

## Rencontres Old Time 2014

Enfin, comme tous les ans, début juin cette fois-ci, les rencontres old
time. Il s'agit de *craftmusicianship*. On se rencontre, on partage, on
mélange, on apprend, une grande session de *music retreat* dans les deux
sens du terme *retreat*. Encore un univers alternatif qui me nourrit.
Vous y êtes les bienvenus, il faut me prévenir par mail si vous êtes
intéressés.
