---
date: 2014-02-09T00:00:00Z
slug: lean-startup-christophe-monnier
tags: ['leanstartup']
title: Lean Startup - Christophe Monnier
---

Une fois n'est pas coutume, je vous présente les slides d'un autre : une
rapide introduction à *Lean Startup* par Christophe Monnier. En fait
Christophe est mon associé au sein de
[SmartView](http://www.smartview.fr). *Lean Startup* est un sujet clef,
passionnant. Et je ne suis pas mécontent que Christophe s'en soit emparé
autant. Je me plais à penser que j'y suis un peu pour quelque chose. On
parle beaucoup agilité entre nous, sous forme de boutades, de vrais
débats, de pieds de nez, mais dans tous les cas on se challenge, se
remet en question. On échange particulièrement ... des livres (comme la
pratique de *Culture Game* de Dan Mezick), je crois, depuis bien
longtemps, à cela, c'est en partie comme cela que je pense progresser.

![Lean Startup](/images/2014/02/leanstartup.jpg)

Et puis au sein de [SmartView](http://www.smartview.fr) on expérimente :
on a essayé de supprimer les primes objectivées (lisez "Drive" de Dan
Pink). Essayé car tous les collègues n'ont pas nécessairement voulus. On
a aussi supprimé les congés (le procotole est en test en ce moment).
Cela veut dire quoi ? Que l'on ne compte plus les congés, les gens
prennent ce qu'ils veulent tant que [SmartView](http://www.smartview.fr)
marche bien. On essaye des rétrospectives régulièrement (là on n'est pas
assez régulier), des *business model canvas* pour nos offres, etc.

Christophe est par ailleurs affilié à des groupements de *business
angels*, il accompagne ces groupements dans la sélection de *start up*,
pas étonnant donc qu'il ait complétement accroché au *Lean Startup*.

<iframe src="http://www.slideshare.net/slideshow/embed_code/30552618" width="427" height="356" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px 1px 0; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="https://www.slideshare.net/cmonnier/lean-startup-synthse-smartview-jan-2014" title="Lean startup - synthèse SmartView - Jan 2014" target="_blank">Lean startup - synthèse SmartView - Jan 2014</a> </strong> from <strong><a href="http://www.slideshare.net/cmonnier" target="_blank">Christophe Monnier</a></strong> </div>

