---
date: 2012-05-10T00:00:00Z
slug: quand-jen-vois-je-sais-que-cen-est
tags: ['porno','agile']
title: Quand j'en vois, je sais que ç'en est
---

Ce petit article fait écho à une définition de l'agile donnée par
@nrosenberg lors d'un apérifif (web, agile, autre, ? je ne sais plus,
j'ai l'impression que pour vaincre l'isolement les toulousains boivent
tous les soirs).

![Potter Stewart](/images/2012/05/300x390xpotter_stewart_portrait_cropped.1.jpg.pagespeed.ic_.S2A3tvBk0M-230x300.jpg)

Cette définition m'est venue aux oreilles et elle m'a beaucoup plu. De
quoi s'agit-il ? Un gouverneur de l'Ohio, [Potter Stewart](http://www.oyez.org/justices/potter_stewart), avait lancée une
croisade contre l'industrie pornographique. Mais lors des procès qu'il
avait engagé quand on lui a demandé comment il définissait la
pornographie il répondit : "Quand j'en vois, je sais que ç'en est".

C'est une définition très intuitive, très émotionnelle, qui se définit
par une rupture. A y réfléchir si je devais donner une définition rapide
de l'agile, ou du moins répondre à la question : "est-ce que nous sommes
agiles ?", cette réponse me satisferait assez.

## Intuition & rupture

Il faut bien percevoir que l'agile est en rupture complète avec la façon
dont on a envisagé les projets, le management, depuis plus de 20 ans
(ok, je ne généralise pas totalement). Le changement, c'est l'agile. Et
c'est une nouveauté fracassante, choquante donc pour certaines
personnes. Donc oui à la question "est-ce que nous sommes agiles ?", on
devrait pouvoir s'interroger facilement : "Quand j'en vois, je sais que
ç'en est (ou pas)" d'une façon très intuitive et émotionnelle. Posez
vous la question. L'évidence, la rupture devraient apparaître
naturellement (ou pas). Je me répète, cette réponse met en avant la
façon flagrante dont nous nous sommes fourvoyés concernant la gestion
projet, le management, depuis des années.

Merci Nathalie (@nrosenberg) pour cette intuition.

Sur ce, je retourne m'occuper d'une série rose digne de M6 des années 80
qu'il me faut rendre plus épicée.
