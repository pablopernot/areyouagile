---
date: 2014-10-17T00:00:00Z
slug: evenements-conferences-automne-2014
tags: ['conference']
title: Evénements & conférences automne 2014
---

Les événements à venir, et ceux qui se sont déjà déroulés, ainsi que les
supports de mes dernières interventions dans les deux conférences agiles
: Agile Tour Montpellier & Agile Toulouse 2014. A gauche en photo la
présentation infaisable : clôture de l'Agile Tour Montpellier sur un
thème très visuel, sans slide, autour du bar, avec une foule assoiffée
(et donc dangereuse) en guise de public.

![Conférences](/images/2014/10/conf.jpg)

## Raid agile - du 4 au 7 novembre 2014

![les 3C.](/images/2014/10/3c.png)

[raid agile, le raidme](http://raidagile.fr/pdf/raidme.pdf)

Bon d'abord vous le savez, je pense que je sature déjà mes proches avec
le [raid agile](http://raidagile.fr), mais c'est vraiment une formation
à laquelle je tiens (3 jours *off* dans les Cévennes). Tout ce que je
vous recommande c'est de lire le tout récent
[RaidMe.txt](http://raidagile.fr/pdf/raidme.pdf) qui vous dévoilera le
lieu, le menu, le programme, les à côtés. Il reste des places !

## Charlotte & Claude Emond, le 24 & 25 novembre 2014

![Claude & Charlotte](/images/2014/10/charlotteclaude.jpg)

Puis fin novembre j'ai le plaisir d'accueillir Charlotte Goudreault &
[Claude Emond](http://www.claudeemond.com/) à Montpellier pour a) une
soirée repas/conférence (le lundi 24 novembre 2014 au soir) autour de
l'engagement b) une conférence/atelier le lendemain (le mardi 25
novembre 2014) sur la conduite du changement en entreprise avec le
*changeboxing* (technique spécifique de Charlotte & Claude).

Tout savoir sur ces deux amis canadiens et leur conférence :
[convergenc.es](http://convergenc.es/claudeemond.html#main). C'est aussi
le moment de s'inscrire car les [tickets "early bird" (ou
"lève-tôt")](https://www.weezevent.com/claude-emond-charlotte-goudreault-montpellier-2014)
sont disponibles jusqu'à la fin du mois.

## Les organisations vivantes

J'ai pu au travers d'une sorte de premier jet présenter quelques
nouvelles réflexions, notamment sur le fameux *scaling agile* (mais pas
vraiment en prenant les chemins habituels). Une [première
version](/2014/09/conferences-printemps-2014/) avait vu le jour à Agile
France en mai, et une seconde là, à Montpellier lors de l'Agile Tour
2014, mais vraiment dans des conditions particulières (derrière le bar
en fin de journée avec tout le monde qui attend la fin du discours pour
commencer à boire...). J'ai pu vraiment donner la conférence à Agile
Toulouse 2014, avec des slides (très imagés donc très importants) et
dans un amphi. Même si ma conférence était un état des lieux d'une
réflexion en cours (et donc pas vraiment figée) j'ai été satisfait de
cette conversation avec les gens (et il semble que eux aussi).

J'espère rapidement transformer cette réflexion en articles pour le blog
(très vite concernant les bidonvilles...), et à moyen terme en suite de
[la horde agile](/pdf/lahordeagile.pdf).

Voici les slides :

<script async class="speakerdeck-embed" data-id="42b9202038d5013275d562001b84dca3" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>  

En savoir plus : lire les prochains articles du blog

## Agilité organisationnelle & dynamique individuelle et d'équipe avec Boris

![Boris & Pablo en plein effort](/images/2014/10/voyage.jpg)

Ce qui a été une franche réussite à Agile Tour Montpellier 2014 c'est
par contre ma session commune avec Boris Quinchon autour de l'atelier
des *Lego Cynefin* : permettant à chacun de prendre conscience des
différentes dimensions organisationnelles et de dynamique individuelle
et d'équipe selon les contextes. Cette double voix (agilité
organisationnelle et dynamique individuelle) nous parait essentielle et
ne peut pas et ne doit pas être portée par la même personne, c'est
pourquoi depuis six mois nous travaillons avec Boris sur cette offre :
<http://agilecoaching365.fr>.

L'atelier qui devait regrouper 20/30 personnes a été finalement très
plein (\~50 personnes, voire plus), et un franc succès. Il donne une
nouvelle dimension à ce jeu pourtant déjà très intéressant : les
[sources de l'atelier](/2013/04/cynefin-et-son-lego-game/). Voici des
slides qui complètent notre atelier et notre offre :

<script async class="speakerdeck-embed" data-id="6f02a2d038e701329c3e167618ddf14d" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>  

En savoir plus : <http://agilecoaching365.fr>

## RH et agile avec Jas

Jas Chong a pu faire un retour d'expérience lors de deux manifestations
évoquées (et elle continue à [Agile Tour
Brussels](http://www.atbru.be/)). Je suis intervenu si le programme me
le permettait pour donner mon point de vue -- qui évolue mais qui se
marque encore plus plus : les RH ne seraient-ils pas finalement les
personnes destinées à porter l'agilité ? la culture, les valeurs, la
neutralité, c'est leur business --. Ces réflexions en cours avec Jas (on
se retrouve sur des points, on diverge sur d'autres) a aussi pris la
forme d'une offre pour laquelle nous avions pu faire un évènement en
septembre 2014 ainsi qu'un site <http://transformation-rh.fr> :

<script async class="speakerdeck-embed" data-id="6b9fafd039900132ebcd560aad5ce8df" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>
