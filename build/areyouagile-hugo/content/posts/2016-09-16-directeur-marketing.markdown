---
date: 2016-09-16T00:00:00Z
slug: mon-directeur-marketing-a-des-idees
tags: ['leanstartup','marketing','design','thinking','designthinking']
title: Mon directeur marketing a des idées
---

Deux fois en deux jours chez deux clients on me soumet la même
problématique. "Je suis un *product manager* et j'ai autour de moi un ou
plusieurs directeurs marketing qui ont plein d'idées et qui sont assez
incontrôlables. Qu'est ce que je dois leur dire, comment dois-je
appréhender ce flot d'idées ?"


## Le directeur marketing a une idée

**Lui dire qu'on la refuse systématiquement ?** Parce qu'elle est nulle
? Parce qu'elle ne nous plaît pas ? Parce que l'on a trop de choses à
faire ? Pas si simple, c'est peut-être votre "chef", et il fera
peut-être jouer ses biscottos. Pas si simple, son idée est peut être
bonne. Une idée c'est positif, soyez bienveillant, jetez au moins un
oeil.

De l'autre côté : **Lui dire qu'elle est bien que l'on fait
systématiquement ?** Intégrer toutes les demandes ? Pas si simple, on ne
pourra pas tout faire, on n'en a jamais les moyens. Pas si simple, trop
de fonctionnalités, trop d'idées nuisent généralement à votre produit
qui peut y perdre son sens, et devenir abscons pour ses utilisateurs. Et
**toutes ces fonctionnalités**, il faut les maintenir, **elles sont un
coût**.

## Lui donner du sens

À cette idée, il faut donner du sens. **Résout-elle un besoin ?
Apporte-t-elle de la valeur ?** Il s'agit des questions qu'il faudrait
se poser, auxquelles il faudrait essayer de répondre. Apporte-t-elle de
la valeur ? Confrontez là à vos ateliers et à vos outils de *design
thinking*, découvrez si cette idée répond à quelque chose, et comment
tester vos hypothèses.

Si l'idée ne répond pas à un besoin et ne semble pas amener de valeur,
il faut dire non. Et expliquer pourquoi on estime qu'elle n'amène pas de
valeur et ne répond pas à un besoin. Si l'idée ne répond pas à un besoin
mais semble amener de la valeur, il faut essayer de passer à l'étape
suivante. Si l'idée répond à un besoin mais n'apporte pas de valeur, et
que l'on est certain de cela, il faut dire non, sinon il faut essayer.
Idem, il faudra expliquer pourquoi on est sûr qu'elle n'amène pas de
valeur.

Malgré tout toutes nos analyses restent bien souvent des **hypothèses**.
Et généralement une hypothèse autant essayer de la valider ou de
l'invalider.

## Essayer avec le plus petit morceau qui fasse sens dont la valeur estimé est la plus haute

Découpez l'idée du directeur marketing en **petits éléments autonomes
faisant sens**, donnez ce **relief**, et utilisez l'élément dont le
ratio effort/valeur est le meilleur pour lancer une réalisation. Il faut
**mesurer la valeur et comparer avec celle attendue** (plus de
fréquentation, plus de transformation, plus de ...). Ça marche ? on peut
s'interroger : est-ce suffisant ? Si oui, on arrête, si non, étoffons
notre réalisation. Pareil, avec le plus petit morceau possible. **Ça ne
marche pas ? On retire la fonctionnalité** ainsi déployée et on arrête.

On fabrique le minimum et on test, comme dans le *lean startup*.
L'*impact mapping* est idéal pour tracer ce chemin à suivre.

Si vous n'avez que des bonnes idées qui apportent de la valeur, et que
l'on peut mesurer systématiquement celles-ci, vous allez être célèbre,
si vous ne l'êtes pas déjà.

## Rendre transparent le processus et donner du feedback

On explique tout au directeur marketing et on le tient au courant des
résultats, étape par étape. **Idéalement on ne traite qu'une idée à la
fois par directeur marketing**.

Sur la durée la pertinence des idées de votre directeur marketing va
émerger. Et un niveau de compréhension commune apparaîtra. Il sera plus
facile de dialoguer. N'oubliez jamais qu'il peut avoir une idée géniale
même si il en a eu de très mauvaises auparavant, et vice versa.

Je sais que tout cela parait évident a beaucoup. Mais il faut instaurer
un dialogue basé sur des petits pas. 

## Une petite carte mentale

![Carte](/images/2016/09/directeur-marketing.png)
