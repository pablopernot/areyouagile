---
date: 2016-01-15T00:00:00Z
slug: portfolio-projets-kanban-partie-1
tags: ['kanban','wip','limite','pull','portfolio']
title: Portfolio Projets Kanban, partie 1
---

La "Portfolio Projets Kanban" revient régulièrement dans mes
interventions. Trop souvent méconnu, cette activité qui ne demande
finalement pas tant d'effort que cela peut-être très structurante pour
l'organisation et apporter beaucoup de valeur. J'ai pu souvent en faire
la présentation et je me suis rendu compte que je ne l'avais vraiment
formalisé pour ce blog. Objectif de cet article : vous montrer comment
j'appréhende un "Portfolio Projets Kanban", du moins les premières mais
principales étapes. Un "Kanban Portfolio Projets" c'est quoi ? (à partir
de maintenant je prends des libertés avec la littérature sur Kanban et
je vous donne mon expérience, ma connaissance). C'est le portefeuille
projets de l'organisation, du département, de l'équipe, représenté de
façon visuelle, qui va *aligner* tout le monde, *communiquer* de façon
égale et directe à tous, pousser à *gouverner*, à *s'améliorer*.

Il s'agit donc d'afficher et de gouverner l'activité macro projet (ou
moins macro) d'une organisation.

Voici comment je le réalise **in situ**. Mes trois points de repères, ma
lecture de Kanban :

 -   Montrer la réalité et que la réalité
 -   Adapter la capacité pour favoriser valeur et apprentissage concernant votre stratégie.
 -   Adapter au mieux à la capacité et impliquer avec un flux tiré

*(j'essayerai d'aborder cette troisième "règle" dans un prochain
article, celui-ci se résume aux deux premières)*.

## Mais quand faire un portfolio projets Kanban ?

Quand il n'y en a pas. Quand on a besoin de gouvernance car l'amplitude
de l'activité le nécessite. Quand vous vous posez cette question.

## Afficher son activité

![Kanban Portfolio 001](/images/2016/01/001-kanban-stickies.jpg)

Un Portfolio Projets Kanban c'est à la portée de tous. Il suffit de
commencer le plus simplement du monde en demandant à tous de coller au
mur une fiche projet sur laquelle chacun travaille. Pas de doublon. Pas
de post-it, il s'agit d'une petite fiche sur laquelle nous ajouterons
des informations plus tard (mais vous pouvez commencer avec des post-it
pas de souci). Mettez simplement au mur tous les projets sur lesquels
travaille l'organisation. À vous de décider de la taille de ses projets,
ils peuvent être de tailles très différentes, pas de souci, vous pouvez
mélanger gros projets, et petits projets, et évolutions, et autres.
J'aborderai plus ce point dans la deuxième partie. Sentez vous libre
tant que vous respectez la réalité.

Cela à l'air tout bête. Juste afficher les projets sur lesquels chacun
travaille. C'est déjà disruptif. Vous prenez une matinée pour récolter
toute cette information. Vous l'avez déjà ? Tant mieux. Donc désormais
chaque conversation projet ou produit concerne un élément qui est au
mur, nous sommes d'accord ?

Je n'insiste pas sur l'importance du mur, elle est pourtant fondamentale
: un mur unique, tactile, qui aligne et partage la connaissance. Vous
êtes tous sur des sites différents ? Le **Portfolio Projets Kanban** va
servir à gouverner, mettez le sur un mur accessible au plus de monde
possible, sur le site des personnes qui gouvernent. Elles sont sur
plusieurs sites ? J'espère qu'elles se croisent régulièrement.

## Un brin d'organisation

![Kanban Portfolio 002](/images/2016/01/002-kanban-basic.jpg)

Bon plutôt que ce tas informe vous allez rapidement donner une dynamique
de lecture et de création de valeur (de gauche à droite) en ajoutant
simplement : *à faire, en cours, fini*. J'imagine que cette étape ne
prend pas beaucoup de temps. (Vous pouvez cliquer sur toutes les images
pour les agrandir).

Mais *à faire, en cours, fini* c'est rapidement trop succinct. Et peu
représentatif de la réalité, c'est elle qui nous intéresse. Pour cela
autant aller interroger tout un chacun sur le flux projet ou produit et
placer les bonnes étapes. C'est pour cela qu'il devient très important
de se rappeler cette première règle essentielle : *Montrer la réalité et
que la réalité*. En effet dès cette étape il serait aisé de se projeter
et de mettre ce que l'on souhaite. C'est la façon la plus certaine de
tuer son Kanban. Si il ne montre pas la réalité, il n'aligne personne,
il ne montre rien de réel, personne n'y accorde d'importance. Pour
aligner un groupe de personne, pour faire une résonance nous dit Daniel
Goleman dans l'excellent "Primal Leadership", il est important de
partager une vision commune et claire de l'état des lieux de l'équipe,
du département, de l'organisation. Voici déjà ce que fait votre
*Portfolio Projets Kanban*.

![Kanban Portfolio 003](/images/2016/01/003-kanban-basic-2.jpg)

Ici je me suis amusé à prendre un processus projet un peu hybride et non
agile. Peu importe concernant Kanban si il s'agit de Agile ou pas. On
montre la réalité et rien que la réalité. A toutes les questions que
vous vous posez lors de la réalisation d'un Kanban c'est la réponse la
plus pertinente : afficher la réalité. Si le Kanban montre la réalité
personne ne refusera la conversation. Peu importe le nom qu'elle porte.

**La réalité, rien que la réalité.**

### Combien d'éléments ?

J'ai pu observer qu'un Kanban acceptait **a minima** autour d'une
trentaine d'éléments (pas de règle précise, juste une mesure empirique),
en dessous, les éléments ne vont pas assez avoir de dynamique. Et donc
pas assez de sentiment de progrès pour se sentir impliqué. C'est
probablement le moment de penser à un découpage plus fin des éléments :
mais encore une fois que si cela correspond à la, à une réalité. Si vous
modifiez quelque chose sur le Kanban vous devriez immédiatement le
modifier dans la réalité (donc plutôt une chose à la fois...). Si vous
avez moins de \~20/30 éléments et que la lenteur de l'évolution du
Kanban vous démotive, ce n'est probablement que le reflet de ce qui se
passe en réalité. Les choses n'avancent pas assez, vous travaillez
probablement par trop gros blocs. Un maximum d'éléments ? Tout dépend de
la taille de votre mur, mais j'ai observé qu'au delà de 150/200 éléments
cela commençait à devenir beaucoup, genre sapin de noël baroque.

Si on parle de projets, et que chaque fiche représente un projet disons
de 4 personnes (c'est une hypothèse), on arrive à facilement gérer 100
projets et donc un impact sur une organisation de 400 personnes. C'est
un bon départ non ?

## Commencer la gouvernance

![Kanban Portfolio 004](/images/2016/01/004-kanban-swim-lanes.jpg)

On peut démarrer en clarifiant la gouvernance et en scindant notre
Kanban (notre management visuel) en lignes horizontales. Ce découpage
pourrait être par domaine métiers, par zones géographiques, par
activités, par types de projets... Ces lignes vont refléter votre
organisation stratégique, comment vous pensez votre activité.

Par exemple un cycle de vente pour un département commerce contact avec
les colonnes : rendezvous1, présentation, rendezvous2, etc. Et les
lignes horizontales : Europe, Amériques, Asie, etc.

Par exemple un département RH qui gère l'intégralité de son activité sur
un Kanban avec comme colonne : idées/demandes, sélection, en cours, etc.
Et les lignes horizontales : Recrutement, Affaires sociales, Suivi
collaborateurs, etc.

Par exemple, des projets de types légaux, innovations, etc.

Par exemple, pour les aficionado de Spotify, Squad 1 domaine XXX, Squad
2 domaine YYY, etc.

### Électronique versus physique

Tiens au passage, dans mon exemple factice, que j'ai ordonné avec des
lignes horizontales par domaine métiers, chaque ligne peut avoir des
spécificités par colonne comme indiqué ici (sur l'image plus haut, les
espaces "agile" et "agile/devops" pour certains domaines). Je n'ai à ce
jour trouvé aucun outils électronique qui sache reproduire cette
finesse. Et naturellement aucun outil électronique qui implique cette
résonance de groupe. À la question électronique ou physique, la réponse
est simple : les deux. Car les deux amènent des facilités et des
dynamiques. Les deux se complètent. Là on me réponds "Oh làa làa, c'est
long". Mais c'est juste une fausse intuition. On parle d'un *Portfolio
Projets Kanban*. On parle disons des 60 projets de votre entreprise.
Combien de fiches vont changer de colonne par jour... 5 serait énorme.
Je vous un accorde un extraordinaire 10. 10 fiches à bouger par jour.
2mn ? Doubler l'information dans l'outil. 5mn de plus. Le tout moins de
10mn par jour. On peut même doubler le Kanban physique (à 3 murs
synchronisés cela devient plus difficile).

Et difficile de rendre un exemple de mur visuel et tactile sur une image
au travers un navigateur...

Naturellement et quitte à me répéter, je commence par le Kanban
physique. C'est lui le Kanban maître. Le Kanban électronique le
complète.

Mais où le placer ? Je me répète : c'est le portefeuille projets de
l'organisation, du département, de l'équipe, représenté de façon
visuelle, qui va *aligner* tout le monde, *communiquer* de façon égale
et directe à tous, pousser à *gouverner*, à *s'améliorer*. Il faudrait
donc qu'il soit visible, accessible, mais aussi grand (tactile,
englobant), et pas loin des gens qui le manipulent et qui gouvernent.
Une grande pièce à l'entrée de vos bureaux ? Sur un grand mur dans votre
openspace ? On essayera d'éviter les bureaux fermés, ou les couloirs
sans recul.

## Des règles claires

![Kanban Portfolio 005](/images/2016/01/005-kanban-reglesclaires.jpg)

Pour faciliter l'implication et la prise d'initiative il convient
d'avoir des règles claires. Les règles claires permettent à chacun de
s'investir au mieux. Dans Kanban on précise souvent, mais uniquement si
c'est la réalité, ou si on décide que cela devient la réalité, on
précise souvent à qui appartient la colonne, qui en est responsable, qui
peut en manipuler les éléments. Une personnes, des personnes, un groupe,
un département, tout le monde etc, **la réalité rien que la réalité**.
Il est souvent bien de préciser ces propriétaires, ces responsabilités,
ces redevabilités. Au moins c'est clair, et chacun peut s'accaparer
l'information, se projeter, s'investir.

![Kanban Portfolio 006](/images/2016/01/006-kanban-reglesclaires-2.jpg)

Pareil, il est souvent précieux de préciser les critères de sortie d'une
colonne à l'autre. Établir ce protocole clarifie les tenants et les
aboutissants. Vous pouvez uniquement le préciser ici et là, pas
forcément partout. Et toujours : la réalité, rien que la réalité.
L'ajout et la clarification de critères de sortie intervient souvent
quand une fiche passe dans une colonne, puis revient à la précédente,
voire plusieurs fois. Oui j'autorise un retour arrière (c'est un débat
dans la communauté Kanban).

Pas forcément lisible sur les exemples, vous pouvez appliquer des
responsabilités et des critères de sortie par bloc. Attention cependant
à ce que la lecture de votre Kanban reste simple. C'est cet aspect
compact, synthétique qui facilite le branchement de tous sur un élément
central et partagé. C'est la vision par motif, macro, qui aide le mieux
votre intuition (toute l'expérience que vous avez emmagasiné et qui
parle sans mot) à prendre des décisions. On sait que toutes les
décisions passent par une part d'intelligence émotionnelle non
rationnelle. Ne cherchez pas à l'éviter, travaillez avec.

### Prenons un peu de recul

On se retrouve avec un grand tableau mural qui nous indique où nous
travaillons, quelles sont les densités, quelles sont les vides, quelles
sont les équilibres entre mes différents domaines. D'un coup d’œil je
vois les dynamiques, les inerties, les anticipations, les densités, les
manques, les vides, les trop-pleins, les choix, les encours, etc.

## La fiche projet

![Fiche Kanban](/images/2016/01/fiche-kanban.jpg)

Tout cela va s'enrichir de la fiche projet. Chaque fiche je vous l'ai
dit n'est pas un post-it, plutôt un quart de page A4 qui va rassembler
les principaux éléments descriptifs du projet. Ne faite pas de la
littérature ! Pour cela justement pointez sur vos références dans vos
outils électroniques, mettez sur la fiche les éléments essentiels qui
vont permettre la gouvernance, les arbitrages, faire apparaître les
signaux faibles ou forts.

![Fiche Kanban](/images/2016/01/fiche-kanban-details.jpg)

Par exemple ici j'affiche la *deadline* estimée, le nombre de personnes
affectées au projet, le budget estimé, le type de projet (si les lignes
horizontales ne prennent pas ce sens), etc. Dans la case acteurs on
précise les sponsors, les rôles, etc.

Très important j'affiche en bas de la fiche les dates d'entrées dans les
différentes colonnes. Ce n'est PAS une projection, *la réalité rien que
la réalité*, c'est quand la fiche bouge que j'inscris la date dessus.
Cela va me donner de précieuses indications sur les inerties, les temps
de traversées, etc.

### Prenons un peu plus de recul

J'obtiens désormais un tableau physique que personne ne peut éluder,
éviter, sur lequel tout le monde s'aligne, partage une même vision.

On y voit donc les densités, les inerties, les vides, les dynamiques,
les poids, les charges (avec les fiches).

On y voit les liaisons entre les projets, les dépendances, ce qui va
arrive, qui travaille sur quoi.

On y voit quand une personne est présente partout.

On y voit les projets fossiles, les projets factices, les dinosaures et
autres animaux de la ménagerie.

On y voit les décisions : quelle densité pour les projets du domaine 1 ?
et quelle densité pour les projets du domaines 2 ? combien de projets du
domaine 3 sont prêts à partir ? etc. Toutes les idées sont les
bienvenues, les "déverseurs" à l'entrée (idées, demandes), et à la
sortie (live/prod) peuvent accueillir autant d'éléments que vous voulez.
Mais à quoi bon mettre une 66ème idée dans la case "domaine 1" si 65
attendent déjà ? Une priorisation s'impose, etc. *Kanban c'est rendre
visible la réalité partager cette connaissance, et être poussé à
s'améliorer et à gouverner*.

## Les limites

![Kanban Portfolio 007](/images/2016/01/007-kanban-limit-1.jpg)

L'histoire de Kanban pourrait démarrer dans un vieux parc de Tokyo que
les japonais veulent préserver. Et pour cela, limiter son accès car au
delà de 200 visiteurs le parc souffre. Kanban signifie étiquette : il
suffit d'imprimer 200 tickets (ou étiquettes). Quand quelqu'un entre
dans le parc, il prend un ticket. Quand tous les tickets sont pris, il
n'est plus possible d'entrer. Quand quelqu'un sort il libère un ticket.
La capacité est adaptée au parc. Le flux des visiteurs est adapté à sa
préservation.

C'est ainsi que les limites fonctionnent aussi dans notre Kanban : elles
peuvent contrôler la capacité de notre portfolio projets de façon à ne
pas faire entrer trop de projets avant d'en avoir fait sortir et d'avoir
engrangé la valeur (tant que ce n'est pas en production la valeur n'est
pas là) et d'avoir optimisé la valeur, la performance (mot compliqué) et
limitant les choses sur lesquelles on travaille. On renforce ici un
adage de Kanban : *commencer à finir, arrêter de commencer*. En
contraignant le flux à certains endroits on s'oblige à avoir un flux
plus rythmé, et souvent cela fait la promotion des synergies
inter-colonnes.

Une limite indique donc le nombre d'éléments maximum autorisés par
colonne ou bloc ou ligne horizontale selon votre mode de fonctionnement
(ou tous à la fois).

Dans mon exemple je peux placer une limite sur toute une colonne (ici 3
éléments max pour la colonne "préparation", mais mon exemple ne la
respecte pas du tout, c'est maaal ! Ceci dit quand vous placez une
limite la première fois vous laissez la colonne se "purger" jusqu'à ce
que l'application de la limite devienne possible). Je peux aussi placer
une limite par bloc.

**Adapter la capacité pour favoriser valeur et apprentissage concernant votre stratégie**

### Combien de temps cela prend tout cela ? Quand placer les limites ?

Monter le Kanban prend quelques jours maximum, disons de 1 à 3 jours. Y
ajouter les règles claires, quelques heures de plus. Le maintenir, 10mn
par jour (le vrai "travail" c'est de coacher, d'expliquer, d'accompagner
autour, vous pouvez décider de le faire seul, ou pas). Le premier mois
je laisse vivre le Kanban, quelqu'un passe 10mn par jour à le maintenir.
Voire plus si on décide de le réaménager car on s'est rendu compte que
ce n'était pas la réalité, ou que celle-ci commençait à changer sous
l'effet du Kanban. À 2 à 3 mois j'espère que les personnes des colonnes
ont pris part à la manipulation du Kanban. On va rapidement imaginer
avoir une réunion hebdomadaire de 30mn (par exemple) dessus pour évoquer
les focus, les points de contention, les décisions à prendre, arbitrer,
etc. en un mot, gouverner.

Pour les limites c'est probablement plus long. Leur application peut
sévèrement bousculer les choses. j'ai donc tendance à d'abord faire
vivre un peu le Kanban. Voir la réalité de la création de valeur (de
gauche à droite) nous apprendre des choses avant. Quand vous le jugez
bon, placez les limites, elles doivent impacter la réalité.

Comment calculer les limites ? Vaste débat. Observez et placez quelque
chose de cohérent avec ce que vous avez découvert, et avec votre
gouvernance.

### Limites basses

![Kanban Portfolio 008](/images/2016/01/008-kanban-limit-2.jpg)

Dans ce nouvel exemple j'ai placé des limites basses : à défaut de
limiter le flux pour dynamiser son rythme, je place des limites basses
pour m'assurer de l'existence d'un flux. Il me faut *a minima* des
éléments en préparation pour garantir le non assèchement de mon activité
de réalisation.

### Limites "stratégiques"

![Kanban Portfolio 009](/images/2016/01/009-kanban-limit-3.jpg)

Plus stratégique, plus difficile à manipuler les limites par ligne
horizontale, elles indiquent le poids, la capacité que vous accordez
globalement à chaque ligne. Bien souvent ce ne sont pas des limites ici,
mais plutôt des équipes que l'on affecte, ou des groupes, une capacité,
etc. Là on a 3 équipes, et là 4, etc.

## Les urgences

![Kanban Portfolio 010](/images/2016/01/010-kanban-limit-4.jpg)

Quand on parle "limite" on arrive vite à parler "urgence". Les Kanban de
ce type sont utilisés dans les hôpitaux. Difficile d'annoncer à une
personne touchée par une hémorragie que, désolé, mais le service a
atteint sa limite. Donc attention avec les limites : pour beaucoup c'est
la clef de Kanban, mais c'est aussi un élément très structurant. À mes
yeux pas obligatoire. Mais très puissant.

Bref, malgré les limites il est souvent pertinent d'avoir une ligne
horizontale dédiée aux urgences. Urgences que l'on va elles mêmes
limiter. Si tout est urgence, il n'y a pas d'urgence.

## Les moments de synchronisation

Comme évoqué plus haut, et là je retombe dans la **littérature** Kanban,
il y a plusieurs moments de synchronisation.

### Point hebdo de vision globale

Probablement un point hebdo de synchronisation et de stratégie : voici
où est le focus, voici l'élément clef cette semaine, voici l'action qui
prime, etc. Avec tous, devant le Kanban si possible. Je recommande de
30mn voire un peu plus. C'est ce que l'Holacratie appelle une réunion
opérationnelle.

### Point mensuel d'amélioration façon rétrospective

Comme la rétrospective de Scrum ou la réunion de gouvernance pour
l'Holacratie, c'est le moment où on peut changer l'organisation,
chercher à améliorer des points, propager des apprentissages, etc. Il
s'agit d'optimiser le système.

### Point journalier à la Scrum

Comme toujours il est important de se croiser quotidiennement dans une
équipe. Ce que l'Holacratie va appeler la réunion debout. Tout le monde
inspire tout le monde.

### Communautés / Cercles

Par domaine, ligne horizontale, ou comme vous voulez, il y a
probablement aussi des moments de synchronisation entre cercle
(Holacratie), ou communautés de pratiques, un apprentissage par les
pairs, etc.

## Pour conclure cette première partie

Première partie ou pas, l'essentiel est là. Imaginez qu'un mur rende de
façon aussi perceptible et synthétique votre portefeuille de projets.
Que chacun puisse voir, percevoir, s'engager, s'impliquer. Le Kanban ne
fait que montrer la réalité, il n'est pas contestable. Toutes les
discussions s'appuient très rapidement sur lui. Si votre gestion ou si
votre système est trop compliqué pour être rendu dans un Kanban de ce
type, c'est qu'il est probablement trop compliqué pour être géré tout
simplement, et qu'il ne marche pas. Sinon Kanban est une vraie
longue-vue, un vrai gouvernail.

La deuxième partie : [portfolio projets kanban partie
2̀](/2016/02/portfolio-projets-kanban-partie-2/).
