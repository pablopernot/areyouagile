---
date: 2015-01-24T00:00:00Z
slug: raids-agiles-2015
tags: ['raidagile']
title: Raids Agiles 2015
---

Au moment où je vous écris le raid agile \#1 est achevé (novembre 2014),
le raid agile \#2 (mars 2015) est quasi complet (nous attendons la
confirmation d'une dernière personne ; nous allons mettre en place une
liste d'attente au cas où). Enfin de futurs raids agiles se préparent
pour 2015 : un en juin 2015, l'autre à l'automne au **québec** ([raid
agile au Québec](http://raidagile.fr/raid-agile-quebec.html)) avec
[Claude Emond](http://www.claudeemond.com) & Charlotte Goudreault en
plus de notre duo classique ([clodio](http://www.aubryconseil.com/) et
moi).

## Ravi

D'abord je dois dire que je suis ravi du succès du premier raid agile,
ce novembre 2014. (Pour en savoir plus : l'[origine du raid
agile](http://www.areyouagile.com/2014/07/raid-agile-en-cevennes/), ou
l'[article de
clodio](http://www.aubryconseil.com/post/Hypotheses-validees-annonce-du-Raid-Agile-2)
dessus, ou cet article, encore de clodio, sur la
[rétrochataigne](http://www.aubryconseil.com/post/La-retrochataigne)).
Tout s'est goupillé plutôt bien. Jusqu'à la bonne inspiration de
dernière minute qui a été d'opter pour un grand van noir pour
transporter tout le monde au lieu (plutôt qu'un beaucoup plus aléatoire
voyage en voitures). Je remercie tous les gens qui ont fait suivre
l'information sur cette formation pour le moins différente, tous les
gens qui nous ont fait part de leur intérêt et de leur soutien pour ce
type d'action.

Ainsi triplement ravi : ravi du raid \#1 le soleil, la nourriture
physique et spirituelle, une bonne ambiance. Ravi aussi du bon retour
des participants (quelques
[témoignages](http://raidagile.fr/raid1.html)). Ravi des futurs raids
qui s'annoncent (pour le second on passe de 12 à 18, soit la capacité
maximum).

## Feedback

-   J'ai beaucoup aimé ce duo avec Claudio, et l'esprit général d'une
    retraite dans les Cévennes entre copains avec guitares de Neil
    Young, et cuisses de canard.
-   Ah j'ai bien vu quelques moments de désaccord entre nous deux. Mais
    je ne vois pas comment cela pourrait être autrement. Et au contraire
    j'ai perçu cette différence enrichissante pour tous.
-   Une deuxième journée très dense en contenu où le besoin de sortie
    (balade, randonnée) est devenu évident. Nous allons équilibrer cela.
    Mais je rassure toute le monde le "raid" est surtout et va rester
    une immersion dans les Cévennes sans effort physique particulier si
    ce n'est des balades assez classiques.
-   La nourriture cévenole ne rigole pas avec votre foie. Je vais
    chercher comment ajouter des choses plus légères aux repas costauds
    du traiteur local.
-   Je me demande si nous n'allons pas simuler la panne de réseau
    systématiquement le premier jour (comme ce fut le cas lors du
    premier raid). Cela a contribué à l'isolement et à la concentration
    générale.

## On garde

-   Une grosse partie du programme déroulé durant ces trois jours (mais
    on est prêt à tout changer si il le faut).
-   Le [gîte](http://www.grand-gite-gard-cevennes-sud.com/) de Claude
    (ils sont partout, là c'est "elle"), et Luc.
-   Le traiteur (que l'on combine avec un peu de douceur).
-   Le ou les vans pour ramasser tout le monde à Nîmes.
-   La pétanque, le ping pong, le babyfoot dans le
    [gîte](http://www.grand-gite-gard-cevennes-sud.com/), la musique
    pour les loup-garous du soir, et le toujours cruel "skulls & roses".

## Amélioration continue

-   Équilibrer les moments : randonnées, ateliers agiles pour améliorer
    le rythme général. Plus généralement ré-agencer notre programme,
    nous avons perçu les zones trop denses, et la pertinence de certains
    ateliers à certains moments ou non.
-   Affiner, compléter nos exemples Peetic avec Clodio, notamment sur
    Kanban.
-   Trouver une cérémonie journalière qui ouvre la discussion en fin de
    soirée en mode [world
    café](http://www.theworldcafe.com/method.html).
-   Peut-être aborder de façon plus marquée l'agilité organisationnelle.
-   Faire des tee-shirts pour tous (ainsi que les participants du numéro
    \#1).
-   Certains participants du premier raid avaient évoqué la création
    d'un groupe linkedin ou autre de façon à continuer les discussions
    après. Pourquoi pas !

De toute cela naturellement je dois parler avec Clodio. Mais je ne pense
pas trop me tromper en évoquant ces pistes.

[Le raid agile sur twitter](https://twitter.com/search?q=%23raidagile&src=typd)
(\#raidagile).

## C'est tiguidou

Tout va donc très bien pour la suite. Le raid de mars est en préparation
: on va voir si l'augmentation du groupe (de 12 à 18) chamboule nos
premiers apprentissages. On est ravi d'avoir dès à présent un raid quasi
plein (il reste une confirmation. On va ouvrir une liste d'attente, et
en lancer un 3ème pour juin). Et aussi plein d'espoir sur un [raid agile
au Québec](http://raidagile.fr/raid-agile-quebec.html) à l'automne 2015.
Je n'ai pas été au Québec depuis...1987.

[Tiguidou](http://oreilletendue.com/2013/04/12/tiguidou-nest-point-ketchup/) ?

