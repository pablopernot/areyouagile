---
date: 2013-12-11T00:00:00Z
slug: auto-organisation-et-storytelling
tags: ['storytelling','auto','organisation','lecture','livre','auto-organisation']
title: Auto-organisation et storytelling
---

Encore une belle lecture : *entre le cristal et la fumée* de **Henri
Atlan**, ce livre est bourré d'informations sur l'auto-organisation des
systèmes vivants, sur la mémoire, sur la transmission de l'information,
sur les systèmes ouverts et fermés, sur la complexité, etc. Datant de la
fin des années 70, il cite énormément le *paradigme perdu : la nature
humaine* de Edgar Morin, et les ouvrages de Marc Halévy ou Dominique
Dupagne y ont puisé directement ou indirectement beaucoup de matières.

![Entre le cristal et la fumée, Henri Atlan](/images/2013/12/cristalfumeehenriatlan.jpg)

Alors que je reviens d'un week-end à Tunis où j'ai pu avoir le plaisir
de donner une conférence (pour l'Agile Tour Tunisia 2013 sur
l'**OAA** (*Open Agile Adoption*) -dont vous trouverez [les slides
là](https://speakerdeck.com/pablopernot/open-agile-adoption)-. Parmi les
idées fortes de l'*OAA*, le *storytelling* et l'auto-organisation
tiennent une place prépondérante à mes yeux. Les propositions de Henri
Atlan me permettent de renforcer ou d'expliciter certaines de mes
réflexions.

Du **storytelling** on ressent bien qu'il s'agit d'une forme très
adaptée à notre façon de fonctionner, et comme le disait [Oana Juncu
dans son blog]() (ou lors des sessions *the storytelling battle* que
nous avons pu donner ensemble, au fait Oana as-tu libéré le support ?):
*notre cerveau est cablé histoire, la culture s'appuie sur des
histoires, le changement s'appuie sur des histoires pour se consolider*.

De l'auto-organisation, je reprendrais les mots de **Harrison Owen**
dans son récent TEDx sur l'Openspace (le forum ouvert), [Dancing with
Shiva]() : *les systèmes humains comme tous les systèmes vivants, sont
auto-organisés*, et plus loin il assène : *organiser un système
auto-organisé, ce n'est pas seulement un oxymore, c'est surtout stupide
!*

Tout imbibé de ces réflexions c'est en lisant donc ce livre de Henri
Atlan qu'une clef m'est donnée (notamment dans le chapitre *conscience
et volonté dans des systèmes ouverts auto-organisateurs*). J'essaye de
la formuler avec mes mots.

## Motifs mémoriels, *storytelling*

L'apprentissage c'est l'intégration de motifs. Plus on apprend plus on
intègre de motifs pour différencier les situations. Le bruit et le
désordre existent,et donc de nombreux motifs se ressemblent mais sont
différents. On créé alors de plus en plus de motifs pour répondre à
notre apprentissage grandissant. Ainsi le désordre génère de l'ordre (de
plus en plus de motifs) mais dans un ensemble de plus en plus complexe.
C'est ce qui constitue en fait ce que nous sommes, nos histoires, d'où
le *storytelling*, c'est notre passé, nos motifs mémoriels. C'est notre
conscience volontaire nous dit aussi Henri Atlan, mais elle concerne
ainsi d'abord le passé.

## Auto-organisation

L'auto-organisation est un phénomène naturel de tous les systèmes
vivants, auxquels nous n'échappons pas. Ce que précise Henri Atlan c'est
que l'auto-organisation est introduite par le désordre, auquel il faut
répondre, comme vu au dessus pour générer des motifs (de l'ordre).
L'auto-organisation c'est cette phase d'apprentissage non dirigé (par un
professeur par exemple) qui à pour but de générer des motifs. Cette
auto-organisation dévoile selon Henri Atlan notre volonté inconsciente
(et ainsi le futur, vers quoi nous tendons), puisque nous essayons de
créer de l'organisé en partant du chaos selon nos désirs.

## Enseignements

Plusieurs enseignements à mes yeux :

-   c'est bien le désordre qui génère de la valeur car il est la matière
    pour de nouveaux motifs, pour un nouvel apprentissage.
-   L'auto-organisation c'est cette faculté des systèmes vivants à
    transformer ce bruit, ce désordre en motifs, en histoire, le chaos
    en ordre. Mais en faisant cela, l'apprentissage augmente, ainsi que
    la complexité : l'entrelacement des motifs mémorisés.
-   L'histoire peut-être considérée comme un idéal motif mémorisé.
-   L'auto-organisation et le *storytelling* sont donc intrinsèquement
    liés. Il y a une forte relation de *feedback* de l'un avec l'autre.

## Maladie organisationnelle

Henri Atlan explique ensuite certains troubles psychologiques, le délire
évoqué par Edgar Morin dans son *paradigme perdu*, par des systèmes
figés, ou l'apprentissage n'a plus lieu : c'est donc le même motif,
immuable, que l'on retourne constamment (et qui prend la forme de délire
chez l'homme). Le lien avec une maladie organisationnelle me parait
évident : pensez à Kafka, on est figé dans des motifs où l'apprentissage
n'a plus lieu, que l'on répète constamment. Pensez aux organisations
dont les processus sont figés, qui n'apprennent plus de nouveaux motifs
liés aux variations que l'organisation rencontre (nécessairement). **Une
organisation sans auto-organisation est une organisation malade,
délirante**.

## Les temps complexes

On retrouve bien là aussi -pour ceux qui connaissent- la [spirale
dynamique](), plus nos organisations apprennent, plus nos civilisations
apprennent, plus les motifs sont créés, plus le monde devient complexe,
plus l'organisation, plus la civilisation à des motifs complexes, plus
les réponses doivent se baser sur l'implication, la coopération, la
responsabilisation. Ce que j'appelle les **temps complexes**.

![Les temps complexes](/images/2013/12/temps-complexes.png)

Pour résumer, **auto-organisation et storytelling sont donc
indispensables**, tension vers le futur, appui sur le passé.
