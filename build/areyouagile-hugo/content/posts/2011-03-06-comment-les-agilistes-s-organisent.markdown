---
date: 2011-03-06T00:00:00Z
slug: comment-les-agilistes-sorganisent
tags: ['organisation']
title: Comment les agilistes s'organisent ?
---

Récemment [Jurgen Appelo](http://www.noop.nl/) a déclenché une
initiative prometteuse : réunir, fédérer les agilistes européens. Je
trouve cette action très intéressante. Surtout dans sa forme. Jurgen
essaye de laisser le réseau faire émerger sa propre organisation tout en
l'accompagnant (et pas en le guidant). 

![Blackadder](/images/2011/03/blackadder_agile1-300x224.jpg)

Je ne sais pas si il réussira. On sent de nombreuses tensions derrière (pas de sa part !), mais comme me
l'a glissé Laurent B., l'agile devient "un secteur de l'économie du
logiciel" (j'aime sa formule), et donc l'agile aiguise les appétits.
Chaque initiative, chaque positionnement est observé avec suspicion. (Ah
j'oubliais un postulat important : je suis paranoïaque). Jurgen évoque
une grosse liste -beaucoup d'agilistes d'Europe-, une petite liste -les
agilistes qui se positionnent pour les actions types conférences, etc.-
et une mini-liste : les personnes clefs. Oups pardon, Jurgen précise
bien que plus la liste est réduite moins elle est importante (en taille
comme en valeur), donc il ne s'agit pas de personnes clefs, du moins pas
comme on l'entend habituellement. Il faut faire l'effort de le croire
sur parole, car d'instinct on imagine l'inverse:  que la mini liste
dirige la petite qui dirige la grosse. Jurgen espère -et je l'espère
aussi- que la mini liste encadrera la petite qui encadrera la grosse.
Encadrer au lieu de diriger. Pas de prévalence.

![Blackadder](/images/2011/03/blackadder_agile2-300x224.jpg)

Toute l'ambition est là, il faudrait réussir à ce que l'agile s'organise
à l'image de ses propres valeurs (auto-organisation, transparence,
confiance, respect, etc.).Certains donc demandent à ce que rien de ne
soit organisé : pas de structure, pas de réseau, etc. Certains demandent
à laisser le monde agile en complète autonomie, autogestion. Ils ont
peut-être raison mais jusqu'où peut-on aller ? Cette question fait écho
à ce qu'écrit Clay Shirky dans [Cognitive Surplus](http://www.amazon.com/Cognitive-Surplus-Creativity-Generosity-Connected/dp/1594202532) : il faut peut-être accepter autant que possible le chaos ("As much
chaos as we can stand") pour laisser émerger un nouvel horizon. C'est le
"autant que possible" qu'il faut définir.

Il faudrait que l'agile et les "agilistes" réussissent à s'organiser
comme les meilleurs des média-sociaux. Pour cela, par exemple, mon
action au sein d'un mouvement agile doit m'apporter un gain  (plaisir,
opportunité, argent, rayonnement, etc.), sinon je n'agirai pas. Et il
doit dans le même temps **en apporter aux autres utilisateurs et plus
globalement à l'agile**. Il faudrait une organisation de participants,
et pas de consommateurs et de fournisseurs.

C'est le pari de Jurgen il me semble. Suivons cela avec attention.

*ps : en image l'inoubliable Blackadder et son fidèle Baldrick. Merci
Fred pour la découverte*
