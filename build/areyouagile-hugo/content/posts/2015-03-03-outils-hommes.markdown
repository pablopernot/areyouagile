---
date: 2015-03-03T00:00:00Z
slug: des-outils-et-des-hommes
tags: ['outil','interaction','individu']
title: Des outils et des hommes
---

"Mais qu'est-ce que tu racontes, Pablo fait une présentation sur un
outil ?" glisse l'un, "Pablo ? Pablo !? Il propose de ne plus utiliser
des tableaux physiques, au mur ?" s'interroge l'autre ; et le troisième
narquois : "et Pablo il réagit comment quand
[Stéphane](http://www.stephanegenin.com/) propose cela ?". Dans ma
sphère -- c'est à dire 250m de rayon autour de notre adresse à
Castelnau-Le-Lez (Montpellier), et 75m de rayon autour de ma coloc sur
Paris -- c'est l'émoi.

"Non je n'ai pas changé", comme un chanteur l'a si bien dit, je penche
toujours pour **les interactions et les individus plutôt que les outils
et les processus**.

Mais donc que vient faire [Stéphane](http://www.stephanegenin.com/) avec
toi autour de ces outils ? Et ce
[atlassian.smartview.fr](http://atlassian.smartview.fr) ?

## Une aventure humaine

C'est d'abord **une aventure humaine**, une question d'individu
justement. J'ai croisé Stéphane sur un mission il y a bientôt 5 ans
(avec un troisième larron qui lui a décidé de rester dans l'entreprise
où nous nous sommes croisés, devenir coach agile, et faire mieux que
nous deux réunis). Depuis on se rencontre régulièrement, Stéphane a
vendu sa boite, il a cherché son chemin quelques temps par ailleurs,
puis nous avons décidé de travailler ensemble, par amitié, quelqu'en
soit le véhicule.

Depuis que je le fréquente, Stéphane, c'est un supporter de la suite
Atlassian, vous savez Jira, Confluence, Bamboo, Stash, et consorts.
**Atlassian** je connais bien, comme consommateur, je le pratique
souvent chez mes clients. Et **j'en suis content**, c'est un piège
d'ailleurs.

## Un outil dans un monde complexe ?

Un piège ? C'est tellement simple de se faire happer par un outil, on se
repose, on se laisse bercer, on délègue sa réflexion et sa
responsabilité, Ulysse et le doux chant des sirènes. Et puis on s'écrase
sur un récif.

L'Agile c'est une approche adaptative pour évoluer dans un monde
complexe donc mouvant. Agile c'est souvent l'histoire de transformations
d'entreprises, et le changement ce n'est pas maintenant, c'est tout le
temps.

A cela vous voulez adjoindre un outil ? Vous faire dominer par un gros
obélisque figé ? Représenter la réalité fluctante de vos équipes, leurs
différences, à travers un seul prisme ? Vous risquez de rendre froid un
monde qui vit, de geler les dynamiques, de fossiliser les énergies.

Vous n'êtes pas là pour l'outil. Jamais. Ne tombez pas sous son joug.

## Le syndrome du penalty

Mais c'est quand même fichtrement pratique. Ah, *monde de
contradictions* ! En fait c'est un peu le syndrome du penalty. Il y a un
drame quand on le rate. Le réussir c'est normal. Pour l'outil c'est la
même chose : C'est utile pour favoriser les transformations, servir de
guide (léger), de capitalisation, de référentiel (et là *Confluence* est
mon chouchou). Je gagne du temps pour diffuser les informations,
centraliser certaines façons de faire **dans les grandes lignes**. Quand
il n'y a pas d'outil c'est très bien aussi : cela foisonne, c'est
inventif, mais cela demande plus de temps (pour de bonnes raisons
peut-être : une adaptation plus réussie) mais souvent, si l'on destine
l'outil à un usage très générique, j'ai l'impression que l'on perd du
temps à réinventer la roue. Tiens ça y est j'entends le chant des
sirènes au loin, faut faire attention de ne pas trop se rapprocher du
rivage.

L'outil n'est sûrement pas votre cible, tout comme il n'est pas la
mienne dans les transformations Agiles. Mais j'en ai besoin d'un pour
m'appuyer, sans qu'il me gène, sans qu'il me gangrène.

## Un choix de raison "gagnant-gagnant"

En cela la proposition de [Stéphane](http://www.stephanegenin.com/) sur
Atlassian (qu'il côtoie depuis plusieurs années) me convient très bien.
C'est un outillage assez neutre, assez peu intrusif, il ne chante pas
trop à mes oreilles. Il est là, discret, fidèle, à l'appui, sans me
trainer contre mon gré là je ne veux pas aller. Et il se révèle assez
complet.

Enfin je suis persuadé qu'un monde moderne, privé d'une abondance de
ressources, un monde où la richesse est immatérielle, passera
nécessairement par un travail distribué, à distance, et là, il faudra
des espaces virtuels.

Pour revenir au fondement, c'est une aventure humaine que j'accepte avec
plaisir, [Stéphane](http://www.stephanegenin.com/).

## Une utilisation raisonnée

Gardez à l'esprit une utilisation raisonnée de vos outils. **Les
conversations, les histoires, les interactions, les individus avant les
processus et les outils**, n'oubliez jamais, navigateurs.

Généralement par exemple : je garde **toujours** les radiateurs
d'informations, les tableaux physiques muraux, qui offrent une dynamique
locale incomparable. Alors oui je demande à mes clients, collaborateurs,
collègues, de dupliquer le tableau physique et le tableau électronique.
L'effort, très faible, en vaut très largement le coup.

C'est aussi physiquement, si vous faites diagrammes ou courbes, que je
les propose. Pour les mêmes raisons.

Enfin, je ne place généralement pas les tâches dans l'outil, a) c'est un
périmètre local d'équipe, b) je tiens à garder une identité d'équipe
partagée et pas individualisée (dans les tâches, on peut aussi placer un
utiliseur "équipe"). On perd ainsi malheureusement un lien confluence,
jira, bamboo, stash qui peut s'avérer assez intéressant. Monde cruel.
Comme toujours tout cela dépend du contexte.

Et donc bienvenu à [Stéphane](http://www.stephanegenin.com/) chez
SmartView, fait comme chez toi. Et bienvenu à ton offre
[atlassian.smartview.fr](http://atlassian.smartview.fr).

Ecrit en écoutant "Aerosmith - Aerosmith 1973" et "Aerosmith - Draw the
line 1977"
