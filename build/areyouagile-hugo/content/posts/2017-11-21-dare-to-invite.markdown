---
date: 2017-11-21T00:00:00Z
slug: dare-to-invite-openspace-agility
tags: ['openspace','agility','openspaceagility','open','agile','adoption']
title: Dare to invite, Openspace Agility
---

Je profite d'une session hier autour de **openspace agility** et de la venue de [Dan Mezick la semaine prochaine](https://osa.areyouagile.com) pour remettre des slides que j'avais réalisé pour NCrafts 2016, ne les trouvant jamais quand je les cherche.  

En anglais, sur la conduite du changement, l'engagement et **openspace agility**. 

<iframe src="//www.slideshare.net/slideshow/embed_code/key/KGINVlqEgL4hq2" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/pablopernot/open-adoption-dare-to-invite" title="Open Adoption - Dare to invite" target="_blank">Open Adoption - Dare to invite</a> </strong> from <strong><a href="https://www.slideshare.net/pablopernot" target="_blank">Pablo Pernot</a></strong> </div>

Enfin, la vidéo de la session. 

<div class="videoWrapper">
<iframe src="https://player.vimeo.com/video/167722770" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/167722770">Pablo Pernot-Open Adoption</a> from <a href="https://vimeo.com/newcrafts">NEWCRAFTS Conferences</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
</div>

Merci [ncrafts](http://ncrafts.io/). 




