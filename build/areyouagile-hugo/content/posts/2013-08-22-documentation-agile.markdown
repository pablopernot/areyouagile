---
date: 2013-08-22T00:00:00Z
slug: documentation-agile
tags: ['documentation','agile']
title: Documentation agile
---

Non non non ! Je ne relance pas la discussion sur ce mythe selon lequel
il n'y a pas de doc en agile. Je vous fait part d'un feedback de l'un
des "technical writer" avec lequel j'ai pu travailler de long mois et
avec plaisir (**Christopher Nelmes** pour ne pas le citer). Comme nous
allions répandre la *bonne parole* auprès des autres "technical writer"
(comment le traduire ? -un comble- les gens qui font la doc quoi), je
lui ai demandé voilà deux ans un petit retour sur ses impressions et
comment aborder l'agilité quand on est uniquement (et c'est beaucoup)
responsable de la documentation (*end user*, utilisateur final).

Je ne dis pas que je suis en phase avec tout, mais son point de vue est
intéressant. Nous sommes en Scrum, il a été décidé qu'il était intégré à
toutes les équipes du département (disons de trois à cinq). Au delà de
deux ou trois la charge s'avère intenable, et la qualité se dégrade
(comme toujours), et donc il avait priorisé son implication (par valeur)
sur deux ou trois équipes/projets "phares".

J'aime beaucoup cette vision décalée et en le lisant vous comprendrez
immédiatement tout l'intérêt des profils variés au sein d'une équipe
agile. La combinaison des profils et son bénéfice espéré :
l'intelligence collective. (Ce texte a donc deux ans).

Voilà ce qu'il m'écrit (je traduis, la version originale est en
dessous).

## Prise en main de l'agilité par une personne responsable de la documentation utilisateur

et intégrée à plusieurs équipes.

### Comment mettre à jour une documentation utilisateur existante lors de développements agiles

-   Participer à tous les *sprint planning*
-   Bien connaître son équipe et identifier les principaux contacts
    (scrum master, product owner,etc)
-   Identifier (rapidement) les impacts des projets scrums dans les
    différentes documentations pré-existantes - toujours penser à
    demander si il y aura plus d'impact, notamment dans des domaines
    moins clairs, évidents, initialement.
-   Ne pas prendre pour acquis si vous vous entendez dire qu'il n'y aura
    pas d'impact. Trouvez par vous même quels sont les changements. La
    personne en charge de la documentation connait son domaine et
    comprendra certains impacts qui resteront invisibles pour une
    personne moins au fait.
-   Créer les titres de chapitres, les thèmes. Faîtes que vos chapitres
    soient facilement identifiables et que l'on puisse les associer avec
    les user stories et les sprints.
-   Essayer de les étoffer assez pour qu'ils fournissent une
    compréhension globale et cohérente du projet scrum.

### Comment créer une documentation utilisateur lors de les développements agiles

-   Reprendre tous les points vus plus haut.
-   Initialiser une table des matières complète pour la documentation.
    Le responsable de la documentation (Technical Writer) doit connaître
    l'ensemble du périmètre du nouveau projet, même si il n'en connait
    pas les détails, ni le contenu précis, il doit être capable d'en
    connaitre le cheminement complet.
-   Identifier les parties particulièrement impactantes pour la
    documentation utilisateur même sans connaitre les détails du
    projet/produit.
-   Créer les titres de chapitres, les thèmes. Faîtes que vos chapitres
    soient facilement identifiables et que l'on puisse les associer avec
    les user stories et les sprints. Attention il y aura potentiellement
    beaucoup de sprints/scrums en parallèle.
-   Essayer de les étoffer assez pour qu'ils fournissent une
    compréhension globale et cohérente du projet scrum.

### Principaux bénéfices de l'agile

-   Actif au sein de l'équipe dès le premier jour.
-   Facilité d'accès à l'information et aux personnes.
-   Connaissance améliorée de la charge de travail et des
    responsabilités en participant aux réunions et évènements agiles.
-   Point clarificateur au début et à la fin de chaque itération.

### Principaux inconvénients

-   La participation de mes co-équipiers à des relectures a été très
    aléatoire. J'ai besoin de revues approfondies. Parfois l'implication
    de mes co-équipiers a été trop légère et a eu des répercussions plus
    tard.
-   Le Product Owner est vraiment important, il doit vraiment être bon.
    J'ai connu un PO qui avait simplement oublié de me dire quelque
    chose d'essentiel et je l'ai découvert trop tardivement dans le
    projet.
-   Plein d'inconvénients si on n'est pas véritablement agile
    (connaissance, communication et existence d'une vision pour le
    projet, manque de communication due à un manque d'organisation (NDT
    : non respect du cadre scrum)).
-   Une grande part de la réussite ou non de la documentation tient dans
    l'intéraction avec des personnes clefs (le product owner et parfois
    le scrummaster). Ainsi la relation avec ces personnes est
    fondamentales et jouera un rôle très important sur la qualité et la
    complétude de la documentation. Pas de beaucoup de place pour
    l'erreur (NDT : sous entendu : si cette relation est mauvaise, la
    documentation sera mauvaise)

Voilà, Christopher tu me dis si la traduction ne convient pas.

## Version anglaise originale

### How to integrate Agile development into existing content

-   Attend all sprint planning meetings
-   Get familiar with the teams and identify principal contacts (scrum
    master, product owner, etc)
-   Identify impact from a scrum in different guides – always think to
    ask if there is more impact, perhaps in areas that may not appear
    obvious
-   Don’t take for granted if you are told that there is no doc impact.
    Find out for yourself what the changes are. The technical writer
    knows the content of the documentation and may know about a doc
    impact that someone else does not know about
-   Create reqs. Make reqs easily identifiable and corresponding to
    scrum and sprint. There may be lots of concurrent scrums/sprints.
    Contain as much detail as possible to at least show the scope of the
    scrum project

### How to integrate Agile development into new content

-   All the above
-   Create project plan for doc. Technical writer (TW) must know the
    complete scope of the new project even if the TW does not have any
    real details/content to be able to create a plan for the doc.
    Identify all areas of impact even without knowing all the exact
    details.
-   Create reqs. Make reqs easily identifiable and corresponding to
    scrum and sprint. There may be lots of concurrent scrums/sprints.
    Contain as much detail as possible to at least show the scope of the
    scrum project

### Principal benefits of Agile

-   Active in the team process from Day 1
-   Ease of access to information/resources
-   Knowing about work loads, responsibilities, etc from meetings
-   Summaries both before and after a sprint in planning and review
    meetings

### Principal downsides

-   I have experienced very differing levels of review comments. I
    require thorough review. Sometimes review has been done too quickly
    in my opinion and may have repercussions later.
-   The Product Owner (PO) must really be thorough and on top of the
    game. I have had the experience of a PO simply forgetting to inform
    me of substantial material that required documentation at a late
    stage.
-   All downsides apply to not using the agile principles (TW is not
    always aware of project – difficult to obtain information due to
    lack of organisation).
-   Much of the responsibility for the documentation comes down to the
    interaction with few people (product owners and in one instance, the
    scrum master). Consequently, it is a relationship of great
    importance to ensure a high quality and complete documentation. Not
    much room for error.

