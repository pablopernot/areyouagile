---
date: 2011-09-11T00:00:00Z
slug: consideration-respect-et-effet-pygmalion
tags: ['manager','coaching','mentoring']
title: Considération, respect et effet pygmalion
---

Il est assez facile d'expliquer aux managers que l'on côtoie dans le
coaching agile l'application de certaines pratiques, par exemple :
l'automatisation des tests, la notion de "fini", les itérations, etc, et
certaines valeurs : inspection & adaptation (pour -par exemple- scrum).
Pour les managers les gains apportés par ces pratiques et valeurs
coulent de source. Quand on parle de confiance, de respect, c'est
toujours beaucoup plus difficile. Ces notions agiles (depuis Lean)
paraissent trop souvent liées à une philosophie, voire une idéologie, et
donc ne sont pas appréciées à leur juste valeur par des profils dont
l'objectif est avant tout de produire des résultats, de rentabiliser en
quelque sorte leurs équipes, leurs produits, etc. 

![Pygmalion](/images/2011/09/pygmalion-240x300.jpg)


Pour mieux les convaincre que ces valeurs ont autant d'importance que
les autres rappelez leur l'[effet Pygmalion](http://fr.wikipedia.org/wiki/Effet_Pygmalion) (ou Rosenthal).

Prenez deux équipes, expliquez aux managers que l'une est constituée de
membres très performants, et l'autre de membres très anodins. Dans les
deux cas c'est totalement faux : les deux équipes se valent. Les
managers vont se comporter différemment en ayant plus de respect et de
confiance avec l'équipe dite performante, s'attribuant même (à eux les
managers) potentiellement au passage certains des échecs. Cette approche
(qui n'est pas très éloignée de la méthode Coué) va non seulement faire
paraître l'équipe soit-disante plus performante meilleure, mais va
véritablement la rendre meilleure (même si d'après les études cette
différence de performance ne tient pas forcément sur la durée).

Donc **en ayant du respect, de la confiance et de la considération pour
leurs équipes, les managers les rendent concrètement plus
performantes.**

La considération cela ne coûte rien et cela fait beaucoup de bien. Ne
voyez pas que le côté cynique de cette phrase. On oublie trop souvent de
féliciter les gens quand quelque chose de bien se produit.

[Peinture de Gérome](http://fr.wikipedia.org/wiki/Pygmalion_et_Galat%C3%A9e)

