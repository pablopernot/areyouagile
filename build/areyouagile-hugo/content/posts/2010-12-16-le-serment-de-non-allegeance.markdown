---
date: 2010-12-16T00:00:00Z
slug: le-serment-de-non-allegeance-de-mr-cockburn
tags: ['cockburn','methode']
title: Le serment de non allégeance de Mr Cockburn
---

Je suis en train de penser à un billet qui s'intitulerait "passe ton
cycle en V d'abord" et qui souhaiterait mettre en évidence l'intérêt
(l'importance ?) d'avoir pratiqué d'autres formes de gestion de projet
pour mieux appréhender la gestion agile. Je repense à une pétition sur
le web, signée il y a quelques temps, un très juste rappel de [Alistair
Cockburn](http://alistair.cockburn.us) : le serment de non allégeance.

![oath of non allegiance](/images/2010/12/oathofnonallegiance.png)

[http://alistair.cockburn.us/Oath+of+Non-Allegiance](http://alistair.cockburn.us/Oath+of+Non-Allegiance)

*Je promet de n’exclure aucune idée sur la base de sa source mais de
donner toute la considération nécessaire aux idées de toutes les écoles
ou lignes de pensées afin de trouver celle qui est la mieux adaptée à
une situation donnée.*

A méditer et mettre en pratique.
