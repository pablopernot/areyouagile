---
date: 2017-12-16T00:00:00Z
slug: agile-a-l-echelle-liberation-auto-organisation
tags: ['agile','scaling','echelle','safe','nexus','less','auto','organisation','auto-organisation']
title: Agile à l'échelle, libération de l'auto-organisation
---

Casablanca, décembre 2017. J'interroge le groupe de personnes qui se trouvent face à moi. Un groupe de personnes dédiées aux processus et à la qualité. Un groupe bienveillant et ouvert. "Combien d'habitants à Casablanca ?", "Entre 5 et 6 millions". "Combien de repas par jour par habitants ? Est-ce que je peux estimer à trois par personne ?", "Oui". "Bon donc disons 15 millions de repas par jour, et disons une semaine ou deux semaines de stock de repas. Et ils sont tous délivrés, ou plutôt mangés, sans trop de souci, tous les jours, depuis longtemps", "Oui". "Proposez moi un schéma, une liste des processus, une série de gestes, d'actions, une organisation qui permettent de mettre cela en œuvre s'il vous plaît."

La seule réponse valable me parait être l'auto-organisation. 

Il n'y a pas d'autres réponses à mes yeux. Je n'en vois pas d'autres.

Est-ce que vous pouvez en décrire d'autres ? En imaginer d'autres ? 

Sérieusement ?   

**La gestion d'un système qui nous dépasse passe par la libération de l'auto-organisation**.

Pas d'autres solutions qui rendent cela possible, pas d'autres solutions qui rendent cela aussi efficace. Il n'y a **aucune imposition du geste** juste une **clarification des contraintes**, la définition d'un cadre que chacun peut s'approprier (tant que les contraintes ne sont pas étouffantes). Et la capacité innée des systèmes vivants à s'auto-organiser. Cet exemple, lumineux à mes yeux, je le tire du livre de *Harrison Owen, "the practice of peace"*.     

À l'heure où tout le monde a les mots "agile à l'échelle" sur les lèvres, les villes, les mégapoles, peuvent devenir des références intéressantes auxquels consacrer du temps. Elles ne sont certainement pas de grands ensembles froids et mécaniques. Elles gardent un aspect très organiques. Elles grouillent. Elles ont toutes des rues, des immeubles. Mais pas un n'est vraiment comme l'autre. La conversation sur Casablanca est une analogie claire sur l'importance de l'auto-organisation. Cela marche avec Paris, New-York, etc. Mais pas avec toutes les villes, pas avec toutes les mégapoles géantes. 

Certaines croulent sous leur poids, certaines sont malades, certaines ne savent plus assez s'auto-organiser, ou n'ont jamais su.   

Comme certaines de nos organisations par exemple. 

Comme **Les bidonvilles** par exemple.  

Dans ces mégapoles insalubres, comme dans de nombreuses de nos organisations à l'échelle, les artères sont bouchées, les ressources épuisées, les humains fatalistes. **On sent pourtant cette énergie à (sur)vivre mais elle ne réussit pas à s'agréger en quelque chose de satisfaisant**. On a l'impression d'être au pied d'une montagne de détritus à ciel ouvert et que quelqu'un nous demande de faire du tri sélectif. La tâche parait insurmontable. 

![Bidonville](/images/2014/10/bidonvilles2.jpg)

Or comme dans le cas de Casablanca, ou de Paris, ou de New York, ou de &lt;*placer le nom de votre organisation à l'échelle ici*&gt;, diriger le geste parait vain, amener un cadre qui permette à l'auto-organisation d'émerger semble être la solution.      

Un petit livret passionnant sur l'amélioration des favelas, des bidonvilles brésiliens, peut se trouver sur le net :[Slum upgrading, lessons learned from Brazil](http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf). Il est une source surprenante de clairvoyance pour amener un cadre propice à l'auto-organisation, qui a donné des fruits pour ces favelas. Qu'est ce qui ressort de ce rapport ?   

La **libération de l'auto-organisation** et ainsi la résolution de la problématique d'un système qui nous dépasse est venu -- dans le cadre de ces bidonvilles -- de la création d'un maillage de petits ensembles dont les caractéristiques principales ont été les suivantes :

* Donner une autonomie administrative (et notamment -- et symboliquement -- budgétaire) à ces ensembles. Leur donner les moyens de la réponse. Pour nos organisations il s'agit de déléguer la décision au bon niveau, au bon endroit. Pas de centralisation. Une vraie autorisation, un vrai sentiment de contrôle, à chaque niveau. **La résolution de problèmes doit être décentralisée**.
* Proposer une **approche participative** dans les choix qui s'offrent (habitats, etc.), personne n'étant plus à même de savoir répondre à une question que celui pour qui elle est posée. **La vision est collective, par le consensus** dans ces bidonvilles qui s'assainissent. La participation, l'implication, l'observation des personnes portent l'amélioration continue. **Cette approche participative est soutenue par de la transparence et des mesures qui font sens**.  
* Un **réseau modulaire de ressources** qui permette à chaque groupe de s'approprier les siennes (encore de l'**autonomie**). Pour les favelas brésiliennes il s'agit de diffuser l'eau au travers de micro-bassins déployés en réseau au travers de la mégapole au lieu de proposer un bassin géant pour tous. Pour vos organisations le principe est le même, une modularisation des ressources, pas de gros point de contention, pas de silos qui maîtrise un maillon de la chaîne et rend caduque l'idée d'autonomie.  
* Une proximité des habitats avec le lieu de travail, et une diversité dans les habitats. Chacun est libre de s'installer comme il le souhaite mais une proximité doit exister entre les personnes et leur travail. Co-localisation physique, ou espace de co-localisation virtuels (pour les nouveaux métiers), le lien social est fait de proximité, mais il n'est pas fait de similarité (chacun vient comme il est). **On doit encourager la socialisation**, la création de communautés.  
* L'équipe qui accompagne cette libération de l'auto-organisation est pluridisciplinaires, elle propose une sensibilisation continue, et un changement du cadre étape par étape. Elle intègre de nouvelles têtes, des gens différents qui portent un regard neuf sur les choses.  

Pour les gens qui se posent ces questions autour de "mises à l'échelle agiles" cela voudrait dire que l'équipe qui fait du coaching agile se devrait de, et quelque soit les cadres utilisés, même le fameux SAFe, donner de l'autonomie à chaque groupe pour sa prise de décision, et son accès aux ressources (budget), de l'autonomie en un mot, réelle. 

Une vraie **autonomie**. 

Mettez de la **rigueur et de la discipline** dans cette réalisation. 

Plutôt que dans l'application d'un geste. C'est votre rôle de manager de libérer cette autonomie, de la protéger.   

Proposer une approche participative au niveau de chacun de ces groupes pour qu'il adapte, change, observe, juge par lui même de l'avancée de ses travaux. Mettre en place une plate-forme sociale qui permette à chacun de ces groupes de partager une expérience commune, mais sans faire que cette appartenance à un groupe ne gomme l'individu (chacun reste lui-même, pas tout le monde ne doit possèder le même ordinateur, pas tout le monde n'a à utiliser exactement le même outil, pas tout le monde n'a de nécessité d'avoir la même façon de travailler, tant que tout le monde peut travailler ensemble).

La mise à l'échelle n'est définitivement pas la réplication à l'identique. 

Avoir des mesures qui font sens et qui soient transparentes, attachées au sens et au pourquoi.

Sans cela pas d'auto-organisation. 

Sans cela un système qui nous dépasse et que l'on ne peut pas maîtriser car si paradoxal que celui-ci puisse paraître c'est en libérant l'auto-organisation que vous le maîtrisez, en jouant sur le cadre, sur le sens. 

Est-ce que cela arrive vite ? Je ne sais pas. La vitesse de votre mise à l'échelle n'a aucun intérêt en tant que telle. Le seul intérêt est : est-ce que vous répondez avec le bon rythme à votre pourquoi de la mise à l'échelle. Mais là encore, la vélocité (je ne parle pas de la mesure abscons proposée dans certains cadres agiles) ne sera que meilleure si vous libérez l'auto-organisation, plutôt que d'imposer vos gestes à tous.  

Cet article est un écho de celui d'un extrait des [organisations vivantes](/organisations-vivantes/chap4/) (comme l'alchimiste qui ne cesse de reprendre son travail pour le refondre, le revisiter, l'améliorer, je ne cesse de revenir sur mes réflexions, mais je ne sais pas si j'améliore).


## Une petite série sur mise à l’échelle (et auto-organisation).

* [Agile à l'échelle : c'est clair comme du cristal, 2013](/2013/11/agile-a-grande-echelle-cest-clair-comme-du-cristal/)
* [Agile à l'échelle : synchronisation, 2017](/2017/11/agilite-a-grande-echelle-synchronisation/)
* [Agile à l'échelle : pourquoi il faut se méfier de SAFe, 2017](/2017/11/pourquoi-faut-il-se-mefier-de-safe/)
* [Agile à l'échelle : libération de l'auto-organisation, 2017](/2017/12/agile-a-l-echelle-liberation-auto-organisation/) 
* [Agile à l'échelle : équipes et management, 2017](/2017/12/agile-a-l-echelle-equipes-et-management/)
* [Agile à l'échelle : grandir ou massifier](/2018/02/grandir-ou-massifier/)
* [Agile à l'échelle : Podcast "Café Craft" - "L'agilité à l'échelle"](http://www.cafe-craft.fr/20)

Et sinon l'événement que nous organisons en autour de l'agile à l'échelle avec [Dragos Dreptate](http://andwhatif.fr): 

* [VALUE DRIVEN SCALING](http://valuedrivenscaling.com)
