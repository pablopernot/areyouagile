---
date: 2013-01-04T00:00:00Z
slug: annonce-agile-open-sud-2013
tags: ['conference']
title: Annonce Agile Open Sud 2013
---

Pour la deuxième année le sud auto-organise un **agile open sud**. Il
s'agit d'un lieu ouvert de discussions, pour 25 personnes maximum (mode
*roots* : 4 dortoirs de 5 lits, 1 chambre de 4 lits). Le lieu qui nous
accueille est le gîte [Equisud](http://www.equisud.com), aux Angles,
dans les Pyrénées. Avouez cela a de la gueule. Nous serons en pension
complète, tranquille, à discuter (le tout pour 110 euros) du 5 au 7
avril 2013. Il y aura probablement du co-voiturage depuis Montpellier,
Toulouse, Marseille ou même Bordeaux.

![Agile Open Sud](/images/2013/01/agileopensud2013.png)

Il reste **3 places le 13 janvier 2013 (sur les 25)**, les inscriptions
se font [ici](http://is.gd/aoSud2013).

Pour en savoir plus, ou poser des questions : le [groupe agile occitanie
sur google](https://groups.google.com/forum/?fromgroups#!forum/agileoccitanie).

**Agile Open Sud 2012** (avec de multiples retours sur blogs en liens)
[là](/2012/03/agile-open-sud-2012-cest-fait-aussi/).

Le hastag sur twitter est \#aosud.

