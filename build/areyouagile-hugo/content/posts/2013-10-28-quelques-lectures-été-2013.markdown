---
date: 2013-10-28T00:00:00Z
slug: quelques-lectures-recentes-2013
tags: ['livre','lecture']
title: Quelques lectures récentes (2013)
---

Un petit moment que je n'avais pas fait de fiche de lecture, pourtant
2013 me semble une bonne année de lecture. La plupart de ces lectures
sont en relation avec la keynote "la horde agile" que j'ai présenté à
plusieurs Agile Tour cette année. Ces lectures m'ont porté jusqu'à ce
sujet. Beaucoup de mes réflexions sont en relation avec le cerveau
droit, l'émotion, notre culture, ce que nous sommes. Si quelqu'un à des
références ou des suggestions, je suis très preneur.

## Crucial Conversations

![Crucial Conversations](/images/2013/10/crucial-conversations.jpg)

Lu durant le printemps. Intéressant, mais un livre dont l'idée supplante
le livre. C'est à dire que l'idée est excellente, et que le livre en est
la déclinaison, mais il ne dépasse jamais l'idée et l'on reste un peu
sur sa faim. L'idée c'est que nous maitrisons très mal certains moments
clefs de notre vie. Ces moments clefs sont des conversations. Et trop
souvent nos faisons parler le chasseur de tigre à dent de sabre qui en
nous, par réflexe (pas de lien avec la *horde agile*, juste je reprends
là ce que dit le livre...), et ainsi nous échouons à maîtriser ces
moments clefs ( moments clefs ? autant dans la vie privée (relation de
couple) que professionnelle (relation avec nos collègues)). La
description des symptômes est très intéressante. C'est l'idée. Et
surtout le fait que de grandes choses se jouent dans ces petits moments
est essentiel. D'autant que nous sommes mal programmés pour ces petits
moments très influents. La mise en oeuvre de solutions me parait plus
conventionnelle. Un peu comme dans la *process com* elle fait appel à
une sorte de *meta-communication* (faîtes un pas en arrière et
communiquez sur la communication), ou des choses assez évidentes (la
vérité est toujours plus simple à dire). J'ai été donc moins sensible à
cette seconde partie, cependant je sais que les conversations ne sont
pas nécessairement des moments compliqués pour moi, tel que je suis, et
il me semble que j'applique déjà pas mal de recommandations énoncées
ici. Cette deuxième partie peut se révéler peut-être cruciale pour des
gens moins à l'aise avec l'oral. Je le recommande en tous cas car en peu
de mots il appuie des idées très importantes et très problématiques dans
notre vie de tous les jours.

Juste des fois j'aimerais que l'on nous explique comment aborder ces
questions en ne prenant pas de recul, mais en justement laissant
exploser le chasseur de tigre à dent de sabre qui est en nous...

## Introduction à la pensée complexe, Edgar Morin

![Introduction à la pensée complexe](/images/2013/10/p-complexe.jpg)

Indispensable.

Petit livre qui résume parfaitement toute la problématique dans laquelle
nous sommes plongés. Pourquoi l'agile ? parce que la pensée complexe. Il
faut le lire. On reproche des fois à Edgar Morin ses phrases "choc" à la
Paris Match, personnellement j'y vois une vulgarisation de bon aloi qui
me sied bien (28 points bingo pour cette dernière phrase).

Je le répète : indispensable.

## Le paradigme perdu, la nature humaine, Edgar Morin

![Le paradigme perdu : la nature humaine](/images/2013/10/paradigme-perdu.jpg)

"Notre nature est notre culture, et notre culture est notre nature."

Comment Edgar Morin projette ses réflexions sur nos ancêtres. Comment
Edgar Morin passe de l'homo sapiens à l'homo sapiens demens. Comment il
explique les spécificités de notre espèce, nos comportements actuels, et
à venir (l'homo péninsulaire). En plein durant la naissance de la
psychologie évolutionniste, un livre remarquable pour qui s'intéresse
aux sujets que j'ai pu évoqué durant "la horde agile", comme notre
nature, notre culture, nos modes de pensées, adaptés, à adapter, etc.

## Un univers complexe, Marc Halévy

![Un univers complexe](/images/2013/10/univers-complexe.gif)

J'ai pu avoir le plaisir de voir Marc Halévy, un philosophe
prospectiviste physicien, durant des conférences organisées par le CJD.
J'y ai retrouvé un écho fort à toutes ces idées qui me plaisent sur le
monde complexe, et l'obsolescence du mécaniscisme, du rationalisme
cartésien. A noter que le thème des conférences était ... l'agilité
(grand thème national du CJD durant 2 ans). Mais là où il m'a scotché
c'est lors de sa présentation de "la grande bifurcation". Il annonce une
bascule entre les deux mondes qui s'opposent : l'un cartésien,
mécaniciste, rationel, l'autre complexe, systémique, organique. Celle-ci
pointerait son nez dans les années à venir : d'où la crise, mais alors
vive la crise ! Ce livre : un univers complexe, est surtout cette
constatation, cet état des lieux (un état des lieux parfois compliqué
pour moi qui ne suis pas un grand physicien...). Et qui s'achève sur
cette ouverture vers ce nouveau paradigme (à le lire on comprend vite
que Edgar Morin est l'une de ses sources). La bifurcation serait :
d'*une économie de masse vers une économie de niche, d'une économie de
volumes vers une économie de marges, d'une économie capitalistique vers
une économie humanistique, d'une économie du standard vers une économie
du génie, d'une économique de la productivité, vers une économie de
l'agilité, d'une économie de la taille, vers une économie de la
frugalité, d'une économie du pillage, du prix, vers une économie de la
valeur*. Cette phrase vient de ses slides, mais elle se retrouve peu ou
proue dans le livre. Cependant le coeur de son thème sur la bifurcation
doit apparaître dans son livre -en octobre 2013...-. Ce "monde complexe"
c'est le postulat, le décors. La crise, le changement, la bifurcation
est celui à venir.

Au passage de belles théories sur la mémoire accumulée, un big bang
revisité, nos lois de la physique expliquées différement, etc.

## La spirale dynamique, F&P Chabreuil

![Spirale Dynamique](/images/2013/10/spirale-dynamique.jpg)

De fait je retrouve aussi chez Marc Halévy, cette prémonition (toute
calculée, analysée) de Clares Graves et de sa spirale dynamique. Lequel
nous dit que nous transitons en ce moment d'une civilisation
rationalisée, déshumanisée, vers une civilisation de la coopération. Ce
petite livre rappelle toute sa théorie autour de la *spirale dynamique*.
Qui sous des dehors *new age* se révèle passionnante. Comme toujours
avec le new age, c'est le packaging qui est mauvais, mais les idées sont
souvent bonnes. La spirale dynamique présente donc une grille de lecture
du niveau de maturité des civilisations et donc des cultures des
entreprises qui nous entourent. Elle propose un agencement, des étapes
et se projette (et là je dis chapeau) sur le futur. Je dis chapeau car
il semble que ce que nous vivons donne raison à Clares Graves.

## Jeu de culture, INFOQ, de Dan Mezick, la traduction de Olivier Destrade

Que vous trouverez
[ici](https://docs.google.com/file/d/0B8bg756IEYMhdlVrWWxMaVUycVU/edit).

A lire, la belle petite traduction en français de Olivier Destrade, sur
le minibook de Dan Mezick chez InfoQ (qui est un sous-ensemble de son
"Culture Game"). Pratique, intéressant, si on réussi à survivre aux deux
prologues qui eux sont superfétatoires (bonus 50 points, bing !).
Cependant à la lecture du libre entier je note que ce sous-ensemble qui
a le mérite d'être en français (merci Olivier) ne se suffit pas à lui
même : il faut lire le livre complet. Mais il propose une bonne entrée
en matière : comment changer la culture de votre entreprise en
respectant votre entreprise et les personnes qui la compose. Comment
prendre un peu de recul pour se situer entre la culture, les valeurs et
les pratiques. Le bon niveau en fait : une façon de proposer des règles
qui prendront leurs formes définitives chez vous.

## Lectures en cours (trop à la fois en l’occurrence...):

-   **L'homme qui prenait sa femme pour un chapeau** de **Olivier
    Sacks** (merci Stéphane Langlois, le début est déjà passionnant) en
    complément des histoires sur Phineas Gage, sur l'importance du
    cerveau droit.
-   **The Culture Game** de **Dan Mezick** naturellement, je connais
    déjà pas mal le sujet grâce à la venue de Dan et à la traduction du
    minibook de InfoQ, mais je suis toujours bluffé par l'esprit de
    synthèse de Dan qui ramène bieb à l'essentiel et trouve le bon cadre
    pour l'exprimer. Pour moi **le** livre du moment.
-   **Food of the gods**, **Terence McKenna**, vous savez les
    champignons hallucinogènes à la source de l'évolution de l'espèce...
-   **Spirit** de **Harrison Owen**, le livre de référence de Dan Mezick
    sur l'openspace.

## À venir

-   Reality is broken de Jane McDonegal, encore une reco de Dan Mezick.
-   Le dernier de Marc Halévy.

Toujours des trucs de sciencefi ou autre pour me vider la tête de temps
en temps (L'homme rune tome 1 & 2, récemment par exemple)

### Lectures, les liens depuis 2010

-   [Lectures automne/hiver 2014](/2014/03/lectures-automnehiver-2014/)
-   [Fiche de lecture 2013 : entre le cristal & la fumée (Henri
    Atlan)](/2013/12/auto-organisation-et-storytelling/)
-   [Lectures automne 2013](/2013/10/quelques-lectures-recentes-2013/)
-   [Lectures automne/hiver 2013](/2013/04/lectures-automnehiver-2013/)
-   [Lectures printemps/été 2012](/2012/10/lectures-printempsete-2012/)
-   [L'empire des coachs (Gori &
    LeCoz)](/2012/04/lecture-lempire-des-coachs-de-gori-le-coz-2006/)
-   [Quelques bouquins à lire pour faire de
    l'agile (2011)](/2011/12/quelques-bouquins-a-lire-pour-faire-de-lagile/)
-   [Fiche de lecture 2010 : Karmic
    Management](/2010/08/fiche-de-lecture-karmic-management-roachmcnallygordon/)
-   [Fiche de lecture 2010 : Scrum (de Claude
    Aubry)](/2010/08/fiche-de-lecture-scrum-guide-pratique-claude-aubry/)

