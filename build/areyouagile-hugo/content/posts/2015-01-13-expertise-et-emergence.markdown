---
date: 2015-01-13T00:00:00Z
slug: expertise-et-emergence
tags: ['complexite','complexe','modernite','emergence']
title: Expertise et émergence
---

Alors que j'accompagne de plus en plus mon associé, et surtout ami,
[Gilles](http://mvp.microsoft.com/fr-fr/MVP/Gilles%20Pommier-5000494),
dans son monde de MVP SharePoint, pour tous les aspects *transformation
et management organisationnel*, notamment sur **Office365** (voir [Open
Office365 Adoption](/2014/12/open-office365-adoption/)), ce qui me
frappe c'est l'importance du mélange entre **expertise** et
**émergence**. Je le savais, mais là, dans ce monde d'experts liés à des
grosses solutions logicielles, cela me frappe, la nécessité de la double
approche est évidente.

Par contre il faut bien comprendre que cette double approche mêle des
façons de faire divergentes voire opposées et donc qu'il faut savoir
manier à bon escient.

## Ferrari & mayonnaise

Cela fait appel aux contextes de l'expertise et de la complexité. Pour
reprendre un exemple parlant (de Edgar Morin ? je ne sais plus vraiment)
nous avons d'un côté la Ferrari : on va savoir la construire si on
s'entoure de la bonne personne, des bonnes personnes. Ceux qui savent
comment une Ferrari se construit. On sait que l'on va savoir faire si on
trouve les bons experts. La difficulté avec les experts c'est plutôt a)
les mettre d'accord, trancher à un moment, et pas trop tard b) ne pas
s'ankyloser dans des réponses classiques quand le contexte a changé ou
attend d'autres réponses.

De l'autre coté, c'est le coté émergent, complexe, c'est la mayonnaise.
On va réussir à la faire, probablement. **Probablement**. A peu près à
ce moment, **a peu près**. Mais rien n'est sûr et cela va beaucoup
dépendre du contexte. Dans cette complexité il faut savoir attendre la
bonne émergence et provoquer beaucoup d'interactivité pour enrichir la
réflexion collective.

## Office365, Atlassian, Liferay, etc

Dans toutes les grandes solutions techniques que mes camarades chez
[SmartView](http://www.smartview.fr) portent, une adoption passe par un
savant mélange de ces différents ingrédients. Dans vos adoptions c'est
pareil. Si vous laissez entièrement la main aux experts, et si je
caricature : vous aurez une solution qui marche. Quand à savoir si elle
est utile, adaptée, c'est une autre histoire. A l'inverse si vous
laissez entièrement la main à l'émergence vous aurez une gros capital
apprentissage et adaptation. Mais souvent dans ces solutions techniques
des erreurs ou des incompréhensions peuvent entraîner de grosses
limitations ou de grosses refontes, si elles ne sont pas connues au bon
moment (par avance = expérience = expertise), c'est dommage, cela coûte.

Ainsi il faut savoir marier les deux mondes : une approche émergente
adaptative qui est encadrée par les experts. Les experts sont les
contraintes ou les sources d'informations relatifs au *comment*, le
conteneur. Vous êtes le contenu. Naturellement pour que cette synergie
fonctionne elle doit avancer au même rythme, en parallèle. Ainsi de
nouveau (et pour plein d'autres raisons), des cycles adaptatifs sont
nécessaires.

Expertise et émergence ne sont pas à opposer.

Expertise et émergence sont complémentaires dans nombreux de contextes.

Chacun de ces mondes demande des réponses et des décisions différentes
(voir par exemple cet [article](/2013/04/cynefin-et-son-lego-game/) sur
les différents contextes).

