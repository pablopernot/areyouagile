---
date: 2013-08-14T00:00:00Z
slug: appelez-cela-comme-vous-voulez
tags: ['porno','agile']
title: Appelez cela comme vous voulez
---

J'entends beaucoup ces temps-ci : "l'agile c'est fini, c'est mort", et
je suis curieux : le monde serait-il devenu beaucoup plus prédictible
durant mes vacances ? Aurait-on trouvé le moyen d'innover sans autoriser
le droit à l'échec ? Serait-il finalement possible d'optimiser nos
travaux avec des gens très malheureux ?

Naturellement non.

C'est juste que comme souvent on a apposé le mot "agile" sur une équipe,
un département, etc sans vraiment prendre deux choses en compte. La
première, la plus mineure, se donner les moyens dans la pratique de le
mettre en oeuvre, la seconde, beaucoup plus importante, en comprendre la
pensée, la culture. (La première est mineure car c'est la forme, la
seconde c'est le fond, le fond entrainera la forme).

Cela partait probablement d'un bon sentiment : une volonté de
changement, de relance. La nouveauté de l'agilité, son aspect ludique et
le bon sens flagrant dont il fait preuve, attire beaucoup d'envie. Tant
mieux.

Bref, on s'en empare, et c'est bien.

Mais souvent cela ne va pas plus loin. Ou plutôt cela ne va pas assez
loin. Je vois des équipes en déréliction. Enfin bon c'est toujours la
même question. Sans savoir pourquoi on veut faire les choses, on les
fait mal, on devient frustré et on rejette. On ne sait pas ce que l'on
rejette mais on le rejette, cet agile. Cela a échoué. Mais qu'est ce qui
a échoué ? Si vous formuliez juste ce qui a échoué et quel était le but
poursuivi. Si en connaissant de cause vous analysiez votre implication à
la poursuite de ce but, et les moyens que vous lui avez donné.

L'agile c'est un mot qui dit : le monde est devenu complexe. Le complexe
c'est l'acceptation de l'incertitudes et de contradictions. Pour palier
à ces incertitudes et à ces contradictions il faut responsabiliser les
gens, et du feedback, de la transparence, de la communication, le droit
à l'échec (au mieux dans des intervalles de temps qui limitent la portée
de celui-ci), etc.

Appelez cela comme vous voulez, l'agile n'a pas disparu, et ne
disparaitra pas de sitôt. Peut-être le mot va-t-il disparaître, pas ce
qu'il véhicule.
