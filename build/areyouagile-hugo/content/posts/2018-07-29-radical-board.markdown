﻿---
date: 2018-08-30
slug: extrospection-collective
tags: [radical, board, candor, carte, force]
title: "Extrospection collective"
---

L'introspection est une activité que tout coach -- quel qu'il soit -- se doit de réaliser régulièrement. On peut la nourrir avec du feedback, avec de l'extrospection collective. Toutes les deux semaines chez [beNext](http://benextcompany.com) c'est la journée dit "des poètes disparus", plus prosaïquement : celle des coaches. Durant ces journées tout le monde veut apprendre, veut progresser. On alterne un format holacratie/sociocratie[^gotham] avec de la supervision, ou des présentations. On y chasse souvent les non-dits pour garantir la bonne santé du groupe. Cet été on a pu y réaliser deux séances d'extrospection collective. Nous avons besoin de constamment avoir du feedback des autres nous concernant, surtout si il s'agit de pairs. Sur nos comportements quels qu'ils soient. Le groupe est bienveillant et assez mature, c'est donc possible.

Il faut savoir faire disparaître les non-dits.

Il faut savoir se dire les choses. 

Nous souhaitons nourrir l'introspection de chacun. 

## Le Radical Board

Le "radical board" est spontanément né durant ces journées en mixant une matrice *give and take* et l'idée de la candeur radicale (*radical candor*)

### Du RACI aux besoins

Au début fut le [RACI](https://fr.wikipedia.org/wiki/RACI). Une liste sèche sous forme de matrice des responsabilités et des différents niveaux de culpabilité.  Avec le RACI chacun construit sa citadelle et on oublie les espaces intermédiaires. Chacun décrit son pré carré, et il se creuse des écarts entre les groupes. 

Puis est venu le "Give and Take" dans lequel on décrit les interactions entre les groupes au travers d'une matrice. Ce tableau croisé entre les groupes remplace intelligemment le RACI (trop figé, trop fossilisé dans sa façon de penser). "Je te donne ça et je prends ça de toi". Donner, prendre, on construit une dynamique, mais ces verbes restent un peu secouants, tranchants. On est encore un peu dans le troc, dans la négociation.

Peu à peu les gens bien intentionnés ont transformé le "Give and take", en "j'ai besoin de ..." (moi c'est [Dragos](http://andwhatif.fr) qui m'avait montré cela). Toujours une matrice, mais chaque groupe indique ce qu'il attend, ce qu'il espère, d'un autre. Les écarts ne se créent plus, ils deviennent visibles et donc appellent à être comblés.  

![Needs board](/images/2016/10/needs.jpg)

Dans l'image ci-dessus l'équipe de développement commercial, celle des juristes, celle de la comptabilité, celle de la fiscalité, et enfin celle de la trésorerie indiquent de quoi chacune à besoin en désignant le groupe de chez qui elle imagine que cela pourrait venir. Ici le contexte était celui du rachat d'un espace événementiel. Dans l'exemple l'équipe des Juristes indique qu'elle attend quatre éléments de la part de l'équipe développement commercial. On se rendra compte que seuls deux sont bien réalisés par celle-ci. On trouvera la solution pour combler le manque.  

### Radical Candor

Vous vous souvenez de ce [TED sur "radical candor"](https://www.youtube.com/watch?v=4yODalLQ2lM) ? OK il était long, poussif, en un mot, ennuyeux. Mais il soutenait une idée intéressante : être radicalement franc, d'une franchise brutale, dans ses retours auprès des gens. Sous-entendu une franchise sans jugement, une franchise directe, qui épargne mille circonvolutions, et qui donne radicalement son ressenti, son avis. C'est aussi un livre, je crois, mais je ne l'ai pas lu. 

On a pu détourner le "j'ai besoin de" de la matrice évoquée plus haut, en "j'aimerais plus de cela chez toi" ou "il faudrait que tu saches entendre cela (mais tu en fais ce que tu veux)" mais avec l'esprit de cette candeur radicale. Nous sommes bienveillants et matures, nous pouvons nous dire les choses qui pourront nourrir chacun notre introspection et nous aider à nous améliorer.

![Radical board](/images/2018/08/radical-board.jpg)

C'est devenu très intéressant quand les différents points de vue que nous recevions des autres coaches...convergeaient. 

## Les cartes des forces

La semaine dernière c'est un autre format d'extrospection collective que nous avons choisi. Alexandre (aussi appelé "Le grizzli", aussi appelé "le dentiste", aussi appelé "belle praline", [c'est curieux comme les gens sont méchants](https://fr.wikiquote.org/wiki/Les_Barbouzes)) avait amené son jeu de ["cartes des forces"](https://www.positran.fr/produit-319724-cartes_des_forces.html). 

![Cartes des forces](/images/2018/08/cartes-des-forces.jpg)

À l'instar d'un atelier [*extreme quotation*](/2017/09/estimation-visuelle/) nous avons silencieusement et collectivement choisi trois cartes de forces désignant les qualités de l'un de nous à tour de rôle. En silence, sauf si un dilemme subsistait vraiment (là on parle et on vote). Si quelqu'un amène une quatrième force, il doit en retirer une. Quand cela se stabilise, on obtient un résultat. 

![Cartes des forces 2](/images/2018/08/cartes-forces.jpg)

C'est toujours amusant d'être le témoin de ce qui émerge pour soi-même. Naturellement nous n'avons pas pu nous empêcher aussi d'évoquer les points d'amélioration de chacun, les forces à travailler. Je vous livre les résultats, les autres coaches sont OK. 

#### Forces

Dans l'ordre de passage, avec les coaches présents.

Coach | Forces   
 --- | ---
Yoann      | Critique, Humilité, Curiosité 
Dragos     | Stratégie, Sagesse, Responsabilité 
Fleur | Planification, Courage, Sens du détail 
Pablo | Créativité, Leadership, Vitalité
Laurence | Humour, Action, Authenticité
Alexandre T. | Gentillesse, Travail d'équipe, Mise en relation 
Alexandre M. | Conscience de soi, Ouverture d'esprit, Spiritualité 
Pauline | Aventure, Ouverture d'esprit, Empathie 
Nils | Responsabilité, Travail d'équipe, Action

#### Forces à travailler 

Dans l'ordre de passage, avec les coaches présents.

Coach | Forces à travailler 
 --- | ---
Dragos | Séduction, Pardon, Travail d'équipe 
Pauline | Persuasion, Optimisation du temps, Stratégie 
Yoann | Compétition, Optimisme, Persuasion
Alexandre T. | Pardon, Explication, Prudence
Alexandre M. | Solution, Responsabilité, Adaptabilité 
Pablo | Humilité, Gratitude, Maîtrise de soi
Laurence | Adaptabilité, Créativité, Personnalisation 
Fleur | Optimisme, Séduction, Humour
Nils | Sagesse, Stratégie, Critique 

Cela m'a permis de nourrir mon introspection. 

Cela m'a permis d'évacuer des non-dits. 

Cela m'a permis de dire de choses à mes camarades de façon intelligente. 


[^gotham]: Chez beNext, nous avons notre recette personnelle que l'on appelle *gothamocratie*.  
