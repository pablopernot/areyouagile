---
date: 2013-03-05T00:00:00Z
slug: pourquoi-une-user-story-technique-est-un-aveu-dechec-2
tags: ['user','story','backlog','userstory']
title: Pourquoi une *user story* technique est un aveu d'échec 2
---

Je reviens sur cette *user story* technique qui comme le chewing-gum
semble s'être agrippée aux godasses de plusieurs. Je rebondis sur
quelques conversations twitter sur et sur l'[article de Damien](http://www.agile-it.fr/post/2013/03/05/User-Story-Technique-or-not-Technique.aspx)
qui a eu la gentillesse de me répondre et de faire référence à mon
[précédent article](/2013/03/pourquoi-une-user-story-technique-est-un-aveu-dechec/).
Et qui plus est, propose potentiellement une table ronde / atelier-débat
à agile france 2013 sur le sujet (je ne sais pas si cela se fera).

Je rappelle un truc fondamental : ce que je présente c'est mon avis
aujourd'hui. J'essaye de l'expliquer, de le confronter. Je peux me
tromper. Je peux me tromper souvent. Je peux changer d'avis, je peux ne
pas changer d'avis. Etc. etc.

## KISS de la pensée

Dans ma façon d'aborder les sujets j'essaye de respecter le "KISS de la
pensée". *Keep it simple stupid*. Ce mojo agile veut dire tout
simplement, et vous l'avez compris : qu'à mon avis, pour s'en sortir, il
faut réussir à synthétiser une pensée simple (KISS est généralement
utilisé pour le code, d'où l'analogie). Une pensée simple ce n'est pas
penser simplement, c'est ramener une pensée à une esthétique simple, à
une conceptualisation élégante, mais pas dans le sens raffinée, dans le
sens dépouillée, épurée. C'est la seule façon d'évoluer sainement dans
notre monde complexe il me semble. Si on arrive avec un système de
pensée compliqué, on se noie. En tous cas, c'est mon cas. Je dois
manquer d'équipement.

Bref c'est peut-être principalement à cause du "KISS de la pensée" que
je diffère un peu de mes compagnons (Damien, Claude, etc.). Là ils
cherchent en toute bonne foi des solutions, des ouvertures, des
alternatives, je m'y refuse, où du moins j'essaye de répondre
différemment.

## Déviances pour des déviances

Damien évoque des projets avec une grosse dette technique (que nous
croisons tous malheureusement). Il invoque ainsi la nécessité sous
couvert de bénéfices de *user stories* de refactoring aka techniques.
Mais c'est que le refactoring est lourd là ! et qu'il n'aurait pas pu
être anticipé (là je demande à voir malgré tout... et quid du
refactoring tout au long du dév ?). **Je ne souhaite pas justifier des
déviances (user story technique) par des déviances (on a une énorme
dette technique)**, sinon c'est le début de la fin.

## Oui mais Pablo, là on est bloqué alors quoi ?

Je rappelle ce que j'ai écrit [précédemment](/2013/03/pourquoi-une-user-story-technique-est-un-aveu-dechec/)
: vous pouvez faire des user stories techniques **à condition** que cela
vous démange, vous gratte, et que vous vous interrogiez pour que cela
n'arrive plus à l'avenir ou s’atténue fortement.

Dans le cadre du très gros projet nécessitant du refactoring évoqué par
Damien dans une optique KISS et pour aller au bout de la démarche :
faite du cœur du projet le refactoring. Le projet n'est plus qu'un
projet technique. Son métiers est devenu la technique. Satisfaire le PO
et les parties prenantes c'est refactoriser.

Ce n'est pas tricher, c'est penser différemment. Soit on n'inclut pas
des *user stories* techniques dans un backlog, soit on décide que
l'intégralité du projet est technique. **KISS**. Les autres solutions
sont des aveux d'apprentissage, d'échec, des pis-allers (et ce n'est pas
un drame aussi... bon sang). Les autres solutions vont vous apprendre à
"malapprendre", à "malpenser".

Naturellement je ne suis pas pour cette solution. Mon vécu me fait
penser que revoir uniquement techniquement une énorme application est
souvent voué à l'échec (note pour les lecteurs : je me trompe souvent).
Ce n'est pas la pensée, la culture agile, à mes yeux. Mieux à mes yeux :
on reprend, itérativement, une application, et donc les *user stories*
techniques n'existent pas et la question technique est traitée sous
couvert d'aspects fonctionnels. Finalement je ne change pas d'avis :)

## Long terme et ROI

![Damien](/images/2013/03/tweet-stories-tech4.png)

Je cite Damien : "Le rôle du PO n’est pas de satisfaire les
utilisateurs. Il est de maximiser le retour sur investissement du
produit. La différence, pour moi, se situe à deux niveaux : la prise en
compte de l’ensemble des parties prenantes au projet, d’une part, et la
prise en compte du long terme d’autre part".

Long terme et ROI pour justifier des *user stories* techniques ? Je ne
comprends pas.

Naturellement que les aspects techniques doivent prendre en compte ces
objectifs. En quoi cela justifie-t-il des *user stories* techniques ?
Non je ne comprends pas. Au contraire j'insiste sur la liberté qui est
du coup donnée aux aspects techniques en les libérant du fardeau de la
justification : une *user story* est métiers MAIS vous êtes les garants
techniques de la pertinence technologique, de sa mise à jour continuelle
(refactoring), et son évolutivité, etc. La user story technique est
morte, vive la technique !

## Pas de tyrannie

![David](/images/2013/03/tweet-stories-tech6.png)

Désolé David, j'ai échoué. Je n'ai pas mis FUN dans mon post. Mais j'ai
l'impression que tu t'arc-boutes sur tes souffrances de "tech". Je ne
veux pas, peux pas, te convaincre. Je te dis juste que de mon expérience
(ce que j'ai vécu) cette approche est vécue comme une libération et une
réappropriation d'un vrai statut pour les devs/techs, etc. Il ne doit
pas y avoir de tyrannie du métiers et il ne doit pas y avoir de tyrannie
de la technique. C'est presque finalement la meilleure justification de
ce mal aimé de scrummaster.

Cette "peur", "frustration", "dégout" que tu décris, je l'ai aussi
croisé dans le "camp d'en face" (le métiers). Donc encore une fois : Il
ne doit pas y avoir de tyrannie du métiers et il ne doit pas y avoir de
tyrannie de la technique. Chacun a sa place dans cet écosystème.

Cependant, on règle cela autour d'un bras de fer à agile open sud.

![JB](/images/2013/03/tweet-stories-tech7.png)

Quelques tweets avec JB dont j'ai repris le contenu plus haut. Le
[lien](https://agilarium.wikispaces.com/La+v%C3%A9locit%C3%A9+est+en+train+de+tuer+l%27agilit%C3%A9)
qu'il évoque de Jim Highsmith. Pour moi on est tout à fait là dans la
justification de déviances par d'autres déviances. Il faut reprendre les
problèmes à la source, tout simplement. Donc les arguments de Jim
Highsmith, oui... mais non. Il a raison, mais ce n'est pas notre sujet.
Il décrit des déviances. Je décris mon mode de pensée (agile j'espère).
