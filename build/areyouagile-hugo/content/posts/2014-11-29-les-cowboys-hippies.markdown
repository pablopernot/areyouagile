---
date: 2014-11-29T00:00:00Z
slug: les-cowboys-hippies
tags: ['agile','lean','cowboy','hippie']
title: Les cowboys hippies
---

Y'a un truc qui m'amuse et m'agace : le retour en force du mot **Lean**.
Lean Startup, Lean Canvas, Running Lean, Lean manager, Lean par-ci Lean
par là. Mais qu'est ce qui se passe ? Le futur de agile dans le Lean ?
Ah ah cette blague !!!

La sur-utilisation -- même au sein de la communauté agile -- du mot Lean
est tout à fait compréhensible pour plusieurs raisons, mais aucune
d'elles n'est bonne :

## Lean ça fait plus sérieux mais c'est dépassé

Soi-disant, Les agilistes sont des cowboys hippies, les gars du Lean,
c'est du sérieux, ça vient de l'industrie (ça y est on vient de se
planter). Pour tous les corps de métiers qui n'actent pas du changement
de paradigme de notre [période
complexe](/2014/03/les-temps-complexes-revisitons-agile/) c'est très
pratique d'invoquer le Lean, soi-disant de Toyota (Le [Lean de
Toyota](/2014/09/petit-rappel-historique-ford-toyota/)). On commence à
payer le retour de flamme de l'agilité : de trop nombreuses
incompréhensions, récupérations, échecs font la promotion d'un agile
complètement dévoyé. C'est exactement ce qui est arrivé au Lean des
années 1990. Donc pour faire sérieux, pour ne pas dire "agile" qui fait
peur au système en place (mais oui il décrète la fin du système
précédent...), on parle de Lean. Ce n'est pas une bonne raison. C'est
encore une fuite. D'autant plus que le Lean que l'on évoque est
archaïque. On évoque le Lean du non-gaspillage : on oubli complètement
celui du respect des personnes et de l'amélioration continue.

La période n'est plus au Lean, standardisation, amélioration continue
sur l'élimination des gaspillages par implication et respect des
personnes. La période est à l'adaptation, à la NON standardisation, à la
NON linéarité, à la NON répétabilité, induites par la [complexité de nos
temps](/2014/03/les-temps-complexes-revisitons-agile/). Adaptation
plutôt que standardisation (comme l'avait si bien glissé
[Romain](http://romain.vignes.me/) lors d'une discussion).

## Lean c'est pour les grandes structures...

Autre mythe, le Lean (et soi-disant pas l'agile) s'adapte aux grandes
structures, aux organisations "à l'échelle", c'est encore l'image non
adaptée de l'industrie qui surgit. Or nous savons que : a) l'agile est
tout à fait adapté pour des organisations de très grandes tailles, et
que b) la solution de la mise à l'échelle [ne proviendra pas d'une
industrialisation](/2014/10/des-bidonvilles/) (en tous cas c'est ma
conviction).

## Marketing

Lean Startup, MVP, etc. du bon marketing, mais n'en faisons pas plus. On
va rétorquer que le mot agile c'est pareil : du bon marketing. Oui en
partie, c'est un mot fourre-tout, mais au moins il ne pas fait appel à
des notions qui sont dépassées, il porte des réflexions et des notions
du monde actuel.

Le Lean Startup, par exemple, ne se base d'ailleurs pas du tout sur le
Lean industriel mais sur son pilier "kaizen" : amélioration continue.
Mais pas du tout une amélioration continue par une équipe de travail
dans un cadre industriel d'une chaîne d'usine. Ce "Lean Startup" devrait
en fait s'appeler "Agile Startup" car il implique surtout des notions
d'adaptabilité au marché. C'est d'ailleurs le mot Startup qui rappelle
que cette approche réactive, adaptative, est liée au marché.

Lean startup -&gt; Agile Startup -&gt; pléonasme -&gt; Agile.

Bref "Lean Startup" c'est juste la traduction marketing de "Agile", qui
lui même est la traduction marketing de "temps complexe".

## "Lean", les derniers sursauts d'un monde passé

Pour finir, ce qui me dérange dans cette utilisation abusive c'est
surtout le retour de vieux motifs, la résistance de l'ancien système :
le Lean industriel, l'idée que l'on mettra à l'échelle en homogénéisant
de façon matricielle (industrielle) qui n'ont plus d'être.

Agile est à Lean, ce que Lean était au Fordisme.

J'aimerais que les agilistes qui pour des raisons marketing s'orientent
vers le Lean aient...la dignité de (se) le rappeler.

## Lookin' for adventure and whatever comes our way

<iframe width="420" height="315" src="//www.youtube.com/embed/rMbATaj7Il8" frameborder="0" allowfullscreen></iframe><br/>

## Feedback

### Thomas

![Feedback](/images/2014/11/lean-feedback1.png)
