---
date: 2018-01-01T00:00:00Z
slug: good-work-best-skills-love-believe
tags: ['valeur','sens','introspection','intuition','envies','envie','competences','competence']
title: Good work = best skills + what we love doing + what we believe in
---

On me demande de faciliter et d'organiser ce contexte ? Mais quel est ce contexte ? Et qui me le demande exactement ? Pour quelles raisons ? Quel est l'objectif ? La raison ? Porte-t-il un sens ? Est-il partagé ? Est-ce que je suis en train de répondre convenablement aux attentes ? Est-ce que je suis aligné avec ces attentes, cette raison, ce sens ? Est-ce que j'ai besoin d'être aligné ? Est-ce que mes actes de hier ont servis ce sens ? Pourquoi ces personnes ont-elles agi ainsi ? Pourquoi cette personne a-t-elle dit cela ? Est-ce que ma posture était la bonne ? Qu'aurais-je du faire différemment ? Qu'est ce que je regrette et qui pourtant a été bénéfique ? Qu'est ce qui a été surprenant ? Qu'est ce que cela risque de provoquer ? Comment cela a-t-il perçu ? Que devrais-je lire ? Qui devrais-je écouter ? Y-a-t-il un lien entre ce discours et mon contexte ? Est-ce que mon analyse n'est pas biaisée ? Qu'est ce que je devrais essayer de différent ? 

Toutes ces questions, et mille autres, je me les pose constamment. Je suis constamment en introspection. Avoir beaucoup d'introspection pour évoluer soi-même, sur un plan individuel, dans cette complexité qui nous entoure. Se laisser surprendre, essayer, apprendre. L'introspection est la réponse à la complexité sur le plan individuel. L'introspection passe par le questionnement. Le questionnement est aussi la réponse pour appréhender à l'extérieur cette complexité. Interroger les gens avec de belles questions, appelez ça questions puissantes, paroles de girafe[^girafe], questions de *learner*[^learner] (mon favori), celles du *clean language*, etc c'est un apprentissage primordial. Se questionner, questionner.

Toutes ces questions, et toutes les observations des réponses amenées, bâtissent peu à peu un référentiel. Un référentiel de motifs, d'histoires[^atlan], d'essais et de questionnements qui viennent s'agréger dans notre cerveau. L'apprentissage c'est l'intégration de ces motifs. Plus on apprend plus on intègre de motifs pour différencier les situations. Cette ruche de motifs qui s'agrègent dans mon cerveau pour ressortir de façon qui semble spontanée c'est mon intuition. C'est votre intuition. 

L'intuition c'est l'absence de mot mais pourtant une sensation claire. Car toutes ces choses que nous avons amassées (motifs, histoires, expériences, questionnements) ont été se loger dans ce vieux cerveau qui contient bien plus de choses que notre nouveau cerveau (physiologiquement parlant), mais qui n'a aucune liaison avec les mots du fait de son ancienneté. 

Autrement dit, par Daniel Goleman ([Daniel Goleman: "Focus: the Hidden Driver of Excellence" -- Talks at Google](https://www.youtube.com/watch?v=b9yRmpcXKjY)), bien se connaître (*self-awareness*), bien se gérer (*self-management*), avoir de l'empathie sont les piliers de vos compétences sociales, de votre intelligence émotionnelle. Dans le monde complexe, c'est cette intelligence qui est recherchée.  

Pour lui il est essentiel de constamment s'interroger : est-ce que je suis toujours en phase avec mes valeurs et le sens que je recherche ? (*sense of purpose, value and meaning*). C'est cette conversation sans fin avec soi-même que je décris plus haut. 
Daniel Goleman complète en citant un de ses confrères, Howard Gardner, le bon boulot (*good work*) vient quand vous associez vos meilleures compétences avec ce que vous aimez et ce en quoi vous croyez. 

> *good work = best skills + what we love doing + what we believe in* 
> -- Howard Gardner cité par Daniel Goleman [ici](https://www.youtube.com/watch?v=b9yRmpcXKjY)

Avec cette combinaison on obtient un état d'attention que l'on appelle "**efficacité cognitive maximum**" (*maximum cognitive efficiency*) ou encore "**harmonie neurologique maximum**" (*maximum neural harmony*).  

Ce que l'on appelle aussi souvent **le flow**. 

**Constante introspection, une intuition que vous bâtissez au fil des jours, et un "flot" qui peut jaillir si vous êtes en accord avec vos envies et vos convictions (vos valeurs et le sens)**.  

![Daniel Goleman et le *flow*](/images/2018/01/goleman-flow.png)

en français : 

> *bon boulot* = meilleures compétences + ce que vous aimez faire + ce que en quoi vous croyez 

Ainsi donc : 

**Valeurs, sens, introspection, intuition, vos envies, vos compétences**

merci, et bonne année 2018. 

ps : quand je regarde la [vidéo de Howard Gardner](https://www.youtube.com/watch?v=IfzrN2yMBaQ), il dit "excellence, engagement et éthique" 

[^girafe]: Communication Non Violente de Marshall Rosenberg  
[^learner]: "Change your question, change your life", Marilee Adams
[^atlan]: [Auto-organisation et storytelling](/2013/12/auto-organisation-et-storytelling/)