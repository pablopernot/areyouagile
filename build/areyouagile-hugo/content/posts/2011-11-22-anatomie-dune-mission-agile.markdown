---
date: 2011-11-22T00:00:00Z
slug: anatomie-dune-mission-agile
tags: ['conference','sudweb']
title: Anatomie d'une mission agile
---

Alors que [Sudweb](http://sudweb.fr/) version 2012 lance son [appel à orateurs](https://docs.google.com/spreadsheet/viewform?formkey=dGI2TTRseEZQeFpGbHpoUC1IN3h1cXc6MA),
certains enregistrements des sessions de l'année dernière émergent dont
mon intervention "anatomie d'une mission agile". Voici donc la vidéo
(merci [Sudweb : l'annonce de la session en 2011](http://sudweb.fr/post/Anatomie-d-une-mission-agile)) et les
slides.

*Vous avez aussi la version 2 (saison 2) dans les liens en pied de page,
vidéos & slides.*

## La vidéo

<iframe src="http://player.vimeo.com/video/53349009" width="500" height="283" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

## Les slides

<script async class="speakerdeck-embed" data-id="4f9cf100b3f2f3001f00cb92" data-ratio="1.44837340876945" src="//speakerdeck.com/assets/embed.js"></script>

