---
date: 2012-06-01T00:00:00Z
slug: introduction-a-la-strategie-et-la-tactique-du-product-owner
tags: ['po','productowner','strategie','tactique']
title: Introduction à la stratégie et la tactique du Product Owner
---

Mandaté par l’un de mes clients pour mettre en place le portfolio des
projets du groupe (1000 collaborateurs), j’ai sollicité plusieurs
personnes que je soupçonnais d’avoir, chacunes à leur niveau, assez
d’expérience, de recul, de culture d’entreprise et d’esprit de synthèse
pour contribuer à l’objectif : une vision d’ensemble, une cartographie,
et sa projection dans le temps. Au fil des discussions, j’ai été
interpellé par une nuance à laquelle, jusque là, je n’ai jamais fait
trop attention. Projets tactiques, projets stratégiques… 

Se noue alors un dialogue avec **Alexis Beuve**:

– Alexis, pourquoi tiens-tu à ce point à distinguer des projets
tactiques et des projets stratégiques ? C’est un peu la même chose,
non ?

Interloqué, voire choqué (il me provoque en fait), il répond :

– Pablo... Pablo... est-ce que tu te moquerais de moi ?

Un peu c'est vrai, car Alexis est aussi auteur et éditeur. Il a fondé la
maison d’Editions [Praxeo](http://praxeo-fr.blogspot.fr) il y a dix ans,
reconnue pour l’enseignement de jeux asiatiques dont le jeu de go, et il
est lui-même auteur de plusieurs gros bouquins hautement stratégiques et
tactiques : poker, backgammon, et même sur des jeux de simulations
historique : [Mémoire 44](http://www.daysofwonder.com/memoir44/fr/content/guide/) (**© Days of
Wonder 2011**). À cela, il faut ajouter qu'il met ses théories en
pratique au quotidien.

Reprenons.

– Pablo... il faut que tu lises la préface d’un livre. Et là, il me sort
un gros manuel de plus de cinq cents pages sur [Mémoire 44](http://www.daysofwonder.com/memoir44/fr/content/guide/) (**© Days of
Wonder 2011**), un jeu de simulation sur des conflits de la Deuxième
Guerre mondiale ! Il précise :

– À l’origine, le projet comportait même deux ouvrages séparés : **Le
Guide Tactique** et **Le Guide Stratégique** , tellement la distinction
est grande.

– Mais donc en deux mots ?

– En deux mots ? Tu es dur. Le stratège n'envisage pas la défaite. Il se
projette et s'adapte. La tactique est opérationnelle. C'est une
expertise. Le tacticien calcule et manœuvre. Le stratège évalue et
décide.

– Eh mais tu me parles de mes product owner là !

## Product Owners

– Je ne sais pas, peut-être, c’est toi l’expert, mais si ça t’intéresse,
sache que j'ai entamé l'écriture d'un e-book qui ne traite que de la
stratégie. Étonnamment, la stratégie des jeux n’est pas du tout
intuitive, et mérite parfois quelques analogies et parallèles, notamment
avec l’économie d’entreprise et même la finance de marché.

– Alexis, faut qu’on parle !

– L’idée de cet e-book (qui n’est pas encore paru) m’est venue lorsque
je relisais deux manuscrits : le premier est [Chûban, la stratégie au
jeu de go de Dai
Junfu](http://praxeo-fr.blogspot.fr/2010/02/livre-chuban-la-strategie-au-jeu-de-go.html)(8e
Dan), le second est [Xiang qi, l’univers des échecs chinois de
Marc-Antoine
Nguyen](http://praxeo-fr.blogspot.fr/2002/10/livres-deux-nouveautes-praxeo-en.html)
(Praxeo, 2009).

![Praxeo](/images/2012/06/Logo-PRAXEO-2012.jpg)

Ces deux experts dans leur jeu respectif s’accordent sur l’utilisation
de la stratégie et, sans concertation, dissocient les positions en trois
statuts : (1) En avance (2) À l’équilibre (3) En retard. Qui doit
prendre des risques ? Faut-il compliquer ou stabiliser la position ?
Point final. La stratégie c’est ça et rien d’autre : que faire à
l’échelle du champ de bataille lorsqu’on est en position de faiblesse,
ou en position de force.

– Le go, les échecs, [Mémoire
44](http://www.daysofwonder.com/memoir44/fr/content/guide/) (**© Days of
Wonder 2011**), je suis sûr qu'il y a plein de choses intéressantes pour
les product owner, des analogies évidentes qui peuvent enrichir nos
réflexions.

– Tu jugeras. Mais d’après moi, après des années de réflexion, je
converge vers une définition de la stratégie minimaliste et très
précise : quel est le niveau d’exposition au risque adapté selon qu’on
est en position de faiblesse ou en position de force ? Que faire en
retard ou en avance ?

Il enchaine, quelques fondamentaux :

– La stratégie est la projection précoce d’une globalité dans le temps.

Le stratège définit un but global et anticipe un résultat.

La stratégie induit une part d’imprévus et de risques.

Elle se propose d’optimiser le rapport profit / risque en vue
d’atteindre un objectif précis.

La stratégie intègre l’incertitude, le facteur chance et même les
comportements irrationnels.

–  On est vraiment proche de la vision (au sens où justement on demande
aux product owners une stratégie, une projection dans l'avenir), et je
comprends que ces champions de go ou d'échecs actent, comme nous les
agilistes, qu'il est illusoire d'imaginer tout prévoir ?

## Incertitude et exposition au risque

– Ah oui ! Bien sûr. Le champion **Marc Antoine Nguyen** écrit par
exemple : « Si vous pensiez qu’un joueur vraiment fort pouvait jouir
d’une diminution des niveaux de risques et des imprévus, vous vous
trompiez sur la stratégie : c’est tout l’inverse qui se produit. Au
contraire, le fort joueur est capable d’assumer des niveaux
d’incertitude et d’exposition au risque très supérieurs à la moyenne !".
C’est ce qui fait sa force.

![Mémoire 44](/images/2012/06/memoire44-212x300.jpg)

– Cette définition me plaît, car elle m’amène à une règle de business
directement applicable : "La stratégie est le privilège du product
owner. Mais je comprends qu'il partage les manœuvres tactiques avec ses
équipiers. Si je me risque à une métaphore sur le champ de bataille : le
général est le product owner, il a une stratégie (et il n'envisage pas
la défaite), il décide, il évalue les risques (j'ai bien lu
l'introduction de [Mémoire
44](http://www.daysofwonder.com/memoir44/fr/content/guide/) !). Pendant
ce temps, le tacticien calcule, manœuvre et utilise ses armes à bon
escient, tout en restant à tout moment aligné sur la stratégie du
général. C'est bien là l'autonomie et la compétence de l'équipe. Les
choix tactiques sont aussi partagés par le product owner : comment il
découpe ses user stories, comment il priorise certains choix, etc.
Alexis c'est passionnant et je suis persuadé que cela peut apporter
beaucoup à mes product owners.

– Si tu te permets ces analogies, il faudra être très rigoureux et
précis. J’insiste par exemple que le tacticien doit appliquer la
stratégie définie par son état major, même s’il ne la comprend pas.

– Ah ? C’est brutal.

– Pourtant je pense que c’est essentiel, et même vital. Prends l’exemple
d’une bataille à l’équilibre où tu souffres d’une faiblesse inquiétante
sur l’une de tes sections, disons sur ton flanc droit (Waterloo !). Eh
bien dans cette configuration, le tacticien aura toujours le réflexe de
renforcer sa section faible.

– C’est mal ?

– C’est même fatal dans certains cas. Renforcer sa section faible dans
une position à l’équilibre signifie dégarnir une partie des autres
sections pour renforcer la première. Résultat : ce sont les sections
dégarnies qui se retrouvent affaiblies à leur tour. Ajoute à cela que la
dimension temporelle, absolument stratégique, a été complètement
occultée dans cette manœuvre purement tactique.

– Comment raisonnerait le stratège ?

– Il se pose deux questions simples : combien je perds (en espace, en
matériel, et surtout en temps) si je renforce la section faible ? Tu
vois l’autre question ? L’alternative ?

– Non

– Combien je perds si je sacrifie complètement la section faible. Il
évalue les deux pertes et choisit la moindre.

– Il perd dans tous les cas ! Peut-on parler d’une stratégie pertinente
où l’on est sûr de perdre ?

– Il s’agit là d’une perte tactique, un petit bout d’espace et de
matériel jugés acceptables. Pendant ce temps, tu vas utiliser ta section
faible comme un appât chronophage et renforcer ton centre, où tu es déjà
fort, pour y lancer une manœuvre de supériorité. Et tu défonces le type
d’en face. Rappelons qu’à l’origine, tu étais à l’équilibre.

– Si je reviens à la question posée, le tacticien n’est pas meilleur
s’il comprend le calcul du stratège, s’il accepte l’idée du sacrifice ?

– Ah non ! On s’en fout et on n’a pas le temps. Chacun son boulot. Par
exemple, ton product owner décide qu’il faut sacrifier (abandonner) un
pan de projet entier (la fameuse section faible). Il est responsable de
cette décision et supposé parfaitement compétent pour savoir qu’avec les
ressources où le budget ainsi dégagés, il va pouvoir renforcer son core
business et au choix, livrer Time2Market, gagner un nouveau deal ou
faire la différence contre un concurrent. Est-ce qu’il est obligé
d’expliquer (de prouver, justifier) tout ça à toute l’équipe avant
d’avoir le droit de décider ? J’espère bien que non. Une stratégie ne
s’explique pas. Elle se décide, puis elle s’expose et s’exécute sans
moufeter.

– De mon point de vue d'agiliste toutes intéractions avec l'équipe est
positive. Tout échange ou « feedback » est une richesse. Mais c'est vrai
qu'il est indispensable qu'une seule personne, le product owner, puisse
trancher sur les priorités (il impose l'ordre à l'équipe). Pour aider
mes product owners à appréhender ces questions comment débuter la
réflexion au sein de son entreprise ?

– Pablo, quand j'ai travaillé sur ce sujet, j’ai constaté que les
stratèges d’échecs et les stratège de go décidaient des stratégies
opposées (des prises de risques divergeantes selon l’avance ou le retard
positionnel). Je suis arrivé à la conclusion que l'on pouvait
conceptualiser deux systèmes (concernant le stratège et donc ton product
owner). Le premier me dit que si je suis en retard, je dois attaquer,
compliquer la position, accroître mon niveau d’exposition au risque.
C’est ma seule chance de renverser la tendance qui n’est pas en ma
faveur. Inversement, le second préconise une défense solide, stabiliser
les arrières avant de contre-attaquer pour éviter la déroute.

## Systèmes de comptage et de mort subite

Les systèmes (les jeux), appartiennent a priori à l’une des deux
familles suivantes, dont nous connaissons deux prototypes
représentatifs.

La famille « jeu de go » : En retard, j’attaque. J’augmente mon
exposition au risque, je complique la position. Je l'appelle le système
de comptage.

–  Un peu comme une entreprise en position dominante, du moins pérenne,
qui gère ses investissements ?

– Oui, à condition qu’elle ait de la trésorerie. Dans ce cas, ses seuls
risques sont des risques de marché. Pas de perte fatale en vue. L’autre
famille est représentée par les échecs  : En retard, je consolide
d’abord, j’assure d’abord la défense de mon Roi avant d’envisager toute
contre-attaque. Cette fois, le risque dépasse une simple perte
matérielle. C’est l’organisme tout entier qui est en péril. Il s’agit
des systèmes de mort subite, matérialisés par le Roi des échecs, par une
trésorerie fragile en économie, et par les risques de crédit (liquidités
et contrepartie) en finance.

– D'accord une startup, mais pas seulement, une entreprise qui joue sa
peau et dont les choix sont décisifs.

– Un jour, en appliquant ces règles issues de la stratégie des jeux, je
t’expliquerai pourquoi le raisonnement stratégique est naturel pour un
enfant de huit ans, et aussi comment l’espagnol **Chupa Chups** a
explosé le leader mondial et incontesté de la sucette **Pierrot
Gourmand**.

– Laisse moi digérer déjà tout cela. Il faut que nous creusions ce
sujet. Je suis intuitivement persuadé que les stratégies de tous ces
jeux peuvent servir d'appui à nos stratégies d'entreprises. Cela me
paraît d'une grande richesse pour les product owners ou product
managers. Je vais réfléchir à tout ça et nous en reparlerons.

A suivre.

L'article suivant est  : [Le product owner
contre-attaque](/2012/09/le-product-owner-contre-attaque/)

Et [ici vous avez tous les articles liés sur le blog d'alexis](http://praxeo-fr.blogspot.fr/2012/12/strategies-le-sommaire.html).

![Alexis Beuve](/images/2012/06/alexisbeuve-300x206.jpg)

Alexis, pris en en photo par Chantal *Tara* Delannoy, du [Photo Club
Vicinois](https://picasaweb.google.com/PhotoClubVicinois)

