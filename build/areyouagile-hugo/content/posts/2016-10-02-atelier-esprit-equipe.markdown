---
date: 2016-10-02T00:00:00Z
slug: atelier-esprit-dequipe
tags: ['brainstorming','teambuilding','eventstorming','retrospective','giveandtake']
title: Atelier esprit d'équipe
---

Les semaines passent... et se ressemblent. La [semaine
dernière](/2016/09/atelier-creativite/) je vivais une journée dans
l'ambiance Art Déco du Bœuf sur le toit. Cette semaine c'est une journée
dans un cabinet d'avocats où des marbres et dorures magnifiques
(probablement du XIXème me glisse
[Lahouari](http://www.salondemontrouge.com/80-lahouari-mohammed-bakir.htm))
donnent un allant à tous les ateliers mais m'interdisent d'accrocher
quoi que ce soit aux murs...

Objectif de la journée : bâtir un esprit d'équipe, du *team building* et
idéalement repartir avec quelque chose pour le lundi suivant. Une
vingtaine de personnes, plutôt dans le domaine du bizdev, de la finance
et du juridique. On m'explique qu'il peut y avoir des acrimonies, des
tensions, et avant toute chose qu'il faut ouvrir un espace de parole et
rendre visible tous les actes et vandalismes non verbaux sous-jacents
pour les atténuer. Je propose alors de me faire accompagner par
[Boris](http://www.coaching-renouveau.fr/) afin d'avoir une **double
voix : l'une orientée vers l'organisation, la facilitation, le
management, et qui mènera le fil rouge de la journée, moi, et l'autre
orientée dynamique individuelle et d'équipe qui opérera plus comme une
voix off pour le groupe ou pour des personnalités**,
[Boris](http://www.coaching-renouveau.fr/) donc, que je connais de
longue date et avec qui je fais missions et conférences.
[Boris](http://www.coaching-renouveau.fr/) que nous faisons
régulièrement intervenir chez [beNext](http://benextcompany.com). Si la
même personne mêle ces deux voix elle perd en intelligibilité.

![Va donc mettre un post-it](/images/2016/10/dorures.jpg)

Va donc mettre un post-it sur ces murs...

Bref. Ma stratégie est de **partir d'une réconciliation pour aller vers
l'action**. Une première étape autour d'une rétrospective sécurisée
(sans mélanger les groupes), puis un atelier [Give and
take](http://gamestorming.com/games-for-design/give-and-take-matrix/)
transformé en atelier "needs", puis j'imaginais un kanban pour rendre
visible à tous le processus complet d'un projet mais
[Dragos](http://www.andwhatif.fr/) m'a astucieusement conseillé de
basculer *carrément* sur un [event
storming](http://ziobrando.blogspot.fr/2013/11/introducing-event-storming.html)
(dont
[Alberto](http://ziobrando.blogspot.fr/2013/11/introducing-event-storming.html)
m'avait fait une démonstration à ALE 2015 et j'utilise de ci de là
depuis). Enfin une [scierie à
pratiques](/2016/02/la-scierie-a-pratiques-evolue/) pour donner un
relief et une action à l'*event-storming*. Je reviens sur tout cela
ci-dessous.

## Rétrospective et saynètes

![Retrospective](/images/2016/10/retro.jpg)

D'abord donner un **espace de parole, de partage, de compréhension**
pour atténuer les crispations dont on me dit qu'elles fleurissent dans
l'équipe. Faire comprendre à chaque groupe la vision de l'autre. Bonne
pioche pour Boris qui met en évidence par la même occasion un triptyque
*émotion, action, pensée* qui se répartit bien entre les trois
sous-groupes (finance, bizdev, juridique) et qui explique aussi
certaines incompréhensions (on dit la même chose mais personne ne
comprend l'autre, ou ne l'écoute avec le bon canal). On débute par une
rétrospectives étoile de mer (*plus de, moins de, continuer à, arrêter
de, commencer à*) assez classique. L'intérêt étant pour chaque groupe de
présenter ses réflexions et le ou les deux points clefs qui en
ressortent aux autres groupes. **Énoncer à haute voix, expurger,
clarifier**.

![Saynète](/images/2016/10/saynete.jpg)

Puis pour aller plus loin je demande à chaque groupe de jouer des
[saynètes](/2016/08/saynetes/) autour de ces éléments clefs qui sont
ressortis. Comme toujours personne n'est obligé de jouer, on peut jouer
avec des notes, on peut souffler. Pour moi il y a deux éléments
importants. Premier point : la préparation, la conversation sur les
scènes à jouer. Ceux sont des moments physiques, très palpables, à
imaginer ou retranscrire. On touche du doigt une réalité (même si on
l'exagère souvent). Deuxième point : la **personnification**. On prend
la peau de, ou on voit quelqu'un qui nous personnifie. Là aussi de la
caricature, tant mieux, elle **dédramatise**, elle donne de la bonne
humeur. Mais soudain ce n'est plus du discours mais du ressenti,
palpable.

## Give and take = needs

![Give and take = needs](/images/2016/10/needs.jpg)

L'atelier [Give and
take](http://gamestorming.com/games-for-design/give-and-take-matrix/)
c'est l'idée d'une matrice qui permette à chaque groupe d'évoquer ce
qu'il prend (*take*) et ce qu'il donne (*give*) aux autres. Dans mon
cadre je le simplifie à "*qu'est ce que vous attendez de l'autre
groupe*" (*needs*) dans le contexte que l'on se donne (ici la poursuite
de projets bizdev). On **ne juge pas, on n'attaque pas, on sera
peut-être en désaccord**, et ce n'est **pas grave, on clarifie** juste
les attentes de chacun, où chacun projetait l'autre.
[Boris](http://www.coaching-renouveau.fr/) en profite pour ajouter une
couche de [communication non
violente](https://fr.wikipedia.org/wiki/Communication_non-violente) dans
l'expression des attentes. L'atelier rempli son objectif, on découvre
des gaps, des tensions, des manques. Maintenant on les connaît.

![Saynète](/images/2016/10/pause.jpg)

C'est la pause, et un petit jardin intérieur, à deux pas des
Champs-Elysées, se révèle. Dans la verdure j'appelle ma muse. J'observe
le groupe. Le repas ce moment décisif. On rigole. Une personne nous
demande si on était dans le dernier "Capital". On rigole encore plus.
Interdit de me vanner là dessus !

## Event Storming

![Event Storming](/images/2016/10/event-storming.jpg)

Après une **introspection et une personnification** (la rétrospective et
les saynètes), puis une clarification des attentes des périmètres de
chacun (*give and take*) on attaque un gros morceau : un [event
storming](http://ziobrando.blogspot.fr/2013/11/introducing-event-storming.html)
du processus projet. On clarifie encore, en groupe, mélangé cette fois,
toutes les étapes d'un projet, les acteurs attachés à chaque étape, les
adhérences externes, les déclencheurs, les données clefs. Cela complète
parfaitement la matrice des attentes, donne du relief dans le temps,
aligne tout le monde sur un processus commun, partagé. On commence déjà
dans les discussions à ajouter des élément que l'on estime manquants.

C'est un **travail physique**, où chacun travaille sur la **même
matière**. La **lecture est donc partagée**. Le relief est donné par une
ligne de temps et les densités, goulots, matière des post-it. Les
éléments physiques limitent aussi à l'essentiel. Les densités de
couleurs font apparaître des dérives (trop de jaune, trop de
participants, probablement trop de silos...).

*Event Storming* est un élément très clarifiant pour un groupe. La photo
en haut à gauche le montre bien.

## Scierie à pratiques : relief et action

![Relief et action](/images/2016/10/scierie.jpg)

Quand tout le monde s'accorde à dire que le métier est ainsi bien
représenté (par l'*event storming*) alors par un dernier atelier (une
sorte de [scierie à pratiques](/2016/02/la-scierie-a-pratiques-evolue/))
j'essaye de donner du **relief** (**sens et priorisation**) et une
première action (pour changer il faut y aller étape par étape, par
cycle, dans un espace protégé).

On demande donc à chacun de désigner dans l'*event storming* le moment
qui lui parait le plus essentiel, le plus stratégique. Et pourquoi à ces
yeux c'est ce moment là. Le pourquoi, le sens. Puis on découvre que cela
représente *grosso modo* trois groupes, sur trois phases clefs :
structuration, arbitrage, et clôture de l'affaire. A chaque groupe je
demande de réfléchir un peu et de s'attribuer une note sur dix
concernant ce moment qu'ils estiment si important dans cette frise
représentant leur domaine. On obtient, par exemple, 4/10, 4/10 et 8/10.
À chaque groupe je demande ensuite de *brainstormer* et de m'expliquer
ce qu'ils vont faire dans le mois à venir pour augmenter cette note de
un ou deux points. Il s'agit pour réussir un changement de ne pas voir
trop grand. Avoir quelque chose de **concret, activable**. Note : les
groupes sont désormais hétérogènes par rapport aux groupes initiaux pour
le *brainstorming*.

J'ai aussi demandé quel passage de l'*event storming* leur semblait
inutile ou anecdotique. Il n'y en avait pas, mais le cas échéant on
aurait pu essayer de ne plus faire cette activité le mois suivant, pour
essayer (**le changement est facilité si on peut revenir en arrière, on
essaye donc simplement**).

Voilà il m'a semblé que c'était un bon parcours, qui faisait sens. D'une
phase analytique, introspective, qui pouvait expurger des choses, à une
phase clarifiante (matrice et *event storming*), qui déclenchait un
travail de groupes hétérogènes, pour s'ouvrir sur une phase avec des
actions concrètes, simples, réalistes, activables dès le lundi suivant.
Encore une fois, si vous essayez le même déroulé n'hésitez pas à me
faire part de vos retours. 

Puisque l'on parle **finance** en ce moment aussi : le **DAF/CFO Agile**
avec [agilecfo](http://agilecfo.fr/).
