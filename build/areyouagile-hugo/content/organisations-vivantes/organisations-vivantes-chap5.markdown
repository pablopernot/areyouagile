---
title: "Les organisations vivantes"
url: /organisations-vivantes/chap5/
---

# Chapitre 5 : Éléments constituants

> Je refuse de faire partie d’un club qui m’accepterait comme membre ! – Groucho Marx

L’élément qui constitue nos organisations c’est principalement nous,
l’être humain. Et on a tendance à singulièrement oublier ce dont nous
sommes capables, ou incapables. Si l’on s’accorde à dire que la
dynamique qui lie tous les éléments de nos organisations, i.e nous,
c’est la communication, voici quelques points clefs à garder à l’esprit.

## La communication « constructive » devient ardue au delà de sept

L’objectif de nos organisations est généralement de bâtir quelque chose,
de générer quelque chose. Cette construction dans notre cas se fait à
plusieurs (sinon on ne parlerait pas d’organisation). Pour que ces
groupes d’humains soient efficaces mes observations et expériences
m’indiquent qu’au delà de sept ou huit personnes les dynamiques de
communication au sein des groupes périclitent. Comme si la combinatoire
au delà de huit personnes devenait une charge trop ardue pour nos
capacités physiques. C’est ce que j’ai pu retrouver avec plaisir dans le
livre de Fred Brooks (”The mythical man month”)[^20], lorsqu’il soumet
ce diagramme sur les communications au sein du groupe :

![PolygonDiagonals.png](/organisations-vivantes/media/PolygonDiagonals.png)

Voici une bonne première indication : inutile de chercher à créer de
grands groupes : la dynamique de communication « constructive » aura du
mal à émerger au delà de huit personnes, disons sept pour utiliser un
chiffre auquel on accorde souvent des pouvoirs mystérieux. Oui cela
indique la nécessité d’une charge de travail dédiée à synchroniser ces
groupes (nous y reviendrons plus tard), mais c’est bien normal, c’est la
caractéristique des grandes organisations (si l’on souhaite une image
organique : on parlera d’un côté cellulaire).

Ce chiffre de huit ou sept sera renforcé par le plaisir d’appartenance à
un groupe. Des études[^21] ont démontrées que le plaisir d’appartenance
à un groupe fluctue selon la taille du groupe. Si vous êtes sept ou
cinquante les personnes du groupe vont se reconnaître et se sentir bien
entres-elles. N’oubliez pas que c’est aussi une clef de l’implication et
de l’engagement.

Dernier chiffre, le chiffre de Dunbar comme on le nomme, qui avoisine
150-220, c’est à ce chiffre que les villages de préhistoire se
scindaient. Au delà il semble que les relations sociales s’effritent (or
il apparaît de plus en plus que les relations sociales sont l’avantage
naturel de l’être humain sur les autres espèces[^22]). Ainsi autour de
150-220, nous ne savons plus soutenir de relation sociale, et ainsi une
séparation se produit.

![satisfaction.png](/organisations-vivantes/media/satisfaction.png)[^23]

Voici donc un impact de l’élément que nous sommes pour nos organisations
: autant créer des départements de 150-220 personnes, composés de
groupes de 50 personnes, eux-mêmes composés d’équipes de sept à huit
personnes maximum (et disons trois ou quatre minimum, car en dessous ce
n’est plus un groupe, donc on commence à perdre la dynamique et
l’intelligence collective, même si on gagne d’autres facilités).

## Les mots ne sont pas les meilleurs vecteurs d’informations

Le *verbal overshadowing* c’est l’idée que les mots emportent avec eux
un tel passé, si propre à chacun de nous, qu’il est juste illusoire de
croire qu’ils clarifient la communication. C’est un apprentissage
contre-intuitif, surtout vis-à-vis de notre éducation à la française,
amoureuse des belles lettres, les mots introduisent de l'ambiguïté[^24].

Pour communiquer efficacement il faut dessiner, tracer, faire des
diagrammes, comme les aurochs des grottes de nos ancêtres. Un dessin
vaut mieux qu’un long discours. Entre l’aurochs sur le mur et la pierre
de rosette (qui propose le même texte traduit dans différentes langues)
une étape a été franchie. Elle a peut-être été nécessaire à l’humanité,
mais elle est aussi la source de bien des incompréhensions[^25].

![cavepaint-728304.jpg](/organisations-vivantes/media/cavepaint-728304.jpg)
![rosette.jpg](/organisations-vivantes/media/rosette.jpg)

La pierre de rosette c’est la fin d’une compréhension intuitive et ainsi
rapide et efficace.

Pour vos organisations abondez dans la communication non verbale à base
de diagramme, schéma, etc.

On connaît le *Pictural Superiority Effect* : le sens visuel est bien
plus accentué chez nous que n’importe quel autre. Il englobe 75% de nos
stimulis externes. Á cela sachez que plus on associe de sens entre eux,
plus la qualité de la mémorisation est accentuée. Oui il existe une
vraie différence entre un tableau physique que l’on manipule (on touche
les éléments) qu’un tableau électronique que l’on ne manipule pas
vraiment. Nous sommes fabriqués ainsi. Le bon terreau d’une organisation
c’est donc une communication visuelle, non pas basée sur les mots mais
sur des outils de management visuel qui associent le plus de sens
possible pour améliorer la mémorisation et donc la résolution de
problèmes[^26].

## Distance et co-localisation

Cette histoire de colocalisation est un point important aujourd’hui. Le
développement de nombreux outils permettant le travail à distance a
ouvert des possibilités : bien-être de la personne qui souhaite
travailler dans son environnement personnel, constitution d’équipes au
travers le globe, etc.

La frugalité énergétique (économie de matière, optimisation énergétique,
fin des ressources) et l’espace pris par l’immatériel sont les deux
figures emblématiques de notre nouveau monde.

Ce que l’on sait de ces équipes non-localisées c’est qu’il y a une
différence fondamentale entre les équipes dispersées : leur répartition
(nombre, géographie) est déséquilibrée, et les équipes distribuées où
leur répartition est équilibrée. Par exemple pour dispersée vous avez
quatre personnes sur site, et deux sur des sites distants. C’est
compliqué à faire fonctionner car il y a un déséquilibre dans la
communication. Mais si vous avez six personnes, chacune sur des sites
distants, il n’y a pas de déséquilibre. C’est bien la communication qui
est clef. En ayant le même niveau communication, on induit le même
niveau de transparence et donc de confiance, et donc d’implication et de
liberté sur lequel bâtir quelque chose est possible.

![distance.png](/organisations-vivantes/media/distance.png)

Alistair Cockburn

Je me retrouve cependant face à un choix cornélien : il est évident
qu’une équipe fonctionne mieux sur un même site, dans une même salle
projet. A cela d’ailleurs Alistair Cockburn nous dit qu’au delà de la
distance d’un bus les dégâts sont considérables sur la dynamique
d’équipe (d’où aussi la pièce projet, obeya que l’on évoquera plus loin)
; c’est aussi pour cela que l’on dit que la distance entre deux étages
est aussi considérable que celle entre deux villes. Je reviens à mon
choix cornélien : d’un côté la qualité de la communication et ainsi de
la dynamique d’équipe, de l’autre côté, si un désir de travail à
distance est exprimé, la liberté et ainsi l’implication et l’engagement
des personnes qui constituent cette équipe. La poursuite des études de
Tom Allen sur la courbe évoquée indiquent cependant que tous les moyens
de communication moderne ont accentués cette rupture. Je reste plus
mesuré là dessus.

![allen-curve.png](/organisations-vivantes/media/allen-curve.png)

### Obeya

Concernant la co-localisation et l’effet de mémorisation évoqué plus
haut : sachez que Toyota avait prôné les obeya (pièce projet du Lean de
Toyota). L’Obeya c’est un espace dédié à un thème et réservé à une
équipe. Si les informations sont clairement localisées dans l’espace
physique, elles le seront dans le cerveau[^27]. Par exemple si vous
apprenez l’anglais et l’espagnol à votre enfant, le mieux est de
réaliser chacun de ces apprentissages dans des pièces différentes, le
stockage dans le cerveau s’associant au lieu.

## Auto-organisation

« Tout système vivant est auto-organisé, vouloir l’organiser c’est non
seulement inutile, mais c’est surtout stupide »

-- Harrison Owen[^28]

Si nous nous classons dans la catégorie des systèmes vivants, ce que
j’espère vivement, nous héritons de leurs capacités. Or tous les
systèmes vivants sont connus pour s’auto-organiser. Cela veut dire qu’il
n’est nul besoin d’organiser leurs activités, ils le font tout seuls. En
d’autres termes il n’est nul besoin d’organiser nos organisations, si
nous avons un objectif clair, et des règles claires : c’est à dire une
sorte de conteneur, un ensemble de contraintes, l’organisation va
s’auto-organiser. Elle ne va pas être désorganisée. C’est une approche
bien connue : posez le cadre (règles, objectif, capacité à savoir où
l’on en est : feedback), et laissez fructifier l’auto-organisation en
son sein.

## Apprentissage & histoire

![Storytelling1.jpg](/organisations-vivantes/media/Storytelling1.jpg)

L’apprentissage c’est l’intégration de motifs. Plus on apprend, plus on
intègre de motifs pour différencier les situations. Le bruit et le
désordre existent et donc de nombreux motifs se ressemblent mais sont
différents. On crée alors de plus en plus de motifs pour répondre à
notre apprentissage grandissant. Ainsi le désordre génère de l’ordre (de
plus en plus de motifs) mais dans un ensemble de plus en plus
complexe[^29]. C’est ce qui constitue en fait ce que nous sommes, nos
histoires, d’où le storytelling, c’est notre passé, nos motifs
mémoriels.

L’histoire peut-être considérée comme un idéal motif mémorisé.

Et puis on se projette facilement dans une histoire. Comme l’effet
miroir de notre cerveau : si vous voyez quelqu’un frapper un clou avec
un marteau, les zones de votre cerveau liées à cette activité sont en
éveil ; les histoires provoquent un effet miroir[^30].

Enfin et non des moindres les histoires sont depuis toujours la base de
notre sociabilité : elles décrivent notre tissus relationnels. Et cette
propension sociale est la base même de notre humanité.

Selon Jérôme Bruner, un fait raconté au travers une histoire est
mémorisé 22 fois mieux que le même fait présenté directement[^31].

Un fait est mémorisé 22 fois mieux si c’est une histoire, un manager
prend 22 fois plus de décisions qu’il y a 100 ans, à vos histoires !

### Menu

* [Menu](/organisations-vivantes/)
* [Chapitre 1 : Monde agile, monde complexe](/organisations-vivantes/chap1/)
* [Chapitre 2 : Penser organique](/organisations-vivantes/chap2/)
* [Chapitre 3 : Dynamique des formes](/organisations-vivantes/chap3/)
* [Chapitre 4 : Choisir sa voie ?](/organisations-vivantes/chap4/)
* [Chapitre 5 : Éléments constituants](/organisations-vivantes/chap5/)
* [Chapitre 6 : Changer la trajectoire de nos organisations](/organisations-vivantes/chap6/)
* [Chapitre 7 : Conclusion](/organisations-vivantes/chap7/)



[^20]: Fred Brooks, https://archive.org/details/mythicalmanmonth00fred
[^21]: Voir le ’Plaisir d’appartenance à un groupe([*http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/*](http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/)) suivant concernant les écrits de Christopher Allen et Robin Dunbar

[^22]: « Social : Why Our Brains Are Wired to Connect », Matthew D. Lieberman

[^23]: ([*http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/*](http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/)

[^24]: “En lisant ces paragraphes j'ai eu un choc, parce que j'aime la cohérence. Pourquoi écrivez-vous ce livre alors? Pourquoi ces digressions sur les cristaux et les termitières ? En effet, une image c'est 300 à 3000 mots. Quelle valeur supplémentaire ont donc les mots pour que vous en veniez à écrire un livre ? Bref, j'aime les discours mesurés. Selon moi, chaque médium a son utilité. D'ailleurs, comment pourrais-je vous communiquer ce que je vous écris avec un dessin sans que vous interprétiez ? Le dessin aussi peut introduire des ambiguïtés. Bref, je que j'aimerais trouver dans ce que vous écrivez à propos des mots ce sont les circonstances dans lesquelles un dessin vous semble plus approprié.” S’insurge Paul-Georges Crismer en commentant ces mots. Je suis bien d’accord. Lorsque je ne serai plus incohérent ou paradoxal je serai certainement mort. Les mots ont la force de rester, de m’aider à organiser ma pensée. Si je pousse dans l’autre sens c’est que dans notre culture la culture de la puissance de l’écrit est considérable et qu’il faut savoir la mettre en perspective.

[^25]: « Blah blah blah », Dan Roam, http://changethis.com/manifesto/show/88.01.BlahBlahBlah

[^26]: Brain rules, John Medina

[^27]: Brain rules, John Medina

[^28]: Harrison Owen, Dancing with Shiva (https://www.youtube.com/watch?v=APD7oQ3xrSA)

[^29]: “Entre le cristal et la fumée. Essai sur l'organisation du vivant”, Henri Atlan

[^30]: « Social : Why Our Brains Are Wired to Connect », Matthew D. Lieberman

[^31]: Jeff Patton, User Story Mapping. //TODO aller chercher la source originale
