---
title: "Les organisations vivantes"
url: /organisations-vivantes/chap4/
---

# Chapitre 4 : Choisir sa voie ?

> « There are two path you can go by, but in the long run 
> There’s still time to change the road you’re on »
> -- Robert Plant, Stairway to heaven

## Bidonvilles

Personne ne souhaite vivre dans un bidonville[^17] : l’insalubrité,
l’insécurité, etc. sont aux antipodes de nos aspirations. Cependant
certains aspects des bidonvilles peuvent nous intéresser : une façon de
s’adapter au mieux à l’espace, une exploitation maximum de la matière
première offerte, (et peut-être des effets de communication très
intéressants à observer).

On retrouve dans le bidonville cette faculté naturelle d’optimisation de
l’énergie que nous avons constaté chez le cristal comme chez la
termitière. On y retrouve – par anticipation malheureusement – les
difficultés en matière première auxquelles nous sommes condamnés dans
les années. Enfin, autre source d’intérêt concernant les bidonvilles :
leur capacité à être très grands. Pas mal de réponses amenées à ceux qui
s’intéressent aux organisations à grande échelle.

On retrouve peut-être là au niveau de l’organisation (topologie, forme,
architecture) ce qui a fait le succès du Lean et de l’agile : une
nécessité d’optimiser la valeur, de gérer les ressources, d’appréhender
l’imprévu. Soyons positifs : l’optimisation organique et spatiale des
bidonvilles sans l’insalubrité et l’insécurité, avec du sens et de la
création de richesse, serait une cible intéressante.

![ken_kibera_large.jpg](/organisations-vivantes/media/ken_kibera_large.jpg)

Bidonville de ken Kibera

### Les leçons des favelas brésiliennes

En recherchant des informations à propos des bidonvilles, je suis tombé
sur un rapport consolidé au sujet des apprentissages liés à
l’amélioration des bidonvilles brésiliens : les leçons retenues des
bidonvilles qui ont réussi à aller mieux. En lisant ce rapport : Slum
upgrading lessons from Brazil[^18] je suis un peu tombé de ma chaise
tellement j’avais l’impression que les dites leçons semblaient provenir
tout droit d’un manuel d’organisation Lean/Agile.

Mais écoutez plutôt :

#### Comment faire qu’un bidonville aille mieux ?

Pour s’en sortir il faut :

-   Autonomie administrative

-   Formation continue

-   Diversité des habitats

-   Installation incrémentale de bassins d’eau

-   Proximité entre l’habitat et le lieu de travail

-   Équipe multidisciplinaire

-   Changement étape par étape

Si l’on reprend ces indications, on y retrouve indéniablement l’histoire
moderne de l’organisation, et cette culture de l’adaptation en milieu
complexe qui résume l’agilité.

Une amélioration continue (la formation continue), une
responsabilisation des personnes (l’autonomie administrative) et des
groupes ; la capacité à avoir du feedback sur des choses finies, d’où la
véritable autonomie, permise par des équipes pluridisciplinaires.

On y retrouve aussi des éléments que nous allons aborder plus loin : une
conduite du changement qui avance étape par étape, par petit pas (les
bassins d’eau), la proximité des actions (habitat, travail, équipe).

Tout cela constitue des éléments organisationnels sur lesquels se bâtit
la silhouette de l’organisation que nous avons décrit précédemment.

#### Comment faire pour maintenir un meilleur état (dans les bidonvilles) ?

Maintenir cette organisation (ce bidonville) en bon état, nous disent
les auteurs, c’est :

-   Encourager la socialisation

-   La résolution de problème doit être décentralisée

-   Création de communautés

-   L’implication vient de la transparence

-   Utiliser de vraies métriques

-   La vision est collective, par le consensus

-   Incorporation de nouvelles têtes, de regards externes

Là aussi on retrouve des préceptes de l’agilité que nous avons évoqué,
et des pistes pour les chapitres suivants.

Encore, une responsabilisation des personnes (la résolution de problème
localisée), un feedback réel (de vraies métriques), une notion de
transparence essentielle : pour l’implication, la responsabilisation, le
feedback, le droit à l’erreur, etc. ; une vision partagée (un objectif
clair).

Et d’autres éléments à noter : socialisation, communautés, injecter du
sang neuf.

#### Grandir ce n’est pas se massifier, ni se répéter

La taille des bidonvilles interpelle. Il est souvent plus question de la
limiter que de les faire grandir. Mais grandir, grossir, est souvent le
but de nos organisations (notamment dans le monde concurrentiel où la
taille reflète une certaine « force de frappe » ou une certaine capacité
d’investissement. Plus commun encore dans ce grand mouvement actuel qui
veut faire de l’agilité la nouvelle façon de procéder (et qui en fait la
caricature et la vide de sa substance), la question de mettre à
l’échelle une approche émergente est une question clef.

Ce que les bidonvilles nous disent à ce sujet, toujours dans Slum
upgrading lessons from Brazil[^19], c’est :

-   Grandir ne veut pas dire grossir, massifier (scale up versus mass)

-   Grandir ne veut pas dire répéter (scale up versus repetition)

-   Effet pervers de la réduction des coûts

On touche là deux points centraux et souvent mal compris des
organisations vivantes, deux choses radicalement différentes dans notre
nouveau paradigme : pas de répétabilité, linéarité et une gestion par la
valeur (et pas par les coûts).

Ne croyez plus à linéarité, ni à la répétabilité. Le monde change trop
vite et la linéarité ou la répétabilité sont un leurre (même si des fois
je m’interroge si le changement permanent n’est pas une manière
confortable de ne pas affronter les vrais problèmes ?). C’est difficile
de manier l’émergence, on la taxe souvent d’improvisation.

Concernant la réductions des coûts je ne citerai que cette phrase (dont
je pense qu’elle est de Peter Drucker, mais je ne suis pas sûr) : *si
vous suivez les coûts, vous générez des coûts, si vous suivez la valeur,
vous générez de la valeur*.

Cette absence de linéarité et la primauté de la valeur sur le coût ne
sont vraiment pas dans nos habitudes. C’est le retour du risque et donc
– en quelque sorte – la fin des conservatismes.

### Structure et énergie

Si j’observe ainsi le bidonville comme le fruit d’un laisser aller bien
plus naturel que les organisations telles que nous les pensons
faussement j’obtiens effectivement une organisation qui cherche à
optimiser l'énergie, autant celle des matières premières, que celle
dégagée par ses « composants », nous, communication, implication.

Cette organisation se bâtit en suivant des principes organiques
(répétition de schéma, structure économique, « fainéante », croisements
(macles), émergence (termitière), prend des formes organiques (théorie
des catastrophes, etc)).

Mais cette organisation pour s’élever vers le bien être (il n’est pas
question d’avoir pour objectif la misère et la dureté de la vie d’un
bidonville -- cruauté liée à l’aspect organique, naturel ? --) suit une
bonne partie des réponses proposées par l’agilité : responsabilisation,
amélioration continue, vision partagée, règles claires, « feedback »,
primauté de la valeur sur le coût, proximité, émergence, etc.

Ces réponses pour penser son organisation différemment, pour penser une
organisation vivante, peuvent s’appuyer sur des outils, des approches,
des savoir-faire. Mais tout cela est beaucoup conditionné par ce qui
constitue ces organisations, leurs composants, leurs éléments, nous.

### Menu

* [Menu](/organisations-vivantes/)
* [Chapitre 1 : Monde agile, monde complexe](/organisations-vivantes/chap1/)
* [Chapitre 2 : Penser organique](/organisations-vivantes/chap2/)
* [Chapitre 3 : Dynamique des formes](/organisations-vivantes/chap3/)
* [Chapitre 4 : Choisir sa voie ?](/organisations-vivantes/chap4/)
* [Chapitre 5 : Éléments constituants](/organisations-vivantes/chap5/)
* [Chapitre 6 : Changer la trajectoire de nos organisations](/organisations-vivantes/chap6/)
* [Chapitre 7 : Conclusion](/organisations-vivantes/chap7/)



[^17]: “Et pourquoi pas parler des villes du moyen-Age ? Un exemple plus proche de chez "nous" (càd dont nous pouvons expérimenter encore les traces) et associé à moins d'interprétations aujourd'hui (pauvreté, insalubrité) me paraîtrait plus concret.” m’écrit Paul-Georges Crismer. Certainement c’est une bonne idée. Il se trouve que je suis tombé sur un document qui a résonné en moi et qui parlait des bidonvilles. Les bidonvilles sont cependant plus liés à l’augmentation sans fin de notre population et à la fin proche de nos ressources, ils me semblent bien adaptés au propos.
[^18]: [Slum upgrading lessons from Brazil](http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf)
[^19]: [Slum upgrading lessons from Brazil](http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf)

