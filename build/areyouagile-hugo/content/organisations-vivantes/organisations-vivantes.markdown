---
title: "Les organisations vivantes"
url: /organisations-vivantes/
---

# LES ORGANISATIONS VIVANTES

C'est un travail en cours -- depuis 2014 -- chaque année à l'approche de noël je me dis que je vais le finaliser. Cette année je me dis que je vais le rebâtir peu à peu avec des articles (changer des parties), et écrire les morceaux manquants toujours en utilisant des articles à écrire dans les mois à venir.

Le texte est susceptible de changer à tout moment. Il est à l'état de brouillon. Vos commentaires sont les bienvenus. 

![Dance](/organisations-vivantes/media/dance_large.jpg)

> « There are two path you can go by, but in the long run 
> There’s still time to change the road you’re on »
> -- Robert Plant, Stairway to heaven

### Menu

* [Menu](/organisations-vivantes/)
* [Chapitre 1 : Monde agile, monde complexe](/organisations-vivantes/chap1/)
* [Chapitre 2 : Penser organique](/organisations-vivantes/chap2/)
* [Chapitre 3 : Dynamique des formes](/organisations-vivantes/chap3/)
* [Chapitre 4 : Choisir sa voie ?](/organisations-vivantes/chap4/)
* [Chapitre 5 : Éléments constituants](/organisations-vivantes/chap5/)
* [Chapitre 6 : Changer la trajectoire de nos organisations](/organisations-vivantes/chap6/)
* [Chapitre 7 : Conclusion](/organisations-vivantes/chap7/)

### Révisions

   Date  |   Objet 
   ------|-------- 
  2014-2016  | Premiers travaux, premiers écrits 
  2016-10-21 | Initialisation du document du gdrive 
  2016-10-31 | Ajout bibliographie, webographie, remerciements, relecture rapide : je dois finir les derniers chapitres 
  2016-11-01 | Intégration de certains commentaires en note de bas de page 
  2017-08-16 | Prise en compte de commentaires 
  2017-11-11 | Reprise du texte, retour markdown / web blog. 
  2017-11-12 | Début de nettoyage d'hiver 
 

