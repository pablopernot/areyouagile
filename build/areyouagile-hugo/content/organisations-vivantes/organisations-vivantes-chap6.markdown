---
title: "Les organisations vivantes"
url: /organisations-vivantes/chap6/
---

# Chapitre 6 : Changer la trajectoire de nos organisations

Comment mener nos organisations dans cette voie ? Comment transformer
celles-ci ? La transformation elle-même répond au même paradigme que la
vision organique de nos organisations. Des dynamiques de groupes
réduits, avec de l’amélioration continue, de l’opportunisme, du sang
neuf, du feedback, etc.

Toute l’émergence de ces organisations joue sur un paradoxe toujours
difficile à maîtriser. Celui-ci se décline d’un côté sur le besoin
d’alimenter l’émergence d’une réponse contextuelle, comme la termitière,
comme le cristal, et de l’autre d’essayer en toute bonne foi, c’est à
dire ici avec une certaine croyance dans ces idées. Malheureusement très
souvent sous couvert d’une adaptation au contexte on est irrespectueux
des idées initiales pour lesquelles nous nous transformions, et bien
souvent cette adaptation au contexte cache la résurgence de nos
habitudes plutôt qu’un vrai changement, qu’une véritable transformation.

## Système complexe émergent

Les gens et les organisations ont raison : chaque transformation, ou
création d’organisation vivante, aura sa personnalité, sa propre
définition. C’est l’idée même de la complexité, de l’agilité, que cette
adaptation au contexte qui tend vers la réponse la plus adéquate, celle
portant le plus de valeur pour le minimum d’effort. Ce constat paraît
simple, il ne l’est pas. Cette adaptation au contexte sous-entend aussi
que quand le contexte change, l’adaptation change aussi. Et le contexte
change sans arrêt. La transformation ou la création de nos organisations
vivantes, comme tout système complexe émergent, est par définition,
émergente, donc adaptée.

Dans mes observations je note quatre paradoxes qui compliquent la
situation.

### Emergence et orthodoxie

Problème récurrent : cette adaptation devrait provenir d’un
apprentissage, cette adaptation devrait remettre en cause certaines
pratiques d’une doctrine, une référence ; malheureusement bien souvent
on ne prend pas le temps d’apprendre, on court-circuite derechef. Bien
souvent les personnes et les compagnies décident qu’elles connaissent
déjà les adaptations nécessaires, et c’est souvent une erreur. Oubliez
le naturel il revient au galop, c’est bien souvent la mémoire du muscle
qui parle. D’autant que cette façon de penser l’organisation, aussi
naturelle qu’elle puisse être, est malgré tout un vrai saut dans un
autre paradigme, il est difficile de se projeter sans le vivre avant.

On devrait laisser émerger tout en suivant les idées sur lesquelles on
se base. Essayer ce qui se fait ailleurs, scolairement en quelque sorte,
avant de savoir pour soi. L’enseignement des arts martiaux au Japon
utilise un idéogramme que l’on appelle Shu-Ha-Ri :

SHU : Fait ce que le mentor te demande de faire sans sourciller.
Applique la règle, pratique, et pratique encore.

HA : En faisant tu as vraiment compris comment cela fonctionnait et
maintenant tu peux faire des adaptations contextuelles.

RI : Tu as intégré l’enseignement, tu le fais intuitivement (entorse ou
non à la règle).

Donc d’un côté on ne peut pas nier l’émergence d’une réponse
contextuelle, de l’autre le besoin de répondre dans un premier temps de
façon très orthodoxe, ou plutôt de baser cette adaptation sur une
connaissance réelle du nouveau paradigme. Cet apprentissage de
l’orthodoxie (du nouveau paradigme) est bien trop souvent ignoré. Soit
parce que l’on en comprend pas l’importance, soit car il est impossible
à mettre en œuvre (pour de bonnes raisons).

Avant d’émerger il faut souvent exécuter les pratiques d’une référence
dans le domaine pour a) ne pas se faire avaler par sa mémoire du muscle
(ses habitudes), et b) découvrir une référence tangible sur laquelle
baser notre émergence.

**Paradoxe : l’émergence s’appuie sur un apprentissage orthodoxe de ce
nouveau paradigme (et pas sur nos habitudes ou nos désirs).**

### Transformation continue

On parle d’adaptation à un contexte, d’émergence contextuelle. Souci :
on s’adapte à un contexte qui n’attend pas pour évoluer, donc on
s’adapte constamment. Il y a l’idée d’une transformation continue, d’une
adaptation permanente.

**Paradoxe : on s’adapte à un contexte qui n’attend pas pour évoluer,
donc on s’adapte constamment.**

### Y croire avant de savoir

En sachant que l’on vise une réponse émergente contextuelle qui sera
changeante, il est impératif de savoir suspendre ses croyances pour
appliquer ses idées au mieux. On ne sait pas ce que sera le résultat. On
veut changer sans savoir réellement quelle forme prendra le changement.
Mais c’est une nécessité pour changer. L’intention est donc clef.

**Paradoxe : Savoir où l’on va (vision), sans savoir où l’on va
(émergence).**

### Le changement doit demeurer une option

On a vu que la nature n’était pas contrainte, qu’elle saisissait
l’opportunité. Ou que dans la contrainte elle s’invitait ailleurs. La
meilleur façon de préserver un espace émergent c’est de le laisser libre
d’échouer, et donc un retour arrière possible est paradoxalement un
amplificateur d’essais. L’invitation à se transformer est très
importante. Il n’y a pas de transformation contrainte, sans droit à
l’erreur.

**Paradoxe : Savoir changer c’est s’autoriser à revenir en arrière (un
pas en arrière peut en amener deux en avant).**

## Poser le cadre de la transformation

La transformation se mène avec les mêmes principes que ceux qui nous
guident pour faire évoluer notre organisation. Parce qu’il s’agit du
même monde. Et que nous venons dire que la transformation, que
l’évolution était continue.

### Invitation et auto-organisation

Le concept le plus important est probablement celui de l’invitation.
L’idée c’est d’inviter les gens à être les acteurs de leur propre
évolution. De toutes manières on n’emmène ni quelqu’un ni une
organisation où elle ne veut pas aller. Rappelez vous des principes de
l’implication des personnes : invitation.

On va donc les inviter. Et plusieurs fois : à venir, à proposer des
idées, à participer aux groupes de discussions, à mettre en œuvre leurs
propositions. Mais comme c’est une invitation ils ne sont pas obligés de
l’accepter : ils peuvent décider de ne pas venir, ne pas avoir d’idée,
de ne pas participer aux groupes de discussion, de ne pas mettre en
œuvre leurs propositions. Pourquoi ? Car toute action coercitive est
contre-productive. Il est illusoire de vouloir changer les gens ou les
organisations sans leur consentement, sans leur envie. Le seul
changement durable se fera avec l’engagement de chacun, et cela démarre
par une invitation.

L’invitation est à double tranchant. Si on refuse systématiquement,
constamment, de venir, de proposer des idées, ou de mettre en oeuvre des
idées d’autres personnes, d’essayer, on va s’interroger sur la
pertinence de son appartenance à ce groupe, à ce cadre, à cette
organisation. On n’évoluerait pas dans le même sens.

Si l’invitation est acceptée, embrassée, et c’est très souvent le cas
dans mon expérience, on va laisser les gens s’auto-organiser. Souvent le
cadre de mes interventions sont des organisations. Nul besoin
d’expliquer aux gens ce qui serait nécessaire, quoi faire, comment le
faire, quoi changer, quoi essayer. Ils le savent bien mieux que moi. Je
ne suis généralement que le signal que l’organisation a décidé
d’essayer. Si tant est que le cadre est bien défini (j’y reviens dès le
prochain paragraphe) laisser les gens s’auto-organiser est aussi
probablement la meilleure chose à faire, en respectant les principes
évoqués précédemment (sur les tailles d’équipes,etc.). Comme tous les
systèmes vivants l’auto-organisation est innée. « Vouloir organiser un
système auto-organisé est non seulement inutile mais c’est surtout idiot
» rappelle avec une pointe d’humour Harrison Owen. Démocratie,
leadership naturel, dictature, peut-être ; l’auto-organisation a surtout
le mérite, étant acté ses principes, de pouvoir évoluer, de ne pas être
fossilisée.

### Autorisation et cadre libérateur

Pour que cette invitation prenne corps, pour que cette auto-organisation
soit possible, il faut extrêmement clarifier deux choses : le cadre et
les règles qui le régissent. Comme évoqué au début de ce texte, dans
quel cadre puis-je évoluer, quelles sont les limites, les contraintes,
les espaces que je peux prendre. Dans quel but, quel est le sens ?
L’organisation le veut-elle ? Notamment sa direction, ceux qui
autorisent ? Puis-je prendre cet espace sans risque ? Voilà des
questions essentielles qui doivent avoir des réponses très claires de la
part de l’organisation.

L’invitation et l’auto-organisation ne pourront s’épanouir que si cette
clarification a lieu.

Mais pourquoi donc chacun s’échine, dans les entreprises modernes, à
vouloir libérer les personnes, à vouloir impliquer, à supporter, aider,
plutôt que dominer, contrôler ou autre façon coercitive de mener ses
équipes ? La réponse est assez simple, le micro-management, le
management à la tâche, sans vision, sans implication, sans
responsabilisation porte peu de

fruits, et ne convient qu’aux entreprises médiocres. Médiocres ? Celles
qui le sont, et celles qui dominent leur marché par le biais d’un
monopole ou d’une position dominante, ou privilégiée, et qui n’ont pas
nécessité à se repenser. Leur effondrement sera soudain.

L’actualité est à une clarification — pas forcément simple — du sens de
l’entreprise, de sa direction, et du cadre qu’elle propose, de ses
règles claires établies. Dans ce schéma : une direction, une vision, un
sens, et un cadre qui définit les contours des possibles, chacun est
alors à même d’occuper son espace, et d’occuper l’espace, de prendre
l’espace. A l’inverse d’une gestion, à la tâche, du micromanagement,
sans visibilité, qui ne laisse qu’un fil, qu’un espace restreint, sans
surprise, sans innovation, sans implication.

Comme largement évoqué précédemment l’”Homme mois mythique” de Fred
Brooks, il est dit qu’une personne peut donner, produire, offrir,
délivrer, faire émerger, utiliser le verbe que vous chérissez, entre un
rapport de 1 à 10. Donner x 1 ou donner x10. On peut oublier les budgets
et les délais avec un tel spectre autant se concentrer sur ce qui permet
aux gens d’être à x10 (et aussi de s’interroger sur une capacité à
rester à x10 : s’autoriser des cycles, des baisses de régimes, des
phases de repos, etc). J’y crois, comme je crois que l’enthousiasme fait
une bonne partie des compétences. Il ne faut pas tant s’occuper des
personnes que du cadre, de ne pas s’occuper de conduire l’énergie, la
dynamique, que de la libérer et la laisser prendre la forme qu’elle
veut, comme le cristal, comme la termitière.

### Petites victoires

### Cycles

### Storytelling

### Émotion

Dans ce monde qui se rêvait cartésien pour le confort de l’esprit —- «
Je pense donc je suis, ouf »—- l’irruption de l’émotion est un ennui.
Elle passe pour quelque chose d’irrationnel. Mais pourquoi la nier ?
Depuis 30 années elle revient sur le devant de la scène (psychologie
évolutionniste, « erreur de Descartes », Damasio, etc.). Aucune de nos
décisions importantes n’est prise sans émotion. Dans ce monde complexe
il faut allier raisonnement mathématique, et instinct ou intuition, le
fruit de notre vie et de celles de nos ancêtres.

D’ailleurs personne n’en ignore la réalité quand il s’agit de faire des
grands choix de vie : choisir un compagnon/compagne, choisir sa voie,
etc.

Si donc cet aspect émotionnel, intuitif, instinctif, prend une si grande
part dans notre vie, nul doute qu’il est la clef dans notre façon de
changer, d’appréhender une nouvelle façon de faire, de penser. L’image
utilisée par les frères Heath dans leur livre « Switch[^32] » (le titre
est évocateur) n’indique pas autre chose : c’est le conducteur
d’éléphant indien, et son éléphant. Le conducteur est la face
cartésienne, raisonnante, du duo, mais elle ne peut aller nulle part
sans l’adhésion de la partie plus sauvage qu’est l’éléphant. Pour amener
l’éléphant, il faut le séduire, lui montrer des passages évidents,
rendre aussi visible les voies sans issue. Ignorer que sans l’accord de
l’éléphant nous n’irons nulle part c’est échouer d’avance. Dans toutes
conduites du changement il faut ainsi autant jouer sur la raison que sur
les émotions.

Jouer sur les émotions, c’est l’effroi. La manipulation au plus haut
point. Mais déjà en disant cela vous confirmez l’importance de
l’émotion, et notre faiblesse devant sa gestion. Frank me rappelle la
(//TODO Frank ? :) ) de Naomi Klein, la lecture de “thinking fast and
slow” de Daniel Kahneman[^33], ou même le “traité de manipulation à
l’usage des honnêtes gens”[^34] dépriment sur notre asservissement aux
biais cognitifs. Je ne peux pas vous dire autre chose, il me semble que
comme chaque découverte, on peut en faire bon comme mauvais usage. Mais
je ne pense pas qu’une véritable conduite du changement personnelle ou
organisationnelle se déroulera sans émotion.

Rappelez vous vos changements de croyances, d’idéaux, de principes, ne
sont-ils pas produit avec un afflux émotionnel ? Comme une énergie
rendue disponible pour une autre voie, une catastrophe pour reprendre
Thom. Comme le rire, qui se produit, par exemple dans les sketchs
absurdes de Monty Python, quand on a emmagasiné de l’énergie (en nous
projetant, on retrouve ici encore le storytelling, les neurones miroir)
et qu’elle débouche sur une absurdité, elle se déverse par le rire, seul
issue pour cet afflux d’énergie.

Pensez aussi comme me le rappelle Oana, aux ruptures soudaines avec
certains de vos proches, une barrière a été franchie, un comportement
jugé hors des frontières du possible. On assèche immédiatement toutes
communications. Il n’y aura plus aucune énergie qui ira dans ce sens. Ce
changement est très rapide, très violent, très soudain, et souvent
définitif. Et pourtant on parle d’un ami proche. La rupture, le
changement, la catastrophe (Thom encore) est à la hauteur de l’énergie
présente, celle associée à un ami proche.

En écho du discours de Switch des frères Heath, la raison indique la
voie, mais c’est le flux d’énergie émotionnel qui va nous pousser à
avancer dans ce sens. Comme dans un jeu d’irrigation, le changement
paradigme de pensée est comme une rivière détournée. Il faut donc
savoir, à l’instar du rire, permettre cet afflux d’émotion et lui donner
l’occasion de s’orienter différemment. Là aussi on pourra y voir le
visage d’une manipulation je ne peux rien vous dire contre cela. Comme
toujours cela dépend de l’intention que l’on met dans nos actes. Chaque
chose peut être utilisée à bon ou mauvais escient. J’espère les utiliser
à bon escient. On pourrait prendre deux exemples : la transformation de
Unilever durant les années 90 racontée dans “To the desert and back” de
Philip Mirvis[^35], et les *Open Agile Adoption* de Dan Mezick.

#### Des évènements à charge émotionnelle

Quand Mirvis nous conte l’histoire de la transformation d’Unilever, il
analyse a posteriori que celle-ci a été rendu possible par des moments
clefs qui ont permis des bascules significatives. Chacune de ces
bascules ayant bien souvent été cristallisées par des évènements hors du
commun (dans le désert, dans les Ardennes, etc.) avec des mises en
scènes artistiques, et qui apparaissent symboliques a posteriori même si
elles n’étaient pas nécessairement voulues au départ. Cristallisées ou
plutôt rendues possibles par ces évènements qui ont mis en disponibilité
cette émotion propice à générer des déclics, des épiphanies, des
bascules dans les modes de pensée. Bien souvent ces évènements ont été
amplifiés par une approche artistique, de véritables mises en scène.
C’est bien le sens du mot sensibiliser.

Cela permet de mieux comprendre qu’il se déroule quelque chose. On peut
être pour ou contre, selon le degré d’orchestration, que l’on
rapprochera ou non, de celui de manipulation. La meilleure de se
prémunir c’est d’annoncer ce que l’on cherche à faire, pourquoi on le
fait ainsi et de laisser les personnes libres de participer.

A l’instar du conteneur permettant l’émergence dans les environnements
complexes, il faut peut-être mieux proposer un espace sécurisé dans
lequel l’émotion pourra se déverser, que d’orchestrer trop sa mise en
scène.

#### Un espace sécurisé, bienveillant

C’est cette liberté de participer que l’on retrouve dans les *Open Agile
Adoption* proposés par Dan Mezick (qu’il nomme désormais *OpenSpace
Agility*[^36]). Sur invitation, on va poser un cadre propice à un espace
sécurisé. Et c’est cette espace sécurisé qui pourra permettre
l’émergence de l’émotion. Elle-même étant l’énergie qui transforme.

Quand Olaf Lewitz parle protection, de bienveillance et de vulnérabilité
il décrit typiquement ce type de cadre : un espace protégé propice à
l’émotion. Un moment sensible. Pour avoir vécu un certains nombres
d’open agile adoption (j’ai pu croiser et tisser des liens d’amitié avec
Dan Mezick au tout début de sa proposition), il est surprenant
d’observer comme cette émotion ne demande qu’à émerger.

### Homéostasie

Résilience

Schémas propagation

### Menu

* [Menu](/organisations-vivantes/)
* [Chapitre 1 : Monde agile, monde complexe](/organisations-vivantes/chap1/)
* [Chapitre 2 : Penser organique](/organisations-vivantes/chap2/)
* [Chapitre 3 : Dynamique des formes](/organisations-vivantes/chap3/)
* [Chapitre 4 : Choisir sa voie ?](/organisations-vivantes/chap4/)
* [Chapitre 5 : Éléments constituants](/organisations-vivantes/chap5/)
* [Chapitre 6 : Changer la trajectoire de nos organisations](/organisations-vivantes/chap6/)
* [Chapitre 7 : Conclusion](/organisations-vivantes/chap7/)



[^32]: Chip & Dan Heath, “Switch : How to Change Things When Change Is Hard”

[^33]: “Thinking, fast and slow”, Daniel Kahneman 

[^34]: “Petit traité de manipulation à l’usage des gens honnêtes”, Robert-Vincent Joule

[^35]: “To the desert and back : The Story of One of the Most Dramatic Business Transformations on Record”, Philip H. Mirvis

[^36]: OpenSpace Agillity : http://openspaceagility.com/
