---
title: "Les organisations vivantes"
url: /organisations-vivantes/chap1/
---

# Chapitre 1 : Monde agile, monde complexe

> « Aucun plan ne survit au premier contact avec l’ennemi »
> -- Von Moltke

Le présent texte a pour objectif de représenter mes réflexions concernant les dynamiques dans nos organisations, notamment autour de mon activité d'accompagnement en coaching organisationnel. Je travaille avec des organisations. J’aspire à les accompagner, les voir grandir, s’adapter, évoluer. Comme un organisme vivant elles s’adaptent ou meurent. Après un siècle d’industrialisation nos idées sur les organisations sont faussées. Le modèle des entreprises de nos parents et de nos grands-parents est mal en point. Et tant mieux. Car le monde a évolué. Naturellement ou contraint par les assauts de notre mode
de vie. D’autres horizons appellent d’autres réponses, d’autres façons de faire. Je travaille comme mentor[^1] ou coach d’un mouvement que l’on appelle Agile. L’organisation c’est aussi le lien social le plus fort actuellement. C’est là que je passe la plus grosse partie de mon temps. C’est là que je fais la majorité de mes rencontres. C’est là que se déroule la majorité de mes interactions sociales. La socialisation est l’un des traits marquants de notre espèce. Soyons attentif à la façon dont nous la traitons.

Aujourd’hui nous devons relever trois défis:

* Celui de la **complexité** du monde récent.
* De sa **frugalité**, la préservation de nos ressources pour notre survie.
* Et de l’**immatérialité** procurée par les avancées technologiques, la valeur est dans l’information, dans le savoir qui transite dans tous les réseaux. Je vous propose de partir à la rencontre des réponses que nous pourrions amener à ces trois défis : complexité, frugalité, immatérialité. En tous cas de celles, modestes, qui émergent de mon activité quotidienne.

## Complexité

Les entreprises, les managers pensent qu'ils ont besoin de temps et
d'argent, de plus de temps, de plus d'argent.

Elles, ils, se trompent. Dans le monde **complexe**, changeant,
fluctuant dans lequel nous vivons ce n'est ni avoir plus de temps ou
plus d'argent qui fait la différence. Ce qui fait la différence pour les
entreprises, pour les managers, c'est une capacité à engager les
personnes avec lesquelles ils travaillent car l'écart entre l'impact
produit par une personne non engagée ou par une personne engagée est
immense. Ce qui fait la différence c'est maximiser la valeur de ce que
nous produisons en, idéalement, minimisant les efforts pour le produire.

Oubliez le temps et l'argent. Ayez un *focus* sur l'engagement, la
responsabilisation, l'implication des personnes avec qui vous
travaillez, et assurez vous très régulièrement de produire les bonnes
choses. L'engouement actuel pour le terme Agile n'est rien d'autre que
la traduction de cela.

*Agile* c'est finalement le terme marketing pour *complexe*. Dire que
l'on veut être Agile c'est d'abord acter que le monde est complexe.
Complexe vient de "entrelacer". Les besoins évoluent, les technologies
évoluent, la communication est globale, instantanée. Impossible
désormais de nier que les choses, les projets, les produits vivront en
partie de façon imprévisibles, que l'inattendu sera là, impossible
d'avoir des certitudes dans ce monde complexe. Être agile c'est savoir
évoluer dans ce monde complexe. Et, vous en conviendrez, *Agile* est un
terme plus facile à attraper que "complexe". Je doute que vous ayez
embauché un "coach complexe" ou que vous vous soyez lancé dans une
"transformation complexe".

Ainsi donc être agile c'est savoir évoluer, être performant (encore un
mot magique) dans ce monde complexe. Comment est-ce que cela se met en
œuvre ? La première des choses pour les managers c'est apprendre à
laisser les gens se responsabiliser, être autonome. Quand une personne
est responsabilisée, est autonome, elle devient beaucoup plus facilement
engagée. Et l'**engagement** multiplie considérablement l'**impact** de
ces personnes. Vous savez très bien vous même que lorsque vous subissez
les choses sans pouvoir les contrôler, sans avoir envie, ni votre mot à
dire, votre performance est, comment dire, très médiocre. En tous cas
c'est ainsi pour moi. Et si au contraire vous êtes engagé en étant
responsabilisé, en ayant un pouvoir de décision, une autonomie, vous
devenez bien plus impliqué, bien plus performant.

Rappelez vous ces histoires de champs de batailles où les généraux
campaient du haut de leurs collines à dicter à chacun de leurs soldats
ou de leurs escouades les mouvements et les actions qu'ils devaient
opérer. On imagine mal un tel dispositif aujourd'hui, dans une guerre
moderne. Si une troupe ou un commando n'a pas d'autonomie elle sera bien
trop lente, bien trop inefficace, bien trop vulnérable. La
responsabiliser, lui donner de l'autonomie, c'est la rendre bien plus
efficace mais ce n'est pas la laisser libre de faire ce qu'elle veut.
Cette troupe imaginaire a bien un objectif, elle doit détruire tel pont,
elle a bien des règles à respecter : elle ne peut pas aller à tel
endroit car les forces ennemies sont présentes, elle ne doit pas
détruire tel autre ouvrage, etc. Ce n'est plus le geste que vous dictez
à vos équipes, c'est le cadre : l'objectif et les règles que vous
clarifiez. Libre à cette équipe, à ces personnes, de prendre cet espace,
c'est çà qui fait toute la différence en terme d'impact de performance
ou d'innovation comparé au suivi d'un processus pas à pas, fossilisant.

Être agile, évoluer dans ce monde complexe, c'est donc d'abord
responsabiliser, donner de l'autonomie, donner un pouvoir de décisions
sur tout un ensemble de sujets. Le management moderne c'est ainsi
d'abord un lâcher prise, un laisser faire, et une clarification du cadre
et des règles. Mais les gens ne seront autonomes, responsables, que si
ils ont le droit de se tromper. Sinon votre soi-disant
responsabilisation est un mensonge. Ce n'est que si vous avez le droit
de vous tromper que vous pouvez sereinement prendre tout le temps la
décision que vous pensez être la meilleure. Si vous n'avez pas le droit
de vous tromper votre décision est brouillée par d'autres
considérations, vous ne choisissez plus la meilleure. Là encore une
différence fondamentale se fait. Pour que cette autorisation de se
tromper soit facile à donner on va essayer de travailler sur des petits
morceaux. Si on se trompe sur un petit morceau ce n'est pas grave, si on
se trompe sur un gros morceau c'est plus embêtant, implicitement si vous
travaillez sur de gros morceaux vous vous interdisez de vous tromper.
Petites itérations, peu de choses à la fois, il y a plusieurs stratégies
mais on travaille sur de petites choses pour avoir l'autorisation
implicite de se tromper, on n'a pas la peine de se protéger.

Être agile, évoluer dans ce monde complexe, c'est ainsi
**responsabiliser, rendre autonome**, pour transformer l'impact que
peuvent avoir les gens en prenant cet espace. Autoriser les gens c'est
aussi les **autoriser à se tromper**. Cela implique une grosse part
d'**amélioration continue** le but n'étant pas de se tromper. Pour
autoriser facilement les gens à se tromper, que l'interdiction ne soit
pas implicite, on travaille par itération, ou sur peu de choses à la
fois.

Là il y a un autre paramètre important que les gens ne saisissent pas
forcément. Pour avoir un vrai regard, un vrai apprentissage, une vraie
observation sur ce que l'on a produit. Pour s'assurer de ne pas se
tromper et ainsi dire ça marche, ou ça ne marche pas. Pour avoir ce
regard, ce **feedback** dirait-on en anglais, et qu'il soit réel, il
faut observer des **choses finies**, des choses qui fassent sens par
elles-mêmes, des petits morceaux (on a dit que l'on travaillait sur des
petites choses pour autoriser l'erreur) autonomes qui fassent sens. Par
exemple ne me dîtes pas que vous avez préparé tout votre modèle de
données pour les futurs rapports financiers. Au moment d'ajouter les
objets métiers, l'interface utilisateurs, les aspects ergonomiques, les
choses vont changer. On croit que l'on a quelque chose de fini mais
c'est une illusion. On fait donc perdurer le risque de se tromper. Ce
n'est bon pour personne. Quelque chose de fini aurait été l'un des
rapports ou encore plus simplement un chiffre de l'un des rapports, etc,
mais cela aurait mis en œuvre un bout d'interface, un bout d'algorithme,
d'objet métier, un bout de stockage de données, ainsi une approche
verticale. Et surtout **ce petit morceau autonome faisant sens produit
par lui-même de la valeur**. Il amène déjà par lui-même quelque chose
d'utile.

Être agile, évoluer dans ce monde complexe, c'est ainsi responsabiliser,
autonomiser, pour transformer l'impact que peuvent avoir les gens en
prenant cet espace. Autoriser les gens c'est aussi les autoriser à se
tromper. Cela implique une grosse part d'amélioration continue le but
n'étant pas de se tromper. Pour autoriser facilement les gens à se
tromper, que l'interdiction ne soit pas implicite, on travaille par
itération, ou sur peu de choses à la fois. Pour être sûr que l'on ne se
trompe pas on porte un regard, un *feedback* sur les choses produites,
et pour pouvoir vraiment en juger, ces choses produites sont des petits
morceaux autonomes faisant sens.

Pour le management le changement de posture c'est le lâcher prise, le
laisser faire. Pour les gens du métiers, du produit, c'est cette
capacité à ne pas arriver avec une solution globalisante, mais savoir
découper en petits morceaux autonomes qui font sens, porteur de valeur,
et qui vont s'étoffer peu à peu.

C'est pour savoir produire ces petits morceaux autonomes faisant sens et
porteur de valeur que toutes ces organisations se recomposent en
**équipes pluridiscplinaires** : pour avoir cette **autonomie**, cette
capacité à délivrer de façon très verticale. Entres autres raisons.

À cette réflexion sur le être agile, sur le monde complexe qui nous
entoure, il faut ajouter un paramètre : nous n'avons pas le temps de
faire tout ce que nous voudrions faire, nous en avons très très rarement
la capacité. Être agile, évoluer dans un monde complexe, c'est donc
aussi une capacité à **prioriser par valeur** (valeur d'apprentissage,
valeur concrète, etc.). Ainsi on arrive à **la promesse du être agile :
Si je suis capable de responsabiliser et d'autonomiser je multiplie
l'impact et la performance des personnes, et si je sais délivrer des
petits morceaux autonomes faisant sens, apportant de la valeur, et
priorisés par celle-ci, je sais lire, écouter, apprendre, rebondir sur
mon marché. J'économise en échouant rapidement. J'engrange des bénéfices
plus rapidement en n'attendant pas pour délivrer des ensembles trop gros
de choses.**

Pour évoluer dans ce monde complexe, pour être agile faites ce que vous
voulez tant que vous responsabilisez, vous autonomisez, ainsi vous
transformerez l'impact des gens. Faites ce que vous voulez tant que vous
avez un *focus* fort sur l'amélioration continue liée à votre
autorisation à l'échec, à votre compréhension que les bonnes pratiques
émergent dans ce monde changeant. Faites ce que vous voulez tant que
vous avez un vrai *feedback* un vrai regard sur des choses finies
faisant sens, pas sur le nombre de fonctionnalités que vous avez
déployé, pas sur le nombre d'heures que vous avez passé, mais sur le
nombre d'usagers en plus, sur le nombre de coups de téléphone à la
*hotline* liés à votre dernière livraison, etc. Des mesures souvent
externes et pas internes. Ainsi si vous êtes en mesure de prioriser ces
petits éléments autonomes faisant sens et porteur de valeur vous saurez
lire, apprendre, rebondir sur votre marché à moindre coût et en
engrangeant rapidement bénéfices. C'est ça la promesse de l'agile.

Vous n'avez pas de problème de temps et d'argent. Vous avez un problème
d'impact de vos collaborateurs qui ne sont pas assez responsabilisés,
qui n'ont pas assez d'autonomie, et vous avez un problème pour maximiser
la valeur : vous n'êtes pas en capacité de réaction, d'apprentissage vis
à vis du marché.

![Temps Complexes](/organisations-vivantes/media/temps-complexes-4.png)


Un peu partout fleurissent les invocations : soyez heureux, soyez motivés, soyez impliqués, soyez responsables, être heureux, happiness chef blabla. Mais cela ne se décrète pas. Concernant l'engagement, concernant l'implication, ce n'est pas une injonction à laquelle on peut répondre. Vous ne pourrez pas décider de rendre telle ou telle personne
motivée. Mais vous pouvez créer un cadre, un contexte, qui permettent à ces personnes d'être motivées, impliquées. On voit régulièrement revenir ce message : une très petite part des employés dans le monde se sentent impliqués.

Dans le manifeste agile il y a un principe où il est écrit : faîtes des
projets avec des gens motivés. Généralement on s'esclaffe à ce moment.
Naturellement que les projets marchent avec des gens motivés. Quelle
idée ! Et bien donc oubliez tout le reste, focalisez vous sur l'idée
d'engager les personnes avec lesquelles vous travaillez.

Certains expliquent qu'entre une personne engagée et une personne non
engagée la différence est énorme. Dans le [Mythical Man Month](https://archive.org/details/mythicalmanmonth00fred) l’auteur
(Fred Brooks) exprime l’idée qu’une personne (en l’occurrence dans son
cadre, un développeur informatique) à un ratio de productivité de un à
dix selon le contexte. Sa capacité fluctuerait ainsi de un à dix selon
le contexte. Mes observations -sur moi même et les autres- tendent à
confirmer cette information. Et je pense que la grande part de cet écart
viens de l’implication de la personne. Observez vous vous même à
réaliser une tâche sur laquelle vous ne vous sentez pas impliqué, et une
sur laquelle vous vous sentez impliqué, que vous vous êtes approprié.
Personnellement je passe de dépité nonchalant peu inspiré à bulldozer
frénétique grandement productif.

On ne peut pas décréter la motivation, ni l’implication, ni
l’appropriation. Pourtant il y a un domaine, sujet d'études, où on
découvre des personnes impliquées, engagées. Avec un haut niveau
d'exigence. Quand quelque chose échoue, elles essayent d'autres façons,
ne se laissent pas abattre, elles aiment chercher des alternatives. Même
les plus introverties d'entres elles appellent des amis à l'aide ou vont
lire la documentation. Car -- faut-il l'évoquer ? -- la documentation en
ligne est pléthorique : guide du "nouveau/newbie", vidéos,
accompagnements, wikis, forums. Mais quel est donc ce domaine ? Les gens
que j'évoque y passe plusieurs par semaine, voire plusieurs dizaines
d'heures, et, cerise sur le gâteau, ils payent pour cela...

Il s'agit des jeux vidéos, et plutôt des joueurs de vidéos. Pourquoi
donc cette implication dans ce domaine ? C'est exactement ce que nous
recherchons dans nos organisations. Tous ces joueurs de jeux en ligne
qui payent pour échouer, recommencer, apprendre, refaire, fournir une
documentation de qualité, ne pourrait-on pas avoir le même engagement en
entreprise ? L’industrie du jeu vidéo est florissante, comprenez qu’elle
implique d’énormes sommes d’argent. Avant donc pour nous même d’en tirer
des enseignements pour l’organisation, elle s’est interrogée sur
elle-même car son succès est liée à l’implication qu’elle va engendrer.
Les observations qui ont découlé de ces recherches sont une mine pour
nous. À voir : [Jane McGonigal](https://www.ted.com/talks/jane_mcgonigal_gaming_can_make_a_better_world?language=fr)
et à lire : *Reality Is Broken: Why Games Make Us Better and How They
Can Change the World* de la même personne.

Mais alors que j’évoque ce sujet, je reçois le bulletin scolaire de mon
fils aîné, Mathieu. Mathieu a de bons résultats scolaires mais le carnet
indique que si Mathieu était impliqué il aurait de très bons résultats
scolaires. Je cherche Mathieu :

-   Mathieu ? Mathieu ?
-   (de l’autre pièce) Oui papa ?
-   Peux-tu venir que l’on parle de ton bulletin scolaire.
-   Attend, là je ne peux pas, je suis en train de construire la
    quatrième tour de mon château fort à
    [Minecraft](https://minecraft.net/en-us/) et elle n’a pas exactement
    les mêmes décorations que les autres, ni la même constitution en
    fer, je ne peux pas la laisser ainsi.
-   Oui c’est exactement de cela que je souhaite te parler...

En d’autres termes Mathieu est très impliqué dans son jeu, et ses
résultats sont très bons. Qu’est ce qui manque à l’école vous récolter
ce même engagement ? Qu’est-ce qui manque aux organisations pour avoir
ce même type d’implication ?

Ce n'est déjà pas la compétition ou l'envie de gagner qui va faire cette
différence. Je ne crois pas que l'on gagne réellement à Tétris ou Candy
Crush Saga, ni réellement à aucun des jeux en ligne de mon fils, il perd
autant qu'il gagne, ce n'est pas là sa réelle recherche. Ce dont nous
avons besoin et ce que ces études sur les jeux vidéos nous enseignent
est désespérément simple à énoncer, mais diablement difficile à mettre
en œuvre.

## Un objectif clair

D'abord un jeu qui engage a un objectif clair. Amusez vous à tester si
votre projet, votre produit, votre organisation a un objectif clair.
Facilement énonçable. Repensez à cette célèbre citation de Einstein :
"si tu ne sais pas l'expliquer à un enfant de huit ans c'est que tu ne
sais pas ce que tu veux". Votre travail de dirigeants, de managers,
c'est cette clarification.

## Des règles claires

Je ne peux pas lancer de sortilège si je n'ai plus de points de mana, je
ne peux pas cumuler le pouvoir de ces deux armes, mais tiens je pourrais
utiliser les maisons pour protéger ma tour et pas l'inverse. Pour
prendre cet espace qui fera toute la différence, il convient de
clarifier la cible, l'objectif, mais aussi de délimiter le cadre dans
lequel on peut s'épanouir, surprendre, imaginer, s'approprier. Vous
devez clarifier les règles du jeu. Naturellement si vos règles sont trop
contraignantes l'espace que vous proposerez sera faible.

## Feedback, un regard en retour

Si je tue ce monstre j'obtiens 100 points, si je prends cette ville je
gagne 10000 écus, ah mince il n'y avait rien dans ce donjon. Dans le jeu
on sait constamment les bénéfices (substantiels ou catastrophiques) que
nos actions entraînent. Dans l'organisation cela devrait être pareil.
J'ai livré cette fonctionnalité mais elle n'a reçue aucun accueil
favorable, alors que l'autre oui, mais pas de la façon attendue. Oui
cette action a été reçue positivement, celle là non, celle-ci a
déclenché cela, l'autre cela. Peu importe le résultat de votre activité,
vous devriez le mesurer et le connaître, le faire connaître. Sans cela
votre implication se réduira. A quoi bon travailler si on ne sait pas ce
que devient le fruit de notre labeur. Le premier pas vers le sens, c'est
ce feedback.

## Invitation

Plus difficile. Aucun des joueurs de jeux vidéos n'est obligé de jouer.
Idéalement aucun des membres de l'organisation ne devrait être obligé de
venir s'impliquer, travailler. Pas facile. On se heurte à une autre
réalité, celle de la pyramide de Maslow, il faut bien d'abord avoir un
toit, manger, bref... avoir une organisation et un salaire avant de
penser engagement. C'est un vaste débat. Mais difficile aujourd'hui
d'inviter à travailler, en tous cas pas simple selon les contextes.
Alors comment utiliser la notion d'invitation ?

Prenons quelques exemples. Jim McCarthy, celui là même des [core protocols](http://www.mccarthyshow.com/online/) dirigeait un temps Microsoft Studio. Face à toutes ses équipes il a essayé l'invitation :
voici 15 projets*[1]*, "proposez les équipes que vous voulez pour que les projets soient des succès". Émoi chez les managers. Les gens vont faire n'importe quoi. Mais non. Nous sommes des systèmes vivants s'entête à rappeler Harrison Owen (l'inventeur de l'openspace), nous sommes naturellement responsables. C'est en déresponsabilisant les gens que vous obtenez parfois des comportements complètement ubuesques. Dans ce cadre et suivant l'objectif clairement édicté : "que les projets soient des succès" les gens ont mixé intelligemment socialisation (les dynamiques de groupes, de copains) et compétences. Mais surtout effet
crucial de l'invitation -- de la transparence sur la réalité des choses : personne ne va se porter volontaire sur certains projets. Si il y a deux projets "qui puent", ils vont se retrouver sans personne. C'est une très bonne nouvelle. On découvre qu'il y a des sujets compliqués de façon flagrante, peut-être avec surprise. Non pas qu'il ne faille pas faire ces projets, mais il devient clair qu'il faut leur donner d'autres moyens.

Autre exemple, inspiré par [Dan Mezick](https://osa.areyouagile.com),
utilisez l'invitation dans vos réunions. Imaginez le grand chef qui
propose une réunion **réellement** sur invitation. Ce grand chef, on va
l'appeler Mesmaeker (Gaston Lagaffe...), invite bien douze personnes.
*Première option*: personne ne se présente. Et c'est une bonne nouvelle
car c'était vraiment une réunion inutile. Tout le monde a gagné du
temps. *Deuxième option* : personne ne se présente or c'était une
réunion utile. L'apprentissage est clair pour Mr Mesmaeker, il doit
retravailler sa communication pour faire comprendre l'importance,
l'enjeu de cette réunion. Là, de façon manifeste, sa communication est
ineffective. *Troisième option*: personne ne se présente or c'était une
réunion utile, et l'importance en a été comprise. Mais peut-être la
priorité n'était pas claire. Des choses importantes on en a tous à
faire. Mr Mesmaeker aurait du préciser : et "c'est une priorité", ou
"c'est plus important que...". Encore une question, problème de
communication rendu visible par l'invitation. *Quatrième option* :
quatre personnes se présentent sur douze. C'est les quatre "bonnes
personnes" car il s'agit de celles qui ont compris l'importance, qui
sont prêtes à s'investir, qui ont priorisé l'importance de façon à
mettre ce sujet en avant (celles qui ont le temps). *Cinquième option* :
celle que vous connaissez tous, ce n'est pas une réelle invitation, les
douze personnes sont là. Elles disent oui "comme il se doit". Mais on ne
sait pas lesquelles ont le temps, ont compris l'importance, ont intégré
la bonne priorité, on ne fait nécessaire d'effort sur le format de la
réunion puisque tout le monde y est contraint. Et tout le monde dit oui
mais on ne sait pas vraiment ce que cela veut dire.

Vous avez compris l'invitation est clef pour l'engagement (et donc la
conduite du changement). Mais pour nos organisations c'est surtout aussi
un formidable outil de transparence.

L'invitation est aussi un des piliers de l'approche **open agile
adoption** ou **openspace agility**. L'idée est d'inviter chacun à un
forum ouvert de façon cyclique, forum ouvert qui possède un objectif et
un cadre (tiens tiens), d'inviter chacun à donner le comment de la mise
en œuvre. On obtient ainsi quelque chose de très engageant et de très
respectueux des personnes et des organisations. Je vous renvoie à
**openspace agility**/**open agile adoption** :
[ici](/tag/openagileadoption/)

Ainsi à nouveau, vous n’avez pas de problème de temps et d’argent. Vous
avez une question d'implication et d'engagement. Et sous ces préceptes
assez simples : **objectif clair, règles claires, feedback et
invitation**, se cache un spectaculaire levier. Mais pour les
dirigeants, les managers, les leaders, c'est un travail constant de
clarifier, de supporter, de faire évoluer, d'accompagner ces objectifs
clairs, ces règles claires, ce feedback et cette invitation.

Quand Dan Mezick m'a parlé de ces idées sur l'engagement inspirées par
le monde des joueurs de jeux vidéos, courant 2013, il enchaînait souvent
en citant aussi des enseignements de Tony Hsieh le leader de Zappos
(quand Zappos était symbole de réussite). Ces quatre autres éléments que
je vais vous donner faisaient complément, renforçaient les précédents
points ([être engagé - partie 1](/2017/09/etre-engage-1/)). C'est avec
les années que j'ai vraiment compris que cette seconde liste équilibrait
autant la première : garde-fou, résonance.

Voici les quatre points en résonance de ceux évoqués précédemment. Pour
être impliqué, engagé, il faut :
 
## Un sentiment de contrôle

C'est le garde-fou des règles claires évoquées dans la première série
sur l'engagement ([être engagé - partie 1](/2017/09/etre-engage-1/)).
Dans celle-ci je vous parlais d'un cadre, lié à un objectif clair, et
des règles claires qui aidaient à prendre l'espace, à s'impliquer, à
s'engager. Elles impliquent à la condition qu'elles n'entravent pas trop
l'autonomie. Elles sont là pour donner de l'autonomie, pas la
contraindre. Il s'agit d'un sentiment de contrôle de son outil de
travail, pas de contrôle de l'autre. Imaginer un individu dans un groupe
qui démarre son activité et qui doit demander à tel autre groupe l'accès
aux ressources, aux machines, à tel autre groupe le droit de sécurité,
mise en production, accès à telle donnée... qui ne peut pas manipuler
véritablement son outil de travail car sa juridiction est morcelée. Pas
de sentiment de contrôle de son outil de travail, perte de son
implication. C'est pour cela que souvent dans un monde agile on
recherche des "feature teams", des équipes pluridisciplinaires, elles
possèdent une capacité d'autonomie, elles contrôlent leur périmètre. Les
gens doivent pouvoir s'approprier leur périmètre. Des règles claires
mais qui préservent l'autonomie.

## Un sentiment de progrès

Là c'est simplement l'écho de l'idée du feedback que j'évoquais
précédemment. Il susciter l'engagement il faudrait régulièrement savoir
ce que l'on produit, génère, obtient. Il faudrait (très) régulièrement
mesurer. Pour cela il faudrait (très) régulièrement finir des choses. Et
ainsi à travers ce besoin de sentiment de progrès je voudrais
sensibiliser les gens à ne pas travailler sur trop d'éléments à la fois.
Le multitâche est très nocif à notre productivité. Voyez ce schéma :
avec une tâche je suis normal, avec deux je suis meilleur car je peux
basculer de l'une à l'autre pendant les délais incompressibles. Avec
trois ma productivité est déjà généralement en dessous de celle que j'ai
en traitant une tâche à la fois. Si je passe à quatre ou cinq cela
devient la bérésina.

J'en viens à dire aux organisations : "cessez de travailler sur dix
projets à la fois, focalisez vous sur les trois plus importants". Des
fois on me répond, narquois, "mais Pablo c'était le 8,9,10 qui
permettaient de te payer". Alors je reprends (eh eh) : "arrêtez le
septième vous le finirez plus vite". C'est une approche cohérente
globale : si vous savez découper en petits morceaux vos
projets/produits, vous saurez délivrer une moelle substantifique dans un
laps de temps plus court, et passer au suivant. Vous démarrez le suivant
plus tard, mais avec un focus beaucoup plus fort, et aussi cette
capacité à condenser la valeur dans un scope plus réduit. Et ainsi de
suite.

Pour être engagé, il faut garder un sentiment de progrès, nourrit par le
feedback, il faut savoir être focalisé sur un nombre réduit d'activités.

## Appartenance à une communauté

Avec l'invitation on entre dans un groupe, une famille, une équipe.
C'est une bienvenue. L'appartenance à une communauté parait une évidence
pour se sentir engagé, impliqué. Cela l'est, une évidence. J'en profite
pour revenir sur ce qui fait un groupe, une équipe, sur quelques
chiffres importants. Ces chiffres je les évoquais dans [la horde
agile](/2014/01/mini-livre-la-horde-agile/), en 2013.

### Le chiffre de Dunbar

Dunbar est un sociologue/anthropologiste, il constate que les villages
de la préhistoire se scindent autour de 150/220 personnes, laissant
penser que notre physiologie nous interdit d'avoir plus de 150/220
réelles connections sociales. Si donc je veux encourager un sentiment
d'appartenance à une communauté je vais limiter mes
"divisions,départements,agences" à 150/220 personnes.

### Plaisir d'appartenance à un groupe

![Niveau de satisfaction](/images/2017/10/satisfaction.png)

Avec les recherches de Christopher Allen, on évoque le plaisir
d'appartenir à un groupe, [quel chiffre déclenche cette satisfaction](/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/) ? Le tableau ci-contre indique que le meilleur sentiment de
satisfaction, qui appuiera un sentiment d'appartenance, se ressent
autour de 7 et 50. Si donc je suis un dirigeant je vais découper mon
organisation en divisions de 150/220, puis en départements de 50, puis
en équipe de 7.

Souvent c'est le moment où quelqu'un dit qu'on ne peut ainsi pas faire
de "gros projets", de "grands produits" avec une équipe de 7 par manque
de capacité. Peut-être mais avec 7 équipes de 7 on le peut.
Naturellement les "gros projets" entraînent un besoin de synchronisation
(c'est tout l'enjeu du *agile at scale*, **culture et
synchronisation**), mais il est bien plus facile de synchroniser 7
équipes de 7, que de travailler "en rateau" avec 50 personnes.

### Communications interactives

![Charge / Combinaisons](/images/2017/10/combinaisons.png)

Enfin le chiffre 7 est corroboré par l'observation de Fred Brooks (dans
le vieux et célèbre "homme mois mythique"), qui démontre visuellement
que le nombre d'interactions devient insoutenable dès que l'on franchit
la barre des 7/9 personnes. C'est à dire qu'une vraie communication et
dynamique d'équipe se bâtit... de 4 (à partir de quatre sensation
d'équipe, avec c'est encore des individualités, ce n'est pas négatif
cependant), jusqu'à 8. Au delà de 8 on observe que les communications
interactives se scindent en deux groupes.

Ainsi donc à la recherche d'un sentiment d'appartenance je vais décliner
mon organisation en groupes de 150/220, puis 50, enfin des équipes de 4
à 8. Ainsi mon implication et mon engagement seront meilleurs.

### Courbe de Allen

![Allen Curve](/images/2017/10/allen-curve.png)

Puisque l'on parle qualité de la communication, je précise souvent que
la co-localisation est clef. Cette observation est confortée par les
études "officielles" (de IBM !) qui décrivent qu'au delà de 15m la
communication s'effondre. Aleister Cockburn utilise la métaphore
suivante : au delà de la distance d'un bus la communication s'effondre.

Ces dernières années je pourrais remettre en question cette question de
distance et de co-localisation car les nouveautés technologiques peuvent
la rendre invisible. Sous couvert de plusieurs conditions, mais c'est
probablement l'objet d'un autre article.

## Travailler pour quelque chose qui nous dépasse, de plus grand que nous

Nous avions parlé d'un objectif clair pour engager les joueurs de jeux
vidéos. Dans "la vraie vie" -- comme on l'entend souvent -- celui doit
être habité par du sens. C'est la fable du tailleur de pierre, je suis
engagé si je ne taille pas simplement une pierre, ou même si je ne fais
que cela pour nourrir ma famille, je suis engagé, impliqué, si je vois
la cathédrale que nous batissons au travers de ma taille.

## Conclusion

Je reprends comme je le disais dans cette série des écrits passés, qui
me semblent toujours d'actualité avec des conversations actuelles et
récentes. Je vais continuer ce travail de réfection.

Pour conclure sur l'engagement : **un objectif clair qui soit habité par
du sens, des règles claires mais qui laissent assez d'espace pour
l'autonomie et le sentiment de contrôle, du feedback qui permette un
sentiment de progrès régulier, une invitation et les conditions d'une
dynamique de groupe.**

### Menu

* [Menu](/organisations-vivantes/)
* [Chapitre 1 : Monde agile, monde complexe](/organisations-vivantes/chap1/)
* [Chapitre 2 : Penser organique](/organisations-vivantes/chap2/)
* [Chapitre 3 : Dynamique des formes](/organisations-vivantes/chap3/)
* [Chapitre 4 : Choisir sa voie ?](/organisations-vivantes/chap4/)
* [Chapitre 5 : Éléments constituants](/organisations-vivantes/chap5/)
* [Chapitre 6 : Changer la trajectoire de nos organisations](/organisations-vivantes/chap6/)
* [Chapitre 7 : Conclusion](/organisations-vivantes/chap7/)


[^1]: Je préfère mentor (« Guide attentif et sage, conseiller
    expérimenté » dit le Larousse) à coach (ambivalent et prétentieux à
    mes yeux, je ne me sens pas de te changer, ni la compétence, ni le
    droit, ni l’envie).

[^2]: The Machine that changed the world, James P. Womack, Daniel T.
    Jones, and Da-

    niel Roos.

[^3]: J’insiste volontairement sur l’aspect purement économique. Ce
    n’est pas nécessairement ma tasse de thé mais j’évolue moi-même en
    environnement concurrentiel et je sais que c’est là le nerf de la
    guerre pour les entreprises qui font appel à moi. La bonne nouvelle
    est que pour faire réussir son entreprise aujourd’hui il faut
    engager et responsabiliser les personnes comme a pu le faire Toyota.

[^4]: https://hbr.org/2014/11/a-primer-on-measuring-employee-engagement

[^5]: The Mythical Man-Month : Essays on Software Engineering, Fred
    Brooks

[^6]: [*http://www.areyouagile.com/2016/07/faciliter-ou-batir/*](http://www.areyouagile.com/2016/07/faciliter-ou-batir/)

[^7]: http://fr.wikipedia.org/wiki/Organisation

[^8]: Conversations avec mon frère, chercheur, spécialiste du cristal

[^9]: [*http://www.geowiki.fr/index.php?title=Comment\_se\_forment\_les\_cristaux*](http://www.geowiki.fr/index.php?title=Comment_se_forment_les_cristaux)

[^10]: http://www.geowiki.fr/index.php?title=Comment\_se\_forment\_les\_cristaux

[^11]: Dans les commentaires Nicolas Delahaye indique que pour plus
    approfondir cette approche : *Nudge marketing: Comment changer
    efficacement les comportements* de Eric Singler et *Makestorming: Le
    guide du corporate hacking* de Marie-Noéline Viguié, Stéphanie
    Bacquere

[^12]: http://www.geowiki.fr/index.php?title=Troncatures

[^13]: http://www.geowiki.fr/index.php?title=Troncatures

[^14]: http://www.geowiki.fr/index.php?title=Les\_macles

[^15]: J.Scott Turner, The extended organism, *I actually wish to
    explore an idea : that the*

    *edifices constructed by animals are properly external organs of
    physiology.*

[^16]: Étonnamment ces macles ressemblent beaucoup aux guildes proposés
    par une

    organisation comme Spotify.

[^17]: “Et pourquoi pas parler des villes du moyen-Age ? Un exemple plus
    proche de chez "nous" (càd dont nous pouvons expérimenter encore les
    traces) et associé à moins d'interprétations aujourd'hui (pauvreté,
    insalubrité) me paraîtrait plus concret.” m’écrit Paul-Georges
    Crismer. Certainement c’est une bonne idée. Il se trouve que je suis
    tombé sur un document qui a résonné en moi et qui parlait des
    bidonvilles. Les bidonvilles sont cependant plus liés à
    l’augmentation sans fin de notre population et à la fin proche de
    nos ressources, ils me semblent bien adaptés au propos.

[^18]: http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf

[^19]: http://www.citiesalliance.org/sites/citiesalliance.org/files/Slum-Upgrading-Lessons-from-Brazil.pdf

[^20]: Fred Brooks, https://archive.org/details/mythicalmanmonth00fred

[^21]: Voir le ’Plaisir d’appartenance à un groupe
    ([*http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/*](http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/))
    suivant concernant les écrits de Christopher Allen et Robin Dunbar

[^22]: « Social : Why Our Brains Are Wired to Connect », Matthew D.
    Lieberman

[^23]: ([*http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/*](http://areyouagile.com/2011/06/appartenance-a-un-groupe-et-niveau-de-satisfaction/)

[^24]: “En lisant ces paragraphes j'ai eu un choc, parce que j'aime la
    cohérence. Pourquoi écrivez-vous ce livre alors? Pourquoi ces
    digressions sur les cristaux et les termitières ? En effet, une
    image c'est 300 à 3000 mots. Quelle valeur supplémentaire ont donc
    les mots pour que vous en veniez à écrire un livre ? Bref, j'aime
    les discours mesurés. Selon moi, chaque médium a son utilité.
    D'ailleurs, comment pourrais-je vous communiquer ce que je vous
    écris avec un dessin sans que vous interprétiez ? Le dessin aussi
    peut introduire des ambiguïtés. Bref, je que j'aimerais trouver dans
    ce que vous écrivez à propos des mots ce sont les circonstances dans
    lesquelles un dessin vous semble plus approprié.” S’insurge
    Paul-Georges Crismer en commentant ces mots. Je suis bien d’accord.
    Lorsque je ne serai plus incohérent ou paradoxal je serai
    certainement mort. Les mots ont la force de rester, de m’aider à
    organiser ma pensée. Si je pousse dans l’autre sens c’est que dans
    notre culture la culture de la puissance de l’écrit est considérable
    et qu’il faut savoir la mettre en perspective.

[^25]: « Blah blah blah », Dan Roam,
    http://changethis.com/manifesto/show/88.01.BlahBlahBlah

[^26]: Brain rules, John Medina

[^27]: Brain rules, John Medina

[^28]: Harrison Owen, Dancing with Shiva
    (https://www.youtube.com/watch?v=APD7oQ3xrSA)

[^29]: “Entre le cristal et la fumée. Essai sur l'organisation du
    vivant”, Henri Atlan

[^30]: “Social”, Liebermann

[^31]: Jeff Patton, User Story Mapping. //TODO aller chercher la source
    originale

[^32]: Chip & Dan Heath, “Switch : How to Change Things When Change Is
    Hard”

[^33]: “Thinking, fast and slow”, Daniel Kahneman

[^34]: “Petit traité de manipulation à l’usage des gens honnêtes”,
    Robert-Vincent Joule

[^35]: “To the desert and back : The Story of One of the Most Dramatic
    Business Transformations on Record”, Philip H. Mirvis

[^36]: OpenSpace Agillity : http://openspaceagility.com/
