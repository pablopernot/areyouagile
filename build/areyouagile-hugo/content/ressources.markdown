---
sidebar: true
title: Ressources
url: /ressources/
---

## Lectures

### Guide de survie

[Le guide de survie](/pdf/guiderapide.pdf)

Petit document rappelant les bases pour survivre dans un monde complexe. Avec glossaire. Avec bibliograpĥie. 

### Mini livre "La horde agile"

- [La horde agile, le mini livre](https://github.com/pablopernot/lahordeagile/blob/master/lahordeagile.pdf?raw=true), pdf, 42 pages, \~4mo
- [la horde agile, le mini livre (epub)](https://github.com/pablopernot/lahordeagile/blob/master/la-horde-agile.epub?raw=true),
    epub, 42 pages, \~3.5mo
- [Article associé](/2014/01/mini-livre-la-horde-agile/) avec les informations complémentaires

### Livre en cours depuis 2014... "Les organisations vivantes" 

- [Les organisations vivantes - le texte en cours](/organisations-vivantes/)   
- [Les organisations vivantes - ancien article](/2016/10/organisations-vivantes-votre-aide/)

### Jeux, ateliers

Les jeux et ateliers auxquels j'ai pu participé à leur création. La
scierie à pratiques ce fut avec
[Stéphane](https://twitter.com/langlois_s), Le projet dont vous êtes le
héros agile avec [Bruno](http://jehaisleprintemps.net/) et
[Antoine](https://blog.crafting-labs.fr/).

-   [La scierie à pratiques](/pages/la-scierie-a-pratiques.html), un jeu
    pour échanger sur nos pratiques.
-   [Le projet dont vous êtes le héros
    agile](http://www.areyouagile.com/heros/) avec Bruno & Antoine. Un
    parcours sur la culture agile.
-   [l'espace des mèmes](/2015/07/lespace-des-memes/), mutation de notre
    culture basé sur l'open space et l'open agile adoption.

### Peetic

Un scénario qui permet d'outiller mes formations, on y retrouve pas mal
d'éléments d'un projet/produit agile.
[Claude](http://www.aubryconseil.com/) m'a fait le plaisir d'en intégrer
des morceaux dans son livre ("Scrum") et nous l'utilisons dans le [raid
agile](http://www.raidagile.fr).

-   2012 : [Episode 1](/2012/11/peetic/)
-   2012 : [Conversations](/2012/12/peetic-conversations/)
-   2014 : Un article de [l'idée au plan de
    livraison](/2014/06/de-lidee-au-plan-de-livraison/)
-   2015 : [Un peu de
    croquettes](/2015/05/peetic-2015-un-peu-de-croquettes/)

### Très vieux supports de formation

-   [Support de formation "agile" généraliste sur 3
    jours](/pdf/pratiques_de_lagilite_2_1_15.pdf), 12.5mo, pdf.
-   [Initiation agilité 1 journée](/pdf/initiation-agilite-0-6.pdf),
    6.5mo, pdf.
-   [Agilité pour les entrepreneurs, donnez l'initiative à vos
    équipes](/pdf/entrepreneurs.pdf), mars 2014, 7.2mo, pdf.

*Supports en mode *beta* (qui ne seront jamais consolidés)* :

-   [Product Owner](/pdf/product-owner-2014-1-0.pdf), 8.5mo, pdf
-   [Kanban](/pdf/kanban-0-8.pdf), 12mo, pdf
-   [Lean Software Development](/pdf/LSD-08.pdf), 8.5mo, pdf

*Des modules sur une demi-journée* :

-   [Vision & Sens](/pdf/vision-sens-1-1.pdf), 1.9m, pdf
-   [Expression du besoin](/pdf/expression-du-besoin-1-1.pdf), 956ko,
    pdf
-   [Introduction à Kanban](/pdf/kanban-1-3.pdf) , 2.2m, pdf
-   [Optimiser la valeur](/pdf/optimiser-1-1.pdf) (Value Stream
    Mapping), 683ko, pdf
-   [Zoom Scrum](/pdf/zoom-scrum-1-1.pdf), 800ko, pdf
-   [Responsable produit](/pdf/responsable-produit-1-1.pdf), 2.8m, pdf

Un [Scrumshot](/2012/11/scrumshot/) qui me permet de faire un panorama
sympa sur Scrum.

## Raid Agile   

[Le raid agile](http://raidagile.fr)

Une formation différente. 

## La crypte

Bienvenue dans ma petite crypte. Je range ici des vieilles choses qui
n'ont pas de liens avec le blog si ce n'est que c'est moi qui les ai
réalisées. Elles ne sont pas non plus liées à l'agilité, quoique...

### Zepablo's Led Zeppelin

[Site](http://pablo.pernot.free.fr) maintenu de 1997 à 2003 concernant
**Led Zeppelin**.

### DEA non-sens & absurde chez les comiques cinématographiques

Un DEA (Diplôme d'Etudes Approfondies) en Histoire du cinéma
(communication & audiovisuel) daté de 1994. Eh oh, un peu de retenue, je
n'avais que 23 ans... (je vais l'OCRiser et le "porter" en html asap).
Il manque aussi les illustrations (les feuilles que j'ai retrouvé ne les
avaient plus), je me charge de les retrouver.

*Je vous recommande l'utilisation du clique droit*

-   [introduction](/pdf/01-nonsens-absurde-intro.pdf) \[4.2mo\]
-   [première partie : le non-sens et
    l'absurde](/pdf/02-nonsens-absurde-premierepartie.pdf) \[9.0mo\]
-   [deuxième partie : le burlesque
    muet](/pdf/03-nonsens-absurde-deuxiemepartie.pdf) \[17.4mo\]
-   [troisième partie : le burlesque
    parlant](/pdf/04-nonsens-absurde-troisiemepartie.pdf) \[13.9mo\]
-   [filmographie-bibliographie-annexes-index-sommaire](/pdf/05-nonsens-absurde-annexes.pdf)
    \[15.4mo\]
