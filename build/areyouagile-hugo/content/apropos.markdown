---
type: section
layout: apropos
title: A Propos
url: /a-propos/
sidebar: true
---

![Pablo Pernot](/images/pablo-oct2017-400x400.jpg)

## Profession de foi

Aujourd'hui pour réformer les organisations, repenser les produits, re-imaginer ses relations sociales en entreprises, définir si on souhaite utiliser une recette connue (comme un socle à la "spotify", "SAFe", "Less", etc.), des cadres ("Scrum", "Kanban"), des approches ("Lean Startup", Beyond Budgeting", "Holacratie", "Design Thinking", etc) reste une étape importante, mais à la portée de tous. C'est une petite partie de mon rôle de vous aider cependant sur ces sujets. 

Définir jusqu'où, à quel rythme, comment, dans quel sens, l'adaptation et l'émergence de votre propre écosystème doit se réaliser est déjà moins facile à appréhender. Mais si c'était facile cela ne serait pas la question. C'est une partie du rôle de personnes comme moi de vous accompagner là dessus. 

Mais j'en viens à penser avec le temps que la clef de voûte de tout ceci c'est la volonté (l'intention) et la façon d'être (l'attitude) de personnes importantes. Importantes dans le sens où elles ont des leviers, où elles se trouvent à des moments et des endroits stratégiques[^importantes]. Mon rôle consiste à accompagner ces personnes. À m'assurer de leurs intentions, ou les faire éclore, les coacher dans leur savoir être, les nourrir d'autres expériences et d'un savoir. 

Les évolutions des organisations passent par les transformations des personnes qui les composent. L'organisation est la somme des individus. Je travaille avec les individus sur leur savoir être, avec ces personnes importantes[^importantes] qui orientent, déclenchent, les flux et les énergies de l'organisation. Je travaille avec des gens qui réalisent cela et qui veulent s'emparer de ces sujets, ou des organisations qui souhaitent que des gens réalisent cela. 

Bref...

Après une maîtrise sur les Monty Python, un DEA sur le non-sens et l’absurde, l’entame d’un doctorat sur les comiques cinématographiques il parait normal que je me sois lancé dans le management et la conduite du changement, c’est -finalement- une suite logique.

[^importantes]: Il n'y a pas de jugement de valeur. D'autant que généralement tout cela est cyclique.  

## [Mes sujets d'interventions et mes conférences passées](/conferences/).

Cet [interview avec Stéphane pour InfoQ](http://www.infoq.com/fr/articles/la-voix-du-coach-pablo-pernot) aidera-t-elle à mieux me connaître ? 

Je suis très fier d'avoir été à la création et de poursuivre les aventures du [raid agile](http://raidagile.fr) (avec Claude Aubry) depuis 2015, de la [schoolof po](https://schoolofpo.com) et la [conférence associée](https://laconf.schoolofpo.com) (avec Dragos Dreptate), et de [wake up rh](https://wakeuprh.fr) (avec Pauline Garric), depuis 2016 les deux. Enfin dans les couloirs de XP2011 Barcelona, d'avoir en compagnie de Olaf Lewitz et Jurgen Appelo, déclenché "Agile Lean Europe, ALE".   

[Mes propositions de sujets pour les conférences](https://www.areyouagile.com/conferences/).

## beNext

Depuis 2015 je suis enchanté d'avoir rejoint [beNext](http://benextcompany.com), à cette époque j'écrivais :

*Chaque choix a une valeur. Chaque choix a une date d'expiration. Ne pas faire un choix trop tôt avant de savoir vraiment pourquoi. Chaque choix qui contient des incertitudes peut se révéler encore plus gratifiant. C'est avec ces **Real Options** de Chris Matts en tête que j'entame une nouvelle partie de ma vie en choisissant, avec enthousiasme, et l'amitié de David, de me lancer dans l'aventure [beNext](http://benextcompany.com).*

*Chez beNext je souhaite apporter, partager et apprendre, dans un espace où les personnes et les savoirs se libérent et s'émancipent. Où chacun puisse se sentir unique et partie d'un tout. Être acteur de ces nouvelles organisations pionnièrent des temps à venir.*  

*Les sourires, les idées et les propositions des benexters m'ont charmés. Le choix était limpide.*

Septembre 2015. 

## Mieux me connaître ?

Pour mieux me connaître, jetez un dé, et piochez la citation :
       
1. *Aucun plan ne survit au premier contact avec l'ennemi* -- Von Moltke
2. *Je refuse de faire partie d'un club qui m'accepterait comme membre !*  -- Groucho Marx
3. *Ils ne savaient pas que c'était impossible, alors ils l'ont fait* -- Mark Twain
4. *Trop de sages femmes, le bébé est mort* -- proverbe roumain
5. *Ne dîtes pas les faits ! Dîtes la vérité !* -- Maya Angelou
6. *Ne comptez pas vos poulets avant qu'ils soient éclos* -- Esope

## Me joindre ? 

Par la [page contact](/contact/), ou par mon [compte twitter](https://twitter.com/pablopernot) ou [linkedin](https://www.linkedin.com/in/pablopernot/).

## Remerciements

Merci à l'Université de Caen pour son [dictionnaire de synonymes en ligne](http://www.crisco.unicaen.fr/des/).
Merci à [HUGO](https://gohugo.io/) et son incroyable vitesse de génération pour générer ce blog. 
Merci à Frank a.k.a. [DirtyF](https://twitter.com/dirtyf) pour m'avoir poussé vers le thème [Lanyon](https://github.com/tummychow/lanyon-hugo). 

