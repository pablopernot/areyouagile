for i in $(ls *) 
do 

curl -X PUT \
  -H "X-Algolia-API-Key: **KEY**" \
  -H "X-Algolia-Application-Id: **ID**" \
  --data-binary @$i \
  "https://**ID**.algolia.net/1/indexes/areyouagile/$i"

done