#!/usr/bin/python 
# coding: utf8

import re
import os
import os.path 
import json
import string


## pattern url 

regslug = re.compile(r"slug: ([\w\-]*)$", re.MULTILINE | re.IGNORECASE)
regdate = re.compile(r"date: (\d+\-\d+).*$", re.MULTILINE | re.IGNORECASE)
regday = re.compile(r"date: (\d+\-\d+-\d+).*$", re.MULTILINE | re.IGNORECASE)
regtags = re.compile(r"tags: \[(.*)\]$", re.MULTILINE | re.IGNORECASE)
regtitle = re.compile(r"title: (.*)$", re.MULTILINE | re.IGNORECASE)


for file in os.listdir("../content/posts/"):

	
	source = "../content/posts/" + file

	fs = open(source, 'r')

	text = fs.read()

	""" on récupère les tags l'url le titre """
	m = regslug.search(text)
	if m : 
		slug = m.group(1)
		
	m = regdate.search(text)
	if m : 
		ddd = re.sub('-', '/', m.group(1)).strip()

	m = regday.search(text)
	if m : 
		myday = re.sub('-', '', m.group(1)).strip()
		target = "json/" + myday
		if os.path.isfile(target):
			myday = myday+"-2"	
	else : 
		print "erreur avec " + file

	m = regtitle.search(text)
	if m : 
		titre = re.sub('[\,\'\"]+', ' ', m.group(1)).strip()
	else : 
		titre = ""

	m = regtags.search(text)
	if m : 
		tags = re.sub('[\,\'\"]+', ' ', m.group(1)).strip()	
	else: tags = ""

	url = "/"+ddd+"/"+slug+"/"


	""" nettoyage du texte """


	nourl = re.compile(r"\[.*?\]\(.*?\)^", re.DOTALL | re.IGNORECASE)
	text = re.sub(nourl, '', text)

	""" on split le text en mots en minuscule"""
	words = re.split("[\s\'\"\(\)\[\]\#\.\*\,'\-\/]+", text.lower())
	words = words + re.split("[\s\'\"\(\)\[\]\#\.\*\,'\-\/]+", tags.lower())

	""" table des mots inutiles """
	dontindex = ["par", "lui", "vos", "selon", "ces", "ici", "là", "lui", "une", "est", "donc", "pas", "sur", "date:", "title:", "slug:",
		"cet", "pdf", "avec", "les", "mes", "qui", "que", "chez", "non", "oui", "nous", "ainsi", "quoi", "dont", "nos", "aussi",
		"quand", "alors", "peux", "veut", "suis", "entre", "sinon", "etc", "elles", "ils", "vont", "dans", "sont", "ceux", "celle", "celles", 
		"faut", "parce", "cad", "tiens", "voici", "ceci", "item", "celui", "sera", "cela", "doit", "des", "ses", "mais", "bref", "cette"

	]

	motslourds = ["agile", "blog"]

	cleanwords = []
	for word in words:

		""" reduction par la taille """
		if len(word) > 17 or len(word) < 3 : continue
		
		""" mots inutiles """
		if dontindex.count(word) : continue
		if motslourds.count(word) : continue

		""" on injecte dans le nouveau tableau """
		if not cleanwords.count(word): 	cleanwords.append(word)		

	doc = {
		
			"objectID": myday,
			"url": url,
			"title": titre,
			"tags": tags,
			"content": string.join(cleanwords,' ')

		}

	fs.close()
	

	target = "json/" + myday + '.json'

	if os.path.isfile(target): print "le fichier " + target + "existe déjà !"

	ft = open(target, 'w')

	ft.write(json.dumps(doc))

	ft.close()
	


"""

[
  {
    "firstname": "Jimmie",
    "lastname": "Barninger",
    "zip_code": 12345
  },
  {
    "firstname": "John",
    "lastname": "Doe",
    "zip_code": null
  }
]

"""


"""

https://discourse.gohugo.io/t/keeping-algolia-up-to-date-automatically/8317


curl -X POST \
  -H "X-Algolia-API-Key: ${ALGOLIA_API_KEY}" \
  -H "X-Algolia-Application-Id: ${ALGOLIA_APPLICATION_ID}" \
  --data-binary @web/algolia.json \
  "https://${ALGOLIA_APPLICATION_ID}.algolia.net/1/indexes/${ALGOLIA_INDEX_NAME}/batch


{
  "requests": [
    { "action": "updateObject",
      "body": {
         ...,
         "objectID": "/foo/bar" } },


  """

"""

https://www.algolia.com/doc/rest-api/search/#batch-write-operations

"""

