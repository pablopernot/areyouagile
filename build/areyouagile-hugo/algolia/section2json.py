#!/usr/bin/python 
# coding: utf8

import re
import os
import os.path 
import json
import string


## pattern url 

regtitle = re.compile(r"title: (.*)$", re.MULTILINE | re.IGNORECASE)
regurl = re.compile(r"url: (.*)$", re.MULTILINE | re.IGNORECASE)

##base = "../content/organisations-vivantes/"
base = "../content/"

for file in os.listdir(base):

	if os.path.isfile(base+file):

		source = base + file

		fs = open(source, 'r')

		text = fs.read()

		m = regtitle.search(text)
		if m : 
			titre = re.sub('[\,\'\"]+', ' ', m.group(1)).strip()
		else : 
			titre = ""

		m = regurl.search(text)
		if m : 
			url = re.sub('[\,\'\"]+', ' ', m.group(1)).strip()	
		else : 
			print "erreur avec " + file
			continue

		""" nettoyage du texte """

		nourl = re.compile(r"\[.*?\]\(.*?\)", re.DOTALL | re.IGNORECASE)
		text = re.sub(nourl, '', text)


		""" on split le text en mots en minuscule"""
		words = re.split("[\s\'\"\(\)\[\]\#\.\*\,'\-\/]+", text.lower())
		
		""" table des mots inutiles """
		dontindex = ["par", "lui", "vos", "selon", "ces", "ici", "là", "lui", "une", "est", "donc", "pas", "sur", "date:", "title:", "slug:",
			"cet", "pdf", "avec", "les", "mes", "qui", "que", "chez", "non", "oui", "nous", "ainsi", "quoi", "dont", "nos", "aussi",
			"quand", "alors", "peux", "veut", "suis", "entre", "sinon", "etc", "elles", "ils", "vont", "dans", "sont", "ceux", "celle", "celles", 
			"faut", "parce", "cad", "tiens", "voici", "ceci", "item", "celui", "sera", "cela", "doit", "des", "ses", "mais", "bref", "cette"

		]

		motslourds = ["agile", "blog"]

		cleanwords = []
		for word in words:

			""" reduction par la taille """
			if len(word) > 17 or len(word) < 3 : continue
			
			""" mots inutiles """
			if dontindex.count(word) : continue
			if motslourds.count(word) : continue

			""" on injecte dans le nouveau tableau """
			if not cleanwords.count(word): 	cleanwords.append(word)		
			
		id = re.sub("/", "", url)
		doc = {
			
				"objectID": id,
				"url": url,
				"title": titre,
				"content": string.join(cleanwords,' ')

			}
		
		fs.close()
		
		target = "json-section/" + id

		if os.path.isfile(target): print "le fichier " + target + "existe déjà !"

		ft = open(target, 'w')

		ft.write(json.dumps(doc))

		ft.close()
		


"""

[
  {
    "firstname": "Jimmie",
    "lastname": "Barninger",
    "zip_code": 12345
  },
  {
    "firstname": "John",
    "lastname": "Doe",
    "zip_code": null
  }
]

"""


"""

https://discourse.gohugo.io/t/keeping-algolia-up-to-date-automatically/8317


curl -X POST \
  -H "X-Algolia-API-Key: ${ALGOLIA_API_KEY}" \
  -H "X-Algolia-Application-Id: ${ALGOLIA_APPLICATION_ID}" \
  --data-binary @web/algolia.json \
  "https://${ALGOLIA_APPLICATION_ID}.algolia.net/1/indexes/${ALGOLIA_INDEX_NAME}/batch


{
  "requests": [
    { "action": "updateObject",
      "body": {
         ...,
         "objectID": "/foo/bar" } },


  """

"""

https://www.algolia.com/doc/rest-api/search/#batch-write-operations

"""

