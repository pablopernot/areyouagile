 

# How to plan in plan roadmap in advance in Agile approach and how to follow execution

(Pablo Pernot - june 2018) I think the idea is not wether we can or cannot plan in advance. The idea is more : every plan will be not executed like it is announced.

	> "No plan survives first contact with the enemy" -- Von Moltke.

So the point is we can plan in order to know where we want to go. But no plan should become engagement. Only a direction, a forecast. Hence it is easy to plan. The recommended way is to use "release plan". It is very useful to communicate and to arbitrate. See example. 

\begin{figure}[h]
\centering
\includegraphics[width=10cm]{images/release-plan.png}
\caption{Release plan}
\end{figure} 


(Michal Choinsky - june 2018) @Pablo What I was thinking about is preparing information about aspirations and progress for high level management (C and C-1 level) when working in Agile. In tradition approach you have well defined target after analysis phase and then you are reporting if it is done or not. Eventually you are presenting some distractions or risks. 

In Agile you have several aspects that makes it more complicated. For ex:  

* How to describe aspiration when not having detail analysis
* When progressing you backlog is becoming very detailed, in MVP approach execution of functionalities is spread in time. It’s good for business but it’s hard to say If something is done or not.
* It is common to report on epic or even higher level, but epics have tendencies to evolve. I had situations that epic were “active” for more than one year but a lot was already done
* One Sprint last 2 weeks, it is pointless to focus on that when discussing with management. Maybe it is good to have another unit of time (ex. Phases) but how to describe them not having analysis.  

In most cases in Agile I had situations that after some time picture was very different than on the beginning. How to show and explain that. How to convince that this is good, on track, and final picture is more valuable for clients than draft on beginning.

(Alexander Frumkin - July 2018) @Pablo, great point. To deliver MVP we should not concentrate on a single epic, but on multiple. As the result, a particular epic could be complete in multiple releases. How about reporting progress on releases? JIRA provides an out of the box burn up chart “version report” (terms version and release are used interchangeably that may be confusing) that not only makes progress visible, but also provides scientific forecast for release day based on current scope and velocity.

(Pablo Pernot - July 2018) @MiCho well I hear/read some ideas that we commonly fight. 

You are discussing several topics. Make me think about : how to measure we are on the right track. With manager : measures are important. But usually they are not the good ones. Or you measure something unuseful : numbers of functionnality, code line, time spent : all these things has nothing to do with value. Value is key. Good measures could be : customer happiness, or customer connection per..., or how much time you gain ... or how many new customers, or how less hotline calls, etc. real value.

So the idea is not : you're reporting about the product or the project but about the value delivered.
Depending of the managers : or they are real entrepreneurs and they quickly understand it. Or they are much more in a political process (no judgement here), and their goal is : the project has to be done, has to work ; whatever the value delivered. The goal is really different. In that case you have to secure them to win their support.

Secure them by offering a way to make things differently (with agile) and does not make it appears like a risk.

So @MiCho your question are opening a wide range of possibilities. I would wrap up a first answer with : focus on value measurement (entrepreneur managers) and secure insecured managers (traditionnal managers).

@Alex sure the interesting work (but hard work) is to split features, epics, stories, in something deliverable with value. To validate or invalidate our assumptions, our value. Impact Mapping (in french here : https://www.areyouagile.com/2017/02/cartographie-strategie-impact-mapping/ -- in french...) is a good workshop to define "critical path". Maximise value minimize effort (Jeff Patton, user story mapping is also a tool, https://www.areyouagile.com/2017/01/cartographie-plan-action/ -- in french) is key. That is a great learning path for product management. I'm not fond of tools, like Jira. And velocity is usually meaningless but we probably need a lot of time to explain that to the managers.  I mean velocity based on "points". Tell me how many user stories you delivered every sprint is far better. That is a good velocity. And be sure that most of the stories delivered value by itselves.

# How to prepare management information when working in Agile – what to focus on

# How to combine Agile projects with non-Agile environment (monolithic and waterfall core applications, governance projects and teams etc.)

(Pablo Pernot - june 2018) In a word : painful. And agile will lose. Painful because of the dependencies. Agile will lose because it will try to deliver small bits, prioritized by value in order manage risks, to get value as soon as possible (benefits, learning, etc.). If this agile team is dependant with a non agile team this way of working make no sense. 


What can be helpful is to set a Kanban Portfolio to manage dependencies and by making them visible try to mitigate the risks related. Here is a blog post about (from me) about Kanban Portfolio. It is in french, but the pictures will probably tell a lot. 

* [Kanban Portfolio in french](https://www.areyouagile.com/2016/01/portfolio-projets-kanban-partie-1/)
* [Kanban Portfolio with google translate / english](https://translate.google.fr/translate?hl=fr?sl=fr&tl=en&u=https%3A//www.areyouagile.com/2016/01/portfolio-projets-kanban-partie-1/)

The goal is to minimize dependencies. To minimize dependencies we try to set-up multidisciplinary teams. So they can handle the product from the idea to the delivery. That is why agile is perceived as fast. But in fact they are not going faster, they drastically reduce "waiting time" because they handle by themselves all the tasks. That is the goal. Having dependencies is difficult. Having dependencies with a planning approach and culture approach so much different is painful. It will be very hard to have no dependencies. But you have to tend to it. 

\pagebreak 

## Reminder : agile is iterative and incremental

\begin{figure}[!h]
\centering
\includegraphics[width=12cm]{images/agile.jpg}
\caption{Agile is iterative and incremental}
\end{figure}


## Reminder : waterfall development

\begin{figure}[!h]
\centering
\includegraphics[width=8cm]{images/cycle-v.jpg}
\caption{Waterfall}
\end{figure}

\pagebreak 

## Another Waterfall approach 

\begin{figure}[!h]
\centering
\includegraphics{images/waterfall.png}
\caption{Waterfall by Astérix}
\end{figure}

# How to identify dependencies with other projects / systems in advance to be able to plan and order integration

# Role of PM vs SM – do we need PM in Agile?

PM : project manager ? SM : Scrummaster ? I think so. Hence the difference is crucial. In modern days we observed that to answer complexity of the world (everything is connected, linked, everything goes fast, too fast, etc) we can't rely on one single person. One single person is too slow. One single person is wrong. We rely on collective intelligence, team dynamics (but not too huge team, no more than 8/10 people). We now deliver regularly value. So we do not need any more a single person controlling and organizing the work, but we need someone who is facilitating the team in delivering value. That is a clear difference.    

# Budgeting in Agile – business justification, committees vs flexibility

# How to create functional and technical infrastructure upfront in Agile


\pagebreak 

# Thanks to 

* Alexander Frumkin
* Michal Choinsky
* Pablo Pernot

---

\tiny 

Generated with : pandoc agile-ring-faq.markdown --pdf-engine=xelatex --template=agile-ring-faq-template.tex -o agile-ring-faq-$(date +%Y-%m-%d).pdf


